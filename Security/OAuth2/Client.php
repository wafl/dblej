<?php

namespace DblEj\Security\OAuth2;

/**
 * An OAuth2 client.
 */
class Client
extends \DblEj\Communication\Http\ClientBase
{
    private $_clientId;

    /**
     *
     * @param string $serverUrl
     * @param string $resourcePath
     * @param string $clientId
     * @param boolean $verifySslPeer
     * @param boolean $verifySslHost
     * @param boolean $followRedirects
     * @param string $cookieWriteFile
     * @param string $cookieReadFile
     * @param string $httpUsername
     * @param string $httpPassword
     */
    public function __construct($serverUrl, $resourcePath, $clientId, $verifySslPeer = false, $verifySslHost = false, $followRedirects = false, $cookieWriteFile = null, $cookieReadFile = null, $httpUsername = null, $httpPassword = null)
    {
        parent::__construct($serverUrl . $resourcePath, $verifySslPeer, $verifySslHost, $followRedirects, $cookieWriteFile, $cookieReadFile, $httpUsername, $httpPassword);
        $this->_clientId = $clientId;
    }

    /**
     *
     * @param string $redirectUri
     * @param string $scope
     * @param string $state
     * @param string $responseType
     * @param string $loginHint
     * @param string $serverDefinedParams
     * @param string $httpHeaders
     * @param string $clientIdField
     * @param string $redirectUriField
     * @param string $scopeField
     * @param string $stateField
     * @param string $responseTypeField
     * @return string
     */
    public function GetUrl($redirectUri, $scope, $state = null, $responseType = "code", $loginHint = null, $serverDefinedParams = null, $httpHeaders = null, $clientIdField = "client_id", $redirectUriField = "redirect_uri", $scopeField = "scope", $stateField = "state", $responseTypeField = "response_type")
    {
        $accessParams                     = array();
        $accessParams[$clientIdField]     = $this->_clientId;
        $accessParams[$redirectUriField]  = $redirectUri;
        $accessParams[$scopeField]        = $scope;
        $accessParams[$stateField]        = $state;
        $accessParams[$responseTypeField] = $responseType;

        $url = $this->_serverUrl . "?" . \DblEj\Communication\Http\Util::MakeQueryString($accessParams);
        return $url;
    }
}