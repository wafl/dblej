/**
 * @namespace DblEj.Security.OAuth2
 */
Namespace("DblEj.Security.OAuth2");

/**
 * @class Client An OAuth2 client.
 * @extends Class
 */
DblEj.Security.OAuth2.Client = Class.extend({
    /**
     * @constructor
     * @param {String} serverUrl
     * @param {String} resourcePath
     * @param {String|Number} clientId
     * @param {Function} resultCallback
     */
    init: function (serverUrl, resourcePath, clientId, resultCallback)
    {
        this._resourceUrl = serverUrl + resourcePath;
        this._clientId = clientId;
        DblEj.EventHandling.Events.AddHandler(window, "message", function (e) {
            resultCallback(e.data);
        });
    },
    /**
     * @function GetAccess
     *
     * @param {String} redirectUrl
     * @param {String} scope
     * @param {String} responseType
     * @param {String} state
     * @param {String} loginHint
     * @param {Array} serverDefinedParams
     * @returns {Client} Returns <i>this</i> for chained method calls.
     */
    GetAccess: function (redirectUrl, scope, responseType, state, loginHint, serverDefinedParams)
    {
        var paramString = "client_id=" + this._clientId + "&redirect_uri=" + redirectUrl + "&scope=" + scope + "&state=" +
            state + "&response_type=" + responseType;
        if (!IsNullOrUndefined(loginHint))
        {
            paramString += "&login_hint=" + loginHint;
        }
        if (!IsNullOrUndefined(state))
        {
            paramString += "&state=" + state;
        }
        if (!IsArray(serverDefinedParams))
        {
            for (var paramName in serverDefinedParams)
            {
                paramString += "&" + paramName + "=" + serverDefinedParams[paramName];
            }
        }

        var childWindow = window.open(this._resourceUrl + "?" + paramString, 'OAuth2AccessRequest', 'height=600,width=450');
        return this;
    },
    /**
     * @function GetAccessCode
     * @param {String} redirectUrl
     * @param {String} scope
     * @param {String} state
     * @param {String} loginHint
     * @param {Array} serverDefinedParams
     * @returns {Client} Returns <i>this</i> for chained method calls.
     */
    GetAccessCode: function (redirectUrl, scope, state, loginHint, serverDefinedParams)
    {
        return this.GetAccess(redirectUrl, scope, "code", !IsNullOrUndefined(state) ? state : null, !IsNullOrUndefined(loginHint) ? loginHint : null, !IsNullOrUndefined(serverDefinedParams) ? serverDefinedParams : null);
    },
    /**
     * Request an access token.
     *
     * @param {String} redirectUrl
     * @param {String} scope
     * @param {String} state
     * @param {String} loginHint
     * @param {Array} serverDefinedParams
     * @returns {Client} Returns <i>this</i> for chained method calls.
     */
    GetAccessToken: function (redirectUrl, scope, state, loginHint, serverDefinedParams)
    {
        return this.GetAccess(redirectUrl, scope, "token", !IsNullOrUndefined(state) ? state : null, !IsNullOrUndefined(loginHint) ? loginHint : null, !IsNullOrUndefined(serverDefinedParams) ? serverDefinedParams : null);
    }
});
