<?php

namespace DblEj\Security\OAuth2;

/**
 * Thrown if there is an unexpected event during OAuth2 authetication.
 */
class ClientException
extends \DblEj\Communication\Http\Exception
{

    /**
     *
     * @param \DblEj\Security\OAuth2\Client $client
     * @param string $errorMessage
     * @param integer $httpErrorCode
     * @param integer $severity
     * @param \Exception $innerException
     */
    public function __construct(Client $client, $errorMessage = "", $httpErrorCode = 500, $severity = E_ERROR, $innerException = null)
    {
        parent::__construct($client->Get_ServerUrl(), $errorMessage, $httpErrorCode, $severity, $innerException);
    }
}