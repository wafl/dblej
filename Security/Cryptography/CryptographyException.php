<?php

namespace DblEj\Security\Cryptography;

/**
 * Thrown if there is an unexpected event or unexpected data as part of a
 * cryptography related operation.
 */
class CryptographyException
extends \DblEj\System\Exception
{

    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("$message", $severity, $inner);
    }
}