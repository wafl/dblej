<?php

namespace DblEj\Communication;

abstract class ApiRequestHandler
extends Http\RequestHandlerBase
{
    private $_app = null;
    public function HandleHttpRequest(Http\Request $request, $apiCallFunction, \DblEj\Application\IApplication $app)
    {
        $this->_app = $app;
        $headers = new \DblEj\Communication\Http\Headers("application/json", \DblEj\Communication\Http\Headers::CACHEPOINT_NONE, 0, true);
        $callResult = $this->$apiCallFunction($request, $apiCallFunction, $app);

        if ($request->Get_ReindexArrayResults())
        {
            if (is_array($callResult))
            {
                $callResult = array_values($callResult);
            }
        }
        return new Http\ApiResponse($this->GetAjaxCallName($apiCallFunction), JsonUtil::EncodeJson($callResult), 200, $headers);
    }

    public function GetAjaxCallName($functionName)
    {
        $className = get_called_class();
        $lastSlashPos = strrpos($className, "\\");
        if ($lastSlashPos > -1)
        {
            $className = substr($className, $lastSlashPos+1);
        }
        return "$className.$functionName";
    }
}