<?php
namespace DblEj\Communication;

/**
 * Thrown when there is an unexpected situation during communication.
 */
class Exception
extends \DblEj\System\Exception
{
    private $_errorOutputCode;
    private $_communicationTargetOrUri;

    public function __construct($communicationTargetOrUri, $errorMessage = null, $errorOutputCode = null, $publicDetails = null, \Exception $innerException = null, $severity = E_ERROR)
    {
        if (!$errorMessage)
        {
            $errorMessage = "There was an error while communicating with $communicationTargetOrUri";
        }
        $this->_errorOutputCode = $errorOutputCode;
        $this->_communicationTargetOrUri = $communicationTargetOrUri;

        parent::__construct($errorMessage, $severity, $innerException, $publicDetails);
    }

    /**
     * The arbitrary string that the application should use to determine the correct output to send for this exception.
     *
     * When the application responds to a request with this exception it will use ErrorOutputCode to determine the type of output to generate.
     * For example, you might use the string "Resource Not Found" for any exception resulting in a resource not being found.
     * The application would determine what type of output to send based on the ErrorOutputCode.
     *
     * Whatever value you use, it is up to the application to interpret the code and send the correct output type (or it may choose to send generic output).
     *
     * @return string
     */
    public function Get_ErrorOutputCode()
    {
        return $this->_errorOutputCode;
    }

    public function Get_CommunicationTargetOrUri()
    {
        return $this->_communicationTargetOrUri;
    }
}