<?php
namespace DblEj\Communication;

/**
 * Represents a response to a request.
 */
class Response
implements \DblEj\Communication\IResponse
{
    private $_content;
    private $_hasBeenSent = false;
    protected $_viewerRoles = ["\\DblEj\\Authentication\\IUser", "\\DblEj\\Authentication\\IUserGroup"];

    /**
     * Any type of response that doesn't have a better type definer.
     */
    const RESPONSE_TYPE_GENERAL = "RESPONSE_TYPE_GENERAL";

    /**
     * Create a Response.
     *
     * @param mixed $responseContent
     * The contents of the response.
     * The type of the contents depends on the request.
     */
    public function __construct($responseContent)
    {
        $this->_content = $responseContent;
    }

    /**
     * The contents of the response.
     *
     * @return mixed
     */
    public function Get_Response()
    {
        return $this->_content;
    }

    public function Get_ContentIsSymbolic()
    {
        return false;
    }

    /**
     * The type of the response.
     *
     * @return mixed
     */
    public function Get_ResponseType()
    {
        return self::RESPONSE_TYPE_GENERAL;
    }

    /**
     * Whether or not the response had been sent to the requestor.
     *
     * @return boolean
     */
    public function Get_HasBeenSent()
    {
        return $this->_hasBeenSent;
    }

    /**
     * Whether or not the response had been sent to the requestor.
     *
     * @param boolean $hasBeenSent
     */
    public function Set_HasBeenSent($hasBeenSent)
    {
        $this->_hasBeenSent = $hasBeenSent;
    }

    public function Get_ViewerRoles()
    {
        return $this->_viewerRoles;
    }

    public function Set_ViewerRoles(array $rolesArray)
    {
        $this->_viewerRoles = $rolesArray;
    }
}