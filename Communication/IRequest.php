<?php

namespace DblEj\Communication;

/**
 * Represents any request that solicits a response.
 * The data and format of the request is determined by the implementation.
 */
interface IRequest
{

    /**
     * The contents of the request.
     */
    public function Get_Request();
}