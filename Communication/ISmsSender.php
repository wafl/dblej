<?php

namespace DblEj\Communication;

/**
 * @deprecated since revision 1629 in favor of DblEj\Communication\Integration\SmsSender
 * @see \DblEj\Communication\Integration\SmsSender
 */
interface ISmsSender
{
    public function SendSms($fromNumber, $toNumber, $message);
}