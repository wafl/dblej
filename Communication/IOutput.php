<?php

namespace DblEj\IO;

interface IOutput
extends \DblEj\EventHandling\IEventRaiser
{

    public function sendOutput($output);
}