/**
 * @namespace DblEj.Communication
 */
Namespace("DblEj.Communication");

/**
 * @class JsonUtil 
 * A utility class for encoding/decoding objects to/from JSON.
 *
 * The methods in this class can be used to serialize objects that have private members
 * exposed via public property accessors.
 *
 * Instances of counterpart-compliant classes are
 * encoded with meta-data that allow them to be decoded
 * as an instance of the correct class.
 *
 * Likewise, JSON strings with encoded counterpart meta-data will be
 * decoded as an instance of the correct counterpart-compliant class.
 * 
 * This allows for cross-platform transmission of serialized data where
 * strong-typed data's types are preserved.
 *
 * For example:
 * Let's say there is a PHP class called "Employee" that
 * has properties that describe an employee and methods
 * that act on an employee record.
 *
 * Let's say there is also a Javascript class called "Employee"
 * that has the same properties, and some methods of it's own that
 * perform client-side actions on the employee record.
 *
 * Normally, if you serialize an instance of the PHP Employee class and send it to
 * Javascript (via an Ajax response, for example), it will be deserialized
 * on the client-side as a generic Javascript Object.
 * It's properties will be whatever public variables that were exposed by
 * the PHP object (which usually is none, in DblEj applications).  It will not
 * have any of the methods that the Employee class has.
 *
 * With DblEj, if you serialize an instance of the PHP Employee counterpart-compliant 
 * class and send it to Javascript (via an Ajax response, for example),
 * it will be deserialized on the client-side as an instance of the
 * Javascript Employee class.
 * It's properties will be whatever properties the javascript Employee class has defined,
 * and where those properties match public properties that were exposed by
 * the PHP object via accessors (which usually all of them, in DblEj applications),
 * their values will be the same as well.
 * Because it is an actual Employee object, and not a generic Object,
 * it will have all of it's normal methods.
 * 
 * Likewise, an instance of a counterpart-compliant Javascript Employee class
 * on the client-side that is encoded using the methods in this class
 * can be decoded on the server-side as an instance of the PHP Employee class.
 *
 * @static
 * @returns {DblEj.Communication.JsonUtil.Anonym$0}
 */
DblEj.Communication.JsonUtil = {};

/**
 * @method EncodeJson
 * Encode an object, array, or string into a JSON string.
 *
 * This method is a wrapper for the native JSON.stringify function.
 * If the object being encoded has a public method named <i>ToSerializable</i>,
 * as all counterpart-compliant Javascript classes do,
 * then the result of <i>ToSerializable</i> will be stringified instead of the actual object.
 * This allows classes to expose certain private members, which are normally exposed by
 * property accessors, so that they'll be serialized.
 *
 * This method will also include pertient counterpart information in the encoded object
 * so that it can be decoded to the correct type.
 *
 * @param {Object} object The object to be encoded into a JSON string.
 *
 * @return String
 * @static
 */
DblEj.Communication.JsonUtil.EncodeJson = function (object)
{
    if (IsDefined(object["ToSerializable"]))
    {
        return JSON.stringify(object.ToSerializable(), DblEj.Communication.JsonUtil._serializeReplacer);
    } else {
        return JSON.stringify(object, DblEj.Communication.JsonUtil._serializeReplacer);
    }
};

DblEj.Communication.JsonUtil._serializeReplacer =function(serializeKey, serializeValue)
{
    if (serializeValue == null)
    {
        return "";
    } else {
        return serializeValue;
    }
};

/**
 * @method DecodeJson
 * Decode a JSON string back into an object or an array of objects.
 * If any part of the JSON string represents a counterpart-compliant class instance and
 * the JSON string was created by JsonUtil.EncodeJson,
 * or any other counterpart-compliant encoder,
 * then the decoded object or it's parts/elements
 * will be of the same type as it was when encoded.
 * Otherwise, it will be a generic Object.
 *
 * Instead of a String, you can pass in an Object that has already been decoded
 * by a non-counterpart-compliant decoder.  If it had been originally encoded
 * by a counterpart-compliant encoder, and if the encoded object was counterpart-compliant,
 * then it will be re-decoded as an instance of the correct counterpart-compliant class.
 *
 * @param {String|Object} jsonStringOrObject
 * The JSON string to decode.
 * Alternately this can be an already-decoded object that was decoded
 * using a different decoder, in which case, the object will be re-decoded.
 *
 * @param {Function} callbackMethod
 * A Function to call when the decoded object is ready.
 *
 * There is a chance that although certain client-side classes exist, they haven't been
 * sent to the client yet.
 * Consider a typical web application, in which the client-side Javascript files live on the server.
 * In doesn't always make sense to download every single class file on page load.
 *
 * When decoding a JSON string, if a counter-part class definition is found
 * but the client-side class file doesn't exist, 
 * the client will request the file from the server.
 * As a result, there may be a delay before the string can be decoded into the correct
 * class instance.
 *
 * Rather than freeze the client and wait for the client-side class file to be
 * downloaded from the server, the decoder will instead return the generic Object
 * representation of the JSON string immediately.
 * Then, when the client-side class is finished downloading, the correct class
 * instance will be instantiated and sent to the <i>callbackMethod</i> Function.
 *
 * @param {String} callbackToken
 * An arbitrary string token that will be passed to the <i>callbackMethod</i> function when it is called.
 *
 * @return {Object|DblEj.Data.Model|Array} The new object or array of objects created from the encoded string.
 * If the encoded string represented a data model or some other counterpart-compliant class,
 * then the new object will be an instance of the corresponding counterpart-compliant Javascript class.
 * @static
 */
DblEj.Communication.JsonUtil.DecodeJson = function (jsonStringOrObject, callbackMethod, callbackToken)
{
    if (IsDefined(jsonStringOrObject) && jsonStringOrObject !== "")
    {
        var jsonObject;
        if (IsString(jsonStringOrObject))
        {
            jsonObject = JSON.parse(jsonStringOrObject);
        } else {
            jsonObject = jsonStringOrObject;
        }

        if (jsonObject)
        {
            return DblEj.Communication.JsonUtil.RecursivePropertyDecode(jsonObject, callbackMethod, callbackToken);
        } else {
            return {};
        }
    } else {
        return {};
    }
};

/**
 * @property Set_DecoderLibPath
 * The path where the decoder will look for classes to be used when creating instances of
 * counterpart-compliant classes from decoded JSON strings.
 *
 * @type {String}
 *
 * @param {String} decoderLibPath
 * 
 * The path where the decoder will look for classes to be used when creating instances of
 * counterpart-compliant classes from decoded JSON strings.
 *
 * @returns {void}
 * @static
 * The decoder now figures this out using counterpart-provided meta-data and now supports infinite levels of inheritence, as opposed to just one.
 * There are some things still dependant on this functionality, namely the MVC controller dependency manager.
 */
DblEj.Communication.JsonUtil.Set_DecoderLibPath = function (decoderLibPath)
{
    DblEj.Communication.JsonUtil._decodeLibPath = decoderLibPath;
};

/**
 * @property Set_DecoderLibBasePath
 * The path where the decoder will look for base classes to be used
 * when creating instances of counterpart-compliant classes from decoded JSON strings,
 * and the counterpart-compliant class is a sub-class.
 *
 * @type {String}
 *
 * @param {String} decoderLibBasePath
 * @returns {void}
 * @static
 *
 * @deprecated 
 * The decoder now figures this out using counterpart-provided meta-data and now supports infinite levels of inheritence, as opposed to just one.
 * There are some things still dependant on this functionality, namely the MVC controller dependency manager.
 */
DblEj.Communication.JsonUtil.Set_DecoderLibBasePath = function (decoderLibBasePath)
{
    DblEj.Communication.JsonUtil._decodeLibBasePath = decoderLibBasePath;
};

/**
 * @method RecursivePropertyDecode
 * Recusively convert a deserialized object, or array of objects, back into an instance, or instances, of the class that it was when serialized and restore the object's properties.
 * The synchronous use of this method has been left in tact for legacy use.
 * However it is recommended that you use this method asynchronously and use the <i>callbackFunction</i> for reliable decoding.
 *
 * @param {Object} jsonObject
 * The object to decode.
 *
 * @param {Function} callbackFunction
 * This function will be called when the object is done being decoded.
 * This is especially useful when instantiating a model that is forced to be asynchrnous while we wait for dependent js files to download.
 *
 * @param {String} callbackToken A token to be sent back to the callback.
 * 
 * @returns {Object|Array|DblEj.Data.Model} The decoded object when it is available synchronously (because the js file had already been included).  Returns false if there is a delay forcing it to be asynchronous (usually because the class's .js file hasn't been loaded yet, so we can't instantiate it yet.)
 */
DblEj.Communication.JsonUtil.RecursivePropertyDecode = function (jsonObject, callbackFunction, callbackToken)
{
    var returnObject;
    var jsonProp;
    if (IsDefined(jsonObject["ClientObjectName"]))
    {
        var filePath = jsonObject.ClientObjectName.split('.');
        filePath.shift(); //remove the application name from the front, since it is not part of the file path
        filePath = "/" + filePath.join("/") + ".js?rev="+jsonObject.ClientObjectVersion;
        var parentFilePaths = [];
        var ancestoryChain = jsonObject.ClientObjectAncestoryChain;
        for (var ancestoryChainIdx = 0; ancestoryChainIdx < ancestoryChain.length; ancestoryChainIdx++)
        {
            var parentFilePath = ancestoryChain[ancestoryChainIdx].split("\\");
            parentFilePath.shift(); //remove the application name from the front, since it is not part of the file path
            parentFilePath = "/" + parentFilePath.join("/") + ".js?rev="+jsonObject.ClientObjectVersion;
            parentFilePaths[ancestoryChainIdx] = parentFilePath; //the base class's info
        }


        //convert an array of the server-side representation of an object into a typed client-side object
        jsonObject.ConvertFromServerObject =
            function (onConvertComplete, callbackToken)
            {
                var clientObjectParts = this.ClientObjectName.toString().split(".");
                var returnClass = window;
                for (var clientObjectPartIdx = 0; clientObjectPartIdx < clientObjectParts.length; clientObjectPartIdx++)
                {
                    returnClass = returnClass[clientObjectParts[clientObjectPartIdx]];
                }
                returnObject = new returnClass();
                var totalPropsToDecode = 0;
                for (jsonProp in this)
                {
                    totalPropsToDecode++;
                }
                for (jsonProp in this)
                {
                    if (IsDefined(returnObject[jsonProp]))
                    {
                        if (this[jsonProp] !== null && (typeof (this[jsonProp]) === "object"))
                        {
                            //note that we dont even attempt to wait for RecursivePropertyDecode in the event one of the properties has a counterpart,
                            //the consequence being that if you have models down inside the properties of another model, this wont wait for the js for the models to load
                            //before returning.  Use the onConvertComplete attribute to set a callback to invoke when the complete object with counterparts rendered.
                            returnObject[jsonProp] = DblEj.Communication.JsonUtil.RecursivePropertyDecode(this[jsonProp], function(innerReturnObject, innerCallbackToken)
                                {
                                    returnObject[jsonProp] = innerReturnObject;
                                    totalPropsToDecode--;

                                    if (totalPropsToDecode == 0 && IsDefined(onConvertComplete) && onConvertComplete !== null)
                                    {
                                        onConvertComplete(returnObject, innerCallbackToken);
                                    }
                                }, callbackToken);
                        } else {
                            returnObject[jsonProp] = this[jsonProp];
                            totalPropsToDecode--;
                        }
                    } else {
                        totalPropsToDecode--;
                    }
                }
                if (totalPropsToDecode == 0 && IsDefined(onConvertComplete) && onConvertComplete !== null)
                {
                    onConvertComplete(returnObject, callbackToken);
                }
                return returnObject;
            };
        jsonObject.RecursiveInclude = function (parentFilePaths, onAllIncluded, allincludedCallbackToken)
        {
            var parentFilePath = parentFilePaths.shift();
            DblEj.SiteStructure.SitePage.IncludeJs(parentFilePath,
                function (includedFilename)
                {
                    if (parentFilePaths.length == 0)
                    {
                        onAllIncluded();
                    } else {
                        this.RecursiveInclude(parentFilePaths, onAllIncluded, allincludedCallbackToken);
                    }
                }.Bind(this));
        };
        //before we can convert a server-side model into a client-side one
        //we need to be sure we've included any client-side base classes
        if (DblEj.SiteStructure.SitePage.GetFileIncluded(filePath))
        {
            return jsonObject.ConvertFromServerObject(callbackFunction, callbackToken);
        } else {

            if (parentFilePaths.length > 0)
            {
                jsonObject.RecursiveInclude(parentFilePaths,
                    function ()
                    {
                        DblEj.SiteStructure.SitePage.IncludeJs(filePath,
                            function (includedFilename)
                            {
                                this.ConvertFromServerObject(callbackFunction, callbackToken);
                            }.Bind(this));
                    }.Bind(jsonObject));
            } else {
                DblEj.SiteStructure.SitePage.IncludeJs(filePath,
                    function (includedFilename)
                    {
                        var objectConvertedCallback = function(convertedObject, callbackToken)
                        {
                            for (var objectCallbackIdx = 0; objectCallbackIdx < this._callbackFunctions.length; objectCallbackIdx++)
                            {
                                this._callbackFunctions[objectCallbackIdx](convertedObject, callbackToken);
                            }
                        }.Bind(this);
                        this.ConvertFromServerObject(objectConvertedCallback, this._callbackToken);
                    }.Bind(jsonObject));
            }

            DblEj.Logging.Tracing.Trace("DblEj was not able to return the \"" + jsonObject.ClientObjectName +
                "\" Model instance immediately because the corresponding files had not been loaded yet (" + filePath +
                ", " + parentFilePath +
                ").  I am loading the models now, and will return the appropriate object in the callbackMethod if you specified one.  This method will return a raw json object.  In a moment this object will be ready to load immediately.  To avoid this, you can preload the client-side model for this page or in Includes.syrp", DblEj.Logging.Tracing.LOGLEVEL_INFO);
            return jsonObject;
        }
    } else {
        var callbacksWaiting = 0;
        returnObject = [];
        var typedReturnObjects = [];
        if (IsArray(jsonObject))
        {
            callbacksWaiting = jsonObject.length;
            if (jsonObject.length > 0)
            {
                for (var jsonPropIdx = 0; jsonPropIdx < jsonObject.length; jsonPropIdx++)
                {
                    jsonProp = jsonObject[jsonPropIdx];
                    if (jsonProp !== null && typeof (jsonProp) === "object")
                    {
                        typedReturnObjects[typedReturnObjects.length] = "ASYNC_WAIT";
                        returnObject[jsonPropIdx] = DblEj.Communication.JsonUtil.RecursivePropertyDecode(jsonProp,
                            function (returnObject, callbackToken)
                            {
                                typedReturnObjects[this] = returnObject;
                                callbacksWaiting--;
                                if (callbacksWaiting == 0)
                                {
                                    if (IsDefined(callbackFunction) && callbackFunction !== null)
                                    {
                                        callbackFunction(typedReturnObjects, callbackToken);
                                    }
                                }
                            }.Bind(jsonPropIdx), callbackToken);
                    } else {
                        returnObject[jsonPropIdx] = jsonProp;
                        typedReturnObjects[typedReturnObjects.length] = jsonProp;
                        callbacksWaiting--;
                        if (callbacksWaiting == 0)
                        {
                            if (IsDefined(callbackFunction) && callbackFunction !== null)
                            {
                                callbackFunction(typedReturnObjects, callbackToken);
                            }
                        }
                    }
                }
            } else {
                if (IsDefined(callbackFunction) && callbackFunction !== null)
                {
                    callbackFunction(typedReturnObjects, callbackToken);
                }
            }
        } else if (IsObject(jsonObject)) {
            for (var propName in jsonObject)
            {
                callbacksWaiting++;
            }
            if (callbacksWaiting > 0)
            {
                returnObject = {};
                var typedReturnObject = {};

                for (var propName in jsonObject)
                {
                    jsonProp = jsonObject[propName];
                    if (jsonProp !== null && typeof (jsonProp) === "object")
                    {
                        returnObject[propName] = DblEj.Communication.JsonUtil.RecursivePropertyDecode(jsonProp,
                            function (decodedProperty, callbackToken)
                            {
                                typedReturnObject[this] = decodedProperty;
                                callbacksWaiting--;

                                if (callbacksWaiting == 0)
                                {
                                    if (IsDefined(callbackFunction) && callbackFunction !== null)
                                    {
                                        callbackFunction(typedReturnObject, callbackToken);
                                    }
                                }
                            }.Bind(propName), callbackToken);
                    } else {
                        returnObject[propName] = jsonProp;
                        typedReturnObject[propName] = jsonProp;
                        callbacksWaiting--;
                        if (callbacksWaiting == 0)
                        {
                            if (IsDefined(callbackFunction) && callbackFunction !== null)
                            {
                                callbackFunction(typedReturnObject, callbackToken);
                            }
                        }
                    }
                }
            } else {
                if (IsDefined(callbackFunction) && callbackFunction !== null)
                {
                    callbackFunction(returnObject, callbackToken);
                }
            }

//note about above code and this comment:
//we used to only do this for numeric arrays.
//and for all other objects we would just pass object as is (commented code below)
//now we try to reolve the correct object tyupes and return them (used code above)
//            returnObject = jsonObject;
//            if (IsDefined(callbackFunction) && callbackFunction !== null)
//            {
//                callbackFunction(jsonObject, callbackToken);
//            }
        }
        else
        {
            returnObject = jsonObject;
            if (IsDefined(callbackFunction) && callbackFunction !== null)
            {
                callbackFunction(returnObject, callbackToken);
            }
        }

        return returnObject;
    }
};