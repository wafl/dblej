<?php
namespace DblEj\Communication;

use DblEj\Application\IApplication;

/**
 * Represents a route to a destination.  The type and value of the destination is not constrained,
 * other than that in order for it to ever be reached, it (the destination) must be reachable.
 * The destination doesn't necessarily need to be reachable now, but it needs to be
 * reachable at some point in it's lifetime, or it is not a valid destination.
 * A stated destination that is never reachable is therefore said to be a
 * two-faced non-destination.  The only sure way to prove that a destination is reachable
 * is that it is reached at some point in it's lifetime.  Otherwise you're just a
 * two-faced non-destination sensation.
 * However, a destination can only be reached if it's reachable!
 */
class Route
implements IRoute, ITravelDestination
{
    protected $_destination;
    protected $_destinationArgs;
    protected $_prepareDestination;

    /**
     * Create an instance of the Route.
     *
     * @param mixed $destination
     * The destination.
     * This must be a <i>callable</i> function (for the default implementation).
     *
     * @param mixed $destinationArgs
     * The arguments that will be delivered to the destination.
     */
    public function __construct($destination, $destinationArgs = null, $prepareDestination = null)
    {
        $this->_destination     = $destination;
        $this->_destinationArgs = $destinationArgs;
        $this->_prepareDestination = $prepareDestination;
    }

    /**
     * The arguments to be delivered to the destination.
     *
     * @return mixed
     */
    public function Get_DestinationArguments()
    {
        return $this->_destinationArgs;
    }

    /**
     * This route's destination.
     *
     * @return type
     */
    public function Get_Destination()
    {
        return $this->_destination;
    }

    public function Get_PrepareDestination()
    {
        return $this->_prepeareDestination;
    }

    /**
     * Allow routes to do preliminary work and return preliminary information before actually going to the route destination
     *
     * @return Response The response returned from the destination.
     */
    public function PrepareRoute(IApplication $app = null)
    {
        $response = null;
        if ($this->_prepareDestination && is_callable($this->_prepareDestination))
        {
            $passArgs = $this->_destinationArgs;
            $response = call_user_func_array($this->_prepareDestination, $passArgs);
        }
        elseif ($this->_prepareDestination)
        {
            throw new CommunicationException("Prepare route destinations must be callable.  You passed: " . var_export($this->_destination, true));
        }
        return $response;
    }

    /**
     * Deliver the <i>Arguments</i> to the route.
     *
     * @param \DblEj\Application\IApplication $app
     * The application, if any, that received the routing request that triggered this operation.
     *
     * @return Response The response returned from the destination.
     *
     * @throws CommunicationException
     * Thrown when the destination is nto callable.
     */
    public function GotoRoute(IApplication $app = null)
    {
        $response = null;
        if (is_callable($this->_destination))
        {
            $passArgs = $this->_destinationArgs;
            $response = call_user_func_array($this->_destination, $passArgs);
        }
        else
        {
            throw new CommunicationException("Route destinations must be callable.  You passed: " . var_export($this->_destination, true));
        }
        return $response;
    }

    public function Get_TravelerRoles()
    {
        if (is_array($this->_destination) && is_a($this->_destination[0], "\\DblEj\\Communication\\ITravelDestination"))
        {
            $destClass = $this->_destination[0]->Get_TravelerRoles();
        } else {
            $destClass = ["\\DblEj\\Authentication\\IUser", "\\DblEj\\Authentication\\IUserGroup"];
        }
        return $destClass;
    }
    public static function Get_ClassTravelerRoles()
    {
        return ["\\DblEj\\Authentication\\IUser", "\\DblEj\\Authentication\\IUserGroup"];
    }

    function __toString()
    {
        $destinationDesc = var_export($this->_destination, true);
        return "Route w/ destination: " . $destinationDesc;
    }
}