<?php

namespace DblEj\Communication;

use DblEj\Application\IApplication;

/**
 * A RequestHandler's job is to receive a request and return an appropriate response.
 */
interface IRequestHandler
extends ITravelDestination
{
    /**
     * Handle an incoming request.
     *
     * @param String $requestString
     * A name or token identifying the request or type of request.
     *
     * @param \DblEj\Communication\IRequest $request
     * The contents of the request.
     *
     * @param \DblEj\Application\IApplication $app
     * The application that received the <i>request</i>.
     */
    public function HandleRequest($requestString, IRequest $request, IApplication $app);
}