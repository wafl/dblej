<?php

namespace DblEj\IO;

/**
 * Raised when data is received by a DataReceiver.
 */
class DataReceivedEvent
extends \DblEj\EventHandling\EventInfo
{
    const EVENT_DATARECEIVED = 1400;

    private $_data;
    private $_sender;

    public function __construct($data, \DblEj\IO\IInput $recipient, \DblEj\IO\IOutput $sender)
    {
        parent::__construct(self::EVENT_DATARECEIVED, $recipient);
        $this->_data   = $data;
        $this->_sender = $sender;
    }

    public function Get_Data()
    {
        return $this->_data;
    }

    public function Get_Sender()
    {
        return $this->_sender;
    }
}