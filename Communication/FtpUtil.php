<?php

namespace DblEj\Communication;

/**
 * FtpUtil is an FTP client utility class.
 */
class FtpUtil
{

    /**
     * Send an FTP request to a web server and stream the response to disk
     * @param string $serverUrl URL to the ftp server
     * @param string $remoteFilePath path (relative to your ftp root) of the remote file you want to download
     * @param string $localDestinationPath Where you want to save the downloaded file on the local system
     * @param string $username ftp account user name
     * @param string $password ftp account passworf
     * @param int $ftpMode use one of these constants: FTP_BINARY or FTP_ASCII
     * @param integer $serverPort The TCP Port that the FTP Server is listening on
     * @return string
     */
    public static function DownloadFile($serverUrl, $remoteFilePath, $localDestinationPath, $username = "", $password = "", $ftpMode = FTP_BINARY, $serverPort = 21, $isPassive = false)
    {
        $ftp = ftp_connect($serverUrl, $serverPort);
        if ($ftp)
        {
            if (ftp_login($ftp, $username, $password))
            {
                $outputStream = fopen($localDestinationPath, "w");
                if ($isPassive)
                {
                    ftp_pasv($ftp, true);
                }
                if (!ftp_fget($ftp, $outputStream, $remoteFilePath, $ftpMode))
                {
                    ftp_close($ftp);
                    fclose($outputStream);
                    throw new CommunicationException("Ftp was able to connect and login but there was an error getting the file");
                }
                else
                {
                    ftp_close($ftp);
                    fclose($outputStream);
                }
            }
            else
            {
                ftp_close($ftp);
                throw new CommunicationException("FTP Login failed");
            }
        }
        else
        {
            throw new CommunicationException("Could not connect to ftp server");
        }
    }

    /**
     * Get a list of files from the specified folder
     * @param string $serverUrl
     * @param string $remoteFolderPath
     * @param string $username
     * @param string $password
     * @param int $ftpMode use one of these constants: FTP_BINARY or FTP_ASCII
     * @param int $serverPort
     * @param boolean $isPassive
     * @return array
     * @throws CommunicationException
     */
    public static function GetFileList($serverUrl, $remoteFolderPath, $username = "", $password = "", $ftpMode = FTP_BINARY, $serverPort = 21, $isPassive = false)
    {
        $fileList = array();
        $ftp      = ftp_connect($serverUrl, $serverPort);
        if ($ftp)
        {
            if (ftp_login($ftp, $username, $password))
            {
                try
                {
                    if ($isPassive)
                    {
                        ftp_pasv($ftp, true);
                    }
                    $fileList = ftp_nlist($ftp, $remoteFolderPath);
                }
                catch (\Exception $e)
                {
                    throw new CommunicationException("Ftp was able to connect and login but there was an error getting the file list: " . $e->getMessage(), E_ERROR, $e);
                }
                ftp_close($ftp);
            }
            else
            {
                ftp_close($ftp);
                throw new CommunicationException("FTP Login failed");
            }
        }
        else
        {
            throw new CommunicationException("Could not connect to ftp server");
        }
        return $fileList;
    }
}