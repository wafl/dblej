<?php

namespace DblEj\Communication;

/**
 * A utility class for encoding/decoding objects to/from JSON.
 *
 * The methods in this class can be used to serialize objects that have private members
 * exposed via public property accessors.
 *
 * Instances of counterpart-compliant classes are
 * encoded with meta-data that allow them to be decoded
 * as an instance of the correct class.
 *
 * Likewise, JSON strings with encoded counterpart meta-data will be
 * decoded as an instance of the correct counterpart-compliant class.
 *
 * This allows for cross-platform transmission of serialized data where
 * strong-typed data's types are preserved.
 *
 * For example:
 * Let's say there is a PHP class called "Employee" that
 * has properties that describe an employee and methods
 * that act on an employee record.
 *
 * Let's say there is also a Javascript class called "Employee"
 * that has the same properties, and some methods of it's own that
 * perform client-side actions on the employee record.
 *
 * Normally, if you serialize an instance of the PHP Employee class and send it to
 * Javascript (via an Ajax response, for example), it will be deserialized
 * on the client-side as a generic Javascript Object.
 * It's properties will be whatever public variables that were exposed by
 * the PHP object (which usually is none, in DblEj applications).  It will not
 * have any of the methods that the Employee class has.
 *
 * With DblEj, if you serialize an instance of the PHP Employee counterpart-compliant
 * class and send it to Javascript (via an Ajax response, for example),
 * it will be deserialized on the client-side as an instance of the
 * Javascript Employee class.
 * It's properties will be whatever properties the javascript Employee class has defined,
 * and where those properties match public properties that were exposed by
 * the PHP object via accessors (which usually all of them, in DblEj applications),
 * their values will be the same as well.
 * Because it is an actual Employee object, and not a generic Object,
 * it will have all of it's normal methods.
 *
 * Likewise, an instance of a counterpart-compliant Javascript Employee class
 * on the client-side that is encoded using the methods in this class
 * can be decoded on the server-side as an instance of the PHP Employee class.
 *
 * @static
 */
class JsonUtil
{

    /**
     * Encode an object, array, or string into a JSON string.
     *
     * This method is a wrapper for the native json_encode function.
     * If the object being encoded implements <i>IJsonInterfacable</i>,
     * as all counterpart-compliant PHP classes do,
     * then the result of <i>ToSerializable</i> will be encoded instead of the actual object.
     * This allows classes to expose certain private members, which are normally exposed by
     * property accessors, so that they'll be serialized.
     *
     * This method will also include pertient counterpart information in the encoded object
     * so that it can be decoded to the correct type.
     *
     * @param mixed $object The object to be encoded into a JSON string.
     *
     * @param boolean $forceUtf This value will be passed to the object as an instruction when the object is expsosing it's values.  It is up to the object to honor the instruction.
     *
     * @param boolean $reindexObjectArrays If this is true, then any associative arrays will be converted into sequentially indexed arrays
     *
     * @return string The JSON encoded string representation of the </i>$object</i>.
     */
    public static function EncodeJson($object, $forceUtf = true, $reindexObjectArrays = false)
    {
        $fieldValues = self::_getFieldValuesRecursive($object, $forceUtf, $reindexObjectArrays);
        $encodedJson = json_encode($fieldValues);
        return $encodedJson;
    }

    /**
     * Decode a JSON string back into an object or array.
     *
     * If any part of the JSON string represents a counterpart-compliant class instance
     * and was created by JsonUtil\EncodeJson,
     * or any other counterpart-compliant encoder,
     * then the decoded object or it's parts/elements
     * will be of the same type as it was when encoded.
     * Otherwise, it will be a generic array.
     *
     * @param string $jsonString
     * The JSON-encoded string to decode.

     * @param \DblEj\Data\Integration\IDatabaseServer $saveableObjectStorageEngine
     * @deprecated since version 1855
     * IGNORED
     * If the decoded object is an instance of a DblEj Data Model,
     * it can be assigned an underlying storage engine for any Active Record
     * operations that you may want to perform on the decoded object.
     *
     * @return mixed
     * The decoded JSON as a counterpart-compliant class instance, an Object or an Array.
     */
    public static function DecodeJson($jsonString, \DblEj\Data\Integration\IDatabaseServer $saveableObjectStorageEngine = null, $convertNullStringToNull = false)
    {

        //json_encode is only supposed to accept utf-8.
        //I have found that is also accepts straight ascii.
        //but strings that look like unicode (have special chars) but are not utf8, will be rejected by json_decode
        //So we try to detect that and encode utf8 where needed
        if (!\DblEj\Util\Strings::IsUtf8($jsonString) && !\DblEj\Util\Strings::IsLatin($jsonString))
        {
            $jsonString = utf8_encode($jsonString);
        }
        $jsonObject = json_decode($jsonString, true);
        if (!$jsonObject) //maybe its url encoded
        {
            switch (json_last_error())
            {
                case JSON_ERROR_DEPTH:
                    throw new \Exception("Cannot decode json: Maximum stack depth exceeded");
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    throw new \Exception("Cannot decode json: Unexpected control character found");
                    break;
                case JSON_ERROR_SYNTAX:
                    throw new \Exception("Cannot decode json: Syntax error, $jsonString");
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    throw new \Exception("Cannot decode json: Invalid or malformed JSON");
                    break;
            }
        }
        if (isset($jsonObject["ServerObjectName"]))
        {
            $jsonObject = \DblEj\Data\PersistableModel::CreateInstanceFromJsonArray($jsonObject);
        }
        elseif ($convertNullStringToNull)
        {
            array_walk_recursive($jsonObject, function(&$dataVal, $dataKey)
            {
                if ($dataVal === "null")
                {
                    $dataVal = null;
                }
            });
        }
        return $jsonObject;
    }

    private static function _getFieldValuesRecursive($object, $forceUtf8 = false, $reindexObjectArrays = false)
    {
        if (is_array($object))
        {
            if ($reindexObjectArrays)
            {
                $object = array_values($object);
            }
            $fieldValues = array();
            foreach ($object as $idx => $elem)
            {
                $fieldValues[$idx] = self::_getFieldValuesRecursive($elem, $forceUtf8, $reindexObjectArrays);
            }
        }
        elseif (is_object($object) && is_a($object, "\DblEj\Communication\IJsonInterfacable"))
        {
            $fieldValues = $object->Get_SerializableFieldValues($forceUtf8, $reindexObjectArrays);
        }
        elseif (is_object($object))
        {
            $methods     = get_class_methods($object);
            $fieldValues = array();
            foreach ($methods as $method)
            {
                if (substr($method, 0, 4) == "Get_")
                {
                    $methodName               = substr($method, 4);
                    $fieldValues[$methodName] = self::_getFieldValuesRecursive($object->$method(), $forceUtf8, $reindexObjectArrays);
                }
            }
        }
        else
        {
            if ($forceUtf8 && is_string($object))
            {
                if (mb_check_encoding($object, "UTF-8") !== true)
                {
                    $fieldValues = utf8_encode($object);
                }
                else
                {
                    $fieldValues = $object;
                }
            }
            else
            {
                $fieldValues = $object;
            }
        }
        return $fieldValues;
    }
}