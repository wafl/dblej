<?php
namespace DblEj\Communication;

interface IService
{
    /**
     * Get string representing the last error that occurred
     *
     * @return string the last error that occurred
     */
    public function GetLastError();
}