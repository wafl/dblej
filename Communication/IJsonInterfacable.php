<?php

namespace DblEj\Communication;

/**
 * Exposes methods called by the JSON serializer when serializing an object.
 *
 * Makes instances of a class serializable by the DblEj JSON serializer (which is used for Ajax/Api communications).
 */
interface IJsonInterfacable
{

    /**
     * Create a instance of the implementing class, initializing it with an array
     * that was created by deserializing a JSON object, or an array that follows
     * the same structure, as if it had been created by calling ToJson() on an instance
     * of the implementing class.
     *
     * The array is associative, where element indexes correspond to the
     * implementing class's property names.
     *
     * @return IJsonInterfacable
     */
    public static function CreateInstanceFromJsonArray(array $jsonArray);

    /**
     * The client-side counterpart class name.
     * @return string
     */
    public function Get_ClientObjectName();

    public function Get_ClientObjectVersion();

    /**
     * An array of strings represeting the names of all the properties in the implementing class that are serializable.
     *
     * @return array
     */
    public static function Get_SerializableFieldNames();

    /**
     * An associative array of all the properties in the implementing class,
     * where the element index corresponds to the property's name,
     * and it's value corresponds to the property's value.
     *
     * @return array
     */
    public function Get_SerializableFieldValues($forceUtf8Strings = false, $reindexObjectArrays = false);

    /**
     * Setting this property will change the the property values in
     * an instance of the implementing class to the <i>$fieldValues</i>,
     * where keys in the array corresponds to a property name.
     * @return void
     */
    public function Set_SerializableFieldValues(array $fieldValues);

    /**
     * Serialize an instance of the implementing class to a JSON encoded string.
     *
     * @param boolean $reindexObjectArrays
     * For a PHP array to be serialized into a JSON Array, it's indexes must be zero-based and sequential.
     * If this requirement is not met, then instead of an Array, the JSON representation of the
     * PHP array will be an Object, where each property corresponds to the indexes in the PHP array.
     *
     * Setting <i>reindexObjectArrays</i> to <i>true</i> will instruct the serializer
     * to reindex any arrays in the object to be zero-based and sequential so that they
     * will be serialized to JSON as Arrays.
     *
     * Setting <i>reindexObjectArrays</i> to <i>false</i> will instruct the serializer
     * to leave the arrays as they are.  Use this option if the implementing class has
     * properties who's values may contain associative arrays in which it is important to
     * include the array element's keys during serialization.  Instead of a JSON Array, the
     * serialized string will define JSON Objects, where each property corresponds to a key
     * in the PHP array.
     */
    public function ToJson($reindexObjectArrays = true);
}
?>