/*
 *@namespace DblEj.Communication.Ajax
 */
Namespace("DblEj.Communication.Ajax");

/*An Ajax connection.
 * 
 *@class Connection
 *@extends Class
 */
DblEj.Communication.Ajax.Connection = Class.extend(
    {
        /**
         * @constructor
         *
         * @param {String} serverUrl
         * The Url of the server-side handler for this connection's requests.
         **/
        init: function (serverUrl)
        {
            this._serverUrl = serverUrl;
            this._httpClient = null;
        },
        /**
         * @method SendDataSync
         * Make a synchronous request to a DblEj Ajax/API handler on the server-side.
         * 
         * @example
         * var ajax = new AjaxHttpConnection('http://dblej.com/AjaxHandler.php');
         * var response = ajax.SendDataSync();
         * alert(response);
         *
         * @param {String} getVariables
         * The data to send, formatted in the same way as a url query string
         * 
         * @param {String} method
         * The HTTP request method.  Must be "post" or "get".
         *
         * @param {Boolean} useDefaultJsonParser
         * Normally an internal parser wrapper is used to encode objects to deal with things like nesting,
         * recursion, and tagging instances of classes that have client/server-side counterparts.
         * Setting this to true will use the default parser instead.
         *
         * @param {Array} headers
         * An array of HTTPHeader objects, one for each HTTP header to be sent along with the request.
         * 
         * @returns {Array|Object}
         * The deserialized response from the server,
         * either as an instance (or an array of instances) of the client-side counterpart
         * for the server's response object, or if none exists, as a generic Json object.
         */
        SendDataSync: function (getVariables, method, useDefaultJsonParser, headers)
        {
            if (!IsDefined(useDefaultJsonParser))
            {
                useDefaultJsonParser = false;
            }
            if (typeof (getVariables) == "undefined" || getVariables == null)
            {
                getVariables = "";
            }
            var http;
            http = DblEj.Communication.Ajax.Utils.GetHttpClientObject();
            if (typeof method == undefined || !method)
            {
                method = "post";
            }
            if (method == "post")
            {
                http.open(method, this._serverUrl, false);
                if (IsDefined(headers))
                {
                    for (var hdrIdx = 0; hdrIdx < headers.length; hdrIdx++)
                    {
                        http.setRequestHeader(headers[hdrIdx].Get_HeaderName(), headers[hdrIdx].Get_HeaderValue());
                    }
                }
                http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
                http.send(getVariables);
            } else {
                if (getVariables != null && getVariables != "")
                {
                    http.open(method, this._serverUrl + "?" + getVariables, false);
                } else {
                    http.open(method, this._serverUrl, false);
                }
                if (IsDefined(headers))
                {
                    for (var hdrIdx = 0; hdrIdx < headers.length; hdrIdx++)
                    {
                        http.setRequestHeader(headers[hdrIdx].Get_HeaderName(), headers[hdrIdx].Get_HeaderValue());
                    }
                }
                http.send(null);
            }
            if (useDefaultJsonParser)
            {
                return JSON.parse(http.responseText);
            } else {
                return DblEj.Communication.JsonUtil.DecodeJson(http.responseText);
            }
        },
        /**
         * @method SendDataAsync
         * Make an asynchronous to a DblEj Ajax/API handler on the server-side.
         * 
         * @example
         * var ajax = new AjaxHttpConnection('http://dblej.com/AjaxHandler.php');
         * ajax.SendDataAsync("test1",AsyncResponse_handler);
         * ajax.SendDataAsync("test2",AsyncResponse_handler);
         * ajax = new AjaxHttpConnection('http://dblej.com/AjaxHandler2.php');
         * ajax.SendDataAsync("test3",MyNamespace.AsyncResponse_handler);
         *
         * @param {String} sendToken
         * Any string you wish to pass in.
         * This can be used to identify the response when it arrives.
         * @default ""
         *
         * @param {Function} callbackMethod
         * The function to execute when the response arrives from the server.
         * The callback function's signature should be
         *     <code>function(responseObject,responseToken){... your code ...}</code>
         * @default null
         * 
         * @param {String} getVariables The data to send, formatted as a url query string.
         * @default ""
         *
         * @param {String} method The HTTP request method.  Must be "post" or "get".
         * @default "post"
         *
         * @param {Boolean} useDefaultJsonParser
         * Normally an internal parser wrapper is used to encode objects to deal with things like nesting,
         * recursion, and tagging instances of classes that have client/server-side counterparts.
         * Setting this to true will use the default parser instead.
         * Setting this to 2 will return raw text without parsing
         * @default false
         *
         * @param {Array} headers An array of HTTPHeader objects, one for each HTTP header to be sent along with the request.
         * @default []
         *
         * @returns {Boolean} Returns <em>true</em> if the request is successfully sent, otherwise <em>false</em>.
         */
        SendDataAsync: function (sendToken, callbackMethod, getVariables, method, useDefaultJsonParser, headers)
        {
            if (!IsDefined(useDefaultJsonParser))
            {
                useDefaultJsonParser = false;
            }
            if (IsNullOrUndefined(getVariables))
            {
                getVariables = "";
            }
            this._httpClient = DblEj.Communication.Ajax.Utils.GetHttpClientObject();
            this._httpClient.onreadystatechange = function () {
                if (this._httpClient.readyState === 4) {
                    if (callbackMethod) {
                        if (useDefaultJsonParser == 2)
                        {
                            callbackMethod(this._httpClient.responseText, sendToken, getVariables);
                        }
                        else if (useDefaultJsonParser)
                        {
                            callbackMethod(JSON.parse(this._httpClient.responseText), sendToken, getVariables);
                        } else {
                            DblEj.Communication.JsonUtil.DecodeJson(this._httpClient.responseText, callbackMethod, sendToken);
                        }
                    }
                }
            }.Bind(this);

            if (typeof method == undefined || !method)
            {
                method = "post";
            }
            if (method == "post")
            {
                this._httpClient.open(method, this._serverUrl, true);
                this._httpClient.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
                if (IsDefined(headers))
                {
                    for (var hdrIdx = 0; hdrIdx < headers.length; hdrIdx++)
                    {
                        this._httpClient.setRequestHeader(headers[hdrIdx].Get_HeaderName(), headers[hdrIdx].Get_HeaderValue());
                    }
                }
                this._httpClient.send("token=" + sendToken + "&" + getVariables);
            } else {
                if ((sendToken != null && sendToken != "") || (getVariables != null && getVariables != ""))
                {
                    this._httpClient.open(method, this._serverUrl + "?token=" + sendToken + "&" + getVariables, true);
                } else {
                    this._httpClient.open(method, this._serverUrl, true);
                }

                if (IsDefined(headers))
                {
                    for (var hdrIdx = 0; hdrIdx < headers.length; hdrIdx++)
                    {
                        this._httpClient.setRequestHeader(headers[hdrIdx].Get_HeaderName(), headers[hdrIdx].Get_HeaderValue());
                    }
                }
                this._httpClient.send(null);
            }
            return true;
        },
        /**
         * @method UploadFile
         * Upload a file to a DblEj Ajax/API handler on the server-side.
         *
         * @param {Array|File} fileOrFileList
         * A File object or an array of File objects, returned from the File API
         * or from a file input control(s).
         *
         * @param {String} getVariablesString
         * If you want to pass anything in the Url's query string, you can specify it here.
         * @default ""
         *
         * @param {Function} progressCallback
         * A function to be called to report realtime upload progress.
         * The callback function's signature should be
         *     <code>function(progressEvent)</code>
         * @default null
         *
         * @param {Function} completedCallback
         * A function to be called when the file(s) are finished uploading.
         * The callback function's signature should be
         *     <code>function(progressEvent)</code>
         * @default null
         *
         * @param {Array|String} UploadVarNames
         * If <i>fileOrFileList</i> is a single file, then <i>UploadVarNames</i> will be the
         * name of the POST variable to use when uploading the file.
         * If <i>fileOrFileList</i> is an array of files, then <i>UploadVarNames</i> will by an array
         * of POST variable names to use when uploading the files
         * with indexes that correspond to those in <i>fileOrFileList</i>.
         * If you leave this blank, the variable name <i>POST</i> will be used
         * for single files, and <i>POST[n]</i> for file arrays.
         * @default ""
         *
         * @param {Object} postVariables
         * An object where each property represents a POST variable of the same name and value.
         * @default null
         * 
         * @param {Boolean} useDefaultJsonParser
         * Normally an internal parser wrapper is used to encode objects to deal with things like nesting,
         * recursion, and tagging instances of classes that have client/server-side counterparts.
         * Setting this to true will use the default parser instead.
         * @default false
         *
         * @param {Array} headers
         * An array of HTTPHeader objects, one for each HTTP header to be sent along with the request.
         * @default []
         *
         * @returns {Connection}
         * Returns this object instance, useful for making chained method calls.
         */
        UploadFile: function (fileOrFileList, getVariablesString, progressCallback, completedCallback, UploadVarNames, postVariables, useDefaultJsonParser, headers)
        {
            this._httpClient = DblEj.Communication.Ajax.Utils.GetHttpClientObject();

            if (!IsDefined(UploadVarNames))
            {
                if (IsDefined(fileOrFileList.length))
                {
                    UploadVarNames = new Array();
                    for (var fileIdx = 0; fileIdx < fileOrFileList.length; fileIdx++)
                    {
                        UploadVarNames[fileIdx] = "Files[" + fileIdx + "]";
                    }
                } else {
                    UploadVarNames = "File";
                }
            }

            if (IsDefined(this._httpClient.upload))
            {

                if (progressCallback)
                {
                    DblEj.EventHandling.Events.AddHandler(this._httpClient.upload, "progress", progressCallback);
                }
                this._httpClient.onreadystatechange = function (e) {
                    if (this.readyState === 4) {
                        if (completedCallback) {
                            var responseText = this.responseText;
                            if (responseText)
                            {
                                if (IsDefined(useDefaultJsonParser) && useDefaultJsonParser)
                                {
                                    completedCallback(e, this.status, JSON.parse(responseText), getVariablesString);
                                } else {
                                    completedCallback(e, this.status, DblEj.Communication.JsonUtil.DecodeJson(responseText), getVariablesString);
                                }
                            } else {
                                if (this.status == 200)
                                {
                                    completedCallback(e, this.status, {
                                        "ErrorMessage": "Upload succeeded, but the server sent a blank response."
                                    },
                                    getVariablesString);
                                } else {
                                    completedCallback(e, this.status, {
                                        "ErrorMessage": "There was an error uploading the file, but the server did not return an error message."
                                    },
                                    getVariablesString);
                                }
                            }
                        }
                    }
                };

                if (getVariablesString != "" && getVariablesString != null)
                {
                    this._httpClient.open("post", this._serverUrl + "?" + getVariablesString, true);
                } else {
                    this._httpClient.open("post", this._serverUrl, true);
                }
                if (IsDefined(headers) && headers != null)
                {
                    for (var hdrIdx = 0; hdrIdx < headers.length; hdrIdx++)
                    {
                        this._httpClient.setRequestHeader(headers[hdrIdx].Get_HeaderName(), headers[hdrIdx].Get_HeaderValue());
                    }
                }
                var formData = new FormData();
                if (IsDefined(fileOrFileList.length))
                {
                    for (var fileIdx = 0; fileIdx < fileOrFileList.length; fileIdx++)
                    {
                        formData.append(UploadVarNames[fileIdx], fileOrFileList[fileIdx]);
                    }
                } else {
                    formData.append(UploadVarNames, fileOrFileList);
                }
                if (IsDefined(postVariables))
                {
                    for (var postVarname in postVariables)
                    {
                        formData.append(postVarname, postVariables[postVarname]);
                    }
                }
                this._httpClient.send(formData);
            } else {
                throw "This browser does not support asynchronous file upload";
            }
            return this;
        },
        /**
         * @method SubmitForm
         * Submit a form to a DblEj Ajax/API handler on the server-side.
         *
         * @param {HTMLFormElement} form
         * The form that is being submitted.
         *
         * @param {String} getVariablesString
         * If you want to pass anything in the Url's query string, you can specify it here.
         * @default ""
         *
         * @param {Function} callbackMethod
         * A function to be called when the file(s) are finished uploading.
         * The callback function's signature should be
         *     <code>function(progressEvent)</code>
         * @default null
         *
         * @param {Boolean} useDefaultJsonParser
         * Normally an internal parser wrapper is used to encode objects to deal with things like nesting,
         * recursion, and tagging instances of classes that have client/server-side counterparts.
         * Setting this to <i>true</i> will use the default parser instead.
         * @default false
         *
         * @param {Array} headers
         * An array of HTTPHeader objects, one for each HTTP header to be sent along with the request.
         * @default []
         *
         * @param {String} sendToken
         * Any string you wish to pass in.
         * This can be used to identify the response when it arrives.
         * @default ""
         * 
         * @returns {Boolean}
         * Returns True on success.
         */
        SubmitForm: function (form, getVariablesString, callbackMethod, useDefaultJsonParser, headers, sendToken)
        {
            if (!IsDefined(useDefaultJsonParser))
            {
                useDefaultJsonParser = false;
            }
            var formData = new FormData(form);
            this._httpClient = DblEj.Communication.Ajax.Utils.GetHttpClientObject();
            this._httpClient.onreadystatechange = function () {
                if (this._httpClient.readyState === 4) {
                    if (callbackMethod) {
                        if (useDefaultJsonParser)
                        {
                            callbackMethod(JSON.parse(this._httpClient.responseText), sendToken);
                        } else {
                            DblEj.Communication.JsonUtil.DecodeJson(this._httpClient.responseText, callbackMethod, sendToken);
                        }
                    }
                }
            }.Bind(this);

            if (getVariablesString != null && getVariablesString != "")
            {
                this._httpClient.open("post", this._serverUrl + "?" + getVariablesString, true);
            } else {
                this._httpClient.open("post", this._serverUrl, true);
            }
            if (IsDefined(headers))
            {
                for (var hdrIdx = 0; hdrIdx < headers.length; hdrIdx++)
                {
                    this._httpClient.setRequestHeader(headers[hdrIdx].Get_HeaderName(), headers[hdrIdx].Get_HeaderValue());
                }
            }
            this._httpClient.send(formData);
            return true;
        }

    });