<?php
namespace DblEj\Communication\Ajax;

/**
 * Encapsulates a string of HTML to be sent to the client in response to an Ajax/API request.
 */
class AjaxResultHtml
implements \DblEj\Communication\IJsonInterfacable
{
    private $_resultHtml;

    /**
     * Instantiate the result.
     *
     * @param string $resultHtml The HTML to return to the client.
     */
    public function __construct($resultHtml)
    {
        $this->_resultHtml = $resultHtml;
    }

    /**
     * The HTML to be sent to the client.
     *
     * @param string $resultHtml
     */
    public function Set_ResultHtml($resultHtml)
    {
        $this->_resultHtml = $resultHtml;
    }

    /**
     * The HTML to be sent to the client.
     *
     * @return string
     */
    public function Get_ResultHtml()
    {
        return $this->_resultHtml;
    }

    /**
     * The name of the Client-Side Counterpart to this class.
     *
     * @return string
     */
    public function Get_ClientObjectName()
    {
        return "DblEjAjaxResultHtml";
    }

    /**
     * All the properties in this class that are serializable.
     *
     * @return array
     */
    public static function Get_SerializableFieldNames()
    {
        return array(
            "ResultHtml");
    }

    /**
     * All serializable properties and their values.
     *
     * @return array
     */
    public function Get_SerializableFieldValues($forceUtf8Strings = false, $reindexObjectArrays = false)
    {
        return array(
            "ResultHtml" => $this->_resultHtml);
    }

    /**
     * Set the this object's properties using the values in the passed array.
     *
     * @param array $fieldValues
     * An associative array where each element is keyed by the property name.
     */
    final public function Set_SerializableFieldValues(array $fieldValues)
    {
        $this->_resultHtml = $fieldValues["ResultHtml"];
    }

    /**
     * Create an AjaxResult instance from a deserialized json instance
     * @param array $jsonArray
     * @return \DblEj\Communication\Ajax\AjaxResult
     */
    public static function CreateInstanceFromJsonArray(array $jsonArray)
    {
        $returnObj = new AjaxResultHtml($jsonArray["ResultHtml"]);
        return $returnObj;
    }

    /**
     * Serialize the result to JSON, adding special tags to allow
     * counterpart class instantiation on the client-side.
     *
     * @param boolean $reindexObjectArrays
     * This flag has no effect on this class, because this class
     * should never have an object array as a value for any of it's properties.
     * It is here because the IJsonInterfacable interface requires it.
     *
     * @return string This object, serialized to a Json string.
     */
    public function ToJson($reindexObjectArrays = true)
    {
        return \DblEj\Communication\JsonUtil::EncodeJson($this, true, $reindexObjectArrays);
    }

    public function Get_ClientObjectVersion()
    {
        return "0.3";
    }
}