<?php
namespace DblEj\Communication\Ajax;

class AjaxRedirectPageResult
implements \DblEj\Communication\IJsonInterfacable
{
    private $_redirectUrl;
    public function __construct($redirectUrl)
    {
        $this->_redirectUrl = $redirectUrl;
    }

    public function Get_RedirectUrl()
    {
        return $this->_redirectUrl;
    }

    /**
     * The name of the Client-Side Counterpart to this class.
     *
     * @return string
     */
    public function Get_ClientObjectName()
    {
        return "DblEjAjaxRedirectPageResult";
    }

    /**
     * All the properties in this class that are serializable.
     *
     * @return array
     */
    public static function Get_SerializableFieldNames()
    {
        return array(
            "RedirectUrl");
    }

    /**
     * All serializable properties and their values.
     *
     * @return array
     */
    public function Get_SerializableFieldValues($forceUtf8Strings = false, $reindexObjectArrays = false)
    {
        return array(
            "RedirectUrl" => $this->_redirectUrl);
    }

    /**
     * Set the this object's properties using the values in the passed array.
     *
     * @param array $fieldValues
     * An associative array where each element is keyed by the property name.
     */
    final public function Set_SerializableFieldValues(array $fieldValues)
    {
        $this->_redirectUrl = $fieldValues["RedirectUrl"];
    }

    /**
     * Create an AjaxResult instance from a deserialized json instance
     * @param array $jsonArray
     * @return \DblEj\Communication\Ajax\AjaxResult
     */
    public static function CreateInstanceFromJsonArray(array $jsonArray)
    {
        $returnObj = new AjaxRedirectPageResult($jsonArray["RedirectUrl"]);
        return $returnObj;
    }

    /**
     * Serialize the result to JSON, adding special tags to allow
     * counterpart class instantiation on the client-side.
     *
     * @param boolean $reindexObjectArrays
     * This flag has no effect on this class, because this class
     * should never have an object array as a value for any of it's properties.
     * It is here because the IJsonInterfacable interface requires it.
     *
     * @return string This object, serialized to a Json string.
     */
    public function ToJson($reindexObjectArrays = true)
    {
        return \DblEj\Communication\JsonUtil::EncodeJson($this, true, $reindexObjectArrays);
    }
}