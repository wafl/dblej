/*
 *@namespace DblEj.Communication.Ajax
 */
Namespace("DblEj.Communication.Ajax");

/*
 *@class Utils
 *@static
 */
DblEj.Communication.Ajax.Utils = {
    /**
     * Sets the url of the server-side script which handles this client's request.
     *
     * @property {String} Set_HandlerUrl Sets the url of the server-side script which handles this client's request.
     * 
     * @param {String} newHandlerUrl The new Url.
     * @type {String}
     */
    Set_HandlerUrl: function (newHandlerUrl)
    {
        DblEj.Communication.Ajax.Utils._handlerUrl = newHandlerUrl;
    },
    /**
     * @method GetHttpClientObject Get a native Http object from the javascript client.
     * @return Object If the client is an old Microsoft browser, this will be an ActiveX Object, otherwise DblEj will attempt to create an XMLHttpRequest object.
     */
    GetHttpClientObject: function (withCredentials)
    {
        var returnHttp;

        if (!IsDefined(withCredentials))
        {
            withCredentials = true;
        }
        if (IsDefined(XMLHttpRequest))
        {
            returnHttp = new XMLHttpRequest();
        }
        else if (IsDefined(ActiveXObject))
        {
            returnHttp = new ActiveXObject('Msxml2.XMLHTTP');
        } else {
            throw "Cannot create an Http connection because the client does not support any known XMLHttpRequest types";
        }

        returnHttp.withCredentials = withCredentials;
        return returnHttp;
    },
    /**
     * Send a request to the server and wait for the response.
     *
     * @function SendRequest Send a synchronous request to the server.  Wait for the response.
     *
     * @param {String} call The name of the call.  Will be passed as an http get/post parameter.
     * 
     * @param {Object} requestObject The object to pass to the server.
     * @default null
     *
     * @param {String} method Must be one of: "post", "get"
     * @default post
     *
     * @param {Boolean} useDefaultJsonParser If true then it will skip wafl json parsing and use default json.parse from browser implementation.
     * @default false
     *
     * @return {String} The response from the server.
     */
    SendRequest: function (call, requestObject, method, useDefaultJsonParser)
    {
        var preparedRequest = DblEj.Communication.Ajax.Utils._prepareRequest(call, requestObject);
        var response =
            preparedRequest.RemoteConnection.SendDataSync(preparedRequest.RequestVariables, method, useDefaultJsonParser, preparedRequest.Headers);
        return response;
    },
    /**
     * Send an asynchronous request to the server.
     * Optionally call a callback method with the response whenever it is returned.
     *
     * @function SendRequestAsync send an asynchronous request to the server.  Do not wait for the response.  Optionally call a callback method with the response whenever it is returned.
     *
     * @param {String} call the name of the call.  Will be passed as an http get/post parameter
     * @param {Object} requestObject object to pass to the server
     * @param {String} sendToken a token that will be passed back with the response
     * @param {Function} callbackMethod a function that must be of the following signature: method(responseText,token)
     * @param {String} method must be either post|get
     * @param {Boolean} useDefaultJsonParser if true then it will skip wafl json parsing and use default json.parse from browser implementation
     * @return {Void}
     */
    SendRequestAsync: function (call, requestObject, sendToken, callbackMethod, method, useDefaultJsonParser)
    {
        var preparedRequest = DblEj.Communication.Ajax.Utils._prepareRequest(call, requestObject);
        preparedRequest.RemoteConnection.SendDataAsync(sendToken, callbackMethod, preparedRequest.RequestVariables, method, useDefaultJsonParser, preparedRequest.Headers);
    },
    SendRawAsync: function (url, requestString, callbackMethod, sendToken, method, useDefaultJsonParser)
    {
        if (!IsDefined(useDefaultJsonParser) || useDefaultJsonParser === null)
        {
            useDefaultJsonParser = 2; //raw text response
        }
        var headers = new Array();
        headers[0] = new DblEj.Communication.Ajax.HttpHeader("X-Requested-With", "XMLHttpRequest");
        var ajaxConnection = new DblEj.Communication.Ajax.Connection(url);

        ajaxConnection.SendDataAsync(sendToken, callbackMethod, requestString, method, useDefaultJsonParser, headers);
    },
    SendRaw: function (url, requestString, method, useDefaultJsonParser)
    {
        if (!IsDefined(useDefaultJsonParser) || useDefaultJsonParser === null)
        {
            useDefaultJsonParser = 2; //raw text response
        }
        var headers = new Array();
        headers[0] = new DblEj.Communication.Ajax.HttpHeader("X-Requested-With", "XMLHttpRequest");
        var ajaxConnection = new DblEj.Communication.Ajax.Connection(url);

        return ajaxConnection.SendDataSync(requestString, method, useDefaultJsonParser, headers);
    },
    /**
     * Asynchronously upload a file
     * @param {String} call
     * @param {HTMLInputElement} file
     * @param {Object} requestObject
     * @param {Function} progressCallback
     * @param {Function} completedCallback
     * @param {String} filenameHeader
     * @param {Boolean} useDefaultJsonParser
     * @returns {Void}
     */
    SendFile: function (call, file, requestObject, progressCallback, completedCallback, filenameHeader, useDefaultJsonParser)
    {
        var preparedRequest = DblEj.Communication.Ajax.Utils._prepareRequest(call, requestObject);
        preparedRequest.RemoteConnection.UploadFile(file, preparedRequest.RequestVariables, progressCallback, completedCallback, filenameHeader, null, useDefaultJsonParser, preparedRequest.Headers);
    },
    SendForm: function (call, form, requestObject, callback, sendToken, useDefaultJsonParser)
    {
        var preparedRequest = DblEj.Communication.Ajax.Utils._prepareRequest(call, requestObject);
        preparedRequest.RemoteConnection.SubmitForm(form, preparedRequest.RequestVariables, callback, useDefaultJsonParser, preparedRequest.Headers, sendToken);
    },
    _prepareRequest: function (call, requestObject, url)
    {
        var vars;
        var headers = new Array();

        if (!IsDefined(url) || url == null)
        {
            url = DblEj.Communication.Ajax.Utils._handlerUrl;
        }
        if (IsDefined(requestObject) && requestObject != null)
        {
            var reqObjectJson = DblEj.Communication.JsonUtil.EncodeJson(requestObject);
            vars = "RequestObject=" + reqObjectJson.UrlEncode();
        } else {
            vars = "";
        }
        headers[0] = new DblEj.Communication.Ajax.HttpHeader("WAFLACTION", call);
        headers[1] = new DblEj.Communication.Ajax.HttpHeader("X-Requested-With", "XMLHttpRequest");
        var ajaxConnection = new DblEj.Communication.Ajax.Connection(url);

        return {
            "RemoteConnection": ajaxConnection,
            "RequestVariables": vars,
            "Headers": headers
        };
    }
};