<?php

namespace DblEj\Communication\Ajax;

/**
 * Represents an error while handing an Ajax request.
 */
class AjaxError
implements \DblEj\Communication\IJsonInterfacable
{
    private $_errorMessage;
    private $_lineNumber;
    private $_file;
    private $_exception;

    /**
     * @param \Exception|string $exceptionOrError an exception object or a string describing the error
     * @param boolean $stripTags set to true if you want to remove html tags from the error message (which php adds if html_errors is set to true in php.ini)
     */
    public function __construct($exceptionOrError = null, $stripTags = true, $showTechDetails = false)
    {
        if ($exceptionOrError)
        {
            if (is_a($exceptionOrError, "\\DblEj\System\\Exception"))
            {
                $this->_errorMessage = $exceptionOrError->Get_PublicDetails();
                if ($showTechDetails)
                {
                    $this->_errorMessage .= " - DEBUG: ".$exceptionOrError->getMessage()." at ".$exceptionOrError->getLine()." in ".$exceptionOrError->getFile();
                    $this->_errorMessage .= $exceptionOrError->getTraceAsString();

                    $inner = $exceptionOrError;
                    while ($inner = $inner->getPrevious())
                    {
                        $this->_errorMessage .= " - INNER: ".$inner->getMessage()." at ".$inner->getLine()." in ".$inner->getFile();
                        $this->_errorMessage .= $inner->getTraceAsString();
                    }
                }
            }
            elseif (is_object($exceptionOrError) && $showTechDetails)
            {
                $this->_errorMessage = $exceptionOrError->getMessage();
                $this->_errorMessage .= $exceptionOrError->getTraceAsString();
                $inner = $exceptionOrError;
                while ($inner = $inner->getPrevious())
                {
                    $this->_errorMessage .= " - INNER: ".$inner->getMessage()." at ".$inner->getLine()." in ".$inner->getFile();
                    $this->_errorMessage .= $inner->getTraceAsString();
                }
            }
            elseif (is_string($exceptionOrError)) //if they pass a string specifially, we assume they want to show it
            {
                $this->_errorMessage = $exceptionOrError;
            } else {
                $this->_errorMessage = "An unexpected error occured. Please try again or contact support.".print_r($exceptionOrError, true);
            }
            if ($stripTags)
            {
                $this->_errorMessage = strip_tags($this->_errorMessage);
            }

            if (is_object($exceptionOrError)) {
                $this->_lineNumber = $exceptionOrError->getLine();
                $this->_file       = $exceptionOrError->getFile();
                $this->_exception  = $exceptionOrError;
            } else {
                $this->_lineNumber = 0;
                $this->_file       = "";
                $this->_exception  = new \Exception($exceptionOrError);
            }
        }
    }

    /**
     * Sets a message that describes the error.
     * @param string $message
     */
    public function Set_ErrorMessage($message)
    {
        $this->_errorMessage = $message;
    }

    /**
     * Returns a message that describes the error.
     * @return string
     */
    public function Get_ErrorMessage()
    {
        return $this->_errorMessage;
    }

    /**
     * The line number that the error occurred on.
     * Note that sometimes this will actually show the line number of an error handler depending on how the exception is thrown.
     * @return int
     */
    public function Get_LineNumber()
    {
        return $this->_lineNumber;
    }

    /**
     * The name of the file that the error occurred in
     * @return string
     */
    public function Get_File()
    {
        return $this->_file;
    }

    public function Get_Exception()
    {
        return $this->_exception;
    }

    /**
     * The name of the Client-Side Counterpart to this class
     * @return string
     */
    public function Get_ClientObjectName()
    {
        return "DblEjError";
    }

    /**
     * A list of all the Properties in this class that are serializable
     * @return array
     */
    public static function Get_SerializableFieldNames()
    {
        return array(
            "Exception",
            "ErrorMessage",
            "LineNumber",
            "File");
    }

    /**
     * All serializable properties and their values
     * @return array
     */
    public function Get_SerializableFieldValues($forceUtf8Strings = false, $reindexObjectArrays = false)
    {
        if (is_a($this->_exception, "\DblEj\Communication\IJsonInterfacable"))
        {
            $serializedException = $this->_exception->Get_SerializableFieldValues();
        } else {
            $serializedException = $this->_exception;
        }
        $returnArray = array(
            "Exception"    => $serializedException,
            "ErrorMessage" => $this->_errorMessage,
            "LineNumber"   => $this->_lineNumber,
            "File"         => $this->_file);
        return $returnArray;
    }

    /**
     * Deserialize an array that was serialized with <i>Get_SerializableFieldValues</i>,
     * restoring it's property values to this object's properties
     *
     * @param array $fieldValues An associative array containing a member for each property of this object.
     */
    final public function Set_SerializableFieldValues(array $fieldValues)
    {
        $this->_exception    = $fieldValues["Exception"];
        $this->_errorMessage = $fieldValues["ErrorMessage"];
        $this->_lineNumber   = $fieldValues["LineNumber"];
        $this->_file         = $fieldValues["File"];
    }

    /**
     * Create an AjaxException instance from a deserialized json instance
     * @param array $jsonArray
     * @return \DblEj\Communication\Ajax\AjaxException
     */
    public static function CreateInstanceFromJsonArray(array $jsonArray)
    {
        $returnObj = new AjaxException();
        $returnObj->Set_ErrorMessage($jsonArray["ErrorMessage"]);
        return $returnObj;
    }

    /**
     * Serialize the error in JSON
     * @param boolean $reindexObjectArrays
     * @return string The Json object.
     */
    public function ToJson($reindexObjectArrays = true)
    {
        return \DblEj\Communication\JsonUtil::EncodeJson($this, true, $reindexObjectArrays);
    }

    public function Get_ClientObjectVersion()
    {
        return "0.3";
    }
}