/**
 * @namespace DblEj.Communication.Ajax
 */
Namespace("DblEj.Communication.Ajax");

/**
 * A single Http header to be included with a request.
 * 
 * @class HttpHeader
 * @extends Class
 */
DblEj.Communication.Ajax.HttpHeader = Class.extend(
    {
        /**
         * @constructor init
         * @param headerName string
         * @param headerValue string
         **/
        init: function (headerName, headerValue)
        {
            this._headerName = headerName;
            this._headerValue = headerValue;
        },
        /**
         * @property {string} Get_HeaderName The name part of the header.
         * @returns {string}
         */
        Get_HeaderName: function ()
        {
            return this._headerName;
        },
        /**
         * @property {string} Get_HeaderValue The value part of the header.
         * @returns {string}
         */
        Get_HeaderValue: function ()
        {
            return this._headerValue;
        }
    });