<?php

namespace DblEj\Communication\Ajax;

class AjaxException
extends \DblEj\Communication\Http\Exception
{
    public function __construct($ajaxFunction, $publicDetails, $debugMessage = null, $innerException = null, $httpErrorCode = 500)
    {
        if (!$debugMessage)
        {
            $debugMessage = $publicDetails;
        }
        parent::__construct($ajaxFunction, $debugMessage, $httpErrorCode, E_ERROR, $innerException, null, $publicDetails);
    }
}