<?php
namespace DblEj\Communication\Ajax;

/**
 * Encapsulates the information needed to handle an HTTP JSON request.
 *
 * Use the static factory method AddAjaxHandler() to get an instance of this class.
 *
 * When a request is receievd, use the static method LoadByAjaxFunctionName()
 * to load the correct handler for that request.
 *
 * Use the instance method HandleCall() on the handler to handle the request.
 */
final class AjaxHandler
implements \DblEj\Communication\ITravelDestination
{
    private $_ajaxFunctionName;
    private $_sourceFunctionName;
    private $_sourceFile;
    private $_sourceClass;
    private $_minimumPrivilegeLevel;
    private $_requestType; //get or post
    private $_requestObjectName;
    private $_modelsToPreload;
    private $_sourceIsInterfaceCompliant;
    private $_app;
    private $_reindexModelArrays;
    private $_allowCredentials;

    private static $_handlers = array();

    /**
     * Ajax requests sent using HTTP GET will be of this type
     */
    const REQUEST_TYPE_GET = "get";

    /**
     * Ajax requests sent using HTTP PUT will be of this type
     */
    const REQUEST_TYPE_PUT = "put";

    /**
     * Create an AjaxHandler
     * @param string $ajaxFunctionName The name of the ajax request (the "call")
     * @param string $handlerFunctionName The name of the php function which will handle this request
     * @param string $handlerFile The php file which contains the function passed in $handlerFunctionName
     * @param string $handlerClass the fully qualifed name of the class which contains the function passed in $handlerFunctionName
     * @param string $requestType AjaxHandler::REQUEST_TYPE_GET or AjaxHandler::REQUEST_TYPE_PUT
     * @param int $minimumPrivilegeLevel An aributrary value that is used to compare against a users privelege level to restrict access (this feature will be phased out in subsequent releases)
     */
    private function __construct($ajaxFunctionName, $handlerFunctionName, $handlerFile = "", $handlerClass = "", $requestType = AjaxHandler::REQUEST_TYPE_GET, $minimumPrivilegeLevel = 0, $requestObjectName = "RequestObject", \DblEj\Data\Integration\IDatabaseServer $storageEngine = null, \DblEj\Application\IApplication $app = null)
    {
        $this->_ajaxFunctionName      = $ajaxFunctionName;
        $this->_sourceFile            = $handlerFile;
        $this->_sourceClass           = $handlerClass;
        $this->_sourceFunctionName    = $handlerFunctionName;
        $this->_minimumPrivilegeLevel = $minimumPrivilegeLevel;
        $this->_requestType           = $requestType;
        $this->_requestObjectName     = $requestObjectName;
        $this->_reindexModelArrays    = false;
        $this->_modelsToPreload       = null;
        $this->_sourceIsInterfaceCompliant = null; //old style used to just use any static method without an interface and they were expected to have certain params.  If this is set to true then that means the handler is a newer style typed ApiHandler.  We'll calculate it on-the-flay to save time.
        $this->_app = $app;
        $this->_allowCredentials = true; //need a way to set this
    }

    protected $_travelerRoles;

    public static function Get_ClassTravelerRoles()
    {
        return ["\\DblEj\\Authentication\\IUser", "\\DblEj\\Authentication\\IUserGroup"];
    }
    public function Get_TravelerRoles()
    {
        if (is_a($this->_sourceClass, "\\DblEj\\Communication\\ITravelDestination", true))
        {
            $travClass = $this->_sourceClass;
            $this->_travelerRoles = $travClass::Get_ClassTravelerRoles();
        }
        if (!$this->_travelerRoles)
        {
            $this->_travelerRoles = static::Get_ClassTravelerRoles();
        }
        return $this->_travelerRoles;
    }
    public static function Set_TravelerRoles($travelerRoles)
    {
        $this->_travelerRoles = $travelerRoles;
    }

    /**
     * The name of the Ajax/API request.
     *
     * @return string
     */
    public function Get_AjaxFunctionName()
    {
        return $this->_ajaxFunctionName;
    }

    public function Set_AjaxFunctionName($newValue)
    {
        $this->_ajaxFunctionName = $newValue;
    }

    public function Get_ReindexModelArrays()
    {
        return $this->_reindexModelArrays;
    }

    public function Set_ReindexModelArrays($newValue)
    {
        $this->_reindexModelArrays = $newValue;
    }


    public function Get_ModelsToPreload()
    {
        return $this->_modelsToPreload;
    }

    public function Set_ModelsToPreload($newValue)
    {
        $this->_modelsToPreload = $newValue;
    }


    /**
     * An aributrary value that is used to compare against a users privelege level to restrict access (this feature will be phased out in subsequent releases)
     * @return int
     */
    public function Get_MinimumPrivilegeLevel()
    {
        return $this->_minimumPrivilegeLevel;
    }

    /**
     * The name of the php function which will handle this request
     * @return string
     */
    public function Get_SourceFunctionName()
    {
        return $this->_sourceFunctionName;
    }

    public function Set_SourceFunctionName($newValue)
    {
        $this->_sourceFunctionName = $newValue;
    }

    /**
     * The php file which contains either:
     *  the class in <i>SourceClass</i>, if <i>SourceClass</i> is not null,
     *  otherwise, the global function in <i>SourceFunctionName</i>.
     * @return string
     */
    public function Get_SourceFile()
    {
        return $this->_sourceFile;
    }

    public function Set_SourceFile($newValue)
    {
        $this->_sourceFile = $newValue;
    }

    /**
     * The fully qualifed name of the class which contains the <i>handlerFunctionName</i> class,
     * which was specified during handler registration.
     *
     * @return string
     */
    public function Get_SourceClass()
    {
        return $this->_sourceClass;
    }

    public function Set_SourceClass($newValue)
    {
        $this->_sourceClass = $newValue;
    }

    private function _getRequestHeaders()
    {
        $requestHeaders = array();
        foreach ($_SERVER as $serverVar => $serverVal)
        {
            if (\DblEj\Util\Strings::StartsWith($serverVar, "HTTP_X"))
            {
                $requestHeaders[substr($serverVar, 7)] = $serverVal;
            }
        }
        return $requestHeaders;
    }

    private function _getRequestObject($requestVars)
    {
        $requestObject  = isset($requestVars[$this->_requestObjectName]) ? $requestVars[$this->_requestObjectName] : $requestVars;
        if (is_string($requestObject))
        {
            $requestObject = \DblEj\Communication\JsonUtil::DecodeJson($requestObject, null, true);
        }
        return $requestObject;
    }

    private function _getReturnRaw($requestVars)
    {
        return isset($requestVars["ReturnRaw"]) ? $requestVars["ReturnRaw"] : false;
    }

    private function _getFileUploadArray()
    {
        $fileUploadArray = [];
        foreach ($_FILES as $varName=>$file)
        {
            $fileUploadArray[$varName] = $file;
        }
        return $fileUploadArray;
    }

    private function _getResponse($requestObject, $requestHeaders, $fileUploadArray, \DblEj\Communication\IRequest $request, &$returnRaw)
    {
        $response = null;
        if ($this->_sourceClass)
        {
            if ($this->_sourceIsInterfaceCompliant === null)
            {
                $this->_sourceIsInterfaceCompliant = is_a($this->_sourceClass, "\\DblEj\\Communication\\ApiRequestHandler", true);
            }
            if ($this->_sourceIsInterfaceCompliant)
            {
                $apiRequest = new \DblEj\Communication\Http\ApiRequest($this->_sourceFunctionName, $requestObject, $requestHeaders, $fileUploadArray, $this->_app, $request, $this->_reindexModelArrays);
                $sourceClass = $this->_sourceClass;

                $apiHandlerObject = new $sourceClass();
                $response = $apiHandlerObject->HandleHttpRequest($apiRequest, $this->_sourceFunctionName, $this->_app);
                $returnRaw = true;
            } else {
                $response = call_user_func_array
                (
                    array
                    (
                        $this->_sourceClass,
                        $this->_sourceFunctionName
                    ),
                    array
                    (
                        $requestObject,
                        $requestHeaders,
                        $fileUploadArray,
                        $this->_app
                    )
                );
            }
        }
        else
        {
            $response = call_user_func_array($this->_sourceFunctionName, array(
                $requestObject,
                $requestHeaders,
                $fileUploadArray));
        }
        return $response;
    }

    private function _validateResponse($response)
    {
        if ($response === false)
        {
            throw new \Exception("There was an error when trying to call the ajax handler for $this->_sourceFunctionName.  Are you sure your routing is set up correctly?");
        }
        if ($response === null || (is_array($response) && count($response) == 0 ))
        {
            $lastError = error_get_last();
            if ($lastError !== null && ($lastError["message"] != \Wafl\Util\Debug::Get_LastSuppressedError()))
            {
                throw new \ErrorException($lastError["message"], $lastError["type"], $lastError["type"], $lastError["file"], $lastError["line"]);
            }
            elseif ($response === null)
            {
                throw new \Exception("null response returned from ajax handler for $this->_sourceFunctionName.  Check that your handler is returning a valid response and is being referenced correctly (check the namespace)");
            }
        }
        return $response;
    }
    private function _prepareResponse($response, $returnRaw)
    {
        if (!$returnRaw)
        {
            $response = \DblEj\Communication\JsonUtil::EncodeJson($response, true, $this->_reindexModelArrays);
            $response = new \DblEj\Communication\Http\ApiResponse($this->_ajaxFunctionName, $response);
        }
        elseif ($this->_reindexModelArrays)
        {
            if (is_array($response))
            {
                $response = array_values($response);
            }
            elseif (is_a($response, "\\DblEj\\Communication\\Http\\ApiResponse"))
            {
                $response->ReindexResponseArray();
            }
        }
        if (is_a($response, "\\DblEj\\Communication\\Http\\ApiResponse"))
        {
            $response->Set_ETag(null);
            $response->Set_MimeType("application/json");
            $response->Set_BrowserCacheTimeout(\DblEj\Communication\Http\Headers::CACHEPOINT_NONE, 0, true);
        }
        return $response;
    }

    /**
     * Handle an incoming AJAX request
     * @param array $requestVars usually the HTTP GET and/or POST variables in key/value pairs
     * @return mixed a serialized response object
     */
    public function HandleCall(\DblEj\Communication\IRequest $request, $requestVars)
    {
        $response = null;
        $requestObject  = $this->_getRequestObject($requestVars);
        $requestHeaders = $this->_getRequestHeaders();
        $returnRaw = $this->_getReturnRaw($requestVars);

        if (!headers_sent())
        {
            \DblEj\Communication\Http\Util::DontAllowAnyCaching();
            header("Content-Type: application/json"); //need a way to set this dynamically
            if ($this->_allowCredentials)
            {
                header("Access-Control-Allow-Credentials: true");
            }

        }

        //NOTE: Throwing an exception during Ajax should result is user seeing a nice message, but it is still recorded as an Exception.
        //If you want to tell the user something is wrong, without recording an exception, then return an AjaxError instead of throwing an Exception.

        try
        {
            $fileUploadArray = $this->_getFileUploadArray();
            $response = $this->_getResponse($requestObject, $requestHeaders, $fileUploadArray, $request, $returnRaw);
            $this->_validateResponse($response);
            $response = $this->_prepareResponse($response, $returnRaw);
        }
        catch (\DblEj\System\Exception $ex)
        {
            throw new AjaxException("API - $this->_ajaxFunctionName", $ex->Get_PublicDetails(), "There was a System Exception while handling an Ajax request.", $ex);
        }
        catch (\Exception $ex)
        {
            throw new AjaxException("API - $this->_ajaxFunctionName", null, "There was an Exception while handling an Ajax request.", $ex);
        }
        return $response;

    }

    /**
     * Load a globally registered AJAX handler from memory
     * NOTE: handlers can be registered using AjaxHandler::AddAjaxHandler()
     * @param string $ajaxCall
     * @return \DblEj\Communication\Ajax\AjaxHandler
     */
    public static function LoadByAjaxFunctionName($ajaxCall)
    {
        $returnFunction = null;
        if (isset(self::$_handlers[$ajaxCall]))
        {
            $returnFunction = self::$_handlers[$ajaxCall];
        }
        return $returnFunction;
    }

    /**
     * Register a handler to be used for incoming Ajax/API requests.
     * You can retrieve the handler later by using AjaxHandler::LoadByAjaxFunctionName($callName);
     *
     * @param string $ajaxFunctionName
     * Every Ajax/API request in DblEj is expected to pass a "Call",
     * which represents the action that the client is requesting.
     * This can be passed either in the HTTP headers, or in a request variable (GET/POST).
     * That call name is represented here in the handler by this variable.
     *
     * @param string $handlerFunctionName
     * The name of the php function or method which will handle this request.
     *
     * @param string $handlerFile
     * The php file which contains the <i>handlerFunctionName</i> function,
     * or which contains the <i>handlerClass</i> class, if it is specified.
     *
     * @param string $handlerClass
     * The fully qualifed name of the class which contains the <i>$handlerFunctionName</i> method,
     * or <i>null</i> if <i>$handlerFunctionName</i> is a global function.
     *
     * @param string $requestType
     * The HTTP request type.
     * Use one of these constants:
     * AjaxHandler::REQUEST_TYPE_GET,
     * AjaxHandler::REQUEST_TYPE_PUT
     *
     * @param int $minimumPrivilegeLevel
     * An aributrary value that is used to compare against a users privelege level
     * to restrict access.
     * Deprecation of the entire <i>privilege level</i> concept is being recommended.
     */
    public static function AddAjaxHandler($ajaxFunctionName, $handlerFunctionName, $handlerFile = "", $handlerClass = "", $requestType = AjaxHandler::REQUEST_TYPE_GET, $minimumPrivilegeLevel = 0, $reindexModelArrays = false, $modelsToPreload = null, $app = null, array $travelerRoles = null)
    {
        if (!isset(self::$_handlers[$ajaxFunctionName]))
        {
            $handler = new AjaxHandler($ajaxFunctionName, $handlerFunctionName, $handlerFile, $handlerClass, $requestType, $minimumPrivilegeLevel, "RequestObject", null, $app);
            $handler->Set_ReindexModelArrays($reindexModelArrays);
            $handler->Set_ModelsToPreload($modelsToPreload);
            self::$_handlers[$ajaxFunctionName] = $handler;
        }
        return $handler;
    }

    /**
     * Unregister an Ajax/API handler
     * @param string $ajaxFunctionName The name of the ajax request (the "call") to be removed
     */
    public static function RemoveAjaxHandler($ajaxFunctionName)
    {
        if (isset(self::$_handlers[$ajaxFunctionName]))
        {
            unset(self::$_handlers[$ajaxFunctionName]);
        }
    }
}