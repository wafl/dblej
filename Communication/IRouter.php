<?php

namespace DblEj\Communication;

use DblEj\Application\IApplication;
use DblEj\Communication\IRequest;

/**
 * A Router's job is to find a suitable route for a given request.
 * A router should act as though it is part of a router chain in a chain-of-responsibility type of implementation.
 */
interface IRouter
{

    /**
     * Return an appropriate route for the given request.
     *
     * In case it is part of a Router chain, if this router does not find an appropriate route,
     * it should return false so that other routers in the chain can get a try at it.
     *
     * @param \DblEj\Communication\IRequest $request
     * The request that the router is being asked to find a route for.
     * The correct route is the one that knows how to respond to the request.
     *
     * @param \DblEj\Application\IApplication $app
     * The application that receieved the <i>request</i>.
     *
     * @param \DblEj\Communication\IRouter &$usedRouter
     * If this Router does find a route for the request,
     * it will assign itself to the by-reference argument <i>usedRouter</i>
     * so that the application knows which router handled the request, in the
     * event that this router is part of a router chain.
     *
     * @return \DblEj\Communication\IRoute The Route
     */
    public function GetRoute(IRequest $request, IApplication $app = null, IRouter &$usedRouter = null);
}