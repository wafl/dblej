<?php

namespace DblEj\Communication;

/**
 * EmailUtil is an email client utility class.
 */
class EmailUtil
{

    /**
     * Send a multi-part mime message with plain text and html versions of the email embedded.
     * @param string $textEmail The email body as plain text.
     * @param string $htmlEmail The email body as HTML.
     * @param string $subject The subject of the email (RFC 2047).
     * @param string $fromAddress The email address of the sender.
     * @param string $fromName The name of the sender.
     * @param string $toAddress Receiver, or receivers of the mail (RFC 2822).  Examples: user@example.com user@example.com, anotheruser@example.com User <user@example.com> User <user@example.com>, Another User <anotheruser@example.com>.
     * @return boolean Returns true if the message is successfully delivered to the outgoing mail queue (note this does not guarantee the message has been delivered).
     */
    public static function SendMixedMail($textEmail, $htmlEmail, $subject, $fromAddress, $fromName, $toAddress, $bcc = null, $smtpServer = null, $smtpServerUser = null, $smtpServerPassword = null)
    {
        // Boundary
        $innerboundary = "=_" . time() . "_=";
        // Mail-Header
        $mail_head     = "MIME-Version: 1.0\n";
        $mail_head.="From: $fromName<$fromAddress>\n";
        $mail_head.="Reply-To: $fromName<$fromAddress>\n";
        $mail_head.="Return-Path: $fromName<$fromAddress>\n";
        $mail_head.="X-Mailer: DblEj-Mailer\n";
        $mail_head.="Content-Type: multipart/alternative;\n\tboundary=\"" . $innerboundary . "\"\n";
        if ($bcc)
        {
            $mail_head.="Bcc: $bcc\n";
        }
        $mail_body = "";
        // TEXT part
        $mail_body.="\n--" . $innerboundary . "\n";
        $mail_body.="Content-Type: text/plain; charset=us-ascii\n\n";
        $mail_body.=$textEmail . "\n\n";
        // HTML part
        $mail_body.="\n--" . $innerboundary . "\n";
        $mail_body.="Content-type: text/html; charset=utf-8;\n";
        $mail_body.="Content-Transfer-Encoding: base64\n\n";
        $mail_body.=chunk_split(base64_encode($htmlEmail)) . "\n\n";
        $mail_body.="\n--" . $innerboundary . "--\n";
        $mail_body.="\n\n";

        // Send the mail
        if ($smtpServer)
        {
            ini_set("SMTP", $smtpServer);
        }
        if (mail($toAddress, $subject, $mail_body, $mail_head))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}