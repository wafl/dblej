<?php
namespace DblEj\Communication\Tcp;

class Server extends MessagingSocket
{
    /**
     * @param resource $socket
     */
    public function __construct($socket = null, $dataReceiveCallback = null, $errorCallback = null, $messageTerminator = "\r\n")
    {
        $returnVal = parent::__construct($socket, $dataReceiveCallback, $errorCallback, $messageTerminator);
        if ($this->_socket)
        {
            socket_set_nonblock($this->_socket);
            $this->_connected = true;
            $this->_remoteip = "";
            socket_getpeername($this->_socket, $this->_remoteip);
            return $returnVal;
        } else {
            throw new \Exception("A listener must provide the socket argument to create a tcp server");
        }
    }
}