<?php
namespace DblEj\Communication\Tcp;

class MessagingSocket extends Socket
{
    protected $_messageTerminatorIn;
    protected $_messageTerminatorOut;

    /**
     * @param resource $socket
     */
    public function __construct($socket = null, $dataReceiveCallback = null, $errorCallback = null, $messageTerminator = ["\r\n", "\r\n"], $closeData = "")
    {
        if (!is_array($messageTerminator))
        {
            $messageTerminator = [$messageTerminator, $messageTerminator];
        }
        $this->_messageTerminatorIn = $messageTerminator[0];
        $this->_messageTerminatorOut = $messageTerminator[1];

        return parent::__construct($socket, $dataReceiveCallback, $errorCallback, $messageTerminator, $closeData);
    }

    public function SendData($data, $appendEol=false)
    {
        if ($appendEol)
        {
            $data .= $this->_messageTerminatorOut;
        }
        parent::SendData($data, false);
    }

    protected function _processInBuffer()
    {
        if ($this->_inBuffer)
        {
            if ($this->_messageTerminatorIn !== null)
            {
                $lastEol = strripos($this->_inBuffer, $this->_messageTerminatorIn);
                if ($lastEol !== false)
                {
                    $completedLines = substr($this->_inBuffer, 0, $lastEol);
                    $this->_inBuffer = substr($this->_inBuffer, $lastEol+1);
                    if (strlen($completedLines)>0)
                    {
                        $bufferLines = explode($this->_messageTerminatorIn, $completedLines);
                        foreach ($bufferLines as $bufferLine)
                        {
                            $inputMeta = null;
                            $this->onDataReceived($this->decodeInputProtocol($bufferLine, $inputMeta), $inputMeta);
                        }
                        return true;
                    }
                }
                return false;
            } else {
                return parent::_processInBuffer();
            }
        }
        return false;
    }
}