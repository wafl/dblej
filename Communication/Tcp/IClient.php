<?php
namespace DblEj\Communication\Tcp;

interface IClient
{
    function Connect($remoteIp = null, $remotePort = null);
    function Close();
}