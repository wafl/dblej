<?php
namespace DblEj\Communication\Tcp;

class Socket
{
    protected $_socket;
    protected $_connected;
    protected $_inBuffer;
    protected $_mypid;
    protected $_remoteip;
    protected $_receiveDataCallback;
    protected $_errorCallback;
    private $_closeData;

    /**
     * @param resource $socket
     */
    public function __construct($socket = null, $dataReceiveCallback = null, $errorCallback = null, $messageTerminator = ["\r\n", "\r\n"], $closeData = "")
    {
        if ($socket)
        {
            $this->_socket = $socket;
        } else {
            if (($this->_socket = $this->_createSocketInstance()) === false) {
                $lastError = $this->_getLastSocketError();
                throw new \Exception("socket_create() failed: reason: " . $lastError[1]);
            }
        }

        $this->_inBuffer="";
        $this->_mypid=posix_getpid();
        $this->_remoteip="";
        $this->_receiveDataCallback = $dataReceiveCallback;
        $this->_errorCallback = $errorCallback;
        $this->_closeData = $closeData;
    }

    protected function _createSocketInstance()
    {
        return socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    }

    public function CheckDataLoop()
    {
        $this->_mypid=posix_getpid();
        $loopIdx = 0;
        while ($this->_connected)
        {
            if ($this->checkData())
            {
                usleep(10000);
            } else {
                usleep(50000);
            }
            $loopIdx++;
            if ($loopIdx % 10 == 0)
            {
                //in case parent is asking me to quit, need to check signals
                //this is to remedy runaway processes that wouldnt quit as long as client was connected,
                //which as a feature actually has its merits too.  Would be nice to be optional.
                pcntl_signal_dispatch();
            }
        }
        pcntl_signal_dispatch();
    }

    protected function _writeSocket($data)
    {
        $totalBytes = strlen($data);
        return @socket_write($this->_socket, $data, $totalBytes);
    }

    protected function _readSocket($data)
    {
        return @socket_read($this->_socket, 1024);
    }

    public function SendData($data, $appendEol=false)
    {
        if ($this->_connected)
        {
            $totalBytes = strlen($data);
            $bytesSent = $this->_writeSocket($data);
            if ($bytesSent === false)
            {
                $bytesSent = 0;
            }
            usleep(10000);
            $startTime = time();
            while ($this->_connected && isset($this->_socket) && $this->_socket && ($bytesSent < $totalBytes) && ((time() - $startTime) < 3)) //if it hasnt sent if 3 secs something terribly wrong.  THis is here to prevetn infinite loop
            {
                $addlBytesSent = $this->_writeSocket(substr($data, $bytesSent));
                if ($addlBytesSent !== false)
                {
                    $bytesSent += $addlBytesSent;
                    if ($bytesSent < $totalBytes)
                    {
                        usleep(10000);
                    }
                }
            }
        }
    }

    protected function decodeInputProtocol($input, &$metaData = null)
    {
        return $input;
    }

    protected function checkData()
    {
        $wasData = false;
        $this->_processInBuffer();
        $buf = $this->_readSocket(1024);
        if ($buf) {
            $wasData = true;
            $this->_lastContact = time();
            $this->_inBuffer .= $buf;
        } elseif ($buf === $this->_closeData) {
            $this->Close();
        }

        if ($buf === false) {
            $lastError = $this->_getLastSocketError();
            if ($lastError[0] > 0 && $lastError[0] != 11) //11 just means there is no data
            {
                //$errorData could be the same as $closeData, in which case we don't need to close again
                if ($buf !== $this->_closeData)
                {
                $this->Close();
                }
                $this->onError($lastError[0], $lastError[1]);
            }
        }
        return $wasData;
    }

    protected function _processInBuffer()
    {
        if ($this->_inBuffer)
        {
            $bufferLen = strlen($this->_inBuffer);
            $bufferLine = substr($this->_inBuffer, 0, $bufferLen);
            $this->_inBuffer = substr($this->_inBuffer, $bufferLen);
            $inputMeta = null;
            $this->onDataReceived($this->decodeInputProtocol($bufferLine, $inputMeta), $inputMeta);
            return true;
        } else {
            return false;
        }
    }

    protected function _getLastSocketError()
    {
        $lastErrorCode = socket_last_error();
        return [$lastErrorCode?$lastErrorCode:0, $lastErrorCode?socket_strerror($lastErrorCode):""];
    }

    protected function onDataReceived($data, $inputMetaData = null)
    {
        if (is_callable($this->_receiveDataCallback))
        {
            $this->_receiveDataCallback($data);
        }
    }
    protected function onError($code, $description)
    {
        if (is_callable($this->_errorCallback))
        {
            $this->_errorCallback($code, $description);
        }
    }

    public function Close()
    {
        $this->_connected = false;
        if (isset($this->_socket))
        {
            unset($this->_socket);
        }
        $this->_processInBuffer();
    }

    public function Get_IsConnected()
    {
        return $this->_connected;
    }
}