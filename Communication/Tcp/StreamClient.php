<?php
namespace DblEj\Communication\Tcp;

class StreamClient extends Stream implements IClient
{
    public function Connect($hostUri = null, $port = null)
    {
        $this->_remoteIp = $hostUri;

        set_time_limit(0);
        ob_implicit_flush();
        $this->_stream = null;

        $errno = null;
        $errstr = null;
        $timeout = 30;
        $flags = STREAM_CLIENT_CONNECT;

        $context = stream_context_create([
          'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
          ]
        ]);

        if (($this->_stream = stream_socket_client("$hostUri:$port", $errno, $errstr, $timeout, $flags, $context)) === false) {
            throw new \Exception("stream_socket_client() failed: reason: $errstr");
        }

        stream_set_blocking($this->_stream, false);

        $this->_connected = true;
        $this->_remoteip = stream_socket_get_name($this->_stream, true);
    }
}