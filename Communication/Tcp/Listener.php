<?php
namespace DblEj\Communication\Tcp;

class Listener
{
    private $_address;
    private $_port;
    private $_socket;
    private $_listening;
    private $_isMaster;
    private $_childProcesses;
    private $_serverClass;
    private $_pidFile;

    public function __construct($address, $port, $pidFile = null, $serverClass = "\\DblEj\\Communication\\Tcp\\Server")
    {
        $this->_address = $address;
        $this->_port = $port;
        $this->_listening = false;
        $this->_childProcesses = array();
        $this->_serverClass = $serverClass;
        $this->_pidFile = $pidFile;
    }

    public function StartService()
    {
        $this->_isMaster = ($this->BecomeDaemon()===true);

        if ($this->_isMaster)
        {
            //write my pid to a text file so linux services can stop me when they want to
            //even though we did this earlier, we need to do this because after forking our pid has changed
            $pid = \posix_getpid();
            if ($this->_pidFile)
            {
                file_put_contents($this->_pidFile, "$pid");
            }

            try
            {
                $this->Listen();
            } catch (\Exception $ex) {
                if ($this->_pidFile && file_exists($this->_pidFile))
                {
                    unlink($this->_pidFile);
                }
                throw $ex;
            }
        }
    }
    public function  __destruct()
    {
        if ($this->_isMaster)
        {
            $this->StopListening();
        }
    }

    public function Get_IsMaster()
    {
        return $this->_isMaster;
    }
    private function BecomeDaemon()
    {
        //create new process, which will be set to seesion leader and then used to create actual daemon process
        $newpid = \pcntl_fork();
        if ($newpid === -1)
        {
            exit(0);
        }
        else if ($newpid>0)
        {
            //end the originating process which created this daemon

            //this is the start thread, pre-daemon.
            //Lets wait a second to allow the daemon thread to write its pid to disk,
            //otherwise when I exit, systemd will think that my PID is the daemon process.
            sleep(1);

            exit(0);
        }
        // Become the session leader
        \posix_setsid();
        \usleep(100000);

        //now as session leader, create the daemon process
        $newpid = \pcntl_fork();
        if ($newpid === -1)
        {
            exit(0);
        }
        else if ($newpid>0)
        {
            //end the session leader process which created this daemon
            exit(0);
        }
        return true;
    }


    public function Set_Listening($isLestening)
    {
        $this->_listening = $isLestening;
    }

    private function Listen()
    {
        set_time_limit(0);
        ob_implicit_flush();
        $this->_socket=null;
        if (($this->_socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            throw new \Exception("socket_create() failed: reason: " . socket_strerror(socket_last_error()));
        }

        if (socket_set_option($this->_socket, SOL_SOCKET, SO_REUSEADDR, 1) === false) {
            throw new \Exception("cannot set reuseaddr option: " . socket_strerror(socket_last_error($this->_socket)));
        }

        if (@socket_bind($this->_socket, $this->_address, $this->_port) === false) {
            throw new \Exception("socket_bind() failed: reason: " . socket_strerror(socket_last_error($this->_socket)));
        }

        if (socket_listen($this->_socket, 5) === false) {
            throw new \Exception("socket_listen() failed: reason: " . socket_strerror(socket_last_error($this->_socket)));
        }

        socket_set_nonblock($this->_socket);
        $this->_listening = true;
        $this->CheckRequestLoop();
    }

    private function CheckRequestLoop()
    {
        while ($this->_listening && $this->_isMaster)
        {
            $newGameServer = false;
            $newConnection = $this->TryAcceptConnectionRequest();
            if (is_resource($newConnection))
            {
                $newServerConnectionPid = \pcntl_fork();

                if ($newServerConnectionPid == -1)
                {
                    //Logger::Trace ("error trying to fork child process for server connection");
                    exit(0);
                }
                elseif ($newServerConnectionPid == 0)
                {
                    //this is the new connection process, which is not a listen server
                    $this->_listening = false;
                    $this->_isMaster = false;
                    $this->_childProcesses = array();
                    $this->_isMaster = false;
                    $newGameServer = new $this->_serverClass($newConnection);
                    $newGameServer->CheckDataLoop();

                    exit(0);
                } else {
                    //this is the listening process
                    $this->_childProcesses[] = $newServerConnectionPid;
                    //Logger::Trace("Accepted connection on new server (pid: $newServerConnectionPid), open connections = ".count($this->_childProcesses));
                }
                usleep(20000);
            } else {
                usleep(250000);
            }
            pcntl_signal_dispatch();
        }
    }
    private function TryAcceptConnectionRequest()
    {
        $newGameServer=null;
        if (isset($this->_socket) && $this->_socket)
        {
            $newGameServer=@socket_accept($this->_socket);
        }
        return $newGameServer;
    }

    public function KillChildren($signal)
    {
        foreach ($this->_childProcesses as $childPid)
        {
            \posix_kill($childPid, $signal);
        }
    }
    public function StopListening()
    {
        if ($this->_listening)
        {
            $this->_listening = false;
            socket_close($this->_socket);
        }
    }
}

