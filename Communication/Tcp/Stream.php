<?php
namespace DblEj\Communication\Tcp;

class Stream Extends MessagingSocket
{
    protected function _createSocketInstance()
    {
        return null;
    }

    protected function _writeSocket($data)
    {
        return @fwrite($this->_stream, $data);
    }

    protected function _readSocket($data)
    {
        return @stream_get_contents($this->_stream, 1024);
    }

    protected function _getLastSocketError()
    {
        return "";
    }

    public function Close()
    {
        if (isset($this->_stream) && $this->_stream)
        {
            fclose($this->_stream);
        }
        return parent::Close();
    }
}