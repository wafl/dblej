<?php
namespace DblEj\Communication\Tcp;

class Client extends MessagingSocket implements IClient
{
    private $_remotePort;

    public function Connect($remoteIp = null, $remotePort = null)
    {
        $this->_remoteIp = $remoteIp;
        $this->_remotePort = $remotePort;

        set_time_limit(0);
        ob_implicit_flush();

        if (@socket_connect($this->_socket, $this->_remoteIp, $this->_remotePort) === false) {
            throw new \Exception("@socket_connect() failed: reason: " . socket_strerror(socket_last_error($this->_socket)));
        }

        socket_set_nonblock($this->_socket);
        $this->_connected = true;
        socket_getpeername($this->_socket, $this->_remoteip);
    }
}