<?php

namespace DblEj\IO;

interface IInput
extends \DblEj\EventHandling\IEventRaiser
{

    public function __construct(IOutput $inputSignal);

    function onInput($input);
}