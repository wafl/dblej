<?php
namespace DblEj\Communication\WebSocket;

class Utils
{
    public static function GetHandshakeKey ($clientKey)
    {
        $handshakeKey = $clientKey . "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        $handshakeKey = sha1($handshakeKey, true);
        $handshakeKey = base64_encode($handshakeKey);
        return $handshakeKey;
    }

    /**
     * @param type $message
     * The message payload
     *
     * @param int $frameType
     * can be just the opcode, which will be between 1-15
     * or it can be the first header byte, which is FIN,RSV1,RSV2,RSV3,opcode (opcode is 4 bits, the rest are 1. opcode is between 0001 and 000F atm)
     * making a full byte of 1000XXXX where XXXX is the opcode (since apparently FIN always = 1 (if this is a full, non fragmented message) and RSV1, 2, and 3 always = 0 for now)
     * so the opcodes will be 0x01 through 0x0f
     * which is decimal 1 through 15
     * and the FIN+RSVs in decimal is 128
     * so therefor, the full header byte can be between 129 (opcode 1) and 143 (opcode 15) decimal
     *
     * @return string
     */
    public static function EncodeFrame($message, $frameType = 129)
    {

        //if $frameType <= 15 then it is probably just the opcode portion
        if ($frameType <= 15)
        {
            $frameType += 128;
        }
        $length = strlen($message);

        $bytesHeader = [];
        $bytesHeader[0] = $frameType; // 0x1 text frame (FIN,RSV1,RSV2,RSV3,opcode)

        if ($length <= 125) {
                $bytesHeader[1] = $length;
        } else if ($length >= 126 && $length <= 65535) {
                $bytesHeader[1] = 126;
                $bytesHeader[2] = ( $length >> 8 ) & 255;
                $bytesHeader[3] = ( $length      ) & 255;
        } else {
                $bytesHeader[1] = 127;
                $bytesHeader[2] = ( $length >> 56 ) & 255;
                $bytesHeader[3] = ( $length >> 48 ) & 255;
                $bytesHeader[4] = ( $length >> 40 ) & 255;
                $bytesHeader[5] = ( $length >> 32 ) & 255;
                $bytesHeader[6] = ( $length >> 24 ) & 255;
                $bytesHeader[7] = ( $length >> 16 ) & 255;
                $bytesHeader[8] = ( $length >>  8 ) & 255;
                $bytesHeader[9] = ( $length       ) & 255;
        }

        $str = implode(array_map("chr", $bytesHeader)) . $message;

        return $str;
    }

    public static function DecodeFrame($messageFrameString, &$opcode = null, &$isMasked = null){

        $firstByteBinary = sprintf('%08b', ord($messageFrameString[0]));
		$secondByteBinary = sprintf('%08b', ord($messageFrameString[1]));
        $opcode = bindec(substr($firstByteBinary, 4, 4));
        $isMasked = ($secondByteBinary[0] == '1') ? true : false;

        $messageFrame = array_map("ord", str_split($messageFrameString));
        $frameLength = $messageFrame[1] AND 127;
//        if(strlen($messageFrameString) < $frameLength) //the message was truncated by tcp buffers... need to wait for the rest of the message
//		{
//            print "TRUNC MESSAGE: ".strlen($messageFrameString).", $frameLength";
//			return false;
//		}

        if ($frameLength == 126)
        {
            $headerMsgLength = 4;
        }
        else if ($frameLength == 127)
        {
            $headerMsgLength = 10;
        }
        else
        {
            $headerMsgLength = 2;
        }
        $masks = array_slice($messageFrame, $headerMsgLength, 4);

        $decodedString = "";
        for ($i = $headerMsgLength + 4, $j = 0; $i < count($messageFrame); $i++, $j++ ) {
            $decodedString .= chr($messageFrame[$i] ^ $masks[$j % 4]);
        }
        return $decodedString;
    }
}