<?php
namespace DblEj\Communication\WebSocket;

/**
 * Base class for implementing an HTML5 web socket client.
 * When implemented you can override onDataReceived to deal with input.
 */
class Client extends \DblEj\Communication\Tcp\StreamClient
{
    protected $_handshakeDone = false;
    private $_serverHandshakeKey = null;
    private $_returnedHandshakeKey = null;

    private $_headers = [];
    private $_postHandshakeInputTerminator = "\n";

    public function SetHeader($headerName, $headerVal)
    {
        $this->_headers[$headerName] = $headerVal;
    }

    protected function decodeInputProtocol($input, &$metaData = null)
    {
        if ($this->_handshakeDone)
        {
            $input = Utils::DecodeFrame($input, $metaData);
        }
        return $input;
    }

    public function Connect($url = null, $remotePort = null)
    {
        if (!$url)
        {
            throw new \Exception("The web socket cliet needs a url to connect");
        }
        $parsedUrl = parse_url($url);
        $scheme = $parsedUrl["scheme"];
        $hostName = $parsedUrl["host"];
        if (!$remotePort)
        {
            $remotePort = isset($parsedUrl["port"])?$parsedUrl["port"]:null;
        }
        $endPoint = isset($parsedUrl["path"])?$parsedUrl["path"]:"/";
        $query = isset($parsedUrl["query"])?$parsedUrl["query"]:null;
        if ($query)
        {
            $endPoint .= "?$query";
        }

        $this->_postHandshakeInputTerminator = $this->_messageTerminatorIn;
        $this->_messageTerminatorIn = "\r\n";
        parent::Connect((($scheme == "wss" || $scheme == "ssl")?"ssl://":"").$hostName, $remotePort);

        //send http hello
        $reqKey = base64_encode(\DblEj\Util\Strings::GenerateRandomString(17, [], false));
        $this->_serverHandshakeKey = Utils::GetHandshakeKey($reqKey);

        $this->SendHeader("GET $endPoint HTTP/1.1");
        $this->SendHeader("Host: $hostName");
        $this->SendHeader("Upgrade: websocket");
        $this->SendHeader("Connection: Upgrade");
        $this->SendHeader("Sec-WebSocket-Key: $reqKey");
        $this->SendHeader("Sec-WebSocket-Version: 13");
        foreach ($this->_headers as $headerKey=>$headerVal)
        {
            $this->SendHeader("$headerKey: $headerVal");
        }
        $this->SendHeader("");

        //start check data loop
        $this->CheckDataLoop();
    }

    /**
     * An "event" method that will be called whenever data comes in on the socket.
     * Override this method to intercept input.
     * It is important that you call parent::onDataReceived on the first line
     * if you override this method.
     *
     * @param String $data
     * @param String $inputMetaData
     * @return void
     */
    protected function onDataReceived($data, $inputMetaData = null)
    {
        if ($data == "" && $this->_returnedHandshakeKey && !$this->_handshakeDone)
        {
            $this->_handshakeDone = true;
            $this->_messageTerminatorIn = $this->_postHandshakeInputTerminator;
        }
        elseif (!$this->_handshakeDone)
        {
            $incomingMsgLineParts = explode(":",$data);
            if (count($incomingMsgLineParts) > 1)
            {
                $parsedKey = trim(strtolower($incomingMsgLineParts[0]));
                if ($parsedKey == "sec-websocket-accept")
                {
                    $serverKey = trim($incomingMsgLineParts[1]);
                    if ($serverKey != $this->_serverHandshakeKey)
                    {
                        throw new \Exception("Invalid sec key returned from wss server, ret: $serverKey, exp: $this->_serverHandshakeKey");
                    }
                    $this->_returnedHandshakeKey = $serverKey;
                }
            }
        }
        else
        {
            $opcode = $inputMetaData;
            switch ($opcode)
            {
                case 1:
                case 2:
                    return parent::onDataReceived($data);
                    break;
                case 8:
                    //close connection
                    $this->Close();
                    break;
                case 9:
                    //ping
                    $this->SendData($data, false, 138);
                    break;
                case 10:
                    //pong
                    break;
            }
        }
    }

    public function SendHeader($headerLine)
    {
        return parent::SendData($headerLine, true);
    }

    public function SendJson($dataToBeEncoded, $appendEol = false)
    {
        $data = \DblEj\Communication\JsonUtil::EncodeJson($dataToBeEncoded);
        return parent::SendData(Utils::EncodeFrame($data), $appendEol);
    }

    public function SendData($data, $appendEol = false, $frameType = 129)
    {
        return parent::SendData(Utils::EncodeFrame($data, $frameType), $appendEol);
    }

    public function Close()
    {
        parent::Close();
    }
}
