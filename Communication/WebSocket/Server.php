<?php
namespace DblEj\Communication\WebSocket;

/**
 * Base class for implementing an HTML5 web socket server.
 * When implemented you can override onDataReceived to deal with input.
 */
class Server extends \DblEj\Communication\Tcp\Server
{
    protected $_handshakeDone = false;
    private $_clientHandshakeKey = null;
    private $_upgradeRequested = false;
    protected $_clientSessionId = null;
    protected $_sessionCookieName = "sessid";
    public function __construct($socket, $dataReceiveCallback = null, $errorCallback = null, $messageTerminator = "\r\n")
    {
        $this->_clientSessionId = null;
        $messageTerminator = "\r\n"; //force terminator to \r\n
        parent::__construct($socket, $dataReceiveCallback, $errorCallback, $messageTerminator);
    }
    protected function decodeInputProtocol($input, &$metaData = null)
    {
        if ($this->_handshakeDone)
        {
            $input = Utils::DecodeFrame($input, $metaData);
        }
        return $input;
    }

    /**
     * An "event" method that will be called whenever data comes in on the socket.
     * Override this method to intercept input.
     * It is important that you call parent::onDataReceived on the first line
     * if you override this method.
     *
     * @param String $data
     * @param String $inputMetaData
     * @return void
     */
    protected function onDataReceived($data, $inputMetaData = null)
    {
        if ($data == "" && !$this->_handshakeDone)
        {
            //end of headers, we can reply now
            if ($this->_upgradeRequested && $this->_clientHandshakeKey)
            {
                $handshakeKey = Utils::GetHandshakeKey($this->_clientHandshakeKey);

                $this->SendHeader("HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: $handshakeKey\r\n");
                $this->_handshakeDone = true;

            } else {
                $this->SendHeader("HTTP/1.1 501 Not Implemented");
                $this->SendHeader("\r\n");

                $this->_handshakeDone = true;
            }
        }
        elseif (!$this->_handshakeDone)
        {
            $incomingMsgLineParts = explode(":",$data);
            if (count($incomingMsgLineParts) > 1)
            {
                if (trim(strtolower($incomingMsgLineParts[0])) == "sec-websocket-key")
                {
                    $clientKey = trim($incomingMsgLineParts[1]);
                    $this->_clientHandshakeKey = $clientKey;
                }
                elseif ((trim(strtolower($incomingMsgLineParts[0])) == "upgrade" && trim(strtolower($incomingMsgLineParts[1])) == "websocket")||(trim(strtolower($incomingMsgLineParts[0])) == "connection" && trim(strtolower($incomingMsgLineParts[1])) == "upgrade"))
                {
                    $this->_upgradeRequested = true;
                }
                elseif (trim(strtolower($incomingMsgLineParts[0])) == "cookie")
                {
                    $cookies = explode(";", trim($incomingMsgLineParts[1]));
                    foreach ($cookies as $cookie)
                    {
                        $cookieArray = explode("=", trim($cookie));
                        $cookieName = $cookieArray[0];
                        if ($cookieName == $this->_sessionCookieName)
                        {
                            $this->_clientSessionId = urldecode($cookieArray[1]);
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            $opcode = $inputMetaData;
            switch ($opcode)
            {
                case 1:
                case 2:
                    return parent::onDataReceived($data);
                    break;
                case 8:
                    //close connection
                    $this->Close();
                    break;
                case 9:
                    //ping
                    $this->SendData($data, 138);
                    break;
                case 10:
                    //pong
                    break;
            }
        }
    }

    public function SendHeader($headerLine)
    {
        return parent::SendData($headerLine, true);
    }

    public function SendJson($dataToBeEncoded, $appendEol = false)
    {
        $data = \DblEj\Communication\JsonUtil::EncodeJson($dataToBeEncoded);
        return parent::SendData(Utils::EncodeFrame($data), $appendEol);
    }

    public function SendData($data, $appendEol = false, $frameType = 129)
    {
        return parent::SendData(Utils::EncodeFrame($data, $frameType), $appendEol);
    }

    public function Close()
    {
        if ($this->_socket)
        {
            $this->SendData("", false, 136);
        }
        parent::Close();
    }
}
