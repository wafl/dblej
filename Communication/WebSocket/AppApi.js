/*
 *@namespace DblEj.Communication.WebSocket
 */
Namespace("DblEj.Communication.WebSocket");

/*
 *@class AppApi
 *@static
 */
DblEj.Communication.WebSocket.AppApi = {
    _dataReceivedHandlers: [],
    _socketOpenHandler: null,
    _clientSocket: null,
    _useDefaultJsonParser: false,
    _waitingToSendOnOpen: [],
    _socketIsOpen: false,
    /**
     * Initialize a Web Socket client for an application
     *
     * @function {void} InitAppClient
     * 
     * @param {String} newHandlerUrl The new Url.
     * @type {String}
     *
     * @param {Boolean} useDefaultJsonParser If true then it will skip wafl json parsing and use default json.parse from browser implementation.
     * @default false
     *
     * @param {Function} socketOpenCallback
     * @default null
     */
    Initialize: function (newHandlerUrl, useDefaultJsonParser, socketOpenCallback)
    {
        DblEj.Communication.WebSocket.AppApi._useDefaultJsonParser = useDefaultJsonParser;
        DblEj.Communication.WebSocket.AppApi._socketOpenHandler = socketOpenCallback;
        DblEj.Communication.WebSocket.AppApi._clientSocket = new DblEj.Communication.WebSocket.Client(newHandlerUrl, DblEj.Communication.WebSocket.AppApi._dataReceived, DblEj.Communication.WebSocket.AppApi._socketOpened);
    },
    /**
     * Send a request to the server and wait for the response.
     *
     * @function SendString Send a asynchronous request to the server.
     *
     * @param {String} call The name of the call.
     * 
     * @param {Object} requestObject The object to pass to the server.
     * @default null
     */
    SendString: function (call, requestObject)
    {
        if (DblEj.Communication.WebSocket.AppApi._socketIsOpen)
        {
            var reqObjectJson = "";
            if (!IsDefined(requestObject) || requestObject == null)
            {
                requestObject = {};
            }
            requestObject.ApiCall = call;
            reqObjectJson = DblEj.Communication.JsonUtil.EncodeJson(requestObject);
            DblEj.Communication.WebSocket.AppApi._clientSocket.SendString(reqObjectJson+"\r\n");
        } else {
            DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen[DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen.length] = [call, requestObject];
        }
    },
    AddDataReceivedHandler: function (handler)
    {
        DblEj.Communication.WebSocket.AppApi._dataReceivedHandlers[DblEj.Communication.WebSocket.AppApi._dataReceivedHandlers.length] = handler;
    },
    _dataReceived: function (data)
    {
        var parsedData;
        var handlerIdx;
        if (this._useDefaultJsonParser)
        {
            parsedData = JSON.parse(data);
            for (handlerIdx = 0; handlerIdx < DblEj.Communication.WebSocket.AppApi._dataReceivedHandlers.length; handlerIdx++)
            {
                var handler = DblEj.Communication.WebSocket.AppApi._dataReceivedHandlers[handlerIdx];
                handler(parsedData);
            }
        } else {
            parsedData = DblEj.Communication.JsonUtil.DecodeJson(data, function(decodedData)
            {
                for (handlerIdx = 0; handlerIdx < DblEj.Communication.WebSocket.AppApi._dataReceivedHandlers.length; handlerIdx++)
                {
                    var handler = DblEj.Communication.WebSocket.AppApi._dataReceivedHandlers[handlerIdx];
                    handler(decodedData);
                }
            });
        }
    },
    _socketOpened: function()
    {
        DblEj.Communication.WebSocket.AppApi._socketIsOpen = true;
        if (DblEj.Communication.WebSocket.AppApi._socketOpenHandler)
        {
            DblEj.Communication.WebSocket.AppApi._socketOpenHandler();
        }
        if (DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen.length > 0)
        {
            for (var msgIdx=0; msgIdx < DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen.length; msgIdx++)
            {
                DblEj.Communication.WebSocket.AppApi.SendString(DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen[msgIdx][0], DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen[msgIdx][1]);
            }
        }
        DblEj.Communication.WebSocket.AppApi._waitingToSendOnOpen = [];
    }
};