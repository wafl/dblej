/*
 *@namespace DblEj.Communication.WebSocket
 */
Namespace("DblEj.Communication.WebSocket");

DblEj.Communication.WebSocket.Utils =
    {
        EncodeFrame: function(dataString){
            var bytesFormatted = new Array();
            bytesFormatted[0] = 129;
            if (dataString.length <= 125) {
                bytesFormatted[1] = dataString.length;
            } else if (dataString.length >= 126 && dataString.length <= 65535) {
                bytesFormatted[1] = 126;
                bytesFormatted[2] = ( dataString.length >> 8 ) & 255;
                bytesFormatted[3] = ( dataString.length      ) & 255;
            } else {
                bytesFormatted[1] = 127;
                bytesFormatted[2] = ( dataString.length >> 56 ) & 255;
                bytesFormatted[3] = ( dataString.length >> 48 ) & 255;
                bytesFormatted[4] = ( dataString.length >> 40 ) & 255;
                bytesFormatted[5] = ( dataString.length >> 32 ) & 255;
                bytesFormatted[6] = ( dataString.length >> 24 ) & 255;
                bytesFormatted[7] = ( dataString.length >> 16 ) & 255;
                bytesFormatted[8] = ( dataString.length >>  8 ) & 255;
                bytesFormatted[9] = ( dataString.length       ) & 255;
            }
            for (var i = 0; i < dataString.length; i++){
                bytesFormatted.push(dataString.charCodeAt(i));
            }
            return bytesFormatted;
        }
    };