/*
 *@namespace DblEj.Communication.WebSocket
 */
Namespace("DblEj.Communication.WebSocket");

/*A WebSocket client.
 * 
 *@class WebSocketClient
 *@extends Class
 */
DblEj.Communication.WebSocket.Client = Class.extend(
    {
        /**
         * @constructor
         *
         * @param {String} serverUrl
         * The Url of the server-side handler for this connection's requests.
         *
         * @param {Function} dataReceivedHandler
         * A callback to handle messages received from the server
         **/
        init: function (serverUrl, dataReceivedHandler, socketOpenedHandler)
        {
            window.AddBeforeUnloadHandler(function(event)
            {
                if (this._wsClient.readyState == 1)
                {
                    this._wsClient.close();
                }
            }.Bind(this));
            this._serverUrl = serverUrl;
            this._wsClient = new WebSocket(serverUrl);
            this._wsClient.onmessage = function (event)
            {
                if (IsDefined(dataReceivedHandler) && (dataReceivedHandler != null))
                {
                    dataReceivedHandler(event.data);
                }
            };
            if (IsDefined(socketOpenedHandler) && socketOpenedHandler)
            {
                this._wsClient.onopen = function()
                {
                    socketOpenedHandler();
                };
            }
        },
        /**
         * @method SendData
         * Send data to the WebSocket server
         * 
         * @param {String} dataString The data to send
         *
         * @param {Array} headers An array of HTTPHeader objects, one for each HTTP header to be sent along with the request.
         * @default []
         *
         * @returns {Boolean} Returns <em>true</em> if the request is successfully sent, otherwise <em>false</em>.
         */
        SendString: function (dataString)
        {
            this._wsClient.send(dataString);
            return true;
        },
        SendJson: function (jsonObject)
        {
            this.SendString(DblEj.Communication.JsonUtil.EncodeJson(jsonObject));
        }
    });