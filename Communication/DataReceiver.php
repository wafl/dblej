<?php

namespace DblEj\IO;

/**
 * Base class for a class that receives some sort of data and raises events notifying subscribers
 * when data is received.
 */
abstract class DataReceiver
extends \DblEj\EventHandling\EventRaiser
{

    /**
     * Should be called from within the class any time data is received.
     * @param mixed $data
     * @param \DblEj\IO\IOutput $sender The sender of the data.
     */
    protected function onDataReceived($data, IOutput $sender)
    {
        $this->raiseEvent(new DataReceivedEvent($data, $this, $sender));
    }

    public function GetRaisedEventTypes()
    {
        return DataReceivedEvent::EVENT_DATARECEIVED;
    }
}