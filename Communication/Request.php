<?php
namespace DblEj\Communication;

/**
 * A simple request with a string for it's contents.
 */
class Request
implements \DblEj\Communication\IRequest
{
    private $_requestString;

    /**
     * Create the Request.
     * @param $requestString The contents of the request
     */
    public function __construct($requestString)
    {
        if ($requestString == null)
        {
            $requestString = "";
        }
        $this->_requestString = $requestString;
    }

    /**
     * The contents of the request.
     *
     * @return string
     */
    public function Get_Request()
    {
        return $this->_requestString;
    }
}