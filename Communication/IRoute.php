<?php

namespace DblEj\Communication;

/**
 * An IRoute defines a destination, and provides a mechanism for sending information to that destination.
 * A destination can be any recipient of information, via any interface.
 * It is a route's job to know how to talk to the destination.
 */
interface IRoute
{
    /**
     * Send the <i>DestinationArguments</i> to the <i>Destination</i>.
     *
     * @param \DblEj\Application\IApplication $app
     * The application that received the routing request that triggered this operation.
     *
     * @return \DblEj\Communication\IResponse
     * A response that is suitable for this route's destination.
     */
    public function GotoRoute(\DblEj\Application\IApplication $app = null);

    /**
     * Allow routes to do preliminary work and return preliminary information before actually going to the route destination
     *
     * @return Response The response returned from the destination.
     */
    public function PrepareRoute(\DblEj\Application\IApplication $app = null);

    /**
     * This route's destination end-point.
     */
    public function Get_Destination();

    /**
     * The information to send to the destination.
     */
    public function Get_DestinationArguments();

    /**
     * When someone is trying to go to this route, in what role(s) should they be treated?
     * By default the traveler is represented by the current User object.
     * As an example, a website user might have multiple roles like admin, csr, seller, buyer, etc.
     * Some routes, like a home page, might only care about the traveler in the context of a regular user.
     * But some routes, say for example a seller administration section, might only be available to a seller
     * staff member for the specified seller.  That route would want to specify "Seller" (or something similar)
     * as the traveler role.  The actual value is arbitrary, except that a traveler must be aware of the value's
     * meaning to be able to act in that role/context.  A good practice is to use the allowed traveller (sub)class name(s).
     *
     * @return array An array of strings that the traveler can use to decide how (in what context / as what class) it wants to represent itself
     */
    public function Get_TravelerRoles();
}