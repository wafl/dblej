<?php
namespace DblEj\Communication;

interface IConnectedService
extends IService
{
    /**
     * Whether or not there is an active connection with this service.
     * @return boolean <i>True</i> if there is an active connection with this service, otherwise <i>false</i>.
     */
    public function IsConnected();

    /**
     * Connect to the service
     */
    public function Connect();
}