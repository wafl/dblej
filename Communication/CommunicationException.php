<?php

namespace DblEj\Communication;

/**
 * An exception for communication related operations.
 *
 * @deprecated since revision 1545 in favor of DblEj\Communication\Exception
 */
class CommunicationException
extends Exception
{
    public function __construct($message, $severity = E_WARNING, \Exception $innerException = null, $communicationTarget = "Unknown Target", $publicDetails = null, $outputCode = null)
    {
        parent::__construct($communicationTarget, $message, $outputCode, $publicDetails, $innerException, $severity);
    }
}