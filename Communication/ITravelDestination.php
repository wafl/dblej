<?php
namespace DblEj\Communication;

interface ITravelDestination
{
    public function Get_TravelerRoles();
    public static function Get_ClassTravelerRoles();
}
