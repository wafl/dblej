<?php
namespace DblEj\Communication\Http;

class PageNotFoundException
extends Exception
{
    public function __construct($requestUri, $privateDetails = "404: [PAGE_URL]", $httpErrorCode = 404, $severity = E_ERROR, $innerException = null, $errorOutputCode = null, $publicDetails = "The specified page cannot be found")
    {
        if (!$publicDetails)
        {
            $publicDetails = "You are not authorized to view the specified page or resource.";
        }
        if ($privateDetails)
        {
            $privateDetails = str_replace("[PAGE_URL]", $requestUri, $privateDetails);
        }
        $httpErrorCode = 404;
        parent::__construct($requestUri, $privateDetails, $httpErrorCode, $severity, $innerException, $errorOutputCode, $publicDetails);
    }
}