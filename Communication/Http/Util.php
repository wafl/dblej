<?php
namespace DblEj\Communication\Http;

use DblEj\Data\Validator;

/**
 * Utility class providing high level HTTP client, HTTP server, and related functionality.
 */
class Util
{
    private static $_outputSent = false;

    public static function IsInput($variableName, $inputType = Request::INPUT_GET)
    {
        $isInput = false;
        if ($inputType == INPUT_REQUEST)
        {

            //request vars not yet implemented in php
            //so check both post and get instead
            $isInput = filter_has_var(INPUT_GET, $variableName);
            if (!$isInput)
            {
                $isInput = filter_has_var(INPUT_POST, $variableName);
            }
        }
        elseif ($inputType == INPUT_SERVER)
        {
            $isInput = filter_has_var(INPUT_SERVER, $variableName);
            if (!$isInput)
            {
                $isInput = isset($_SERVER[$variableName]);
            }
        } else {
            $isInput = filter_has_var($inputType, $variableName);
        }
        return $isInput;
    }

    public static function GetInputArray($inputVariable, $inputType = Request::INPUT_GET, $validationType = Validator::VALIDATE_NONE, $defaultValue = null, $flags = FILTER_FLAG_NONE, $decimalSeperator = ".", $minAllowedValue = 0, $maxAllowedValue = PHP_INT_MAX, $regExp = null)
    {
        return self::GetInput($inputVariable, $inputType, $validationType, $defaultValue, $flags | FILTER_REQUIRE_ARRAY, $decimalSeperator, $minAllowedValue, $maxAllowedValue, $regExp);
    }

    public static function GetInput($inputVariable, $inputType = Request::INPUT_GET, $validationType = Validator::VALIDATE_NONE, $defaultValue = null, $flags = FILTER_FLAG_NONE, $decimalSeperator = ".", $minAllowedValue = 0, $maxAllowedValue = PHP_INT_MAX, $regExp = null)
    {
        $options = array("default" => null);
        switch ($validationType)
        {
            case Validator::VALIDATE_DECIMAL:
                $options["decimal"]   = $decimalSeperator;
                break;
            case Validator::VALIDATE_INTEGER:
                $options["min_range"] = $minAllowedValue;
                $options["max_range"] = $maxAllowedValue;
                break;
            case Validator::VALIDATE_REGEX:
                $options["regexp"]    = $regExp;
                break;
        }
        if ($validationType == Validator::VALIDATE_BOOLEAN)
        {
            $flags = $flags | FILTER_NULL_ON_FAILURE;
        }
        $options["options"] = $options;
        $options["flags"]   = $flags;

        if ($inputType == INPUT_REQUEST)
        {

            //request vars not yet implemented in php
            //so check both post and get instead
            $result = filter_input(INPUT_POST, $inputVariable, $validationType, $options);
            if ($result === false || $result === null)
            {
                $result = filter_input(INPUT_GET, $inputVariable, $validationType, $options);
            }
        }
        elseif ($inputType == INPUT_SERVER)
        {
            if (filter_has_var(INPUT_SERVER, $inputVariable))
            {
                $result = filter_input($inputType, $inputVariable, $validationType, $options);
            }
            elseif (isset($_SERVER[$inputVariable]))
            {
                $result = filter_var($_SERVER[$inputVariable], $validationType, $options);
            }
            else
            {
                $result = null;
            }
        }
        else
        {
            if (is_numeric($inputType))
            {
                $result = filter_input($inputType, $inputVariable, $validationType, $options);
            } else {
                throw new \Exception("Invalid input type ($inputType) sent to GetInput.  Input type should be one of: INPUT_REQUEST, INPUT_GET, INPUT_POST, or INPUT_SERVER");
            }
        }
        if ($result === false || $result === null)
        {
            $result = $defaultValue;
        }
        return $result;
    }

    public static function GetInputs($inputType = Request::INPUT_GET)
    {
        if ($inputType == INPUT_REQUEST)
        {
            $gets  = filter_input_array(INPUT_GET);
            $posts = filter_input_array(INPUT_POST);
            if (!$gets)
            {
                $gets = array();
            }
            if (!$posts)
            {
                $posts = array();
            }
            $inputs = array_merge($gets, $posts);
        }
        else
        {
            $inputs = filter_input_array($inputType);
        }
        if (!$inputs)
        {
            $inputs = [];
        }
        return $inputs;
    }

    /**
     *
     * Get text (usually a web page's html) from a web server
     * Send an HTTP request to a web server and get the raw text response (without http headers)
     * @param string $serverUrl
     * @param boolean|integer $isPostOrPut Originally this was a boolean only indicating isPost.  Integer support added where a value of 1=post and 3=put.  No other integer values are valid.  True indicates it is a Post request.  False indicates it is neither Post nor Put.
     * @param string $postFieldsOrPutBody A URL query string specifying the Post/Put variables and their values.
     * @param boolean $verifySslCertificate
     * @param boolean $verifySslHostname
     * @param string $username
     * @param string $password
     * @param boolean $followRedirects
     * @param string $cookieWriteFile
     * @param string $cookieReadFile
     * @param array $headers
     * @param boolean $isDelete
     * @param string $returnStatus
     * @param string $sslCertificateBundle
     * @param string $dnsServers host[:port][,host[:port]]...
     *
     * @return mixed
     * @throws \Exception
     * @throws \DblEj\System\MissingPhpExtensionException
     */
    public static function SendRequest($serverUrl, $isPostOrPut = false, $postFieldsOrPutBody = "", $verifySslCertificate = false, $verifySslHostname = true, $username = "", $password = "", $followRedirects = false, $cookieWriteFile = null, $cookieReadFile = null, $headers = array(), $isDelete = false, &$returnStatus = null, $sslCertificateBundle = null, $dnsServers = null, $userAgent = null, $connectionTimeout = 60)
    {
        if (function_exists("curl_init"))
        {
            if (!$userAgent)
            {
                $userAgent = "Mozilla/5.0 (".PHP_OS."; U; OS like ".PHP_OS."; en-US;) DblEj/0.2";
            }
            $userAgent = "Mozilla/5.0 (".PHP_OS."; U; OS like ".PHP_OS."; en-US;) DblEj/0.2";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $serverUrl);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connectionTimeout);

            if ($dnsServers)
            {
                curl_setopt($ch, CURLOPT_DNS_SERVERS, $dnsServers);
            }

            if ($cookieWriteFile)
            {
                curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieWriteFile);
            }
            if ($cookieReadFile)
            {
                curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieReadFile);
            }

            // Turn off the server and peer verification (TrustManager Concept).
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $verifySslCertificate?1:0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $verifySslHostname?2:0);

            if ($verifySslCertificate)
            {
                curl_setopt($ch, CURLOPT_CAINFO, $sslCertificateBundle?$sslCertificateBundle:__DIR__.DIRECTORY_SEPARATOR."cacert.pem");
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            if ($isPostOrPut === true || $isPostOrPut === Request::HTTP_POST)
            {
                curl_setopt($ch, CURLOPT_POST, 1);
            }
            elseif ($isPostOrPut == Request::HTTP_PUT)
            {
//                curl_setopt($ch, CURLOPT_PUT, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            elseif ($isPostOrPut == Request::HTTP_PATCH)
            {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
            }
            elseif ($isDelete)
            {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }
            else
            {
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
            }
            if ($postFieldsOrPutBody)
            {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFieldsOrPutBody);
            }
            if ($username != "" && $password != "")
            {
                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            }
            elseif ($username != "")
            {
                curl_setopt($ch, CURLOPT_USERNAME, "$username");
            }
            if ($followRedirects)
            {
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            }
            // Get response from the server.
            $httpResponse = curl_exec($ch);
            $returnStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($httpResponse === false)
            {
                throw new \Exception("Could not connect (or get result) from http server: " . curl_error($ch));
            }
            curl_close($ch);
            return $httpResponse;
        }
        else
        {
            throw new \DblEj\System\MissingPhpExtensionException("cUrl", "DblEj.Communication.Http.Util requires cURL.  It appears you do not have cURL installed or the PHP cUrl extension is not enabled.");
        }
    }

    /**
     * Get a file from a web server.
     * Send an HTTP request to a web server and stream the response to disk (without http headers)
     * @param string $serverUrl URL to the file you want to download
     * @param string $localDestinationPath Where you want to save the downloaded file on the local system
     * @param boolean $isPost
     * @param string $postFields
     * @param boolean $verifySslCertificate
     * @param boolean $verifySslHostname
     * @param string $username
     * @param string $password
     * @param boolean $errorIfNotFound
     * @return void
     */
    public static function DownloadFile($serverUrl, $localDestinationPath, $isPost = false, $postFields = "", $verifySslCertificate = false, $verifySslHostname = false, $username = "", $password = "", $cookieWriteFile = null, $cookieReadFile = null, $errorIfNotFound = true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serverUrl);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        if ($cookieWriteFile)
        {
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieWriteFile);
        }
        if ($cookieReadFile)
        {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieReadFile);
        }

        // Turn off the server and peer verification (TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $verifySslCertificate);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $verifySslHostname);

        $outputStream = fopen($localDestinationPath, "w");
        curl_setopt($ch, CURLOPT_FILE, $outputStream);
        if ($isPost)
        {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        }
        if ($username != "")
        {
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // Get response from the server.
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        fclose($outputStream);

        if ($httpCode == 404)
        {
            if (file_exists($localDestinationPath))
            {
                unlink($localDestinationPath);
            }
            if ($errorIfNotFound)
            {
                throw new \Exception("HTTP Download 404 for url $serverUrl");
            }
        }
    }

    public static function SetResponseHeader($header, $value)
    {
        header("$header: $value");
    }

    /**
     * Serves a file to an HTTP client
     * @param string $filePath
     * @param string $mimeType The mime type that will be reported in the Content-Type HTTP Header
     * @param bool $forceDownload Instruct the client (usually a web browser) that the file shoule be downloaded, as opposed to displaying the content
     * @param bool $allowCache
     * @param string $headerFileName The name of the file as it will be reported by the Content-Disposition Filename HTTP Header
     * @param int $cacheTimeoutSecs
     * @param bool $requireCacheRevalidation
     * @param string $overrideEtag The string to send for the ETAG HTTP Header
     * @return void
     */
    public static function ServeFile($filePath, $mimeType, $forceDownload = false, $allowCache = true, $headerFileName = null, $cacheTimeoutSecs = 600, $requireCacheRevalidation = true, $overrideEtag = null)
    {
        if (!file_exists($filePath))
        {
            throw new \Exception("Specified filePath does not exist");
        }
        $filename              = $headerFileName ? $headerFileName : basename($filePath);
        $lastModifiedTimestamp = filemtime($filePath);
        if (self::DoesClientAlreadyHaveTheLatest($lastModifiedTimestamp))
        {
            http_response_code(304);
        }
        else
        {
            while (ob_get_level() > 0)
            {
                ob_end_clean();
            }
            self::ServeContent("file:".$filePath, $mimeType, $filename, true, $lastModifiedTimestamp, $forceDownload, $allowCache, $cacheTimeoutSecs, $requireCacheRevalidation, $overrideEtag);
        }
    }

    /**
     * Serves any kind of file content to HTTP client
     * @param string $content The content to serve
     * @param string $mimeType The mime type that will be reported in the Content-Type HTTP Header
     * @param string $filename The name of the file as it will be reported by the Content-Disposition Filename HTTP Header
     * @param bool $isBinary
     * @param int $lastModifiedTimestamp the timestamp (an integer, the unix epoch) of the last modified date/time for this file
     * @param bool $forceDownload Instruct the client (usually a web browser) that the content shoule be downloaded, as opposed to being displayed
     * @param bool $allowCache
     * @param int $cacheTimeoutSecs
     * @param bool $requireCacheRevalidation
     * @param string $overrideEtag The string to send for the ETAG HTTP Header
     * @return void
     */
    public static function ServeContent($content, $mimeType, $filename = null, $isBinary = false, $lastModifiedTimestamp = null, $forceDownload = false, $allowCache = true, $cacheTimeoutSecs = 600, $requireCacheRevalidation = true, $overrideEtag = null)
    {
        if ($mimeType)
        {
            header("Content-Type: $mimeType");
        }
        if ($isBinary)
        {
            header("Content-Transfer-Encoding: binary");
        }

        if (!$lastModifiedTimestamp)
        {
            $lastModifiedTimestamp = time();
        }
        self::SendLastModified($lastModifiedTimestamp);
        if ($allowCache)
        {
            if ($requireCacheRevalidation)
            {
                self::AllowAllCachingWithRevalidation($cacheTimeoutSecs);
            }
            else
            {
                self::AllowAllCachingWithoutRevalidation($cacheTimeoutSecs);
            }
        }
        else
        {
            self::DontAllowAnyCaching();
        }
        if ($forceDownload)
        {
            header("Content-Disposition: attachment; filename=\"$filename\";");
        }
        else
        {
            header("Content-Disposition: inline; filename=\"$filename\";");
        }

        //dont use buffering if sending a file
        if ((ob_get_level() > 0) && (substr($content, 0, 5) == "file:"))
        {
            while (ob_get_level() > 0)
            {
                ob_end_clean();
            }
        }
        if (ob_get_level() > 0)
        {
            print $content;
            $bufferContents = "";
            $bufferSize     = 0;
            while (ob_get_level() > 0)
            {
                $bufferSize += ob_get_length();
                $bufferContents .= ob_get_clean();
            }
            header("Content-Length: $bufferSize");
            print $bufferContents;
        }
        else
        {
            if (substr($content, 0, 5) == "file:")
            {
                $content = substr($content, 5);
                $filesize = filesize($content);
                header("Content-Length: $filesize");
                readfile($content);
            } else {
                $filesize = strlen($content);
                header("Content-Length: $filesize");
                print $content;
            }
        }
        self::$_outputSent = true;
    }

    public static function Get_HasOutputBeenSent()
    {
        return self::$_outputSent;
    }

    /**
     * Send a HTTP "location" header to the client, instructing them to redirect to a new url.
     * Note that calling this method does not necessaily end execution of your script and there fore you should call exit() immiedatly after callig this method if you do not wish for you script to continue.
     * @param string $url The fully qualified url of the new location to redirect to (example: http://www.dlcware.com)
     * @param $autoDie [DEPRECATED] this option no longer does anything
     * @return void
     */
    public static function HeaderRedirect($url, $autoDie = false, $isPermanent = false, $maintainRequestMethod = false)
    {
        $reposneCode = 301;
        if ($isPermanent)
        {
            if ($maintainRequestMethod)
            {
                //$reposneCode = 308;  lacking browser support
                $reposneCode = 301;
            }
            else
            {
                $reposneCode = 301;
            }
        }
        else
        {
            if ($maintainRequestMethod)
            {
                //$reposneCode = 307;lacking browser support
                $reposneCode = 302;
            }
            else
            {
                //$reposneCode = 303;lacking browser support
                $reposneCode = 302;
            }
        }
        http_response_code($reposneCode);
        header("location: $url", true, $reposneCode);
        if ($autoDie)
        {
            die();
        }
    }

    /**
     * Use gzip compression on all data output to the HTTP client (usually a web browser), only if the HTTP client has sent us headers indicating that it supports gziop compression (HTTP_ACCEPT_ENCODING).
     * Note that this must be called before any other output is sent to the HTTP client (any prints, echos, or any other output)
     * @return void
     */
    public static function ZipOutputIfBrowserSupported()
    {
        return ob_start("ob_gzhandler"); //php automatically checks the server headers so we dont need to (we were checking them before)
    }

    /**
     * Send HTTP headers instructing the HTTP client (usually a web browser),
     * and any proxies in-between,
     * that they may cache whatever they wish and never need to check if the cache is stale (until it expires).
     * @param int $maxAgeSeconds Instruct the client (and proxies) that it must reload the data from the server after it's cache is this old
     * @return void
     */
    public static function AllowAllCachingWithoutRevalidation($maxAgeSeconds = 600)
    {
        header("cache-control: public,max-age=$maxAgeSeconds");
    }

    /**
     * Send HTTP headers instructing the HTTP client (usually a web browser),
     * and any proxies in-between,
     * that they may cache whatever they wish, but must check if the cache is stale whenever it is displayed.
     * @param int $maxAgeSeconds Instruct the client (and proxies) that it must reload the data from the server after it's cache is this old
     * @return void
     */
    public static function AllowAllCachingWithRevalidation($maxAgeSeconds = 600)
    {
        header("cache-control: public,max-age=$maxAgeSeconds, must-revalidate");
    }

    /**
     * Send HTTP headers instructing the HTTP client (usually a web browser),
     * that they may cache whatever they wish, but must check if the cache is stale whenever it is displayed.
     * Instructs proxies in-between the server and the client not to cache anything.
     * @param int $maxAgeSeconds Instruct the client that it must reload the data from the server after it's cache is this old
     * @return void
     */
    public static function DontAllowProxyCaching($maxAgeSeconds = 600)
    {
        if (!headers_sent())
        {
            header("cache-control: private,max-age=$maxAgeSeconds, must-revalidate");
            header("expires: -1");
            header('Pragma: no-cache');
        }
    }

    /**
     * @deprecated
     *
     * Send HTTP headers instructing the HTTP client (usually a web browser),
     * and any proxies in-between,
     * that they may not cache any of the returned content in this response
     * @return void
     */
    public static function DontAllowAnyCaching()
    {
        if (!headers_sent())
        {
            header("cache-control: max-age=0, no-store, no-cache, must-revalidate");
            header("expires: -1");
            header('Pragma: no-cache');
        }
    }

    /**
     * Send an HTTP header telling the client the last time that this file (or response) has changed
     * @param int $lastModifiedTimeStamp The timestamp (integer, unix epoch)
     * @return void
     */
    public static function SendLastModified($lastModifiedTimeStamp)
    {
        $last_modified = substr(date("r", $lastModifiedTimeStamp), 0, -5) . "GMT";
        header("Last-Modified: $last_modified");
    }

    public static function DoesClientAlreadyHaveTheLatest($latestTimeStamp, &$etag = null)
    {
        $fileStaleToTheCaller = false;
        $last_modified        = substr(date("r", $latestTimeStamp), 0, -5) . "GMT";
        if (!$etag)
        {
            $etag = md5($last_modified);
        }
        $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ?
        stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) :
        false;
        $if_none_match     = isset($_SERVER['HTTP_IF_NONE_MATCH']) ?
        stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) :
        false;
        if (!$if_modified_since && !$if_none_match)
        {
            $fileStaleToTheCaller = true;
        }
        // At least one of the headers is there - check them
        if ($if_none_match && ($if_none_match != $etag))
        {
            $fileStaleToTheCaller = true; // etag is there but doesn't match
        }
        if ($if_modified_since && ($if_modified_since != $last_modified))
        {
            $fileStaleToTheCaller = true; // if-modified-since is there but doesn't match
        }
        return !$fileStaleToTheCaller;
    }

    public static function GetClientIpAddress()
    {
        $returnIp = "";
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            $returnIp = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            $returnIp = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["REMOTE_ADDR"]))
        {
            $returnIp = $_SERVER["REMOTE_ADDR"];
        }
        else
        {
            $returnIp = "127.0.0.1";
        }
        return $returnIp;
    }

    /**
     * Create a url-encoded http query string from an array.
     * @param array $args An associative array where each element key is a variable name and the element is the value.
     *
     * @return string The query string
     */
    public static function MakeQueryString($args)
    {
        return http_build_query($args);
    }
}