<?php

namespace DblEj\Communication\Http;

/**
 * An HTTP response that includes a SitePage to be rendered and sent to the client.
 */
interface ISitePageResponse
extends IResponse
{

    /**
     * The SitePage to be rendered and sent to the client.
     */
    public function Get_SitePage();

    public static function Set_DefaultCachepoint($cachePoint);
}