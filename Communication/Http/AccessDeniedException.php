<?php
namespace DblEj\Communication\Http;

class AccessDeniedException
extends Exception
{
    public function __construct($requestUri, $privateDetails = "", $httpErrorCode = 403, $severity = E_ERROR, $innerException = null, $errorOutputCode = null, $publicDetails = null)
    {
        if (!$publicDetails)
        {
            $publicDetails = "You are not authorized to view the specified page or resource.";
        }
        parent::__construct($requestUri, $privateDetails, $httpErrorCode, $severity, $innerException, $errorOutputCode, $publicDetails);
    }
}