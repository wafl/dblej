<?php
namespace DblEj\Communication\Http;

/**
 * Thrown when there is an unexpected event during an HTTP communication.
 */
class Exception
extends \DblEj\Communication\Exception
{
    private $_httpErrorCode = 500;
    public function __construct($requestUri, $errorMessage = "", $httpErrorCode = 500, $severity = E_ERROR, \Exception $innerException = null, $errorOutputCode = null, $publicDetails = null)
    {
        if (!$errorOutputCode)
        {
            $errorOutputCode = $httpErrorCode;
        }
        $this->_httpErrorCode = $httpErrorCode;
        if (!$publicDetails)
        {
            switch ($httpErrorCode)
            {
                case 404:
                    $publicDetails = "The document <i>$requestUri</i> cannot be found";
                    break;
                default:
                    $publicDetails = "There was a server error while handling the request.";
                    \DblEj\Logging\Tracing::TraceWarning("unknown error sent to user. ".$errorMessage.print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), true).($innerException?" ".$innerException->getTraceAsString():""), $innerException?$innerException->getFile():"", $innerException?$innerException->getLine():"");
                    break;
            }
        }
        parent::__construct($requestUri, $errorMessage, $errorOutputCode, $publicDetails, $innerException, $severity);
    }

    public function Get_HttpErrorCode()
    {
        return $this->_httpErrorCode;
    }

    /**
     * The arbitrary string that the application should use to determine the correct output to send for this exception.
     *
     * When the application responds to a client request with this exception it will use ErrorOutputCode to determine the type of output to generate.
     * For example, you might use standard http codes, and a 404 might result in a customized "Page Not Found" HTML page.
     * Or you might want to use non-standard codes for error groupings or for things not well defined by HTTP (or for any other reason).
     * For example you might use the string "LoginRequired" which might be sent with a 403, 200, 500, or other HTTP result.
     * Rather than defining the same output type for each of the possible results, the application would
     * send the same type of output for each of these HTTP results based on the common ErrorOutputCode.
     * Whatever value you use, it is up to the application to interpret the code and send the correct output.
     * Applications should fallback to using the HttpErrorCode if they do not understand the ErrorOutputCode or the ErrorOutputCode is empty.
     *
     * @return string
     */
    public function Get_ErrorOutputCode()
    {
        return parent::Get_ErrorOutputCode();
    }
}