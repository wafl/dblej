<?php
namespace DblEj\Communication\Http;

/**
 * A response to a DblEj style API request.
 */
class ApiResponse
extends Response
{
    private $_apiCall = null;

    public function __construct($apiCall, $responseString, $responseCode = self::HTTP_OK_200, $headers = null)
    {
        $this->_apiCall = $apiCall;
        parent::__construct($responseString, $responseCode, $headers, self::CONTENT_TYPE_PRINTABLE_OUTPUT);
    }

    /**
     * The API Call that this response is in response to.
     *
     * @return string
     */
    public function Get_ApiCall()
    {
        return $this->_apiCall;
    }

    public function ReindexResponseArray()
    {
        if (is_array($this->_content))
        {
            $this->_content = array_values($this->_content);
        }
    }
}