<?php
namespace DblEj\Communication\Http;

use DblEj\Application\IApplication,
    DblEj\Application\IWebApplication,
    DblEj\Communication\IRequest,
    DblEj\Communication\IRequestHandler,
    DblEj\Data\IModel,
    DblEj\Communication\Http\SitePageResponse,
    DblEj\Presentation\RenderOptions,
    DblEj\SiteStructure\SitePage;

/**
 * A base class for HTTP request handlers.
 */
abstract class RequestHandlerBase
implements IRequestHandler
{
    protected $_travelerRoles;

    public static function Get_ClassTravelerRoles()
    {
        return ["\\DblEj\\Authentication\\IUser", "\\DblEj\\Authentication\\IUserGroup"];
    }
    public function Get_TravelerRoles()
    {
        if (!$this->_travelerRoles)
        {
            $this->_travelerRoles = static::Get_ClassTravelerRoles();
        }
        return $this->_travelerRoles;
    }
    public static function Set_TravelerRoles($travelerRoles)
    {
        $this->_travelerRoles = $travelerRoles;
    }

    /**
     * By overriding this method, you can customize the HTTP headers that are sent prior to your response.
     * If you want to wait and not send the headers until your response is ready, override this method and return null;
     * If you want to return a default http 200 response, you can leave this method to its default behavior (in that case, do not override)
     *
     * @param \DblEj\Communication\Http\Request $request
     * @param type $requestStringOrObject
     * @param IWebApplication $app
     *
     * @return \DblEj\Communication\Http\Response
     */
    public function PrepareHttpResponse(Request $request, $requestStringOrObject, IWebApplication $app)
    {
        return new Response();
    }

    public function PrepareResponse($requestString, IRequest $request, IApplication $app)
    {
        return $this->PrepareHttpResponse($request, $requestString, $app);
    }

    /**
     * Handle a request.
     *
     * This implementation handles requests that come in via the IRequestHandler interface.
     * It is a wrapper for the <i>HandlerHttpRequest</i> method.
     *
     * @param string $requestString A string identifying what is being requested.
     * @param \DblEj\Communication\IRequest $request The request that is being handled.
     * @param IApplication $app The application that received the initial request.
     * @return Response An appropriate response for the given request.
     */
    public function HandleRequest($requestString, IRequest $request, IApplication $app)
    {
        return $this->HandleHttpRequest($request, $requestString, $app);
    }

    /**
     * Handle an HTTP request.
     *
     * @param Request $request The request that is being handled.
     * @param string $requestStringOrObject A string or object identifying what is being requested.
     * @param IWebApplication $app The application that received the initial request.
     *
     * @return Response An appropriate response for the given request.
     */
    abstract public function HandleHttpRequest(Request $request, $requestStringOrObject, IApplication $app);

    /**
     * Create a default response for the given request.
     *
     * @param Request $request
     * @param IWebApplication $app
     * @param IModel $dataModel
     * @param RenderOptions $options
     * @param Headers $headers
     * @return SitePageResponse
     */
    protected function createResponseFromRequest(Request $request, IWebApplication $app, IModel $dataModel = null, RenderOptions $options = null, Headers $headers = null)
    {
        $currentSitePage = $app->GetSitePageByRequest($request, null);
        if ($currentSitePage)
        {
            if ($dataModel)
            {
                $currentSitePage->Set_Model($dataModel);
            }
            return $this->createResponseFromSitePage($currentSitePage, $options ? $options : new RenderOptions(), $headers);
        }
        else
        {
            throw new \Exception("There is no page to show for this request");
        }
    }

    /**
     * Create a default response that returns the specified SitePage.
     *
     * @param SitePage $currentSitePage
     * @param RenderOptions $options
     * @param Headers $headers
     * @return SitePageResponse
     */
    protected function createResponseFromSitePage(SitePage $currentSitePage, RenderOptions $options, Headers $headers = null)
    {
        $httpResponse = new SitePageResponse($currentSitePage, Response::HTTP_OK_200, $headers, $options);
        return $httpResponse;
    }

    /**
     * Create a response that instructs the client to redirect to a different URL.
     *
     * @param string $url
     * @param boolean $isPermanent
     * @param boolean $maintainHttpMethod
     * @return Response
     */
    protected function createRedirectUrlResponse($url, $isPermanent = false, $maintainHttpMethod = false)
    {
        $httpResponse = new Response();
        $httpResponse->Set_RediredctUrl($url, $isPermanent, $maintainHttpMethod);
        return $httpResponse;
    }
}