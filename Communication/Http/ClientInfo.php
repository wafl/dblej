<?php
namespace DblEj\Communication\Http;

class ClientInfo
{
    private $_title = "Generic";
    private $_version = "N/A";
    private $_deviceType = "N/A";
    private $_hostOs = "N/A";

    const DEVICE_TYPE_GENERIC = 0;
    const DEVICE_TYPE_DESKTOP = 10;
    const DEVICE_TYPE_TABLET = 20;
    const DEVICE_TYPE_PHONE = 21;
    const DEVICE_TYPE_MOBILE = 29;

    public function __construct($title, $version, $deviceType, $hostOs)
    {
        $this->_title = $title;
        $this->_version = $version;
        $this->_deviceType = $deviceType;
        $this->_hostOs = $hostOs;
    }

    public function Get_Title()
    {
        return $this->_title;
    }
    public function Get_Version()
    {
        return $this->_version;
    }
    public function Get_DeviceType()
    {
        return $this->_deviceType;
    }
    public function GetDeviceTypeString()
    {
        switch ($this->_deviceType)
        {
            case self::DEVICE_TYPE_DESKTOP:
                return "Desktop";
                break;
            case self::DEVICE_TYPE_TABLET:
                return "Tablet";
                break;
            case self::DEVICE_TYPE_PHONE:
                return "Phone";
                break;
            case self::DEVICE_TYPE_MOBILE:
                return "Mobile Device";
                break;
            default:
                return "Generic";
                break;
        }
    }
    public function Get_HostOs()
    {
        return $this->_hostOs;
    }
    public static function GetFromUserAgent($userAgentString)
    {
        $capabilities = \DblEj\Util\WebBrowser::GetCapabilities($userAgentString);
        switch ($capabilities["device_type"])
        {
            case "Desktop":
                $deviceType = self::DEVICE_TYPE_DESKTOP;
                break;
            case "Tablet":
                $deviceType = self::DEVICE_TYPE_TABLET;
                break;
            case "Mobile Device":
                $deviceType = self::DEVICE_TYPE_MOBILE;
                break;
            case "Mobile Phone":
                $deviceType = self::DEVICE_TYPE_PHONE;
                break;
            default:
                $deviceType = self::DEVICE_TYPE_GENERIC;
                break;
        }
        return new ClientInfo($capabilities["browser"], $capabilities["version"], $deviceType, $capabilities["platform"]);
    }
}