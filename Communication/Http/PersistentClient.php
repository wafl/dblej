<?php
namespace DblEj\Communication\Http;

class PersistentClient
extends \DblEj\Communication\Tcp\StreamClient
implements IClient
{
    protected $_serverUrl;
    protected $_httpUsername = null;
    protected $_httpPassword = null;
    private $_handshakeDone = false;

    public function __construct($serverUrl, $httpUsername = null, $httpPassword = null, $cookieWriteFile = null, $cookieReadFile = null)
    {
        $this->_serverUrl       = $serverUrl;
        $this->_cookieReadFile  = $cookieReadFile;
        $this->_cookieWriteFile = $cookieWriteFile;
        $this->_httpUsername    = $httpUsername;
        $this->_httpPassword    = $httpPassword;

        parent::__construct(null, null, null, ["\r\n", "\r\n"], false);
    }

    private $_headers = [];
    public function SetHeader($headerName, $headerVal)
    {
        $this->_headers[$headerName] = $headerVal;
    }
    public function SendHeader($headerLine)
    {
        return $this->SendData($headerLine, true);
    }
    
    public function SendData($data, $appendEol = false) {
        print "Sending $data...";
        $returnVal = parent::SendData($data, $appendEol);
        print "done\n";
        return $returnVal;
    }
    public function Connect($remoteIp = NULL, $remotePort = NULL)
    {
        $url = parse_url($this->_serverUrl);
        $hostUri = $url["host"];
        $port = isset($url["port"])?$url["port"]:null;
        if (!$port)
        {
            switch (isset($url["scheme"])?$url["scheme"]:"http")
            {
                case "https":
                    $port = 443;
                    break;
                default:
                    $port = 80;
                    break;
            }
        }
        $returnVal = parent::Connect($hostUri, $port);
        return $returnVal;
    }

    protected function onDataReceived($data, $inputMetaData = null)
    {
        parent::onDataReceived($data, $inputMetaData);
        if (!$this->_handshakeDone)
        {
            //parse http headers
            $headerLines = explode("\r\n", $data);
            foreach ($headerLines as $headerLine)
            {
                $headerArray = explode($headerLine, ":");
                if (count($headerArray) >= 2)
                {
                    $headerKey = $headerArray[0];
                    $headerVal = $headerArray[1];
                }
            }
            if ($data == "")
            {
                $this->_handshakeDone = true;
            }
        } else {
            $this->onInput($data);
        }
    }

    /**
     * The URL of the HTTP server.
     * @return type
     */
    public function Get_ServerUrl()
    {
        return $this->_serverUrl;
    }

    public function Del($fileOrResource, $httpHeaders = array(), &$httpResultCode = null)
    {

    }
    protected function _processInBuffer() {
        if ($this->_inBuffer)
        {
            print "buffer in: ".$this->_inBuffer."\n";
        }
        return parent::_processInBuffer();
    }
    public function Get($fileOrResource, $httpHeaders = null, &$httpResultCode = null)
    {
        $url = parse_url($this->_serverUrl);
        $hostUri = $url["host"];
        $this->SendHeader("GET $fileOrResource HTTP/1.1");
        $this->SendHeader("Host: $hostUri");
        foreach ($this->_headers as $headerKey=>$headerVal)
        {
            $this->SendHeader("$headerKey: $headerVal");
        }
        $this->SendHeader("");
        
        if (is_array($httpHeaders))
        {
            foreach ($httpHeaders as $httpHeadersKey=>$httpHeaderVal)
            {
                $this->SendHeader("$httpHeadersKey: $httpHeaderVal");
            }
        }
        $this->SendHeader("");    
    }

    public function Post($fileOrResource, $postPayload = null, $httpHeaders = null, &$httpResultCode = null)
    {
        $url = parse_url($this->_serverUrl);
        $hostUri = $url["host"];
        $this->SendHeader("POST $fileOrResource HTTP/1.1");
        $this->SendHeader("Host: $hostUri");
        foreach ($this->_headers as $headerKey=>$headerVal)
        {
            $this->SendHeader("$headerKey: $headerVal");
        }
        $this->SendHeader("");
        
        if (is_array($httpHeaders))
        {
            foreach ($httpHeaders as $httpHeadersKey=>$httpHeaderVal)
            {
                $this->SendHeader("$httpHeadersKey: $httpHeaderVal");
            }
        }
        $this->SendHeader("");
    }

    public function Put($fileOrResource, $putPayload, $httpHeaders = null, &$httpResultCode = null)
    {

    }
}