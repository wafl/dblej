<?php
namespace DblEj\Communication\Http;

/**
 * Represents a response to an HTTP request.
 */
interface IResponse
extends \DblEj\Communication\IResponse
{

    /**
     * The raw response contents
     * @param string $rawResponse
     */
    public function Set_RawResponse($rawResponse);

    /**
     * The raw response contents
     * @return string
     */
    public function Get_RawResponse();

    /**
     * The ETag header value.
     * @param string $etag
     */
    public function Set_ETag($etag);

    /**
     * The HTTP body.
     *
     * @return string
     */
    public function Get_Content();

    /**
     * The HTTP body.
     *
     * @param string $content
     */
    public function Set_Content($content);

    /**
     * A key identifying the response's content type.
     *
     * @return string
     */
    public function Get_ContentType();

    /**
     * A key identifying the response's content type.
     *
     * @param string $contentType
     */
    public function Set_ContentType($contentType);

    /**
     * The HTTP response code.
     *
     * @return int
     */
    public function Get_ResponseCode();

    /**
     * The HTTP response code.
     *
     * @param int $responseCode
     */
    public function Set_ResponseCode($responseCode);

    /**
     * Whether or not the response content is binary data.
     * @param boolean $isBinary
     */
    public function Set_IsBinaryFile($isBinary);

    /**
     * The MIME type of the response's contents.
     * @param string $mimeType
     */
    public function Set_MimeType($mimeType);

    /**
     * The MIME type of the response's contents.
     * @return string $mimeType
     */
    public function Get_MimeType();

    /**
     * The last time that the contents were modified, as a unix time stamp.
     * @param int $lastModifiedTimestamp
     */
    public function Set_LastModifiedTime($lastModifiedTimestamp = null);

    /**
     * Cache instructions for the client receiving this response.
     *
     * @param string $allowableCachepoint A cachepoint string from Headers::CACHEPOINT_* constants.
     * @param type $cacheSeconds How long the cachepoint is allowed to cache the response.
     * @param type $cachepointMustRevalidate Whether or not the client has to revalidate it's cache contents to determine if it is stale.
     */
    public function Set_BrowserCacheTimeout($allowableCachepoint = Headers::CACHEPOINT_BROWSER, $cacheSeconds = 86400, $cachepointMustRevalidate = false);

    /**
     * @param string $filename
     * The filename that should be used when saving the response contents to disk or similar media.
     *
     * @param boolean $forceDownload
     * Whether or not to instruct the client to force a download of the contents as a file.
     */
    public function Set_Filename($filename, $forceDownload = false);

    /**
     * Tell the client to go to a different Url to get the response for this request.
     *
     * @param string $url The Url that the client should send the request.
     * @param boolean $isPermanent Whether or not this is a permanent condition.
     * @param type $maintainHttpMethod Whether or not the same HTTP method must be used when sending the request to the redirect url.
     */
    public function Set_RediredctUrl($url, $isPermanent = false, $maintainHttpMethod = false);

    /**
     * Returns a hash that can be used to identify this response instance
     */
    public function Get_Etag();

    public function Get_ContentIsSymbolic();
}