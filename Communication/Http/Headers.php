<?php
namespace DblEj\Communication\Http;

/**
 * A structure-like object used to store a "set" of HTTP headers.
 * @since 770
 */
class Headers
{
    /**
     * internally we'll just use a HeaderCollection
     * @var HeaderCollection
     */
    private $_headers;

    /**
     * Responses should be cached at the browser level
     */
    const CACHEPOINT_BROWSER = "CACHEPOINT_BROWSER";

    /**
     * Responses should be cached at the proxy level
     */
    const CACHEPOINT_PROXY = "CACHEPOINT_PROXY";

    /**
     * Responses should be cached at the no level (don't cache)
     */
    const CACHEPOINT_NONE = "CACHEPOINT_NONE";

    /**
     * Create an instance of the Headers class,
     * initializing it with the passed arguments.
     *
     * @param string $contentType The valid mime type to be used as the value for the HTTP Content-Type header.
     * @param type $allowableCachepoint A cachepoint string from Headers::CACHEPOINT_* constants to be used in the HTTP cache control headers.
     * @param type $cacheSeconds How long the cachepoint is allowed to cache the response, to be used as part of the HTTP cache timeout headers.
     * @param type $cachepointMustRevalidate Whether or not the client has to revalidate it's cache contents to determine if it is stale, to be used as part of the HTTP cache control headers.
     * @param type $lastModifiedTimeStamp The unix timestamp of the time that the contents of the HTTP response were last modified.
     */
    public function __construct($contentType = "text/html", $allowableCachepoint = self::CACHEPOINT_BROWSER, $cacheSeconds = 86400, $cachepointMustRevalidate = false, $lastModifiedTimeStamp = null)
    {
        $this->_headers = new HeaderCollection();
        $this->Set_ContentType($contentType);
        $this->Set_ClientCaching($allowableCachepoint, $cacheSeconds, $cachepointMustRevalidate);
        $this->Set_LastModified($lastModifiedTimeStamp);
    }

    /**
     * Set the mime type of the returned data.
     * @param string $mimeType A valid mime type.
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_ContentType($mimeType = "text/html")
    {
        $this->_headers->SetHeader("Content-Type", $mimeType);
        return $this;
    }

    public function Set_ContentDisposition($fileName, $forceDownload = false)
    {
        if ($forceDownload)
        {
            $this->_headers->SetHeader("Content-Disposition", "attachment; filename=\"$fileName\";");
        }
        else
        {
            $this->_headers->SetHeader("Content-Disposition", "inline; filename=\"$fileName\";");
        }
    }
    /**
     * Is the data contained in the response binary data such as an image file?
     * @param boolean $isBinary
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_IsBinary($isBinary = false)
    {
        if ($isBinary)
        {
            $this->_headers->SetHeader("Content-Transfer-Encoding", "binary");
        }
        elseif ($this->_headers->ContainsKey("Content-Transfer-Encoding"))
        {
            $this->_headers->RemoveItemByKey("Content-Transfer-Encoding");
        }
        return $this;
    }

    /**
     * Sets the content-disposition header, which contains the file's name,
     * and whether or not you want to download the file.
     *
     * @param boolean $forceFileDownload Whether or not you want the client to download the response data to
     * it's file-system (as opposed to displaying it for immediate view).
     * Set this to true to tell browsers to pop-up the Save File Dialog.
     *
     * @param string $fileName The file name that will be provided to the client.
     * You should not include the path.
     *
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_ForceDownload($forceFileDownload = false, $fileName = null)
    {
        $this->Set_Filename($fileName, $forceFileDownload);
        return $this;
    }

    /**
     * Sets the content-disposition header, which contains the file's name,
     * and whether or not you want to download the file.
     *
     * @param string $fileName The file name that will be provided to the client.
     * You should not include the path.
     *
     * @param boolean $forceFileDownload Whether or not you want the client to download the response data to
     * it's file-system (as opposed to displaying it for immediate view).
     * Set this to true to tell browsers to pop-up the Save File Dialog.
     *
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_Filename($fileName, $forceFileDownload = false)
    {
        $headerString = "";
        if ($forceFileDownload)
        {
            $headerString = "attachment;";
        }
        else
        {
            $headerString = "inline;";
        }
        if ($fileName)
        {
            $headerString.="filename=\"$fileName\";";
        }
        $this->_headers->SetHeader("Content-Disposition", $headerString);
        return $this;
    }

    /**
     * Sets the HTTP content-length header.
     * This tells the client how many bytes of data is.
     *
     * @param int $bytes The number of bytes
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_ContentLength($bytes)
    {
        $this->_headers->SetHeader("Content-Length", $bytes);
        return $this;
    }

    /**
     * Sets HTTP headers that instruct the client on if, and how to, cache the data
     *
     * @param string $allowableCachepoint At what point is caching allowed?
     * can be set to Headers::CACHEPOINT_BROWSER, Headers::CACHEPOINT_PROXY, or Headers::CACHEPOINT_NONE
     *
     * @param int $cacheSeconds The number of seconds the cache point should store the data
     *
     * @param type $cachepointMustRevalidate Whether or not the cache point has to revalidate
     * caches by sending last-modified header requests to the server, or if it can always assume the cache is good until it expires
     *
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_ClientCaching($allowableCachepoint = self::CACHEPOINT_BROWSER, $cacheSeconds = 86400, $cachepointMustRevalidate = false)
    {
        if ($allowableCachepoint == self::CACHEPOINT_BROWSER)
        {
            $cacheControl = "public";
            if ($this->_headers->ContainsKey("pragma"))
            {
                $this->_headers->RemoveHeader("pragma");
            }
            if (!headers_sent())
            {
                header_remove("pragma");
            }
        }
        elseif ($allowableCachepoint == self::CACHEPOINT_PROXY)
        {
            $cacheControl = "private";
            if ($this->_headers->ContainsKey("pragma"))
            {
                $this->_headers->RemoveHeader("pragma");
            }
            if (!headers_sent())
            {
                header_remove("pragma");
            }
        }
        else
        {
            $this->_headers->SetHeader("pragma", "no-cache");
            $cacheControl = "no-cache, no-store";
            $cacheSeconds = 0;
        }

        $mustRevalidateString = $cachepointMustRevalidate ? ", must-revalidate" : "";

        $this->_headers->SetHeader("expires", substr(date("r", time()-date('Z')+$cacheSeconds), 0, -5) . "GMT");
        $this->_headers->SetHeader("cache-control", "$cacheControl, max-age=$cacheSeconds" . $mustRevalidateString);
        return $this;
    }

    /**
     * Set the last modified time of the document or data
     *
     * @param int $lastModifiedTimeStamp a Unix timestamp measured in seconds (number of seconds since Unix epoch)
     *
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_LastModified($lastModifiedTimeStamp)
    {
        $lastModifiedString = substr(date("r", $lastModifiedTimeStamp-date('Z')), 0, -5) . "GMT";
        $this->_headers->SetHeader("Last-Modified", $lastModifiedString);
        if (!$this->_headers->ContainsKey("Etag"))
        {
            $this->Set_Etag(md5($lastModifiedString));
        }
        return $this;
    }

    /**
     * The ETag HTTP header.
     *
     * @param string $etag The ETag string.
     *
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_Etag($etag)
    {
        if (!$etag && $this->_headers->ContainsKey("ETag"))
        {
            $this->_headers->RemoveHeader("ETag");
        } else {
            if (substr($etag, 0, 1) !== "\"")
            {
                $etag = "\"$etag\"";
            }
            $this->_headers->SetHeader("ETag", $etag);
        }
        return $this;
    }

    /**
     * Get the ETag HTTP Header value
     * @return string
     */
    public function Get_Etag()
    {
        return $this->_headers->GetHeaderValue("ETag");
    }

    /**
     * Set headers which will instruct the client to redirect to a different document temporarily.
     * (If you want a permanent redirect, set the http response code to 301)
     *
     * @param string $destinationUrl The Url of the document to redirect to
     *
     * @return \DblEj\Communication\Http\Headers This instance is returned; for chained usage.
     */
    public function Set_RedirectUrl($destinationUrl)
    {
        $this->_headers->SetHeader("location", $destinationUrl);
        return $this;
    }

    /**
     * Returns all of the headers that have been set, in a format that's easy to send straight to the client.
     * NOTE: If you do not set a property, the respective header for that property will not be sent at all, even if it has a default value.
     *
     * @return array An array of the HTTP headers
     */
    public function GetHeaders()
    {
        $returnVar = array();
        foreach ($this->_headers as $headerName => $header)
        {
            $returnVar[] = $headerName . ": " . $header;
        }
        return $returnVar;
    }

    public function SetHeader($headerName, $headerValue = null)
    {
        $this->_headers->SetHeader($headerName, $headerValue);
    }

    public function IsHeaderSet($headerName)
    {
        return $this->_headers->ContainsKey($headerName) && $this->_headers->GetHeaderValue($headerName) != null;
    }
}