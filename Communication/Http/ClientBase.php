<?php
namespace DblEj\Communication\Http;

/**
 * A simple base class for HTTP clients that implements some standard properties.
 */
abstract class ClientBase implements IClient
{
    /**
     *
     * @var string The URL of the HTTP server.
     */
    protected $_serverUrl;

    /**
     *
     * @var boolean Should Ssl peer be verified with CA?
     */
    protected $_verifySslPeer = false;

    /**
     *
     * @var boolean Should Ssl host be verified with CA?
     */
    protected $_verifySslHost = false;

    /**
     *
     * @var boolean Should requests follow HTTP redirects?
     */
    protected $_followRedirects = false;

    /**
     *
     * @var string Path of a local file which will be used to store cookie data.
     */
    protected $_cookieWriteFile = null;

    /**
     *
     * @var string Path of a local file that cookie data will be read from.  If blank/null, then $cookieWriteFile will be used.
     */
    protected $_cookieReadFile = null;

    /**
     *
     * @var string The username to supply in response to HTTP Auth requests.
     */
    protected $_httpUsername = null;

    /**
     *
     * @var string The password to supply in response to HTTP Auth requests.
     */
    protected $_httpPassword = null;

    /**
     * Initialize the base properties with values from the constructor.
     * Sub-classes should always call this constructor.
     *
     * @param string $serverUrl The URL of the HTTP server.
     * @param boolean $verifySslPeer Should Ssl peer be verified with CA?
     * @param boolean $verifySslHost Should Ssl host be verified with CA?
     * @param boolean $followRedirects Should requests follow HTTP redirects?
     * @param string $cookieWriteFile Path of a local file which will be used to store cookie data.
     * @param string $cookieReadFile Path of a local file that cookie data will be read from.  If left blank/null, then $cookieWriteFile will be used.
     * @param string $httpUsername HttpAuth username The username to supply in response to HTTP Auth requests.
     * @param string $httpPassword HttpAuth password The password to supply in response to HTTP Auth requests.
     */
    protected function __construct($serverUrl, $verifySslPeer = false, $verifySslHost = false, $followRedirects = false, $cookieWriteFile = null, $cookieReadFile = null, $httpUsername = null, $httpPassword = null)
    {
        $this->_serverUrl       = $serverUrl;
        $this->_verifySslHost   = $verifySslHost;
        $this->_verifySslPeer   = $verifySslPeer;
        $this->_followRedirects = $followRedirects;
        $this->_cookieReadFile  = $cookieReadFile;
        $this->_cookieWriteFile = $cookieWriteFile;
        $this->_httpUsername    = $httpUsername;
        $this->_httpPassword    = $httpPassword;
    }
    
    protected function onInput($data);

    /**
     * The URL of the HTTP server.
     * @return type
     */
    public function Get_ServerUrl()
    {
        return $this->_serverUrl;
    }
}