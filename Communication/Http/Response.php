<?php
namespace DblEj\Communication\Http;

/**
 * Represents a server-response to an HTTP request from a client.
 */
class Response
extends \DblEj\Communication\Response
implements IResponse
{
    const HTTP_OK_200                  = 200;
    const HTTP_PARTIAL_206             = 206;
    const HTTP_FILE_MOVED_PERMANENTLY  = 301;
    const HTTP_FILE_NOT_CHANGED_304    = 304;
    const HTTP_PEMRISSIONDENIED_403    = 403;
    const HTTP_FILENOTFOUND_404        = 404;
    const HTTP_SERVERERROR_500         = 500;
    const HTTP_NOTIMPLEMENTED_501      = 501;
    const HTTP_SERVICE_UNAVAILABLE_503 = 503;

    protected $_content;
    private $_contentLength;
    private $_contentType;
    private $_responseCode;
    private $_options;
    private $_headersSent = false;
    private $_contentIsSymbolic = false;

    /**
     *
     * @var Headers
     */
    private $_headers;
    private $_rawOutputSent;

    const CONTENT_TYPE_PRINTABLE_OUTPUT  = "CONTENT_TYPE_PRINTABLE_OUTPUT";
    const CONTENT_TYPE_PHP_SCRIPT        = "CONTENT_TYPE_PHP_SCRIPT";
    const CONTENT_TYPE_FILE              = "CONTENT_TYPE_FILE";
    const CONTENT_TYPE_RENDERABLE_OUTPUT = "CONTENT_TYPE_RENDERABLE_OUTPUT";

    protected static $_defaultCachePoint = \DblEj\Communication\Http\Headers::CACHEPOINT_BROWSER;

    /**
     * Create a response to an HTTP request.
     *
     * @param string $content The contents of the response/
     * @param int $responseCode The HTTP code to be sent to the client.
     * @param \DblEj\Communication\Http\Headers $headers The HTTP headers to be sent to the client.
     * @param string $contentType The type of the resonse's contents.
     * @param type $options Options specific to the specified <i>contentType</i>.
     */
    public function __construct($content = "", $responseCode = self::HTTP_OK_200, Headers $headers = null, $contentType = self::CONTENT_TYPE_PRINTABLE_OUTPUT, $options = null, $contentLength = null, $contentIsSymbolic = false)
    {
        parent::__construct($content);
        $this->_responseCode = $responseCode;
        $this->_headers      = $headers ? $headers : new Headers("text/html", self::$_defaultCachePoint, 86400, true);
        $this->_contentType  = $contentType;
        $this->_content      = $content;
        $this->_options      = $options;
        $this->_contentLength = $contentLength;
        $this->_contentIsSymbolic = $contentIsSymbolic;
    }

    public function Get_ContentIsSymbolic()
    {
        return $this->_contentIsSymbolic;
    }

    public function Set_ContentIsSymbolic($isSymbolic)
    {
        $this->_contentIsSymbolic = $isSymbolic;
    }

    public function Get_Options()
    {
        return $this->_options;
    }

    public function Set_Options($options)
    {
        $this->_options = $options;
    }

    /**
     * The raw response contents
     * @param string $rawResponse
     */
    public function Set_RawResponse($rawResponse)
    {
        $this->_rawOutputSent = $rawResponse;
    }

    /**
     * The raw response contents
     * @return string
     */
    public function Get_RawResponse()
    {
        return $this->_rawOutputSent;
    }

    /**
     * Specify an eTag that represents the freshness of the contents being returned.
     *
     * @param string $etag
     */
    public function Set_ETag($etag)
    {
        $this->_headers->Set_Etag($etag);
    }

    /**
     * Get the Etag Header
     * @return string
     */
    public function Get_ETag()
    {
        return $this->_headers->Get_Etag();
    }

    /**
     * Get the text content of the response.
     * @return string
     */
    public function Get_Content()
    {
        return $this->_content;
    }

    /**
     * Set the text content of the response.
     * @param string $content
     */
    public function Set_Content($content)
    {
        $this->_content = $content;
        if (!$this->_contentLength)
        {
            $this->_contentLength = strlen($content);
        }
    }

    public function Get_ContentLength()
    {
        return $this->_contentLength;
    }
    public function Set_ContentLength($contentLength)
    {
        $this->_contentLength = $contentLength;
    }

    /**
     * Alias for Get_Content()
     *
     * @return string
     */
    public function Get_Response()
    {
        return $this->Get_Content();
    }

    /**
     * Get the content type for this response.
     *
     * @return string One of the CONTENT_TYPE_* constants defined in this class (Response)
     */
    public function Get_ContentType()
    {
        return $this->_contentType;
    }

    /**
     * Sets the content type for this response.
     *
     * @param string One of the CONTENT_TYPE_* constants defined in this class (Response)
     */
    public function Set_ContentType($contentType)
    {
        $this->_contentType = $contentType;
    }

    /**
     * Get the HTTP response code.
     *
     * @return integer The HTTP response code to be sent to the client.
     */
    public function Get_ResponseCode()
    {
        return $this->_responseCode;
    }

    /**
     * The HTTP response code.
     *
     * @param integer $responseCode The HTTP response code to be sent to the client.
     * @return \DblEj\Communication\Http\Response
     */
    public function Set_ResponseCode($responseCode)
    {
        $this->_responseCode = $responseCode;
        return $this;
    }

    /**
     * The HTTP headers that will be sent to the client.
     *
     * @return array
     */
    public function Get_Headers()
    {
        return $this->_headers;
    }

    /**
     * Set a header instructing the client that the conent is binary.
     *
     * @param boolean $isBinary
     */
    public function Set_IsBinaryFile($isBinary)
    {
        $this->_headers->Set_IsBinary($isBinary);
    }

    /**
     * Set a header that specifies the MIME type of the contents in this response.
     *
     * @param string $mimeType
     */
    public function Set_MimeType($mimeType)
    {
        $this->_headers->Set_ContentType($mimeType);
    }

    /**
     * Get the header that specifies the MIME type of the contents in this response.
     *
     * @param string $mimeType
     */
    public function Get_MimeType()
    {
        $headers = $this->_headers->GetHeaders();
        return isset($headers["Content-Type"])?$headers["Content-Type"]:"text/html";
    }

    /**
     * Set the header that specifies the last modified time for the contents in this response.
     *
     * @param integer $lastModifiedTimestamp The Unix timestamp of the last modified time of the content.
     */
    public function Set_LastModifiedTime($lastModifiedTimestamp = null)
    {

        if (!$lastModifiedTimestamp)
        {
            $lastModifiedTimestamp = time();
        }
        $this->_headers->Set_LastModified($lastModifiedTimestamp);
    }

    /**
     * Set a header instructing the client how long they may cache the contents in this response.
     *
     * @param string $allowableCachepoint The most public cachepoint allowed.
     * Must be one of the Headers::CACHEPOINT_* constants.
     *
     * @param integer $cacheSeconds How long the cache point should cache the contents of this response for.
     * @param boolean $cachepointMustRevalidate Whether the client (or proxy) must revalidate the cache to determine if it is stale.
     */
    public function Set_BrowserCacheTimeout($allowableCachepoint = Headers::CACHEPOINT_BROWSER, $cacheSeconds = 86400, $cachepointMustRevalidate = false)
    {
        $this->_headers->Set_ClientCaching($allowableCachepoint, $cacheSeconds, $cachepointMustRevalidate);
    }

    /**
     * Set a header specifying the filename that should be used for the contents in this response.
     *
     * @param string $filename
     * @param boolean $forceDownload
     */
    public function Set_Filename($filename, $forceDownload = false)
    {
        $this->_headers->Set_ContentDisposition($filename, $forceDownload);
    }

    /**
     * Whether or not the headers have been sent to the client.
     *
     * @return boolean
     */
    public function Get_HeadersSent()
    {
        return $this->_headersSent;
    }

    /**
     * Set/override my headers with any in the passed collection
     * @param \DblEj\Communication\Http\HeaderCollection $headers
     */
    public function SetHeaders(HeaderCollection $headers)
    {
        foreach ($headers as $headerName=>$headerValue)
        {
            $this->_headers->SetHeader($headerName, $headerValue);
        }
    }

    /**
     * Whether or not the response have been sent to the client.
     *
     * @param boolean $headersSent
     */
    public function Set_HeadersSent($headersSent)
    {
        $this->_headersSent = $headersSent;
    }

    /**
     * Specify a URL that the client should redirect to upon receiving this response.
     * This will also set the HTTP response code to an appropriate value (30x).
     *
     * @param string $url The URL to redirect to.
     *
     * @param boolean $isPermanent Whether this redirect is permanent (301, 308) or temporary (307, 303).
     *
     * @param boolean $maintainHttpMethod Whether or not the same request method should
     * be used for the redirect as was used for the request that solicited this response.
     *
     * @param int $redirectCacheTimeout If greater than zero, the response will instruct the client that the next time they make this request,
     * they should redirect to the RedirectURL automatically without first sending the un-redirected request.  Time unit is seconds.
     */
    public function Set_RediredctUrl($url, $isPermanent = false, $maintainHttpMethod = false, $redirectCacheTimeout = 0)
    {
        $this->_headers->Set_RedirectUrl($url, $isPermanent, $maintainHttpMethod);
        if ($isPermanent)
        {
            if ($maintainHttpMethod)
            {
                //$this->Set_ResponseCode(308); Some browsers dont support it yet
                $this->Set_ResponseCode(301);
            }
            else
            {
                $this->Set_ResponseCode(301);
            }
            if ($redirectCacheTimeout > 0)
            {
                $this->Set_BrowserCacheTimeout(Headers::CACHEPOINT_BROWSER, $redirectCacheTimeout, false);
            } else {
                $this->Set_BrowserCacheTimeout(Headers::CACHEPOINT_NONE, 0, true);
            }
        }
        else
        {
            if ($maintainHttpMethod)
            {
                //$this->Set_ResponseCode(307); Some browsers dont support it yet
                $this->Set_ResponseCode(302);
            }
            else
            {
                //$this->Set_ResponseCode(303); Some browsers dont support it yet
                $this->Set_ResponseCode(302);
            }
            if ($redirectCacheTimeout > 0)
            {
                $this->Set_BrowserCacheTimeout(Headers::CACHEPOINT_BROWSER, $redirectCacheTimeout, true);
            } else {
                $this->Set_BrowserCacheTimeout(Headers::CACHEPOINT_NONE, 0, true);
            }
        }
    }

    /**
     * Alias for Get_ContentType()
     *
     * @see Response::Get_ContentType
     *
     * @return string
     */
    public function Get_ResponseType()
    {
        return $this->Get_ContentType();
    }

    public static function Set_DefaultCachePoint($cachePoint)
    {
        self::$_defaultCachePoint = $cachePoint;
    }
}