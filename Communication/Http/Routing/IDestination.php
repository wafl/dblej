<?php
namespace DblEj\Communication\Http\Routing;

/**
 * A Route's destination.
 */
interface IDestination
{

    /**
     * Go to this destination.
     *
     * @param string $request The request object that was received.
     * @param \DblEj\Application\IApplication $app The application that received the request and routed it to this destination.
     *
     * @return \DblEj\Communication\Http\Response The destination's response.
     */
    public function GotoDestination($request, \DblEj\Application\IApplication $app);

    /**
     * Do any preliminary work and return any info that can be used before actually going to the destination
     * @param string $request The request object that was received.
     * @param \DblEj\Application\IApplication $app The application that received the request and routed it to this destination.
     *
     * @return \DblEj\Communication\Http\Response The destination's preliminary response, which does not usually include content.
     */
    public function PrepareDestination($request, \DblEj\Application\IApplication $app);

}