<?php
namespace DblEj\Communication\Http\Routing;

/**
 * Routers that are part of ActiveWAFL, and that play an integral part in it's basic
 * functionality, implement this method.
 *
 * It is for ActiveWAFL internal use only.
 */
interface IInternalRouter
extends \DblEj\Communication\Http\Routing\IRouter
{

}