<?php

namespace DblEj\Communication\Http\Routing;

/**
 * Thrown when a router cannot find an appropriate route
 * for a request because there isn't one.
 *
 * Throwing this exception sets the HTTP response code to 404.
 */
class RouteNotFoundException
extends \DblEj\Communication\Http\Exception
{

    public function __construct($requestUri, $message = null, $severity = E_ERROR, $inner = null)
    {
        $message = "Route could not be found for the specified request Uri ($requestUri)<br>" . $message;
        parent::__construct($requestUri, $message, 404, $severity, $inner);
    }
}