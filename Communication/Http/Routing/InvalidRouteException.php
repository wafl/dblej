<?php

namespace DblEj\Communication\Http\Routing;

/**
 * Thrown when a router returns an invalid route or when a routes destination is invalid or unreachable.
 */
class InvalidRouteException
extends \DblEj\System\Exception
{

    /**
     * Create an instance of an InvalidRouteException.
     *
     * @param IRoute The route that is invalid.
     * @param string $message Additional information about the route's invalidity.
     * @param int $severity The severity of the exception.
     * @param \Exception $innerException The Exception that threw this InvalidRouteException,
     * or null if this InvalidRouteException was not thrown by an inner-exception.
     */
    public function __construct(IRoute $route, $message = null, $severity = E_ERROR, $innerException = null)
    {
        if ($route)
        {
            $message = "<em>" . \DblEj\Util\Reflection::GetBaseClassname(get_class($route)) . "</em>: ";
        }
        else
        {
            $message = "<em>NULL ROUTE</em>: ";
        }
        $message.="<b>Invalid Route Specified</b><br>" . $message;
        parent::__construct($message, $severity, $innerException);
    }
}