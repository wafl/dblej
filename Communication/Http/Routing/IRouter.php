<?php

namespace DblEj\Communication\Http\Routing;

use DblEj\Application\IWebApplication;
use DblEj\Communication\Http\Request;

/**
 * A Router's job is to find a suitable route for a given request.
 *
 * An HTTP Router exposes an additional method, GetHttpRoute, for consumers that
 * are aware of the router's type and are able to resolve this interface.
 *
 * For consumers only aware of the base interface, they will call the base
 * interfacses <i>GetRoute</i> method, which implementors of this interface
 * should redirect to </i>GetHttpRoute</i>.
 */
interface IRouter
extends \DblEj\Communication\IRouter
{

    /**
     * Return an appropriate route for the given request.
     *
     * If this router does not find an appropriate route, it should return false so that other routers in the chain can get a try at it
     *
     * @param \DblEj\Communication\Http\Request $request The request used to find the correct route and also passed to the route so that it may ultimately be taken to the destination.
     * @param \DblEj\Application\IApplication $app The application that received the initial request.
     * @param \DblEj\Communication\IRouter &$usedRouter
     * If this Router does find a route for the request,
     * it will assign itself to the by-reference argument <i>usedRouter</i>
     * so that the application knows which router handled the request, in the
     * event that this router is part of a router chain.
     *
     * @return \DblEj\Communication\Http\Routing\Route The appropriate HTTP Route for the given request.
     */
    public function GetHttpRoute(Request $request, IWebApplication $app = null, \DblEj\Communication\Http\Routing\IRouter &$usedRouter = null);
}