<?php
namespace DblEj\Communication\Http\Routing;

use DblEj\Application\IApplication;
use DblEj\Communication\Http\ApiResponse;
use DblEj\Communication\Http\Request;
use DblEj\Communication\Http\Response;
use DblEj\Communication\Http\Exception;
use DblEj\Communication\Route as NonHttpRoute;
use DblEj\Util\File;

/**
 * Represents a route to a destination that can handle HTTP requests.
 *
 * The destination can be any <i>callable</i> or <i>IDestination</i>.
 */
class Route
extends NonHttpRoute
{
    private $_request;

    /**
     * Construct the route.
     *
     * @param Request $request The request that was routed to this route.
     *
     * @param \callable|string $destination
     * Either a callback to the function that the request should be routed to,
     * or the name of a script file to hand the request off to if the destination is not a function
     *
     * @param array $destinationArguments
     */
    public function __construct(Request $request, $destination, $destinationArguments = null, $prepareDestination = null)
    {
        parent::__construct($destination, $destinationArguments, $prepareDestination);
        $this->_request = $request;
    }

    /**
     * Go to the destination and return it's response.
     *
     * @param \DblEj\Application\IApplication $app The application that received the original request.
     *
     * @return \DblEj\Communication\Http\Response
     * @throws Exception
     */
    public function GotoRoute(IApplication $app = null)
    {
        if (is_callable($this->_destination))
        {
            $response = parent::GotoRoute($app);
            if (is_a($response, "DblEj\\Communication\\Http\\Response"))
            {
                $httpResponse = $response;
            }
            elseif (is_a($this->_destination[0], "DblEj\\Communication\\Ajax\\AjaxHandler"))
            {
                $ajaxHandler  = $this->_destination[0];
                $httpResponse = new ApiResponse($ajaxHandler->Get_AjaxFunctionName(), $response);
            }
            else
            {
                $httpResponse = new Response($response, Response::HTTP_OK_200, null, Response::CONTENT_TYPE_PRINTABLE_OUTPUT);
            }
        }
        elseif (is_a($this->_destination, "DblEj\\Communication\\Http\\Routing\\IDestination"))
        {
            if (!$this->_destinationArgs)
            {
                $this->_destinationArgs = array();
            }
            array_unshift($this->_destinationArgs, $app);
            array_unshift($this->_destinationArgs, $this->_request);
            $httpResponse = call_user_func_array(array($this->_destination, "GotoDestination"), $this->_destinationArgs);
        }
        elseif (!is_object($this->_destination) && is_file($this->_destination))
        {
            //try to figure out if its a php file that should be included, or another filetype meant to just be printed out
            $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($this->_destination);
            if ($isPhpFile)
            {
                $httpResponse = new Response($this->_destination, Response::HTTP_OK_200, null, Response::CONTENT_TYPE_PHP_SCRIPT);
            }
            else
            {
                $httpResponse = new Response($this->_destination, Response::HTTP_OK_200, null, Response::CONTENT_TYPE_FILE);
            }
        }
        else
        {
            throw new Exception($this->_request->Get_RequestUrl(), 404);
        }

        return $httpResponse;
    }

    public function PrepareRoute(IApplication $app = null)
    {
        $httpResponse = null;
        if ($this->_prepareDestination)
        {
            if (is_callable($this->_prepareDestination))
            {
                $response = parent::PrepareRoute($app);
                if (is_a($response, "DblEj\\Communication\\Http\\Response"))
                {
                    $httpResponse = $response;
                }
                elseif (is_a($response, "DblEj\\Communication\\Response"))
                {
                    $httpResponse = new Response($response->Get_Response(), Response::HTTP_OK_200);
                }
                elseif (is_string($response))
                {
                    $httpResponse = new Response($response, Response::HTTP_OK_200);
                }
            }
            elseif (is_a($this->_prepareDestination, "DblEj\\Communication\\Http\\Routing\\IDestination"))
            {
                if (!$this->_destinationArgs)
                {
                    $this->_destinationArgs = array();
                }
                array_unshift($this->_destinationArgs, $app);
                array_unshift($this->_destinationArgs, $this->_request);
                $httpResponse = call_user_func_array(array($this->_prepareDestination, "PrepareDestination"), $this->_destinationArgs);
            }
            elseif (!is_object($this->_destination) && is_file($this->_destination))
            {
                //no way to prepare because we dont really know what the response is/does
                //so return default headers
                //try to figure out if its a php file that should be included, or another filetype meant to just be printed out
                $isPhpFile = \DblEj\Util\PhpFile::DoesFileHaveOpeningPhpTag($this->_destination);
                if ($isPhpFile)
                {
                    $httpResponse = new Response($this->_destination, Response::HTTP_OK_200, null, Response::CONTENT_TYPE_PHP_SCRIPT);
                }
                else
                {
                    $httpResponse = new Response($this->_destination, Response::HTTP_OK_200, null, Response::CONTENT_TYPE_FILE, null, filesize($this->_destination));
                }
            }
            else
            {
                throw new Exception($this->_request->Get_RequestUrl(), Response::HTTP_FILENOTFOUND_404);
            }
        }
        return $httpResponse;
    }

}