<?php

namespace DblEj\Communication\Http\Routing;

/**
 * An IRoute defines a destination, and provides a mechanism for sending information to that destination.
 *
 * An HTTP IRoute expects the destination to be an <i>IDestination</i> and
 * provides additional information specifying the name of the
 * <i>IDestination</i> class implementation the the Route's destination is an instance of.
 */
interface IRoute
extends \DblEj\Communication\IRoute
{

    /**
     * Get the fully qualified class name of the destination for this route.
     */
    public function GetDestinationClassname();
}