<?php

namespace DblEj\Communication\Http\Routing;

/**
 * Thrown when a router doesn't know how to find an appropriate route for a request
 * because the request is invalid or corrupt.
 */
class InvalidRouterRequestException
extends \DblEj\Communication\Http\Exception
{

    /**
     * Create an instance of a InvalidRouterRequestException.
     *
     * @param string $requestUri The URI that was requested.
     * @param \DblEj\Communication\Http\Routing\IRouter $router The router that tried to find a route for the request.
     * @param string $message Additional details about the invalidity of the request.
     * @param type $severity The severity of the exception.
     * @param \Exception $inner The Exception that threw this InvalidRouterRequestException,
     * or null if this InvalidRouterRequestException was not thrown by an inner-exception.
     */
    public function __construct($requestUri, IRouter $router = null, $message = null, $severity = E_ERROR, $inner = null)
    {
        if (is_a($router, "\DblEj\Communication\Http\Routing\IInternalRouter"))
        {
            $message = "Internal Router (" . \DblEj\Util\Reflection::GetBaseClassname(get_class($router)) . "): <b>Invalid Route Specified</b><br>" . $message;
        }
        elseif ($router != null)
        {
            $message = "<em>" . \DblEj\Util\Reflection::GetBaseClassname(get_class($router)) . "</em>Router: <b>Invalid Route Specified</b><br>" . $message;
        } else {
            $message = "<b>Null Router Specified</b><br>" . $message;
        }
        parent::__construct($requestUri, $message, 404, $severity, $inner);
    }
}