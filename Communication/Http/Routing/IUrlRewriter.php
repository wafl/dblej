<?php

namespace DblEj\Communication\Http\Routing;

interface IUrlRewriter
{
    public function RewriteUrl($url);
}