<?php
namespace DblEj\Communication\Http;

class ApiRequest
extends Request
{
    private $_app;
    private $_callName;
    private $_callArgs;
    private $_requestHeaders;
    private $_requestFiles;
    private $_reindexArrayResults;

    public function __construct($callName, $callArgs, $requestHeaders, $requestFiles, \DblEj\Application\IApplication $app, \DblEj\Communication\Http\Request $request, $reindexArrayResults = false)
    {
        parent::__construct($request->Get_RequestUrl(), $request->Get_RequestType());
        $this->_app = $app;
        $this->_callArgs = $callArgs;
        $this->_callName = $callName;
        $this->_requestFiles = $requestFiles;
        $this->_requestHeaders = $requestHeaders;
        $this->_reindexArrayResults = $reindexArrayResults;
    }

    public function Get_App()
    {
        return $this->_app;
    }
    public function Get_ReindexArrayResults()
    {
        return $this->_reindexArrayResults;
    }
    public function Get_Arguments()
    {
        return $this->_callArgs;
    }
    public function Get_ApiCall()
    {
        return $this->_callName;
    }
    public function Get_RequestFiles()
    {
        return $this->_requestFiles;
    }
    public function Get_RequestHeaders()
    {
        return $this->_requestHeaders;
    }

    public function IsInput($variableName, $inputType = self::INPUT_REQUEST)
    {
        return isset($this->_callArgs[$variableName])?true:parent::IsInput($variableName, $inputType);
    }

    public function GetInput($variableName, $inputType = self::INPUT_REQUEST, $validation = \DblEj\Data\Validator::VALIDATE_NONE, $sanitization = \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue = null, $flags = null, $throwExceptionOnInvalidData = false, $decimalSeperator = ".", $minAllowedValue = 0, $maxAllowedValue = PHP_INT_MAX, $regExp = null)
    {
        $returnValue = isset($this->_callArgs[$variableName])?$this->_callArgs[$variableName]:null;

        if (!$returnValue)
        {
            $returnValue = parent::GetInput($variableName, $inputType, $validation, $sanitization, $defaultValue, $flags, $throwExceptionOnInvalidData, $decimalSeperator, $minAllowedValue, $maxAllowedValue, $regExp);
        }
        return $returnValue;
    }
}
