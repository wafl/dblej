<?php
namespace DblEj\Communication\Http;

/**
 * Represents an HTTP request.
 */
class Request
implements \DblEj\Communication\IRequest
{
    const HTTP_POST        = 1;
    const HTTP_GET         = 2;
    const HTTP_PUT         = 3;
    const HTTP_DELETE      = 4;
    const HTTP_PATCH       = 5;
    const INPUT_POST       = INPUT_POST;
    const INPUT_GET        = INPUT_GET;
    const INPUT_REQUEST    = INPUT_REQUEST;
    const INPUT_SERVER     = INPUT_SERVER;
    const INPUT_SESSION    = INPUT_SESSION;
    const INPUT_COOKIE     = INPUT_COOKIE;
    const INPUT_ENVIROMENT = INPUT_ENV;

    private $_requestType;
    private $_requestUrl;
    private $_isLiveRequest;
    
    private $_fakedInputs = [self::INPUT_GET     => [],
        self::INPUT_POST    => [],
        self::INPUT_REQUEST => []];

    /**
     * Create a request instance.
     *
     * @param string $requestUrl
     * The Url being requested.
     *
     * @param int $requestType
     * The HTTP request type.
     *
     * Should be one of:
     * Request::HTTP_GET, Request::HTTP_POST, Request::HTTP_PUT, Request::HTTP_DELETE
     */
    public function __construct($requestUrl, $requestType = self::HTTP_GET, $isLiveRequest = true)
    {
        $this->_requestType = $requestType;
        $this->_requestUrl  = $requestUrl;
        $this->_isLiveRequest = $isLiveRequest;
    }

    /**
     * The request type for this request.
     *
     * Must be one of the following:
     * Request::HTTP_GET, Request::HTTP_POST, Request::HTTP_PUT, Request::HTTP_DELETE
     *
     * @return string
     */
    public function Get_RequestType()
    {
        return $this->_requestType;
    }

    /**
     * The Url that was requested by the client.
     *
     * @return string
     */
    public function Get_RequestUrl()
    {
        return $this->_requestUrl;
    }

    /**
     * Override the URL sent from the client.
     *
     * @param string $url The URL to report that that the client requested.
     * @return \DblEj\Communication\Http\Request Returns this instance for chained method calls.
     */
    public function Set_RequestUrl($url)
    {
        $this->_requestUrl = $url;
        return $this;
    }

    /**
     * Alias for Get_RequestUrl().
     *
     * @return string
     */
    public function Get_Request()
    {
        return $this->Get_RequestUrl();
    }

    /**
     * Get all of the HTTP variables (such as submitted GET or POST variables)
     * of the specified type.
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * Request::INPUT_REQUEST returns both GET + POST variables.
     *
     * @return array All HTTP variables of the specified type.
     */
    public function GetAllInputs($inputType = self::INPUT_REQUEST)
    {
        if ($this->_isLiveRequest)
        {
            $realInputs = \DblEj\Communication\Http\Util::GetInputs($inputType);
            $returnInputs = array_replace_recursive($realInputs, $this->_fakedInputs[$inputType]);
        } else {
            $returnInputs = $this->_fakedInputs[$inputType];
        }
        
        return $returnInputs;
    }

    private function getFakedInput($inputName, $inputType = self::INPUT_REQUEST)
    {
        if (isset($this->_fakedInputs[$inputType][$inputName]))
        {
            return $this->_fakedInputs[$inputType][$inputName];
        }
        else
        {
            return null;
        }
    }

    /**
     * Check if there is an HTTP variable of the specified name.
     *
     * @param string $variableName
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * Request::INPUT_REQUEST checks both GET + POST variables.
     *
     * @return boolean <em>True</em> if the specified variable is an HTTP input variable, <em>false</em> otherwise.
     */
    public function IsInput($variableName, $inputType = self::INPUT_REQUEST)
    {
        $allInputs = $this->GetAllInputs($inputType);
        return key_exists($variableName, $allInputs);
    }

    /**
     * Get HTTP input variables (such as submitted GET or POST variables)
     * where the value is an array.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param string $validation
     * The validation type to be performed on the retreived variables.
     * Must be a constant from \DblEj\Data\Validator.
     *
     * @param type $sanitization
     * The sanitization type to be performed on the retreived variables.
     * Must be a constant from \DblEj\Data\Validator.
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @param integer $flags
     * One or more of php's FILTER_* flags, used to specify flags for certain validation and sanitization types.
     *
     * @param boolean $throwExceptionOnInvalidData
     * Whether an exception should be thrown if the specified data doesn't exist.
     *
     * @return array
     */
    public function GetInputArray($variableName, $inputType = self::INPUT_REQUEST, $validation = \DblEj\Data\Validator::VALIDATE_NONE, $sanitization = \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue = [], $flags = null, $throwExceptionOnInvalidData = false)
    {
        return $this->GetInput($variableName, $inputType, $validation, $sanitization, $defaultValue, $flags | FILTER_REQUIRE_ARRAY, $throwExceptionOnInvalidData);
    }

    /**
     * Get details about files uploaded via an HTTP form input element
     * @param string $variableName The name of the input element
     * @return array An array containing information about the uploaded file.
     */
    public function GetUploadedFile($variableName)
    {
        $returnFileInfo = null;
        if (isset($_FILES[$variableName])) {
            $file = $_FILES[$variableName];
            if (is_uploaded_file($file["tmp_name"])) {
                $returnFileInfo = $file;
            }
        }
        return $returnFileInfo;
    }

    /**
     * Get details about files uploaded via multiple HTTP form input elements
     * @param string $variableName The name of the input elements, not including the sqaure brackets.
     * @return array An array containing information about the uploaded files.
     */
    public function GetUploadedFileArray($variableName)
    {
        $returnFileInfo = null;
        if (isset($_FILES[$variableName])) {
            $file = $_FILES[$variableName];
            if (is_array($file["tmp_name"])) {
                $returnFileInfo = $file;
            }
        }
        return $returnFileInfo;
    }

    /**
     * Sets a request input variable, as if it had actually been input.
     *
     * @param string $variableName The name of the input variable to set.
     * @param string|number $variableValue The value of the input.
     * @param int $inputType The type of input to emulate.  Set to one of the INPUT_* constants.
     */
    public function SetInput($variableName, $variableValue, $inputType = self::INPUT_GET)
    {
        if ($inputType === null)
        {
            $inputType = self::INPUT_REQUEST;
        }
        if ($this->_isLiveRequest)
        {
            $inputData = \DblEj\Communication\Http\Util::GetInput($variableName, $inputType);

            if ($inputData !== null)
            {
                switch($inputType)
                {
                    case self::INPUT_GET:
                        unset($_GET[$variableName]);
                        unset($_REQUEST[$variableName]);
                        break;
                    case self::INPUT_POST:
                        unset($_POST[$variableName]);
                        unset($_REQUEST[$variableName]);
                        break;
                    case self::INPUT_REQUEST:
                        unset($_POST[$variableName]);
                        unset($_GET[$variableName]);
                        unset($_REQUEST[$variableName]);
                        break;
                    case self::INPUT_SERVER:
                        unset($_SERVER[$variableName]);
                        break;
                    default:
                        throw new \Exception("This method isnt suitable for setting cookies or session variables");
                }
            }
        }
        $this->_fakedInputs[$inputType][$variableName]          = $variableValue;
        $this->_fakedInputs[self::INPUT_REQUEST][$variableName] = $variableValue;
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables)
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param string $validation
     * The validation type to be performed on the retreived variables.
     * Must be a constant from \DblEj\Data\Validator.
     *
     * @param type $sanitization
     * The sanitization type to be performed on the retreived variables.
     * Must be a constant from \DblEj\Data\Validator.
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @param integer $flags
     * One or more of php's FILTER_* flags, used to specify flags for certain validation and sanitization types.
     *
     * @param boolean $throwExceptionOnInvalidData
     * Whether an exception should be thrown if the specified data doesn't exist.
     *
     * @param string $decimalSeperator The decimal seperator to use when validating decimal input.
     *
     * @param decimal $minAllowedValue The minimum value allowed when validating numeric input.
     *
     * @param decimal $maxAllowedValue The maximum value allowed when validating numeric input.
     *
     * @param string $regExp The regualar expression to use when performing regex validation.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exit and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInput($variableName, $inputType = self::INPUT_REQUEST, $validation = \DblEj\Data\Validator::VALIDATE_NONE, $sanitization = \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue = null, $flags = null, $throwExceptionOnInvalidData = false, $decimalSeperator = ".", $minAllowedValue = 0, $maxAllowedValue = PHP_INT_MAX, $regExp = null)
    {
        if ($inputType === null)
        {
            $inputType = self::INPUT_REQUEST;
        }
        $inputData = null;
        if (isset($this->_fakedInputs[$inputType]) && isset($this->_fakedInputs[$inputType][$variableName]))
        {
            $inputData = $this->getFakedInput($variableName, $inputType);
        }
        if ($inputData === null && $this->_isLiveRequest)
        {
            $inputData = \DblEj\Communication\Http\Util::GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_NONE, null, $flags, $decimalSeperator, $minAllowedValue, $maxAllowedValue, $regExp);
        }
        if ($inputData === null || $inputData == "")
        {
            $inputData = $defaultValue;
        }
        if ($sanitization)
        {
            $inputData = \DblEj\Data\Validator::Sanitize($inputData, $sanitization, $flags);
        }
        if ($validation)
        {
            $inputData = \DblEj\Data\Validator::GetValidData($inputData, $validation, $defaultValue, $flags);
        }
        if ($throwExceptionOnInvalidData)
        {
            if (($validation == \DblEj\Data\Validator::VALIDATE_BOOLEAN) && $inputData === null)
            {
                throw new \DblEj\Data\InvalidDataException();
            }
            elseif ($inputData === false)
            {
                throw new \DblEj\Data\InvalidDataException();
            }
        }
        return $inputData;
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid email address.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputEmailAddress($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_EMAIL_ADDRESS, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid URL.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputUrl($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_URL, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid whole number.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputInteger($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_INTEGER, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid decimal.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputDecimal($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_DECIMAL, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid boolean.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputBoolean($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_BOOLEAN, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid ip address.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputIpAddress($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_IP_ADDRESS, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value matches the specified regular expression.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputRegex($variableName, $regexPattern, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_REGEX, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue, null, false, ".", 0, PHP_INT_MAX, $regexPattern);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid mac address.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputMacAdress($variableName, $inputType = self::INPUT_REQUEST, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_MAC_ADDRESS, \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue);
    }

    /**
     * Get an HTTP variable (such as submitted POST or GET variables),
     * if the value is a valid string.
     *
     * @param string $variableName The name of the HTTP request variable
     *
     * @param string $inputType
     * Must be one of the following:
     * Request::INPUT_GET, Request::INPUT_POST, Request::INPUT_REQUEST
     * Request::INPUT_SERVER, Request::INPUT_SESSION,
     * Request::INPUT_COOKIE, Request::INPUT_ENVIRONMENT
     *
     * @param mixed $defaultValue
     * The value to return of the variable does not exist.
     *
     * @return string The value of the specified variable.
     *
     * @throws \DblEj\Data\InvalidDataException If the variable does not exist and if <em>$throwExceptionOnInvalidData</em> is true.
     */
    public function GetInputString($variableName, $inputType = self::INPUT_REQUEST, $sanitize = \DblEj\Data\Validator::SANITIZE_NONE, $defaultValue = null)
    {
        return $this->GetInput($variableName, $inputType, \DblEj\Data\Validator::VALIDATE_NONE, $sanitize, $defaultValue);
    }
}