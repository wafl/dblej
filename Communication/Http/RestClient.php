<?php
namespace DblEj\Communication\Http;

/**
 * An HTTP client for RESTful services.
 */
class RestClient
extends ClientBase
{

    /**
     * Create a RESTful client
     * @param string $serverUrl The Url to the RESTful server.
     * @param boolean $verifySslPeer Should Ssl peer be verified with CA?
     * @param boolean $verifySslHost Should Ssl host be verified with CA?
     * @param boolean $followRedirects Should requests follow HTTP redirects?
     * @param string $cookieWriteFile Path of a local file which will be used to store cookie data.
     * @param string $cookieReadFile Path of a local file that cookie data will be read from.  If left blank/null, then $cookieWriteFile will be used.
     * @param string $httpUsername HttpAuth username
     * @param string $httpPassword HttpAuth password
     */
    public function __construct($serverUrl, $verifySslPeer = false, $verifySslHost = false, $followRedirects = false, $cookieWriteFile = null, $cookieReadFile = null, $httpUsername = null, $httpPassword = null)
    {
        parent::__construct($serverUrl, $verifySslPeer, $verifySslHost, $followRedirects, $cookieWriteFile, $cookieReadFile, $httpUsername, $httpPassword);
    }

    /**
     * Get an entity from the server
     * @param string $entityResource Name of the resource that lives on the server that you wish to retreive.  This is usually a file name.
     * @param array $httpHeaders An array of HTTP headers to send
     * @param int $httpResultCode If this is passed-in, it will be populated with the HTTP result code returned from the server.
     * @return string The raw response from the server
     */
    public function Get($entityResource, $httpHeaders = null, &$httpResultCode = null)
    {
        $response = Util::SendRequest($this->_serverUrl . $entityResource, false, null, $this->_verifySslPeer, $this->_verifySslHost, $this->_httpUsername, $this->_httpPassword, $this->_followRedirects, $this->_cookieWriteFile, $this->_cookieReadFile, $httpHeaders, false, $httpResultCode);
        if ($response !== null)
        {
            $this->onInput($response);
        }
    }

    /**
     * Post data to a server handler (such as a PHP script)
     * @param string $handlerResource Name of the resource that you will handle this request.  This is usually the file name of a server-side script (such as a PHP file).
     * @param array $postData An array of key/val pairs (where the array index is the key and the element is the value) to post to the server.
     * @param array $httpHeaders An array of HTTP headers to send
     * @param int $httpResultCode If this is passed-in, it will be populated with the HTTP result code returned from the server.
     * @return string The raw response from the server
     */
    public function Post($handlerResource, $postData = null, $httpHeaders = null, &$httpResultCode = null)
    {
        $response = Util::SendRequest($this->_serverUrl . $handlerResource, true, $postData ? Util::MakeQueryString($postData) : null, $this->_verifySslPeer, $this->_verifySslHost, $this->_httpUsername, $this->_httpPassword, $this->_followRedirects, $this->_cookieWriteFile, $this->_cookieReadFile, $httpHeaders, false, $httpResultCode);
        if ($response !== null)
        {
            $this->onInput($response);
        }
    }

    /**
     * Put an entity on the server at the specified resource.
     * @param string $entityResource Name of the resource that lives on the server that you wish to retreive.  Often, this is a file name.
     * @param string|array $entityBody the payload to create/replace the entity at the specified resource with.
     * @param array $httpHeaders An array of HTTP headers to send
     * @param integer $httpResultCode If this is passed-in, it will be populated with the HTTP result code returned from the server.
     * @return string The raw response from the server
     */
    public function Put($entityResource, $entityBody, $httpHeaders = array(), &$httpResultCode = null)
    {
        $url = $this->_serverUrl . $entityResource;
        $response = Util::SendRequest($url, Request::HTTP_PUT, $entityBody, $this->_verifySslPeer, $this->_verifySslHost, $this->_httpUsername, $this->_httpPassword, $this->_followRedirects, $this->_cookieWriteFile, $this->_cookieReadFile, $httpHeaders, false, $httpResultCode);
        if ($response !== null)
        {
            $this->onInput($response);
        }
    }

    public function Del($entityResource, $httpHeaders = array(), &$httpResultCode = null)
    {
        return $this->Delete($entityResource, $httpResultCode);
    }

    /**
     * Delete an entity from the server at the specified resource.
     * @param string $entityResource Name of the resource that lives on the server that you wish to delete.  Often, this is a file name.
     * @param integer $httpResultCode If this is passed-in, it will be populated with the HTTP result code returned from the server.
     * @return string The raw response from the server
     */
    public function Delete($entityResource, &$httpResultCode)
    {
        $url = $this->_serverUrl . $entityResource;
        $response = Util::SendRequest($url, false, null, $this->_verifySslPeer, $this->_verifySslHost, $this->_httpUsername, $this->_httpPassword, $this->_followRedirects, $this->_cookieWriteFile, $this->_cookieReadFile, true, $httpResultCode);
        if ($response !== null)
        {
            $this->onInput($response);
        }
    }

    /**
     * The URL of the RESTful server.
     * @return string
     */
    public function Get_ServerUrl()
    {
        return $this->_serverUrl;
    }

    public function Connect()
    {

    }
}