<?php

namespace DblEj\Communication\Http;

/**
 * Thrown when a complete absolute URL is expected and it is not found.
 */
class InvalidAbsoluteUrlException
extends Exception
{

    /**
     * Create an instance of the InvalidAbsoluteUrlException.
     *
     * @param string $requestUri
     * The requested URI, which may or may not be a URL.
     *
     * @param string $errorMessage
     * A text description of the nature of the URL's invalidity.
     *
     * @param int $httpErrorCode
     * The HTTP error code to be included in any HTTP response describing this exception.
     *
     * @param int $severity The severity of the exception.
     * @param \Exception $innerException The Exception that threw this InvalidAbsoluteUrlException,
     * or null if this InvalidAbsoluteUrlException was not thrown by an inner-exception.
     */
    public function __construct($requestUri, $errorMessage = "", $httpErrorCode = 500, $severity = E_ERROR, $innerException = null)
    {
        parent::__construct($requestUri, $errorMessage, $httpErrorCode, $severity, $innerException);
    }
}