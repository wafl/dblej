<?php

namespace DblEj\Communication\Http;

/**
 * An HTTP response that includes a SitePage to be rendered and sent to the client.
 */
interface IClient
{
    public function Connect();
    public function Get($fileOrResource, $httpHeaders = null, &$httpResultCode = null);
    public function Put($fileOrResource, $putPayload, $httpHeaders = null, &$httpResultCode = null);
    public function Post($fileOrResource, $postPayload = null, $httpHeaders = null, &$httpResultCode = null);
    public function Del($fileOrResource, $httpHeaders = array(), &$httpResultCode = null);

    public function Get_ServerUrl();
}