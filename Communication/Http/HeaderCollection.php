<?php

namespace DblEj\Communication\Http;

/**
 * A collection of HTTP Headers.
 * A header is simply a key/val pair.
 *
 * @since 770
 */
class HeaderCollection
extends \DblEj\Collections\KeyedCollection
{

    /**
     * Add a header to the collection.
     *
     * @param string $headerName The name of the header to add.
     * @param string $headerValue The value of the header being added.
     * @return \DblEj\Communication\Http\HeaderCollection $this for making chained method calls on this collection.
     */
    public function AddHeader($headerName, $headerValue = null)
    {
        return $this->AddItem($headerValue, $headerName);
    }

    /**
     * Set the value of a header in the collection.
     * If it doesn't exist, add it.
     * Functionally analogous to AddHeader().
     *
     * @param string $headerName The name of the header to set.
     * @param string $headerValue The value of the header being set.
     * @return \DblEj\Communication\Http\HeaderCollection $this for making chained method calls on this collection.
     */
    public function SetHeader($headerName, $headerValue = null)
    {
        return $this->SetItem($headerValue, $headerName);
    }

    /**
     * Remove the header of the specified name.
     *
     * @param string $headerName The name of the header to remove.
     * @return \DblEj\Communication\Http\HeaderCollection $this for making chained method calls on this collection.
     */
    public function RemoveHeader($headerName)
    {
        return $this->RemoveItemByKey($headerName);
    }

    /**
     * Get the value of the specified header.
     *
     * @param string $headerName The name of the header to retrieve.
     * @param string $defaultValue The value to return if the header doesn't exist.
     * @return string The value of the specified header.
     */
    public function GetHeaderValue($headerName, $defaultValue = null)
    {
        $returnValue = $defaultValue;
        if ($this->ContainsKey($headerName))
        {
            $returnValue = $this->GetItem($headerName);
        }
        return $returnValue;
    }
}