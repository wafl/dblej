<?php
namespace DblEj\Communication\Http;

use DblEj\Presentation\ITemplateRenderer,
    DblEj\SiteStructure\SitePage;

/**
 * A client response to an HTTP request for a sitepage
 */
class SitePageResponse
extends Response
implements ISitePageResponse
{
    private $_sitePage;
    private static $_defaultCachePoint = \DblEj\Communication\Http\Headers::CACHEPOINT_BROWSER;

    /**
     * Create an instance of the SitePageResponse class
     * @param SitePage $sitePage The site page to return to the server
     * @param ITemplateRenderer $renderer The view engine that will be used to render the site page
     * @param string $responseCode The HTTP response code to be returned to the server
     * @param mixed $headers an array of HTTP headers to be returned to the server
     * @param mixed $renderVersionKey1
     * @param mixed $renderVersionKey2
     * @param array $controls
     * @param RenderViewOptions $renderViewOptions
     */
    public function __construct(SitePage &$sitePage, $responseCode = self::HTTP_OK_200, Headers $headers = null, $options = null)
    {
        $this->_sitePage = $sitePage;

        if (!$headers)
        {
            //create the http headers to be included in the response
            $headers = new Headers("text/html", self::$_defaultCachePoint, 86400, true);
        }
        parent::__construct($this->_sitePage, $responseCode, $headers, self::CONTENT_TYPE_RENDERABLE_OUTPUT, $options);
    }

    public static function Set_DefaultCachePoint($cachePoint)
    {
        self::$_defaultCachePoint = $cachePoint;
    }

    /**
     * @return \DblEj\SiteStructure\SitePage
     */
    public function Get_SitePage()
    {
        return $this->_sitePage;
    }
}