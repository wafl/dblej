<?php
namespace DblEj\Communication\Http;

/**
 * Thrown when an invalid response is returned by a Route handling an HTTP request.
 */
class InvalidResponseException
extends Exception
{

    /**
     * Create an instance of InvalidResponseException.
     *
     * @param string $requestUri
     * The URI of the request that was handled that generated the invalid response.
     *
     * @param string $errorMessage
     * A description of the invalidity.
     *
     * @param int $httpErrorCode
     * The HTTP error code include in the response to the HTTP client that made the request.
     *
     * @param int $severity The severity of the exception.
     *
     * @param \Exception $innerException The Exception that threw this InvalidAbsoluteUrlException,
     * or null if this InvalidAbsoluteUrlException was not thrown by an inner-exception.
     */
    public function __construct($requestUri, $errorMessage = "", $httpErrorCode = 500, $severity = E_ERROR, $innerException = null)
    {
        parent::__construct($requestUri, $errorMessage, $httpErrorCode, $severity, $innerException);
    }
}