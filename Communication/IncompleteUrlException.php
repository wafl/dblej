<?php
namespace DblEj\Communication;

/**
 * Thrown when an incomplete URL is used in an operation or passed as an argument,
 * except where an incomplete URL is expected.
 */
class IncompleteUrlException
extends CommunicationException
{

    /**
     * Creates an instance of an IncompleteUrlException.
     *
     * @param string $message Additional details about the exception occurance.
     * @param integer $severity The severity level.
     * @param \Exception $innerException The Exception, if any, that threw this IncompleteUrlException.
     */
    public function __construct($message = "", $severity = E_WARNING, \Exception $innerException = null)
    {
        $message = "Incomplete Url.  Url's must contain a valid scheme and scheme specific part (as in <scheme>:<scheme-specific-part>)  See http://www.ietf.org/rfc/rfc1738.txt." . $message;
        parent::__construct($message, $severity, $innerException);
    }
}