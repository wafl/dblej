<?php
namespace DblEj\IO;

/**
 * Pathway from an input to an output.
 */
class Pathway
extends \DblEj\EventHandling\EventRaiser
implements IPathway
{
    private $_input;
    private $_output;

    /**
     * Anything received by <i>$input</i> will be sent to <i>$output</i>.
     *
     * @param \DblEj\IO\IOutput $output
     * @param \DblEj\IO\IInput $input
     */
    public function __construct(IOutput $output, \DblEj\IO\IInput $input)
    {
        parent::__construct();
        $this->_input  = $input;
        $this->_output = $output;

        $this->_input->AddHandler(new \DblEj\EventHandling\DynamicEventHandler(
        DataReceivedEvent::EVENT_DATARECEIVED, function(DataReceivedEvent $eventInfo)
        {
            $this->onInput($eventInfo);
        }));
    }

    final public function GetRaisedEventTypes()
    {
        return array(
            DataReceivedEvent::EVENT_DATARECEIVED,
            DataSentEvent::EVENT_DATASENT);
    }

    protected function onInput($eventInfo)
    {
        $this->raiseEvent(new DataReceivedEvent($eventInfo->Get_Data(), $this->_input, $eventInfo->Get_Sender()));
        $this->sendOutput($eventInfo->Get_Data());
    }

    protected function sendOutput($inputData)
    {
        $this->raiseEvent(new DataSentEvent($inputData, null, $this->_output));
        $this->_output->sendOutput($inputData);
    }
}