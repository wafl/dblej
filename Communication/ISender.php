<?php
namespace DblEj\Communication;

interface ISender
{
    public function Send($from, $to, $data);
}
