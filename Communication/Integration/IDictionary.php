<?php
namespace DblEj\Communication\Integration;

interface IDictionary
{
    public function GetDefinitions($word);
    public function GetPartsOfSpeech($word);
    public function Lookup($word);
}