<?php
namespace DblEj\Communication\Integration;

interface ISmsSenderExtension
extends ISmsSender, \DblEj\Extension\IExtension
{
}