<?php
namespace DblEj\Communication\Integration;

interface ISmsSender
extends \DblEj\Communication\ISender
{
    public function Send($fromNumber, $toNumber, $message);
}