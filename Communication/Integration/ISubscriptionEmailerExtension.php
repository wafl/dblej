<?php
namespace DblEj\Communication\Integration;

interface ISubscriptionEmailerExtension
extends ISubscriptionEmailer, \DblEj\Extension\IExtension
{
}