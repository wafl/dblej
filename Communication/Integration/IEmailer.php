<?php
namespace DblEj\Communication\Integration;

interface IEmailer
{
    /**
     * Send an email
     * @param string $subject Email subject
     * @param string $from Email address of sender or an array such as ["senderemail@address.com", "Acme, Inc."] where the first element is the email address and the second element is the sender's name
     * @param string|array $to Recipient email address or an array of email addresses and names such as [["john@host.com", "John Doe"], ["jane@host.com", "Jane Doe"], ...]
     * @param string $htmlContents HTML email contents
     * @param string $plainContents (Optional) Plain text email contents
     * @param string|array $cc (Optional) CC email address or an array of email addresses and names such as [["john@host.com", "John Doe"], ["jane@host.com", "Jane Doe"], ...]
     * @param string|array $bcc (Optional) BCC email address or an array of email addresses and names such as [["john@host.com", "John Doe"], ["jane@host.com", "Jane Doe"], ...]
     * @param string|array $attachments (Optional) Array of files with the filename and mime type such as [ [ "/absolute/local/file1.txt" => ["file1.txt", "text/plain" ]], [ "/absolute/local/file2.txt" => ["file2.txt", "text/plain" ]], ... ]
     * @param string|array $embeddedImages (Optional) Array of image files with the filename and mime type such as [ [ "/absolute/local/pic1.jpg" => ["JohnsAvatar.jpg", "image/jpeg" ]], [ "/absolute/local/pic2.png" => ["JanesAvatar.png", "image/png" ]], ... ]
     * @param string $replyTo (Optional) Reply to remail address
     * @param boolean $inlineCss (Optional) Whether or not css class should be converted to inline styles
     * @param mixed $sendResultDetails (Optional) When passed, the variable may be filled by implementor-specific details about the transaction
     */
    function SendEmail($subject, $from, $to, $htmlContents, $plainContents = null, $cc = null, $bcc = null, $attachments = null, $embeddedImages = null, $replyTo = null, $inlineCss = true, $additionalHeaders = null, &$sendResultDetails = null);
}