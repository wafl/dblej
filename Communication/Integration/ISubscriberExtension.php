<?php
namespace DblEj\Communication\Integration;

interface ISubscriberExtension
extends ISubscriber, \DblEj\Extension\IExtension
{
}