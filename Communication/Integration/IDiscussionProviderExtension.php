<?php
namespace DblEj\Communication\Integration;

interface IDiscussionProviderExtension
extends IDiscussionProvider, \DblEj\Extension\IExtension
{
}
