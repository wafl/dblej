<?php
namespace DblEj\Communication\Integration;

interface IAnswerGiverExtension
extends IAnswerGiver, \DblEj\Extension\IExtension
{
}