<?php
namespace DblEj\Communication\Integration;

interface IPostSharerExtension
extends IPostSharer, \DblEj\Extension\IExtension
{
}