<?php
namespace DblEj\Communication;

/**
 * Represents a response to any type of request.
 */
interface IResponse
{
    /**
     * The response's data.
     */
    public function Get_Response();

    /**
     * Information that defines what the data means and/or what type it is of.
     */
    public function Get_ResponseType();

    /**
     * When someone is trying to view this response, in what role(s) should they be treated?
     * By default the viewer is represented by the current User object.
     * As an example, a website user might have multiple roles like admin, csr, seller, buyer, etc.
     * Some responses, like a home page, might only care about the traveler in the context of a regular user.
     * But some responses, say for example a seller administration section, might only be available to a seller
     * staff member for the specified seller.  That response would want to specify "Seller" (or something similar)
     * as the viewer role.  The actual value is arbitrary, except that a viewer must be aware of the value's
     * meaning to be able to act in that role/context.  A good practice is to use the allowed traveller (sub)class name(s).
     *
     * @return array An array of strings that the traveler can use to decide how (in what context / as what class) it wants to represent itself
     */
    public function Get_ViewerRoles();

    public function Set_ViewerRoles(array $roleArray);
}