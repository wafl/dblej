<?php
namespace DblEj\Communication;

class AccessDeniedException
extends Exception
{
    /**
     * Instantiate an AccessDeniedException
     *
     * @param string $inaccessableThing
     * @param string $whoIsBeingDenied
     * @param string $sourceAction
     * @param string $privateDetails
     * @param string $publicDetails
     * @param \Exception $inner
     * @param int $severity
     */
    public function __construct($inaccessableThing, $whoIsBeingDenied, $sourceAction = null, $privateDetails = null, $publicDetails = null, $errorOutputCode = null, \Exception $inner = null, $severity = E_ERROR)
    {
        if (!$privateDetails)
        {
            $privateDetails = "Access to $inaccessableThing is denied for $whoIsBeingDenied.".($sourceAction?" Source: $sourceAction.":"");
        }
        parent::__construct($inaccessableThing, $privateDetails, $errorOutputCode, $publicDetails, $inner, $severity);
    }
}