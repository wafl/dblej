<?php

namespace DblEj\Data;

/**
 * Thrown when a storage engine is not ready to perform a requested task.
 */
class StorageEngineNotReadyException
extends \DblEj\System\Exception
{

    /**
     * Create an instance of StorageEngineNotReadyException.
     *
     * @param string $message Details about the exception.
     * @param int $severity The severity of the exception.
     * @param \Exception $inner The exception in which this exception was thrown.
     */
    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("Storage engine not ready.  $message", $severity, $inner);
    }
}