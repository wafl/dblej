<?php
namespace DblEj\Data;

interface IWriteable
{
    public function Write($destination, $dataName, $dataValue);
}