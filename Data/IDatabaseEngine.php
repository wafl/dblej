<?php

namespace DblEj\Data;

/**
 * Represents a database engine.
 *
 * Database engines should implement this interface to be used as a
 * StorageEngine persistant data source.
 *
 * @deprecated since revision 1629 in favor of DblEj\Data\IDatabaseConnection
 * @see \DblEj\Data\IDatabaseConnection
 */
interface IDatabaseEngine
extends IStorageEngine
{
    /**
     * Whether or not there is an active session with this database engine.
     * @return boolean <i>True</i> if there is an active session with this database engine, otherwise <i>false</i>.
     */
    public function IsConnected();

    /**
     * Get string representing the last error that occurred in the data engine,
     * relative to this instance's connection.
     *
     * @return string the last error that occurred in the data engine,
     * relative to this instance's connection.
     */
    public function GetLastError();

    /**
     * In transactional database engines, this method starts a transaction.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function BeginTransaction();

    /**
     * In transactional database engines, this method commit's a transaction.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function CommitTransaction();

    /**
     * In transactional database engines, this method rolls back a transaction.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function RollbackTransaction();

    /**
     * In database engines that support engine or catalog-level locking,
     * this method will lock the specified name on the engine or catalog.
     *
     * @param string $lockname The name of the lock to get.
     * @param string $timeout The amount of time, in seconds, to wait for the lock before timing out.
     * This can help reduce/prevent waiting too long for a lock and potential dead-locks and
     * other race-conditions.
     *
     * @return The lock returned by the database engine.
     */
    public function GetLock($lockname, $timeout);

    /**
     * In database engines that support engine or catalog-level locking,
     * this method will return a boolean indicating if the engine or catalog
     * is currently locked by the specified </i>$lockname</i>.
     *
     * @param string $lockname The name of the lock to check for.
     * @return boolean Whether or not the lock exists and is currently active.
     */
    public function IsLocked($lockname);

    /**
     * In database engines that support engine or catalog-level locking,
     * This method will release the engine or catalog-level lock of the specified name.
     *
     * @param string $lockname The name of the lock to release.
     */
    public function ReleaseLock($lockname);

    /**
     * Get an array of data rows (represented by an associative array) from the
     * database engine based on the passed <i>$query</i>.
     * This method returns the entire result of the query and so memory should be
     * considered when using this method on large query's.  For larger queries, you
     * may want to use GetRows() instead and then fetch a single row at a time using <i>GetNextRow()</i>.
     *
     * @param string $query
     * A string that specifies the query that will return the desired rows.
     * For most common relational database engines, this is a SQL string.
     *
     * @param type $dbCatalog
     * The name of the catalog or database to run the query against.
     *
     * @return array
     * An array of associative sub-arrays where each sub-array represents a row of data.
     * Each element in the sub array is keyed by the column name and the value represents the value
     * for that column in that row.
     *
     * For tables with a primary key, the outer array will be keyed by the value in the primary key column.
     * For tables without a primary key, the outer array will be numerically indexed in sequence.
     */
    public function GetRowsAsArray($query, $dbCatalog = "");

    /**
     * Ask the engine for rows of data based on the specified <i>$query</i>.
     * For some database engines such as mySql, this will prepare the result of the query,
     * but will not actually return the result.
     * This can be useful for keeping memory consumption down on larger queries.
     *
     * To get the result data, you can use <i>GetNextRow</i> on the object returned by <i>GetRows</i> to get a single row at a time.
     *
     * @param string $query
     * A string that specifies the query that will return the desired rows.
     * For many common relational database engines, this is a SQL string.
     *
     * @param type $dbCatalog
     * The name of the catalog or database to run the query against.
     *
     * @return mixed
     * An object that identifies the result, which for some engines might be the result itself.
     * This object can subsequently be passed to GetNextRow() to get the next row of data.
     * Some engines will return the actual data with this method,
     * because it has no concept of "preparing" a result.
     * However, those implementation should still implement GetNextRow to get the next row
     * from that result.
     */
    public function GetRows($query, $dbCatalog = "");

    /**
     * Get the next row available in a result prepared by <i>GetRows</i>.
     *
     * @param mixed $rows A result from calling the <i>GetRows</i> method.
     * @return array An associative array representing the row of data where each array element's
     * key corresponds to the column name of the data in that element.
     */
    public function GetNextRow($rows);

    /**
     * Instruct the engine to switch to the specified catalog/database.
     *
     * @param string $dbCatalog The name of the catalog or database.
     */
    public function SetCatalog($dbCatalog);

    /**
     * Get an array of data representing the first row returned by the specified <i>$query</i>.
     *
     * @param string $query The query string used to specify which data to return.
     * @param string $dbCatalog The name of the database or catalog to run the query against.
     * If blank then the current catalog (the one last sent to <i>SetCatalog</i>) will be used.
     *
     * @return array An array of data representing the first row returned by the specified <i>$query</i>.
     */
    public function GetFirstRow($query, $dbCatalog = "");

    /**
     * Run a query that returns a scalar response, and return that value.
     *
     * @param string $query The query to run whose result will be a scalar value.
     * @param string $dbCatalog The name of the database or catalog to run the query against.
     * If blank then the current catalog (the one last sent to <i>SetCatalog</i>) will be used.
     *
     * @return mixed A single value as a string or a number.
     */
    public function GetScalar($query, $dbCatalog = "");

    /**
     * Execute a stored procedure that returns a scalar response, and return that value.
     *
     * @param string $procName The name of the stored procedure to run.
     *
     * @param string $argList
     * A string argument to be sent to the stored procedure.
     * This is usually a comma delimited string of argument values.
     *
     * @param string $dbCatalog The name of the database or catalog that contains the specified stored procedure.
     * If blank then the current catalog (the one last sent to <i>SetCatalog</i>) will be used.
     *
     * @return mixed A single value as a string or a number.
     */
    public function GetScalarSp($procName, $argList = "", $dbCatalog = "");

    /**
     * Ask the engine for rows of data returned by the specified stored procedure.
     * For some database engines such as mySql, this will prepare the result of the procedure,
     * but will not actually return the result.
     * This can be useful for keeping memory consumption down on larger queries.
     * For procedures that return more than one result, only the last result will be returned.
     *
     * To get the result data, you can use <i>GetNextRow</i> on the object returned by <i>GetRowsSP</i> to get a single row at a time.
     *
     * @param string $procName
     * The name of the stored procedure to run.
     *
     * @param string $argList
     * A string argument to be sent to the stored procedure.
     * This is usually a comma delimited string of argument values.
     *
     * @param string $dbCatalog
     * The name of the catalog or database to run the query against.
     *
     * @return mixed
     * An object that identifies the result, which for some engines might be the result itself.
     * This object can subsequently be passed to GetNextRow() to get the next row of data.
     * Some engines will return the actual data with this method,
     * because it has no concept of "preparing" a result.
     * However, those implementation should still implement GetNextRow to get the next row
     * from that result.
     */
    public function GetRowsSP($procName, $argList = "", $dbCatalog = "");

    /**
     * Get an array of data representing the first row returned by the specified stored procedure.
     *
     * @param string $procName
     * The name of the stored procedure to run.
     *
     * @param string $argList
     * A string argument to be sent to the stored procedure.
     * This is usually a comma delimited string of argument values.
     *
     * @param string $dbCatalog
     * The name of the catalog or database to run the query against.
     *
     * @return array An array of data representing the first row returned by the specified stored procedure.
     */
    public function GetFirstRowSp($procName, $argList = "", $dbCatalog = "");

    /**
     * Get an array of data rows (represented by an associative array) from the
     * database engine based on the passed stored procedure.
     * This method returns the entire result of the procedure and so memory should be
     * considered when using this method on procedures that return large data sets.
     * For larger data sets, you may want to use GetRowsSp() instead and then fetch
     * a single row at a time using <i>GetNextRow()</i>.
     *
     * @param string $procName
     * The name of the stored procedure to run.
     *
     * @param string $argList
     * A string argument to be sent to the stored procedure.
     * This is usually a comma delimited string of argument values.
     *
     * @param string $dbCatalog
     * The name of the catalog or database to run the procedure against.
     *
     * @return array
     * An array of associative sub-arrays where each sub-array represents a row of data.
     * Each element in the sub array is keyed by the column name and the value represents the value
     * for that column in that row.
     *
     * For tables with a primary key, the outer array will be keyed by the value in the primary key column.
     * For tables without a primary key, the outer array will be numerically indexed in sequence.
     */
    public function GetRowsSpAsArray($procName, $argList = "", $dbCatalog = "");

    /**
     * Execute the specified command.
     *
     * @param string $command
     * The command to execute.
     * This can be a SQL statment or whatever the database engine understands.
     *
     * @param string $dbCatalog
     * The name of the catalog or database to run the command against.
     *
     * @return boolean The implementing class should return <i>true</i> on success and </i>false</i> on failure.
     */
    public function Execute($command, $dbCatalog = "", $parameters = null);

    /**
     * Execute the specified stored procedure.
     *
     * @param string $procName
     * The name of the stored procedure to run.
     *
     * @param string $args
     * A string argument to be sent to the stored procedure.
     * This is usually a comma delimited string of argument values.
     *
     * @param string $dbCatalog
     * The name of the catalog or database that contains the procedure.
     *
     * @return boolean The implementing class should return <i>true</i> on success and </i>false</i> on failure.
     */
    public function ExecuteSP($procName, $args = "", $dbCatalog = "");

    /**
     * Get the primary key of the row that was last inserted into a table that
     * has a primary key defined.
     *
     * @return  mixed The id of the row that was last inserted during this session.
     */
    public function GetLastInsertId();

    /**
     * Get the number of rows affected by the last executed command in the current session (on the current connection).
     *
     * @return  int The number of rows affected by the last executed command.
     */
    public function GetLastAffectedCount();

    /**
     * Delete all rows from a table.
     * @param string $table The name of the table to truncate.
     *
     * @return boolean <i>True</i> on success and </i>false</i> on failure.
     */
    public function TruncateTable($table);

    /**
     * An engine-specific string-sanitizer that is responsible for escaping any necessary
     * characters in a string before that string is used as a value in an insert statement,
     * update statement, or a (where, join, group by) clause.
     *
     * @param string $string The string to be escaped.
     */
    public function EscapeString($string);

    /**
     * Insert a row into the specified table.
     *
     * @param string $table The table to insert the row into.
     * @param string[] $columnNames The names of the columns to populate when inserting the row.
     * @param string[] $columnValues The values to populate the columns specified by <i>$columnNames</i> with.
     */
    public function InsertRow($table, $columnNames, $columnValues);

    /**
     * Insert a new row into the specified table if the value for <i>$keyColumnName</i> is unique,
     * or update an existing row if there is already a row
     * that has the same value for the <i>$keyColumnName</i> column as the value
     * specified in <i>$columnValues[$keyColumnName]</i>.
     *
     * @param string $table The table to upsert the row into.
     * @param string[] $columnNames The names of the columns to populate when inserting the row.
     * @param string[] $columnValues The values to populate the columns specified by <i>$columnNames</i> with.
     * @param string $keyColumnName The name of the column to be used as a key when identifying
     * if a row already exists or not.
     * @param boolean $isAutoIncrementingKey
     * <i>True</i> if the key in the destination table is auto-generated by the engine,
     * otherwise <i>false</i>.
     * Some engines need this information to detect when an insert is a duplicate and should be an update instead.
     *
     * @return $boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function UpsertRow($table, $columnNames, $columnValues, $keyColumnName, $isAutoIncrementingKey);

    /**
     * Ask the data engine if the specified table exists.
     * @param string $tableName The name of the table to inquire about.
     * @return boolean <i>True</i> if the table exists, otherwise <i>false</i>.
     */
    public function DoesTableExist($tableName);

    /**
     * Set the character encoding to be used when transmitting data to/from this storage engine.
     * @param string $encoding A string identifying the character encoding to use such as "utf8".
     */
    public function SetConnectionCharacterEncoding($encoding);
}