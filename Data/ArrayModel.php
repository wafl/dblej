<?php

namespace DblEj\Data;

use DblEj\Data\InvalidDataFieldException;
use DblEj\System\InvalidArgumentException;

/**
 * A data model that uses a php associative array as the underlying data source.
 * Each element value is expected to be a string.
 *
 * @since 739
 */
class ArrayModel
implements IModel,
           \Countable
{
    private $_dataArray;

    /**
     * Create an instance of an ArrayModel
     *
     * @param array $dataModel An associative array to initialize this model with where each
     * element is a data point in the model named the same as the array element's key.
     */
    public function __construct(array $dataModel = array())
    {
        $this->_dataArray = $dataModel;
    }

    /**
     * @internal php Countable imeplementation
     */
    public function offsetExists($offset)
    {
        return key_exists($offset, $this->_dataArray);
    }

    /**
     * @internal php Countable imeplementation
     */
    public function offsetGet($offset)
    {
        return $this->_dataArray[$offset];
    }

    /**
     * @internal php Countable imeplementation
     */
    public function offsetSet($offset, $value)
    {
        $this->_dataArray[$offset] = $value;
    }

    /**
     * @internal php Countable imeplementation
     */
    public function offsetUnset($offset)
    {
        unset($this->_dataArray[$offset]);
    }

    /**
     * @internal php Countable imeplementation
     */
    public function count()
    {
        return count($this->_dataArray);
    }

    /**
     * Check whether the model has the specified data point.
     *
     * @param string $dataName The name of the data to check for.
     * @return boolean <i>True</i> if the data model has data pf the specified name, otherwise <i>false</i>.
     */
    public function HasFieldValue($dataName)
    {
        return array_key_exists($dataName, $this->_dataArray);
    }


    /**
     * Get the internal data storage for this data model
     */
    public function Get_DataSource()
    {
        return $this->_dataArray;
    }

    /**
     * An associate array of the values of this objects published fields,
     * keyed by the field names.  Part of the IFieldPublisher implementation.
     *
     * @return array
     */
    public function Get_FieldValues($autoEscapeStrings = false)
    {
        if ($autoEscapeStrings)
        {
            throw new InvalidArgumentException("autoEscapeStrings", "ArrayModel does not support auto escaping field values");
        }
        return array_values($this->_dataArray);
    }

    /**
     * This type of model does not have any static field names.
     * The field names are dependent on the underlying data of each instance.
     *
     * @return null
     */
    public static function Get_FieldNames()
    {
        return null;
    }

    /**
     * This type of model does not have any static field names.
     * The field names are dependent on the underlying data of each instance.
     *
     * @return boolean <i>False</i>
     */
    public static function Get_HasStaticFieldNames()
    {
        return false;
    }

    /**
     * The field names of a specific instance of an ArrayModel.
     *
     * @return string[] As array of field names.
     */
    public function Get_InstanceFieldNames()
    {
        return array_keys($this->_dataArray);
    }

    /**
     * Data point values in an array model are always strings,
     * so this method always returns Field::DATA_TYPE_STRING.
     */
    public static function GetFieldDataType($fieldName)
    {
        return Field::DATA_TYPE_STRING;
    }

    /**
     * Get the data types of the fields in this model.
     *
     * @return array An associative array where the key is the field name and the value
     * is the name of the data type.
     */
    public static function GetFieldDataTypes()
    {
        $returnArray = [];
        foreach (static::Get_FieldNames() as $fieldName)
        {
            $returnArray[$fieldName] = \DblEj\Data\Field::DATA_TYPE_STRING;
        }
        return $returnArray;
    }

    /**
     * @internal php Countable imeplementation
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_dataArray);
    }

    public function CanCurrentUserSetProperty($fieldName)
    {
        return true;
    }

    public function GetFieldValue($fieldName, $defaultValue = null)
    {
        $returnValue = $defaultValue;
        if (array_key_exists($fieldName, $this->_dataArray))
        {
            if ($this->_dataArray[$fieldName] !== null)
            {
                $returnValue = $this->_dataArray[$fieldName];
            }
        }
        else
        {
            throw new InvalidDataFieldException($this, $fieldName);
        }
        return $returnValue;
    }

    public function SetFieldValue($fieldName, $fieldValue)
    {
        $this->_dataArray[$fieldName] = $fieldValue;
    }

    public function SetFieldValues(array $fieldValues)
    {
        foreach ($fieldValues as $idx => $val)
        {
            $this->SetFieldValue($idx, $val);
        }
    }
}