<?php

namespace DblEj\Data;

/**
 * An interface to a search indexing service.
 */
interface IIndex
{

    /**
     * Connect to the index.
     *
     * @param string $serviceUri The URL to the index.  This can be a file path, a host name, an ip address, or any URI that identifies the index.
     * @param type $loginId The username/login id to use to authenticate to the index.
     * @param type $password The password to use to authenticate to the index.
     */
    public function Connect($serviceUri, $loginId = null, $password = null);

    /**
     * Add the specified <i>$indexableItem</i> to this index.
     *
     * @param \DblEj\Data\IIndexable $indexableItem
     * The item to index.
     * @return boolean Returns true on success and false on failure
     */
    public function Index(IIndexable $indexableItem);

    /**
     * Remove items from this index based on the specified query.
     *
     * @param string $indexerQuery The criteria for which items to unindex.
     */
    public function Unindex($indexerQuery);

    /**
     * Remove the item matching the specified unique id from the index.
     *
     * @param string $Uid The unique id of the item to unindex.
     */
    public function UnindexByUid($Uid);

    /**
     * Search for items in the index that match the search phrase.
     * @param string|array $fieldToSearchOn The name of the field to search on, or an array of the names of the fields to search on.
     * @param string|array $searchPhrase The name of the phrase to search or,
     * if <i>$fieldToSearchOn</i> is an array, an array of phrases to search for that correspond to those fields.
     * @param int $startOffset If the search returns multiple records, ignore the first <i>$startOffset</i> of them.
     * @param int $count The maximum number of records to return.
     * @param string[] $returnFields An array of the names of the fields to be returned.
     * @param IndexSort[] $sorts An array of IndexSort objects instructing the index on how to sort the results.
     * @param mixed $args Implementation specific arguments.
     */
    public function Search($fieldToSearchOn, $searchPhrase, $startOffset = 0, $count = null, array $returnFields = null, array $sorts = null, $args = null, &$totalAvailableCount = null);

    /**
     * Allows clients to check if this index is online.
     * The implementation should return a boolean indicating if the underlying service is online.
     *
     * @return boolean <i>True</i> if the service is online, otherwise <i>false</i>.
     */
    public function Ping();
}