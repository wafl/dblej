/*
 *@namespace DblEj.Data
 */
Namespace("DblEj.Data");

/*
 *@class ModelProperty Stores information about a property.
 *@extends Class
 */
DblEj.Data.ModelProperty = Class.extend(
    {
        /**
         * @constructor
         * Create an instance of a ModelProperty.
         *
         * @param {String} propertyName The name of the property.
         * @param {String} propertyId The unique id of the property.
         * @param {String} editorVisible
         * When showing the parent model in an editor, should this property be visible?
         *
         * @param {String} editorReadonly Editor Readonly
         * When showing the parent model in an editor, should this property be read-only?
         *
         * @param {String} getFunction The Model method for setting this property's value.
         * @param {String} setFunction The Model method for getting this property's value.
         * @param {String} displayOrder
         * The order to display this property, compared to the other properties, when displaying
         * this property among the other properties on this model.
         *
         * @param {String} dataTypeId
         * The id of the property's data type.
         *
         * @param {String} dataTypeArgs The arguments that the data type of the property value accept accept as construction arguments.
         *
         * @param {String} defaultValue string The default value for this property, when no value has been set yet.
         */
        init: function (propertyName, propertyId, editorVisible, editorReadonly, getFunction, setFunction, displayOrder, dataTypeId, dataTypeArgs, defaultValue)
        {
            this.PropertyName = propertyName;
            this.DataTypeArgs = dataTypeArgs;
            this.DefaultValue = defaultValue;
            this.PropertyId = propertyId;
            this.DataTypeId = dataTypeId;
            this.EditorVisible = editorVisible;
            this.EditorReadonly = editorReadonly;
            this.GetFunction = getFunction;
            this.SetFunction = setFunction;
            this.DisplayOrder = displayOrder;
        }
    });

/**
 * @internal
 * Comparison function for sorting model properties.
 *
 * @param {String} a
 * @param {String} b
 * 
 * @returns {Number}
 */
DblEj.Data.ModelProperty.CompareByDisplayOrder = function (a, b)
{
    if (typeof a[1] == "undefined" && typeof b[1] == "undefined")
    {
        return 0;
    }
    else if (typeof a[1] != "undefined" && typeof b[1] == "undefined")
    {
        return 1;
    }
    else if (typeof a[1] == "undefined" && typeof b[1] != "undefined")
    {
        return -1;
    }
    else if (a[1].DisplayOrder > b[1].DisplayOrder)
    {
        return 1;
    }
    else if (a[1].DisplayOrder < b[1].DisplayOrder)
    {
        return -1;
    }
    else
    {
        return 0;
    }
};