<?php
namespace DblEj\Data;

/**
 * Thrown when there is a data-related exception.
 */
class DataException
extends \DblEj\System\Exception
{

    /**
     * Create an instance of a DataException.
     * @param string $message An explanation of the exception.
     * @param int $severity The severity of the exception.
     * @param \Exception $inner The Exception, if any, that threw this DataException.
     */
    public function __construct($message, $severity = E_ERROR, \Exception $inner = null)
    {
        parent::__construct($message, $severity, $inner);
    }
}