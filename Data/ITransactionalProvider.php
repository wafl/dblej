<?php
namespace DblEj\Data;

interface ITransactionalProvider
{
    /**
     * Start a transaction.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function BeginTransaction();

    /**
     * In Commit a transaction.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function CommitTransaction();

    /**
     * Rolls back transaction.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function RollbackTransaction();
}
