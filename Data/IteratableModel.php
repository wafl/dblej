<?php

namespace DblEj\Data;

abstract class IteratableModel
extends DeletableModel
{

    /**
     * @internal interator related
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->Get_SerializableFieldValues());
    }

    /**
     * @internal interator related
     * @return \ArrayIterator
     */
    public function offsetExists($offset)
    {
        if (is_numeric($offset))
        {
            return count($this->Get_FieldValues()) > $offset;
        } else {
            return array_search($offset, $this->Get_InstanceFieldNames()) !== false;
        }
    }

    /**
     * @internal interator related
     * @return \ArrayIterator
     */
    public function offsetGet($offset)
    {
        return array_values($this->Get_FieldValues())[$offset];
    }

    /**
     * @internal interator related
     * @return \ArrayIterator
     */
    public function offsetSet($offset, $value)
    {
        $key = array_keys($this->Get_FieldValues())[$offset];
        return $this->GetFieldValue($key);
    }

    /**
     * @internal interator related
     * @return \ArrayIterator
     */
    public function offsetUnset($offset)
    {
        $key = array_keys($this->Get_FieldValues())[$offset];
        $this->SetFieldValue($key, null);
    }
}