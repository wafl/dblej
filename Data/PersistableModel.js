/*
 *@namespace DblEj.Data
 */
Namespace("DblEj.Data");

/*
 *@class PersistableModel An observable and persistable base-class for data model's.
 *Instances of this class can be observed with <i>ObserveMe<i> and persisted with <i>SaveToServer</i>.
 *@extends DblEj.Data.Model
 *@abstract
 */
DblEj.Data.PersistableModel = DblEj.Data.Model.extend(
{
    /**
     * @method SaveToServer Persist this instance to the server specified in DblEj.Communication.Ajax.Utils.Set_HandlerUrl
     * @param completedCallback function The function to call when the model has been saved
     * @param callbackToken string A string that can be used to identify every time you save this model.  This value will be passed along to the callback for identification.
     * @return void
     */
    SaveToServer: function (completedCallback, callbackToken)
    {
        if (typeof callbackToken == 'undefined' || typeof callbackToken == "undefined")
        {
            callbackToken = "";
        }
        DblEj.Communication.Ajax.Utils.SendRequestAsync("DblEj.Data.Model.UpdateModel", this, callbackToken, completedCallback);
    }
});