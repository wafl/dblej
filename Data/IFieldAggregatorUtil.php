<?php

namespace DblEj\Data;

/**
 * Interface for a class that exposes static methods for aggregating data from
 * multiple different instances of that class.
 */
interface IFieldAggregatorUtil
{

    /**
     * Gets the sum of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to sum.
     *
     * @param string $filter
     * The criteria that determines which records are included when summing.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function Sum($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null);

    /**
     * Gets the average of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to average.
     *
     * @param string $filter
     * The criteria that determines which records are included when averaging.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function Average($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null);

    /**
     * Gets the minimum value of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to get the minimum value for.
     *
     * @param string $filter
     * The criteria that determines which records are included when finding the minimum value.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function GetMinimum($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null);

    /**
     * Gets the maximum value of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to get the maximum value for.
     *
     * @param string $filter
     * The criteria that determines which records are included when finding the maximum value.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function GetMaximum($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null);

    /**
     * Get the number of matches there are for the given criteria.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are counted.
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinFieldDefinitions optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @return int The number of matching objects.
     */
    public static function Count($filter = null, $groupingField = null, $joinFieldDefinitions = null);
}