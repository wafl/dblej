<?php

namespace DblEj\Data;

/**
 * An interface for classes who's instances can be persisted.
 */
interface IPersistable
extends IFieldPublisher
{

    /**
     * The name of the destination where objects of this type should be persisted to, when posssible.
     */
    public static function Get_Destination();

    /**
     * Delete this object from it's persisted state.
     * @param boolean $checkPermissions <i>True</i> if a method should be called checking the current user's rights to save this object.
     */
    public function Delete($checkPermissions = false);

    /**
     * Load this object's properties from the underlying storage engine.
     * @param boolean $checkPermissions <i>True</i> if a method should be called checking the current user's rights to save this object.
     */
    public function Load($checkPermissions = false);

    /**
     * Save this object to the underlying storage engine.
     * @param boolean $checkPermissions <i>True</i> if a method should be called checking the current user's rights to save this object.
     */
    public function Save($checkPermissions = false);
}