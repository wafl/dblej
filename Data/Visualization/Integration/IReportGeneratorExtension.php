<?php
namespace DblEj\Data\Visualization\Integration;

interface IReportGeneratorExtension
extends IReportGenerator, \DblEj\Extension\IExtension
{
}