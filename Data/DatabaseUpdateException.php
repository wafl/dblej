<?php

namespace DblEj\Data;

/**
 * Thrown when there is an unexpected event related to a database update.
 */
class DatabaseUpdateException
extends DataException
{

    /**
     * Create an instance of a DatabaseUpdateException.
     * @param string $message Additional details about the exception.
     * @param int $severity The severity of the exception.
     * @param \Exception $inner The Exception, if any, that threw this DatabaseUpdateException.
     */
    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("There was an error performing the data update.  $message", $severity, $inner);
    }
}