<?php

namespace DblEj\Data;

/**
 * Represents a set of data of some defined structure.
 */
interface IModel
extends \DblEj\Data\IFieldPublisher
{
    /**
     * Set the value for multiple fields.
     *
     * @param array $fieldValues An associative array of the values to be applied to the fields in this data, where each array element's key corresponds to a field name.
     */
    public function SetFieldValues(array $fieldValues);

    /**
     * Set the value of the field specified by <i>$fieldName</i> to the value specified by <i>$fieldValue<i>.
     *
     * @param string $fieldName The name of the field to set the value for.
     * @param string $fieldValue The value to set the the specified field to.
     */
    public function SetFieldValue($fieldName, $fieldValue);

    /**
     * Get the value of the field specified by <i>$fieldName</i>.
     *
     * @param string $fieldName The name of the field to get the value for.
     * @param string $defaultValue What to return if the specified field is not currently set
     */
    public function GetFieldValue($fieldName, $defaultValue = null);

    /**
     * Does the specified field exist in this data?
     * @param string $fieldName
     */
    public function HasFieldValue($fieldName);

    /**
     * Get the underlying data source for this data model.
     * This can be a data storage instance, a file, an array, an object, or any other source of data.
     */
    public function Get_DataSource();

    public function CanCurrentUserSetProperty($fieldName);
}