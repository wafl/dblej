<?php

namespace DblEj\Data;

/**
 * Represents an indexable object.
 *
 * An indexable object is one which can be indexed in an <i>IIndex</i>.
 */
interface IIndexable
{
    /*
     * The unique id for this object.
     */

    public function Get_Uid();

    /**
     * The data to be indexed for this object.
     */
    public function GetIndexableData();

    /**
     * Add this indexable item to the specified index.
     *
     * @param \DblEj\Data\IIndex $searchIndex
     * The index to add this item to.  If <i>null</i>, then the
     * default index for this type of indexable item will be used (if it exists).
     *
     * @return boolean <i>True</i> on success and <i>False</i> on failure.
     */
    public function Index(IIndex $searchIndex = null);

    /**
     * The name of the index that this item prefers to be indexed in, by default.
     * This is where this item will be indexed by <i>IIndexer</i>s
     * if no other index is specified during indexing operations.
     */
    public static function Get_DefaultIndexId();

    /**
     * Search an index for items matching the given criteria and return the matching items.
     *
     * @param string $searchFieldName The name of the field in the index to search on.
     * @param string $searchValue The indexed value to search for.
     * @param \DblEj\Data\IndexSort[] $sorts How to sort the results.  Multiple sorts can be passed as an array.
     * @param \DblEj\Data\IIndex $searchIndex Which index to seach.  If not provided, the default search index will be used.
     *
     * @return IIndexable[] An array of the matching items.
     */
    public static function Search($searchFieldName, $searchValue, $sorts = null, $maxResults = 100, $startOffset = 0, $resultKeyField = null, IIndex $searchIndex = null, $returnRaw = false);
}