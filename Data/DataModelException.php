<?php

namespace DblEj\Data;

/**
 * Thrown when there is an unexpected event related to an operation dealing with a data model.
 */
class DataModelException
extends DataException
{

    /**
     * Create an instance of a DataModelException.
     * @param string $message An explanation of the exception.
     * @param int $severity The severity of the exception.
     * @param \Exception $inner The Exception, if any, that threw this DataModelException.
     */
    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct($message, $severity, $inner);
    }
}