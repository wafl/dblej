<?php
namespace DblEj\Data;

interface IUniqueWriter
extends IWriteable
{
    /**
     * Get the unique id of the data that was last written
     *
     * @return  mixed The id of the data that was last written during this session.
     */
    public function GetLastWriteId();
}
