<?php

namespace DblEj\Data;

/**
 * A data field or its value was requested on an object which does not contain it
 */
class InvalidDataFieldException
extends DataException
{

    /**
     * Construct an InvalidDataFieldException.
     * @param mixed $dataObjectOrName The object or the name of the item that was invalid.
     * @param string $dataFieldname The name of the field.
     * @param integer $severity The severity of the exception.
     */
    public function __construct($dataObjectOrName, $dataFieldname, $severity = E_ERROR)
    {
        if (is_object($dataObjectOrName))
        {
            $dataObjectOrName = get_class($dataObjectOrName);
        }
        parent::__construct("Invalid data field: $dataObjectOrName::$dataFieldname", $severity, null);
    }
}