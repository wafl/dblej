<?php

namespace DblEj\Data;

use DblEj\EventHandling\EventInfo;

/**
 * Represents an event that fires when a script has executed.
 */
class ScriptExecutedEvent
extends EventInfo
{
    const DATA_SCRIPT_EXECUTED = 2001;

    private $_scriptOrdinal    = 0;
    private $_totalScriptCount = 0;

    /**
     * Create an instance of a ScriptExecutedEvent.
     *
     * @param object $appliesToObject The object this event applies to.
     * @param type $scriptOrdinal The ordinal of this script among other scripts in a batch.
     * @param type $totalScriptCount The total number of scripts in the batch.
     * @param type $promptedByObject The object that prompted this event.
     */
    function __construct($appliesToObject, $scriptOrdinal, $totalScriptCount, $promptedByObject = null)
    {
        $this->_scriptOrdinal    = $scriptOrdinal;
        $this->_totalScriptCount = $totalScriptCount;
        $message                 = "Script $scriptOrdinal of $totalScriptCount completed";
        parent::__construct(self::DATA_SCRIPT_EXECUTED, $appliesToObject, $promptedByObject, $message);
    }

    /**
     * The ordinal of this script among other scripts in a batch.
     *
     * @return int
     */
    public function Get_ScriptOrdinal()
    {
        return $this->_scriptOrdinal;
    }

    /**
     * The total number of scripts in the batch.
     *
     * @return int
     */
    public function Get_TotalScriptCount()
    {
        return $this->_totalScriptCount;
    }
}