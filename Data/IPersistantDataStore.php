<?php

namespace DblEj\Data;

/**
 * A data store who's data can be saved to and restored from a persistant storage medium.
 */
interface IPersistantDataStore
extends IDataStore
{

    /**
     * Save the data to the data store.
     */
    public function Persist();

    /**
     * Load data from the data store.
     */
    public function Load();
}