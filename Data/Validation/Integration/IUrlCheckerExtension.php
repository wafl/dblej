<?php
namespace DblEj\Data\Validation\Integration;

interface IUrlCheckerExtension
extends IUrlChecker, \DblEj\Extension\IExtension
{
}