<?php

namespace DblEj\Data;

/**
 * Represents an application or service that indexes information.
 * An IIndexer has IIndexes which can store IIndexables.
 *
 * @deprecated since revision 1629 in favor of DblEj\Data\Integration\IIndexer
 * @see \DblEj\Data\Integration\IIndexer
 */
interface IIndexer
{

    /**
     * Get the index that has the specified id.
     * @param string $indexId The id of the IIndex to retrieve.
     *
     * @return \DblEj\Data\IIndex
     */
    public function GetIndex($indexId);

    /**
     * Check if this indexer has an index of the specified id.
     *
     * @param string $indexId The id of the IIndex to retrieve.
     *
     * @return boolean <i>True</i> if the index exists, otherwise <i>false</i>.
     */
    public function HasIndex($indexId);

    /**
     * Get the name of this indexer.
     */
    public function GetTitle();

    /**
     * Add an index to the internal list of indexes.
     * Note: This method does not add an index to the underlying indexer service.
     *
     * @param string $indexId The unique id used to refer to this index.
     * @param \DblEj\Data\IIndex $index The index to add.
     *
     * @return void
     */
    public function AddIndex($indexId, IIndex $index);

    /**
     * Add the <i>$indexableItem</i> to the specified index.
     *
     * @param IIndex $index
     * The index to add the item to.
     *
     * @param IIndexable $indexableItem
     * The item to index.
     *
     * @return boolean <i>True</i> if the index exists, otherwise <i>false</i>.
     */
    public function Index(IIndex $index, IIndexable $indexableItem);

    /**
     * Remove items from this index based on the specified query.
     *
     * @param string $indexerQuery The criteria for which items to unindex.
     */
    public function Unindex(IIndex $index, $Uid);

    /**
     * Search for items in the specified index where the specified field matches the specified search phrase.
     * @param string|array $fieldToSearchOn The name of the field to search on, or an array of the names of the fields to search on.
     * @param string|array $searchPhrase The name of the phrase to search or,
     * if <i>$fieldToSearchOn</i> is an array, an array of phrases to search for that correspond to those fields.
     * @param int $startOffset If the search returns multiple records, ignore the first <i>$startOffset</i> of them.
     * @param int $count The maximum number of records to return.
     * @param string[] $returnFields An array of the names of the fields to be returned.
     * @param IndexSort[] $sorts An array of IndexSort objects instructing the index on how to sort the results.
     * @param mixed $args Implementation specific arguments.
     */
    public function Search(IIndex $index, $fieldToSearchOn, $searchPhrase, $startOffset = 0, $count = null, $returnFields = null, $sorts = null, $args = null);
}