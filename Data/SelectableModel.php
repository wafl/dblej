<?php
namespace DblEj\Data;

abstract class SelectableModel
extends Model
implements IFieldAggregatorUtil,
           IFilterable,
           IIndexable,
           IInitializableStatic,
           IKeyedModel
{
    protected $amConstructing              = true;
    protected static $_sharedStorageEngines = array();
    protected static $_currentUser          = null;
    /**
     * @var \DblEj\Data\IIndexer
     * An optional search indexer that can be attached to the model
     * to provide static search against indexed models.
     */
    protected static $sharedSearchEngine;

    protected static $filterResultCache       = [];
    protected static $aggregateResultCache    = [];
    protected static $modelInstanceCache      = [];

    /**
     * Create an instance of the PersistableModel.
     *
     * @param string $keyValue
     * OPTIONAL
     * A key used to identify this model.
     * Default = null
     *
     * @param array $objectData
     * OPTIONAL
     * An array of data used to initialize this model.
     * This is useful if you have a row of data in memory and want to instantiate a model
     * using the row as the starting property values.
     * Default = null
     *
     * @param \DblEj\Data\Integration\IDatabaseServerExtension $storageEngine
     * DEPRECATED
     * OPTIONAL
     * The underlying storage engine to use for data persistence. (Ignored)
     * Default = null
     *
     * @param string $dataGroup
     * OPTIONAL An arbitrary label used to group model's such that they may be referenced together as a group.
     * Default = null
     *
     * @throws \DblEj\System\InstantiationFailedException
     * Thrown if the passed storage engine is null, and there is no shared storage engine available.
     *
     * @throws \DblEj\Coding\InvalidArgumentException
     * Thrown if an invalid shared storage engine was found.
     */
    public function __construct($keyValue = null, array $objectData = null, IDatabaseConnection $storageEngine = null, $dataGroup = null)
    {
        try
        {
            parent::__construct($objectData);

            $className = get_called_class();
            if (!isset(self::$filterResultCache[$className]))
            {
                self::$filterResultCache[$className] = [];
            }
            if (!isset(self::$modelInstanceCache[$className]))
            {
                self::$modelInstanceCache[$className] = [];
            }
            if (!isset(self::$aggregateResultCache[$className]))
            {
                self::$aggregateResultCache[$className] = [];
            }
            if (!$dataGroup)
            {
                $dataGroup = self::Get_StorageGroup();
            }
            $storageEngine = self::getStorageEngine();
            if ($storageEngine == null)
            {
                throw new \DblEj\System\InstantiationFailedException
                (
                    "Can't create the instance of $className because there is no data storage setup to handle this model group ("
                    . $dataGroup . "), and because you didn't pass in a StorageEngine explicitly.  "
                    . "You can fix this by either defining a storage group that has a <em>ModelGroup</em> of \""
                    . self::Get_StorageGroup() . "\" or pass in the \$storageEngine argument when instantiating <em>$className</em> objects."
                    . "Another option is to recreate the $className class and put it into a different data group."
                );
            }
            if ($storageEngine == null)
            {
                throw new \DblEj\Coding\InvalidArgumentException("You should set the PersistableModel::SharedStorageEngines");
            }
            if ($keyValue && !$objectData)
            {
                $this->SetFieldValue($className::Get_KeyFieldName(), $keyValue);
                $this->Load();
            }
            if ($objectData)
            {
                $this->SetFieldValues($objectData);
            }
            if ($this->Get_KeyFieldValue())
            {
                self::$modelInstanceCache[$className][$this->Get_KeyFieldValue()] = $this;
            }
        }
        catch (\Exception $e)
        {
            $className = get_called_class();
            $this->amConstructing = false;
            throw new \DblEj\System\InstantiationFailedException("Exception thrown when trying to create a $className object. ".$e->getMessage(), E_ERROR, $e);
        }
        $this->amConstructing = false;
    }

    /**
     * Does nothing.
     */
    public static function Initialize()
    {

    }

    /**
     * Get an instance of this model, loading from persistant storage when needed.
     *
     * @param string $keyValue
     * @param boolean $loadFromCacheIfAvailable
     * @return \DblEj\Data\PersistableModel
     */
    public static function GetInstance($keyValue = null, $loadFromCacheIfAvailable = true)
    {
        $className = get_called_class();
        if ($keyValue && $loadFromCacheIfAvailable && isset(self::$modelInstanceCache[$className]) && isset(self::$modelInstanceCache[$className][$keyValue]))
        {
            return self::$modelInstanceCache[$className][$keyValue];
        } else {
            return new $className($keyValue);
        }
    }

    /**
     * Specify the user that should be used when authorizing read/write operations on this model.
     *
     * @param \DblEj\Authentication\IUser $user
     */
    public static function SetCurrentUser(\DblEj\Authentication\IUser $user = null)
    {
        self::$_currentUser = $user;
    }

    /**
     * Get the data type for the specified field.
     *
     * @param string $fieldName The name of the field to get the data type for.
     * @return string One of the the Field::DATA_TYPE* constants.
     * @throws \Exception Thrown if the specified field does not exist.
     */
    public static function GetFieldDataType($fieldName)
    {
        $subclass = get_called_class();
        $types    = $subclass::GetFieldDataTypes();
        if (!isset($types[$fieldName]))
        {
            throw new \Exception("Invalid field requested from $subclass: $fieldName");
        }
        return $types[$fieldName];
    }

    protected static function _searchIds($searchFieldName, $searchValue, $sorts = null, $resultKeyField = null, $maxResults = 100, $startOffset = 0, IIndex $searchIndex = null, $indexArgs = null, &$totalAvailableCount = null)
    {
        $subclass = get_called_class();
        if ($searchIndex == null)
        {
            if (self::$sharedSearchEngine != null)
            {
                $searchIndex = self::$sharedSearchEngine->GetIndex($subclass::Get_DefaultIndexId());
            }
            else
            {
                throw new \Exception("searchIndex was not passed in, and a shared instance could not be found");
            }
        }
        $itemArray = array();
        if (!$resultKeyField)
        {
            $resultKeyField = $subclass::Get_KeyFieldName();
        }
        $results   = $searchIndex->Search($searchFieldName, $searchValue, $startOffset, $maxResults, array($resultKeyField), $sorts, $indexArgs, $totalAvailableCount);
        foreach ($results as $result)
        {
            $itemArray[] = $result[$resultKeyField];
        }
        return $itemArray;

    }
    /**
     * _search
     *
     * To be used by subclasses public Search() method.
     * this allows callers to statically call Search() on the subclass
     * and executes this code.
     * The sole purposes of this extra step is to allow the sub-classes to provide
     * in-context php-docs.
     *
     * @param string $searchFieldName
     * @param string $searchValue
     * @param \DblEj\Data\IndexSort[] $sorts
     * @param string $resultKeyField
     * @param int $maxResults
     * @param int $startOffset
     * @param \DblEj\Data\IIndex $searchIndex
     * @return \DblEj\Data\PersistableModel[]
     * @throws \Exception
     */
    protected static function _search($searchFieldName, $searchValue, $sorts = null, $resultKeyField = null, $maxResults = 100, $startOffset = 0, IIndex $searchIndex = null, $indexArgs = null, $returnRaw = false, &$totalAvailableCount = null)
    {
        $subclass = get_called_class();
        if ($searchIndex == null)
        {
            if (self::$sharedSearchEngine != null)
            {
                $searchIndex = self::$sharedSearchEngine->GetIndex($subclass::Get_DefaultIndexId());
            }
            else
            {
                throw new \Exception("searchIndex was not passed in, and a shared instance could not be found");
            }
        }
        $itemArray = array();
        if (!$resultKeyField)
        {
            $resultKeyField = $subclass::Get_KeyFieldName();
        }
        $indexArgs["ReturnRaw"] = $returnRaw;
        $results   = $searchIndex->Search($searchFieldName, $searchValue, $startOffset, $maxResults, array($resultKeyField), $sorts, $indexArgs, $totalAvailableCount);
        if ($returnRaw)
        {
            return $results;
        } else {
            foreach ($results as $result)
            {
                $instance = new $subclass($result[$resultKeyField]);
                if ($instance->Get_Uid())
                {
                    $itemArray[] = $instance;
                }
            }
            return $itemArray;
        }
    }

    protected static function _searchCount($searchFieldName, $searchValue, IIndex $searchIndex = null, $indexArgs = null)
    {
        $results   = self::Search($searchFieldName, $searchValue, null, null, 9999999, 0, null, $indexArgs, true);
        return $results?$results["response"]["numFound"]:0;
    }

    /**
     * Search a search-index for instances of this class based on the specified criteria.
     *
     * @param string|array $searchFieldName The name of the field in the search index to search on, or an array of the names of the fields to search on.
     * This may or may not correspond to a model property or table column.
     * if <i>$fieldToSearchOn</i> is an array, an array of phrases to search for that correspond to those fields.
     *
     * @param string|array $searchValue The value to search for.
     *
     * @param IndexSort[] $sorts The <i>IndexSort</i> instructions for sorting the search results.
     *
     * @param \DblEj\Data\IIndex $searchIndex The search index to search in.
     *
     * @param int $maxResults The maximum number of results to return from the search.
     * @param int $startOffset Ignore the first <i>$startOffset</i> search results.
     * @return type
     */
    public static function Search($searchFieldName, $searchValue, $sorts = null, $resultKeyField = null, $maxResults = 100, $startOffset = 0, IIndex $searchIndex = null, $indexArgs = null, $returnRaw = false, &$totalAvailableCount = null)
    {
        return self::_search($searchFieldName, $searchValue, $sorts, $resultKeyField, $maxResults, $startOffset, $searchIndex, $indexArgs, $returnRaw, $totalAvailableCount);
    }

    public static function SearchIds($searchFieldName, $searchValue, $sorts = null, $resultKeyField = null, $maxResults = 100, $startOffset = 0, IIndex $searchIndex = null, $indexArgs = null, &$totalAvailableCount = null)
    {
        return self::_searchIds($searchFieldName, $searchValue, $sorts, $resultKeyField, $maxResults, $startOffset, $searchIndex, $indexArgs, $totalAvailableCount);
    }

    public static function SearchCount($searchFieldName, $searchValue, IIndex $searchIndex = null, $indexArgs = null)
    {
        return self::_searchCount($searchFieldName, $searchValue, $searchIndex, $indexArgs);
    }

    /**
     * Add this model to the specified search index.
     * Only the data returned from <i>GetIndexableData</i> is indexed.
     *
     * @param \DblEj\Data\IIndex $searchIndex The search index to add this model to.
     */
    public function Index(IIndex $searchIndex = null)
    {
        $subclass = get_called_class();
        if ($searchIndex == null)
        {
            if (self::$sharedSearchEngine->HasIndex($subclass::Get_DefaultIndexId()))
            {
                $searchIndex = self::$sharedSearchEngine->GetIndex($subclass::Get_DefaultIndexId());
            }
        }
        if ($searchIndex)
        {
            return $searchIndex->Index($this);
        }
        else
        {
            throw new \Exception("Cannot index the model " . get_called_class() . " because it specifies an index that does not exist (" . $subclass::Get_DefaultIndexId() . ").");
        }
    }

    /**
     * Unindex a model
     * @param \DblEj\Data\IIndex $searchIndex
     * @throws \Exception
     */
    public function Unindex(IIndex $searchIndex = null)
    {
        $subclass = get_called_class();
        if ($searchIndex == null)
        {
            if (self::$sharedSearchEngine->HasIndex($subclass::Get_DefaultIndexId()))
            {
                $searchIndex = self::$sharedSearchEngine->GetIndex($subclass::Get_DefaultIndexId());
            }
        }
        if ($searchIndex)
        {
            $searchIndex->UnindexByUid($this->Get_Uid());
        }
        else
        {
            throw new \Exception("Cannot unindex the model " . get_called_class() . " because it specifies an index that does not exist (" . $subclass::Get_DefaultIndexId() . ").");
        }
    }

    /**
     * Add a StorageEngine to the internal list of StorageEngines used for loading and persisting data models.
     *
     * If there are multiple storage engines, models will, by default, be stored in the storage engine
     * with a <i>ModelGroup</i> that matches that model's <i>StorageGroup</i>.
     *
     * @param \DblEj\Data\Integration\IDatabaseServerExtension $storageEngine The storage engine to add.
     */
    public static function AddStorageEngine(IDatabaseConnection $storageEngine)
    {
        self::$_sharedStorageEngines[$storageEngine->Get_ModelGroup()] = $storageEngine;
    }

    /**
     * Specify the search Indexer that will be used by this model for search indexing operations.
     *
     * @param \DblEj\Data\IIndexer $indexingService The search index.
     */
    public static function SetSearchIndexer(Integration\IIndexer $indexingService)
    {
        self::$sharedSearchEngine = $indexingService;
    }

    public static function InvalidateAllCaches()
    {
        $subclass = get_called_class();

        //invalidate any caches with this type in it
        if (isset(self::$modelInstanceCache[$subclass]))
        {
            unset(self::$modelInstanceCache[$subclass]);
        }

        unset(self::$filterResultCache[$subclass]);
        unset(self::$aggregateResultCache[$subclass]);
    }

    /**
     * @deprecated since 770
     */
    public static function Get_StorageLocation()
    {
        $subclass = get_called_class();
        return $subclass::Get_Destination();
    }

    /**
     * @deprecated since 770
     */
    public static function Get_KeyValueIsGeneratedByEngine()
    {
        $subclass = get_called_class();
        return $subclass::Get_KeyIsAutoGenerated();
    }

    /**
     * Load the data model from persistent data storage.
     *
     * @throws StorageEngineNotReadyException
     */
    public function Load($checkPermissions = false)
    {
        if (!$checkPermissions || $this->DoesCurrentUserHaveReadAccess())
        {
            $storageEngine = self::getStorageEngine();
            if ($storageEngine && $storageEngine->IsReady())
            {
                $subclass   = get_called_class();
                $loadedData = $storageEngine->GetData($subclass::Get_StorageLocation(), $subclass::Get_KeyFieldName(), $this->GetFieldValue($subclass::Get_KeyFieldName()));
                if ($loadedData)
                {
                    $this->SetFieldValues($loadedData);
                    $this->_dirtyFields = array();
                }
                else
                {
                    $this->SetFieldValue($subclass::Get_KeyFieldName(), null);
                }
            }
            else
            {
                throw new StorageEngineNotReadyException("Attempt to load data from storage engine when it is not ready", E_WARNING);
            }
        } else {
            throw new \Exception("User is not authorized to read this data", E_WARNING);
        }
    }


    protected function markAllFieldsClean()
    {
        $this->_dirtyFields = array();
    }

    /**
     * An associative array of the values of the published fields,
     * keyed by the field names.
     *
     * @return array
     */
    public static function Get_FieldNames($storageEngine = null)
    {
        $subclass = get_called_class();
        if (!isset($subclass::$_fieldNames[$subclass]))
        {
            if ($storageEngine == null)
            {
                $storageEngine = self::getStorageEngine();
            }
            $fields = $storageEngine->GetDataFields($subclass::Get_StorageLocation());
            foreach ($fields as $field)
            {
                $subclass::$_fieldNames[$subclass][] = $field->Get_Title();
            }
        }
        return $subclass::$_fieldNames[$subclass];
    }

    /**
     * Get the average value for the specified field among all models that match the given filter.
     *
     * @param string $fieldName
     * The name of the field to average.
     *
     * @param string $filter
     * The criteria that determines which records are included when averaging.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function Average($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null, $useCachedIfAvailable = true)
    {
        $subclass = get_called_class();
        $cacheKey = "avg-$fieldName-" . ($filter?$filter."-":"") . ($groupingField?$groupingField."-":"");
        if ($joinFieldDefinitions)
        {
            $cacheKey .= serialize($joinFieldDefinitions);
        }
        $cacheKey = md5($cacheKey);
        $avg = null;
        if ($useCachedIfAvailable && isset(self::$aggregateResultCache[$subclass]) && isset(self::$aggregateResultCache[$subclass][$cacheKey]))
        {
            $avg = self::$aggregateResultCache[$subclass][$cacheKey];
        }
        if (!$avg)
        {
            $avg = self::getStorageEngine()->GetDataGroupAvg($subclass::Get_StorageLocation(), $fieldName, $filter, $groupingField, $joinFieldDefinitions);
        }

        if (!isset(self::$aggregateResultCache[$subclass]))
        {
            self::$aggregateResultCache[$subclass] = [];
        }
        self::$aggregateResultCache[$subclass][$cacheKey] = $avg;
        return $avg;
    }

    /**
     * Get the number of matches there are for the given criteria.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are counted.
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinFieldDefinitions optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @return int The number of matching objects.
     */
    public static function Count($filter = null, $groupingField = null, $joinFieldDefinitions = null, $useCachedIfAvailable = true)
    {
        $subclass = get_called_class();
        $cacheKey = "count-" . ($filter?$filter."-":"") . ($groupingField?$groupingField."-":"");
        if ($joinFieldDefinitions)
        {
            $cacheKey .= serialize($joinFieldDefinitions);
        }
        $cacheKey = md5($cacheKey);

        $count = null;
        if ($useCachedIfAvailable && isset(self::$aggregateResultCache[$subclass]) && isset(self::$aggregateResultCache[$subclass][$cacheKey]))
        {
            $count = self::$aggregateResultCache[$subclass][$cacheKey];
        }
        if (!$count)
        {
            $count = self::getStorageEngine()->GetDataGroupCount($subclass::Get_StorageLocation(), $filter, $groupingField, $joinFieldDefinitions);
        }
        if (!isset(self::$aggregateResultCache[$subclass]))
        {
            self::$aggregateResultCache[$subclass] = [];
        }
        self::$aggregateResultCache[$subclass][$cacheKey] = $count;

        return $count;
    }

    private static function _validateStorageEngine()
    {
        $subclass = get_called_class();
        if (count(self::$_sharedStorageEngines) < 1)
        {
            throw new DataModelException("Before you can load models you must tell DblEj what the active DataStorage engine is.  Use PersistableModel::SharedStorageEngine to set the active StorageEngine");
        }
        if (!isset(self::$_sharedStorageEngines[$subclass::Get_StorageGroup()]))
        {
            throw new DataException("Cannot execute select statment because there is no appropriate data storage.  (Looking for model group " . $subclass::Get_StorageGroup() . ")");
        }
    }

    private static function _getCacheKey($filter, $orderByFieldName, $maxRecordCount, $startOffset, $joinObjects, $groupingField, $arrayKeyField)
    {
        if ($orderByFieldName == "rand()") //instead of being hard-coded we should ask the data driver what his random sort keyword is
        {
            $cacheKey = null;
        } else {
            $cacheKey = $filter;
            if ($orderByFieldName)
            {
                $cacheKey .= $orderByFieldName;
            }
            if ($maxRecordCount)
            {
                $cacheKey .= $maxRecordCount;
            }
            if ($groupingField)
            {
                $cacheKey .= $groupingField;
            }
            if ($startOffset)
            {
                $cacheKey .= serialize($startOffset);
            }
            if ($arrayKeyField)
            {
                $cacheKey .= serialize($arrayKeyField);
            }
            if ($joinObjects)
            {
                $cacheKey .= serialize($joinObjects);
            }
            $cacheKey = md5($cacheKey);
        }
        return $cacheKey;
    }
    /**
     * Get the models from the Storage Engine that matches the given filter and other criteria.
     * This method is protected so that it may be exposed by a public method in a subclass.
     * The purpose of doing so is to allow subclass php-docs to be type-specific.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param int $maxRecordCount optional The maximum number of result objects's to return
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @param int $startOffset
     * Of the records returned, the first <i>$startOffset</i> of them will be ignored.
     *
     * @param string $arrayKeyField optional
     * The name of the field on the matched record that contains it's key.
     *
     * @param boolean $useCachedIfAvailable optional
     * If true, the filter will return results from the last call who's result was not from the cache that was made to this method with identical filter and related settings.
     *
     * @return \DblEj\Data\PeristableModel[] an array of the matching Models
     *
     * @throws DataModelException
     * Thrown if there are no shared data storage engines setup.
     *
     * @throws DataException
     * Thrown if there are no shared data storage engines setup that are appropriate to filter this data.
     */
    protected static function _filter($filter = null, $orderByFieldName = null, $maxRecordCount = null, $groupingField = null, $joinObjects = null, $startOffset = 0, $arrayKeyField = null, $useCachedIfAvailable = true)
    {
        $subclass = get_called_class();
        $cacheKey = self::_getCacheKey($filter, $orderByFieldName, $maxRecordCount, $startOffset, $joinObjects, $groupingField, $arrayKeyField);
        if ($useCachedIfAvailable && $cacheKey && isset(self::$filterResultCache[$subclass]) && isset(self::$filterResultCache[$subclass][$cacheKey]))
        {
            $returnArray = self::$filterResultCache[$subclass][$cacheKey];
        } else {
            self::_validateStorageEngine();
            $dataStorage = self::getStorageEngine();
            $returnArray = self::_prepareData($dataStorage, $filter, $orderByFieldName, $maxRecordCount, $groupingField, $joinObjects, $startOffset, $arrayKeyField, $useCachedIfAvailable);
            self::_cacheData($cacheKey, $returnArray);
        }

        return $returnArray;
    }

    private static function _prepareData($dataStorage, $filter, $orderByFieldName, $maxRecordCount, $groupingField, $joinObjects, $startOffset, $arrayKeyField, $useCachedIfAvailable)
    {
        $subclass = get_called_class();
        $returnArray = [];
        $data        = $dataStorage->GetDataGroup($subclass::Get_StorageLocation(), $filter, $orderByFieldName, $maxRecordCount, $groupingField, $joinObjects, $startOffset);
        foreach ($data as $dataObject)
        {
            $instance = null;
            if ($arrayKeyField && !$orderByFieldName)
            {
                if ($useCachedIfAvailable && isset(self::$modelInstanceCache[$subclass]) && isset(self::$modelInstanceCache[$subclass][$dataObject[$arrayKeyField]]))
                {
                    $instance = self::$modelInstanceCache[$subclass][$dataObject[$arrayKeyField]];
                } else {
                    $instance = new $subclass(null, $dataObject, $dataStorage);
                    $instance->markAllFieldsClean();
                }
                $returnArray[$dataObject[$arrayKeyField]] = $instance;
            }
            elseif ($subclass::Get_KeyFieldName() && !$orderByFieldName)
            {
                if ($useCachedIfAvailable && isset(self::$modelInstanceCache[$subclass]) && isset(self::$modelInstanceCache[$subclass][$dataObject[$subclass::Get_KeyFieldName()]]))
                {
                    $instance = self::$modelInstanceCache[$subclass][$dataObject[$subclass::Get_KeyFieldName()]];
                } else {
                    $instance = new $subclass(null, $dataObject, $dataStorage);
                    $instance->markAllFieldsClean();
                }
                $returnArray[$dataObject[$subclass::Get_KeyFieldName()]] = $instance;
            }
            else
            {
                $instance = new $subclass(null, $dataObject, $dataStorage);
                $instance->markAllFieldsClean();
                $returnArray[] = $instance;
            }
        }
        return $returnArray;
    }

    private static function _cacheData($cacheKey, $data)
    {
        if ($cacheKey)
        {
            $subclass = get_called_class();
            if (count($data) > 0 || $subclass::ShouldCacheEmptyResponses())
            {
                self::$filterResultCache[$subclass][$cacheKey] = $data;
            } else {
                unset(self::$filterResultCache[$subclass][$cacheKey]);
            }
        }
    }

    public static function ShouldCacheEmptyResponses()
    {
        return false;
    }

    /**
     * Get the models from the Storage Engine that matches the given filter and other criteria.
     * This method is protected so that it may be exposed by a public method in a subclass.
     * The purpose of doing so is to allow subclass php-docs to be type-specific.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param int $maxRecordCount optional The maximum number of result objects's to return
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @param int $startOffset
     * Of the records returned, the first <i>$startOffset</i> of them will be ignored.
     *
     * @param string $arrayKeyField optional
     * The name of the field on the matched record that contains it's key.
     *
     * @param boolean $useCachedIfAvailable optional
     * If true, the filter will return results from the last call who's result was not from the cache that was made to this method with identical filter and related settings.
     *
     * @return \DblEj\Data\PeristableModel[] an array of the matching Models
     *
     * @throws DataModelException
     * Thrown if there are no shared data storage engines setup.
     *
     * @throws DataException
     * Thrown if there are no shared data storage engines setup that are appropriate to filter this data.
     */
    public static function Filter($filter = null, $orderByFieldName = null, $maxRecordCount = null, $groupingField = null, $joinObjects = null, $startOffset = 0, $arrayKeyField = null, $useCachedIfAvailable = true)
    {
        return self::_filter($filter, $orderByFieldName, $maxRecordCount, $groupingField, $joinObjects, $startOffset, $arrayKeyField, $useCachedIfAvailable);
    }

    /**
     * Get the first model from the Storage Engine that matches the given filter and other criteria.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param string $groupingField optional The name of the field to group on.
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @param boolean $useCachedIfAvailable optional
     * If true, the filter will return results from the last call who's result was not from the cache that was made to this method with identical filter and related settings.
     *
     * @return \DblEj\Data\PeristableModel[] an array of the matching Models
     *
     * @throws DataModelException
     * Thrown if there are no shared data storage engines setup.
     */
    protected static function _filterFirst($filter = null, $orderByFieldName = null, $groupingField = null, $joinObjects = null, $useCachedIfAvailable = true)
    {
        if (count(self::$_sharedStorageEngines) < 1)
        {
            throw new DataModelException("Before you can load models you must tell DblEj what the active DataStorage engine is.  Use PersistableModel::SharedStorageEngines to set the active StorageEngine");
        }
        $returnArray = self::Filter($filter, $orderByFieldName, 1, $groupingField, $joinObjects, 0, null, $useCachedIfAvailable);
        if (count($returnArray) > 0)
        {
            return array_pop($returnArray);
        }
        else
        {
            return null;
        }
    }

    /**
     * Get the first model from the Storage Engine that matches the given filter and other criteria.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @param boolean $useCachedIfAvailable optional
     * If true, the filter will return the result from the last call who's result was not from the cache that was made to this method with identical filter and related settings.
     *
     * @return \DblEj\Data\PeristableModel[] an array of the matching Models
     *
     * @throws DataModelException
     * Thrown if there are no shared data storage engines setup.
     */
    public static function FilterFirst($filter = null, $orderByFieldName = null, $groupingField = null, $joinObjects = null, $useCachedIfAvailable = true)
    {
        return self::_filterFirst($filter, $orderByFieldName, $groupingField, $joinObjects, $useCachedIfAvailable);
    }

    /**
     * Get the models from the Storage Engine that matches the given filter and other criteria
     * Deprecated in favor of Filter
     *
     * @deprecated since version 837
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param int $maxRecordCount optional The maximum number of result objects's to return
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @param int $startOffset
     * Of the records returned, the first <i>$startOffset</i> of them will be ignored.
     *
     * @param string $arrayKeyField optional
     * The name of the field on the matched record that contains it's key.
     *
     * @return \DblEj\Data\PeristableModel[] an array of the matching Models
     * @throws DataModelException
     * @throws DataException
     */
    public static function Select($filter = null, $orderByFieldName = null, $maxRecordCount = null, $groupingField = null, $joinObjects = null, $startOffset = 0, $arrayKeyField = null)
    {
        return self::Filter($filter, $orderByFieldName, $maxRecordCount, $groupingField, $joinObjects, $startOffset, $arrayKeyField);
    }

    /**
     * Get the first model from the Storage Engine that matches the given filter and other criteria
     * Deprecated in favor of FilterFirst
     *
     * @deprecated since version 837
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     * @return null|\DblEj\Data\PeristableModel
     * @throws DataModelException
     */
    public static function SelectFirst($filter = null, $orderByFieldName = null, $groupingField = null, $joinObjects = null)
    {
        return self::FilterFirst($filter, $orderByFieldName, $groupingField, $joinObjects);
    }

    /**
     * Gets the maximum value of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to get the maximum value for.
     *
     * @param string $filter
     * The criteria that determines which records are included when finding the maximum value.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function GetMaximum($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null)
    {
        return self::getStorageEngine()->GetDataGroupMax(static::Get_StorageLocation(), $fieldName, $filter, $groupingField, $joinFieldDefinitions);
    }

    /**
     * Gets the minimum value of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to get the minimum value for.
     *
     * @param string $filter
     * The criteria that determines which records are included when finding the minimum value.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function GetMinimum($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null)
    {
        return self::getStorageEngine()->GetDataGroupMin(static::Get_StorageLocation(), $fieldName, $filter, $groupingField, $joinFieldDefinitions);
    }

    /**
     * Get the value of <i>$fieldName</i> where the key field value equals <i>$lookupKey</i>.
     *
     * @param type $fieldName The name of the field to return.
     * @param type $lookupKey The key value that will be used to lookup the record.
     */
    public static function Lookup($fieldName, $lookupKey)
    {
        return self::getStorageEngine()->GetScalarData(static::Get_StorageLocation(), $fieldName, static::Get_KeyFieldName(), $lookupKey);
    }

    /**
     * Get the value of <i>$fieldName</i> where the record matches the specified <i>$filter</i>.
     * @param string $fieldName
     * @param string $filter A filter string, based on the underlying storage engine's syntax.  (This is usually a SQL where clause).
     * @return string|integer|float
     */
    public static function LookupByFilter($fieldName, $filter, $useCachedIfAvailable = true)
    {
        $record = self::FilterFirst($filter, null, null, null, $useCachedIfAvailable);
        if ($record)
        {
            return $record->GetFieldValue($fieldName);
        } else {
            return null;
        }
    }

    /**
     * Gets the sum of the <i>$fieldName</i> field in the matching records.
     *
     * @param string $fieldName
     * The name of the field to sum.
     *
     * @param string $filter
     * The criteria that determines which records are included when summing.
     *
     * @param string $groupingField
     * The name of the field to group on.
     *
     * @param array $joinFieldDefinitions
     * An array of columns to inner-join on the storage location as an added filter constraint.
     * The array should be associative where the key is the name of the storage location to join on
     * and the value is the name of a column that is <b>mutual</b> between the two storage locations
     * and that will be the column that is joined on.
     * If there is not a mutual column between the storage locations, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the columns you wish to join.
     */
    public static function Sum($fieldName, $filter = null, $groupingField = null, $joinFieldDefinitions = null)
    {
        return self::getStorageEngine()->GetDataGroupSum(static::Get_StorageLocation(), $fieldName, $filter, $groupingField, $joinFieldDefinitions);
    }

    /**
     * Get_StorageGroup
     * Every PersistableModel can be part of a so-called "Storage Group".
     * The Storage Group helps the system know which storage engine to use for this model's persistence.
     *
     * @return string The name of the storage group.
     */
    public static function Get_StorageGroup()
    {
        $subclass       = get_called_class();
        $methodname     = "Get_StorageGroup";
        $method         = new \ReflectionMethod($subclass, $methodname);
        $declaringclass = $method->getDeclaringClass();
        if ($declaringclass->name != get_class())
        {
            return $subclass::Get_StorageGroup();
        }
        else
        {
            reset(self::$_sharedStorageEngines);
            return key(self::$_sharedStorageEngines);
        }
    }

    protected static function getStorageEngine()
    {
        return self::$_sharedStorageEngines[static::Get_StorageGroup()];
    }

    /*
     * The unique id for this object.
     */
    public function Get_Uid()
    {
        if ($this->DoesCurrentUserHaveReadAccess())
        {
            return $this->GetFieldValue(static::Get_KeyFieldName());
        } else {
            throw new \Exception("User is not authorized to read this data", E_WARNING);
        }
    }

    /**
     * The data to be indexed for this object.
     */
    public function GetIndexableData()
    {
        return null;
    }

    public function DoesCurrentUserHaveReadAccess()
    {
        return true;
    }

    public function DoesCurrentUserHaveWriteAccess()
    {
        return true;
    }
    public function CanCurrentUserSetProperty($propertyName)
    {
        if ($this->instantiatedFromClientSide)
        {
            return $this->DoesCurrentUserHaveWriteAccess();
        } else {
            return true;
        }
    }

    /**
     * The name of the index that this item prefers to be indexed in, by default.
     * This is where this item will be indexed by <i>IIndexer</i>s
     * if no other index is specified during indexing operations.
     */
    public static function Get_DefaultIndexId()
    {
        throw new \Exception("static Get_DefaultIndexId() must be implemented by sub classes of PersistableModel");
    }

    public function __toString()
    {
        return get_called_class() . " " . $this->Get_Uid();
    }
}