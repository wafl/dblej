<?php

namespace DblEj\Data;

/**
 * A class that publishes information about it's fields.
 * Iterating over an instance of this class will iterate on it's fields.
 */
interface IFieldPublisher
extends \ArrayAccess, \IteratorAggregate
{

    /**
     * Whether or not this field publisher's fields can be retreived from it statically,
     * or if the published fields are specific to a particular instance.
     */
    public static function Get_HasStaticFieldNames();

    /**
     * A class's static field names should be returned by this method.
     * Some field publishers publish different fields based on their instance;
     * you would not do that in this method.
     *
     * @return string[] As array of field names.
     */
    public static function Get_FieldNames();

    /**
     * Get the data type for the specified field.
     *
     * @param string $fieldName The name of the field to get the data type for.
     */
    public static function GetFieldDataType($fieldName);

    /**
     * A class instances non-static field names should be returned by this method.
     * Some field publishers publish different fields based on their instance;
     * you would do that in this method.
     *
     * @return string[] As array of field names.
     */
    public function Get_InstanceFieldNames();

    /**
     * An associativee array of the values of the published fields,
     * keyed by the field names.
     *
     * @return array
     */
    public function Get_FieldValues($autoEscapeStrings = false);

    /**
     * Get the data types of the fields published by this field publisher.
     */
    public static function GetFieldDataTypes();
}