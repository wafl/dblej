<?php
namespace DblEj\Data;

/**
 * Thrown when an operation is attempted that requires a storage engine but a storage engine could not be found.
 */
class NoStorageEngineException
extends \Exception
{

}