<?php

namespace DblEj\Data;

/**
 * A utility class for validating and sanitizing data.
 *
 * @static
 */
class Validator
{
    const VALIDATE_EMAIL_ADDRESS = FILTER_VALIDATE_EMAIL;
    const VALIDATE_DECIMAL       = FILTER_VALIDATE_FLOAT;
    const VALIDATE_INTEGER       = FILTER_VALIDATE_INT;
    const VALIDATE_IP_ADDRESS    = FILTER_VALIDATE_IP;
    const VALIDATE_REGEX         = FILTER_VALIDATE_REGEXP;
    const VALIDATE_URL           = FILTER_VALIDATE_URL;
    const VALIDATE_NONE          = FILTER_DEFAULT;
    const VALIDATE_BOOLEAN       = FILTER_VALIDATE_BOOLEAN;
    const VALIDATE_MAC_ADDRESS   = FILTER_VALIDATE_MAC;
    const SANITIZE_NONE          = FILTER_DEFAULT;
    const SANITIZE_EMAIL_ADDRESS = FILTER_SANITIZE_EMAIL;
    const SANITIZE_URL           = FILTER_SANITIZE_URL;
    const SANITIZE_URL_STRING    = FILTER_SANITIZE_ENOCDED;
    const SANITIZE_DECIMAL       = FILTER_SANITIZE_NUMBER_FLOAT;
    const SANITIZE_INTEGER       = FILTER_SANITIZE_NUMBER_INT;
    const SANITIZE_HTML          = FILTER_SANITIZE_SPECIAL_CHARS;
    const SANITIZE_HTML_STRICT   = FILTER_SANITIZE_FULL_SPECIAL_CHARS;
    const SANITIZE_STRING        = FILTER_SANITIZE_STRING;

    /**
     * Return the specified input data if it passes the specified validation.
     *
     * @param string $data The data to validate, and to return if the validation succeeds.
     * @param int $validationType The type of validation.  This will be one of the Validator::VALIDATE_* constants.
     * @param mixed $defaultValue The value to return if the validation fails.
     * @param int $flags Bitwise disjunction of PHP flags from PHP's constant values.
     * @param string $decimalSeperator The character(s) to be used as the decimal point (usually . or ,)
     * @param type $minAllowedValue Some validation types, such as VALIDATE_INTEGER, have an optional minimum allowed value.
     * @param type $maxAllowedValue Some validation types, such as VALIDATE_INTEGER, have an optional maximum allowed value.
     * @param type $regExp The VALIDATE_REGEX validation type accepts a regular expression.
     * @link http://www.php.net/manual/en/filter.filters.flags.php PHP's constant values for the $flags argument
     * @return mixed Returns the valid data, or <i>$defaultValue</i> if the validation fails.
     */
    public static function GetValidData($data, $validationType, $defaultValue = null, $flags = null, $decimalSeperator = ".", $minAllowedValue = 0, $maxAllowedValue = PHP_INT_MAX, $regExp = null)
    {
        $options                       = array(
            "options" => array(),
            "flags"   => 0);
        $options["options"]["default"] = $defaultValue;
        $options["flags"]              = $flags;
        switch ($validationType)
        {
            case self::VALIDATE_DECIMAL:
                $options["decimal"]   = $decimalSeperator;
                break;
            case self::VALIDATE_INTEGER:
                $options["min_range"] = $minAllowedValue;
                $options["max_range"] = $maxAllowedValue;
                break;
            case self::VALIDATE_REGEX:
                $options["regexp"]    = $regExp;
                break;
        }
        $result = filter_var($data, $validationType, $options);
        return $result;
    }

    /**
     * Attempts to santize the string according to the specified sanitization type.
     *
     * @param mixed $data The data to sanitize.
     * @param int $sanitizationType The type of sanitization.  This will be one of the Validator::SANITIZE_* constants.
     * @param int $flags Bitwise disjunction of PHP flags from PHP's constant values
     * @return mixed The sanitized data, or <i>false</i> if the data could not be sanitized according to the specified type.
     */
    public static function Sanitize($data, $sanitizationType, $flags = null)
    {
        $result = filter_var($data, $sanitizationType, $flags);
        return $result;
    }

    /**
     * Validate an email address.
     *
     * @param string $emailAddress The email address to check.
     * @param string $defaultValue The value to return if the validation fails.
     * @return string|boolean The email address if it's valid, otherwise <i>$defaultValue</i>.
     */
    public static function ValidateEmailAddress($emailAddress, $defaultValue = null)
    {
        return self::GetValidData($emailAddress, self::VALIDATE_EMAIL_ADDRESS, $defaultValue);
    }

    /**
     * Check that the passed in string is a valid url according to RFC 2396.
     *
     * @param string $url The url to check.
     * @param string $defaultValue The value to return if the validation fails.
     * @param bool $pathRequired If the path part of the URL is required.
     * @param bool $queryRequired If the query part of the URL is required.
     * @link http://www.faqs.org/rfcs/rfc2396.html The URI spec
     * @return mixed The validated URL on success, or <i>$defaultValue</i> on failure.
     */
    public static function ValidateUrl($url, $defaultValue = null, $pathRequired = false, $queryRequired = false)
    {
        $flags = FILTER_FLAG_NONE;
        if ($pathRequired)
        {
            $flags = $flags | FILTER_FLAG_PATH_REQUIRED;
        }
        if ($queryRequired)
        {
            $flags = $flags | FILTER_FLAG_QUERY_REQUIRED;
        }
        return self::GetValidData($url, self::VALIDATE_URL, $defaultValue, $flags);
    }

    /**
     * Check if the specified path is a valid folder and if so, if it's writable.
     * @param string $folderPath
     * @return string|boolean The folder if it's valid and writable, otherwise <i>false</i>.
     */
    public static function ValidateWriteableFolder($folderPath)
    {
        return self::ValidateFolder($folderPath, true, true);
    }

    /**
     * Validate the format of the <i>$folderPath</i> to ensure it is a valid folder path and other optional criteria.
     * @param type $folderPath The folder path to validate.
     * @param type $mustExist <i>True</i> if the folder must exist for the validation to pass, otherwise <i>false</i>.
     * @param type $mustBeWritable <i>True</i> if the folder must be writable for the validation to pass, otherwise <i>false</i>.
     * @return boolean
     */
    public static function ValidateFolder($folderPath, $mustExist = true, $mustBeWritable = false)
    {
        $folderPath = self::GetValidData($folderPath, Validator::VALIDATE_REGEX, null, null, null, null, null, '/^[A-Za-z0-9]:?[A-Za-z0-9_\-/\\\.]+/');
        if ($folderPath)
        {
            if ($mustExist)
            {
                if (!file_exists($folderPath))
                {
                    $folderPath = false;
                }
                elseif ($mustBeWritable)
                {
                    if (!is_writeable($folderPath))
                    {
                        $folderPath = false;
                    }
                }
            }
        }

        return $folderPath;
    }

    /**
     * Validate that the passed <i>$number</i> is a valid decimal.
     *
     * @param mixed $number The string or number to validate.
     * @param mixed $defaultValue The value to return if the validation fails.
     * @param boolean $allowThousandsSeperator <i>True</i> if the thousands separator is allowed in the string, otherwise <i>false</i>.
     * @return mixed The validated decimal on success, or <i>false</i> on failure.
     */
    public static function ValidateDecimal($number, $defaultValue = null, $allowThousandsSeperator = true)
    {
        if ($allowThousandsSeperator)
        {
            $flags = FILTER_FLAG_ALLOW_THOUSAND;
            $flags = $flags | FILTER_FLAG_ALLOW_FRACTION;
        }
        else
        {
            $flags = FILTER_FLAG_ALLOW_FRACTION;
        }
        return self::GetValidData($number, self::VALIDATE_DECIMAL, $defaultValue, $flags);
    }

    /**
     * Validate that the passed <i>$number</i> is a valid integer.
     *
     * @param mixed $number The string or number to validate.
     * @param mixed $defaultValue The value to return if the validation fails.
     * @param int $minAllowedValue The minimum value that the <i>$number</i> can be lest it meet the wrath of failing validation.
     * @param int $maxAllowedValue The maximum value that the <i>$number</i> can be lest it meet the wrath of failing validation.
     * @return mixed The validated integer on success, or <i>false</i> on failure.
     */
    public static function ValidateInteger($number, $defaultValue = null, $minAllowedValue = 0, $maxAllowedValue = PHP_INT_MAX, $allowOctal = false, $allowHex = false)
    {
        $flags = FILTER_FLAG_NONE;
        if ($allowOctal)
        {
            $flags = $flags | FILTER_FLAG_ALLOW_OCTAL;
        }
        if ($allowHex)
        {
            $flags = $flags | FILTER_FLAG_ALLOW_HEX;
        }
        return self::GetValidData($number, self::VALIDATE_INTEGER, $defaultValue, $flags, ".", $minAllowedValue, $maxAllowedValue);
    }

    /**
     * Sanitize a string so that it is a valid email address.
     *
     * @param string $emailAddress The email address to validate.
     * @return string The santized email address, or <i>false</i> if the email address is too malformed to be sanitized..
     */
    public static function SanitizeEmailAddress($emailAddress)
    {
        return self::Sanitize($emailAddress, self::SANITIZE_EMAIL_ADDRESS);
    }

    /**
     * Sanitize a URL to contain only valid characters according to according to RFC 2396.
     *
     * @param string $url
     * @return string The sanitized URL, or <i>false</i> if the URL is too malformed to be sanitized.
     */
    public static function SanitizeUrl($url)
    {
        return self::Sanitize($url, self::SANITIZE_URL);
    }

    /**
     * Sanitize a string to contain only decimal numbers and optionally ,.e an E.
     * @param string $number A decimal value
     * @param boolean $allowThousandsSeperator <i>True</i> if thousands separators, such as commas,
     * are allowed in the string without invalidating it, otherwise <i>false</i>.
     * @param boolean $allowScientific <i>True</i> if scientific notation is permitted, otherwise </i>false</i>.
     * @return float The sanitized decimal, or <i>false</i> if the decimal is too malformed too sanitize.
     */
    public static function SanitizeDecimal($number, $allowThousandsSeperator = true, $allowScientific = false)
    {
        $flags = FILTER_FLAG_NONE;
        if ($allowThousandsSeperator)
        {
            $flags = $flags | FILTER_FLAG_ALLOW_THOUSAND;
        }
        if ($allowScientific)
        {
            $flags = $flags | FILTER_FLAG_ALLOW_SCIENTIFIC;
        }
        $flags   = $flags | FILTER_FLAG_ALLOW_FRACTION;
        $decimal = self::Sanitize($number, self::SANITIZE_DECIMAL, $flags);
        if ($decimal)
        {
            return (floatval($decimal));
        }
        else
        {
            return false;
        }
    }

    /**
     * Sanitize a string to contain only integer values.
     *
     * @param string $number The string to sanitize.
     *
     * @return The sanitized integer, or <i>false</i> if the integer is too malformed too sanitize.
     */
    public static function SanitizeInteger($number)
    {
        return self::Sanitize($number, self::SANITIZE_INTEGER);
    }
}