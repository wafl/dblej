<?php

namespace DblEj\Data;

/**
 * A container for one or more storage engines.
 */
interface IStorageEngineContainer
{

    /**
     * Get all of the storage engines in this container.
     *
     * @return \DblEj\Data\Integration\IDatabaseServer[]
     */
    public function GetStorageEngines();

    /**
     * Get the specified storage engine.
     *
     * @param string $lookupKey
     *
     * @return \DblEj\Data\Integration\IDatabaseServer
     */
    public function GetStorageEngine($lookupKey);
}