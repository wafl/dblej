<?php

namespace DblEj\Data;

interface IReadable
{
    public function Read($dataLookup, $dataLookupArguments = null, $dataLocation = null);
}