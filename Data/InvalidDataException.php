<?php

namespace DblEj\Data;

/**
 * Thrown when invalid data is passed as an argument or returned as a value.
 */
class InvalidDataException
extends DataException
{

    /**
     * Create an instance of InvalidDataException.
     * @param string $message
     * @param \Exception $previous
     */
    public function __construct($message = "Invalid Data", $previous = null)
    {
        parent::__construct($message, E_ERROR, $previous);
    }
}