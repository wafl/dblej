<?php

namespace DblEj\Data;

use DblEj\Communication\IConnectedService,
    DblEj\Data\ITransactionalProvider,
    DblEj\System\IExecutable;

interface IDatabaseConnection
extends IConnectedService, IExecutable, IDataLocker, ITabularDataReader, ITabularDataWriter, ITransactionalProvider, ICataloger, IStorageEngine
{
}
