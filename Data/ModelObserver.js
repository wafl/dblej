/*
 *@namespace DblEj.Data
 */
Namespace("DblEj.Data");

/*
 *@class ModelObserver Observes a model's properties.
 *@extends Class
 */
DblEj.Data.ModelObserver = Class.extend(
    {
        /**
         * @constructor Create an instance of a ModelObserver.
         */
        init: function ()
        {
            this._model = null;
            this._modelChangedCallback = null;
            this._uniqueId = String.CreateUUID();
        },
        /**
         * @property Get_Model The model that this ModelObserver is observing.
         * @type {Model}
         */
        Get_Model: function ()
        {
            return this._model;
        },
        /**
         * @property Set_Model The model that this ModelObserver is observing.
         * @type {Model}
         *
         * @param {Model} newModel The new model to observe
         * @return {ModelObserver} <i>this</i> to facilitate chained method calls.
         */
        Set_Model: function (newModel)
        {
            this._model = newModel;
            if (newModel)
            {
                newModel.ObserveMe(this._modelChanged_handler.Bind(this), "Observer" + this._uniqueId);
            }
            return this;
        },
        /**
         * Set the function that should be called if any properties change on the model.
         *
         * @method SetChangedCallback The function that will run when there is a change to any of the model's properties.
         * @param {Function} callback The callable function.
         */
        SetChangedCallback: function (callback)
        {
            this._modelChangedCallback = callback;
        },
        /**
         * @method _modelChanged_handler
         * @access private
         * @param {String} propertyName
         * @param {String|int|float} newValue
         */
        _modelChanged_handler: function (propertyName, newValue)
        {
            if (this._modelChangedCallback)
            {
                this._modelChangedCallback(propertyName, newValue);
            }
        }
    });