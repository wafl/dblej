<?php

namespace DblEj\Data;

/**
 * Represents a set of data of some defined structure.
 */
abstract class Model
implements IModel,
           \DblEj\Communication\IJsonInterfacable
{
    protected static $_fieldNames = [];
    protected $instantiatedFromClientSide = false;
    protected $_dirtyFields               = array();

    /**
     * Create an instance of the Model
     *
     * @param mixed $keyValue
     * Optional.  The unique key that identifies this instance of the model.
     *
     * @param array $objectData
     * Optional.  An associative array of data that should be assigned to the properties of this model, where each element's key is the property name and each element's value is the property value.
     *
     * @throws \DblEj\System\InstantiationFailedException
     */
    public function __construct(array $objectData = null, $instantiatedFromClientSide = false)
    {
        try
        {
            $this->instantiatedFromClientSide = $instantiatedFromClientSide;
            if ($objectData)
            {
                $this->SetFieldValues($objectData);
            }
        }
        catch (\Exception $e)
        {
            $className = get_called_class();
            throw new \DblEj\System\InstantiationFailedException("Exception thrown when trying to create a $className object. ".$e->getMessage(), E_ERROR, $e);
        }
    }

    /**
     * Whether or not this field publisher's fields can be retreived from it statically,
     * or if the published fields are specific to a particular instance.
     */
    public static function Get_HasStaticFieldNames()
    {
        return true;
    }

    /**
     * Determine whether or not certain data exists.
     *
     * @param string $dataName
     * The name of the data to check for.
     *
     * @return boolean
     */
    public function HasFieldValue($dataName)
    {
        $availableFields = $this->Get_InstanceFieldNames();
        return in_array($dataName, $availableFields);
    }

    /**
     * Get the names of every field in models of this type
     *
     * @return array
     * a numerically-indexed array of the names of every field
     */
    public static function Get_FieldNames()
    {
        throw new \Exception("static Get_DefaultIndexId() must be implemented by sub classes of PersistableModel");
    }

    /**
     * Get the names of every dirty field in this model
     *
     * @return array
     * a numerically-indexed array of the names of every dirty field
     */
    public function Get_DirtyFields()
    {
        return array_keys($this->_dirtyFields);
    }

    /**
     * Used by implementing classes to notify the base class that a change has occurred.
     * This will set the field dirty.
     *
     * @param string $fieldName The name of the field whose value has changed.
     */
    protected function ModelChanged($fieldName)
    {
        $this->_dirtyFields[$fieldName] = 1;
    }

    /**
     * Whether or not this model contains any dirty fields
     *
     * @return boolean
     * Returns True if the model has changed
     */
    public function IsDirty()
    {
        return count($this->_dirtyFields) > 0;
    }

    /**
     * Whether or not the specified field is dirty
     * @param string $fieldName
     * @return boolean True if the field is dirty, otherwise false
     */
    public function IsFieldDirty($fieldName)
    {
        return isset($this->_dirtyFields[$fieldName]) && $this->_dirtyFields[$fieldName];
    }

    /**
     * An instance method to get all of the fields available on this model
     *
     * @return array
     * A numercially-indexed array of the field names
     */
    final public function Get_InstanceFieldNames()
    {
        return static::Get_FieldNames();
    }

    /**
     * Escapes the string.
     *
     * @param string $string
     * @return string
     */
    private function escapeString($string)
    {
        return $string; //@todo NEED TO FINISH THIS (better yet deprecate it)!!!
    }

    /**
     * Get the value of every field in this model
     *
     * @param boolean $autoEscapeStrings
     * Whether or not special characters in the value should be escaped
     *
     * @return array
     * An associative array of the fields' values where the key is the field name and the value is the field's value.
     */
    final public function Get_FieldValues($autoEscapeStrings = false, $rekeyNumericArrays = false)
    {
        $values   = array();
        $fieldIdx = -1;
        foreach (static::Get_FieldNames() as $fieldName)
        {
            $fieldValue = $this->GetFieldValue($fieldName, null, false, $rekeyNumericArrays);
            if ($fieldValue !== null && $autoEscapeStrings)
            {
                $values[$fieldName] = self::escapeString($fieldValue);
            }
            else
            {
                if (is_numeric($fieldName) && $rekeyNumericArrays)
                {
                    $fieldIdx++;
                }
                else
                {
                    $fieldIdx = $fieldName;
                }
                $values[$fieldIdx] = $fieldValue;
            }
        }
        return $values;
    }

    /**
     * Get the value of a field on this model
     *
     * @param string $fieldName
     * The name of the field
     *
     * @param mixed $defaultValue The value to return if the field is null
     *
     * @param boolean $serializableChildren
     * If true, will only look at serializable fields
     *
     * @return mixed
     * The current value of the field
     *
     * @throws DataModelException
     * Throws an exception if the model does not have the requested field.
     */
    public function GetFieldValue($fieldName, $defaultValue = null, $serializableChildren = false, $rekeyNumericArrays = false)
    {
        $fieldValue = null;
        $object     = new \ReflectionObject($this);
        if ($object->hasMethod("Get_$fieldName"))
        {
            $propertyMethod = $object->getMethod("Get_$fieldName");
            $fieldValue     = $propertyMethod->invoke($this);
            if (is_array($fieldValue))
            {
                foreach ($fieldValue as &$fieldValueElement)
                {
                    if (is_a($fieldValueElement, "\DblEj\Data\Model"))
                    {
                        if ($serializableChildren)
                        {
                            $fieldValueElement = $fieldValueElement->Get_SerializableFieldValues(false, $rekeyNumericArrays);
                        }
                        else
                        {
                            $fieldValueElement = $fieldValueElement->Get_FieldValues(false, $rekeyNumericArrays);
                        }
                    }
                }
            }
            if (is_object($fieldValue) && is_a($fieldValue, "\DblEj\Data\Model"))
            {
                if ($serializableChildren)
                {
                    $fieldValue = $fieldValue->Get_SerializableFieldValues($rekeyNumericArrays);
                }
                else
                {
                    $fieldValue = $fieldValue->Get_FieldValues(false, $rekeyNumericArrays);
                }
            }
            elseif (is_array($fieldValue) && $rekeyNumericArrays)
            {
                $fieldValue = array_values($fieldValue);
            }
            elseif ($fieldValue === null)
            {
                $fieldValue = $defaultValue;
            }
        }
        else
        {
            $thisClass = get_called_class();
            throw new DataModelException("Model ($thisClass) does not has an accessor for one of it's defined (or requested) fields ($fieldName).", E_WARNING);
        }
        return $fieldValue;
    }

    /**
     * Set the values of serializable fields in this model
     *
     * @param array $fieldValues
     * An associative array of values where the key is the field name and the value is the field value.
     *
     * @return \DblEj\Data\Model
     * Return this for use in chained method calls
     */
    final public function Set_SerializableFieldValues(array $fieldValues)
    {
        $subclass = get_called_class();
        $myFields = $subclass::Get_SerializableFieldNames();
        foreach ($fieldValues as $fieldName => $fieldValue)
        {
            if ($fieldValue !== "undefined")
            {
                $fieldExists = array_search($fieldName, $myFields);
                if ($fieldExists !== false)
                {
                    $this->SetFieldValue($fieldName, $fieldValue);
                }
            }
        }
        return $this;
    }

    /**
     * Set the value of a field
     *
     * @param string $fieldName
     * The name of the field to set
     *
     * @param mixed $fieldValue
     * The value of the field
     *
     * @return \DblEj\Data\Model
     * Return this for use in chained method calls
     *
     * @throws DataModelException
     * Throws an exception if the model does not contain this data.
     */
    public function SetFieldValue($fieldName, $fieldValue)
    {
        if (!$fieldName)
        {
            throw new \Exception("Cannot set invalid field value (field: $fieldName)");
        }
        if (!$this->CanCurrentUserSetProperty($fieldName))
        {
            throw new \Exception("Current user does not have permission to set model property $fieldName");
        }
        $object    = new \ReflectionObject($this);
        $thisClass = get_class($this);
        if ($object->hasMethod("Set_$fieldName"))
        {
            $propertyMethod = $object->getMethod("Set_$fieldName");
            $fieldValue     = $propertyMethod->invoke($this, $fieldValue);
        }
        else
        {
            throw new DataModelException("Model ($thisClass) does not have a setter for one of it's defined (or requested) fields ($fieldName).", E_USER_WARNING);
        }
        return $this;
    }

    /**
     * Get the name of all the serializable fields in this model
     *
     * @return array
     * a numerically-indexed array of the names of the serializable fields of this model
     */
    public static function Get_SerializableFieldNames()
    {
        $fieldNames = static::Get_FieldNames();
        array_push($fieldNames, "ClientObjectName");
        array_push($fieldNames, "ClientObjectAncestoryChain");
        array_push($fieldNames, "ClientObjectVersion");
        return $fieldNames;
    }

    /**
     * Get the value of every serializable field in this model
     *
     * @param boolean $forceUtf8Strings
     * Whether or not to encode the values with utf8
     *
     * @return array
     * An associative array of the serializable fields' values where the key is the field name and the value is the field's value.
     */
    public function Get_SerializableFieldValues($forceUtf8Strings = false, $reindexObjectArrays = false)
    {
        $values     = array();
        $fieldNames = $this->Get_SerializableFieldNames();
        $fieldIdx   = -1;
        foreach ($fieldNames as $fieldName)
        {
            $fieldVal = $this->GetFieldValue($fieldName, null, true, $reindexObjectArrays);
            if ($forceUtf8Strings && is_string($fieldVal))
            {
                if (function_exists("mb_check_encoding"))
                {
                    if (mb_check_encoding($fieldVal, "UTF-8") !== true)
                    {
                        $fieldVal = utf8_encode($fieldVal);
                    }
                }
                else
                {
                    throw new \Exception("Cannot use UTF8 to serialize model", E_USER_NOTICE);
                }
            }
            if (is_numeric($fieldName) && $reindexObjectArrays)
            {
                $fieldIdx++;
            }
            else
            {
                $fieldIdx = $fieldName;
            }
            $values[$fieldIdx] = $fieldVal;
        }
        return $values;
    }

    /**
     * Get a JSON-encoded version of this model
     * @return string a JSON encoded string representing this model
     */
    public function ToJson($reindexObjectArrays = true)
    {
        $jsonString = \DblEj\Communication\JsonUtil::EncodeJson($this, true, $reindexObjectArrays);
        return $jsonString;
    }

    /**
     * Get the unique id that represents this model
     * @return mixed
     */
    public function Get_KeyFieldValue()
    {
        $thisClass = new \ReflectionClass($this);
        $subclass  = $thisClass->getName();
        if ($subclass::Get_KeyFieldName())
        {
            $getprop   = "Get_" . $subclass::Get_KeyFieldName();
            $value     = $this->$getprop();
        } else {
            $value = "";
        }
        return $value;
    }

    /**
     * Get the name of the client-side counterpart for this model
     * @return string
     */
    public function Get_ClientObjectName()
    {
        $objectInfo = new \ReflectionObject($this);

        $serverObjectNamespace = $objectInfo->getNamespaceName();
        $clientObjectNamespace = str_replace("\\", ".", $serverObjectNamespace);
        return $clientObjectNamespace . "." . $objectInfo->getShortName();
    }

    public function Get_ClientObjectVersion()
    {
        return "0";
    }

    /**
     * Get the inheritence chain for the javascript counterpart to the inheritor of this class.
     *
     * @return type
     */
    public function Get_ClientObjectAncestoryChain()
    {
        $objectInfo = new \ReflectionObject($this);
        $chain      = array();
        while ($objectInfo = $objectInfo->getParentClass())
        {
            if (\DblEj\Util\Strings::StartsWith($objectInfo->getNamespaceName(), "DblEj"))
            {
                break;
            }
            else
            {
                $chain[] = $objectInfo->getName();
            }
        }
        return array_reverse($chain);
    }

    /**
     * Create an instance of a model from an array of values
     *
     * @param array $jsonArray
     * An array of values used to build the model
     *
     * @return Model the newly created model
     */
    final public static function CreateInstanceFromJsonArray(array $jsonArray)
    {
        $returnObject = $jsonArray;
        if (is_array($jsonArray))
        {
            if (isset($jsonArray["ServerObjectName"]))
            {
                $className         = $jsonArray["ServerObjectName"];
                $returnObject      = new $className();
                $returnObject->Set_SerializableFieldValues($jsonArray);
                $returnObjectClass = new \ReflectionClass($returnObject);
                foreach ($jsonArray as $key => $value)
                {
                    if (is_array($value))
                    {
                        if ($returnObjectClass->hasMethod("Set_" . $key))
                        {
                            $setMethod = new \ReflectionMethod($returnObject, "Set_" . $key);
                            if ($setMethod)
                            {
                                $setMethod->invoke($returnObject, self::CreateInstanceFromJsonArray($value));
                            }
                        }
                    }
                }
            }
            else
            {
                foreach ($returnObject as $key => $value)
                {
                    if (is_array($value))
                    {
                        $returnObject[$key] = self::CreateInstanceFromJsonArray($value);
                    }
                }
            }
        }
        return $returnObject;
    }

    /**
     *
     * @return \DblEj\Data\Model
     */
    public function Get_DataSource()
    {
        return $this;
    }

    /**
     * Set the value of this model's properties from an http form submission.
     * Note that for this to work, all form fields must have a name of "[$fieldPrefix]FieldName".
     *
     * @param \DblEj\Communication\Http\Request $request
     * The HTTP request that contains the submitted form
     *
     * @param string $fieldPrefix
     * A string prefix that was prepended to the property name to make the form's element name
     *
     * @param boolean $stripHtmlTagsFromStrings
     * If true, then strip_tags will be called on every string value before being set
     *
     * @return \DblEj\Data\Model
     * Returns this for use in chained method calls
     */
    public function SetFromForm(\DblEj\Communication\Http\Request $request, $fieldPrefix = "", $inputType = \DblEj\Communication\Http\Request::INPUT_POST, $arrayInstanceIndex = null, $trimStrings = true, $stripHtmlTagsFromStrings = false)
    {
        if ($inputType === null)
        {
            $inputType = \DblEj\Communication\Http\Request::INPUT_POST;
        }
        $allInputs = $request->GetAllInputs($inputType);
        return $this->SetFromArray($allInputs, $fieldPrefix, $arrayInstanceIndex, $trimStrings, $stripHtmlTagsFromStrings);
    }

    /**
     * Set the value of this model's properties from an associative array.
     * Note that for this to work, array elements must have a key of "[$fieldPrefix]FieldName".
     *
     * @param \DblEj\Communication\Http\Request $request
     * The HTTP request that contains the submitted form
     *
     * @param string $fieldPrefix
     * A string prefix that was prepended to the property name to make the form's element name
     *
     * @param boolean $stripHtmlTagsFromStrings
     * If true, then strip_tags will be called on every string value before being set
     *
     * @return \DblEj\Data\Model
     * Returns this for use in chained method calls
     */
    public function SetFromArray($inputArray, $fieldPrefix = "", $arrayInstanceIndex = null, $trimStrings = true, $stripHtmlTagsFromStrings = false)
    {
        foreach ($inputArray as $inputKey => $allInput)
        {
            $cleanedKey = substr($inputKey, strlen($fieldPrefix));
            if ($this->HasFieldValue($cleanedKey))
            {
                if (is_array($allInput))
                {
                    if ($arrayInstanceIndex === null)
                    {
                        $arrayInstanceIndex = $this->Get_KeyFieldValue();
                    }
                    if (isset($allInput[$arrayInstanceIndex]))
                    {
                        $fieldValue = $allInput[$arrayInstanceIndex];
                    }
                }
                else
                {
                    $fieldValue = $allInput;
                }
                if ($trimStrings)
                {
                    $fieldValue = trim($fieldValue);
                }
                if ($stripHtmlTagsFromStrings)
                {
                    $fieldValue = strip_tags($fieldValue);
                }
                $this->SetFieldValue($cleanedKey, $fieldValue);
            }
        }
        return $this;
    }

    /**
     * Set the value of one or more fields based on the $fieldValues
     *
     * @param array $fieldValues
     * An associative array where each element's key equals the field name and each element's value represents the value.
     * For example: array("Title"=>"The Wizard of Oz", "MediaType"=>"Movie")
     *
     * @param string $sourceFieldsPrefix
     * The string that the source fields are prefixed with.
     * This is useful for processing, for example, form submissions where each form field is named: somePrefix + fieldName.
     * By passing in the $sourceFieldsPrefix, this method will remove the prefix portion of the field name when
     * setting the value on the object.
     *
     * @return \DblEj\Data\Model
     * Returns this for use in chained method calling
     */
    final public function SetFieldValues(array $fieldValues, $sourceFieldsPrefix = "")
    {
        foreach ($fieldValues as $fieldName => $fieldValue)
        {
            $cleanedFieldName = substr($fieldName, strlen($sourceFieldsPrefix));
            if (!in_array($cleanedFieldName, self::$_fieldNames))
            {
                self::$_fieldNames[] = $cleanedFieldName;
            }

            $this->SetFieldValue($cleanedFieldName, $fieldValue);
        }
        return $this;
    }
    public function CanCurrentUserSetProperty($propertyName)
    {
        return true;
    }

    public static function GetFieldDataTypes()
    {
        $returnArray = [];
        foreach (static::Get_FieldNames() as $fieldName)
        {
            $returnArray[$fieldName] = self::GetFieldDataType($fieldName);
        }
        return $returnArray;
    }
}