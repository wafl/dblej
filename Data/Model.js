/*
 *@namespace DblEj.Data
 */
Namespace("DblEj.Data");

/*
 *@class Model Represents a set of data of some defined structure.
 *This class is an observable base-class for data model's.
 *Instances of this class can be observed with <i>ObserveMe<i>.
 *
 *@extends Class
 *@abstract
 */
DblEj.Data.Model = Class.extend(
    {
        /**
         * @constructor
         */
        init: function ()
        {
            this._changedCallbacks = {};
            this._callbackCount = 0;
            this._updating = false;
            this._updated = false;
            this._properties = null;
        },
        /**
         * @property Get_ClientId A unique string to identify this instance (unique only to other instances of the same type)
         * @type {String}
         * @return {String} The unique string identifying this instance.
         */
        Get_ClientId: function () {
            throw "You must override Get_ClientId()";
        },
        /**
         * @method GetChildren
         * Get the child-models of this model.
         *
         * Sub-classes of Model should override this method and return an array of all Models which are children of this Model.
         *
         * @return {Array} The child-models.
         */
        GetChildren: function ()
        {
            return null;
        },
        /**
         * @method RemoveChildren
         * Remove the specified child model.
         * Sub-classes of Model should override this method and remove the specified instance from the children of this Model.
         *
         * @param {String} childId The ID of the child model to remove.
         * @param childTypeLabel string
         * @return {Boolean} <i>True</i> for success, otherwise false.
         */
        RemoveChild: function (childId, childTypeLabel)
        {
            return false;
        },
        /**
         * @method BeginUpdate Call this method before performing multiple fast updates on properties of an instance of this class to prevent the object from observable object from notifying obersvers of every single property change
         * @return void
         */
        BeginUpdate: function ()
        {
            this._updating = true;
        },
        /**
         * @method EndUpdate Call this method after performing multiple fast updates on properties of an instance of this class to notify obersvers that there has been a property change
         * @return void
         */
        EndUpdate: function ()
        {
            this._updating = false;
            if (this._updated)
            {
                this.ModelChanged("bulk");
            }
        },
        /**
         * @method ModelChanged Subclasses should call this method anytime there has been a change to their internal model (usually via property set)
         * @access protected
         * @param {String} propertyName The name of the property that has changed
         * @return void
         */
        ModelChanged: function (propertyName)
        {
            if (!this._updating)
            {
                for (var callbackIdx in this._changedCallbacks)
                {
                    if (this._changedCallbacks[callbackIdx])
                    {
                        this._changedCallbacks[callbackIdx](propertyName, this[propertyName]);
                    }
                }
                this._updated = false;
            } else {
                this._updated = true;
            }
        },
        /**
         * @method ObserveMe
         * Observe this model's properties.
         *
         * @param {Function} callback A callback function to call when a property on this model changes.
         * @param {String} key A key used to identify the callback.
         * @returns {Model} <i>this</i>, for making chained calls.
         */
        ObserveMe: function (callback, key) {
            this.AddCallback(callback, key);
            return this;
        },
        /**
         * @method StopObservingMe
         * Stop observing this model.
         *
         * @param {String} key
         * @returns {Model} <i>this</i>, for making chained calls.
         */
        StopObservingMe: function (key) {
            this.RemoveCallback(key);
            return this;
        },
        /**
         * @method AddCallback Subscribe a callback for model change notifications.
         * This is an alias of ObserveMe.
         *
         * @param {Function} callback The function to call when the model changes.
         * @param {String} key A unique string to identify the callback.
         * @optional
         * @return string The callback key is returned.
         */
        AddCallback: function (callback, key)
        {
            if (typeof key === undefined || typeof key == "undefined")
            {
                key = this._callbackCount;
            }
            if (key in this._changedCallbacks)
            {
                this._changedCallbacks[key] = callback;
            } else {
                this._changedCallbacks[key] = callback;
                this._callbackCount++;
            }
            return key;
        },
        /**
         * @method RemoveCallback Unsubscribe a callback from model change notifications.
         * This is an alias of StopObservingMe.
         *
         * @param {String} key The unique identifer of the callback (returned frm AddCallback).
         * @returns {Model} <i>this</i>, for making chained calls.
         */
        RemoveCallback: function (key)
        {
            delete this._changedCallbacks[key];
        },
        /**
         * @method GetProperties
         * Get all of this model's properties.
         *
         * @return Object A Key/DblEj.Data.ModelProperty pair of all properties on this object.
         */
        GetProperties: function ()
        {
            if (this._properties)
            {
                return this._properties;
            }
            else
            {
                this._properties = new Object();
                var setMethod;
                for (var propIdx in this)
                {
                    if (propIdx.substr(0, 4) == "Get_")
                    {
                        if (("Set_" + propIdx.substr(4)) in this)
                        {
                            setMethod = this["Set_" + propIdx.substr(4)].Bind(this);
                        } else {
                            setMethod = null;
                        }
                        this._properties[propIdx.substr(4)] =
                            new DblEj.Data.ModelProperty(propIdx.substr(4), propIdx, true, false, this[propIdx].Bind(this), setMethod, 10, 3, null, null);
                    }
                }
                this._properties = DblEj.Util.Objects.CloneObjectWithPropertiesSortedByName(this._properties, false);
            }
            return this._properties;
        },
        /**
         * @method Clone
         * Create a clone of this data model (properties only).
         *
         * @returns {DblEj.Data.Model}
         */
        Clone: function ()
        {
            var object = new DblEj.Data.Model();
            for (var name in this)
            {
                // Check if we're overwriting an existing function
                if (typeof this[name] != "function")
                {
                    object[name] = this[name];
                }
            }
            return object;
        },
        /**
         * @method ToSerializable
         * Convert this object into a serializable one.
         * In this case, the class is already serializable.
         *
         * @returns {Model} This model.
         */
        ToSerializable: function ()
        {
            return this;
        }
    });

/**
 * @method LoadInstanceFromKey
 * Load an existing instance of a Model from a server.
 *
 * @static
 *
 * @param {String} serverClassName The name of the server-side counterpart class that represents this Model.
 * @param {String} key The unique identifer of the model instance.
 * @param {Function} callbackMethod The method to notify when the model is received from the server.
 * If no callbackMethod is specified, then the request will be sent synchronously and will block
 * until a response is returned from the server, or until the request times out.
 * @default null
 *
 * @returns {Object|null} The returned instance, if no callback is specified.  Otherwise, null.
 */
DblEj.Data.Model.LoadInstanceFromKey = function (serverClassName, key, callbackMethod)
{
    var sendObject = {
        keyValue: key,
        serverClass: serverClassName
    };
    if (IsDefined(callbackMethod))
    {
        DblEj.Communication.Ajax.Utils.SendRequestAsync("DblEj.Data.Model.GetModelByKey", sendObject, "", callbackMethod);
        return null;
    } else {
        return DblEj.Communication.Ajax.Utils.SendRequest("DblEj.Data.Model.GetModelByKey", sendObject);
    }
};

/**
 * @function DeleteByKey
 * Delete the specified model from a server.
 * 
 * @static
 * 
 * @param {String} serverClassName The name of the server-side class that represents this model.
 * @param {String} key The unique identifier used to find the model to be deleted.
 * @param {Function} callbackMethod The function to notify when the model has been deleted.
 * If no callbaclMethod is specified, then the request will be sent synchronously and will
 * block until the response is received, or until the request times out.
 * 
 * @returns {Object|null}
 */
DblEj.Data.Model.DeleteByKey = function (serverClassName, key, callbackMethod)
{
    var sendObject = {
        keyValue: key,
        serverClass: serverClassName
    };
    if (IsDefined(callbackMethod))
    {
        DblEj.Communication.Ajax.Utils.SendRequestAsync("DblEj.Data.Model.DeleteModelByKey", sendObject, "", callbackMethod);
        return null;
    } else {
        return DblEj.Communication.Ajax.Utils.SendRequest("DblEj.Data.Model.DeleteModelByKey", sendObject);
    }
};