<?php

namespace DblEj\Data;

/**
 * A store of key/val string pairs.
 */
interface IDataStore
{
    /**
     * Store the value <i>$val</i> under the name <i>$key</i>.
     *
     * @param type $key The name of the data.
     * @param type $val The value of the data.
     */
    public function SetData($key, $val, $dataExpiration = null);

    /**
     * Get the data stored under the key <i>$key</i>.
     *
     * @param string $key The key of the data to get the value for.
     */
    public function GetData($key, $default = null);

    /**
     * Check if the data store has the specified data.
     * @param string $key The key of the data to check for.
     */
    public function HasData($key);

    /**
     * Delete data from the data store.
     * @param string $key The key of the data to delete.
     */
    public function DeleteData($key);

    /**
     * Remove all data from the data store.
     */
    public function FlushAllData();
}