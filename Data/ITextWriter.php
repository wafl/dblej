<?php
namespace DblEj\Data;

interface ITextWriter
extends IWriteable
{
    /**
     * An engine-specific string-sanitizer that is responsible for escaping any necessary
     * characters in a string before that string is used as a value in an insert statement,
     * update statement, or a (where, join, group by) clause.
     *
     * @param string $string The string to be escaped.
     */
    public function EscapeString($string);

    /**
     * Set the character encoding to be used when transmitting data to/from this storage engine.
     * @param string $encoding A string identifying the character encoding to use such as "utf8".
     */
    public function Set_CharacterEncoding($encoding);
}
