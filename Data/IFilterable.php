<?php

namespace DblEj\Data;

/**
 * Interface for classes that want to expose methods for filtering though some underlying
 * collection of instances of that same class.
 *
 * For example, let's say you have a class named "Animal" and you want to be able to
 * query Animal for all instances that are "Dogs".  Animal would implement IFilterable
 * and then expose the <i>Filter</i> method for finding a subset of the Animal's (in this case, dogs)
 * based on the given <i>$filter</i> and other parameters.
 */
interface IFilterable
{

    /**
     * Get an array of instances of this class, based on the given criteria.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param int $maxRecordCount optional The maximum number of result objects's to return
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @param int $startOffset
     * Of the records returned, the first <i>$startOffset</i> of them will be ignored.
     *
     * @param string $arrayKeyField optional
     * The name of the field on the matched record that contains it's key.
     *
     * @return IFilterable[] An array of the matching objects.
     */
    public static function Filter($filter = null, $orderByFieldName = null, $maxRecordCount = null, $groupingField = null, $joinObjects = null, $startOffset = 0, $arrayKeyField = null);

    /**
     * Get the first instance of this class that matches the given criteria.
     *
     * @param string $filter optional The filter to filter by.  If no filter is passed in, then all results are returned.
     * @param string $orderByFieldName optional The name of the field to order the result objects by.
     * @param string $groupingField optional The name of the field to group on.
     *
     * @param array $joinObjects optional
     * An array of items to inner-join on the filterable object as an added filter constraint.
     * The array should be associative where the key is the name of the item to join on
     * and the value is the name of a field that is <b>mutual</b> between the filterable item and the join item.
     * If there is not a mutual field between the items, then the value should be null.
     * In that case, you will need to add an equality condition to the
     * <i>$filter</i> for the fields you wish to join on.
     *
     * @return IFilterable The first matching object.
     */
    public static function FilterFirst($filter = null, $orderByFieldName = null, $groupingField = null, $joinObjects = null);

    /**
     * Delete the <i>IFilterable</i>s that match the given <i>$filter</i>.
     * @param type $filter
     */
    public static function DeleteByFilter($filter = null);
}