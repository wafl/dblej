<?php
namespace DblEj\Data;

interface ICataloger
{
    /**
     * Instruct the engine to switch to the specified catalog/database.
     *
     * @param string $dbCatalog The name of the catalog or database.
     */
    public function SetCatalog($dbCatalog);
}
