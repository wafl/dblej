<?php

namespace DblEj\Data;

/**
 * Represents a foreign key constraint in a relational data model.
 */
class ForeignKey
{
    private $_tableName;
    private $_columnName;
    private $_foreignTableName;
    private $_foreignColumnName;

    /**
     * Create an instance of a <i>ForeignKey</i> constraint.
     *
     * If a value is not present in the <i>$foreignColumnName</i> column of the
     * <i>$foreignTableName</i> table, then it is not a valid value for the <i>$columnName</i>
     * column in any row of the <i>$tableName</i> table.
     *
     * @param string $tableName The name of the table that has the foreign constraint.
     * @param string $columnName The name of the column that has a foreign constraint.
     *
     * @param string $foreignTableName
     * The name of the table that has the <i>$foreignColumnName</i> that the
     * <i>$columnName</i> value is constrained by.
     *
     * @param string $foreignColumnName
     * The name of the column in <i>$foreignTableName</i> that has the data that the
     * <i>$columnName</i> value is constrained by.
     */
    public function __construct($tableName, $columnName, $foreignTableName, $foreignColumnName)
    {
        $this->_tableName         = $tableName;
        $this->_columnName        = $columnName;
        $this->_foreignTableName  = $foreignTableName;
        $this->_foreignColumnName = $foreignColumnName;
    }

    /**
     * The name of the table that has the foreign constraint.
     * @return string
     */
    public function Get_TableName()
    {
        return $this->_tableName;
    }

    /**
     *
     * @return string The name of the column that has a foreign constraint.
     */
    public function Get_ColumnName()
    {
        return $this->_columnName;
    }

    /**
     * The name of the table that has the <i>$foreignColumnName</i> that the
     * <i>ColumnName</i> value in the <i>TableName</i> table is constrained by.
     */
    public function Get_ForeignTableName()
    {
        return $this->_foreignTableName;
    }

    /**
     * @param string $foreignColumnName
     * The name of the column in <i>$foreignTableName</i> that has the data that the
     * <i>ColumnName</i> value in the <i>TableName</i> table is constrained by.
     */
    public function Get_ForeignColumnName()
    {
        return $this->_foreignColumnName;
    }
}