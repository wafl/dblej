<?php
namespace DblEj\Data;

interface IDataLocker
extends \DblEj\System\ILockable
{
    /**
     * In database engines that support engine or catalog-level locking,
     * this method will return a boolean indicating if the engine or catalog
     * is currently locked by the specified </i>$lockname</i>.
     *
     * @param string $lockname The name of the lock to check for.
     * @return boolean Whether or not the lock exists and is currently active.
     */
    public function IsLocked($lockname);
}