<?php

namespace DblEj\Data;

/**
 * Represents a static class that has a static initializer.
 */
interface IInitializableStatic
{

    /**
     * Initializes the static class.
     */
    public static function Initialize();
}