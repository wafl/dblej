<?php
namespace DblEj\Data\Integration;

interface IDatabaseServer
{
    /**
     * Get the database connection that has the specified id.
     * @param string $dbId The id of the IDatabaseConnection to retrieve.
     *
     * @return \DblEj\Data\IDatabaseConnection
     */
    public function GetConnection($dbId);

    /**
     * Check if this instance has a database of the specified id.
     *
     * @param string $dbId The id of the IDatabaseConnection to retrieve.
     *
     * @return boolean <i>True</i> if the connection exists, otherwise <i>false</i>.
     */
    public function HasConnection($dbId);

    /**
     * Add a connection to the internal list of connections
     *
     * @param string $dbId The unique id used to refer to the database.
     * @param \DblEj\Data\IDatabaseConnection $db The database connection to keep track of.
     *
     * @return void
     */
    public function AddConnection($dbId, \DblEj\Data\IDatabaseConnection $db);
}
