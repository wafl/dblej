<?php
namespace DblEj\Data\Integration;

abstract class IndexerExtensionBase
extends \DblEj\Extension\ExtensionBase
implements IIndexerExtension
{
    protected $_indexes;

    public function Index(\DblEj\Data\IIndex $index, \DblEj\Data\IIndexable $indexableItem)
    {
        return $index->Index($indexableItem);
    }

    public function Unindex(\DblEj\Data\IIndex $index, $Uid)
    {
        return $index->Unindex($Uid);
    }

    /**
     * Search for items in the specified index where the specified field matches the specified search phrase.
     * @param string|array $fieldToSearchOn The name of the field to search on, or an array of the names of the fields to search on.
     * @param string|array $searchPhrase The name of the phrase to search or,
     * if <i>$fieldToSearchOn</i> is an array, an array of phrases to search for that correspond to those fields.
     * @param int $startOffset If the search returns multiple records, ignore the first <i>$startOffset</i> of them.
     * @param int $count The maximum number of records to return.
     * @param string[] $returnFields An array of the names of the fields to be returned.
     * @param IndexSort[] $sorts An array of IndexSort objects instructing the index on how to sort the results.
     * @param mixed $args Implementation specific arguments.
     */
    public function Search(\DblEj\Data\IIndex $index, $fieldToSearchOn, $searchPhrase, $startOffset = 0, $count = null, $returnFields = null, $sorts = null, $args = null)
    {
        return $index->Search($fieldToSearchOn, $searchPhrase, $startOffset, $count, $returnFields, $sorts, $args);
    }
}