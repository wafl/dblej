<?php
namespace DblEj\Data\Integration;

interface IDatabaseServerExtension
extends IDatabaseServer, \DblEj\Extension\IExtension
{
}
