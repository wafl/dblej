<?php

namespace DblEj\Data;

abstract class DeletableModel
extends SelectableModel
{
    /**
     * Delete the persistant data record for this data model.
     *
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function Delete($checkPermissions = false)
    {
        if (!$checkPermissions || $this->DoesCurrentUserHaveWriteAccess())
        {
            $storageEngine = self::getStorageEngine();
            if ($storageEngine && $storageEngine->IsReady())
            {
                $subclass = get_called_class();
                $deleteResult = $storageEngine->DeleteData($subclass::Get_StorageLocation(), $subclass::Get_KeyFieldName(), $this->GetFieldValue($subclass::Get_KeyFieldName()));

                if ($deleteResult)
                {
                    //invalidate any caches with this object in it
                    if (isset(self::$modelInstanceCache[$subclass][$this->GetFieldValue($subclass::Get_KeyFieldName())]))
                    {
                        unset(self::$modelInstanceCache[$subclass][$this->GetFieldValue($subclass::Get_KeyFieldName())]);
                    }
                    $this->SetFieldValue($subclass::Get_KeyFieldName(), null);

                    $resultCache = self::$filterResultCache[$subclass];
                    foreach ($resultCache as $cacheKey=>$results)
                    {
                        $delIdx = null;
                        foreach ($results as $resultIdx=>$result)
                        {
                            if ($result == $this)
                            {
                                $delIdx = $resultIdx;
                                break;
                            }
                        }
                        if ($delIdx)
                        {
                            unset(self::$filterResultCache[$subclass][$cacheKey]);
                        }
                    }
                }
                return $deleteResult;
            }
            else
            {
                throw new StorageEngineNotReadyException("Attempt to delete data from storage engine when it is not ready", E_WARNING);
            }
        } else {
            throw new \Exception("User is not authorized to delete this data", E_WARNING);
        }
    }

    /**
     * Delete the <i>IFilterable</i>s that match the given <i>$filter</i>.
     * @param type $filter
     */
    public static function DeleteByFilter($filter = null)
    {
        self::getStorageEngine()->DeleteDataGroup(static::Get_StorageLocation(), $filter);
    }
}