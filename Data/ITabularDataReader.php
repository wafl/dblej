<?php
namespace DblEj\Data;

interface ITabularDataReader
extends \DblEj\Data\IReadable
{
    /**
     * Ask the data engine if the specified table exists.
     * @param string $tableName The name of the table to inquire about.
     * @return boolean <i>True</i> if the table exists, otherwise <i>false</i>.
     */
    public function DoesTableExist($tableName);

    /**
     * Get an array of data rows (represented by an associative array) from the
     * database engine based on the passed <i>$query</i>.
     * This method returns the entire result of the query and so memory should be
     * considered when using this method on large query's.  For larger queries, you
     * may want to use GetRows() instead and then fetch a single row at a time using <i>GetNextRow()</i>.
     *
     * @param string $query
     * A string that specifies the query that will return the desired rows.
     * For most common relational database engines, this is a SQL string.
     *
     * @param type $dbCatalog
     * The name of the catalog or database to run the query against.
     *
     * @return array
     * An array of associative sub-arrays where each sub-array represents a row of data.
     * Each element in the sub array is keyed by the column name and the value represents the value
     * for that column in that row.
     *
     * For tables with a primary key, the outer array will be keyed by the value in the primary key column.
     * For tables without a primary key, the outer array will be numerically indexed in sequence.
     */
    public function GetRowsAsArray($dataLookup, $dataLocation = null, $dataLookupArguments = null);

    /**
     * Ask the engine for rows of data based on the specified <i>$query</i>.
     * For some database engines such as mySql, this will prepare the result of the query,
     * but will not actually return the result.
     * This can be useful for keeping memory consumption down on larger queries.
     *
     * To get the result data, you can use <i>GetNextRow</i> on the object returned by <i>GetRows</i> to get a single row at a time.
     *
     * @param string $query
     * A string that specifies the query that will return the desired rows.
     * For many common relational database engines, this is a SQL string.
     *
     * @param type $dbCatalog
     * The name of the catalog or database to run the query against.
     *
     * @return mixed
     * An object that identifies the result, which for some engines might be the result itself.
     * This object can subsequently be passed to GetNextRow() to get the next row of data.
     * Some engines will return the actual data with this method,
     * because it has no concept of "preparing" a result.
     * However, those implementation should still implement GetNextRow to get the next row
     * from that result.
     */
    public function GetRows($dataLookup, $dataLocation = null, $dataLookupArguments = null);

    /**
     * Get the next row available in a result prepared by <i>GetRows</i>.
     *
     * @param mixed $rows A result from calling the <i>GetRows</i> method.
     * @return array An associative array representing the row of data where each array element's
     * key corresponds to the column name of the data in that element.
     */
    public function GetNextRow($rows);

    /**
     * Get an array of data representing the first row returned by the specified <i>$query</i>.
     *
     * @param string $query The query string used to specify which data to return.
     * @param string $dbCatalog The name of the database or catalog to run the query against.
     * If blank then the current catalog (the one last sent to <i>SetCatalog</i>) will be used.
     *
     * @return array An array of data representing the first row returned by the specified <i>$query</i>.
     */
    public function GetFirstRow($dataLookup, $dataLocation = null, $dataLookupArguments = null);
}