<?php
namespace DblEj\Data;

/**
 * A data model that can be persisted to and loaded from an underlying data storage, if one is provided.
 */
abstract class PersistableModel
extends IteratableModel
implements IPersistable
{
    /**
     * Persist the model's properties to a StorageEngine
     *
     * @param boolean $checkPermissions <i>True</i> if a method should be called checking the current user's rights to save this object.
     * @return mixed The key of the saved model.
     */
    public function Save($checkPermissions = false, $forceUseStorageEngine = null)
    {
        if (!$checkPermissions || $this->DoesCurrentUserHaveWriteAccess())
        {
            $storageEngine = $forceUseStorageEngine?$forceUseStorageEngine:self::getStorageEngine();
            $keyVal   = $storageEngine->StoreData(static::Get_StorageLocation(), static::Get_FieldNames($storageEngine), $this->Get_FieldValues(), static::Get_KeyFieldName(), static::Get_KeyValueIsGeneratedByEngine());

            if (static::Get_KeyValueIsGeneratedByEngine())
            {
                $this->SetFieldValue($this->Get_KeyFieldName(), $keyVal);
            }

            $this->_dirtyFields = array();
            return $keyVal;
        } else {
            throw new \Exception("User is not authorized to persist this instance.  To give permission, you must override the DoesCurrentUserHaveWriteAccess method in ".get_called_class(), E_WARNING);
        }
    }
}