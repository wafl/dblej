<?php

namespace DblEj\Data;

/**
 * Represents a data field.
 *
 * Contains properties that inform about the field's type and constraints.
 */
class Field
{
    const DATA_TYPE_INT      = "DATA_TYPE_INT";
    const DATA_TYPE_DECIMAL  = "DATA_TYPE_DECIMAL";
    const DATA_TYPE_BOOL     = "DATA_TYPE_BOOL";
    const DATA_TYPE_STRING   = "DATA_TYPE_STRING";
    const DATA_TYPE_DATE     = "DATA_TYPE_DATE";
    const DATA_TYPE_DATETIME = "DATA_TYPE_DATETIME";
    const DATA_TYPE_TIME     = "DATA_TYPE_TIME";
    const DATA_TYPE_BINARY   = "DATA_TYPE_BINARY";
    const DATA_TYPE_OBJECT   = "DATA_TYPE_OBJECT";
    const DATA_TYPE_ARRAY    = "DATA_TYPE_ARRAY";
    const DATA_TYPE_UNKNOWN  = "DATA_TYPE_UNKNOWN";

    private $_title;
    private $_dataType;

    /**
     * The length of the data that this field stores.
     *
     * Note that the unit of measure is dependent on the data type.
     * @var int
     */
    private $_dataLength;
    private $_floatDecimals;
    private $_isPrimaryKey;
    private $_isIndexed;
    private $_isUnique;
    private $_defaultValue;
    private $_isNullable;
    private $_numberIsUnsigned;
    private $_comment;

    /**
     * Create a <i>Field</i> instance.
     *
     * @param string $title The name of the field.
     * @param string $dataType One of the Field::DATA_TYPE_* constants
     * @param int $dataLength The length of the field.  The exact meaning of this property is dependant on the <i>$dataType</i>.
     * @param boolean $isNullable <i>True</i> if this field will accept a value of <i>null</i>, otherwise <i>false</i>.
     * @param mixed $defaultValue The default value for this field when it's value has not been set, or when it's value has been set to </i>null</i>.
     *
     * @param boolean $isPrimaryKey
     * <i>True</i> if this field contains the primary unique identifier
     * for the entity that this field is a member of, otherwise <i>false</i>.
     *
     * @param int $decimalLength
     * The decimal length or number precision.
     * The meaning of this value is dependant on the <i>$dataType</i>.
     *
     * @param boolean $unsigned
     * <i>True</i> if this field is an unsigned number, otherwise <i>false</i>.
     *
     * @param string $comment Arbitrary text comments about this field.
     */
    public function __construct($title, $dataType = self::DATA_TYPE_STRING, $dataLength = 0, $isNullable = false, $defaultValue = null, $isPrimaryKey = false, $decimalLength = 2, $unsigned = false, $comment = "", $isIndexed = false, $isUnique = false)
    {
        $this->_title            = $title;
        $this->_dataType         = $dataType;
        $this->_dataLength       = $dataLength;
        $this->_isNullable       = $isNullable;
        $this->_defaultValue     = $defaultValue;
        $this->_isPrimaryKey     = $isPrimaryKey;
        $this->_isIndexed        = $isIndexed;
        $this->_isUnique         = $isUnique;
        $this->_floatDecimals    = $decimalLength;
        $this->_numberIsUnsigned = $unsigned;
        $this->_comment          = $comment;
    }

    /**
     * The name of this field
     * @return string
     */
    public function Get_Title()
    {
        return $this->_title;
    }

    /**
     * Whether or not this field is (or is part of) the primary key for it's parent record
     * @return boolean
     */
    public function Get_IsPrimaryKey()
    {
        return $this->_isPrimaryKey;
    }

    /**
     * Whether or not this field is indexed (or is part of an index)
     * @return boolean
     */
    public function Get_IsIndexed()
    {
        return $this->_isIndexed;
    }

    /**
     * Whether or not this field is unique (or is part of a unique index)
     * @return boolean
     */
    public function Get_IsUnique()
    {
        return $this->_isIndexed;
    }

    /**
     * This fields native data type.
     *
     * @return string
     * One of the Field::DATA_TYPE_* constants.
     */
    public function Get_DataType()
    {
        return $this->_dataType;
    }

    public function Get_Comment()
    {
        return $this->_comment;
    }

    /**
     * Get a record's default value for this field if it has  not been set
     * @return mixed
     */
    public function Get_Default()
    {
        return $this->_defaultValue;
    }

    /**
     * Get the name of the PHP data type that corresponds to this fields data type
     * @return string
     */
    public function GetPhpDataType()
    {
        return self::GetNativeTypeStringFromDataTypeConstant($this->_dataType);
    }

    /**
     * Whether or not this field's value must be positive.
     *
     * @return boolean returns true if this field is unsigned (does not allow negative number), false if this field is signed (allows negative numbers)
     */
    public function Get_IsUnsigned()
    {
        return $this->_numberIsUnsigned;
    }

    /**
     * Get the name of the PHP data type for the given data type constant
     * @param string $dataTypeConstant
     * @return string
     */
    public static function GetNativeTypeStringFromDataTypeConstant($dataTypeConstant)
    {
        $typeString = "unknown type";
        switch ($dataTypeConstant)
        {
            case self::DATA_TYPE_INT:
                $typeString = "integer";
                break;
            case self::DATA_TYPE_DECIMAL:
                $typeString = "float";
                break;
            case self::DATA_TYPE_BOOL:
                $typeString = "boolean";
                break;
            case self::DATA_TYPE_STRING:
                $typeString = "string";
                break;
            case self::DATA_TYPE_DATE:
                $typeString = "string";
                break;
            case self::DATA_TYPE_DATETIME:
                $typeString = "string";
                break;
            case self::DATA_TYPE_TIME:
                $typeString = "integer";
                break;
            case self::DATA_TYPE_BINARY:
                $typeString = "string";
                break;
        }
        return $typeString;
    }
}