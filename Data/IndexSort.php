<?php

namespace DblEj\Data;

/**
 * Used to specify sorting criteria when querying an index.
 */
class IndexSort
{
    private $_fieldName;
    private $_direction;

    /**
     * Create the sort.
     *
     * @param string $fieldName
     * The name of the field to sort on.
     *
     * @param int $direction
     * The direction to sort.
     * One of: SORT_DESC, SORT_ASC.
     */
    public function __construct($fieldName, $direction = SORT_ASC)
    {
        $this->_fieldName = $fieldName;
        $this->_direction = $direction;
    }

    /**
     * The name of the field to sort on.
     * @return string The name of the field.
     */
    public function Get_FieldName()
    {
        return $this->_fieldName;
    }

    /**
     * The direction to sort.
     *
     * @return int Either SORT_DESC or SORT_ASC.
     */
    public function Get_Direction()
    {
        return $this->_direction;
    }
}