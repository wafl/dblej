<?php

namespace DblEj\Data;

/**
 * A strong-typed collection of DblEj\Data\Field objects.
 */
class FieldCollection
extends \DblEj\Collections\TypedKeyedCollection
{

    /**
     * Create an instance of a FieldCollection.
     *
     * @param array $collectionArray
     * An associative array to initialize this FieldCollection with where each
     * element represents a field to be added to this collection.
     */
    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray, "\DblEj\Data\Field");
    }

    /**
     * Get the DataField that has the specified name.
     *
     * @param string $fieldName The name of the field to get.
     *
     * @return \DblEj\Data\Field The matching <i>Field</i>, or null if there is no match.
     */
    public function GetDataField($fieldName)
    {
        return $this->GetItem($fieldName);
    }

    /**
     * Add a new Field to the collection.
     *
     * @param \DblEj\Data\Field $field The <i>Field</i> to add.
     */
    public function AddField(\DblEj\Data\Field $field)
    {
        return $this->AddItem($field, $field->Get_Title());
    }
}