<?php

namespace DblEj\Data;

/**
 * A container for multiple data stores.
 */
interface IDataStoreContainer
{

    /**
     * Add a data store to this container.
     * These containers were originally used to store data in memory, hence the name.
     * However the store can store it's data anyplace, it does not need to be in memory.
     *
     * @param string $dataName The name of the
     * @param \DblEj\Data\IDataStore $store
     */
    public function AddMemoryStore($dataName, \DblEj\Data\IDataStore $store);

    /**
     * Get the specified store from the store container.
     * @param string $storeName The name of the store to get
     * @return IDataStore The requested data store.
     */
    public function GetMemoryStore($storeName = null);

    /**
     * Store the value in a data store.
     *
     * @param string $dataName The name of the data to store.
     * @param string $dataValue The value of the data to store.
     * @param string $storeName The name of the store to store the data in.
     * If null, then the first store in the container will be used.
     */
    public function StoreInMemory($dataName, $dataValue, $storeName = null);

    /**
     * Get a value from a data store.
     *
     * @param type $dataName The name of the data to get from the store.
     * @param type $storeName The name of the store to get the data from.
     * If null, then the first store in the container will be used.
     */
    public function GetFromMemory($dataName, $storeName = null);
}