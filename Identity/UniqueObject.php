<?php

namespace DblEj\Identity;

/**
 * Base class for any class who's instances are unique and identifiable.
 */
abstract class UniqueObject
implements IUnique
{

    abstract public function Get_Uid();

    public function Get_Id()
    {
        return $this->Get_Uid();
    }

    public function Get_Type()
    {
        return get_called_class();
    }
}