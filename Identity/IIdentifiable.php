<?php

namespace DblEj\Identity;

/**
 * An identifiable object.
 */
interface IIdentifiable
{

    public function Get_Id();
}