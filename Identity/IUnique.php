<?php

namespace DblEj\Identity;

/**
 * A unique object.
 */
interface IUnique
extends IIdentifiable
{

    public function Get_Uid();
}