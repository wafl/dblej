/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/**
 * Used to display a lightbox effect.
 * 
 * @class Lightbox
 * @extends Class
 */
DblEj.UI.Lightbox = Class.extend(
    {
        _bgElem: null,
        _contentElem: null,
        /**
         * @constructor init
         * @param {Element} contentsElement
         */
        init: function (contentsElement, hideCallback, fade)
        {
            this._contentElem = contentsElement;
            this._contentElem._outerLightbox = this;
            this._origContentElemTop = null;
            this._origContentElemBottom = null;
            this._origContentElemHeight = null;
            this._origContentElemWidth = null;
            this._origContentElemLeft = null;
            this._origContentElemRight = null;
            this._origSizeSaved = false;
            this._lightboxIsShowing = false;
            this._hideCallback = IsDefined(hideCallback)?hideCallback:null;
            this._bgElem = document.createElement("div");
            this._bgElem.style.position = "absolute";
            this._bgElem.style.zIndex = "9998";
            this._bgElem.style.backgroundColor = "#000000";
            this._bgElem.style.width = "100%";
            this._bgElem.style.height = window.GetSize().Get_Y() + "px";
            this._bgElem.style.top = "0px";
            this._bgElem.style.left = "0px";
            this._bgElem.style.marginLeft = "0px";
            this._bgElem.style.marginTop = "0px";
            this._bgElem.style.marginRight = "0px";
            this._bgElem.style.marginBottom = "0px";
            this._bgElem.style.paddingLeft = "0px";
            this._bgElem.style.paddingTop = "0px";
            this._bgElem.style.paddingRight = "0px";
            this._bgElem.style.paddingBottom = "0px";
            this._bgElem.AddClickHandler(function() { this.Hide(fade); }.Bind(this));

            this._setSizeForLighbox = function()
            {
                var scrollOffset = window.GetOffsetPoint();
                var viewportHeight = DblEj.UI.Utils.GetPageSize().Get_Y();
                if (!this._origSizeSaved)
                {
                    this._origContentElemTop = this._contentElem.GetCss("top", false);
                    this._origContentElemBottom = this._contentElem.GetCss("bottom", false);
                    this._origContentElemLeft = this._contentElem.GetCss("left", false);
                    this._origContentElemMarginLeft = this._contentElem.GetCss("margin-left", false);
                    this._origContentElemRight = this._contentElem.GetCss("right", false);
                    this._origContentElemWidth = this._contentElem.GetCss("width", false);
                    this._origContentElemHeight = this._contentElem.GetCss("height", false);
                    this._origSizeSaved = true;
                }
                var elemSize = this._contentElem.GetAbsoluteSize();
                this._contentElem
                    .SetCss("position", "absolute")
                    .SetCss("margin-left", (-1 * (elemSize.Get_X() / 2)) + "px")
                    .SetCss("left", "50%")
                    .SetCss("top", (scrollOffset.Get_Y() + (viewportHeight / 3) - (elemSize.Get_Y() / 3)) + "px")
                    .SetCss("bottom", "auto")
                    .SetCss("height", elemSize.Get_Y() + "px");
            };

            this._resetLightboxSize = function()
            {
                if (this._origSizeSaved)
                {
                    this._contentElem.SetCss("left", this._origContentElemLeft);
                    this._contentElem.SetCss("margin-left", this._origContentElemMarginLeft);
                    this._contentElem.SetCss("right", this._origContentElemRight);
                    this._contentElem.SetCss("top", this._origContentElemTop);
                    this._contentElem.SetCss("bottom", this._origContentElemBottom);
                    this._contentElem.SetCss("width", this._origContentElemWidth);
                    this._contentElem.SetCss("height", this._origContentElemHeight);
                }
            };

            this._bringLightboxToFront = function()
            {
                var maxZIndex = DblEj.UI.Utils.GetMaxZIndex();
                this._bgElem.BringToFront();
                this._contentElem.BringToFront();
            };

            window.AddResizeHandler(
                function()
                {
                    if (this._lightboxIsShowing)
                    {
                        this._resetLightboxSize();
                        this._setSizeForLighbox();
                        this._bringLightboxToFront();
                    }
                }.Bind(this));
        },
        /**
         * @method Show
         * @param {Number} fade
         */
        Show: function (fade)
        {
            if (!IsDefined(fade) || fade === null)
            {
                fade = 0;
            }
            this._bgElem.SetOpacity(0);
            this._contentElem.Show().SetOpacity(0);
            this._setSizeForLighbox();

            if (this._contentElem.GetCss("background-color") === "")
            {
                this._contentElem.SetCss("background-color", "#ffffff");
            }
            document.body.appendChild(this._bgElem);
            document.body.appendChild(this._contentElem);
            if (fade > 0)
            {
                this._contentElem.Fade(0, 100, fade / 10);
                this._bgElem.Fade(0, 70, fade);
            } else {
                this._bgElem.SetOpacity(70);
                this._contentElem.SetOpacity(100);
            }
            this._lightboxIsShowing = true;
            this._bringLightboxToFront();
            return this;
        },
        /**
         * @method Show
         * @param {Number} fade
         */
        Hide: function (fade)
        {
            if (!IsDefined(fade) || fade === null)
            {
                fade = 0;
            }
            if (fade > 0)
            {
                this._contentElem.Fade(100, 0, fade / 10, -1);
                this._bgElem.Fade(70, 0, fade, -1,
                function()
                {
                    document.body.removeChild(this._bgElem);
                    this._contentElem.Hide();
                    this._resetLightboxSize();
                    this._bgElem.onclick = null;
                }.Bind(this));
            } else {
                document.body.removeChild(this._bgElem);
                this._contentElem.Hide();
                this._resetLightboxSize();
                this._bgElem.onclick = null;
            }

            if (IsDefined(this._hideCallback) && (this._hideCallback != null) && (IsFunction(this._hideCallback) || IsObject(this._hideCallback)))
            {
                this._hideCallback();
            }

            this._lightboxIsShowing = false;
            return this;
        }
    });

/**
 * @function ShowLightbox
 * @static
 * @param {Element} contentsElement
 * @param {Number} fadeSpeed
 */
DblEj.UI.Lightbox.ShowLightbox = function (contentsElement, fadeSpeed, hideCallback)
{
    if (IsDefined(contentsElement._outerLightbox))
    {
        var lb = contentsElement._outerLightbox;
    } else {
        var lb = new DblEj.UI.Lightbox(contentsElement, hideCallback, fadeSpeed);
    }
    if (typeof fadeSpeed == undefined || typeof fadeSpeed == "undefined" || fadeSpeed == null)
    {
        fadeSpeed = 0;
    }
    lb.Show(fadeSpeed);
    return lb;
};