/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI")

/*
 * @class Slider
 * @static
 */
DblEj.UI.Slider = {};

DblEj.UI.Slider.ScrollDivs = new Object();
DblEj.UI.Slider.ShownPerFrame = 1;

/**
 * @function RegisterScrollParent
 * @static
 *
 * @param {String} elementid
 * @param {String} slidestyle "Vertical" or "Horizontal"
 */
DblEj.UI.Slider.RegisterScrollParent = function (elementid, slidestyle)
{
    DblEj.UI.Slider.ScrollDivs[elementid] = new Object();
    DblEj.UI.Slider.ScrollDivs[elementid]["Count"] = 0;
    DblEj.UI.Slider.ScrollDivs[elementid]["CurrentIndex"] = 0;
    DblEj.UI.Slider.ScrollDivs[elementid]["SlideStyle"] = slidestyle;
};

/**
 * @function RegisterScrollChild
 * @static
 *
 * @param {String} parentid
 * @param {String} childid
 */
DblEj.UI.Slider.RegisterScrollChild = function (parentid, childid)
{
    DblEj.UI.Slider.ScrollDivs[parentid][DblEj.UI.Slider.ScrollDivs[parentid]["Count"]] = childid;
    DblEj.UI.Slider.ScrollDivs[parentid]["Count"]++;
};

/**
 * @function UnregisterAllScrollChildren
 * @static
 *
 * @param {String} parentid
 */
DblEj.UI.Slider.UnregisterAllScrollChildren = function (parentid)
{
    var slidestyle = DblEj.UI.Slider.ScrollDivs[parentid]["SlideStyle"];
    DblEj.UI.Slider.ScrollDivs[parentid] = new Object();
    DblEj.UI.Slider.ScrollDivs[parentid]["Count"] = 0;
    DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"] = 0;
    DblEj.UI.Slider.ScrollDivs[parentid]["SlideStyle"] = slidestyle;
};

/**
 * @function GetScrollChildIndex
 * @static
 *
 * @param {String} parentid
 * @param {String} childid
 */
DblEj.UI.Slider.GetScrollChildIndex = function (parentid, childid)
{
    var returnIdx = -1;
    for (var i = 0; i < DblEj.UI.Slider.ScrollDivs[parentid]["Count"]; i++)
    {
        if (DblEj.UI.Slider.ScrollDivs[parentid][i] == childid)
        {
            returnIdx = i;
            break;
        }
    }
    return returnIdx;
};
/**
 * @function ScrollToNextDiv
 * @static
 *
 * @param {String} parentid
 * @param {Number} speed
 * @param {Number} increasespeed
 */
DblEj.UI.Slider.ScrollToNextDiv = function (parentid, speed, increasespeed)
{
    DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"]++;
    if (DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"] > DblEj.UI.Slider.ScrollDivs[parentid]["Count"] -
        DblEj.UI.Slider.ShownPerFrame)
    {
        DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"] = 0;
    }
    var childid = DblEj.UI.Slider.ScrollDivs[parentid][DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"]];
    DblEj.UI.Slider.ScrollToInnerElement(childid, speed, increasespeed, DblEj.UI.Slider.ScrollDivs[parentid]["SlideStyle"]);
};

/**
 * @function ScrollToPreviousDiv
 * @static
 *
 * @param {String} parentid
 * @param {Number} speed
 * @param {Number} increasespeed
 */
DblEj.UI.Slider.ScrollToPreviousDiv = function (parentid, speed, increasespeed)
{
    DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"]--;
    if (parseInt(DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"]) < 0)
    {
        DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"] = DblEj.UI.Slider.ScrollDivs[parentid]["Count"] -
            DblEj.UI.Slider.ShownPerFrame;
    }
    var childid = DblEj.UI.Slider.ScrollDivs[parentid][DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"]];
    DblEj.UI.Slider.ScrollToInnerElement(childid, speed, increasespeed, DblEj.UI.Slider.ScrollDivs[parentid]["SlideStyle"]);
};

/**
 * @function ScrollToInnerElement
 * @static
 *
 * @param {String} elementid
 * @param {Number} speed
 * @param {Number} increasespeed
 * @param {String} slidestyle "Horizontal" or "Vertical"
 */
DblEj.UI.Slider.ScrollToInnerElement = function (elementid, speed, increasespeed, slidestyle)
{
    var element = $(elementid);
    var parentElement = element.parentNode;
    var direction;
    var distance;
    var parentid = parentElement.id;
    if (slidestyle == "Vertical")
    {
        var parentTop = parseInt(parentElement.style.top);
        if (isNaN(parentTop))
        {
            parentTop = parentElement.offsetTop;
        }
        var childTop = element.offsetTop;
        parentTop = parentTop * -1;
        if (DblEj.UI.Slider.ScrollDivs[parentid])
        {
            DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"] = DblEj.UI.Slider.GetScrollChildIndex(parentid, elementid);
        }
        if (parentTop < childTop)
        {
            direction = "up";
            distance = childTop - parentTop;
        } else {
            direction = "down";
            distance = parentTop - childTop;
        }
        DblEj.UI.Slider.VerticalSlideElement(parentElement.id, direction, distance, speed, increasespeed);
    } else {
        var parentLeft = parseInt(parentElement.style.left);
        if (isNaN(parentLeft))
        {
            parentLeft = parentElement.offsetLeft;
        }
        var childLeft = element.offsetLeft;
        parentLeft = parentLeft * -1;
        if (DblEj.UI.Slider.ScrollDivs[parentid])
        {
            DblEj.UI.Slider.ScrollDivs[parentid]["CurrentIndex"] = DblEj.UI.Slider.GetScrollChildIndex(parentid, elementid);
        }
        if (parentLeft < childLeft)
        {
            direction = "left";
            distance = childLeft - parentLeft;
        } else {
            direction = "right";
            distance = parentLeft - childLeft;
        }
        DblEj.UI.Slider.HorizontalSlideElement(parentElement.id, direction, distance, speed, increasespeed);
    }
};

/**
 * @function VerticalSlideElement
 * @static
 *
 * @param {String} elementid
 * @param {String} direction "up" or "down"
 * @param {Number} speed
 * @param {Number} increasespeed
 */
DblEj.UI.Slider.VerticalSlideElement = function (elementid, direction, distance, speed, increasespeed)
{
    var nexty;
    var stepcount;
    var increment;
    var delay;
    var starty;
    var endy;
    var element;
    var iteration;
    var directionMultiplier;
    var elemx;

    if (DblEj.Util.WebBrowser.IsIeVariant())
    {
        speed = speed + 20;
    }
    element = $(elementid);
    elemx = parseInt(element.style.left);
    if (isNaN(elemx))
    {
        elemx = element.offsetLeft;
    }
    increment = speed;
    delay = 70 - speed;
    if (delay < 40)
    {
        delay = 40;
    }
    starty = parseInt(element.style.top);
    if (isNaN(starty))
    {
        starty = element.offsetTop;
    }
    if (direction == "down")
    {
        directionMultiplier = 1;
    } else {
        directionMultiplier = -1;
    }
    endy = starty + (distance * directionMultiplier);

    stepcount = parseInt(distance / increment);
    for (iteration = 0; iteration < stepcount; iteration++)
    {
        nexty = starty + (iteration * increment * directionMultiplier);
        if (delay > 40)
        {
            delay = delay - increasespeed;
        }
        if (increment < 100)
        {
            increment = increment + increasespeed;
            stepcount = parseInt(distance / increment);
        }
        if (delay < 40)
        {
            delay = 40;
        }
        if (increment > 100)
        {
            increment = 100;
            stepcount = parseInt(distance / increment);
        }
        setTimeout("DblEj.UI.Animation.MoveElement($('" + elementid + "')," + elemx.toString() + "," + nexty.toString() +
            ")", iteration * delay);
    }
    setTimeout("DblEj.UI.Animation.MoveElement($('" + elementid + "')," + elemx.toString() + "," + endy.toString() +
        ")", iteration * delay);
};

/**
 * @function HorizontalSlideElement
 * @static
 *
 * @param {String} elementid
 * @param {String} direction "left" or "right"
 * @param {Number} distance
 * @param {Number} speed
 * @param {Number} increasespeed
 */
DblEj.UI.Slider.HorizontalSlideElement = function (elementid, direction, distance, speed, increasespeed)
{
    var nextx;
    var stepcount;
    var increment;
    var delay;
    var startx;
    var endx;
    var element;
    var iteration;
    var directionMultiplier;
    var elemy;

    if (DblEj.Util.WebBrowser.IsIeVariant())
    {
        speed = speed + 20;
    }
    element = $(elementid);
    elemy = parseInt(element.style.top);
    if (isNaN(elemy))
    {
        elemy = element.offsetTop;
    }
    increment = speed;
    delay = 70 - speed;
    if (delay < 40)
    {
        delay = 40;
    }
    startx = parseInt(element.style.left);
    if (isNaN(startx))
    {
        startx = element.offsetLeft;
    }
    if (direction == "right")
    {
        directionMultiplier = 1;
    } else {
        directionMultiplier = -1;
    }
    endx = startx + (distance * directionMultiplier);

    stepcount = parseInt(distance / increment);
    for (iteration = 0; iteration < stepcount; iteration++)
    {
        nextx = startx + (iteration * increment * directionMultiplier);
        if (delay > 40)
        {
            delay = delay - increasespeed;
        }
        if (increment < 100)
        {
            increment = increment + increasespeed;
            stepcount = parseInt(distance / increment);
        }
        if (delay < 40)
        {
            delay = 40;
        }
        if (increment > 100)
        {
            increment = 100;
            stepcount = parseInt(distance / increment);
        }
        setTimeout("$('" + elementid + "').SetCssLocation(" + nextx.toString() + "," + elemy.toString() + ")", iteration *
            delay);
    }
    setTimeout("$('" + elementid + "').SetCssLocation(" + endx.toString() + "," + elemy.toString() + ")", iteration * delay);
}