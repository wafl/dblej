/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/**
 * @class ColorTransition
 * Given two colors, this class will give you the value of the color at any relative point in between the two colors.
 * @extends Class
 */
DblEj.UI.ColorTransition = Class.extend({
    /**
     * @constructor
     * @param {DblEj.UI.Color} fromColor
     * @param {DblEj.UI.Color} toColor
     */
    init: function (fromColor, toColor)
    {
        this.colors = [];
        if (!fromColor)
            fromColor = new DblEj.UI.Color();
        if (!toColor)
            toColor = new DblEj.UI.Color();
        this.colors.push(fromColor);

        var toH = toColor.GetCielCh_H();
        var fromH = fromColor.GetCielCh_H();
        var diff = toH - fromH;
        if (Math.abs(diff) > 180) {
            if (diff > 0) {
                fromH += 360;
            }
            else {
                toH += 360;
            }
        }

        var stepInterval = 1 / 101;
        for (var i = 0; i < 100; i++) {
            var value = (i + 1) * stepInterval;

            var l = fromColor.GetCielCh_L() + (toColor.GetCielCh_L() - fromColor.GetCielCh_L()) * value;
            var c = fromColor.GetCielCh_C() + (toColor.GetCielCh_C() - fromColor.GetCielCh_C()) * value;
            var h = fromH + (toH - fromH) * value;

            var color = new DblEj.UI.Color(l, c, h, DblEj.UI.Color.ColorMode.CielCh);
            this.colors.push(color);
        }

        this.colors.push(toColor);
    },
    /**
     * @function GetColorByProgress Get the color at the specified percentage in between the two colors.
     *
     * @param {Float} transitionPercentage Number between 0 and 100, where 0 is the very beginning of the transition and 100 is the very end.
     *
     * @returns {DblEj.UI.Color}
     */
    GetColorByProgress: function (transitionPercentage) {
        var stepInterval = 1 / 101;
        var stepIndex = Math.ceil(transitionPercentage * stepInterval * 100);
        var color = this.colors[stepIndex];
        return new DblEj.UI.Color(color.GetRed(), color.GetGreen(), color.GetBlue());
    }
});