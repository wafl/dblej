/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Css Utility class provides methods for manipulating CSS attributes of an element.
 * @static
 */
DblEj.UI.Css = {};

/**
 * @function SwitchAllClasses
 * @static
 * @param {String} elemType An HTML tag.
 * @param {String} sourceClass
 * @param {String} newClass
 * @param {Element} parentElem
 */
DblEj.UI.Css.SwitchAllClasses = function (elemType, sourceClass, newClass, parentElem)
{
    var selImg;
    if (!parentElem || parentElem === undefined || parentElem == null)
    {
        parentElem = document;
    }
    var imgs = parentElem.GetChildrenByClassName(sourceClass, elemType);
    for (var imgId in imgs)
    {
        selImg = imgs[imgId];
        selImg.className = newClass;
    }
};