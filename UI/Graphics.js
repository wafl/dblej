/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Graphics Provides Javascript 2D drawing functionality.
 * @static
 */
DblEj.UI.Graphics = {};

/**
 * @function DrawLine
 * @static
 * @param {Element} parentElem
 * @param {String} color
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} thickness
 * @param {String} elemNameSuffix
 */
DblEj.UI.Graphics.DrawLine = function (parentElem, color, x1, y1, x2, y2, thickness, elemNameSuffix)
{
    var steep = Math.abs(y2 - y1) > Math.abs(x2 - x1);
    var oldx1;
    var oldx2;
    var oldy1;
    var oldy2;
    var deltax;
    var deltay;
    var curDivWidth;
    var dontDraw;
    var error;

    if (steep)
    {
        oldx1 = x1;
        oldx2 = x2;
        oldy1 = y1;
        oldy2 = y2;
        x1 = oldy1;
        x2 = oldy2;
        y1 = oldx1;
        y2 = oldx2;
    }
    if (x1 > x2)
    {
        oldx1 = x1;
        oldx2 = x2;
        oldy1 = y1;
        oldy2 = y2;
        x1 = oldx2;
        x2 = oldx1;
        y1 = oldy2;
        y2 = oldy1;
    }
    x1 = parseInt(x1);
    x2 = parseInt(x2);
    y1 = parseInt(y1);
    y2 = parseInt(y2);

    deltax = x2 - x1;
    deltay = Math.abs(y2 - y1);
    error = deltax / 2;
    var ystep;
    y = y1;
    if (y1 < y2)
    {
        ystep = 1;
    } else {
        ystep = -1;
    }

    curDivWidth = 0;
    dontDraw = 0;
    for (x = x1; x <= x2; x++)
    {
        error = error - deltay
        curDivWidth++;
        if (error < 0)
        {
            dontDraw++;
            if (dontDraw == 1)
            {
                if (steep) {
                    DblEj.UI.Graphics.DrawSquareDiv(parentElem, color, y, x, thickness, curDivWidth, "Circle" + elemNameSuffix);
                } else {
                    DblEj.UI.Graphics.DrawSquareDiv(parentElem, color, x, y, curDivWidth, thickness, "Circle" + elemNameSuffix);
                }
                dontDraw = 0;
            }
            y = y + ystep;
            curDivWidth = 0;
            error = error + deltax;
        }
    }
    if (curDivWidth != 0)
    {
        x = x - curDivWidth;
        if (steep) {
            DblEj.UI.Graphics.DrawSquareDiv(parentElem, color, y, x, thickness, curDivWidth, "Circle" + elemNameSuffix);
        } else {
            DblEj.UI.Graphics.DrawSquareDiv(parentElem, color, x, y, curDivWidth, thickness, "Circle" + elemNameSuffix);
        }
    }
}

/**
 * @function DrawSquareDiv
 * @static
 * @param {Element} parentElem
 * @param {String} color
 * @param {Number} x
 * @param {Number} y
 * @param {Number} width
 * @param {Number} height
 * @param {String} elemName
 */
DblEj.UI.Graphics.DrawSquareDiv = function (parentElem, color, x, y, width, height, elemName)
{
    var blockDiv = document.createElement('div');
    var divIdName = elemName;
    blockDiv.setAttribute('id', divIdName);
    blockDiv.style.position = "absolute";
    blockDiv.style.top = y + "px";
    blockDiv.style.left = x + "px";
    blockDiv.style.width = width + "px";
    blockDiv.style.height = height + "px";
    blockDiv.style.backgroundColor = color;
    Dhtml.AddChildToElement(parentElem, blockDiv)
};

/**
 * @function Plot
 * @static
 * @param {Element} parentElem
 * @param {String} color
 * @param {Number} x
 * @param {Number} y
 * @param {Number} thickness
 * @param {String} elemName
 */
DblEj.UI.Graphics.Plot = function (parentElem, color, x, y, thickness, elemName)
{
    drawSquareDiv(parentElem, color, x, y, thickness, thickness, elemName);
};