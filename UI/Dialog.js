/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Dialog Used to display dialogs.
 * @extends Class
 */
DblEj.UI.Dialog = Class.extend(
    {
        _title: "New Dialog",
        _contentsElement: "",
        _dlgdiv: null,
        _titlediv: null,
        _bodydiv: null,
        _callbackComplete: null,
        _callbackCancel: null,
        _footerDiv: null,
        _autohideOnConfirm: true,
        _amShowing: false,
        /**
         * @constructor
         *
         * @param {String} title
         * @param {Element} contentsElement
         * @param {String} confirmButtonCaption
         * @param {String} cancelButtonCaption
         * @param {Boolean} autohideOnConfirm
         */
        init: function (title, contentsElement, confirmButtonCaption, cancelButtonCaption, autohideOnConfirm)
        {
            this._title = title;
            this._autohideOnConfirm = IsDefined(autohideOnConfirm) ? autohideOnConfirm : true;
            this._amShowing = false;
            this._contentsElement = contentsElement;
            contentsElement.Show();
            var titleText = document.createElement("h3");
            var closeButton = document.createElement("button");
            this._footerDiv = document.createElement("div");
            titleText.appendChild(document.createTextNode(this._title));
            closeButton.className = "Close";
            closeButton.SetCss("border", "none");
            closeButton.Append("<i class='IconSquareX'></i>");
            closeButton.href = "";
            this._dlgdiv = document.createElement("div");
            this._titlediv = document.createElement("div");
            this._bodydiv = document.createElement("div");
            this._bodydiv.appendChild(this._contentsElement);
            this._dlgdiv.id = "DblPopupDialog" + DblEj.UI.Dialog._dialogCount.toString();
            this._dlgdiv.style.position = "absolute";
            this._titlediv.appendChild(closeButton);
            this._titlediv.appendChild(titleText);
            this._dlgdiv.appendChild(this._titlediv);
            this._dlgdiv.appendChild(this._bodydiv);
            this._dlgdiv.appendChild(this._footerDiv);
            this._footerDiv.AddClass("Footer");
            this._dlgdiv.className = "Dialog";
            this._titlediv.className = "Header";
            this._bodydiv.className = "Body";
            if (IsDefined(confirmButtonCaption) && (confirmButtonCaption != null))
            {
                var confirmButton = document.createElement("button");
                confirmButton.innerHTML = confirmButtonCaption;
                this._footerDiv.appendChild(confirmButton);
                confirmButton.AddClass("Button");
                confirmButton.AddClass("Primary");
                DblEj.EventHandling.Events.AddHandler(confirmButton, "click", this.ConfirmButtonMouseClick_handler.Bind(this), true);
            }
            if (IsDefined(cancelButtonCaption) && (cancelButtonCaption != null))
            {
                var cancelButton = document.createElement("button");
                cancelButton.innerHTML = cancelButtonCaption;
                if (IsDefined(confirmButtonCaption))
                {
                    this._footerDiv.appendChild(document.createTextNode(" "));
                }
                this._footerDiv.appendChild(cancelButton);
                DblEj.EventHandling.Events.AddHandler(cancelButton, "click", this.CloseButtonMouseClick_handler.Bind(this), true);
            }

            DblEj.EventHandling.Events.AddHandler(this._titlediv, "mousedown", this.MouseDown_handler.Bind(this), true);
            DblEj.EventHandling.Events.AddHandler(closeButton, "click", this.CloseButtonMouseClick_handler.Bind(this), true);
            DblEj.UI.Dialog._dialogCount++;
        },
        /**
         * @method Show
         * @param {Number} width
         * @param {Number} height
         * @param {Number} x
         * @param {Number} y
         * @param {Function} callbackComplete
         * @param {Function} callbackCancel
         * @param {Int|String} zIndexOrMaxZIndexSelector
         * @return void
         */
        Show: function (width, height, x, y, callbackComplete, callbackCancel, zIndexOrMaxZIndexSelector)
        {
            if (!IsDefined(zIndexOrMaxZIndexSelector))
            {
                zIndexOrMaxZIndexSelector = null;
            }
            if (typeof callbackComplete != undefined && typeof callbackComplete != "undefined")
            {
                this._callbackComplete = callbackComplete;
            }
            if (IsDefined(callbackCancel))
            {
                this._callbackCancel = callbackCancel;
            }
//            if (IsDefined(y) && y < 0)
//            {
//                y = 0;
//            }
//            if (IsDefined(x) && x < 0)
//            {
//                x = 0;
//            }
            if (!IsDefined(width) || width === null)
            {
                width = this._contentsElement.GetAbsoluteSize(false, true).Get_X();
                if (width > window.GetSize().Get_X() - 36)
                {
                    width = window.GetSize().Get_X() - 36;
                }
            }
            this._bodydiv.appendChild(this._contentsElement);
            if (this._dlgdiv.GetParent() == null)
            {
                document.body.appendChild(this._dlgdiv);
            }
            var bodyPadding = this._bodydiv.GetStyle("padding-left").ToFloat() + this._bodydiv.GetStyle("padding-right").ToFloat();
            width = width + bodyPadding;
            this._dlgdiv.style.width = width + "px";
            if (typeof height != undefined && typeof height != "undefined" && height)
            {
                var bodyPadding = this._bodydiv.GetStyle("padding-top").ToFloat() + this._bodydiv.GetStyle("padding-bottom").ToFloat();
                this._dlgdiv.style.height = (height + bodyPadding) + "px";
            }
            if (typeof x != undefined && typeof x != "undefined" && (x !== null))
            {
                this._dlgdiv.style.left = x + "px";
            }
            if (typeof y != undefined && typeof y != "undefined" && (y !== null))
            {
                this._dlgdiv.style.top = y + "px";
            }
            this._dlgdiv.style.marginLeft = "auto";
            this._dlgdiv.style.marginTop = "auto";
            this._dlgdiv.style.display = "block";
            this._dlgdiv.BringToFront();
            this._dlgdiv.Dialog = this;


            if (!IsDefined(height) || height === null)
            {
                height = this._contentsElement.GetAbsoluteSize().Get_Y()+this.GetNonContentHeight() + 1;
            }

            var pageSize = DblEj.UI.Utils.GetPageSize();
            var pageWidth = pageSize.Get_X();
            var pageHeight = pageSize.Get_Y();
            var locationWidthId = width+","+pageWidth+","+window.GetOffsetPoint().Get_X();
            var locationHeightId = height+","+pageHeight+","+window.GetOffsetPoint().Get_Y();

            if (DblEj.UI.Dialog.LastLocation === null || DblEj.UI.Dialog.LastLocationWidth != locationWidthId || DblEj.UI.Dialog.LastLocationHeight != locationHeightId)
            {
                DblEj.UI.Dialog.LastLocation = new DblEj.UI.Point((pageWidth / 2) - (width / 2), (pageHeight / 2) - (height / 2));
                DblEj.UI.Dialog.LastLocationWidth = locationWidthId;
                DblEj.UI.Dialog.LastLocationHeight = locationHeightId;
            }


            if (!IsDefined(x) || x === null)
            {
                x = DblEj.UI.Dialog.LastLocation.Get_X();
            }
            if (!IsDefined(y) || y === null)
            {
                y = DblEj.UI.Dialog.LastLocation.Get_Y();
            }

            if (x < 0)
            {
                x = 0;
            }
            if (y < 0)
            {
                y = 0;
            }

            y += window.GetOffsetPoint().Get_Y();
            x += window.GetOffsetPoint().Get_X();
            this._dlgdiv.SetCssX(x);
            this._dlgdiv.SetCssY(y);

            if (!this._amShowing)
            {
                DblEj.UI.Dialog._openDialogCount++;
                this._amShowing = true;
                DblEj.UI.Dialog._topDialogElem = this._dlgdiv;
            }
        },
        GetNonContentHeight: function()
        {
            if (this._dlgdiv.GetParent() == null)
            {
                document.body.appendChild(this._dlgdiv);
            }
            var myTopPadding = parseFloat(this._dlgdiv.GetStyle("padding-top"));
            var myBottomPadding = parseFloat(this._dlgdiv.GetStyle("padding-bottom"));
            var myTopBorder = parseFloat(this._dlgdiv.GetStyle("border-top-width"));
            var myBottomBorder = parseFloat(this._dlgdiv.GetStyle("border-bottom-width"));
            if (isNaN(myTopPadding))
            {
                myTopPadding = 0;
            }
            if (isNaN(myBottomPadding))
            {
                myBottomPadding = 0;
            }
            if (isNaN(myTopBorder))
            {
                myTopBorder = 0;
            }
            if (isNaN(myBottomBorder))
            {
                myBottomBorder = 0;
            }
            return this._titlediv.GetAbsoluteSize().Get_Y() + this._footerDiv.GetAbsoluteSize().Get_Y() + myTopPadding + myBottomPadding + myTopBorder + myBottomBorder;
        },
        /**
         * @method Hide
         * @param {cancelled} cancelled Used internally.
         * @return void
         */
        Hide: function (cancelled)
        {
            if (!IsDefined(cancelled))
            {
                cancelled = false;
            }
            if (cancelled)
            {
                if (this._callbackCancel)
                {
                    this._callbackCancel(this._contentsElement);
                }
                this._dlgdiv.Hide();
            } else {
                if (this._callbackComplete)
                {
                    this._callbackComplete(this._contentsElement);
                }
                if (this._autohideOnConfirm)
                {
                    this.Close();
                }
            }
            this._amShowing = false;
        },
        /**
         * @method Close
         * @return void
         */
        Close: function (cancelled)
        {
            if (!IsDefined(cancelled))
            {
                cancelled = false;
            }
            if (cancelled && this._callbackCancel)
            {
                this._callbackCancel(this._contentsElement);
            }

            if (IsDefined(this._dlgdiv) && this._dlgdiv !== null)
            {
                this._contentsElement.Hide();
                document.body.AppendChild(this._contentsElement);
                document.body.RemoveChild(this._dlgdiv);

                DblEj.EventHandling.Events.RemoveAllHandlersFromObject(this._titlediv);
            }
            this._callbackComplete = null;
            this._callbackCancel = null;
            this._contentsElement.OuterDialog = null;

            //this._dlgdiv = null;
            if (this._amShowing)
            {
                this._amShowing = false;
                DblEj.UI.Dialog._openDialogCount--;
            }
        },
        MouseDown_handler: function (event)
        {
            DblEj.UI.Mouse.DragDrop.BeginDrag(this._dlgdiv, "Dialog", "dragkey", "dragdata", 1, false, true, true, this._dlgdiv);
            this._dlgdiv.BringToFront();
        },
        CloseButtonMouseClick_handler: function (event)
        {
            if (IsDefined(this._contentsElement.CloseDialog))
            {
                this._contentsElement.CloseDialog(true);
            } else {
                this.Hide(true);
                return false;
            }
        },
        ConfirmButtonMouseClick_handler: function (event)
        {
            this.Hide();
        },
        /**
         * @property Get_DialogDiv
         * @return {Element}
         */
        Get_DialogDiv: function ()
        {
            return this._dlgdiv;
        }
    });

/**
 * @method ShowDialog
 * @static
 *
 * @param {String} title
 * @param {Element} contentsElement
 * @param {Function} onConfirmCallback
 * @param {Number} width
 * @param {Number} height Either an absolute height in pixels.
 * @param {Number} x
 * @param {Number} y
 * @param {Function} onCancelCallback
 * @param {String} confirmButtonCaption
 * @param {String} cancelButtonCaption
 * @param {Boolean} autohideOnConfirm
 * @param {Int|String} zIndexOrMaxZIndexSelector
 */
DblEj.UI.Dialog.ShowDialog =
    function (title, contentsElement, onConfirmCallback, width, height, x, y, onCancelCallback, confirmButtonCaption, cancelButtonCaption, autohideOnConfirm, zIndexOrMaxZIndexSelector)
    {
        if ((!IsDefined(confirmButtonCaption) || confirmButtonCaption == null) && (IsDefined(onConfirmCallback) &&
            onConfirmCallback != null))
        {
            confirmButtonCaption = "Ok";
        }
        if ((!IsDefined(cancelButtonCaption) || cancelButtonCaption == null) && IsDefined(onCancelCallback) &&
            onCancelCallback != null && cancelButtonCaption !== false)
        {
            cancelButtonCaption = "Cancel";
        }
        //if it was passed as null, it would have been turned into "Cancel".  
        //But passing as false means they want to use the cancel callback for the dialog X button but do not want to show a cancel button.
        //it is now safe to set the caption to null without affecting the callback
        if (cancelButtonCaption === false)
        {
            cancelButtonCaption = null;
        }
        var dlg = new DblEj.UI.Dialog(title, contentsElement, confirmButtonCaption, cancelButtonCaption, autohideOnConfirm);
        dlg.Show(width, height, x, y, onConfirmCallback, onCancelCallback, zIndexOrMaxZIndexSelector);
        contentsElement.Show();
        return dlg;
    };
DblEj.UI.Dialog._dialogCount = 0;
DblEj.UI.Dialog._openDialogCount = 0;
DblEj.UI.Dialog.LastLocation = null;
DblEj.UI.Dialog.LastLocationWidth = null;
DblEj.UI.Dialog.LastLocationHeight = null;