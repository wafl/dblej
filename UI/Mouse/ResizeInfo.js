/*
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class ResizeInfo Information about a resize operation.
 * @extends Class
 */
DblEj.UI.Mouse.ResizeInfo = Class.extend(
    {
        /**
         * @constructor
         */
        init: function ()
        {
            this._amResizing = false;
            this._resizingDiv = null;
            this._dragStartMouseLocation = new DblEj.UI.Point(0, 0);
            this._dragStartElementLocation = new DblEj.UI.Point(0, 0);
            this._resizeCallback = null;
        },
        /**
         * @property Get_AmResizing
         * @type {Boolean}
         */
        Get_AmResizing: function ()
        {
            return this._amResizing;
        },
        /**
         * @property Get_ResizeDiv The element being resized
         * @type {Element}
         */
        Get_ResizeDiv: function ()
        {
            return this._resizeDiv;
        },
        /**
         * @property Get_DragStartMouseLocation
         * @type {DblEj.UI.Point}
         */
        Get_DragStartMouseLocation: function ()
        {
            return this._dragStartMouseLocation;
        },
        /**
         * @property Get_DragStartElementLocation
         * @type {DblEj.UI.Point}
         */
        Get_DragStartElementLocation: function ()
        {
            return this._dragStartElementLocation;
        },
        /**
         * @property Get_ResizeCallback
         * @type {Function}
         */
        Get_ResizeCallback: function ()
        {
            return this._resizeCallback;
        },
        /**
         * @property Set_AmResizing
         * @type {Boolean}
         * @param {Boolean} newValue
         */
        Set_AmResizing: function (newValue)
        {
            this._amResizing = newValue;
        },
        /**
         * @property Set_ResizeDiv The div being resized
         * @type {Element}
         * @param {Element} newValue
         */
        Set_ResizeDiv: function (newValue)
        {
            this._resizeDiv = newValue;
        },
        /**
         * @property Set_DragStartMouseLocation
         * @type {DblEj.UI.Point}
         * @param {DblEj.UI.Point} newValue
         */
        Set_DragStartMouseLocation: function (newValue)
        {
            this._dragStartMouseLocation = newValue;
        },
        /**
         * @property Set_DragStartElementLocation
         * @type {DblEj.UI.Point}
         * @param {DblEj.UI.Point} newValue
         */
        Set_DragStartElementLocation: function (newValue)
        {
            this._dragStartElementLocation = newValue;
        },
        /**
         * @property Set_ResizeCallback
         * @type {Function}
         * @param {Function} newValue
         */
        Set_ResizeCallback: function (newValue)
        {
            this._resizeCallback = newValue;
        }
    });