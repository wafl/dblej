/**
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class DragData Key/val pair used for perststing info between drags and drops.
 * @extends Class
 */
DblEj.UI.Mouse.DragData = Class.extend(
    {
        /**
         * @constructor init
         * @param {String} key
         * @param {mixed} value
         */
        init: function (key, value)
        {
            this._key = key;
            this._value = value;
        },
        /**
         * @property Get_Key
         * @type {String}
         */
        Get_Key: function ()
        {
            return this._key;
        },
        /**
         * @property Get_Value
         * @type {mixed}
         */
        Get_Value: function ()
        {
            return this._value;
        }
    });