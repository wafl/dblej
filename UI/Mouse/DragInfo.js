/*
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class DragInfo Contains information about a drag-in-progress or a completed (dropped) drag.
 * @extends Class
 */
DblEj.UI.Mouse.DragInfo = Class.extend(
    {
        /**
         * @constructor init
         */
        init: function ()
        {
            this._amDragging = false;
            this._dragDiv = null;
            this._dragSource = null;
            this._dragData = null;
            this._dragStartMouseLocation = new DblEj.UI.Point(0, 0);
            this._dragStartElementLocation = new DblEj.UI.Point(0, 0);
            this._lockAxis = null;
        },
        /**
         * @property Get_AmDragging
         * @type {Boolean}
         */
        Get_AmDragging: function ()
        {
            return this._amDragging;
        },
        /**
         * @property Get_DragDiv The div to display during drag
         * @type {Element}
         */
        Get_DragDiv: function ()
        {
            return this._dragDiv;
        },
        /**
         * @property Get_DragSource The element being dragged
         * @type {Element}
         */
        Get_DragSource: function ()
        {
            return this._dragSource;
        },
        /**
         * @property Get_DragData
         * @type {DblEj.UI.Mouse.DragData}
         */
        Get_DragData: function ()
        {
            return this._dragData;
        },
        /**
         * @property Get_DragStartMouseLocation The location of the mouse when the drag was initiated
         * @type {DblEj.UI.Point}
         */
        Get_DragStartMouseLocation: function ()
        {
            return this._dragStartMouseLocation;
        },
        /**
         * @property Get_DragStartElementLocation The location of the drag element when the drag was initiated
         * @type {DblEj.UI.Point}
         */
        Get_DragStartElementLocation: function ()
        {
            return this._dragStartElementLocation;
        },
        /**
         * @property Get_LockAxis Which, if any, axis you want locked during the drag ("x"|"y")
         * @type {String}
         */
        Get_LockAxis: function ()
        {
            return this._lockAxis;
        },
        /**
         * @property Set_AmDragging
         * @type {Boolean}
         * @param {String} newValue
         */
        Set_AmDragging: function (newValue)
        {
            this._amDragging = newValue;
        },
        /**
         * @property Set_DragDiv The div to display during drag
         * @type {Element}
         * @param {String} newValue
         */
        Set_DragDiv: function (newValue)
        {
            this._dragDiv = newValue;
        },
        /**
         * @property Set_DragSource The element being dragged
         * @type {Element}
         * @param {String} newValue
         */
        Set_DragSource: function (newValue)
        {
            this._dragSource = newValue;
        },
        /**
         * @property Set_DragData
         * @type {DblEj.UI.Mouse.DragData}
         * @param {String} newValue
         */
        Set_DragData: function (newValue)
        {
            this._dragData = newValue;
        },
        /**
         * @property Set_DragStartMouseLocation
         * @type {DblEj.UI.Point}
         * @param {String} newValue
         */
        Set_DragStartMouseLocation: function (newValue)
        {
            this._dragStartMouseLocation = newValue;
        },
        /**
         * @property Set_DragStartElementLocation
         * @type {DblEj.UI.Point}
         * @param {String} newValue
         */
        Set_DragStartElementLocation: function (newValue)
        {
            this._dragStartElementLocation = newValue;
        },
        /**
         * @property Set_LockAxis Which, if any, axis you want locked during the drag ("x"|"y")
         * @type {String}
         * @param {String} newValue
         */
        Set_LockAxis: function (newValue)
        {
            this._lockAxis = newValue;
        }
    });