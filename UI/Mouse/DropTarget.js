/**
 * @namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class DropTarget Stores information about a target on which a dragged element can be dropped.
 * @extends Class
 */
DblEj.UI.Mouse.DropTarget = Class.extend(
    {
        init: function (targetElementId, onDropCallbackFunction, onHoverCallbackFunction, targetExcludedChildrenIds, targetData, onUnHoverCallbackFunction)
        {
            this._elementid = targetElementId;
            this._onDropCallbackFunction = onDropCallbackFunction;
            this._onHoverCallbackFunction = onHoverCallbackFunction;
            this._onUnHoverCallbackFunction = onUnHoverCallbackFunction;
            this._excludedChildrenIds = targetExcludedChildrenIds;
            this._targetData = targetData;
        },
        /**
         * @property Get_ElementId
         * @type {String}
         */
        Get_ElementId: function ()
        {
            return this._elementid;
        },
        /**
         * @property Get_OnDropCallbackFunction A function to call when something is dropped in the target
         * @type {Function}
         */
        Get_OnDropCallbackFunction: function ()
        {
            return this._onDropCallbackFunction;
        },
        /**
         * @property Get_OnHoverCallbackFunction A function to call when the target is hovered over
         * @type {Function}
         */
        Get_OnHoverCallbackFunction: function ()
        {
            return this._onHoverCallbackFunction;
        },
        /**
         * @property Get_OnUnHoverCallbackFunction A function to call when the target is unhovered
         * @type {Function}
         */
        Get_OnUnHoverCallbackFunction: function ()
        {
            return this._onHoverCallbackFunction;
        },
        /**
         * @property Get_TargetData
         * @type {mixed}
         */
        Get_TargetData: function ()
        {
            return this._targetData;
        },
        /**
         * @property Get_ExcludedChildrenIds The id's of child elements within the drop target where a drop is not permitted
         * @type {Array}
         */
        Get_ExcludedChildrenIds: function ()
        {
            return this._excludedChildrenIds;
        }
    });