/**
 * @namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

DblEj.UI.Mouse.Mouse = {};

/**
 * @class Mouse Information about the mouse's state.
 * @static
 */
DblEj.UI.Mouse.Mouse._location = new DblEj.UI.Point(0, 0);

/**
 * Get the mouse's current location, in pixels.
 * @returns {DblEj.UI.Point}
 */
DblEj.UI.Mouse.Mouse.Get_Location = function ()
{
    return DblEj.UI.Point.FromPoint(DblEj.UI.Mouse.Mouse._location);
};

window.AddMouseMoveHandler(function (event)
{
    DblEj.UI.Mouse.Mouse._location.Set_X(event.clientX);
    DblEj.UI.Mouse.Mouse._location.Set_Y(event.clientY);
});