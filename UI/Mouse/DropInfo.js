
/*
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class DropInfo Contains information about drop targets.
 * @extends Class
 */
DblEj.UI.Mouse.DropInfo = Class.extend(
    {
        /**
         * @constructor init
         */
        init: function ()
        {
            this._dropTargets = new Array();
            this._dropTargetCount = 0;
            this._highlightedTarget = null;
            this._highlightedTargetOriginalBorder = null;
        },
        /**
         * @property Get_DropTargets
         * @type {Array}
         */
        Get_DropTargets: function ()
        {
            return this._dropTargets;
        },
        /**
         * @property Get_DropTargetCount
         * @type {Number}
         */
        Get_DropTargetCount: function ()
        {
            return this._dropTargetCount;
        },
        /**
         * @property Get_HighlightedTarget
         * @type {Element}
         */
        Get_HighlightedTarget: function ()
        {
            return this._highlightedTarget;
        },
        /**
         * @property Get_HighlightedTargetOriginalBorder a hex color string
         * @type {String}
         */
        Get_HighlightedTargetOriginalBorder: function ()
        {
            return this._highlightedTargetOriginalBorder;
        },
        /**
         * @property Set_DropTargets
         * @param {Array} newValue
         */
        Set_DropTargets: function (newValue)
        {
            this._dropTargets = newValue;
        },
        /**
         * @property Set_DropTargetCount
         * @param {Number} newValue
         */
        Set_DropTargetCount: function (newValue)
        {
            this._dropTargetCount = newValue;
        },
        /**
         * @property Set_HighlightedTarget
         * @param {Element} newValue
         */
        Set_HighlightedTarget: function (newValue)
        {
            this._highlightedTarget = newValue;
        },
        /**
         * @property Set_HighlightedTargetOriginalBorder
         * @param {String} newValue
         */
        Set_HighlightedTargetOriginalBorder: function (newValue)
        {
            this._highlightedTargetOriginalBorder = newValue;
        }
    });
