/*
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class DragDropSettings Contains settings for a dran and drop operation.
 * @extends Class
 */
DblEj.UI.Mouse.DragDropSettings = Class.extend(
    {
        /**
         * @constructor init
         */
        init: function ()
        {
            this._highlightedBorderStyle = "solid 2px #3300dd";
            this._highlightedBackgroundColor = "#ffff44";
            this._dragSourceCssDisplayStyle = "block";
            this._reshowOriginalAfterDropOffTarget = false;
            this._reshowOriginalAfterDropOnTarget = false;
        },
        /**
         * @property Get_HighlightedBorderStyle The style of the border when an element is highlighted (used for highlighting drop targets on hover)
         * @type {String}
         */
        Get_HighlightedBorderStyle: function ()
        {
            return this._highlightedBorderStyle;
        },
        /**
         * @property Get_HighlightedBackgroundColor
         * @type {String}
         */
        Get_HighlightedBackgroundColor: function ()
        {
            return this._highlightedBackgroundColor;
        },
        /**
         * @property Get_DragSourceCssDisplayStyle The styll of the source element while it is being dragged
         * @type {String}
         */
        Get_DragSourceCssDisplayStyle: function ()
        {
            return this._dragSourceCssDisplayStyle;
        },
        /**
         * @property Get_ReshowOriginalAfterDropOffTarget Whether to show the original element if the user drops it somewhere other than a drop target
         * @type {Boolean}
         */
        Get_ReshowOriginalAfterDropOffTarget: function ()
        {
            return this._reshowOriginalAfterDropOffTarget;
        },
        /**
         * @property Get_ReshowOriginalAfterDropOnTarget Whether to show the original element if the user drops it on a drop target
         * @type {Boolean}
         */
        Get_ReshowOriginalAfterDropOnTarget: function ()
        {
            return this._reshowOriginalAfterDropOnTarget;
        },
        /**
         * @property Set_HighlightedBorderStyle
         * @type {String}
         *
         * @param {String} newValue
         */
        Set_HighlightedBorderStyle: function (newValue)
        {
            this._highlightedBorderStyle = newValue;
        },
        /**
         * @property Set_HighlightedBackgroundColor
         * @type {String}
         *
         * @param {String} newValue
         */
        Set_HighlightedBackgroundColor: function (newValue)
        {
            this._highlightedBackgroundColor = newValue;
        },
        /**
         * @property Set_DragSourceCssDisplayStyle
         * @type {String}
         * @param {String} newValue
         */
        Set_DragSourceCssDisplayStyle: function (newValue)
        {
            this._dragSourceCssDisplayStyle = newValue;
        },
        /**
         * @property Set_ReshowOriginalAfterDropOffTarget
         * @type {Boolean}
         * @param {Boolean} newValue
         */
        Set_ReshowOriginalAfterDropOffTarget: function (newValue)
        {
            this._reshowOriginalAfterDropOffTarget = newValue;
        },
        /**
         * @property Set_ReshowOriginalAfterDropOnTarget
         * @type {Boolean}
         * @param {Boolean} newValue
         */
        Set_ReshowOriginalAfterDropOnTarget: function (newValue)
        {
            this._reshowOriginalAfterDropOnTarget = newValue;
        }
    });