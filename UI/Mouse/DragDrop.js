/*
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class DragDrop Utility class to make it easier to add drag and drop capabilities to a web site
 * @static
 *
 * @deprecated in favor of the HTML5 Drag and Drop API
 */
DblEj.UI.Mouse.DragDrop = {};

DblEj.UI.Mouse.DragDrop._dropInfo = new DblEj.UI.Mouse.DropInfo();
DblEj.UI.Mouse.DragDrop._dragInfo = new DblEj.UI.Mouse.DragInfo();
DblEj.UI.Mouse.DragDrop._settings = new DblEj.UI.Mouse.DragDropSettings();


/**
 * @method AddDropTarget Designate an element as a drop target (a place where dragged items can be dropped).
 * 
 * @static
 * @param {String} dropTargetElementId
 * @param {Function} onDropCallbackFunction
 * @param {Function} onHoverCallbackFunction
 * @param {Array} excludeSourceElemIds Child-elements inside the drop target that cannot accept drops.
 * @param {mixed} dropTargetData
 * @return void
 */
DblEj.UI.Mouse.DragDrop.AddDropTarget =
    function (dropTargetElementId, onDropCallbackFunction, onHoverCallbackFunction, excludeSourceElemIds, dropTargetData)
    {
        var newDropTarget;
        newDropTarget =
            new DblEj.UI.Mouse.DropTarget(dropTargetElementId, onDropCallbackFunction, onHoverCallbackFunction, excludeSourceElemIds, dropTargetData);
        DblEj.UI.Mouse.DragDrop._dropInfo.Get_DropTargets()[DblEj.UI.Mouse.DragDrop._dropInfo.Get_DropTargetCount()] =
            newDropTarget;
        DblEj.UI.Mouse.DragDrop._dropInfo.Set_DropTargetCount(DblEj.UI.Mouse.DragDrop._dropInfo.Get_DropTargetCount() + 1);
    };

/**
 * @method BeginDrag
 * @static
 * @param {Element} elementToDrag
 * @param {String} dragText
 * @param {String} dragDataKey
 * @param {mixed} dragData
 * @param {Integer} dragVisualMode
 *                  1=Show the actual div being dragged,
 *                  2=Show the a visual representation of the div being dragged,
 *                  3=Show the dragText with a blank white rectangle background,
 *                  4=show the dragText with no background,
 *                  5=don't show anything (just the mouse cursor
 * @param {Boolean} hideOriginalDuringDrag
 * @param {Boolean} reshowOriginalAfterDropOnTarget
 * @param {Boolean} reshowOriginalAfterDropOffTarget
 * @param {Boolean} visualRepresentation
 * @param {DblEj.UI.Point} locationOffset
 * @param {String} lockAxis either "x" or "y"
 *
 */
DblEj.UI.Mouse.DragDrop.BeginDrag =
    function (elementToDrag, dragText, dragDataKey, dragData, dragVisualMode, hideOriginalDuringDrag, reshowOriginalAfterDropOnTarget, reshowOriginalAfterDropOffTarget, visualRepresentation, locationOffset, lockAxis)
    {
        document.onmousedown = function () {
            return false;
        };
        document.onselectionstart = function () {
            return false;
        };
        document.onselectstart = function () {
            return false;
        };
        document.ondragstart = function () {
            return false;
        };

        if (!DblEj.UI.Mouse.DragDrop._dragInfo.Get_AmDragging()) {
            var dragSource = elementToDrag;
            dragSource.SetCss("margin", "0px");
            
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_AmDragging(true);
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_DragSource(dragSource);
            DblEj.UI.Mouse.DragDrop._settings.Set_DragSourceCssDisplayStyle(dragSource.style.display);
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_DragData(new DblEj.UI.Mouse.DragData(dragDataKey, dragData));
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_LockAxis(lockAxis);
            DblEj.UI.Mouse.DragDrop._settings.Set_ReshowOriginalAfterDropOnTarget(reshowOriginalAfterDropOnTarget);
            DblEj.UI.Mouse.DragDrop._settings.Set_ReshowOriginalAfterDropOffTarget(reshowOriginalAfterDropOffTarget);

            var dragElement;
            switch (dragVisualMode)
            {
                case 1: //must be absolute
                    if (dragSource.GetStyle("position") != "absolute" && dragSource.GetStyle("position") != "fixed")
                    {
                        throw "Can't make visual drag out of non object if it is not set to an absolute/fixed position";
                    }
                    dragElement = dragSource;
                    break;
                case 2:
                    dragElement = MakeNewDivForVisualDrag(visualRepresentation, dragSource, locationOffset);
                    dragElement.innerHTML = dragSource.innerHTML;
                    dragElement.style.border = dragSource.style.border;
                    dragElement.style.backgroundColor = dragSource.style.backgroundColor;
                    break;
                case 3:
                    dragElement = MakeNewDivForVisualDrag(dragSource, dragSource, locationOffset);
                    dragElement.innerHTML = dragText;
                    dragElement.style.backgroundColor = '#ffffff';
                    break;
                case 4:
                    dragElement = MakeNewDivForVisualDrag(dragSource, dragSource, locationOffset);
                    dragElement.innerHTML = dragText;
                    dragElement.style.backgroundColor = 'none';
                    break;
                case 5:
                    dragElement = null;
                    dragElement.innerHTML = "";
                    dragElement.style.backgroundColor = 'none';
                    break;
            }
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_DragDiv(dragElement);
            dragElement.BringToFront();
            if (hideOriginalDuringDrag) {
                dragSource.style.display = 'none';
            }
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_DragStartElementLocation(dragElement.GetAbsolutePosition());
            DblEj.UI.Mouse.DragDrop._dragInfo.Set_DragStartMouseLocation(DblEj.UI.Mouse.Mouse.Get_Location());
        }

        function MakeNewDivForVisualDrag(lookTemplate, locationTemplate, locationOffset)
        {
            var newdiv = document.createElement("div");
            document.body.appendChild(newdiv);

            var sourceAbsPosition = locationTemplate.GetAbsolutePosition();
            var sourceHeight = lookTemplate.GetCurrentStyle().height;
            var sourceWidth = lookTemplate.GetCurrentStyle().width;

            newdiv.className = lookTemplate.className;
            newdiv.style.position = 'absolute';
            newdiv.style.display = 'block';

            if (typeof locationOffset != "undefined" && typeof locationOffset != undefined && locationOffset != null)
            {
                newdiv.style.left = (sourceAbsPosition.Get_X() + locationOffset.Get_X()) + 'px';
                newdiv.style.top = (sourceAbsPosition.Get_Y() + locationOffset.Get_Y()) + 'px';
            } else {
                newdiv.style.left = (sourceAbsPosition.Get_X()) + 'px';
                newdiv.style.top = (sourceAbsPosition.Get_Y()) + 'px';
            }
            newdiv.style.width = sourceWidth;
            newdiv.style.height = sourceHeight;
            newdiv.style.borderWidth = lookTemplate.GetCurrentStyle().borderWidth;
            newdiv.style.borderStyle = lookTemplate.GetCurrentStyle().borderStyle;
            newdiv.style.borderColor = lookTemplate.GetCurrentStyle().borderColor;
            newdiv.style.border = lookTemplate.GetCurrentStyle().border;
            newdiv.style.background = lookTemplate.GetCurrentStyle().background;
            newdiv.style.textAlign = lookTemplate.GetCurrentStyle().textAlign;
            newdiv.style.fontSize = lookTemplate.GetCurrentStyle().fontSize;
            newdiv.style.fontFamily = lookTemplate.GetCurrentStyle().fontFamily;
            newdiv.style.color = lookTemplate.GetCurrentStyle().color;
            return newdiv;
        }
    };

/**
 * @method ProcessHoverObject
 * @static
 * @param {DblEj.UI.Point} hoverLocation
 * @return {Boolean}
 */
DblEj.UI.Mouse.DragDrop.ProcessHoverObject = function (hoverLocation)
{
    //see if we are dragging a source over a potential target, if so highlight it
    var dropTarget;
    var dropTargetElement;
    var hoverProcessed = false;

    dropTarget = DblEj.UI.Mouse.DragDrop.TargetHitTest(hoverLocation);

    if (dropTarget != null)
    {
        dropTargetElement = $(dropTarget.Get_ElementId());

        //its in the target zone... call the target function
        if (dropTarget.OnHoverCallbackFunction) {
            hoverProcessed =
                dropTarget.Get_OnHoverCallbackFunction()(DblEj.UI.Mouse.DragDrop._dragInfo.DragData, dragSource, hoverLocation);
        } else {
            hoverProcessed = true;
        }

        if (hoverProcessed) {
            if (DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget && DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget !=
                dropTargetElement) {
                DblEj.UI.Mouse.DragDrop.UnHighlightDropTarget();
            }
            if (!DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget || DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget !=
                dropTargetElement) {
                DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget = dropTargetElement;
                DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTargetOriginalBorder = dropTargetElement.style.backgroundColor;
                dropTargetElement.style.backgroundColor = DblEj.UI.Mouse.DragDrop._settings.Get_HighlightedBackgroundColor();
            }
        } else {
            DblEj.UI.Mouse.DragDrop.UnHighlightDropTarget();
        }
    } else {
        DblEj.UI.Mouse.DragDrop.UnHighlightDropTarget();
    }
    return hoverProcessed;
};

/**
 * @method UnHighlightDropTarget
 * @static
 * @return void
 */
DblEj.UI.Mouse.DragDrop.UnHighlightDropTarget = function ()
{
    if (DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget) {
        DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget.style.backgroundColor =
            DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTargetOriginalBorder;
        DblEj.UI.Mouse.DragDrop._dropInfo.HighlightedTarget = null;
    }
};

/**
 * @method DropCurrentDragItem
 * @static
 * @return void
 */
DblEj.UI.Mouse.DragDrop.DropCurrentDragItem = function ()
{
    if (DblEj.UI.Mouse.DragDrop._dragInfo.Get_AmDragging()) {
        document.onmousedown = null;
        document.onselectionstart = null;
        document.onselectstart = null;

        var dragElement = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragDiv();
        var dragSource = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragSource();
        var dropProcessed = DblEj.UI.Mouse.DragDrop.ProcessDrop(dragElement.GetAbsolutePosition());
        if (dropProcessed)
        {
            if (DblEj.UI.Mouse.DragDrop._settings.Get_ReshowOriginalAfterDropOnTarget())
            {
                //if explicitly told to do so by the caller
                //or if the drop wasnt even on a valid target
                //then redisplay the original drag source
                dragSource.style.display = DblEj.UI.Mouse.DragDrop._settings.Get_DragSourceCssDisplayStyle();
            } else {
                dragSource.style.display = 'none';
            }
        } else {
            if (DblEj.UI.Mouse.DragDrop._settings.Get_ReshowOriginalAfterDropOffTarget())
            {
                //if explicitly told to do so by the caller
                //or if the drop wasnt even on a valid target
                //then redisplay the original drag source
                dragSource.style.display = DblEj.UI.Mouse.DragDrop._settings.Get_DragSourceCssDisplayStyle();
            } else {
                dragSource.style.display = 'none';
            }
        }

        if (dragElement != dragSource)
        {
            document.body.removeChild(dragElement);
            dragElement = null;
        }
        dragSource = null;
        DblEj.UI.Mouse.DragDrop._settings.Set_ReshowOriginalAfterDropOnTarget(false);
        DblEj.UI.Mouse.DragDrop._settings.Set_ReshowOriginalAfterDropOffTarget(false);
        DblEj.UI.Mouse.DragDrop._dragInfo.Set_AmDragging(false);

        //renable text selection
        document.onmousedown = null;
        document.onselectionstart = null;
    }
};
/**
 * @method TargetHitTest
 * @static
 * @param {DblEj.UI.Point} location
 * @param {Element} dragElement
 * @return {DblEj.UI.Mouse.DropTarget}
 */
DblEj.UI.Mouse.DragDrop.TargetHitTest = function (location, dragElement)
{
    if (!IsDefined(dragElement))
    {
        dragElement = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragDiv();
    }
    var returnTarget = null;
    var dropTarget;
    var dropTargetElement;
    var targetRight;
    var targetBottom;
    var excludeElem;
    var excludeLocation;
    var excludeBottom;
    var excludeRight;
    var isExcluded = false;
    var targetLocation;

    for (i = 0; i < DblEj.UI.Mouse.DragDrop._dropInfo.Get_DropTargetCount(); i++)
    {
        dropTarget = DblEj.UI.Mouse.DragDrop._dropInfo.Get_DropTargets()[i];

        if (dropTarget)
        {
            dropTargetElement = $(dropTarget.Get_ElementId());
            if (dropTargetElement) {
                targetLocation = dropTargetElement.GetAbsolutePosition();
                if (dropTarget.Get_ExcludedChildrenIds().length > 0) {
                    for (var ex = 0; ex < dropTarget.Get_ExcludedChildrenIds().length; ex++)
                    {
                        excludeElem = $(dropTarget.TargetExcludedChildrenIds[ex]);
                        if (excludeElem) {
                            if (dragElement.CollidesWith(excludeElem))
                            {
                                //this drop target is exluded, do not process
                                isExcluded = true;
                                break;
                            }
                        }
                    }
                }
                if (!isExcluded) {
                    if (dragElement.CollidesWith($(dropTarget.Get_ElementId()))) {
                        returnTarget = dropTarget;
                    }
                }
            }
        }
    }
    return returnTarget;
};

/**
 * @method ProcessDrop
 * @static
 * @param {DblEj.UI.Point} dropLocation
 */
DblEj.UI.Mouse.DragDrop.ProcessDrop = function (dropLocation)
{
    var dropTarget;
    var dropProcessed = false;
    var dropTargetLocation;

    dropTarget = DblEj.UI.Mouse.DragDrop.TargetHitTest(dropLocation);
    if (dropTarget)
    {
        var dropTargetElement = $(dropTarget.Get_ElementId());
        if (dropTarget != null && dropTargetElement)
        {
            //its in the target zone... call the target function
            if (dropTarget.Get_OnDropCallbackFunction())
            {
                dropTargetLocation = dropTargetElement.GetAbsolutePosition();
                var dragElement = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragDiv();
                var dragSource = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragSource();
                var dragObjectLocation = dragElement.GetAbsolutePosition();
                dragObjectLocation.Set_X(dragObjectLocation.Get_X() - dropTargetLocation.Get_X());
                dragObjectLocation.Set_Y(dragObjectLocation.Get_Y() - dropTargetLocation.Get_Y());
                dropProcessed =
                    dropTarget.Get_OnDropCallbackFunction()(DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragData(), dragSource, dragObjectLocation, dropTarget);
            } else {
                dropProcessed = true;
            }
        }
        DblEj.UI.Mouse.DragDrop.UnHighlightDropTarget();
    }
    return dropProcessed;
};

DblEj.UI.Mouse.DragDrop.MouseMove_handler = function (e)
{
    if (DblEj.UI.Mouse.DragDrop._dragInfo.Get_AmDragging()) {
        var newLeft;
        var newTop;
        var dragElement = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragDiv();
        if (DblEj.UI.Mouse.DragDrop._dragInfo.Get_LockAxis() == "x")
        {
            newLeft = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartElementLocation()
                .Get_X();
            newTop = DblEj.UI.Mouse.Mouse.Get_Location()
                .Get_Y() -
                (DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartMouseLocation()
                    .Get_Y() -
                    DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartElementLocation()
                    .Get_Y());
        }
        else if (DblEj.UI.Mouse.DragDrop._dragInfo.Get_LockAxis() == "y")
        {
            newLeft = DblEj.UI.Mouse.Mouse.Get_Location()
                .Get_X() -
                (DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartMouseLocation()
                    .Get_X() -
                    DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartElementLocation()
                    .Get_X());
            newTop = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartElementLocation()
                .Get_Y();
        }
        else
        {
            newLeft = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartElementLocation().Get_X() +
                (DblEj.UI.Mouse.Mouse.Get_Location().Get_X() - DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartMouseLocation().Get_X());
            newTop = DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartElementLocation().Get_Y() +
                (DblEj.UI.Mouse.Mouse.Get_Location().Get_Y() - DblEj.UI.Mouse.DragDrop._dragInfo.Get_DragStartMouseLocation().Get_Y());
        }
        if (newTop < 0)
        {
            newTop = 0;
        }
        if (newLeft < 0)
        {
            newLeft = 0;
        }
        dragElement.SetCssLocation(newLeft, newTop);
        var dragObjectLocation = dragElement.GetAbsolutePosition();
        if (dragObjectLocation.Get_X() < 0)
        {
            dragElement.SetCssX(0);
        }
        if (dragObjectLocation.Get_Y() < 0)
        {
            dragElement.SetCssY(0);
        }
        DblEj.UI.Mouse.DragDrop.ProcessHoverObject(dragObjectLocation);
        DblEj.EventHandling.Events.PreventDefaultEvent(e);
    }
};
DblEj.UI.Mouse.DragDrop.MouseUp_handler = function (e)
{
    if (DblEj.UI.Mouse.DragDrop._dragInfo.Get_AmDragging())
    {
        DblEj.UI.Mouse.DragDrop.DropCurrentDragItem();
    }
};

DblEj.UI.Mouse.DragDrop.DisableDefaultBrowserDragging = function ()
{
    if (DblEj.UI.Mouse.DragDrop._dragInfo.Get_AmDragging())
    {
        return false;
    } else {
        return true;
    }
};

//if the browser is IE4+
if (DblEj.Util.WebBrowser.IsIeVariant())
{
    document.onselectstart = DblEj.UI.Mouse.DragDrop.DisableDefaultBrowserDragging;
} else if (window.sidebar) {
    document.onmousedown = DblEj.UI.Mouse.DragDrop.DisableDefaultBrowserDragging;
    document.onclick = DblEj.UI.Mouse.DragDrop.DisableDefaultBrowserDragging;
}
DblEj.EventHandling.Events.AddHandler(document, "mousemove", DblEj.UI.Mouse.DragDrop.MouseMove_handler, false);
DblEj.EventHandling.Events.AddHandler(document, "mouseup", DblEj.UI.Mouse.DragDrop.MouseUp_handler, false);