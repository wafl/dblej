/*
 *@namespace DblEj.UI.Mouse
 */
Namespace("DblEj.UI.Mouse");

/**
 * @class Resize Utility class to make it easier to add drag-and-resize capabilities to a web site
 * @static
 */
DblEj.UI.Mouse.DragResize = {};

DblEj.UI.Mouse.DragResize._resizeInfo = new DblEj.UI.Mouse.ResizeInfo();

/**
 * @method BeginResize
 * @static
 * @param {Element} elementToResize
 * @param {Function} resizeCallback
 * @param {DblEj.UI.Point} locationOffset
 */
DblEj.UI.Mouse.DragResize.BeginResize = function (elementToResize, resizeCallback, locationOffset)
{
    if (!DblEj.UI.Mouse.DragResize._resizeInfo.Get_AmResizing()) {
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_ResizeCallback(resizeCallback);
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_AmResizing(true);
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_ResizeDiv(elementToResize);
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_DragStartMouseLocation(DblEj.UI.Point.FromPoint(DblEj.UI.Mouse.Mouse.Get_Location()));
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_DragStartElementLocation(DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeDiv()
            .GetAbsolutePosition());
    }
};

/**
 * @method StopItemResize
 * @static
 */
DblEj.UI.Mouse.DragResize.StopItemResize = function ()
{
    if (DblEj.UI.Mouse.DragResize._resizeInfo.Get_AmResizing()) {
        if (DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeCallback())
        {
            DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeCallback()(DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeDiv());
        }
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_AmResizing(false);
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_ResizeCallback(null);
        DblEj.UI.Mouse.DragResize._resizeInfo.Set_ResizeDiv(null);
    }
};


DblEj.UI.Mouse.DragResize.MouseMove_handler = function (e)
{
    if (DblEj.UI.Mouse.DragResize._resizeInfo.Get_AmResizing()) {
        var newRightOffset = DblEj.UI.Mouse.Mouse.Get_Location()
            .Get_X() - DblEj.UI.Mouse.DragResize._resizeInfo.Get_DragStartMouseLocation()
            .Get_X();
        var newBottomOffset = DblEj.UI.Mouse.Mouse.Get_Location()
            .Get_Y() -
            DblEj.UI.Mouse.DragResize._resizeInfo.Get_DragStartMouseLocation()
            .Get_Y();
        DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeDiv()
            .SetCssSize(DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeDiv().clientWidth +
                newRightOffset, DblEj.UI.Mouse.DragResize._resizeInfo.Get_ResizeDiv().clientHeight +
                newBottomOffset);
    }
};
DblEj.UI.Mouse.DragResize.MouseUp_handler = function (e)
{
    if (DblEj.UI.Mouse.DragResize._resizeInfo.Get_AmResizing())
    {
        DblEj.UI.Mouse.DragResize.StopItemResize();
    }
};

DblEj.UI.Mouse.DragResize.DisableDefaultBrowserDragging = function ()
{
    if (DblEj.UI.Mouse.DragResize._resizeInfo.Get_AmResizing())
    {
        return false;
    } else {
        return true;
    }
};



//if the browser is IE4+
if (DblEj.Util.WebBrowser.IsIeVariant())
{
    document.onselectstart = DblEj.UI.Mouse.DragResize.DisableDefaultBrowserDragging;
} else if (window.sidebar) {
    document.onmousedown = DblEj.UI.Mouse.DragResize.DisableDefaultBrowserDragging;
    document.onclick = DblEj.UI.Mouse.DragResize.DisableDefaultBrowserDragging;
}

DblEj.EventHandling.Events.AddHandler(document, "mousemove", DblEj.UI.Mouse.DragResize.MouseMove_handler, false);
DblEj.EventHandling.Events.AddHandler(document, "mouseup", DblEj.UI.Mouse.DragResize.MouseUp_handler, false);