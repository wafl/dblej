/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Effects Utility class provides methods for animating elements.
 * @static
 */
DblEj.UI.Effects = {};


DblEj.UI.Effects._animationframeRate = 77;
DblEj.UI.Effects._animateInterval = 1000 / DblEj.UI.Effects._animationframeRate;
DblEj.UI.Effects._animationStart = new Date().getTime();
DblEj.UI.Effects._animationLastTick = new Date().getTime();
DblEj.UI.Effects._animationTickCount = 0;
DblEj.UI.Effects._tweenOperations = [];

/**
 * @function AddPropertyAnimation Add a property animation to the internal animation queue.
 *
 * @param {Element} elem
 * @param {String} propertyName
 * @param {Number} targetNumericValue
 * @param {String} valueNonNumericSuffix
 * @param {Number} speed
 * @param {Number} accelerate
 * @param {Number} decelerate
 * @param {Number} accelerateCurve
 * @param {Number} decelerateCurve
 * @param {Function} callback
 * @param {Function} valueFormatter a function to run on the value before assiging it to the property
 */
DblEj.UI.Effects.AddPropertyAnimation =
    function (elem, propertyName, targetNumericValue, valueNonNumericSuffix, speed, accelerate, decelerate, accelerateCurve, decelerateCurve, callback, valueFormatter)
    {
        if (!IsDefined(valueNonNumericSuffix) || valueNonNumericSuffix == null)
        {
            valueNonNumericSuffix = "";
        }
        if (!IsDefined(valueFormatter))
        {
            valueFormatter = null;
        }
        DblEj.UI.Effects.RemoveAnimation(elem, propertyName);
        var tweenObject = new Object();
        tweenObject["tweenObject"] = elem;
        tweenObject["tweenProperty"] = propertyName;
        tweenObject["tweenPropertySuffix"] = valueNonNumericSuffix;
        tweenObject["pixelsPerMinute"] = speed;
        tweenObject["targetValue"] = targetNumericValue;
        tweenObject["lastTick"] = 0;
        tweenObject["lastAction"] = 0;
        tweenObject["accelerate"] = accelerate;
        tweenObject["decelerate"] = decelerate;
        tweenObject["accelorateCurve"] = accelerateCurve; //1 = linear
        tweenObject["decelorateCurve"] = decelerateCurve; //1 = linear
        tweenObject["callback"] = callback;
        tweenObject["valueFormatter"] = valueFormatter;
        tweenObject["isCss"] = false;
        DblEj.UI.Effects._tweenOperations[DblEj.UI.Effects._tweenOperations.length] = tweenObject;
    };

/**
 * @function AddAnimation Add a CSS animation request to the internal animation queue.
 * 
 * @param {Element} elem
 * @param {String} propertyName
 * @param {Number} targetNumericValue
 * 
 * @param {String} valueNonNumericSuffix If the property requires numeric values with a unit appended on to it,
 * you cannot provide that as a targetNumericValue because the internal animator
 * cannot operate on a string mathmatically.  
 * Instead, provide the string portion of the value in valueNonNumericSuffix.
 * 
 * @param {Number} speed The speed that the animation will occur, in units per minute.
 * The unit is determined by the property being set and potentially the valueNonNumericSuffix.
 * Typically, if you specify a valueNonNumericSuffix, that will also be the unit used.
 * 
 * @param {Number} accelerate
 * @param {Number} decelerate
 * @param {Number} accelerateCurve
 * @param {Number} decelerateCurve
 * 
 * @param {Function} callback
 * The callback will be called when the animation is completed.
 *
 * @param {String} valueNonNumericPrefix
 * @param {String|Number} startValue
 * @param {Function} valueFormatter
 * a function to run on the value before assiging it to the property
 * @returns {DblEj.UI.Effects}
 */
DblEj.UI.Effects.AddAnimation =
    function (elem, propertyName, targetNumericValue, valueNonNumericSuffix, speed, accelerate, decelerate, accelerateCurve, decelerateCurve, callback, valueNonNumericPrefix, startValue, valueFormatter)
    {
        if (!IsDefined(valueNonNumericSuffix) || valueNonNumericSuffix === null)
        {
            valueNonNumericSuffix = "";
        }
        if (!IsDefined(valueNonNumericPrefix) || valueNonNumericPrefix === null)
        {
            valueNonNumericPrefix = "";
        }
        if (!IsDefined(valueFormatter))
        {
            valueFormatter = null;
        }
        DblEj.UI.Effects.RemoveAnimation(elem, propertyName);
        var tweenObject = new Object();
        tweenObject["tweenObject"] = elem;
        tweenObject["tweenProperty"] = propertyName;
        tweenObject["tweenPropertySuffix"] = valueNonNumericSuffix;
        tweenObject["tweenPropertyPrefix"] = valueNonNumericPrefix;
        tweenObject["pixelsPerMinute"] = speed;
        if (IsDefined(startValue) && startValue != null)
        {
            tweenObject["startValue"] = startValue;
            tweenObject["currentValue"] = startValue;
        }
        if (targetNumericValue.toString().substring(0, 4) == "rgb(")
        {
            var colorParts = targetNumericValue.toString()
                .match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i);
            tweenObject["targetColor"] = new DblEj.UI.Color(colorParts[1], colorParts[2], colorParts[3]);

            var valString = elem.GetCss(propertyName, true);
            if (valString != null)
            {
                valString = valString.toString();
            } else {
                valString = "";
            }
            if (valString.substring(0, 4) == "rgb(")
            {
                var colorParts = valString.match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i);
                tweenObject["startColor"] = new DblEj.UI.Color(colorParts[1], colorParts[2], colorParts[3]);
                tweenObject["currentColorPerc"] = 0;
            } else {
                throw "starting value of element property cannot be the starting point for a color transition";
            }

            tweenObject["colorTransition"] = new DblEj.UI.ColorTransition(tweenObject["startColor"], tweenObject["targetColor"]);
            tweenObject["targetValue"] = 100;
            tweenObject["valueType"] = "rgb";
        } else {
            tweenObject["targetValue"] = targetNumericValue;
            tweenObject["valueType"] = "number";
        }
        tweenObject["lastTick"] = 0;
        tweenObject["lastAction"] = 0;
        tweenObject["accelerate"] = accelerate;
        tweenObject["decelerate"] = decelerate;
        tweenObject["accelorateCurve"] = accelerateCurve; //1 = linear
        tweenObject["decelorateCurve"] = decelerateCurve; //1 = linear
        tweenObject["callback"] = callback;
        tweenObject["valueFormatter"] = valueFormatter;
        tweenObject["isCss"] = true;
        DblEj.UI.Effects._tweenOperations[DblEj.UI.Effects._tweenOperations.length] = tweenObject;
        return DblEj.UI.Effects; //allows you to chain static operations
    };

/**
 * Remove an animation from the internal animation queue.
 * @param {Element} elem
 * @param {String} propertyName
 */
DblEj.UI.Effects.RemoveAnimation = function (elem, propertyName)
{
    for (var operationIdx = DblEj.UI.Effects._tweenOperations.length; operationIdx--; ) {
        if (IsDefined(DblEj.UI.Effects._tweenOperations[operationIdx]))
        {
            var operation = DblEj.UI.Effects._tweenOperations[operationIdx];
            if ((operation.tweenObject == elem) && (operation.tweenProperty == propertyName))
            {
                DblEj.UI.Effects._tweenOperations[operationIdx] = null;
                delete DblEj.UI.Effects._tweenOperations[operationIdx];
            }
        } else {
            delete DblEj.UI.Effects._tweenOperations[operationIdx];
        }
    }
};

/**
 * Remove all animations from the internal animation queue.
 *
 * @param {Element} elem
 */
DblEj.UI.Effects.ClearAnimations = function (elem)
{
    for (var operationIdx = DblEj.UI.Effects._tweenOperations.length; operationIdx--; ) {
        var operation = DblEj.UI.Effects._tweenOperations[operationIdx];
        if (IsDefined(operation))
        {
            if ((operation.tweenObject == elem))
            {
                DblEj.UI.Effects._tweenOperations[operationIdx] = null;
                delete DblEj.UI.Effects._tweenOperations[operationIdx];
            }
        } else {
            delete DblEj.UI.Effects._tweenOperations[operationIdx];
        }
    }
};

DblEj.UI.Effects._framesWaitingToDrop = 0;
DblEj.UI.Effects._processAnimations = function ()
{
    var currentTime = new Date().getTime();
    if (DblEj.UI.Effects._framesWaitingToDrop == 0)
    {
        var timeSinceLastTick = currentTime - DblEj.UI.Effects._animationLastTick;
        var fallBehindAmount = Math.max(0, timeSinceLastTick - (DblEj.UI.Effects._animateInterval * 2)); //give the interval some wiggle room of twice the interval, so if its just a little late we give it a chance to catch up on its own.
        DblEj.UI.Effects._framesWaitingToDrop = Math.floor(fallBehindAmount / DblEj.UI.Effects._animateInterval);
        if (DblEj.UI.Effects._framesWaitingToDrop > 0)
        {
            clearInterval(DblEj.UI.Effects._animationTicker);
            //we leave 1 beacause after this block it will actually process one more time
            while(DblEj.UI.Effects._framesWaitingToDrop > 1)
            {
                DblEj.UI.Effects._processAnimations();
            }
            DblEj.UI.Effects._animationTicker = setInterval(DblEj.UI.Effects._processAnimations, Math.ceil(DblEj.UI.Effects._animateInterval));
        }
    }
    if (DblEj.UI.Effects._framesWaitingToDrop > 0)
    {
        DblEj.UI.Effects._framesWaitingToDrop--;
    }
    for (var operationIdx = DblEj.UI.Effects._tweenOperations.length; operationIdx--; ) {
        var operation = DblEj.UI.Effects._tweenOperations[operationIdx];
        if (IsDefined(operation))
        {
            var elem = operation["tweenObject"];
            var property = operation["tweenProperty"];
            var propertySuffix = IsDefined(operation["tweenPropertySuffix"])?operation["tweenPropertySuffix"]:"";
            var propertyPrefix = IsDefined(operation["tweenPropertyPrefix"])?operation["tweenPropertyPrefix"]:"";
            var isCss = operation["isCss"];
            var pixelsPerMinute = operation["pixelsPerMinute"];
            var pixelsPerSecond = pixelsPerMinute / 60;
            var pixelsPerMillisecond = pixelsPerSecond / 1000;
            var targetValue = operation["targetValue"];
            var valueType = operation["valueType"];
            var targetColor = null;
            if (valueType == "rgb")
            {
                targetColor = operation["targetColor"];
            }
            var direction = IsDefined(operation["direction"]) ? operation["direction"] : null;
            var accelerate = operation["accelerate"]; //percentage of beginning that should be curved
            var decelerate = operation["decelerate"]; //percentage of end that should be curved
            var accelerateCurve = operation["acceslorateCurve"]; //1 = linear
            var decelerateCurve = operation["decelerateCurve"]; //1 = linear
            var startValue;
            var startColor;
            if (isCss)
            {
                var valString = elem.GetCss(property, true);
                if (valString != null)
                {
                    valString = valString.toString();
                } else {
                    valString = "";
                }

                if (valString.substring(0, 4) == "rgb(")
                {
                    var colorParts = valString.match(/^rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)$/i);
                    startValue = 0;
                    if (IsDefined(operation["startColor"]))
                    {
                        startColor = operation["startColor"];
                    } else {
                        startColor = new DblEj.UI.Color(colorParts[1], colorParts[2], colorParts[3]);
                        operation["currentColorPerc"] = 0;
                    }
                }
                else if (valString.toString().substring(0, 1) == "#")
                {
                    throw "hex string color values currently not supported for animation";
                    startValue =
                        IsDefined(operation["startValue"]) ? operation["startValue"] : parseInt(valString.substring(1), 16);
                } else {
                    if (propertyPrefix == "")
                    {
                        startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(valString);
                    } else {
                        if ((valString.length >=  propertyPrefix.length) && (valString.substr(0, propertyPrefix.length) == propertyPrefix))
                        {
                            startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(valString.substr(propertyPrefix.length));
                        } else {
                            startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(valString);
                        }
                    }
                }
            } else {
                if (property == "text")
                {
                    startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(elem.GetText());
                }
                else if (propertyPrefix == "")
                {
                    startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(elem[property]);
                } else {
                    if ((elem[property].length >=  propertyPrefix.length) && (elem[property].substr(0, propertyPrefix.length) == propertyPrefix))
                    {
                        startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(elem[property].substr(propertyPrefix.length));
                    } else {
                        startValue = IsDefined(operation["startValue"]) ? operation["startValue"] : parseFloat(elem[property]);
                    }
                }
            }
            operation["startValue"] = startValue;
            operation["startColor"] = startColor;
            var lastAction = operation["lastAction"];
            var timeLapsed = currentTime - lastAction;
            var currentValue;
            if (isCss)
            {
                var valString = elem.GetCss(property, true);
                if (valString != null)
                {
                    valString = valString.toString();
                } else {
                    valString = "";
                }
                if (valString.substring(0, 4) == "rgb(")
                {
                    currentValue = operation["currentColorPerc"];
                }
                else if (valString.substring(0, 1) == "#")
                {
                    throw "hex string color values currently not supported for animation";
                    currentValue = parseInt(valString.substring(1), 16);
                } else if (IsDefined(operation["currentValue"])) {
                    currentValue = operation["currentValue"];
                } else {
                    var cssVal = elem.GetCss(property, true);
                    if (propertyPrefix == "")
                    {
                        currentValue = parseFloat(cssVal);
                    } else {
                        if ((cssVal.length >=  propertyPrefix.length) && (cssVal.substr(0, propertyPrefix.length) == propertyPrefix))
                        {
                            currentValue = parseFloat(cssVal.substr(propertyPrefix.length));
                        } else {
                            currentValue = parseFloat(cssVal);
                        }
                    }
                }
            } else {
                if (property == "text")
                {
                    currentValue = IsDefined(operation["currentValue"])?operation["currentValue"]:parseFloat(elem.GetText());
                } else {
                    if (propertyPrefix == "")
                    {
                        currentValue = parseFloat(elem[property]);
                    } else if (IsDefined(operation["currentValue"])) {
                        currentValue = operation["currentValue"];
                    } else {
                        if ((elem[property].length >=  propertyPrefix.length) && (elem[property].substr(0, propertyPrefix.length) == propertyPrefix))
                        {
                            currentValue = parseFloat(elem[property].substr(propertyPrefix.length));
                        } else {
                            currentValue = parseFloat(elem[property]);
                        }
                    }
                }
            }
            var pixelsToMove;
            if (lastAction > 0)
            {
                pixelsToMove = timeLapsed * pixelsPerMillisecond;
            } else {
                pixelsToMove = pixelsPerMillisecond;
            }

            operation["lastTick"] = currentTime;
            var targetNumericValue;
            if (targetValue.toString()
                .substring(0, 1) ==
                "#")
            {
                throw "hex string color values currently not supported for animation";
                targetNumericValue = parseInt(targetValue.toString()
                    .substring(1, 3)) <<
                    16 |
                    parseInt(targetValue.toString()
                        .substring(3, 5)) <<
                    8 |
                    parseInt(targetValue.toString()
                        .substring(5, 7));
                operation["isHexString"] = true;
            } else {
                targetNumericValue = targetValue;
                operation["isHexString"] = false;
            }

            var currentValueBaseZero;
            var currentProgress;
            if (!direction)
            {
                if (targetNumericValue < startValue)
                {
                    direction = -1;
                } else {
                    direction = 1;
                }
                operation["direction"] = direction;
            }

            if (direction === 1)
            {
                currentValueBaseZero = currentValue - startValue;
                currentProgress = (currentValueBaseZero / (targetNumericValue - startValue)) * 100;
            }
            else if (direction === -1)
            {
                currentValueBaseZero = startValue - currentValue;
                currentProgress = (currentValueBaseZero / (startValue - targetNumericValue)) * 100;
            }
            if (targetNumericValue === startValue)
            {
                currentProgress = 100;
                currentValueBaseZero = 100;
            }
            if ((currentProgress >= 0) && (currentProgress < accelerate) && (accelerate > 0))
            {
                var progressIntoAccelerateRatio;
                if (currentProgress === 0)
                {
                    progressIntoAccelerateRatio = 1 / accelerate;
                } else {
                    progressIntoAccelerateRatio = currentProgress / accelerate;
                }
                pixelsToMove = pixelsToMove * progressIntoAccelerateRatio;
            }
            else if ((currentProgress > (100 - decelerate)) && (currentProgress <= 100) && (decelerate > 0))
            {
                var progressIntoDecelerateRatio;
                if (currentProgress === 100)
                {
                    progressIntoDecelerateRatio = 1 / decelerate;
                } else {
                    progressIntoDecelerateRatio = (currentProgress - (100 - decelerate)) / decelerate;
                }

                pixelsToMove = pixelsToMove - (pixelsToMove * progressIntoDecelerateRatio);
            }
            //this used to be some fix for when it would crossover I think we dont need it anymore
            //dont want it because it messes up things like opactity which needs small values
            if ((pixelsToMove > 0) && pixelsToMove < .001)
            {
                //pixelsToMove = .001;
            }

            animationComplete = false;
            if (pixelsToMove > 0)
            {
                operation["lastAction"] = currentTime;
                if ((direction === 1) && currentValue < targetNumericValue)
                {
                    //if within the pixelsToMove of the target, then go straight to the value
                    if ((targetNumericValue - currentValue) <= pixelsToMove)
                    {
                        var newValue;
                        if (operation["valueFormatter"])
                        {
                            newValue = operation["valueFormatter"](propertyPrefix + targetNumericValue + propertySuffix);
                        } else {
                            newValue = propertyPrefix + targetNumericValue + propertySuffix;
                        }
                        if (isCss)
                        {
                            if (operation["isHexString"])
                            {
                                elem.SetCss(property, "#" + get_rgb(Math.ceil(targetNumericValue)) + propertySuffix);
                            } else if (operation["valueType"] == "rgb") {
                                operation["currentColorPerc"] = 100;
                                var currentColor =
                                    operation["colorTransition"].GetColorByProgress(operation["currentColorPerc"]);
                                elem.SetCss(property, currentColor.GetHex());
                            } else {
                                elem.SetCss(property, newValue);
                                operation["currentValue"] = targetNumericValue;
                            }
                        } else {
                            if (property == "text")
                            {
                                elem.SetText(newValue);
                            } else {
                                elem[property] = newValue;
                            }
                            operation["currentValue"] = targetNumericValue;
                        }
                        animationComplete = true;
                    } else {
                        if (operation["valueFormatter"])
                        {
                            newValue = operation["valueFormatter"](propertyPrefix + (currentValue + pixelsToMove) + propertySuffix);
                        } else {
                            newValue = propertyPrefix + (currentValue + pixelsToMove) + propertySuffix;
                        }
                        if (isCss)
                        {
                            if (operation["isHexString"])
                            {
                                elem.SetCss(property, "#" + get_rgb(Math.ceil(currentValue + pixelsToMove)) + propertySuffix);
                            } else if (operation["valueType"] == "rgb") {
                                operation["currentColorPerc"] = Math.ceil(currentValue + pixelsToMove);
                                var currentColor =
                                    operation["colorTransition"].GetColorByProgress(operation["currentColorPerc"]);
                                elem.SetCss(property, currentColor.GetHex());
                            } else {
                                elem.SetCss(property, newValue);
                                operation["currentValue"] = currentValue + pixelsToMove;
                            }
                        } else {
                            if (property == "text")
                            {
                                elem.SetText(newValue);
                            } else {
                                elem[property] = newValue;
                            }
                            operation["currentValue"] = currentValue + pixelsToMove;
                        }
                    }
                }
                else if ((direction === -1) && currentValue > targetNumericValue)
                {
                    //if within the pixelsToMove of the target, then go straight to the value
                    if ((currentValue - targetNumericValue) <= pixelsToMove)
                    {
                        if (operation["valueFormatter"])
                        {
                            newValue = operation["valueFormatter"](propertyPrefix + targetNumericValue + propertySuffix);
                        } else {
                            newValue = propertyPrefix + targetNumericValue + propertySuffix;
                        }
                        if (isCss)
                        {
                            if (operation["isHexString"])
                            {
                                elem.SetCss(property, "#" + get_rgb(Math.floor(targetNumericValue)) + propertySuffix);
                            } else if (operation["valueType"] == "rgb") {
                                operation["currentColorPerc"] = 100;
                                var currentColor =
                                    operation["colorTransition"].GetColorByProgress(operation["currentColorPerc"]);
                                elem.SetCss(property, currentColor.GetHex());
                            } else {
                                elem.SetCss(property, newValue);
                                operation["currentValue"] = targetNumericValue;
                            }
                        } else {
                            if (property == "text")
                            {
                                elem.SetText(newValue);
                            } else {
                                elem[property] = newValue;
                            }
                            operation["currentValue"] = targetNumericValue;
                        }

                        animationComplete = true;
                    } else {
                        if (operation["valueFormatter"])
                        {
                            newValue = operation["valueFormatter"](propertyPrefix + (currentValue - pixelsToMove) + propertySuffix);
                        } else {
                            newValue = propertyPrefix + (currentValue - pixelsToMove) + propertySuffix;
                        }
                        if (isCss)
                        {
                            if (operation["isHexString"])
                            {
                                elem.SetCss(property, "#" + DblEj.UI.Effects._get_rgb(Math.floor(currentValue - pixelsToMove)) + propertySuffix);
                            } else if (operation["valueType"] == "rgb") {
                                operation["currentColorPerc"] = Math.floor(currentValue - pixelsToMove);
                                var currentColor =
                                    operation["colorTransition"].GetColorByProgress(operation["currentColorPerc"]);
                                elem.SetCss(property, currentColor.GetHex());
                            } else {
                                elem.SetCss(property, newValue);
                                operation["currentValue"] = currentValue - pixelsToMove;
                            }
                        } else {
                            if (property == "text")
                            {
                                elem.SetText(newValue);
                            } else {
                                elem[property] = newValue;
                            }
                            operation["currentValue"] = currentValue - pixelsToMove;
                        }
                    }
                } else {
                    animationComplete = true;
                }

                if (animationComplete)
                {
                    var newValue;
                    if (operation["valueFormatter"])
                    {
                        newValue = operation["valueFormatter"](propertyPrefix + targetNumericValue + propertySuffix);
                    } else {
                        newValue = propertyPrefix + targetNumericValue + propertySuffix;
                    }
                    //Tween complete.
                    if (isCss)
                    {
                        if (operation["isHexString"])
                        {
                            elem.SetCss(property, "#" + DblEj.UI.Effects._get_rgb(Math.ceil(targetNumericValue)) + propertySuffix);
                        } else if (operation["valueType"] == "rgb") {
                            operation["currentColorPerc"] = 100;
                            var currentColor =
                                operation["colorTransition"].GetColorByProgress(operation["currentColorPerc"]);
                            elem.SetCss(property, currentColor.GetHex());
                        } else {
                            elem.SetCss(property, newValue);
                            operation["currentValue"] = targetNumericValue;
                        }
                    } else {
                        if (property == "text")
                        {
                            elem.SetText(newValue);
                        } else {
                            elem[property] = newValue;
                        }

                        operation["currentValue"] = targetNumericValue;
                    }
                    if (IsDefined(operation["callback"]) && operation["callback"] != null)
                    {
                        operation["callback"](operation);
                    }
                    DblEj.UI.Effects._tweenOperations[operationIdx] = null;
                    DblEj.UI.Effects._tweenOperations.splice(operationIdx, 1);

                    if (!IsDefined(DblEj.UI.Effects._animationFinishCount))
                    {
                        DblEj.UI.Effects._animationFinishCount = 0;
                    }
                    DblEj.UI.Effects._animationFinishCount++;
                }
            }
        } else {
            delete DblEj.UI.Effects._tweenOperations[operationIdx];
        }
    }

    DblEj.UI.Effects._animationLastTick = currentTime;
    DblEj.UI.Effects._animationTickCount++;
};

DblEj.UI.Effects._get_rgb = function (dec)
{
    var STR_PAD_LEFT = 1;
    var STR_PAD_RIGHT = 2;
    var STR_PAD_BOTH = 3;
    function pad(str, len, pad, dir) {
        if (!IsDefined(len)) {
            len =
                0;
        }
        if (!IsDefined(pad)) {
            pad =
                ' ';
        }
        if (!IsDefined(dir)) {
            dir =
                STR_PAD_RIGHT;
        }

        if (len + 1 >= str.length) {

            switch (dir) {

                case STR_PAD_LEFT:
                    str = Array(len + 1 - str.length)
                        .join(pad) +
                        str;
                    break;

                case STR_PAD_BOTH:
                    var padlen = len - str.length;
                    var right = Math.ceil(padlen / 2);
                    var left = padlen - right;
                    str = Array(left + 1)
                        .join(pad) +
                        str +
                        Array(right +
                            1)
                        .join(pad);
                    break;

                default:
                    str = str + Array(len + 1 - str.length)
                        .join(pad);
                    break;
            }
        }
        return str;
    }

    var red;
    var green;
    var blue;

    red = (dec >> 16) & 255;
    green = (dec >> 8) & 255;
    blue = dec & 255;

    red = pad(red.toString(16), 2, "0", STR_PAD_LEFT);
    green = pad(green.toString(16), 2, "0", STR_PAD_LEFT);
    blue = pad(blue.toString(16), 2, "0", STR_PAD_LEFT);

    return red + green + blue;
}

Start
(
    function()
    {
        //start animation ticker
        DblEj.UI.Effects._animationTicker = setInterval(DblEj.UI.Effects._processAnimations, Math.ceil(DblEj.UI.Effects._animateInterval));
    }, true
);