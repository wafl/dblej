<?php

namespace DblEj\UI;

/**
 * Represents a CSS stylesheet.
 */
class Stylesheet
{
    private $_skinName;
    private $_fileName;
    private $_preprocess;
    private $_isFullPath;

    /**
     *
     * @param string $skinName
     * @param string $filename
     * @param boolean $preprocess
     * @param boolean $isFullPath
     */
    public function __construct($skinName, $filename, $preprocess = false, $isFullPath = true)
    {
        $this->_skinName   = $skinName;
        $this->_fileName   = $filename;
        $this->_preprocess = $preprocess;
        $this->_isFullPath = $isFullPath;
    }

    /**
     *
     * @return string
     */
    public function Get_SkinName()
    {
        return $this->_skinName;
    }

    /**
     *
     * @return string
     */
    public function Get_Filename()
    {
        return $this->_fileName;
    }

    /**
     *
     * @return boolean
     */
    public function Get_IsFullPath()
    {
        return $this->_isFullPath;
    }

    /**
     *
     * @param string $fileName
     */
    public function Set_Filename($fileName)
    {
        $this->_fileName = $fileName;
    }

    /**
     *
     * @param string $skinName
     */
    public function Set_SkinName($skinName)
    {
        $this->_skinName = $skinName;
    }

    /**
     *
     * @return boolean
     */
    public function Get_Preprocess()
    {
        return $this->_preprocess;
    }

    /**
     *
     * @return string
     */
    public function GetUniqueId()
    {
        return $this->_skinName . "-" . pathinfo($this->_fileName, PATHINFO_FILENAME);
    }
}