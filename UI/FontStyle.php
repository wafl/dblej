<?php

namespace DblEj\UI;

/**
 * Contains information about the size, weight, and color of a CSS font.
 */
class FontStyle
{
    private $_size;
    private $_weight;
    private $_color;
    private $_styling;

    /**
     *
     * @param decimal $size
     * @param string $weight
     * @param string $color
     */
    public function __construct($size, $weight, $color, $styling = "normal")
    {
        $this->_size   = $size;
        $this->_weight = $weight;
        $this->_color  = $color;
        $this->_styling = $styling;
    }

    /**
     *
     * @return decimal
     */
    public function Get_Size()
    {
        return $this->_size;
    }

    /**
     *
     * @return string
     */
    public function Get_Weight()
    {
        return $this->_weight;
    }

    /**
     *
     * @return string
     */
    public function Get_Color()
    {
        return $this->_color;
    }

    public function Get_Styling()
    {
        return $this->_styling;
    }
}