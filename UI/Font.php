<?php

namespace DblEj\UI;

/**
 * Represents a CSS font provided by a Provider.
 */
class Font
{
    private $_family;
    private $_fallbackFamily;
    private $_provider;
    private $_providerArgString;

    /**
     *
     * @param string $family
     * @param string $fallbackFamily
     * @param \DblEj\UI\IFontProvider $provider
     * @param string $providerArgumentString
     */
    public function __construct($family, $fallbackFamily = "serif", \DblEj\Presentation\Integration\IFontProviderExtension $provider = null, $providerArgumentString = "")
    {
        $this->_family            = $family;
        $this->_fallbackFamily    = $fallbackFamily;
        $this->_provider          = $provider;
        $this->_providerArgString = $providerArgumentString;
    }

    /**
     * @return string
     */
    public function Get_Family()
    {
        return $this->_family;
    }

    /**
     * @return string
     */
    public function Get_FallbackFamily()
    {
        return $this->_fallbackFamily;
    }

    /**
     * @return \DblEj\UI\IFontProvider
     */
    public function Get_Provider()
    {
        return $this->_provider;
    }

    public function Get_ProviderArgString()
    {
        return $this->_providerArgString;
    }

    /**
     * @return string
     */
    public function GetStylesheetUrl()
    {
        if ($this->_provider)
        {
            $url = $this->_provider->Get_StylesheetBaseUrl();
            $url = str_replace("%f", urlencode($this->_family), $url);
            $url = str_replace("%a", urlencode($this->_providerArgString), $url);
        } else {
            $url = null;
        }
        return $url;
    }

    /**
     * @param array $fontArray
     * @return \DblEj\UI\Font
     */
    public static function CreateFontFromArray($fontArray)
    {
        $providerClass = isset($fontArray["Provider"]) ? "\\DblEj\\UI\\" . $fontArray["Provider"] : null;
        return new Font($fontArray["Family"], $fontArray["Fallback"], $providerClass ? new $providerClass() : null);
    }
}