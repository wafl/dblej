<?php

namespace DblEj\UI;

class StyledFont
extends Font
{
    private $_style;

    /**
     *
     * @param string $family
     * @param float $size
     * @param string $weight
     * @param string $color
     * @param string $fallbackFamily
     * @param \DblEj\UI\IFontProvider $provider
     * @param string $providerArgumentString
     */
    public function __construct($family = "serif", $size = "16px", $weight = "normal", $color = "#000", $fallbackFamily = "serif", \DblEj\Presentation\Integration\IFontProviderExtension $provider = null, $providerArgumentString = "", $styling = "normal")
    {
        parent::__construct($family, $fallbackFamily, $provider, $providerArgumentString);
        $this->_style = new FontStyle($size, $weight, $color, $styling);
    }

    /**
     *
     * @return FontStyle
     */
    public function Get_Style()
    {
        return $this->_style;
    }

    /**
     *
     * @param array $fontArray
     * @param string $color
     * @return \DblEj\UI\StyledFont
     */
    public static function CreateFontFromArray($fontArray, $color = "#000")
    {
        $fontArray["BaseSize"]               = isset($fontArray["BaseSize"]) ? $fontArray["BaseSize"] : "100%";
        $fontArray["Weight"]                 = isset($fontArray["Weight"]) ? $fontArray["Weight"] : "normal";
        $fontArray["Style"]                 = isset($fontArray["Style"]) ? $fontArray["Style"] : "normal";
        $fontArray["Fallback"]               = isset($fontArray["Fallback"]) ? $fontArray["Fallback"] : "sans-serif";
        $fontArray["ProviderArgumentString"] = isset($fontArray["ProviderArgumentString"]) ? $fontArray["ProviderArgumentString"] : "";

        $providerClass = isset($fontArray["Provider"]) ? $fontArray["Provider"] : null;
        return new StyledFont($fontArray["Family"], $fontArray["BaseSize"], $fontArray["Weight"], $color, $fontArray["Fallback"], $providerClass ? new $providerClass() : null, $fontArray["ProviderArgumentString"], $fontArray["Style"]);
    }
}