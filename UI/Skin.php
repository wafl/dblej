<?php

namespace DblEj\UI;

class Skin
{
    private $_title;
    private $_baseFilename;
    private $_mainFont;
    private $_headingFont;
    private $_subFont;
    private $_accentFont;
    private $_verticalElementSeparation;
    private $_rowSeparation;
    private $_columnSeparation;
    private $_contentBackgroundColor;
    private $_actionColor;
    private $_activeActionColor;
    private $_annotationColor;
    private $_insetColor;
    private $_preprocessCss = false;

    private $_phoneWidth = 480;
    private $_tabletWidth = 800;
    private $_monitorWidth = 1000;
    private $_hdMonitorWidth = 1400;

    /**
     *
     * @param string $title
     * @param string $baseFilename
     * @param \DblEj\UI\StyledFont $mainFont
     * @param integer $verticalElementSeparation
     * @param integer $rowSeparation
     * @param integer $columnSeparation
     * @param string $actionColor
     * @param string $activeActionColor
     * @param string $annotationColor
     * @param string $insetColor
     * @param \DblEj\UI\StyledFont $headingFont
     * @param \DblEj\UI\StyledFont $subFont
     */
    public function __construct($title, $baseFilename, StyledFont $mainFont, $verticalElementSeparation = "9px", $rowSeparation = "9px", $columnSeparation = "15px", $actionColor = "#3366aa", $activeActionColor = "#225599", $annotationColor = "#444444", $insetColor = "#D9EDF7", StyledFont $headingFont = null, StyledFont $subFont = null, StyledFont $accentFont = null, $phoneWidth = 480, $tabletWidth = 800, $monitorWidth = 1000, $hdMonitorWidth = 1400)
    {
        $this->_title                     = $title;
        $this->_baseFilename              = $baseFilename;
        $this->_mainFont                  = $mainFont;
        $this->_headingFont               = $headingFont;
        $this->_subFont                   = $subFont;
        $this->_accentFont                = $accentFont;
        $this->_verticalElementSeparation = $verticalElementSeparation;
        $this->_rowSeparation             = $rowSeparation;
        $this->_columnSeparation          = $columnSeparation;
        $this->_contentBackgroundColor    = "#ffffff"; //@todo dont hardcode
        $this->_actionColor               = $actionColor;
        $this->_activeActionColor         = $activeActionColor;
        $this->_annotationColor           = $annotationColor;
        $this->_insetColor                = $insetColor;
        $this->_phoneWidth                = $phoneWidth;
        $this->_tabletWidth               = $tabletWidth;
        $this->_monitorWidth              = $monitorWidth;
        $this->_hdMonitorWidth            = $hdMonitorWidth;
    }

    /**
     *
     * @param string $title
     * @param array $skinArray
     * @return \DblEj\UI\Skin
     */
    public static function CreateSkinFromArray($title, $skinArray)
    {
        $mainFont    = StyledFont::CreateFontFromArray($skinArray["Fonts"]["BaseFont"], $skinArray["Colors"]["Base"]);
        $headingFont = null;
        $subFont     = null;
        $accentFont     = null;
        $phoneWidth = 480;
        $tabletWidth = 800;
        $monitorWidth = 1000;
        $hdMonitorWidth = 1400;

        if (isset($skinArray["Fonts"]["HeadingFont"]))
        {
            $headingFont = StyledFont::CreateFontFromArray($skinArray["Fonts"]["HeadingFont"], $skinArray["Colors"]["Heading"]);
        }
        if (isset($skinArray["Fonts"]["SubFont"]))
        {
            $subFont = StyledFont::CreateFontFromArray($skinArray["Fonts"]["SubFont"], $skinArray["Colors"]["Sub"]);
        }
        if (isset($skinArray["Fonts"]["AccentFont"]))
        {
            $accentFont = StyledFont::CreateFontFromArray($skinArray["Fonts"]["AccentFont"], $skinArray["Colors"]["Accent"]);
        }
        if (isset($skinArray["MediaBreakpoints"]))
        {
            if (isset($skinArray["MediaBreakpoints"]["Phone"]))
            {
                $phoneWidth = $skinArray["MediaBreakpoints"]["Phone"];
            }
            if (isset($skinArray["MediaBreakpoints"]["Tablet"]))
            {
                $tabletWidth = $skinArray["MediaBreakpoints"]["Tablet"];
            }
            if (isset($skinArray["MediaBreakpoints"]["Monitor"]))
            {
                $monitorWidth = $skinArray["MediaBreakpoints"]["Monitor"];
            }
            if (isset($skinArray["MediaBreakpoints"]["HdMonitor"]))
            {
                $hdMonitorWidth = $skinArray["MediaBreakpoints"]["HdMonitor"];
            }
        }

        return new Skin($title, $skinArray["Path"], $mainFont, $skinArray["Layout"]["VerticalElementSeparation"], $skinArray["Layout"]["RowSeparation"], $skinArray["Layout"]["ColumnSeparation"], $skinArray["Colors"]["Action"], $skinArray["Colors"]["ActiveAction"], $skinArray["Colors"]["Annotation"], $skinArray["Colors"]["Inset"], $headingFont, $subFont, $accentFont, $phoneWidth, $tabletWidth, $monitorWidth, $hdMonitorWidth);
    }

    /**
     *
     * @return string
     */
    public function Get_ContentBackgroundColor()
    {
        return $this->_contentBackgroundColor;
    }

    /**
     *
     * @return string
     */
    public function Get_BaseFilename()
    {
        return $this->_baseFilename;
    }

    /**
     *
     * @return string
     */
    public function Get_Title()
    {
        return $this->_title;
    }

    /**
     *
     * @return boolean
     */
    public function Get_PreProcessCss()
    {
        return $this->_preprocessCss;
    }

    /**
     *
     * @return \DblEj\UI\StyledFont
     */
    public function Get_MainFont()
    {
        return $this->_mainFont;
    }

    /**
     *
     * @return \DblEj\UI\StyledFont
     */
    public function Get_HeadingFont()
    {
        return $this->_headingFont;
    }

    /**
     *
     * @return \DblEj\UI\StyledFont
     */
    public function Get_SubFont()
    {
        return $this->_subFont;
    }

    /**
     *
     * @return \DblEj\UI\StyledFont
     */
    public function Get_AccentFont()
    {
        return $this->_accentFont;
    }

    /**
     *
     * @return integer
     */
    public function Get_VerticalElementSeparation()
    {
        return $this->_verticalElementSeparation;
    }

    /**
     *
     * @return integer
     */
    public function Get_ColumnSeparation()
    {
        return $this->_columnSeparation;
    }

    /**
     *
     * @return integer
     */
    public function Get_RowSeparation()
    {
        return $this->_rowSeparation;
    }

    /**
     *
     * @return string
     */
    public function Get_ActionColor()
    {
        return $this->_actionColor;
    }

    /**
     *
     * @return string
     */
    public function Get_ActiveActionColor()
    {
        return $this->_activeActionColor;
    }

    /**
     *
     * @return string
     */
    public function Get_AnnotationColor()
    {
        return $this->_annotationColor;
    }

    /**
     *
     * @return string
     */
    public function Get_InsetColor()
    {
        return $this->_insetColor;
    }

    public function Get_PhoneWidth()
    {
        return $this->_phoneWidth;
    }

    public function Get_TabletWidth()
    {
        return $this->_tabletWidth;
    }

    public function Get_MonitorWidth()
    {
        return $this->_monitorWidth;
    }

    public function Get_HdMonitorWidth()
    {
        return $this->_hdMonitorWidth;
    }
}