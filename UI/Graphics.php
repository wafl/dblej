<?php
namespace DblEj\UI;

/**
 * Provides the MakeThumnail method.
 */
class Graphics
{

    /**
     * take a jpeg or a png and create a thumbnail of the same format based on the passed dimensions
     * @param string $srcImagePath The local file path of the original imag file that the thumbnail is being made from (Png or Jpg)
     * @param string $destImagePath The local file path of the destination image (the thumbnail)
     * @param integer $newWidth Thumbnail width in pixels
     * @param integer $newHeight Thumbnail height in pixels
     */
    public static function MakeThumbnail($srcImagePath, $destImagePath, $newWidth, $newHeight)
    {
        $isPng         = false;
        $fileExtension = McFile::GetExtension($srcImagePath);
        if (!strcmp("jpg", $fileExtension) || !strcmp("jpeg", $fileExtension))
        {
            $srcImg = imagecreatefromjpeg($srcImagePath);
        }
        elseif (!strcmp("png", $fileExtension))
        {
            $srcImg = imagecreatefrompng($srcImagePath);
            $isPng  = true;
        }
        else
        {
            throw new \Exception("Invalid file extension ($fileExtension), must be png or jpg");
        }
        if ($srcImg)
        {
            $srcSizeX = imageSX($srcImg);
            $srcSizeY = imageSY($srcImg);

            $xRatio = $srcSizeX / $newWidth;
            $yRatio = $srcSizeY / $newHeight;
            if ($xRatio > $yRatio)
            {
                $thumbW = $newWidth;
                $thumbH = $srcSizeY / $xRatio;
            }
            else
            {
                $thumbH = $newHeight;
                $thumbW = $srcSizeX / $yRatio;
            }
            $destImg = ImageCreateTrueColor($thumbW, $thumbH);
            if ($isPng)
            {
                imagecolortransparent($destImg, imagecolorallocate($destImg, 0, 0, 0));
                imagealphablending($destImg, false);
                imagesavealpha($destImg, true);
            }
            imagecopyresampled($destImg, $srcImg, 0, 0, 0, 0, $thumbW, $thumbH, $srcSizeX, $srcSizeY);

            $thumbSaveOk = false;
            if (file_exists($destImagePath))
            {
                unlink($destImagePath);
            }
            if (strcasecmp("png", $fileExtension) == 0)
            {
                $thumbSaveOk = imagepng($destImg, $destImagePath);
            }
            else
            {
                $thumbSaveOk = imagejpeg($destImg, $destImagePath);
            }
            if (!$thumbSaveOk)
            {
                throw new \Exception("Error creating thumbnail");
            }
            //destroys source and destination images.
            imagedestroy($destImg);
            imagedestroy($srcImg);
        }
        else
        {
            throw new \Exception("Error while trying to create thumbnail");
        }
    }

    public static function ConvertJpgToProgressive($jpegFilepath)
    {
        $image = imagecreatefromjpeg($jpegFilepath);
        imageinterlace($image, 1);
        imagejpeg($image, $jpegFilepath, 100);
    }
}