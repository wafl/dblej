/* adapted from http://labs.adamluptak.com/media/labs/downloads/Color.js */
/* Code for converting from RGB to CIE-LCh adapted from Stuart Denman, http://www.stuartdenman.com/improved-color-blending/ */

/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/**
 * @class Color Stores information about a color and provides methods for adjusting the color and converting between color modes.
 * @extends Class
 */
DblEj.UI.Color = Class.extend({
    init: function (r, g, b, mode)
    {
        this.rgbDirty = true;
        this.hsvDirty = true;
        this.cielchDirty = true;

        this.red = 0;
        this.green = 0;
        this.blue = 0;

        this.hue = 0;
        this.saturation = 0;
        this.value = 0;

        this.xyz_x = 0;
        this.xyz_y = 0;
        this.xyz_z = 0;

        this.cielab_l = 0;
        this.cielab_a = 0;
        this.cielab_b = 0;

        this.cielch_l = 0;
        this.cielch_c = 0;
        this.cielch_h = 0;

        this.convertHSVtoRGB = function () {
            var h = this.hue;
            var s = this.saturation;
            var v = this.value;

            var r, g, b;
            var f, p, q, t;
            var i;

            s /= 100;
            v /= 100;

            if (s == 0) {
                r = g = b = v;
            }
            else {
                h /= 60; // sector 0 to 5
                i = Math.floor(h);
                f = h - i; // factorial part of h
                p = v * (1 - s);
                q = v * (1 - s * f);
                t = v * (1 - s * (1 - f));

                switch (i) {
                    case 0:
                        r = v;
                        g = t;
                        b = p;
                        break;

                    case 1:
                        r = q;
                        g = v;
                        b = p;
                        break;

                    case 2:
                        r = p;
                        g = v;
                        b = t;
                        break;

                    case 3:
                        r = p;
                        g = q;
                        b = v;
                        break;

                    case 4:
                        r = t;
                        g = p;
                        b = v;
                        break;

                    default:
                        r = v;
                        g = p;
                        b = q;
                        break;
                }
            }

            this.red = this.bound((r * 255), 0, 255);
            this.green = this.bound((g * 255), 0, 255);
            this.blue = this.bound((b * 255), 0, 255);
        };

        this.convertRGBtoHSV = function () {
            var r = this.red;
            var g = this.green;
            var b = this.blue;

            var min = Math.min(r, g, b);
            var max = Math.max(r, g, b);
            var delta = max - min;

            var h = max;
            var s = max;
            var v = max;

            v = max / 255 * 100;

            if (max != 0) {
                s = delta / max * 100;

                if (r == max)
                    h =
                        (g -
                            b) /
                        delta;
                else if (g == max)
                    h =
                        2 +
                        (b -
                            r) /
                        delta;
                else
                    h =
                        4 +
                        (r -
                            g) /
                        delta;

                h = h * 60;
                if (h < 0)
                    h +=
                        360;
            }

            this.hue = h;
            this.saturation = s;
            this.value = v;
        };

        this.convertRBGtoXYZ = function () {
            var tmp_r = this.red / 255;
            var tmp_g = this.green / 255;
            var tmp_b = this.blue / 255;
            if (tmp_r > 0.04045) {
                tmp_r = Math.pow(((tmp_r + 0.055) / 1.055), 2.4);
            } else {
                tmp_r = tmp_r / 12.92;
            }
            if (tmp_g > 0.04045) {
                tmp_g = Math.pow(((tmp_g + 0.055) / 1.055), 2.4);
            } else {
                tmp_g = tmp_g / 12.92;
            }
            if (tmp_b > 0.04045) {
                tmp_b = Math.pow(((tmp_b + 0.055) / 1.055), 2.4);
            } else {
                tmp_b = tmp_b / 12.92;
            }
            tmp_r = tmp_r * 100;
            tmp_g = tmp_g * 100;
            tmp_b = tmp_b * 100;
            var x = tmp_r * 0.4124 + tmp_g * 0.3576 + tmp_b * 0.1805;
            var y = tmp_r * 0.2126 + tmp_g * 0.7152 + tmp_b * 0.0722;
            var z = tmp_r * 0.0193 + tmp_g * 0.1192 + tmp_b * 0.9505;

            this.xyz_x = x;
            this.xyz_y = y;
            this.xyz_z = z;
        };

        this.convertXYZtoCIELab = function () {
            var Xn = 95.047;
            var Yn = 100.000;
            var Zn = 108.883;

            var x = this.xyz_x / Xn;
            var y = this.xyz_y / Yn;
            var z = this.xyz_z / Zn;

            if (x > 0.008856) {
                x = Math.pow(x, 1 / 3);
            } else {
                x = (7.787 * x) + (16 / 116);
            }
            if (y > 0.008856) {
                y = Math.pow(y, 1 / 3);
            } else {
                y = (7.787 * y) + (16 / 116);
            }
            if (z > 0.008856) {
                z = Math.pow(z, 1 / 3);
            } else {
                z = (7.787 * z) + (16 / 116);
            }

            if (y > 0.008856) {
                var l = (116 * y) - 16;
            } else {
                var l = 903.3 * y;
            }
            var a = 500 * (x - y);
            var b = 200 * (y - z);

            this.cielab_l = l;
            this.cielab_a = a;
            this.cielab_b = b;
        };

        this.convertCIELabToCielCh = function () {
            var var_H = Math.atan2(this.cielab_b, this.cielab_a);

            if (var_H > 0) {
                var_H = (var_H / Math.PI) * 180;
            } else {
                var_H = 360 - (Math.abs(var_H) / Math.PI) * 180;
            }

            this.cielch_l = this.cielab_l;
            this.cielch_c = Math.sqrt(Math.pow(this.cielab_a, 2) + Math.pow(this.cielab_b, 2));

            this.cielch_h = var_H < 360 ? var_H : (var_H - 360);
        };

        this.convertCielChToCIELab = function () {
            var l = this.cielch_l;
            var hradi = this.cielch_h * (Math.PI / 180);
            var a = Math.cos(hradi) * this.cielch_c;
            var b = Math.sin(hradi) * this.cielch_c;

            this.cielab_l = l;
            this.cielab_a = a;
            this.cielab_b = b;
        };

        this.convertCIELabToXYZ = function () {
            var ref_X = 95.047;
            var ref_Y = 100.000;
            var ref_Z = 108.883;

            var var_Y = (this.cielab_l + 16) / 116;
            var var_X = this.cielab_a / 500 + var_Y;
            var var_Z = var_Y - this.cielab_b / 200;

            if (Math.pow(var_Y, 3) > 0.008856) {
                var_Y = Math.pow(var_Y, 3);
            } else {
                var_Y = (var_Y - 16 / 116) / 7.787;
            }
            if (Math.pow(var_X, 3) > 0.008856) {
                var_X = Math.pow(var_X, 3);
            } else {
                var_X = (var_X - 16 / 116) / 7.787;
            }
            if (Math.pow(var_Z, 3) > 0.008856) {
                var_Z = Math.pow(var_Z, 3);
            } else {
                var_Z = (var_Z - 16 / 116) / 7.787;
            }

            this.xyz_x = ref_X * var_X;
            this.xyz_y = ref_Y * var_Y;
            this.xyz_z = ref_Z * var_Z;
        };

        this.convertXYZtoRGB = function () {
            var var_X = this.xyz_x / 100;
            var var_Y = this.xyz_y / 100;
            var var_Z = this.xyz_z / 100;

            var var_R = var_X * 3.2406 + var_Y * -1.5372 + var_Z * -0.4986;
            var var_G = var_X * -0.9689 + var_Y * 1.8758 + var_Z * 0.0415;
            var var_B = var_X * 0.0557 + var_Y * -0.2040 + var_Z * 1.0570;

            if (var_R > 0.0031308) {
                var_R = 1.055 * Math.pow(var_R, (1 / 2.4)) - 0.055;
            } else {
                var_R = 12.92 * var_R;
            }
            if (var_G > 0.0031308) {
                var_G = 1.055 * Math.pow(var_G, (1 / 2.4)) - 0.055;
            } else {
                var_G = 12.92 * var_G;
            }
            if (var_B > 0.0031308) {
                var_B = 1.055 * Math.pow(var_B, (1 / 2.4)) - 0.055;
            } else {
                var_B = 12.92 * var_B;
            }

            this.red = this.bound((var_R * 255), 0, 255);
            this.green = this.bound((var_G * 255), 0, 255);
            this.blue = this.bound((var_B * 255), 0, 255);
        };

        this.convertRGBtoCielCh = function () {
            this.convertRBGtoXYZ();
            this.convertXYZtoCIELab();
            this.convertCIELabToCielCh();
        };

        this.convertCielChToRGB = function () {
            this.convertCielChToCIELab();
            this.convertCIELabToXYZ();
            this.convertXYZtoRGB();
        };

        this.cleanForRGB = function () {
            if (!this.rgbDirty)
                return;

            if (!this.hsvDirty) {
                this.convertHSVtoRGB();
            } else if (!this.cielchDirty) {
                this.convertCielChToRGB();
            }

            this.rgbDirty = false;
        };

        this.cleanForHSV = function () {
            if (!this.hsvDirty)
                return;

            if (!this.rgbDirty) {
                this.convertRGBtoHSV();
            } else if (!this.cielchDirty) {
                this.convertCielChToRGB();
                this.convertRGBtoHSV();
            }

            this.hsvDirty = false;
        };

        this.cleanForCielCh = function () {
            if (!this.cielchDirty)
                return;

            if (!this.hsvDirty) {
                this.convertHSVtoRGB();
                this.convertRGBtoCielCh();
            } else if (!this.rgbDirty) {
                this.convertRGBtoCielCh();
            }

            this.cielchDirty = false;
        };

        this.rgbModified = function () {
            this.hsvDirty = true;
            this.cielchDirty = true;
        };

        this.hsvModified = function () {
            this.rgbDirty = true;
            this.cielchDirty = true;
        };

        this.cielchModified = function () {
            this.rgbDirty = true;
            this.hsvDirty = true;
        };

        this.bound = function (v, l, h) {
            return Math.min(h, Math.max(l, v));
        };

        if (!mode) {
            mode = DblEj.UI.Color.ColorMode.RGB;
        }

        if (mode == DblEj.UI.Color.ColorMode.RGB) {
            if (r)
                this.red =
                    this.bound(r, 0, 255);
            if (g)
                this.green =
                    this.bound(g, 0, 255);
            if (b)
                this.blue =
                    this.bound(b, 0, 255);
            this.rgbDirty = false;
        }
        else if (mode == DblEj.UI.Color.ColorMode.HSV) {
            if (r)
                this.hue =
                    this.bound(r, 0, 360);
            if (g)
                this.saturation =
                    this.bound(g, 0, 100);
            if (b)
                this.value =
                    this.bound(b, 0, 100);
            this.hsvDirty = false;
        }
        else if (mode == DblEj.UI.Color.ColorMode.CielCh) {
            if (r)
                this.cielch_l =
                    r;
            if (g)
                this.cielch_c =
                    g;
            if (b)
                this.cielch_h =
                    b <
                    360 ? b : (b -
                        360);
            this.cielchDirty = false;
        }

    },
    /**
     * Get a hexadecimal string representation of this color. (example: #336699)
     * @returns {String}
     */
    GetHex: function () {
        this.cleanForRGB();

        var tempRed = Math.round(this.red);
        var tempGreen = Math.round(this.green);
        var tempBlue = Math.round(this.blue);

        var hex = (tempRed << 16) + (tempGreen << 8) + tempBlue;
        var hexString = hex.toString(16);
        while (hexString.length < 6)
            hexString =
                "0" +
                hexString;
        return "#" + hexString;
    },
    /**
     * @function GetRed
     * Get the red value of this color.
     * @returns {Number}
     */
    GetRed: function () {
        this.cleanForRGB();
        return this.red;
    },
    /**
     * @function SetRed
     * Set the red value of this color.
     * @param {Number} r The red value.
     */
    SetRed: function (r) {
        this.cleanForRGB();
        this.red = this.bound(r, 0, 255);
        this.rgbModified();
    },
    /**
     * @function GetGreen
     * Get the green value of this color.
     * @returns {Number}
     */
    GetGreen: function () {
        this.cleanForRGB();
        return this.green;
    },
    /**
     * @function SetGreen
     * Set the green value of this color.
     * @param {Number} g The green value.
     */
    SetGreen: function (g) {
        this.cleanForRGB();
        this.green = this.bound(g, 0, 255);
        this.rgbModified();
    },
    /**
     * @function GetBlue
     * Get the blue value of this color.
     * @returns {Number}
     */
    GetBlue: function () {
        this.cleanForRGB();
        return this.blue;
    },
    /**
     * @function SetBlue
     * Set the blue value of this color.
     * @param {Number} b The blue value.
     */
    SetBlue: function (b) {
        this.cleanForRGB();
        this.blue = this.bound(b, 0, 255);
        this.rgbModified();
    },
    /**
     * @function GetHue
     * Get the hue value of this color.
     * @returns {Number}
     */
    GetHue: function () {
        this.cleanForHSV();
        return this.hue;
    },
    /**
     * Set the hue value of this color.
     * @param {Number} h The hue value.
     */
    SetHue: function (h) {
        this.cleanForHSV();
        this.hue = this.bound(h, 0, 360);
        this.hsvModified();
    },
    /**
     * @function GetSaturation
     * Get the saturation value of this color.
     * @returns {Number}
     */
    GetSaturation: function () {
        this.cleanForHSV();
        return this.saturation;
    },
    /**
     * @function SetSaturation
     * Set the saturation value of this color.
     * @param {Number} s The saturation value.
     */
    SetSaturation: function (s) {
        this.cleanForHSV();
        this.saturation = this.bound(s, 0, 100);
        this.hsvModified();
    },
    /**
     * @function GetValue
     * Get the decimal value of this color.
     * @returns {Number}
     */
    GetValue: function () {
        this.cleanForHSV();
        return this.value;
    },
    /**
     * @function SetValue
     * Set the decimal value of this color.
     * @param {Number} v The value.
     */
    SetValue: function (v) {
        this.cleanForHSV();
        this.value = this.bound(v, 0, 100);
        this.hsvModified();
    },
    /**
     * @function GetCielCh_L
     * Get the CielCh L value of this color.
     * @returns {Number}
     */
    GetCielCh_L: function () {
        this.cleanForCielCh();
        return this.cielch_l;
    },
    /**
     * @function SetCielCh_L
     * Set the CielCh L value of this color.
     * @param {Number} l The L value.
     */
    SetCielCh_L: function (l) {
        this.cleanForCielCh();
        this.cielch_l = this.bound(l, 0, 100);
        this.cielchModified();
    },
    /**
     * @function GetChielCh_C
     * Get the CielCh C value of this color.
     * @returns {Number}
     */
    GetCielCh_C: function () {
        this.cleanForCielCh();
        return this.cielch_c;
    },
    /**
     * @function SetCielCh_C
     * Set the CielCh C value of this color.
     * @param {Number} c The C value.
     */
    SetCielCh_C: function (c) {
        this.cleanForCielCh();
        this.cielch_c = this.bound(c, 0, 100);
        this.cielchModified();
    },
    /**
     * @function GetCielCh_H
     * Get the CielCh H value of this color.
     * @returns {Number}
     */
    GetCielCh_H: function () {
        this.cleanForCielCh();
        return this.cielch_h;
    },
    /**
     * @function SetCielCh_H
     * Set the CielCh H value of this color.
     * @param {Number} h The H value.
     */
    SetCielCh_H: function (h) {
        this.cleanForCielCh();
        this.cielch_h = h < 360 ? h : (h - 360);
        this.cielchModified();
    }
});

/**
 * Constants for the available color modes.
 */
DblEj.UI.Color.ColorMode = {
    RGB: 0,
    HSV: 1,
    CielCh: 2
};

DblEj.UI.Color.GetRandomColor = function(minR, minG, minB, maxR, maxG, maxB)
{
    if (!IsDefined(minR)) { minR = 0; }
    if (!IsDefined(minG)) { minG = 0; }
    if (!IsDefined(minB)) { minB = 0; }
    if (!IsDefined(maxR)) { maxR = 255; }
    if (!IsDefined(maxG)) { maxG = 255; }
    if (!IsDefined(maxB)) { maxB = 255; }

    var r = Math.floor((Math.random() * 10) + 1);
    var g = Math.floor((Math.random() * 10) + 1);
    var b = Math.floor((Math.random() * 10) + 1);

    if (r > minR) { r = minR; }
    if (g > minG) { r = minG; }
    if (b > minB) { b = minB; }
    if (r < maxR) { r = maxR; }
    if (g < maxG) { r = maxG; }
    if (b < maxB) { b = maxB; }

    return new DblEj.UI.Color(r, g, b);
};