<?php
Namespace DblEj\UI;

/**
 * Two dimensional coordinate.  Sometimes used for other two dimensional measurements, like size.
 */
class Point
{
    private $_x;
    private $_y;

    /**
     *
     * @param integer $x
     * @param integer $y
     */
    public function __construct($x, $y)
    {
        $this->_x = $x;
        $this->_y = $y;
    }

    /**
     *
     * @return decimal
     */
    public function Get_X()
    {
        return $this->_x;
    }

    /**
     *
     * @return decimal
     */
    public function Get_Y()
    {
        return $this->_y;
    }

    /**
     *
     * @param decimal $newX
     */
    public function Set_X($newX)
    {
        $this->_x = $newX;
    }

    /**
     *
     * @param decimal $newY
     */
    public function Set_Y($newY)
    {
        $this->_y = $newY;
    }

    /**
     * Create a new point based on an existing point.
     *
     * @param \DblEj\UI\Point $pointObj
     * @return \DblEj\UI\Point
     */
    public static function FromPoint(Point $pointObj)
    {
        $newPoint = new Point($pointObj->Get_X(), $pointObj->Get_Y());
        return $newPoint;
    }
}