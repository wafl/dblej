/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Point Two dimensional coordinate.  Sometimes used for other two dimensional measurements, like size.
 * @extends Class
 */
DblEj.UI.Point = Class.extend(
    {
        _x: 0,
        _y: 0,
        /**
         * @constructor init
         * @param {Number} x
         * @param {Number} y
         */
        init: function (x, y)
        {
            this._x = x;
            this._y = y;
        },
        /**
         * @property Get_X
         * @type {Number}
         */
        Get_X: function ()
        {
            return this._x;
        },
        /**
         * @property Get_Y
         * @type {Number}
         */
        Get_Y: function ()
        {
            return this._y;
        },
        /**
         * @property Set_X
         * @type {Number}
         *
         * @param {Number} newX
         */
        Set_X: function (newX)
        {
            this._x = newX;
            return this;
        },
        /**
         * @property Set_Y
         * @type {Number}
         *
         * @param {Number} newY
         */
        Set_Y: function (newY)
        {
            this._y = newY;
            return this;
        },
        /**
         * @method toString
         * @return {String}
         */
        toString: function ()
        {
            return this._x.toString() + "," + this._y.toString();
        }
    });

/**
 * @function FromPoint Create a new instance of a Point object using an existing Point as a prototype
 * @static
 * @param {Point} pointObj
 */
DblEj.UI.Point.FromPoint = function (pointObj)
{
    var newPoint = new DblEj.UI.Point(pointObj.Get_X(), pointObj.Get_Y());
    return newPoint;
};