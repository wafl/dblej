/*
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Utils
 * @static
 */
DblEj.UI.Utils = {};

/**
 * @function SelectAllTextInBox
 * @param {String} textboxId
 * @static
 */
DblEj.UI.Utils.SelectAllTextInBox = function (textboxId)
{
    var textbox = $(textboxId);
    if (textbox)
    {
        textbox.select();
    }
};
/**
 * @function ShowDivAtMouseLocation
 * @param {Element} menuDiv
 * @param {Event} mouseevent
 * @static
 */
DblEj.UI.Utils.ShowDivAtMouseLocation = function (menuDiv, mouseevent)
{
    menuDiv.style.left = DblEj.UI.Mouse.Mouse.Get_Location()
        .Get_X() + "px";
    menuDiv.style.top = DblEj.UI.Mouse.Mouse.Get_Location()
        .Get_Y() + "px";
    menuDiv.style.display = "block";

    try {
        window.getSelection()
            .collapseToStart();  // try to compensate for tendency to treat right-clicking as text selection
    } catch (e) {
    } // do nothing

    if (mouseevent)
    {
        // prevent the event from bubbling up and causing the regular browser context menu to appear.
        DblEj.EventHandling.Events.PreventDefaultEvent(mouseevent);
    }
};

/**
 * @function GetPageSize
 * @static
 *
 * @param {Boolean} useViewportSize
 */
DblEj.UI.Utils.GetPageSize = function (useViewportSize)
{
    var pageWidth;
    var pageHeight;
    if (!IsDefined(useViewportSize) || useViewportSize == null)
    {
        useViewportSize = true;
    }
    if (useViewportSize)
    {
        pageWidth = window.innerWidth != null ? window.innerWidth : document.documentElement &&
            document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body !=
            null ? document.body.clientWidth : null;
        pageHeight = window.innerHeight != null ? window.innerHeight : document.documentElement &&
            document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body !=
            null ? document.body.clientHeight : null;
        if (IsDefined(document.body.scrollWidth) && document.body.scrollWidth < pageWidth)
        {
            pageWidth = document.body.scrollWidth;
        }
        if (IsDefined(document.body.scrollHeight) && document.body.scrollHeight < pageHeight)
        {
            pageHeight = document.body.scrollHeight;
        }
    }
    else if (document.body && document.body.scrollWidth)
    {
        pageWidth = document.body.scrollWidth ? document.body.scrollWidth : null;
        pageHeight = document.body.scrollHeight ? document.body.scrollHeight : null;
    }
    else if (document.body && document.body.clientWidth)
    {
        pageWidth = document.body.clientWidth ? document.body.clientWidth : null;
        pageHeight = document.body.clientHeight ? document.body.clientHeight : null;
    }
    else if (document.documentElement)
    {
        pageWidth = document.documentElement.GetAbsoluteSize().Get_X();
        pageHeight = document.documentElement.GetAbsoluteSize().Get_Y();
    }
    else {
        pageWidth = null;
        pageHeight = null;
    }

    return new DblEj.UI.Point(pageWidth, pageHeight);
};

/**
 * @function SwapDivDisplay
 * @static
 * @param {String} div1id
 * @param {String} div2id
 */
DblEj.UI.Utils.SwapDivDisplay = function (div1id, div2id) {
    var div1 = $(div1id);
    var div2 = $(div2id);
    var div1display = div1.style.display;
    var div2display = div2.style.display;
    div1.style.display = div2display;
    div2.style.display = div1display;
};
/**
 * @function SetGlobalStylesheet
 * @param {String} sheetName
 * @static
 */
DblEj.UI.Utils.SetGlobalStylesheet = function (sheetName)
{
    var headLinkIdx;
    var headLinkElem;
    for (headLinkIdx = 0; (headLinkElem = document.getElementsByTagName("link")[headLinkIdx]); headLinkIdx++)
    {
        if (headLinkElem.getAttribute("rel")
            .indexOf("style") !=
            -1 &&
            headLinkElem.getAttribute("title"))
        {
            headLinkElem.disabled = true;
            if (headLinkElem.getAttribute("title") == sheetName)
            {
                headLinkElem.disabled = false;
            }
        }
    }
    DblEj.Util.WebBrowser.CreateCookie("GlobalStylesheet", sheetName, 365);
};

/**
 * @function GetMaxZIndex
 *
 * @static
 * @param {NodeList|Array} elementsToCompare
 * @param {NodeList|Array} excludeElements
 *
 * @returns {Number}
 */
DblEj.UI.Utils.GetMaxZIndex = function (elementsToCompare, excludeElements)
{
    if (!IsDefined(elementsToCompare) || elementsToCompare === null)
    {
        elementsToCompare = $$q('body header:not([class*="Spans"]):not([class*="Layout"]):not([class*="Row"]), body footer:not([class*="Spans"]):not([class*="Layout"]):not([class*="Row"]), body div[style]:not([class*="Spans"]):not([class*="Layout"]):not([class*="Row"]),body section[style]:not([class*="Spans"]):not([class*="Layout"]):not([class*="Row"]),body div[class]:not([class*="Spans"]):not([class*="Layout"]):not([class*="Row"]),body section[class]:not([class*="Spans"]):not([class*="Layout"]):not([class*="Row"])');
    }
    if (!IsDefined(excludeElements))
    {
        excludeElements = [];
    }
    var maxZIndex = 0;
    if (!IsArray(elementsToCompare))
        elementsToCompare =
            [
                elementsToCompare
            ];

    if (elementsToCompare.OnEach === undefined)
        Iteratize(elementsToCompare);

    elementsToCompare.OnEach(function (el) {
        if (Array.prototype.indexOf.call(excludeElements, el) === -1)
        {
            var zIndex = el.GetCss('z-index');

            if (zIndex === '' || zIndex === undefined)
            {
                zIndex = null;
            }

            if (maxZIndex < zIndex)
            {
                maxZIndex = zIndex;
            }
        }
    });

    return maxZIndex;
};

/**
 * @function ScrollToTop
 *
 * @param {Number} pixelsPerMiunute
 */
DblEj.UI.Utils.ScrollToTop = function (pixelsPerMiunute)
{
    if (!IsDefined(pixelsPerMiunute) || !pixelsPerMiunute)
    {
        pixelsPerMiunute = 145000;
    }
    $$q("html,body")
        .OnEach(
            function (elem)
            {
                elem.AnimateJsProperty("scrollTop", 0, "", pixelsPerMiunute);
            });
};

/**
 * @function MakeFixedElementScrollable
 *
 * @param {Element} fixedElement
 * @param {Element} ceilingElement
 * @param {Element} floorElement
 */
DblEj.UI.Utils.MakeFixedElementScrollable = function (fixedElement, ceilingElement, floorElement)
{
    if (!IsDefined(fixedElement["_innerShow"]) || fixedElement._innerShow == null)
    {
        fixedElement._innerShow = fixedElement.Show;
        fixedElement.Show = function(restoreFromAnimations, displayClass, opacity)
        {
            this._innerShow(restoreFromAnimations, displayClass, opacity);
            autoScrollMenuVertical();
        }.Bind(fixedElement);
        fixedElement.GetParent().SetCss("position", "relative");
        autoScrollMenuVertical();
        fixedElement.RefreshFixedScrollSize = autoScrollMenuVertical;

        fixedElement._scrollHandler = DblEj.EventHandling.Events.AddHandler(window, "scroll",  function (){ autoScrollMenuVertical();}, null);
        fixedElement._resizeHandler = DblEj.EventHandling.Events.AddHandler(window, "resize",  function (){ autoScrollMenuVertical();}, null);
        fixedElement._topBeforeScrollFix = IsDefined(fixedElement.style["top"])?fixedElement.style["top"]:"";

        function autoScrollMenuVertical()
        {
            if (fixedElement.IsShowing())
            {
                var menuTop;
                var menuBottom;
                if (IsDefined(ceilingElement) && ceilingElement != null)
                {
                    menuTop = ceilingElement.GetAbsoluteSize().Get_Y();
                } else {
                    menuTop = 0;
                }
                if (IsDefined(floorElement) && floorElement != null)
                {
                    menuBottom = floorElement.GetAbsoluteSize().Get_Y();
                } else {
                    menuBottom = 0;
                }
                var screenHeight = DblEj.UI.Utils.GetPageSize(true).Get_Y();
                var pageHeight = DblEj.UI.Utils.GetPageSize(false).Get_Y();
                var scrollOffset = window.GetOffsetPoint().Get_Y();
                fixedElement.SetCss("bottom", "auto");
                var menuHeight = fixedElement.GetAbsoluteSize().Get_Y() + menuTop + menuBottom;
                var menuScrollable = menuHeight - screenHeight;
                if (menuHeight > pageHeight)
                {
                    //menu is taller than the available screen size.... so the client
                    //isn't even showing a scroll bar.
                    var menuSizeOffset = menuHeight - screenHeight;
                    var padElement = $create("<div id='FixedScrollHeightExtensionPadding' style='height: " + menuSizeOffset +
                        "px;'>&nbsp;</div>");
                    document.body.appendChild(padElement);
                    pageHeight = DblEj.UI.Utils.GetPageSize(false)
                        .Get_Y();
                    menuScrollable = menuHeight - screenHeight;
                }
                else if ($("FixedScrollHeightExtensionPadding"))
                {
                    var padSize = $("FixedScrollHeightExtensionPadding").GetAbsoluteSize();
                    var unpaddedPageHeight = pageHeight - padSize.Get_Y();
                    if (unpaddedPageHeight > menuHeight)
                    {
                        document.body.removeChild($("FixedScrollHeightExtensionPadding"));
                    }
                    if (parseInt(fixedElement.GetStyle("bottom")) > 0 || isNaN(parseInt(fixedElement.GetStyle("bottom"))) || fixedElement.GetStyle("bottom")=="auto")
                    {
                        fixedElement.SetCss("bottom", "0px");
                    }
                }
                else if (parseInt(fixedElement.GetStyle("bottom")) > 0 || isNaN(parseInt(fixedElement.GetStyle("bottom"))) || fixedElement.GetStyle("bottom")=="auto")
                {
                    fixedElement.SetCss("bottom", "0px");
                }

                if (menuScrollable > 0)
                {
                    if (scrollOffset > menuScrollable)
                    {
                        fixedElement.SetCss("top", (menuTop - menuScrollable) + "px");
                    } else {
                        fixedElement.SetCss("top", (menuTop - scrollOffset) + "px");
                    }
                } else {
                    fixedElement.SetCss("top", menuTop + "px");
                }

                if (fixedElement.GetParent() != null && (fixedElement.GetAbsoluteSize().Get_Y() > fixedElement.GetParent().GetAbsoluteSize().Get_Y()))
                {
                    fixedElement.GetParent().SetCss("height",fixedElement.GetAbsoluteSize().Get_Y()+"px");
                }
            }
        }
    }
};

DblEj.UI.Utils.UndoScrollableFixedElement = function (fixedElement)
{
    if (IsDefined(fixedElement["_innerShow"]) && fixedElement._innerShow != null)
    {
        fixedElement.Show = fixedElement._innerShow;
        fixedElement._innerShow = null;
        fixedElement.RefreshFixedScrollSize = function(){ };
        fixedElement.SetCss("top", fixedElement._topBeforeScrollFix);

        DblEj.EventHandling.Events.RemoveHandler(fixedElement._scrollHandler.Get_Uid());
        DblEj.EventHandling.Events.RemoveHandler(fixedElement._resizeHandler.Get_Uid());
    }
}