Element.prototype._reposition = function ()
{
    var myPosition = this.GetAbsolutePosition();
    var mySize = this.GetAbsoluteSize();
    var tipSize = this._tooltipDiv.GetAbsoluteSize();
    var halfMySize = new DblEj.UI.Point(mySize.Get_X() / 2, mySize.Get_Y() / 2);
    var position;

    if (this._position == "auto" || this._position == "" || this._position == null)
    {
        var pageSize = DblEj.UI.Utils.GetPageSize(false);
        var distanceFromCutOffX = 0;
        var distanceFromCutOffY = 0;
        if ((myPosition.Get_X() + halfMySize.Get_X()) < (pageSize.Get_X() / 2))
        {
            //its on left side of screen
            distanceFromCutOffX = myPosition.Get_X();
            position = "right";
        } else {
            //its on right side of screen
            distanceFromCutOffX = pageSize.Get_X() - myPosition.Get_X();
            position = "left";
        }
        if ((myPosition.Get_Y() + halfMySize.Get_Y()) < (pageSize.Get_Y() / 2))
        {
            //its on top of screen
            distanceFromCutOffY = myPosition.Get_Y();
            if (distanceFromCutOffY > distanceFromCutOffX)
            {
                position = "bottom";
            }
        } else {
            //its on bottom side of screen
            distanceFromCutOffY = pageSize.Get_Y() - myPosition.Get_Y();
            if (distanceFromCutOffY > distanceFromCutOffX)
            {
                position = "top";
            }
        }
    } else {
        position = this._position;
    }

    if (position == "bottom")
    {
        this._tooltipDiv.RemoveClass("RightOf")
            .RemoveClass("Above")
            .RemoveClass("LeftOf")
            .AddClass("Below");
        this._tooltipDiv.SetCssLocation(myPosition.Get_X() + 65 - tipSize.Get_X() + halfMySize.Get_X(), myPosition.Get_Y() +
            mySize.Get_Y() + 6);
    }
    else if (position == "top")
    {
        this._tooltipDiv.RemoveClass("RightOf")
            .RemoveClass("Below")
            .RemoveClass("LeftOf")
            .AddClass("Above");
        this._tooltipDiv.SetCssLocation(myPosition.Get_X() - 65 + halfMySize.Get_X(), myPosition.Get_Y() - tipSize.Get_Y() - 6);
    }
    else if (position == "left")
    {
        this._tooltipDiv.RemoveClass("Below")
            .RemoveClass("Above")
            .RemoveClass("RightOf")
            .AddClass("LeftOf");
        this._tooltipDiv.SetCssLocation(myPosition.Get_X() - tipSize.Get_X() - 12, myPosition.Get_Y() + halfMySize.Get_Y() - 30);
    }
    else if (position == "right")
    {
        this._tooltipDiv.RemoveClass("Below")
            .RemoveClass("Above")
            .RemoveClass("LeftOf")
            .AddClass("RightOf");
        this._tooltipDiv.SetCssLocation(myPosition.Get_X() + mySize.Get_X() + 12, myPosition.Get_Y() + halfMySize.Get_Y() - 30);
    }
};
Element.prototype.SetTooltip = function (html, position, borderColor, delay, trigger)
{
    if (!IsDefined(delay) || delay == null)
    {
        delay = 500;
    }
    if (!IsDefined(this._tooltipDiv))
    {
        this._tooltipDiv = document.createElement("div");
        this._tooltipDiv.AddClass("Tooltip")
            .Hide();
        this._position = position;
        document.body.appendChild(this._tooltipDiv);
        this.title = "";
    }
    this._tooltipDiv.SetInnerHtml(html);
    this._reposition();
    this._mouseInTooltipHandler = null;
    if (trigger == "click")
    {
        this.AddClickHandler(function (event)
        {
            this._mouseInTooltipHandler = setTimeout(function () {
                this._reposition();
                this._tooltipDiv.Show()
            }.Bind(this), delay);
            DblEj.EventHandling.Events.PreventDefaultEvent(event);
            return false;
        });
    }
    else
    {
        this.AddMouseInHandler(function ()
        {
            this._mouseInTooltipHandler = setTimeout(function () {
                this._reposition();
                this._tooltipDiv.Show()
            }.Bind(this), delay);
        });
    }
    this.AddMouseOutHandler(function ()
    {
        if (this._mouseInTooltipHandler != null)
        {
            clearTimeout(this._mouseInTooltipHandler);
        }
        this._tooltipDiv.Hide();
        this._mouseInTooltipHandler = null;
    });
    this._tooltipDiv.SetCss("border-color", borderColor);
};


/**
 * @namespace DblEj.UI
 */
Namespace("DblEj.UI");

/*
 * @class Tooltip
 * @extends Class
 */
DblEj.UI.Tooltip = Class.extend(
    {
        _balloonDiv: document.createElement("div"),
        /**
         * @method Show
         * @param {Number} width
         * @param {Number} height
         * @param {Number} x
         * @param {Number} y
         * @param {String} html
         */
        Show: function (width, height, x, y, html)
        {
            this._balloonDiv.style.position = "absolute";
            document.body.appendChild(this._balloonDiv);
            var currentYOffset = document.body.scrollTop;

            width = width.toString();
            height = height.toString();
            x = x.toString();
            y = y.toString();
            this._balloonDiv.innerHTML = html;

            x = Number(x);
            y = Number(y);
            if (IE > 0) {
                y = y + currentYOffset;
            }

            //we want to generally popup from the mouseloc
            y = y - height - 5;
            x = x + 5;

            this._balloonDiv.style.left = x + "px";
            this._balloonDiv.style.top = y + "px";

            this._balloonDiv.style.width = width + "px";
            this._balloonDiv.style.height = height + "px";
            this._balloonDiv.style.marginLeft = "0px";
            this._balloonDiv.style.marginTop = "0px";

            this._balloonDiv.SetOpacity(0);

            this._balloonDiv.style.display = "block";
            this._balloonDiv.zIndex = 1001;
            this._balloonDiv.Fade(0, 100, 8);
        },
        /**
         * @method Hide
         */
        Hide: function ()
        {
            if (this._balloonDiv)
            {
                document.body.removeChild(this._balloonDiv);
            }
        }
    });