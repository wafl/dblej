<?php

namespace DblEj\Authentication;

interface ISaveableUser
extends IUser, \DblEj\Data\IPersistantModel
{
}