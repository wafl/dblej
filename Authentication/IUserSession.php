<?php

namespace DblEj\Authentication;

interface IUserSession
extends \DblEj\Data\IDataStore
{
    public function Get_SessionId();

    public function Get_IpAddress();

    public function Get_StartTime();
    public function Set_StartTime($timestamp);

    public function Get_User();

    public function Get_LastPingTime();

    public function Close();

    public function End($permanent = true);

    public function Ping();

    public function Set_UserAgent($userAgentString);
    public function Get_UserAgent();

    public static function CanOpen($sessionId = null);
    public static function Open(\DblEj\Application\IApplication $app, $sessionId = null);
}