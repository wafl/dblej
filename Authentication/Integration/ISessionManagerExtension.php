<?php

namespace DblEj\Authentication\Integration;

interface ISessionManagerExtension
extends ISessionManager, \DblEj\Extension\IExtension
{
}
