<?php

namespace DblEj\Authentication;

interface ISaveableUserSession
extends IUserSession, \DblEj\Data\IPersistableModel
{
}