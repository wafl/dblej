<?php

namespace DblEj\Authentication;

/**
 * @deprecated since revision 1629 in favor of Users\Integration\ISessionManagerExtension
 */
interface IUserSessionExtension
extends \DblEj\Extension\IExtension
{
    function OpenSession();
}
