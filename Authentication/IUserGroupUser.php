<?php

namespace DblEj\Authentication;

interface IUserGroupUser
extends IUser
{
    public function Get_UserGroup();

    public function Set_UserGroup(\DblEj\Authentication\IUserGroup $newUserGroup);

    public function Get_UserGroupId();
}