<?php

namespace DblEj\Authentication;

interface IUserGroup
extends \DblEj\Resources\IActor
{

    public function AddUserToGroup(\DblEj\Authentication\IUserGroupUser $user);

    public function RemoveUserFromGroup(\DblEj\Authentication\IUserGroupUser $user);

    public function Get_Title();

    public function Set_Title($newTitle);

    public function Get_UserGroupId();

    public function Set_UserGroupId($newGroupId);

    public function GetUsers();
}