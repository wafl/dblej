<?php

namespace DblEj\Authentication;

interface IUser
extends \DblEj\Resources\IActor
{

    public function Get_UserId();

    public function Get_Username();

    public function Set_Username($newUsername);
}