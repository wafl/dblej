<?php

namespace DblEj\Authentication;

interface ISaveableUserGroup
extends IUserGroup, \DblEj\Data\IPersistantModel
{
}