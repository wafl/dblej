<?php

namespace DblEj\Authentication;

interface IUserGroupSaveableUser
extends IUserGroupUser, \DblEj\Data\IPersistantModel
{
}