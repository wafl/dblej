/**
 * @class Global
 */

/**
 * @function normalizeCssProperty
 * Standardize the property name to be camel-case with no hyphens.
 * This is useful for converting css-style prperty names to javascript-style property names.
 *
 * @param {String} property The name of the non-standardized property (i.e. text-align)
 *
 * @returns {String} The javascript-form of the property name (i.e. textAlign)
 */
function normalizeCssProperty(property)
{
    var normalizedProperty = '';
    var property = property.split('-');
    for (var i = 0; i < property.length; i++)
    {
        if (i == 0)
        {
            normalizedProperty += property[0].toLowerCase();
        }
        else
        {
            normalizedProperty += property[i].UcWords();
        }
    }
    return normalizedProperty;
}