<?php

namespace DblEj\AccessControl;

/**
 * Provides methods useful for implementing objects that encapsulate multiple ResourcePermissions.
 */
interface IResourcePermissionContainer
extends IResourceContainer
{
    public function GetResourcePermissions();

    public function AddResourcePermission(IActor $actor, IResource $resource, $permission = ResourcePermission::RESOURCE_PERMISSION_NONE);

    public function AddResourcePermissionObject(IResourcePermission $resourcePermission);

    public function RemoveResourcePermission(IActor $actor, IResource $resource);

    public function IsAllowed(IResource $resource, IActor $actor, $permissionType = ResourcePermission::RESOURCE_PERMISSION_NONE);

    public function IsAllowedId($resourceId, $resourceType, IActor $actor, $permissionType = ResourcePermission::RESOURCE_PERMISSION_NONE);
}