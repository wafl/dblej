<?php

namespace DblEj\AccessControl;

/**
 * An interface that exposes methods useful for implementing objects that encapsulate multiple resources.
 */
interface IResourceContainer
extends \DblEj\EventHandling\IEventRaiser
{

    public function GetRestrictedResources();

    public function AddRestrictedResource(IRestrictedResource $resource);

    public function RemoveRestrictedResource(IRestrictedResource $resource);

    /**
     *
     * @param string $resourceId
     * @param string $resourceType
     *
     * @return \DblEj\AccessControl\IRestrictedResource Returns the resource, or null if it cannot be found
     */
    public function GetRestrictedResource($resourceId, $resourceType);
}