<?php

namespace DblEj\AccessControl;

/**
 * Provides a standard interface for Resources.
 */
interface IResource
extends \DblEj\Identity\IUnique
{
    public function Get_ResourceId();

    public function Get_ResourceType();

    public function Get_Title();
}