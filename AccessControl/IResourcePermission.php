<?php

namespace DblEj\AccessControl;

interface IResourcePermission
{
    /**
     * @return \DblEj\AccessControl\IResource
     */
    public function Get_Resource();

    public function Get_ResourceId();

    public function Get_ResourceType();

    public function Get_Permission();

    public function Get_Actor();

    public function Get_ActorId();

    public function Get_ActorTypeId();

    public function __toString();
}