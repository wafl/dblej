<?php

namespace DblEj\AccessControl;

/**
 * A generic resource that represents any user group.
 */
final class AnyGroup
extends Actor
implements IActor
{
    public function Get_ActorId()
    {
        return "*";
    }

    public function Get_ActorTypeId()
    {
        return Resource::RESOURCE_TYPE_PEOPLE;
    }

    public function Get_DisplayName()
    {
        return "All Groups";
    }
}