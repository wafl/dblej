<?php
namespace DblEj\AccessControl;

/**
 * A collection of ResourcePermissions
 */
class ResourcePermissionCollection
extends \DblEj\Collections\TypedCollection
{

    public function __construct(array $resourcePermissionArray = null)
    {
        parent::__construct($resourcePermissionArray, "\DblEj\AccessControl\IResourcePermission");
    }

    /**
     *
     * @param \DblEj\AccessControl\IActor $actor
     * @param \DblEj\AccessControl\Resource $resource
     * @return \DblEj\AccessControl\ResourcePermission
     */
    public function GetPermission(\DblEj\AccessControl\IActor $actor, \DblEj\AccessControl\IResource $resource)
    {
        $returnPermission = null;
        /* @var $resourcePermission \DblEj\AccessControl\ResourcePermission */
        foreach ($this->GetItems() as $resourcePermission)
        {
            if
                (
                    (
                        (
                            $resourcePermission->Get_ResourceId() == $resource->Get_ResourceId()
                        )
                        || $resourcePermission->Get_ResourceId() == "*"
                    )
                    &&
                    (
                        (
                            $resourcePermission->Get_ResourceType() == $resource->Get_ResourceType()
                        )
                        || $resourcePermission->Get_ResourceType() == "*"
                    )
                    &&
                    (
                        (
                            $resourcePermission->Get_ActorId() == $actor->Get_ActorId()
                        )
                        || $resourcePermission->Get_ActorId() == "*"
                    )
                    &&
                    (
                        (
                            $resourcePermission->Get_ActorTypeId() == $actor->Get_ActorTypeId()
                        )
                        || $resourcePermission->Get_ActorTypeId() == "*"
                    )
                )
            {
                $returnPermission = $resourcePermission;
                break;
            }
        }
        return $returnPermission;
    }

    public function SetPermission(\DblEj\AccessControl\IActor $actor, \DblEj\AccessControl\IResource $resource, $permission = \DblEj\AccessControl\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $permission = new \DblEj\AccessControl\ResourcePermission($resource, $actor, $permission);
        $this->AddItem($permission);
        return $permission;
    }

    public function RemovePermission(\DblEj\AccessControl\IActor $actor, \DblEj\AccessControl\IResource $resource)
    {
        $permission = $this->GetPermission($actor, $resource);
        $this->RemoveItem($permission);
    }

    /**
     *
     * @param \DblEj\AccessControl\IActor $actor
     * @param \DblEj\AccessControl\IResource $resource
     * @param int $permissionType
     * @return \DblEj\AccessControl\IResourcePermission|null
     */
    public function IsActorPermitted(\DblEj\AccessControl\IActor $actor = null, \DblEj\AccessControl\IResource $resource = null, $permissionType = \DblEj\AccessControl\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $permission = $this->GetPermission($actor, $resource);

        if (!$permission)
        {
            $resourcePermission = \DblEj\AccessControl\ResourcePermission::RESOURCE_PERMISSION_NONE;
        }
        else
        {
            $resourcePermission = $permission->Get_Permission();
        }

        if ($permission && (intval($resourcePermission) & intval($permissionType)))
        {
            return $permission;
        } else {
            return null;
        }
    }
}