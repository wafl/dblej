<?php

namespace DblEj\AccessControl;

/**
 * Thrown when a resource that doesn't exist or is otherwise invalid is referenced.
 */
class InvalidResourceException
extends \Exception
{

    public function __construct($resourceName, $reason = null, $severity = E_ERROR)
    {
        $message = "Invalid resource: $resourceName.";
        if ($reason)
        {
            $message .= "  $reason";
        }
        parent::__construct($message, $severity, null);
    }
}