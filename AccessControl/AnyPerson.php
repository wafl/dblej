<?php

namespace DblEj\AccessControl;

/**
 * A generic resource that represents any person.
 */
final class AnyPerson
extends Actor
implements IActor
{
    public function Get_ActorId()
    {
        return "*";
    }

    public function Get_ActorTypeId()
    {
        return Resource::RESOURCE_TYPE_PERSON;
    }

    public function Get_DisplayName()
    {
        return "Everybody";
    }
}