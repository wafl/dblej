<?php

namespace DblEj\AccessControl;

/**
 * Defines the permissions that a particular Actor has on a particular Resource.
 */
class ResourcePermission
extends \DblEj\System\Object\BaseObject
implements IResourcePermission
{
    //left big gaps in case new ones needed

    const RESOURCE_PERMISSION_NONE      = 0;
    const RESOURCE_PERMISSION_READ      = 2;
    const RESOURCE_PERMISSION_CREATE    = 16;
    const RESOURCE_PERMISSION_WRITE     = 128; //deprecated in favor of overwrite
    const RESOURCE_PERMISSION_OVERWRITE = 128;
    const RESOURCE_PERMISSION_MODIFY    = 128; //alias of overwrite
    const RESOURCE_PERMISSION_DELETE    = 1024;
    const RESOURCE_ROLE_READER          = 2;    //2
    const RESOURCE_ROLE_CREATOR         = 30;   //2|4|8|16
    const RESOURCE_ROLE_EDITOR          = 254;  //2|4|8|16|32|64|128
    const RESOURCE_ROLE_OWN             = 2046; //2|4|8|16|32|64|128|256|512|1024

    /**
     * @var \DblEj\AccessControl\Resource
     */
    protected $_resource;

    /**
     * @var \DblEj\AccessControl\IActor
     */
    protected $_actor;

    /**
     * @var int
     */
    protected $_permissions;

    public function __construct(IResource $resource, IActor $actor, $permissions = self::RESOURCE_PERMISSION_NONE)
    {
        parent::__construct();
        $this->_resource    = $resource;
        $this->_actor       = $actor;
        $this->_permissions = $permissions;
        $this->_uuid        = \DblEj\Util\Crypt::GenerateUuidV4();
    }

    /**
     * @return \DblEj\AccessControl\Resource
     */
    public function Get_Resource()
    {
        return $this->_resource;
    }

    public function Get_ResourceId()
    {
        return $this->_resource->Get_ResourceId();
    }

    public function Get_ResourceType()
    {
        return $this->_resource->Get_ResourceType();
    }

    public function Get_Permission()
    {
        return $this->_permissions;
    }

    /**
     *
     * @return \DblEj\AccessControl\IActor
     */
    public function Get_Actor()
    {
        return $this->_actor;
    }

    public function Get_ActorId()
    {
        return $this->_actor->Get_ActorId();
    }

    public function Get_ActorTypeId()
    {
        return $this->_actor->Get_ActorTypeId();
    }

    public function __toString()
    {
        return "Resource Permission [Resource: " . $this->_resource->Get_Title() . ", Actor: " . $this->_actor->Get_ActorTypeId() . "," . $this->_actor->Get_ActorId() . ", Permissions: " . $this->_permissions . "]";
    }
}