<?php

namespace DblEj\AccessControl;

use DblEj\EventHandling\EventInfo,
    DblEj\EventHandling\EventRaiser,
    DblEj\EventHandling\EventTypeCollection;

/**
 * Encapsulates a collection of Resources and fires events when Resources are added/removed to/from the collection.
 */
class ResourceContainer
extends EventRaiser
implements IResourceContainer
{
    const EVENT_RESOURCE_ADDED   = 1501;
    const EVENT_RESOURCE_REMOVED = 1502;

    private $_restrictedResources;

    function __construct(ResourceCollection $restrictedResources)
    {
        parent::__construct();
        $this->_restrictedResources = $restrictedResources;
    }

    public function GetRestrictedResources()
    {
        return $this->_restrictedResources;
    }

    public function AddRestrictedResource(IRestrictedResource $resource)
    {
        $this->_restrictedResources->AddResource($resource);
        $this->raiseEvent(new EventInfo(self::EVENT_RESOURCE_ADDED, $resource, $this, "The resource was added"));
        return $this;
    }

    public function RemoveRestrictedResource(IRestrictedResource $resource)
    {
        $this->_restrictedResources->RemoveResource($resource);
        $this->raiseEvent(new EventInfo(self::EVENT_RESOURCE_REMOVED, $resource, $this, "The resource was removed"));
        return $this;
    }

    public function GetRestrictedResource($resourceId, $resourceType)
    {
        return $this->_restrictedResources->GetResource($resourceId, $resourceType);
    }

    public function GetRaisedEventTypes()
    {
        return new EventTypeCollection([self::EVENT_RESOURCE_ADDED,
            self::EVENT_RESOURCE_REMOVED]);
    }
}