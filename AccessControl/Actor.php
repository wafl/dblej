<?php
namespace DblEj\AccessControl;

abstract class Actor
implements IActor
{
    use ActorTrait;
}