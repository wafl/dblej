<?php

namespace DblEj\AccessControl;

interface IRestrictedResource
extends IResource
{
    public function GetPermissions(IActor $actor, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ);
}
