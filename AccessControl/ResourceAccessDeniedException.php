<?php
namespace DblEj\AccessControl;

/**
 * Thrown when an Actor attempts to access a Resource in a way that it is not permitted for that Actor.
 */
class ResourceAccessDeniedException
extends \DblEj\Communication\AccessDeniedException
{
    /**
     *
     * @param string $resourceName
     * @param string $actorName
     * @param int $severity
     * @param \Exception $inner
     * @param string $sourceAction
     */
    public function __construct($resourceName, $actorName, $severity = E_ERROR, $inner = null, $sourceAction = null, $publicDetails = null)
    {
        $actorLabel = $actorName?$actorName:"Guest User";
        parent::__construct($resourceName, $actorName, $sourceAction, "Access to the requested Resource ($resourceName) is denied for the specified Actor ($actorLabel)".($sourceAction?". Source: $sourceAction":""), $publicDetails, null, $inner, $severity);
    }
}