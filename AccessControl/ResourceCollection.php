<?php

namespace DblEj\AccessControl;

/**
 * A collection of Resources.
 */
class ResourceCollection
extends \DblEj\Collections\KeyedCollection
{

    /**
     *
     * @param string $id
     * @param string $type
     * @return Resource|null
     */
    public function GetResource($id, $type)
    {
        return $this->GetItem("$type.$id");
    }

    /**
     *
     * @param \DblEj\AccessControl\Resource $resource
     */
    public function AddResource(IResource $resource)
    {
        $this->AddItem($resource, $resource->Get_ResourceType() . "." . $resource->Get_ResourceId());
    }

    /**
     *
     * @param \DblEj\AccessControl\Resource $resource
     */
    public function RemoveResource(IResource $resource)
    {
        $this->RemoveItem($resource);
    }

    /**
     *
     * @param string $id
     * @param string $type
     */
    public function RemoveResourceById($id, $type)
    {
        $this->RemoveItemByKey("$type.$id");
    }

    /**
     *
     * @param string $id
     * @param string $type
     * @return boolean
     */
    public function ContainsId($id, $type)
    {
        return $this->ContainsKey("$type.$id");
    }
}