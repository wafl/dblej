<?php
namespace DblEj\AccessControl;

class RestrictedResource
extends \DblEj\AccessControl\Resource
implements \DblEj\AccessControl\IRestrictedResource
{
    public function GetPermissions(\DblEj\AccessControl\IActor $actor, $accessType = \DblEj\AccessControl\ResourcePermission::RESOURCE_PERMISSION_MODIFY)
    {
        return [];
    }
}

