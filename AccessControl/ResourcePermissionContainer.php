<?php

namespace DblEj\AccessControl;

/**
 * Encapsulates a collection of ResourcePermissions and exposes methods for checking the collection for certain permissions.
 */
class ResourcePermissionContainer
extends ResourceContainer
implements IResourcePermissionContainer
{
    private $_resourcePermissions;

    function __construct(ResourceCollection $allResources, ResourcePermissionCollection $resourcePermissions)
    {
        parent::__construct($allResources);
        $this->_resourcePermissions = $resourcePermissions;
    }

    public function GetResourcePermissions()
    {
        return $this->_resourcePermissions;
    }

    public function AddResourcePermission(IActor $actor, IResource $resource, $permission = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        return $this->_resourcePermissions->SetPermission($actor, $resource, $permission);
    }

    public function AddResourcePermissionObject(IResourcePermission $resourcePermission)
    {
        $this->_resourcePermissions->AddItem($resourcePermission);
        return $this;
    }

    public function RemoveResourcePermission(IActor $actor, IResource $resource)
    {
        $this->_resourcePermissions->RemovePermission($actor, $resource);
        return $this;
    }

    public function IsAllowed(IResource $resource, IActor $actor = null, $permissionType = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        if ($actor)
        {
            return $this->_resourcePermissions->IsActorPermitted($actor, $resource, $permissionType);
        }
        else
        {
            return count($this->_resourcePermissions) == 0?false:reset($this->_resourcePermissions);
        }
    }

    public function IsAllowedId($resourceId, $resourceType, IActor $actor, $permissionType = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $resource = $this->GetRestrictedResource($resourceId, $resourceType);
        if ($resource)
        {
            return $this->_resourcePermissions->IsActorPermitted($actor, $resource, $permissionType);
        }
        else
        {
            return false;
        }
    }
}