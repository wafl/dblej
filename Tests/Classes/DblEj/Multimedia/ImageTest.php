<?php
namespace DblEj\Multimedia;

class ImageTest
extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers DblEj\Multimedia\Image::Trim
     * @covers DblEj\Multimedia\Image::Save
     * @covers DblEj\Multimedia\Image::ReduceMetaSize
     * @large
     */
    public function testTrimImage()
    {
        $testImage = new Image(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Resources/image1.jpg");
        $testImage->Trim(false, 12, 50, true, true);
        $tempFile = sys_get_temp_dir().DIRECTORY_SEPARATOR."tempimage1.jpg";
        if (file_exists($tempFile))
        {
            unlink($tempFile);
        }
        $testImage->Save($tempFile, null, 100);

        $this->assertTrue(file_exists($tempFile), "Image::Save did not save the file as expected");
        $this->assertTrue(filesize($tempFile) == 69119, "Image::Trim result file (".$tempFile.") is not the correct size.  It is ".filesize($tempFile)." instead of 69898");

        $testImage->ReduceMetaSize();

        $this->assertTrue(filesize($tempFile) == 66180, "Image::ReduceMetaSize result file is not the correct size.  It is ".filesize($tempFile)." instead of 66978");
        $diskFileDimensions = getimagesize($tempFile);
        $this->assertEquals($diskFileDimensions[0], 280, "Image::Trim result image file is not the correct width");
        $this->assertEquals($diskFileDimensions[1], 632, "Image::Trim result image file is not the correct height");

        if (file_exists($tempFile))
        {
            unlink($tempFile);
        }
    }
}
