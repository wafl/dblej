<?php
namespace DblEj\Util;

class ArraysTest
extends \PHPUnit_Framework_TestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers DblEj\Util\Arrays::MakeIndexesSequential
     */
    public function testMakeIndexesSequential()
    {
        $testArray =
        [
            "2"     => "test1",
            "8"     => "test2",
            "test3withsubelems"    =>
            [
                "test3.1",
                "test3.2eithsubelems" =>
                [
                    "john" => "3.2john1",
                    "frank" => "3.2frank2",
                    "elvis" => "3.2elvis3"
                ],
                "test3.3",
                "test3.4",
            ],
            "6"     => "test4"
        ];
        $expectedResult =
        [
            "test1",
            "test2",
            [
                "test3.1",
                [
                    "3.2john1",
                    "3.2frank2",
                    "3.2elvis3"
                ],
                "test3.3",
                "test3.4",
            ],
            "test4"
        ];
        $this->assertEquals(
          $expectedResult,
          Arrays::MakeIndexesSequential($testArray)
        );
    }
}
