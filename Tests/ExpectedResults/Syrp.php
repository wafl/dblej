<?php

namespace DblEj\Tests\ExpectedResults;

class Syrp
{

    public static function GetExpectedTest1Output()
    {
        $expectedResult = array
            (
            "Section1Header" => array
                (
                "Setting1TestEasy" => array
                    (
                    "TESTNEWCHILDHERE" => array()
                )
            ),
            "Section2Header" => array
                (
                "0" => array
                    (
                    "0" => "CellValue1",
                    "1" => "CellValue2",
                    "2" => "CellValue3"
                )
            ),
            "Section3Header" => array
                (
                "0"                  => array
                    (
                    "0" => "CellValue2_1",
                    "1" => "CellValue2_2",
                    "2" => "CellValue2_3"
                ),
                "Section2SubHeader"  => array
                    (
                    "0"                                  => array
                        (
                        "0" => "cv1",
                        "1" => "CellValue2",
                        "2" => "CellValue3",
                        "3" => "Offset"
                    ),
                    "1"                                  => array
                        (
                        "0" => "cv2_1",
                        "1" => "CellValue2_2",
                        "2" => "CellValue2_3",
                    ),
                    "Sec2SubSubHead"                     => array(),
                    "This is a test of a sub sub header" => array(),
                    "AnotherValue"                       => array
                        (
                        "0" => array
                            (
                            "0" => "S2SSHChild1",
                            "1" => "S2SSHChild2",
                            "2" => "S2SSHChild3"
                        ),
                    )
                ),
                "1"                  => array
                    (
                    "0" => "WhatIf",
                    "1" => "SomeValues",
                    "2" => "Went",
                    "3" => "Here"
                ),
                "Section2SubHeader2" => array
                    (
                    "Now youre starting to get it" => array()
                )
            )
        );

        return $expectedResult;
    }

}
?>