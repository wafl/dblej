<?php
namespace DblEj\AutomatedTesting;

class TestCaseResult
{
    private $_assertions;
    private $_isFailure;
    private $_isError;
    private $_timeLapsed;
    private $_message;
    private $_testFileName;
    private $_testLineNumber;

    public function __construct($assertions, $isFailure, $isError, $message, $timeLapsed, $testFilename, $testLineNumber)
    {
        $this->_assertions     = $assertions;
        $this->_isError        = $isError;
        $this->_isFailure      = $isFailure;
        $this->_timeLapsed     = $timeLapsed;
        $this->_message        = $message;
        $this->_testFileName   = $testFilename;
        $this->_testLineNumber = $testLineNumber;
    }

    public function Get_Assertions()
    {
        return $this->_assertions;
    }

    public function Get_IsFailure()
    {
        return $this->_isFailure;
    }

    public function Get_IsError()
    {
        return $this->_isError;
    }

    public function Get_Message()
    {
        return $this->_message;
    }

    public function Get_TestFileName()
    {
        return $this->_testFileName;
    }

    public function Get_TestFileLineNumber()
    {
        return $this->_testLineNumber;
    }
}