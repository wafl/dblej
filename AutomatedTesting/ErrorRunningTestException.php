<?php
namespace DblEj\AutomatedTesting;

class ErrorRunningTestException
extends \Exception
{
    public function __construct($message, $testName, $testType = TestCase::UNIT_TEST, $severity = E_ERROR, $inner = null)
    {
        $message = "There was an error while running Test case: " . TestCase::GetHumanReadableTestType($testType) . ", $testName.$message";
        parent::__construct($message, $severity, $inner);
    }
}