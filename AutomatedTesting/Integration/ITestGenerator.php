<?php
namespace DblEj\AutomatedTesting\Integration;

interface ITestGenerator
{
    public function GenerateTests();
}