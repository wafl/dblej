<?php
namespace DblEj\AutomatedTesting\Integration;

interface ITestGeneratorExtension
extends ITestGenerator, \DblEj\Extension\IExtension
{
}