<?php
namespace DblEj\AutomatedTesting;

/**
 * The base class for test cases.
 */
interface ITestCase
{
    const UNIT_TEST        = 1;
    const INTEGRATION_TEST = 2;

    public function Get_TestType();

    public function GetHumanReadableTestType();
}