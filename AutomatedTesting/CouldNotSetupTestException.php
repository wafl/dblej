<?php
namespace DblEj\AutomatedTesting;

class CouldNotSetupTestException
extends \Exception
{

    public function __construct($testName, $testType = TestCase::UNIT_TEST, $severity = E_ERROR, $inner = null)
    {
        $message = "Could not setup the Test case: " . TestCase::GetHumanReadableTestType($testType) . ", $testName.  See inner-exception for more details.";
        parent::__construct($message, $severity, $inner);
    }
}