<?php

namespace DblEj\UnitTesting;

/**
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\Integration\ITestGenerator
 * @see \DblEj\AutomatedTesting\Integration\ITestGenerator
 */
interface ITestGenerator
extends \DblEj\Extension\IExtension
{

    public function GenerateTests();

    public function Initialize();

    public function Shutdown();
}