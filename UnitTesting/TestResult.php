<?php

namespace DblEj\UnitTesting;

/**
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\TestResult
 * @see \DblEj\AutomatedTesting\TestResult
 */
class TestResult
{
    private $_assertionCount;
    private $_failureCount;
    private $_errorCount;
    private $_testSuites;

    public function __construct($assertions = 0, $failureCount = 0, $errorCount = 0)
    {
        $this->_assertionCount = $assertions;
        $this->_errorCount     = $failureCount;
        $this->_failureCount   = $errorCount;
        $this->_testSuites     = array();
    }

    public function AddTestCaseResult($suiteName, $testName, $assertions, $isFailure, $isError, $message, $timeLapsed, $file, $line)
    {
        if (!isset($this->_testSuites[$suiteName]))
        {
            $this->_testSuites[$suiteName] = new TestSuiteResult();
        }
        $this->_assertionCount+=$assertions;
        if ($isError)
        {
            $this->_errorCount++;
        }
        if ($isFailure)
        {
            $this->_failureCount++;
        }
        return $this->_testSuites[$suiteName]->AddTestCaseResult($testName, $assertions, $isFailure, $isError, $message, $timeLapsed, $file, $line);
    }

    public function Get_TestSuites()
    {
        return $this->_testSuites;
    }

    public function Get_AssertionCount()
    {
        return $this->_assertionCount;
    }

    public function Get_FailureCount()
    {
        return $this->_failureCount;
    }

    public function Get_ErrorCount()
    {
        return $this->_errorCount;
    }
}