<?php

namespace DblEj\UnitTesting;

/**
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\ErrorRunningTestException
 * @see \DblEj\AutomatedTesting\ErrorRunningTestException
 */
class ErrorRunningTestException
extends \Exception
{

    public function __construct($message, $testName, $testType = TestCase::UNIT_TEST, $severity = E_ERROR, $inner = null)
    {
        $message = "There was an error while running Test case: " . TestCase::GetHumanReadableTestType($testType) . ", $testName.$message";
        parent::__construct($message, $severity, $inner);
    }
}