<?php

namespace DblEj\UnitTesting;

/**
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\Integration\ITesterExtension
 * @see \DblEj\AutomatedTesting\Integration\ITesterExtension
 */
interface ITester
extends \DblEj\Extension\IExtension
{

    public function RunTest();

    public function Initialize(\DblEj\Application\IApplication $app);

    public function Shutdown();
}