<?php

namespace DblEj\UnitTesting;

/**
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\NoTestingEngineException
 * @see \DblEj\AutomatedTesting\NoTestingEngineException
 */
class NoTestingEngineException
extends \Exception
{

    public function __construct($testTypeString, $severity = E_ERROR, $inner = null)
    {
        $message = "Could not find a suitable testing engine for " . $testTypeString;
        parent::__construct($message, $severity, $inner);
    }
}