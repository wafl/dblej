<?php
namespace DblEj\UnitTesting;

/**
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\TestSuiteResult
 * @see \DblEj\AutomatedTesting\TestSuiteResult
 */
class TestSuiteResult
{
    private $_testCaseResults = array();
    private $_assertionCount;
    private $_failureCount;
    private $_errorCount;

    public function AddTestCaseResult($testName, $assertions, $isFailure, $isError, $message, $timeLapsed, $testFilename, $testLineNumber)
    {
        $this->_assertionCount+=$assertions;
        if ($isError)
        {
            $this->_errorCount++;
        }
        if ($isFailure)
        {
            $this->_failureCount++;
        }
        $this->_testCaseResults[$testName] = new TestCaseResult($assertions, $isFailure, $isError, $message, $timeLapsed, $testFilename, $testLineNumber);
        return $this->_testCaseResults[$testName];
    }

    public function Get_TestCaseResults()
    {
        return $this->_testCaseResults;
    }
}