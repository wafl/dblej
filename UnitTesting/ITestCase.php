<?php

namespace DblEj\UnitTesting;

/**
 * The base class for test cases.
 * @deprecated since revision 1629 in favor of DblEj\AutomatedTesting\ITestCase
 * @see \DblEj\AutomatedTesting\ITestCase
 */
interface ITestCase
{
    const UNIT_TEST        = 1;
    const INTEGRATION_TEST = 2;

    public function Get_TestType();

    public function GetHumanReadableTestType();
}