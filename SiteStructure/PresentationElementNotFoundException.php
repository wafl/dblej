<?php

namespace DblEj\SiteStructure;

/**
 * Thrown when a Presentation Template or Presentation Stylesheet cannot be found.
 */
class PresentationElementNotFoundException
extends \Exception
{

    public function __construct($presentationElementName, $message = "Cannot find a presentation element that is named %s", $severity = E_ERROR, \Exception $innerException = null)
    {
        $message = sprintf($message, $presentationElementName);
        parent::__construct($message, $severity, $innerException);
    }
}