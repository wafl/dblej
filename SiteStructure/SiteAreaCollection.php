<?php

namespace DblEj\SiteStructure;

/**
 * Stores a collection of SiteaAreas
 */
class SiteAreaCollection
extends \DblEj\Collections\TypedKeyedCollection
{

    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray, "\\DblEj\\SiteStructure\\SiteArea");
    }

    /**
     * Get the SiteaArea that has the specified id.
     * @param string $siteAreaId
     * @return \DblEj\SiteStructure\SiteArea
     */
    public function GetSiteArea($siteAreaId)
    {
        return $this->GetItem($siteAreaId);
    }

    /**
     * Add a new site area
     *
     * @param \DblEj\SiteStructure\SiteArea $siteArea
     */
    public function AddSiteArea(\DblEj\SiteStructure\SiteArea $siteArea)
    {
        return $this->AddItem($siteArea, $siteArea->Get_AreaId());
    }
}