<?php
namespace DblEj\SiteStructure;

/**
 * An exception class
 */
class SiteStructureException
extends \DblEj\System\Exception
{

    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("$message", $severity, $inner);
    }
}