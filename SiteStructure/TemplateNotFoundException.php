<?php

namespace DblEj\SiteStructure;

/**
 * Thrown when a Presentation Template can't be found.
 */
class TemplateNotFoundException
extends PresentationElementNotFoundException
{

    public function __construct($templateName, array $folderChecked, $severity = E_ERROR, \Exception $innerException = null)
    {
        $folderList = implode(",", $folderChecked);
        parent::__construct($templateName, "Cannot find a template file by the name of %s.  Looked in $folderList", $severity, $innerException);
    }
}