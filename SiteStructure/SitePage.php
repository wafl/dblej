<?php

namespace DblEj\SiteStructure;

use DblEj\Collections\KeyedCollection,
    DblEj\Collections\TypedKeyedCollection,
    DblEj\Extension\ControlBase,
    DblEj\Presentation\Template,
    DblEj\Resources\IResource,
    DblEj\Resources\Resource,
    DblEj\UI\Stylesheet,
    Exception;

class SitePage
implements IResource
{
    protected $_pageId;
    protected $_logicPath;
    protected $_presentationPath;
    protected $_javascripts           = array();
    protected $_additionalRawHeadHtml = "";
    protected $_additionalRawFooterHtml = "";
    protected $_tokenData             = null;
    private $_title;
    private $_fullTitle;
    private $_description;
    private $_keywords;
    private $_parentAreaId;
    private $_dblejIncludes           = array();
    private $_cssFiles                = array();
    private $_displayOrder;
    private $_menuItems               = array();
    private $_preprocessCss;
    private $_isAreaDefault           = false;
    private $_deferredControls        = array(); //a list of control names that couldnt be loaded during SitePage instantiation because engine wasnt ready
    private $_regiteredBy;

    /**
     * A reference to the (probably Html) template
     * @var Template
     */
    protected $_template;
    private static $_allSitePages = array();

    /**
     * @var KeyedCollection
     */
    private $_controls;

    /**
     *
     * @param string $uniqueId
     * @param string $title
     * @param string $description
     * @param string $logicPath
     * @param string $presentationPath
     * @param integer $displayOrder
     * @param string $parentAreaId
     * @param boolean $isAreaDefault
     * @param string $fullTitle
     * @param string $keywords
     * @param boolean $preprocessCss
     * @param \DblEj\Extension\ControlBase[]|string[] $pageControls An array of control objects or if the engine isn't ready to instantiate controls then an array of control names
     */
    public function __construct($uniqueId, $title, $description, $logicPath, $presentationPath, $displayOrder = 0, $parentAreaId = null, $isAreaDefault = false, $fullTitle = null, $keywords = null, $preprocessCss = false, $pageControls = [], $registeredBy = null)
    {
        $this->_pageId    = $uniqueId;
        $this->_title     = $title;
        $this->_tokenData = new \DblEj\Data\FieldCollection();
        $this->_regiteredBy = $registeredBy;
        if ($fullTitle)
        {
            $this->_fullTitle = $fullTitle;
        }
        else
        {
            $this->_fullTitle = $title;
        }
        $this->_description   = $description;
        $this->_keywords      = $keywords;
        $this->_isAreaDefault = $isAreaDefault;
        $this->_parentAreaId  = $parentAreaId;
        $this->_logicPath     = $logicPath;
        $this->_presentationPath = $presentationPath;
        $this->_displayOrder  = $displayOrder;
        $this->_preprocessCss = $preprocessCss;
        $this->_controls      = new KeyedCollection();

        $this->_template       = new Template($this->_title, $presentationPath);
        self::$_allSitePages[] = $this;

        foreach ($pageControls as $pageControl)
        {
            if (is_object($pageControl))
            {
                $this->AddControl($pageControl);
            } else {
                $this->_deferredControls[] = $pageControl;
            }
        }
    }

    public function DoesClientLogicExist(\DblEj\Application\IWebApplication $app)
    {
        $logicFolder = $app->Get_LocalLogicFolder();
        $filePath = $logicFolder.$this->GetClientLogicFile();
        return file_exists($filePath);
    }

    public function Get_RegisteredBy()
    {
        return $this->_regiteredBy;
    }

    /**
     *
     * @return string
     */
    public function GetServerLogicFile()
    {
        return $this->_logicPath . ".php";
    }

    /**
     *
     * @return string
     */
    public function GetClientLogicFile()
    {
        return $this->_logicPath . ".js";
    }

    /**
     *
     * @param boolean $onlyVisible
     * @return SitePage[]
     */
    public static function GetAll($onlyVisible = true)
    {
        $returnPages = array();
        if ($onlyVisible)
        {
            foreach (self::$_allSitePages as $sitePage)
            {
                if ($sitePage->Get_DisplayOrder() > 0)
                {
                    $returnPages[] = $sitePage;
                }
            }
        }
        else
        {
            $returnPages = self::$_allSitePages;
        }
        return $returnPages;
    }

    /**
     *
     * @param \DblEj\Extension\ControlBase $control
     * @param string $name
     */
    public function AddControl(ControlBase $control, $name = null)
    {
        if (!$name)
        {
            $name = \get_class($control);
        }
        $this->_controls->AddItem($control, $name);
    }

    public function HasClientControlObjects()
    {
        $aControlHasJs = false;
        foreach ($this->_controls as $control)
        {
            if ($control->Get_Javascripts())
            {
                $aControlHasJs = true;
                break;
            }
        }
        return $aControlHasJs;
    }

    public function HasControlCss()
    {
        $aControlHasCss = false;
        foreach ($this->_controls as $control)
        {
            if ($control->Get_MainStylesheet())
            {
                $aControlHasCss = true;
                break;
            }
        }
        return $aControlHasCss;
    }

    public function GetDeferredControls()
    {
        return $this->_deferredControls;
    }

    /**
     *
     * @param \DblEj\Collections\TypedKeyedCollection $controls
     */
    public function AddControls(TypedKeyedCollection $controls)
    {
        $this->_controls->MergeWith($controls);
    }


    /**
     *
     * @return \DblEj\Collections\TypedKeyedCollection
     */
    public function GetControls()
    {
        return $this->_controls;
    }

    /**
     *
     * @param string $filename
     */
    public function AddJavascript($filename)
    {
        if (array_search($filename, $this->_javascripts) === false)
        {
            array_push($this->_javascripts, $filename);
        }
    }

    /**
     *
     * @param array $filenameArray
     * @param string $customPath
     */
    public function AddJavascripts($filenameArray, $customPath = null)
    {
        foreach ($filenameArray as $filename)
        {
            $this->AddJavascript($filename, $customPath);
        }
    }

    /**
     *
     * @param string $filename
     * @param string $customPath
     */
    public function AddJavascriptDblEj($filename, $customPath = null)
    {
        if (array_search($filename, $this->_dblejIncludes) === false)
        {
            if ($customPath == null)
            {
                $customPath = __DIR__ . "/../";
            }
            if (file_exists("$customPath$filename"))
            {
                array_push($this->_dblejIncludes, $filename);
            }
        }
    }

    /**
     *
     * @param \DblEj\UI\Stylesheet $styleSheet
     */
    public function AddCss(Stylesheet $styleSheet)
    {
        $this->_cssFiles[$styleSheet->GetUniqueId()] = $styleSheet;
    }

    /**
     *
     * @param string $headhtml
     */
    public function AddRawHeadHtml($headhtml)
    {
        $this->_additionalRawHeadHtml .= $headhtml;
    }

    public function AddRawFooterHtml($headhtml)
    {
        $this->_additionalRawFooterHtml .= $headhtml;
    }

    /**
     *
     * @return string
     */
    public function GetAdditionalRawHeadHtml()
    {
        return $this->_additionalRawHeadHtml;
    }
    public function GetAdditionalRawFooterHtml()
    {
        return $this->_additionalRawFooterHtml;
    }
    /**
     *
     * @return string
     */
    public function Get_ParentAreaId()
    {
        return $this->_parentAreaId;
    }

    /**
     *
     * @return string
     */
    public function Get_Keywords()
    {
        return $this->_keywords;
    }

    /**
     *
     * @param string $keywords
     */
    public function Set_Keywords($keywords)
    {
        $this->_keywords = $keywords;
    }

    /**
     * Get the SiteArea that this page lives in.
     *
     * @param \DblEj\Application\IWebApplication $app
     * @return SiteArea
     */
    public function Get_ParentArea(\DblEj\Application\IWebApplication $app)
    {
        $returnArea = null;
        if ($this->_parentAreaId)
        {
            $returnArea = $app->Get_SiteMap()->GetSiteArea($this->_parentAreaId);
        }
        return $returnArea;
    }

    /**
     *
     * @return boolean
     */
    public function Get_IsAreaDefault()
    {
        return $this->_isAreaDefault;
    }

    /**
     *
     * @return string
     */
    public function Get_PageId()
    {
        return $this->_pageId;
    }

    /**
     *
     * @return string
     */
    public function Get_Filename()
    {
        return $this->_logicPath;
    }

    /**
     *
     * @param string $filename
     */
    public function Set_Filename($filename)
    {
        $this->_logicPath = $filename;
    }

    /**
     *
     * @return integer
     */
    public function Get_DisplayOrder()
    {
        return $this->_displayOrder;
    }

    /**
     *
     * @return string
     */
    public function Get_Title()
    {
        return $this->_title;
    }

    /**
     *
     * @return string
     */
    public function Get_FullTitle()
    {
        return $this->_fullTitle;
    }

    /**
     * Get the Template associated with this page
     * @return Template
     */
    public function Get_Template()
    {
        return $this->_template;
    }

    /**
     *
     * @return boolean
     */
    public function Get_PreProcessCss()
    {
        return $this->_preprocessCss;
    }

    public function IsCachedOnServerSide($renderer, $cacheKey1 = null, $cacheKey2 = null)
    {
        return $renderer->DoesCacheExist($this->_template->Get_Filename().".tpl", $cacheKey1);
    }


    /**
     *
     * @param \Smarty $templateEngine
     * @return mixed
     */
    public function ClearCache(\Smarty $templateEngine)
    {
        try
        {
            return $templateEngine->clearCache($this->GetSmartyTemplateFilename());
        }
        catch (\Exception $err)
        {
            error_log("Could not clear template cache, probably because it doesnt exist.  " . $err->getMessage());
        }
    }

    /**
     *
     * @param \DblEj\Data\IFieldPublisher $tokenData
     */
    public function Set_TokenData(\DblEj\Data\IFieldPublisher $tokenData)
    {
        $this->_tokenData = $tokenData;
    }

    /**
     *
     * @return \DblEj\Data\IFieldPublisher
     */
    public function Get_TokenData()
    {
        return $this->_tokenData;
    }

    /**
     *
     * @param \DblEj\Presentation\ITemplateRenderer $renderer
     * @param \DblEj\Presentation\RenderOptions $renderOptions
     * @return string
     * @throws SitePageRenderException
     * @throws UndefinedTemplateException
     * @throws UndefinedSitePageException
     * @throws TemplateNotFoundException
     */
    public function Render(\DblEj\Presentation\Integration\ITemplateRenderer $renderer, \DblEj\Presentation\RenderOptions $renderOptions)
    {
        $returnHtml   = "";
        try
        {
            if ($this->_pageId !== null)
            {
                if (!$this->_template)
                {
                    throw new UndefinedTemplateException();
                }
            }
            else
            {
                throw new UndefinedSitePageException("The specified page is not currently defined in any running  application");
            }

            if (!$renderer->DoesTemplateExist($this->_template->Get_Filename()))
            {
                throw new TemplateNotFoundException($this->_template->Get_Filename(), $renderer->GetTemplateFolders());
            }
            $tokenData = $this->Get_TokenData();

            if (is_array($tokenData))
            {
                $renderOptions->AddToken($this->Get_TokenData());
            }
            else
            {
                $renderOptions->AddObjectToken($this->Get_TokenData());
            }
            $returnHtml = $renderer->Render($this->_template, $renderOptions);
        }
        catch (\Exception $e)
        {
            throw new SitePageRenderException($this, E_WARNING, $e, "There was an error trying to render the page (" . $this->_template->Get_Filename() . ")");
        }
        if (!$returnHtml)
        {
            throw new SitePageRenderException($this, E_WARNING, null, "The page returned no contents (" . $this->_template->Get_Filename() . ")");
        }
        return $returnHtml;
    }

    /**
     *
     * @param string $title
     */
    public function Set_Title($title)
    {
        $this->_title = $title;
    }

    /**
     *
     * @param string $fullTitle
     */
    public function Set_FullTitle($fullTitle)
    {
        $this->_fullTitle = $fullTitle;
    }

    /**
     *
     * @param boolean $isAreaDefault
     */
    public function Set_IsAreaDefault($isAreaDefault)
    {
        $this->_isAreaDefault = $isAreaDefault;
    }

    /**
     *
     * @param string $areaid
     */
    public function Set_ParentAreaId($areaid)
    {
        $this->_parentAreaId = $areaid;
    }

    /**
     *
     * @param string $description
     */
    public function Set_Description($description)
    {
        $this->_description = $description;
    }

    /**
     *
     * @return string
     */
    public function Get_Description()
    {
        return $this->_description;
    }

    /**
     *
     * @return string
     */
    public function Get_LogicPath()
    {
        return $this->_logicPath;
    }


    /**
     *
     * @param string $templateParentFolder
     * @param string $logicParentFolder
     * @return int
     */
    public function GetLastModifiedTime(\DblEj\Application\IWebApplication $app)
    {
        //check the php file
        //check the template
        //and return the date for the newest one

        $logicFolder = $app->Get_LocalLogicFolder();
        $phpFilePath      = $logicFolder.$this->GetServerLogicFile();
        $renderer = $app->Get_TemplateRenderer();
        if (file_exists($phpFilePath))
        {
            $phpfiletime = filemtime($phpFilePath);
        }
        else
        {
            $phpfiletime = 0;
        }

        $templatefiletime = 0;
        if ($renderer)
        {
            foreach ($renderer->GetTemplateFolders() as $templateFolder)
            {
                $templateFilePath = $templateFolder . $this->_template->Get_Filename().".tpl";
                if (file_exists($templateFilePath))
                {
                    $templatefiletime = filemtime($templateFilePath);
                    break;
                }
            }
        }

        if ($phpfiletime > $templatefiletime)
        {
            return $phpfiletime;
        }
        else
        {
            return $templatefiletime;
        }
    }

    /**
     *
     * @return \DblEj\UI\Stylesheet[]
     */
    public function GetStyleSheets()
    {
        return $this->_cssFiles;
    }

    /**
     *
     * @return array
     */
    public function GetJavascriptIncludes()
    {
        $returnArray          = array();
        $returnArray["Lib"]   = $this->_javascripts;
        $returnArray["DblEj"] = $this->_dblejIncludes;
        return $returnArray;
    }

    /**
     *
     * @return string[]
     */
    public function Get_JavascriptIncludesLib()
    {
        return $this->_javascripts;
    }

    /**
     *
     * @return string[]
     */
    public function GetJavascriptIncludesDblEj()
    {
        return $this->_dblejIncludes;
    }

    /**
     *
     * @return \DblEj\SiteStructure\MenuItem[]
     */
    public function GetMenuItems()
    {
        return $this->_menuItems;
    }

    /**
     *
     * @param string $seperator
     * @param boolean $withLinks
     * @return string
     */
    public function GetTitleWithBreadcrumb($seperator = "&gt;", $withLinks = false)
    {
        $parentArea = SiteArea::GetAreaById($this->_parentAreaId);
        if ($parentArea)
        {
            return $parentArea->GetTitleWithBreadcrumb($seperator, $withLinks) . $seperator . ($withLinks ? "<a href=\"/" . $this->Get_LogicPath() . "\">" . $this->_title . "</a>" : $this->_title);
        }
        else
        {
            return $withLinks ? "<a class=\"CurrentPage\" href=\"/" . $this->Get_LogicPath() . "\">" . $this->_title . "</a>" : $this->_title;
        }
    }

    /**
     *
     * @param string $menuName
     * @param string $menuItemName
     * @param string $href
     * @param string $onclick
     * @param string $iconImage
     * @param integer $privilegeLevel
     * @return \DblEj\SiteStructure\MenuItem
     */
    public function AddMenuItem($menuName, $menuItemName, $href, $onclick = "", $iconImage = "", $privilegeLevel = 0)
    {
        $menuItem                                   = new MenuItem($menuItemName, $href, $iconImage, $onclick, $privilegeLevel);
        $this->_menuItems[$menuName][$menuItemName] = $menuItem;
        return $menuItem;
    }

    /**
     *
     * @return string
     */
    public function Get_ResourceId()
    {
        return $this->_pageId;
    }

    /**
     *
     * @return integer
     */
    public function Get_ResourceType()
    {
        return Resource::RESOURCE_TYPE_SITEPAGE;
    }

    /**
     *
     * @return string
     */
    public function Get_Id()
    {
        return $this->_pageId;
    }

    /**
     *
     * @return string
     */
    public function Get_Uid()
    {
        return "SitePage_" . $this->_pageId;
    }
}