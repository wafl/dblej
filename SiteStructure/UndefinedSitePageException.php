<?php

namespace DblEj\SiteStructure;

/**
 * Thrown when trying to reference a SitePage that doesn't exist.
 */
class UndefinedSitePageException
extends \Exception
{

}