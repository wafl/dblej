<?php

namespace DblEj\SiteStructure;

/**
 * Thrown when referencing a PresentationTemplate that doesn't exist.
 */
class UndefinedTemplateException
extends \Exception
{

}