<?php

namespace DblEj\SiteStructure;

/**
 * Information about an item in a navigational menu.
 */
class MenuItem
{
    private $_title;
    private $_href;
    private $_onclick;
    private $_privilegeLevel;
    private $_iconImage;

    public function __construct($title, $href, $iconImage = "", $onclick = "", $privilegeLevel = 0)
    {
        $this->_title          = $title;
        $this->_href           = $href;
        $this->_onclick        = $onclick;
        $this->_privilegeLevel = $privilegeLevel;
        $this->_iconImage      = $iconImage;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Get_Href()
    {
        return $this->_href;
    }

    public function Get_Onclick()
    {
        return $this->_onclick;
    }

    public function Get_PrivilegeLevel()
    {
        return $this->_privilegeLevel;
    }

    public function Get_IconImage()
    {
        return $this->_iconImage;
    }
}