<?php

namespace DblEj\SiteStructure;

class SitePageRenderException
extends \Exception
{

    public function __construct(\DblEj\SiteStructure\SitePage $page, $severity = E_ERROR, $inner = null, $message = "")
    {
        $message = $message ? "There was an error while rendering the Site Page.  $message" . $page->Get_Filename() : "There was an error while rendering the Site Page.  See inner-exception for more details." . $page->Get_Filename();
        parent::__construct($message, $severity, $inner);
    }
}