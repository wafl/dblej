<?php

namespace DblEj\SiteStructure;

/**
 * Stores a collection of SiteaPages
 */
class SitePageCollection
extends \DblEj\Collections\TypedKeyedCollection
{

    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray, "\\DblEj\\SiteStructure\\SitePage");
    }

    /**
     * Get the Site Page that has the specified id.
     * @param string $sitePageId
     *
     * @return \DblEj\SiteStructure\SitePage
     */
    public function GetSitePage($sitePageId)
    {
        return $this->GetItem($sitePageId);
    }

    /**
     * Add a new site page
     *
     * @param \DblEj\SiteStructure\SitePage $sitePage
     *
     * @return SitePageCollection Description
     */
    public function AddSitePage(\DblEj\SiteStructure\SitePage $sitePage)
    {
        return $this->AddItem($sitePage, $sitePage->Get_PageId());
    }
}