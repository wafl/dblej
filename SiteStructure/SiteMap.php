<?php

namespace DblEj\SiteStructure;

use DblEj\Application\IApplication,
    DblEj\SiteStructure\SiteArea,
    DblEj\SiteStructure\SiteAreaCollection,
    DblEj\SiteStructure\SitePage,
    DblEj\SiteStructure\SitePageCollection;

class SiteMap
{
    /**
     * All Site Areas.
     *
     * @var SiteAreaCollection|SiteArea[]
     */
    private $_siteAreas;

    /**
     * All Site Pages.
     *
     * @var SitePageCollection|SitePage[]
     */
    private $_sitePages;

    public function __construct(SiteAreaCollection $siteAreas = null, SitePageCollection $sitePages = null)
    {
        $this->_siteAreas = $siteAreas ? $siteAreas : new SiteAreaCollection();
        $this->_sitePages = $sitePages ? $sitePages : new SitePageCollection();
    }

    public function AddSiteArea(SiteArea $siteArea)
    {
        $this->_siteAreas->AddSiteArea($siteArea);
    }

    public function GetSiteArea($siteAreaId)
    {
        return $this->_siteAreas->GetSiteArea($siteAreaId);
    }

    public function GetSiteAreas($parentArea = null)
    {
        $returnAreas = [];
        foreach ($this->_siteAreas as $area)
        {
            if ($area->Get_ParentArea() == $parentArea)
            {
                $returnAreas[$area->Get_AreaId()] = $area;
            }
        }
        return $returnAreas;
    }

    public function GetAllSiteAreas()
    {
        return $this->_siteAreas;
    }

    public function AddSitePage(SitePage $sitePage)
    {
        $this->_sitePages->AddSitePage($sitePage);
    }

    public function GetSitePage($sitePageId)
    {
        return $this->_sitePages->GetSitePage($sitePageId);
    }

    public function GetSitePageByUrl($url)
    {
        $foundPage = null;
        foreach ($this->_sitePages as $sitePage)
        {
            if ($sitePage->Get_LogicPath() == $url)
            {
                $foundPage = $sitePage;
                break;
            }
        }
        return $foundPage;
    }

    public function GetSitePages($parentArea = null)
    {
        $returnPages = [];
        foreach ($this->_sitePages as $page)
        {
            if (($parentArea && $page->Get_ParentAreaId() == $parentArea->Get_AreaId()) || (!$parentArea && !$page->Get_ParentAreaId()))
            {
                $returnPages[$page->Get_PageId()] = $page;
            }
        }
        return $returnPages;
    }

    public function GetAllSitePages()
    {
        return $this->_sitePages;
    }

    public function ParseSiteStructureConfig(array $siteStructureConfig, SiteArea $parentArea = null, IApplication $application = null)
    {
        $siteAreaDisplayOrder = 0;
        foreach ($siteStructureConfig as $itemName => &$itemChildren)
        {
            if (isset($itemChildren["Logic"]))
            {
                //this is a page
                $displayOrder  = isset($itemChildren["DisplayOrder"]) ? $itemChildren["DisplayOrder"] : 999;
                $isDefault     = isset($itemChildren["IsDefault"]) ? $itemChildren["IsDefault"] : false;
                $description   = isset($itemChildren["Description"]) ? $itemChildren["Description"] : "";
                $caption       = isset($itemChildren["Caption"]) ? $itemChildren["Caption"] : "";
                $keywords      = isset($itemChildren["Keywords"]) ? $itemChildren["Keywords"] : "";
                $preProcessCss = isset($itemChildren["PreProcessCss"]) ? $itemChildren["PreProcessCss"] : false;
                $pageControlNames  = isset($itemChildren["Controls"]) ? $itemChildren["Controls"] : [];
                if (!is_array($pageControlNames))
                {
                    if ($pageControlNames)
                    {
                        $pageControlNames = [$pageControlNames];
                    } else {
                        $pageControlNames = [];
                    }
                }

                if ($parentArea)
                {
                    $parentPathString = $parentArea->GetPathString(".");
                    $this->AddSitePage(new SitePage($parentPathString . "." . $itemName, $itemName, $description, $itemChildren["Logic"], $itemChildren["Presentation"], $displayOrder, $parentArea->Get_AreaId(), $isDefault, $caption, $keywords, $preProcessCss, $pageControlNames, "Site Structure Config parent"));
                }
                else
                {
                    $this->AddSitePage(new SitePage($itemName, $itemName, $description, $itemChildren["Logic"], $itemChildren["Presentation"], $displayOrder, null, $isDefault, $caption, $keywords, $preProcessCss, $pageControlNames, "Site Structure Config"));
                }
            }
            elseif (strtolower($itemName) != "menusettings")
            {
                //this is an area
                $siteAreaDisplayOrder++;

                $areaCaption      = $itemName;
                $areaMenuCssClass = "";

                //check for area properties
                foreach ($itemChildren as $pageChildName => $pageChild)
                {
                    switch (strtolower($pageChildName))
                    {
                        case "menusettings":
                            foreach ($pageChild as $pageChildSetting => $pageChildSettingValue)
                            {
                                switch (strtolower($pageChildSetting))
                                {
                                    case "caption":
                                        $areaCaption      = $pageChildSettingValue;
                                        break;
                                    case "cssclass":
                                        $areaMenuCssClass = $pageChildSettingValue;
                                        break;
                                }
                            }
                            break;
                    }
                }
                $siteArea = new SiteArea($itemName, $areaCaption, $siteAreaDisplayOrder, 0, null, $parentArea, $areaMenuCssClass);
                $this->AddSiteArea($siteArea);
                $this->ParseSiteStructureConfig($itemChildren, $siteArea, $application);
            }
        }
        return $this;
    }
}