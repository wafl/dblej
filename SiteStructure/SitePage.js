
/*
 * @namespace DblEj.SiteStructure
 */
Namespace("DblEj.SiteStructure");

/*
 * @class SitePage Utility class to help in the client-side implementation of dblej SitePages
 * @static
 */
DblEj.SiteStructure.SitePage = {};

/**
 * @function SetFileIncluded Global notification that a client-side file has been "included" by the client.
 * @param {String} filename
 * @return {SitePage} Returns <i>this</i> for chained method calls.
 */
DblEj.SiteStructure.SitePage.SetFileIncluded = function (filename)
{
    DblEj.SiteStructure.SitePage._includedFiles[filename] = 1;
    return DblEj.SiteStructure.SitePage;
};

/**
 * @function GetFileIncluded Find out if a client-side file has been "included" by the client.
 * @param {String} filename
 * @return {Boolean} <i>True</i> if the file has been included, otherwise <i>false</i>.
 */
DblEj.SiteStructure.SitePage.GetFileIncluded = function (filename)
{
    if (DblEj.SiteStructure.SitePage._includedFiles[filename])
    {
        return true;
    } else {
        return false;
    }
};

/**
 * @property Get_IncludedFiles Get all of the filenames that have been "included" by the client thus far
 * @return Object
 */
DblEj.SiteStructure.SitePage.Get_IncludedFiles = function ()
{
    return DblEj.SiteStructure.SitePage._includedFiles;
};

/**
 * @function IncludeJs Include a Javascript file dynamically at run-time
 * @static
 * @param filename string The name of the file to include
 * @param callbackFunction function *optional The function to call when the javascript file has been included and parsed by the client
 */
DblEj.SiteStructure.SitePage.IncludeJs = function (filename, callbackFunction)
{
    if (DblEj.SiteStructure.SitePage._fileIncludesInProgress[filename])
    {
        //already started downloading it
        if (!DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename])
        {
            DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename] = [];
        }
        if (callbackFunction)
        {
            DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename][DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename].length] = callbackFunction;
        }
    }
    else if (DblEj.SiteStructure.SitePage._includedFiles[filename])
    {
        if (callbackFunction)
        {
            callbackFunction(filename);
            if (IsDefined(DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename]))
            {
                for (var callbackIdx = 0; callbackIdx <
                    DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename].length; callbackIdx++)
                {
                    DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename][callbackIdx](filename);
                    delete DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename][callbackIdx];
                }
                delete DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename];
            }
        }
        if (IsDefined(DblEj.SiteStructure.SitePage._fileIncludesInProgress[filename]))
        {
            delete DblEj.SiteStructure.SitePage._fileIncludesInProgress[filename];
        }
    } else {
        DblEj.SiteStructure.SitePage._fileIncludesInProgress[filename] = 1;
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.async = true;

        if (!DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename])
        {
            DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename] = [];
        }
        if (callbackFunction)
        {
            DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename][DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename].length] = callbackFunction;
        }

        script.onload = DblEj.SiteStructure.SitePage.ScriptLoad_handler;
        script.src = filename;
        head.appendChild(script);
    }
};
DblEj.SiteStructure.SitePage.ScriptLoad_handler = function ()
{
    var filename = this.GetAttribute("src");
    DblEj.SiteStructure.SitePage.SetFileIncluded(filename);
    if (DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename])
    {
        for (var callbackIdx = 0; callbackIdx < DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename].length; callbackIdx++)
        {
            var callbackFunction = DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename][callbackIdx];
            delete DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename][callbackIdx];
            callbackFunction(filename);
        }
        delete DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting[filename];
    }
    if (IsDefined(DblEj.SiteStructure.SitePage._fileIncludesInProgress[filename]))
    {
        delete DblEj.SiteStructure.SitePage._fileIncludesInProgress[filename];
    }

};
DblEj.SiteStructure.SitePage._includedFiles = new Object();
DblEj.SiteStructure.SitePage._fileIncludesInProgress = new Object();
DblEj.SiteStructure.SitePage._fileIncludesCallbacksWaiting = new Object();