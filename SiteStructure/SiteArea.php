<?php

namespace DblEj\SiteStructure;

/**
 * Represents a <em>Site Area</em> within a web site's structure.
 * A <em>Site Area</em> is a logical grouping of child <em>Site Areas</em> and <em>Site Pages</em>.
 */
class SiteArea
implements \DblEj\Resources\IResource
{
    private $_siteAreaId;
    private $_title;
    private $_displayOrder;

    /**
     * @var array
     */
    private $_defaultPages;
    private $_parentArea;
    private $_privilegeLevel;
    private $_menuCssClass;

    public function __construct($uniqueId, $title, $displayOrder, $privilegeLevel, SitePage $defaultPage = null, SiteArea $parentArea = null, $menuCssClass = null)
    {
        $this->_siteAreaId     = $uniqueId;
        $this->_title          = $title;
        $this->_displayOrder   = $displayOrder;
        $this->_privilegeLevel = $privilegeLevel;
        $this->_parentArea     = $parentArea;
        $this->_defaultPage    = $defaultPage;
        $this->_menuCssClass   = $menuCssClass;
    }

    public function Get_AreaId()
    {
        return $this->_siteAreaId;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Set_Title($title)
    {
        $this->_title = $title;
    }

    public function Get_MenuCssClass()
    {
        return $this->_menuCssClass;
    }

    public function Get_DisplayOrder()
    {
        return $this->_displayOrder;
    }

    public function Set_DisplayOrder($displayOrder)
    {
        $this->_displayOrder = $displayOrder;
    }

    public function Get_PrivilegeLevel()
    {
        return $this->_privilegeLevel;
    }

    public function Set_PrivilegeLevel($val)
    {
        $this->_privilegeLevel = $val;
    }

    /**
     * Get this a Site Area's parent Site Area.
     *
     * @return SiteArea The parent Site Area, or null if this is a top level Site Area.
     */
    public function Get_ParentArea()
    {
        return $this->_parentArea;
    }

    public function Set_ParentArea($area)
    {
        $this->_parentArea = $area;
    }

    /**
     * Add the default page for this site area
     * More than one default page cabe be added
     * in which case the first matching page is used
     * @since 662 replaces Set_DefaultPage()
     * @param SitePage $page
     */
    public function AddDefaultPage(SitePage $page)
    {
        $this->_defaultPages[$page->Get_PageId()] = $page;
    }

    public function GetDefaultPages()
    {
        return $this->_defaultPages;
    }

    public function GetParentAreaId()
    {
        if ($this->_parentArea)
        {
            return $this->_parentArea->Get_AreaId();
        }
        else
        {
            return null;
        }
    }

    public function GetPathString($seperator = ".")
    {
        if ($this->_parentArea)
        {
            return $this->_parentArea->GetPathString($seperator) . $seperator . $this->_siteAreaId;
        }
        else
        {
            return $this->_siteAreaId;
        }
    }

    public function GetDefaultPageLink()
    {
        if ($this->_defaultPage)
        {
            return $this->_defaultPage->Get_PrettyUrlString();
        }
        else
        {
            return null;
        }
    }

    public function GetTitleWithBreadcrumb($seperator = "&gt;", $withLinks = false)
    {
        if ($this->_parentArea)
        {
            return $this->_parentArea->Get_TitleWithBreadcrumb() . $seperator . ($withLinks ? "<a href=\"/" . $this->GetDefaultPageLink() . "\">" . $this->_title . "</a>" : $this->_title);
        }
        else
        {
            return $withLinks ? "<a href=\"/" . $this->GetDefaultPageLink() . "\">" . $this->_title . "</a>" : $this->_title;
        }
    }

    public function Get_Id()
    {
        return $this->_siteAreaId;
    }

    public function Get_ResourceId()
    {
        return $this->_siteAreaId;
    }

    public function Get_ResourceType()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_SITEAREA;
    }

    public function Get_Uid()
    {
        return "Site area: ".$this->_siteAreaId;
    }
}