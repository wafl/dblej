<?php
namespace DblEj\Extension;

/**
 * Provides methods for getting the components of a modular application.
 */
interface IModularLoader
{

    /**
     * Get all available modules in the application.
     *
     * @return \DblEj\Extension\ModuleCollection
     */
    public function GetModules(\DblEj\Application\IApplication $application);

    /**
     * Get all available resources in the application.
     *
     * @return \DblEj\Resources\ResourceCollection
     */
    public function GetResources();

    /**
     * @return \DblEj\Resources\ResourcePermissionCollection Get all permissions for all resources in the application.
     */
    public function GetResourcePermissions();
}