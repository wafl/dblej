<?php
namespace DblEj\Extension;

abstract class ExtensionControllerBase
extends \DblEj\Mvc\ControllerBase
{
    protected $_extension = null;
    public function __construct(ExtensionBase $extension)
    {
        parent::__construct();
        $this->_extension = $extension;
    }
    protected function createResponseFromRequest(\DblEj\Communication\Http\Request $request, \DblEj\Application\IWebApplication $app, \DblEj\Data\IModel $dataModel = null, \DblEj\Presentation\RenderOptions $options = null, \DblEj\Communication\Http\Headers $headers = null)
    {
        if (substr($request->Get_RequestUrl(), strlen($request->Get_RequestUrl()) - 1) == "/")
        {
            $request->Set_RequestUrl($request->Get_RequestUrl()."Home");
        }

        $extension = $this->Get_Extension();
        $extensionClassName = get_class($extension);
        $extensionSettings = [];
        foreach ($extensionClassName::Get_AvailableSettings() as $extenionSettingName)
        {
            $extensionSettings[$extenionSettingName] = $extension->GetSettingValue($extenionSettingName);
        }
        $dataModel->SetFieldValue("EXTENSION_SETTINGS", $extensionSettings);
        return parent::createResponseFromRequest($request, $app, $dataModel, $options, $headers);
    }

    /**
     * @return \DblEj\Extension\ExtensionBase The extension this controller belongs to
     */
    final public function Get_Extension()
    {
        return $this->_extension;
    }
}