<?php
namespace DblEj\Extension;

/**
 * An exception base class for exceptions thrown (and not handled) by extensions.
 */
class ExtensionException
extends \DblEj\System\Exception
{

    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("$message", $severity, $inner);
    }
}