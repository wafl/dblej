<?php

namespace DblEj\Extension;

/**
 * Represents a widget in a modular web application.
 */
interface IWidget
{

    public function Get_WidgetId();

    public function Get_Title();

    public function Get_View();
}