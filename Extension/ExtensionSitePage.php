<?php

namespace DblEj\Extension;

/**
 * Describes an extension's web page.
 */
class ExtensionSitePage
{
    private $_title;
    private $_templateName;
    private $_description;
    private $_stylesheets;
    private $_scripts;
    private $_displayOrder;

    public function __construct($title, $description, $templateName, $stylesheets = array(), $scripts = array(), $displayOrder = 999)
    {
        $this->_title        = $title;
        $this->_templateName = $templateName;
        $this->_description  = $description;
        $this->_stylesheets  = $stylesheets;
        $this->_scripts      = $scripts;
        $this->_displayOrder = $displayOrder;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Get_DisplayOrder()
    {
        return $this->_displayOrder;
    }

    public function Get_TemplateName()
    {
        return $this->_templateName;
    }

    public function Set_Template($templateName)
    {
        $this->_templateName = $templateName;
    }

    public function Get_Description()
    {
        return $this->_description;
    }

    public function Set_Description($description)
    {
        $this->_description = $description;
    }

    public function Get_Stylesheets()
    {
        return $this->_stylesheets;
    }

    public function Get_Scripts()
    {
        return $this->_scripts;
    }
}