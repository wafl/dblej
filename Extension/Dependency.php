<?php

namespace DblEj\Extension;

/**
 * Defines an Extension's or Control's dependency on another Control or Extension.
 *
 * @since 668
 */
class Dependency
{
    const TYPE_EXTENSION = "EXTENSION";
    const TYPE_CONTROL   = "CONTROL";
    const TYPE_EXTENSION_INTERFACE = "EXTENSION_INTERFACE";

    private $_dependant;
    private $_dependantType;
    private $_dependsOn;
    private $_dependsOnType;
    private $_dependencyId;

    /**
     * Create an instance of Dependency.
     *
     * @param string $dependant The name of the dependant control or extension.
     * @param string $dependantType Is the dependant an extension or a control? use the constants: TYPE_EXTENSION or TYPE_CONTROL.
     * @param type $dependsOn The name of the control or extension that the dependant depends on.
     * @param string $dependsOnType Is the $dependsOn an extension or a control? use the constants: TYPE_EXTENSION or TYPE_CONTROL.
     */
    public function __construct($dependant, $dependantType, $dependsOn, $dependsOnType)
    {
        $this->_dependant     = $dependant;
        $this->_dependantType = $dependantType;
        $this->_dependsOn     = $dependsOn;
        $this->_dependsOnType = $dependsOnType;
        $this->_dependencyId  = \DblEj\Util\Strings::GenerateRandomString(16);
    }

    /**
     * A unique string that identifies this dependency.
     *
     * @return string
     */
    public function Get_DependencyId()
    {
        return $this->_dependencyId;
    }

    /**
     * The type of the the item being depended on.  Use one of: TYPE_EXTENSION or TYPE_CONTROL.
     *
     * @return string
     */
    public function Get_DependsOnType()
    {
        return $this->_dependsOnType;
    }

    /**
     * The name of the control or extension that the dependant depends on.
     * @return string
     */
    public function Get_DependsOn()
    {
        return $this->_dependsOn;
    }

    /**
     * The name of the dependant Control or Extension.
     *
     * @return string
     */
    public function Get_Dependant()
    {
        return $this->_dependant;
    }
}