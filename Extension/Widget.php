<?php
namespace DblEj\Extension;

/**
 * A widget in a modular web application.
 */
class Widget
implements IWidget
{
    private $_widgetId;

    /**
     *
     * @var \DblEj\Mvc\IView
     */
    private $_view;
    private $_title;

    public function __construct($widgetId, $title, \DblEj\Mvc\IView $view)
    {
        $this->_widgetId = $widgetId;
        $this->_title    = $title;
        $this->_view     = $view;
    }

    public function Get_ResourceId()
    {
        return $this->_widgetId;
    }

    public function Get_ResourceType()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_MODULE_WIDGET;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Get_View()
    {
        return $this->_view;
    }

    public function Get_WidgetId()
    {
        return $this->_widgetId;
    }
}