<?php

namespace DblEj\Extension;

/**
 * Collection of IModule objects.
 */
class ModuleCollection
extends \DblEj\Collections\TypedKeyedCollection
{

    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray, "\DblEj\Extension\IModule");
    }

    /**
     * Get the Module that has the specified id.
     * @param int $moduleId
     * @return \DblEj\Extension\IModule
     */
    public function GetModule($moduleId)
    {
        return $this->GetItem($moduleId);
    }

    public function GetModules()
    {
        return $this->GetItems();
    }

    /**
     * Add a new Module
     * @param \DblEj\Extension\IModule $module
     */
    public function AddModule(\DblEj\Extension\IModule $module)
    {
        return $this->AddItem($module, $module->Get_ModuleId());
    }

    public function GetScreen($moduleScreenId)
    {
        $returnScreen = null;
        foreach ($this->_itemArray as $module)
        {
            $screen = $module->GetScreen($moduleScreenId);
            if ($screen)
            {
                $returnScreen = $screen;
                break;
            }
        }
        return $returnScreen;
    }
}