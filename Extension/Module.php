<?php

namespace DblEj\Extension;

/**
 * An application module which can contain screens and widgets.
 */
class Module
implements IModule
{
    private $_screens;
    private $_widgets;
    private $_resourceId;
    private $_title;

    public function __construct($moduleId, $title)
    {
        $this->_resourceId = $moduleId;
        $this->_title      = $title;
        $this->_screens    = array();
        $this->_widgets    = array();
        $this->_apiCalls    = array();
    }

    public function AddScreen(IScreen $screen)
    {
        $this->_screens[$screen->Get_ScreenId()] = $screen;
    }

    public function AddApiCall(IApiCall $apiCall)
    {
        $this->_apiCalls[$apiCall->Get_ApiCallId()] = $apiCall;
    }

    public function AddWidget(IWidget $widget)
    {
        $this->_widgets[$widget->Get_WidgetId()] = $widget;
    }

    public function Get_ResourceType()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_MODULE;
    }

    public function GetScreens()
    {
        return $this->_screens;
    }

    public function GetApiCalls()
    {
        return $this->_apiCalls;
    }

    public function GetScreen($screenId)
    {
        if (isset($this->_screens[$screenId]))
        {
            return $this->_screens[$screenId];
        }
        else
        {
            return false;
        }
    }

    public function GetApiCall($apiCallId)
    {
        if (isset($this->_apiCalls[$apiCallId]))
        {
            return $this->_apiCalls[$apiCallId];
        }
        else
        {
            return false;
        }
    }

    public function GetWidgets()
    {
        return $this->_widgets;
    }

    public function GetAllSitePages()
    {
        $returnArray = array();
        foreach ($this->_screens as $screen)
        {
            $sitePage = $screen->Get_SitePage();
            if (!isset($returnArray[$sitePage->Get_PageId()]))
            {
                $returnArray[$sitePage->Get_PageId()] = $sitePage;
            }
        }
        return $returnArray;
    }

    public function GetAllSiteAreas()
    {
        $returnArray = array();
        foreach ($this->_screens as $screen)
        {
            $siteArea = $screen->Get_SiteArea();
            if (!isset($returnArray[$siteArea->Get_areaId()]))
            {
                $returnArray[$siteArea->Get_areaId()] = $siteArea;
            }
        }
        return $returnArray;
    }

    public function Get_ResourceId()
    {
        return $this->_resourceId;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Get_ModuleId()
    {
        return $this->_resourceId;
    }
}