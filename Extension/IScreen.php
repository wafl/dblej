<?php

namespace DblEj\Extension;

/**
 * Represents a screen in a modular web application.
 */
interface IScreen
{

    public function Get_ScreenId();

    public function Get_Title();

    public function Get_SitePage();
}