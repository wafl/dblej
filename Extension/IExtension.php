<?php

namespace DblEj\Extension;

interface IExtension
{

    public function Configure($settingName, $settingValue);

    public function GetSettingDefault($settingName);

    public function Initialize(\DblEj\Application\IApplication $app);

    public function PrepareSitePage($pageName);

    public function Shutdown();

    public function Get_RequiresInstallation();

    public function Get_SiteAreaId();

    public function Set_SiteAreaId($siteAreaId);

    public static function GetLocalizedText($textName);

    public static function InstallData(\DblEj\Data\Integration\IDatabaseServer $storageEngine);

    public static function Get_AvailableSettings();

    public static function Get_DatabaseInstallScripts();

    public static function Get_DatabaseInstalledTables();

    public static function Get_Dependencies();

    public static function Get_GlobalScripts();

    public static function Get_GlobalStylesheets();

    public static function Get_SitePages();

    public static function Get_TablePrefix();

    public static function Set_LanguageFileClassname($langFileClassname);

    public static function TranslateUrl(\DblEj\Communication\Http\Request $request);

    public static function Get_WebOnly();
}