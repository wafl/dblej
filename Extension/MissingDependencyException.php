<?php

namespace DblEj\Extension;

class MissingDependencyException
extends ExtensionException
{
    function __construct(Dependency $dependency, $severity = E_ERROR)
    {
        parent::__construct("Misisng plugin dependency (" . $dependency->Get_DependsOn() . ") for " . $dependency->Get_Dependant(), $severity, null);
    }
}