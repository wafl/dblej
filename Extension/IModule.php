<?php

namespace DblEj\Extension;

/**
 * Provides methods for exposing a module's components.
 */
interface IModule
{

    public function Get_ModuleId();

    /**
     * Get all screens that are contained by this module.
     */
    public function GetScreens();

    /**
     * Get all api calls that are contained by this module.
     */
    public function GetApiCalls();

    /**
     * Get all widgets that are contained by this module.
     */
    public function GetWidgets();
}