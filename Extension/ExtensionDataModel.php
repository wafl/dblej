<?php

namespace DblEj\Extension;

abstract class ExtensionDataModel
extends \DblEj\Data\PersistableModel
{
    public static function Set_Extension($extension)
    {
        throw new \Exception("Extension data models must override the Set_Extension static method (".get_called_class().": ". get_class($extension).")");
    }
}