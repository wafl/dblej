<?php

namespace DblEj\Extension;

/**
 * Represents an API call in a modular web application.
 */
interface IApiCall
extends \DblEj\Resources\IResource
{

    public function Get_ApiCallId();

    public function Get_Title();
}