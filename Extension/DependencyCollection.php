<?php
namespace DblEj\Extension;

/**
 * Collection of Dependency objects.
 */
class DependencyCollection
extends \DblEj\Collections\TypedKeyedCollection
{
    private static $_emptyDependencyCollection;

    public static function Get_EmptyCollection()
    {
        if (!self::$_emptyDependencyCollection)
        {
            self::$_emptyDependencyCollection = new DependencyCollection();
        }
        return self::$_emptyDependencyCollection;
    }

    /**
     * Create a collection of Dependencies using an array of Dependencies for the initial collection values.
     * @param array $collectionArray Dependencies to initialize the collection with.
     */
    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray, "\DblEj\Extension\Dependency");
    }

    /**
     * Get the dependency that has the specified id.
     * @param string $dependencyId
     * @return \DblEj\Extension\Dependency
     */
    public function GetDepenency($dependencyId)
    {
        return $this->GetItem($dependencyId);
    }

    /**
     * Create and add a new dependency.
     *
     * @param string $dependant The name of the dependant control or extension.
     * @param string $dependantType Is the dependant an extension or a control? use the constants: TYPE_EXTENSION or TYPE_CONTROL.
     * @param string $dependsOn The name of the control or extension that the dependant depends on.
     * @param string $dependsOnType Is the dependson an extension or a control? use the constants: TYPE_EXTENSION or TYPE_CONTROL.
     */
    public function AddDependency($dependant, $dependantType, $dependsOn, $dependsOnType)
    {
        $dependency = new \DblEj\Extension\Dependency($dependant, $dependantType, $dependsOn, $dependsOnType);
        return $this->AddDependencyObject($dependency);
    }

    /**
     * Add a dependency to the collection.
     *
     * @param \DblEj\Extension\Dependency $dependency
     * @return DependencyCollection <i>$this</i> for chained method calls.
     */
    public function AddDependencyObject(\DblEj\Extension\Dependency $dependency)
    {
        return $this->AddItem($dependency, $dependency->Get_DependencyId());
    }

    /**
     * Remove a dependency from the collection.
     *
     * @param \DblEj\Extension\Dependency $dependency
     *
     * @return DependencyCollection <i>$this</i> for chained method calls.
     */
    public function RemoveDependency(\DblEj\Extension\Dependency $dependency)
    {
        return $this->RemoveItem($dependency);
    }

    /**
     * Remove a dependency from the collection based on the specified id.
     *
     * @param string $dependencyId
     *
     * @return DependencyCollection <i>$this</i> for chained method calls.
     */
    public function RemoveDependencyById($dependencyId)
    {
        return $this->RemoveItemByKey($dependencyId);
    }
}