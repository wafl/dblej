<?php
namespace DblEj\Extension;

use DblEj\EventHandling\EventRaiser;
use InvalidArgumentException;

/**
 * Base class for DblEj extensions.
 */
abstract class ExtensionBase
extends EventRaiser
implements IExtension
{
    private $_version;
    private $_siteAreaId;
    private static $_langFileClassname;

    // <editor-fold defaultstate="collapsed" desc="Final Properties ">

    /**
     * The id of the top level SiteArea that this extension's SitePages
     * should be mapped to.
     *
     * @param string $siteAreaId The id of the SiteArea.
     */
    final public function Set_SiteAreaId($siteAreaId)
    {
        $this->_siteAreaId = $siteAreaId;
    }

    /**
     * The id of the top level SiteArea that this collection's SitePages
     * should be mapped to.
     *
     * @return string The id of the SiteArea.
     */
    final public function Get_SiteAreaId()
    {
        return $this->_siteAreaId;
    }

    /**
     * The extension's versiob
     * @return string
     */
    final public function Get_Version()
    {
        return $this->_version;
    }

    final public static function Set_LanguageFileClassname($langFileClassname)
    {
        self::$_langFileClassname = $langFileClassname;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Final Public Instance Methods: Configure ">

    /**
     * Called by consumers to specify values for this extension's exposed settings.
     *
     * Sub-classes can not override this method to get setting values.
     * Instead they should override ConfirmedConfigure, which is called from
     * within this function if a setting is deeemed valid.
     *
     * @param string $settingName
     * @param string|int|float $settingValue
     * @throws \Exception
     */
    final public function Configure($settingName, $settingValue)
    {
        if (strtolower($settingName) != "version")
        {
            if (array_search($settingName, self::Get_AvailableSettings()) === false)
            {
                $availString = implode(",", self::Get_AvailableSettings());
                $subclass = get_called_class();
                throw new \Exception("Invalid extension setting: ($subclass: $settingName). Available settings: $availString");
            }
            $this->_confirmedConfigure($settingName, $settingValue);
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Final Public Static Methods: InstallData, GetLocalizedText ">

    final public static function InstallData(\DblEj\Data\Integration\IDatabaseServer $storageEngine)
    {
        if (!$storageEngine)
        {
            throw new InvalidArgumentException("To install an extension, you must provide a storage engine");
        }
        $subclass       = get_called_class();
        $installScripts = $subclass::Get_DatabaseInstallScripts();
        foreach ($installScripts as $installScript)
        {
            if (file_exists($installScript))
            {
                $sql = file_get_contents($installScript);
                $sql = str_replace("{\$PREFIX}", $subclass::Get_TablePrefix(), $sql);
                $storageEngine->DirectScriptExecute($sql, true);
            }
            else
            {
                throw new \Exception("Extension ($subclass) has specified a sql install script which does not exist ($installScript)");
            }
        }
    }

    final public static function GetLocalizedText($textName)
    {
        $langClassName = self::$_langFileClassname;
        return $langClassName::GetText($textName);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Virtual Instance Methods ">

    /**
     * Called when a configuration is set if it is confirmed to be a valid configuration setting.
     *
     * Override this method to read the values that the application is setting
     * for this extension's configurations.
     */
    protected function ConfirmedConfigure($settingName, $settingValue)
    {
    }

    private function _confirmedConfigure($settingName, $settingValue)
    {
        if ($settingName == "SiteAreaId")
        {
            $this->_siteAreaId = $settingValue;
        } else {
            $this->ConfirmedConfigure($settingName, $settingValue);
        }
    }

    protected function getLocalSettingValue($settingName)
    {

    }

    /**
     * Return the default value for the specified setting.
     * @param string $settingName
     * @return string|int|float
     */
    public function GetSettingDefault($settingName)
    {
        return null;
    }

    final public function GetSettingValue($settingName)
    {
        if ($settingName == "SiteAreaId")
        {
            return $this->_siteAreaId;
        } else {
            return $this->getLocalSettingValue($settingName);
        }
    }

    /**
     * Called by the application before a SitePage is rendered and displayed.
     *
     * @param string $pageName
     * @return \DblEj\Extension\ExtensionBase
     */
    public function PrepareSitePage($pageName)
    {
        return $this;
    }

    public static function GetSitePage($pageName)
    {
        return null;
    }

    /**
     * Return <i>true</i> if the extension needs to be "installed" the first
     * time it is run, in which case the script returned by Get_DatabaseInstallScripts
     * will be executed.
     *
     * @return boolean
     */
    public function Get_RequiresInstallation()
    {
        return false;
    }

    /**
     * Override this method and provide an array of the events that this extension raises.
     * @return type
     */
    public function GetRaisedEventTypes()
    {
        return array();
    }

    /**
     * Called before shutting down the extension.
     */
    public function Shutdown()
    {

    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Virtual Static Methods ">
    final public static function Get_AvailableSettings()
    {
        $subclass = get_called_class();
        $availSettings = $subclass::getAvailableSettings();
        $availSettings[] = "SiteAreaId";
        return $availSettings;
    }

    protected static function getAvailableSettings()
    {
        return [];
    }

    public static function Get_DatabaseInstallScripts()
    {
        return array();
    }

    /**
     * Return an array of the tables that this extension installs.
     *
     * @return array
     */
    public static function Get_DatabaseInstalledTables()
    {
        return array();
    }

    /**
     * Return a DependencyCollection of this extensions dependencies.
     *
     * @return DependencyCollection
     */
    public static function Get_Dependencies()
    {
        return DependencyCollection::Get_EmptyCollection();
    }

    /**
     * The paths to any javascripts loaded by this extension for every page.
     * @return array
     */
    public static function Get_GlobalScripts()
    {
        return array();
    }

    /**
     * The paths to any stylesheets loaded by this extension for every page.
     * @return array
     */
    public static function Get_GlobalStylesheets()
    {
        return array();
    }

    /**
     * An array of SitePages provided by this extension.
     *
     * @return array
     */
    public static function Get_SitePages()
    {
        return array();
    }

    /**
     * The prefix on the names of the database tables created by this extension.
     *
     * @return null
     */
    public static function Get_TablePrefix()
    {
        return null;
    }

    public static function TranslateUrl(\DblEj\Communication\Http\Request $request)
    {
        return $request;
    }

    public static function Get_WebOnly()
    {
        return false;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Magic Method Overrides ">

    function __destruct()
    {
        $this->Shutdown();
    }
    // </editor-fold>
}