<?php

namespace DblEj\Extension;

interface IControl
{

    public function Initialize($width, $height, &$params);

    public function GetHtml();

    public function SetTemplateVariable($variableName, $value);

    public function GetRequiredParams();

    public function GetOptionalParams();

    public function GetParamDefaultValue($paramName);

    public static function Get_Name();

    public static function Get_Dependencies();

    public static function Get_MainTemplate();

    public static function Get_MainStylesheet();

    public static function Get_Javascripts();
}