<?php

namespace DblEj\Extension;

abstract class ModularResource
implements \DblEj\AccessControl\IResource
{
    private $_moduleId;

    public function __construct($moduleId)
    {
        $this->_moduleId = $moduleId;
    }
    public function Get_ResourceId()
    {
        return $this->Get_Id();
    }
    public function Get_Uid()
    {
        return $this->Get_ResourceType()."-".$this->Get_ResourceId();
    }
    public function Get_ModuleId()
    {
        return $this->_moduleId;
    }
}