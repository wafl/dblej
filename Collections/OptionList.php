<?php

namespace DblEj\Collections;

/**
 * A collection of <i>Options</i>.
 */
class OptionList
extends \DblEj\Collections\KeyedCollection
{

    /**
     * Converts an associative array of value into an associative array of <i>Option</i>s
     *
     * @param array $associativeArray
     * The contents of this array will be mapped onto the new array,
     * where each element in the new array is an <i>Option</i> that has the same name
     * as the array element's key and the same value as the array element.
     *
     * @return \DblEj\Configuration\OptionList
     * The new instance of OptionList.
     */
    public static function CreateFromArray(array $associativeArray = null)
    {
        $optionArray = array();
        foreach ($associativeArray as $key => $val)
        {
            $option            = new Option($key);
            $option->Set_CurrentValue($val);
            $optionArray[$key] = $option;
        }
        return $optionArray;
    }

    /**
     * Create an instance of an OptionList,
     * optionally initializing it from an array of Options.
     *
     * @param array $arrayOfOptions
     * The array that will be used to initialized the items in the <i>OptionList</i>.
     * Each element in the array must be an <i>Option</i>, and will be added to the new <i>OptionList</i>.
     * @throws \Exception
     */
    public function __construct(array $arrayOfOptions = null)
    {
        $associativeArray = array();
        if ($arrayOfOptions)
        {
            foreach ($arrayOfOptions as $option)
            {
                if (!is_a($option, "\\DblEj\\Configuration\\Option"))
                {
                    throw new \Exception("Every element of the arrayOfOptions must be an option object");
                }
            }
            foreach ($arrayOfOptions as $option)
            {
                $associativeArray[$option->Get_OptionName()] = $option;
            }
        }


        parent::__construct($associativeArray);
    }

    /**
     * Set the value of an option.
     * If the option doesn't exist in the collection, it will be added.
     *
     * @param string $optionName The name of the option to set.
     * @param type $optionValue The new value for the option.
     * @throws InvalidOptionException Thrown if the passed <i>$optionName</i> is blank or all whitespace.
     */
    public function SetOption($optionName, $optionValue)
    {
        if (trim($optionName) != "")
        {
            if (!$this->GetIsOption($optionName))
            {
                $this->AddOption($optionName);
            }
            $option = $this->GetOption($optionName);
            $option->Set_CurrentValue($optionValue);
            $this->SetItem($option, $optionName);
        }
        else
        {
            throw new InvalidOptionException($optionName);
        }
    }

    /**
     * Get the specified option from the collection.
     *
     * @param string $optionName The name of the option to retrieve.
     * @return \DblEj\Configuration\Option The option that matches the passed <i>$optionName</i>, or <i>null</i> if no options matched.
     */
    public function GetOption($optionName)
    {
        return $this->GetItem($optionName);
    }

    /**
     * Get just the value of the specified option.
     *
     * @param string $optionName The name of the option to get the value for.
     * @return mixed The value of the option.
     * @throws InvalidOptionException Thrown if the option isn't found or is not a valid object.
     */
    public function GetOptionValue($optionName)
    {
        if (is_object($this->GetOption($optionName)))
        {
            return $this->GetOption($optionName)->Get_CurrentValue();
        }
        else
        {
            throw new InvalidOptionException($optionName);
        }
    }

    /**
     * Add an option to the collection.
     *
     * @param string $optionName The name of the option to set.
     * @param mixed $defaultValue The value for the option when no value has been set.
     *
     * @return \DblEj\Collections\Option Thew new option.
     */
    public function AddOption($optionName, $defaultValue = null)
    {
        if (!$this->GetIsOption($optionName))
        {
            $newoption = new Option($optionName, $defaultValue);
            $this->AddItem($newoption, $optionName);
        }
        else
        {
            $newoption = $this->GetOption($optionName);
        }
        return $newoption;
    }

    /**
     * Check if the passed string matches the name of any of the <i>Option</i>s in the colelction.
     *
     * @param string $optionName The name of the option to search for.
     * @return boolean <i>True</i> if the option exists in this collection, otherwise </i>false</i>.
     */
    public function GetIsOption($optionName)
    {
        $isOption = $this->ContainsKey($optionName);
        return $isOption;
    }
}