<?php

namespace DblEj\Collections;

/**
 * An option that would typically be used as part of a configuration.
 */
class Option
{
    private $_optionName;
    private $_currentValue;
    private $_defaultValue;

    /**
     * Instantiate an Option object.
     *
     * @param string $optionName The name of the option.
     * @param mixed $defaultValue The value that the option has when no value has been set for it, or when the value is set to <i>null</i>.
     * @param mixed $currentValue The option's current value.
     */
    public function __construct($optionName, $defaultValue = null, $currentValue = null)
    {
        $this->_optionName   = $optionName;
        $this->_currentValue = $currentValue;
        $this->_defaultValue = $defaultValue;
    }

    /**
     * The name of the option.
     * @return string
     */
    public function Get_OptionName()
    {
        return $this->_optionName;
    }

    /**
     * The current value of the option, or, if there is no current value, the default value.
     * @return mixed
     */
    public function Get_CurrentValue()
    {
        return $this->_currentValue !== null ? $this->_currentValue : $this->_defaultValue;
    }

    /**
     * @param mixed $value
     * Set the current value of this option.
     *
     * @return \DblEj\Configuration\Option
     * Returns <i>$this</i> so that property/method calls to this object can be chained.
     */
    public function Set_CurrentValue($value)
    {
        $this->_currentValue = $value;
        return $this;
    }

    /**
     * The default value of this option to be used when there is no current value.
     * @return mixed
     */
    public function Get_DefaultValue()
    {
        return $this->_defaultValue;
    }
}