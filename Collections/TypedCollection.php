<?php
namespace DblEj\Collections;

/**
 * A strongly-typed collection of items.
 *
 * An item's type will be evaluated whenever it is added to the collection
 * and an InvalidDataTypeException will be thrown if the type is not correct.
 *
 * When performing merge commands, the other collection must be strongly typed
 * and be of the same type.
 */
class TypedCollection
extends Collection
implements ICollection,
           ITypedCollection
{
    private $_typeName;

    /**
     * Instantiate a TypedCollection, optionally initializing it with an array.
     *
     * @param array $collectionArray
     * An array of items to initialize the collection with.
     * The items must all be of the type specified in $typeName.
     *
     * @param string $typeName
     * The fully qualified name of the data type that each item in this collection must be.
     *
     * @throws \DblEj\System\InvalidDataTypeException
     * If any item in the <i>collectionArray</i> is not of the <i>typeName</i>,
     * this Exception will be thrown.
     */
    public function __construct(array $collectionArray = null, $typeName = "\DblEj\System\Object\BaseObject")
    {
        $this->_typeName = $typeName;
        if ($collectionArray == null || self::DoesCollectionHaveSingleType($collectionArray))
        {
            parent::__construct($collectionArray);
        }
        else
        {
            throw new \DblEj\System\InvalidDataTypeException("TypedCollection with one or more invalid items", "typedCollection of $this->_typeName");
        }
    }

    /**
     * Add an item to the collection.
     *
     * @param mixed $item The item to add to the collection.
     */
    public function AddItem($item)
    {
        $this->_throwExceptionOnInvalidItemType($item);
        parent::AddItem($item);
    }

    /**
     * Merge this another collection's items into this collection.
     *
     * @param \DblEj\Collections\ICollection $collection The collection who's items will be merged into this collection's items.
     *
     * @return \DblEj\Collections\Collection This collection, now merged with the passed collection; useful for chained method calls.
     */
    public function MergeWith(ICollection $collection)
    {
        if ($collection === null)
        {
            throw new \DblEj\System\InvalidArgumentException("collection", "collection cannot be null");
        }
        if ($this->DoesCollectionMatchType($collection))
        {
            return parent::MergeWith($collection);
        }
        else
        {
            throw new \DblEj\System\InvalidDataTypeException("TypedCollection of $this->_typeName with one or more invalid items");
        }
    }

    /**
     * Check if the passed collection's items are all of the same data type as this collection's item type.
     *
     * @param \DblEj\Collections\DblEj\Collections\ICollection $items A collection of items to test for type compliance with this collection.
     * @return boolean
     * Returns true if the passed collection is typed the same as this collection.
     * That is, if all of it's items are of the same type as this collection's item type.
     * A collection can pass this test even if it is not a TypedCollection.
     */
    public function DoesCollectionMatchType(DblEj\Collections\ICollection $items)
    {
        $returnValue = true;
        foreach ($items->GetItems() as $item)
        {
            if (!is_a($item, $this->_typeName))
            {
                $returnValue = false;
                break;
            }
        }
        return $returnValue;
    }

    /**
     * Check if the passed array's elements are all of the same data type as this collection's item type.
     *
     * @param array $items The array to check.
     * @return boolean
     * Returns true if the passed array's elements are all
     * of the same type as this collection's item type.
     */
    public function DoesArrayMatchType(array $items)
    {
        if ($items === null)
        {
            throw new \DblEj\System\InvalidArgumentException("items", "items cannot be null");
        }
        $returnValue = true;
        foreach ($items as $item)
        {
            if (!is_a($item, $this->_typeName))
            {
                $returnValue = false;
                break;
            }
        }
        return $returnValue;
    }

    /**
     * Check if the passed collection has items that are all of the same type.
     *
     * @return boolean <i>True</i> if all items are of the same type, otherwise <i>false</i>.
     */
    public static function DoesCollectionHaveSingleType($items)
    {
        $returnValue = true;
        $firstType   = null;
        foreach ($items as $item)
        {
            if ($firstType === null)
            {
                $firstType = get_class($item);
            }
            if (!is_a($item, $firstType))
            {
                $returnValue = false;
                break;
            }
        }
        return $returnValue;
    }

    private function _throwExceptionOnInvalidItemType($item)
    {
        if (!is_a($item, $this->_typeName))
        {
            throw new \DblEj\System\InvalidDataTypeException(get_class($item), $this->_typeName);
        }
    }
}