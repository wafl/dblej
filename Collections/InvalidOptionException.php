<?php

namespace DblEj\Collections;

/**
 * Thrown when trying to access an option that doesn't exist in the <i>OptionList</i>.
 */
class InvalidOptionException
extends \Exception
{

    /**
     * Create an instance of an InvalidOptionException.
     *
     * @param string $optionName The name of the option.
     * @param string $reason An explanation of why it's invalid.
     * @param integer $severity The severity of the exception.
     */
    public function __construct($optionName, $reason = null, $severity = E_ERROR)
    {
        $message = "Invalid option: $optionName";
        if ($reason)
        {
            $message .= "  $reason";
        }
        parent::__construct($message, $severity, null);
    }
}