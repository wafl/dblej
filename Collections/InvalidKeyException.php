<?php

namespace DblEj\Collections;

/**
 * Exception that is thrown when an attempt is made to access a key (or an item stored under a key)
 * in a keyed collection that does not contain the specified key.
 */
class InvalidKeyException
extends \BadFunctionCallException
{

    /**
     * Construct the exception.
     *
     * @param string $key The key that was used that triggered this exception.
     * @param integer $severity
     */
    public function __construct($key, $severity = E_ERROR)
    {
        parent::__construct("Invalid key: $key", $severity, null);
    }
}