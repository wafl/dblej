<?php

namespace DblEj\Collections;

/**
 * An interface that can be used to implement classes that store a collection of data.
 *
 * @tags Collections
 */
interface ICollection
extends \IteratorAggregate
{

    /**
     * Get all items from the collection as an array.
     *
     * @return array The internal array of items in the collection.
     */
    public function GetItems();

    /**
     * Get the collection item at the specified index.
     *
     * @param integer $itemIndex
     * @return mixed The collection item found at the specified index
     */
    public function GetItem($itemIndex);

    /**
     * Count the number of items in the collection.
     *
     * @return integer The number of items in the collection.
     */
    public function GetItemCount();

    /**
     * Check if the specified <em>item</em> is a part of this collection.
     *
     * @param mixed $item The item to check.
     *
     * @return integer The index of the item if it is a part of this collection,
     * or false if the item is not a part of this collection.
     */
    public function Contains($item);

    public function __toArray();

    public function __toString();
}