<?php
namespace DblEj\Collections;

/**
 * An interface that can be used to implement classes that store a keyed collection of data.
 *
 * @tags Collections, Key/Val, Hash Map, Hash Table
 */
interface IKeyedCollection
extends ICollection, \DblEj\Data\IFieldPublisher
{

    /**
     * Add an item to this collection.
     *
     * @param mixed $item The item to add.
     * @param string $keyValue The unique key to store this item under.
     */
    public function AddItem($item, $keyValue);

    /**
     * Remove The item stored under the specified key.
     *
     * @param string $keyValue The key used to find the item.
     */
    public function RemoveItemByKey($keyValue);

    /**
     * Merge this keyed collection with another keyed collection.
     *
     * @param \DblEj\Collections\IKeyedCollection $keyedCollection The keyed collection to merge with this one.
     */
    public function MergeWith(IKeyedCollection $keyedCollection);

    /**
     * Merge this keyed collection's items with another keyed collection's items,
     * preserving the keys, and return the resulting keyed collection.
     * If an item exists in both keyed collections under the same key, only
     * one of them will survive the merge.
     *
     * @param \DblEj\Collections\IKeyedCollection $keyedCollection
     * The keyed collection who's items will be merged with this collection's items.
     *
     * @return \DblEj\Collections\Collection
     * A new collection that is the result of the merge.
     */
    public function GetMergedWith(IKeyedCollection $keyedCollection);

    /**
     * Check if there is an item in this collection stored under the specified key.
     *
     * @param string $itemKey The key to check.
     */
    public function ContainsKey($itemKey);
}