<?php
namespace DblEj\Collections;

/**
 * A Collection that raises events when items are added or removed
 * @tags Collection, Event Raisers
 */
class CollectionEventRaiser
extends \DblEj\EventHandling\EventRaiser
implements ICollection
{
    const ITEM_ADDED   = 1001;
    const ITEM_REMOVED = 1002;

    /**
     * The internal array of items
     * @var array
     */
    protected $_itemArray = array();

    /**
     * Collection constructor.
     * Pass in an array to initialize the collection with the array.
     * Each array element will become a collection item.
     *
     * @param array $collectionArray
     */
    public function __construct(array $collectionArray = null)
    {
        parent::__construct();
        $this->_itemArray = array();
        if ($collectionArray)
        {
            $itemIdx = 0;
            foreach ($collectionArray as $item)
            {
                $this->_itemArray[$itemIdx] = $item;
                $itemIdx++;
            }
        }
    }

    /**
     * Add an item to the collection.
     *
     * @assert ("StringItem") == $this->object
     * @param mixed $item
     */
    public function AddItem($item)
    {
        $this->_itemArray[] = $item;
        $this->onItemAdded($item);
    }

    /**
     * Get the collection item at the specified index.
     *
     * @param integer $itemIndex
     * @return mixed The collection item found at the specified index
     */
    public function GetItem($itemIndex)
    {
        return $this->_itemArray[$itemIndex];
    }

    /**
     * Count the number of items in the collection.
     *
     * @return integer The number of items in the collection.
     */
    public function GetItemCount()
    {
        return count($this->_itemArray);
    }

    /**
     * Get all items from the collection as an array.
     *
     * @return array The internal array of items in the collection.
     */
    public function GetItems()
    {
        return $this->_itemArray;
    }

    /**
     * Remove the specified collection item.
     *
     * @param integer $item
     * @return Collection Returns <em>this</em> to allow for chained method calls.
     */
    public function RemoveItem($item)
    {
        $itemIndex = array_search($this->_itemArray, $item);
        array_splice($this->_itemArray, $itemIndex, 1);
        $this->onItemRemoved($item);
    }

    /**
     * Merge this collection's items with another collection's items and return the resulting collection.
     *
     * @param \DblEj\Collections\ICollection $collection The collection who's items will be merged with this collection's items.
     *
     * @return \DblEj\Collections\Collection A new collection that is the result of the merge.
     */
    public function GetMergedWith(ICollection $collection)
    {
        $newCollection = new Collection();
        foreach ($this->GetItems() as $item)
        {
            $newCollection->AddItem($item);
        }
        foreach ($collection as $item)
        {
            $newCollection->AddItem($item);
        }
        return $newCollection;
    }

    /**
     * Check if the specified <em>item</em> is a part of this collection.
     *
     * @param mixed $item The item to check.
     *
     * @return integer The index of the item if it is a part of this collection,
     * or false if the item is not a part of this collection.
     */
    public function Contains($item)
    {
        return array_search($item, $this->_itemArray) !== false;
    }

    /**
     * Get the types of events that this EventRaiser raises.
     *
     * @return \DblEj\EventHandling\EventTypeCollection
     */
    public function GetRaisedEventTypes()
    {
        return new \DblEj\EventHandling\EventTypeCollection
        (
            array
            (
                "ITEM_ADDED"   => self::ITEM_ADDED,
                "ITEM_REMOVED" => self::ITEM_REMOVED
            )
        );
    }

    /**
     * Called internally whenever an item is added to the internal array.
     * This will fire the ITEM_ADDED event.
     *
     * @param mixed $item The item that was added to the collection.
     */
    protected function onItemAdded($item)
    {
        $this->raiseEvent(new \DblEj\EventHandling\EventInfo(self::ITEM_ADDED, $this, $item));
    }

    /**
     * Called internally whenever an item is deleted from the internal array.
     * This will fire the ITEM_REMOVED event.
     *
     * @param mixed $item The item that was removed from the collection.
     */
    protected function onItemRemoved($item)
    {
        $this->raiseEvent(new \DblEj\EventHandling\EventInfo(self::ITEM_REMOVED, $this, $item));
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->_itemArray);
    }

    public function __toArray()
    {
        return $this->_itemArray;
    }

    public function __toString()
    {
        return "Collection (" . $this->GetItemCount() . " Items)";
    }
}