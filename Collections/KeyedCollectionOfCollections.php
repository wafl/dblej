<?php
namespace DblEj\Collections;

/**
 * Stores, and provides access to, a collection of collections, where each collection is stored under a unique key identifier.
 *
 * @tags Collections
 */
class KeyedCollectionOfCollections
extends KeyedCollection
{

    /**
     * Add an item to one of the child collections in this KeyedCollectionOfCollections.
     *
     * @param mixed $subitem The item to be added to the child collections in this KeyedCollectionOfCollections.
     * @param string $itemKey The key of the item being added.
     *
     * @throws NoSubitemsException
     */
    public function AddSubItem($subitem, $itemKey)
    {
        if (!isset($this->_itemArray[$itemKey]))
        {
            $this->_itemArray[$itemKey] = new Collection();
        }
        if (Collection::IsCollection($this->_itemArray[$itemKey]))
        {
            $this->_itemArray[$itemKey]->AddItem($subitem);
        }
        elseif (is_array($this->_itemArray[$itemKey]))
        {
            $this->_itemArray[$itemKey][] = $subitem;
        }
        else
        {
            throw new NoSubitemsException($itemKey);
        }
    }

    /**
     * @internal magic method override
     * @return string
     */
    public function __toString()
    {
        return "KeyedCollectionOfCollections (" . $this->GetItemCount() . " Items)";
    }
}