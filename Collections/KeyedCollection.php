<?php
namespace DblEj\Collections;

use \DblEj\EventHandling\EventInfo;

/**
 * Stores, and provides access to, a collection of things, where each thing is stored under a unique key identifier.
 *
 * @tags Collections
 */
class KeyedCollection
extends \DblEj\EventHandling\EventRaiser
implements IKeyedCollection
{
    const KEYED_ITEM_ADDED   = 1003;
    const KEYED_ITEM_REMOVED = 1004;

    /**
     * @var \ArrayIterator
     */
    protected $_itemArray;

    /**
     * Create an instance of a KeyedCollection,
     * initializing the items in the collection from the passed <i>$collectionArray</i>.
     *
     * @param array $collectionArray An array of items that will be the initial set of items in this KeyedCollection.
     */
    public function __construct(array $collectionArray = null)
    {
        parent::__construct();
        $this->_itemArray = new \ArrayIterator();

        if ($collectionArray)
        {
            foreach ($collectionArray as $itemKey => $item)
            {
                $this->_itemArray[$itemKey] = $item;
            }
        }
    }

    /**
     * An alias for AddItem
     * @param mixed $item
     * @param string $key
     *
     * @return \DblEj\Collections\KeyedCollection $this for making chained method calls on this collection.
     */
    public function SetItem($item, $key)
    {
        return $this->AddItem($item, $key);
    }

    /**
     * Add an <em>item</em> to this keyed collection.
     *
     * @param mixed $item The item to add.
     * @param string $key The unique key to store this item under.
     * @return \DblEj\Collections\KeyedCollection $this for making chained method calls on this collection.
     */
    public function AddItem($item, $key)
    {
        $this->_itemArray[$key] = $item;
        if (is_object($item))
        {
            $item->_collectionKey    = $key;
            $item->Get_CollectionKey = function()
            {
                return $this->_collectionKey;
            };
        }
        $this->onItemAdded($item);
        return $this;
    }

    /**
     * Replace the item stored under the specified <em>key</em> with the specified <em>item</em>.
     *
     * @param mixed $item
     * @param string $key
     * @return \DblEj\Collections\KeyedCollection $this for making chained method calls on this collection.
     */
    public function UpdateItem($item, $key)
    {
        $this->_itemArray[$key] = $item;
        if (is_object($item))
        {
            $item->_collectionKey    = $key;
            $item->Get_CollectionKey = function()
            {
                return $this->_collectionKey;
            };
        }
        $this->onItemAdded($item);
        return $this;
    }

    /**
     * If the item stored under the specified <em>itemKey</em> is a collection,
     * then add the <em>subitem</em> to that collection.
     *
     * If the item doesn't exist under the specified <em>itemKey</em>,
     * then it will be created as a collection and the <em>subitem</em>
     * will be added to it.
     *
     * @param mixed $subitem The item to be added as a child of the specified item.
     * @param string $itemKey The key of the item under which the <em>subitem</em> will be added.
     *
     * @return \DblEj\Collections\KeyedCollection $this for making chained method calls on this collection.
     *
     * @throws NoSubitemsException If the item does exist, but is not a collection, then an exception will be thrown.
     */
    public function AddSubItem($subitem, $itemKey)
    {
        if (!isset($this->_itemArray[$itemKey]))
        {
            $this->_itemArray[$itemKey] = new Collection();
        }
        if (Collection::IsCollection($this->_itemArray[$itemKey]))
        {
            $this->_itemArray[$itemKey]->AddItem($subitem);
        }
        elseif (is_array($this->_itemArray[$itemKey]))
        {
            $this->_itemArray[$itemKey][] = $subitem;
        }
        else
        {
            throw new NoSubitemsException($itemKey);
        }
        return $this;
    }

    /**
     * Get the item stored under the specified key
     *
     * @param string $key The key under which the desired item is stored.
     *
     * @return mixed The item in the collection that has a key of <i>$key</i>, or null if there is no such item.
     *
     * @throws InvalidKeyException If the key is not valid.
     */
    public function GetItem($key)
    {
        if (isset($this->_itemArray[$key]))
        {
            return $this->_itemArray[$key];
        }
        else
        {
            return null;
        }
    }

    /**
     * Find the specified <em>item</em> within this collection's items
     * and, if found, return the key that the <em>item</em> is stored under.
     *
     * @param mixed $item The item that you want to get the key for.
     *
     * @return mixed
     * The key of the <em>item</em> that is found,
     * or <b>false</b> if the item is not found in this collection.
     */
    public function GetKeyForItem($item)
    {
        return array_search($item);
    }

    /**
     * Merge this keyed collection's items with another keyed collection's items,
     * preserving the keys, and return the resulting keyed collection.
     * If an item exists in both keyed collections under the same key, only
     * one of them will survive the merge.
     *
     * @param \DblEj\Collections\IKeyedCollection $collection
     * The keyed collection who's items will be merged with this collection's items.
     *
     * @return \DblEj\Collections\Collection
     * A new collection that is the result of the merge.
     */
    public function GetMergedWith(IKeyedCollection $collection)
    {
        if ($collection === null)
        {
            throw new \DblEj\System\InvalidArgumentException("collection", "collection cannot be null");
        }
        $newCollection = new KeyedCollection();
        foreach ($this->GetItems() as $key => $item)
        {
            $newCollection->AddItem($item, $key);
        }
        foreach ($collection as $key => $item)
        {
            $newCollection->AddItem($item, $key);
        }
        return $newCollection;
    }

    /**
     * Merge this keyed collection with another keyed collection.
     *
     * @param \DblEj\Collections\IKeyedCollection $collection The keyed collection to merge with this one.
     *
     * @return \DblEj\Collections\KeyedCollection <i>$this</i> for making chained method calls.
     *
     * @throws \DblEj\System\InvalidArgumentException
     */
    public function MergeWith(IKeyedCollection $collection)
    {
        if ($collection === null)
        {
            throw new \DblEj\System\InvalidArgumentException("collection", "collection cannot be null");
        }
        foreach ($collection as $key => $item)
        {
            $this->AddItem($item, $key);
        }
        return $this;
    }

    /**
     * Remove the specified collection item.
     *
     * @param integer $item The item that you want to remove.
     * @return Collection Returns <em>$this</em> to allow for chained method calls.
     */
    public function RemoveItem($item)
    {
        $itemIdx = array_search($item, $this->_itemArray);
        if ($itemIdx !== false)
        {
            $this->RemoveItemByKey($itemIdx);
        }
        return $this;
    }

    /**
     * Remove The item stored under the specified key.
     *
     * @param string $key The key used to find the item.
     * @return KeyedCollection Returns <em>$this</em> to allow for chained method calls.     *
     */
    public function RemoveItemByKey($key)
    {
        if (isset($this->_itemArray[$key]))
        {
            $item                       = $this->_itemArray[$key];
            $this->_itemArray[$key] = null;
            unset($this->_itemArray[$key]);
            if (is_object($item))
            {
                $item->Get_CollectionKey    = null;
            }
        }
        else
        {
            throw new InvalidKeyException($key);
        }
        return $this;
    }

    /**
     * Check if there is an item in this collection stored under the specified key.
     *
     * @param string $itemKey The key to check.
     *
     * @return boolean <i>True</i> if the collection contains the specified key, otherwise </i>False</i>.
     */
    public function ContainsKey($itemKey)
    {
        return isset($itemKey[$this->_itemArray]);
    }

    /**
     * Count the number of items in the collection.
     *
     * @return integer The number of items in the collection.
     */
    public function GetItemCount()
    {
        return count($this->_itemArray);
    }

    /**
     * Get all items from the collection as an array.
     *
     * @return array The internal array of items in the collection.
     */
    public function GetItems()
    {
        return $this->_itemArray;
    }

    /**
     * @internal internal magic override
     * @return array
     */
    public function __toArray()
    {
        return $this->_itemArray->getArrayCopy();
    }

    /**
     * @internal internal magic override
     * @return string
     */
    public function __toString()
    {
        return "KeyedCollection (" . $this->GetItemCount() . " Items)";
    }

    /**
     * Check if the specified <em>item</em> is a part of this collection.
     *
     * @param mixed $itemSearch The item to check.
     *
     * @return boolean <i>True</i> if the item is a part of this collection,
     * otherwise false.
     */
    public function Contains($itemSearch)
    {
        $contains = false;
        foreach ($this->_itemArray as $item)
        {
            if ($itemSearch == $item)
            {
                $contains = true;
                break;
            }
        }
        return $contains;
    }

    /**
     * @internal php iterator interface methods
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_itemArray);
    }

    /**
     * Get the types of events that this EventRaiser raises.
     *
     * @return \DblEj\EventHandling\EventTypeCollection A collection of the EventTypes raised by a KeyedCollection.
     */
    public function GetRaisedEventTypes()
    {
        return new \DblEj\EventHandling\EventTypeCollection(
        [
            "KEYED_ITEM_ADDED"   => self::KEYED_ITEM_ADDED,
            "KEYED_ITEM_REMOVED" => self::KEYED_ITEM_REMOVED
        ]);
    }

    /**
     * @internal
     * Called internally whenever an item is added to the internal array.
     * This will fire the ITEM_ADDED event.
     *
     * @param mixed $item The item that was added to the collection.
     */
    protected function onItemAdded($item)
    {
        $this->raiseEvent(new EventInfo(self::KEYED_ITEM_ADDED, $this, $item));
    }

    /**
     * @internal
     * Called internally whenever an item is deleted from the internal array.
     * This will fire the ITEM_REMOVED event.
     *
     * @param mixed $item The item that was removed from the collection.
     */
    protected function onItemRemoved($item)
    {
        $this->raiseEvent(new EventInfo(self::KEYED_ITEM_REMOVED, $this, $item));
    }

    /**
     * An associate array of the values of the published fields,
     * keyed by the field names.
     *
     * @return array
     */
    public function Get_FieldValues($autoEscapeStrings = false)
    {
        return $this->_itemArray;
    }

    /**
     * A class instances non-static field names should be returned by this method.
     * Some field publishers publish different fields based on their instance;
     * you would do that in this method.
     *
     * @return string[] As array of field names.
     */
    public function Get_InstanceFieldNames()
    {
        return static::Get_FieldNames();
    }

    /**
     * @internal php iterator interface methods
     * @access protected
     * @return \ArrayIterator
     */
    public function offsetExists($offset)
    {
        return (count(array_values($this->_itemArray)) > $offset);
    }

    /**
     * @internal php iterator interface methods
     * @access protected
     * @return \ArrayIterator
     */
    public function offsetGet($offset)
    {
        return array_values($this->_itemArray)[$offset];
    }

    /**
     * @internal php iterator interface methods
     * @access protected
     * @return \ArrayIterator
     */
    public function offsetSet($offset, $value)
    {
        $key                    = array_keys($this->_itemArray)[$offset];
        $this->_itemArray[$key] = $value;
    }

    /**
     * @internal php iterator interface methods
     * @access protected
     * @return \ArrayIterator
     */
    public function offsetUnset($offset)
    {
        $key = array_keys($this->_itemArray)[$offset];
        unset($this->_itemArray[$key]);
    }

    /**
     * Get the data type for the specified field.
     *
     * @param string $fieldName The name of the field to get the data type for.
     */
    public static function GetFieldDataType($fieldName)
    {
        return \DblEj\Data\Field::DATA_TYPE_UNKNOWN;
    }

    /**
     * Get the data types of the fields published by this field publisher.
     */
    public static function GetFieldDataTypes()
    {
        $returnArray = [];
        foreach (self::Get_FieldNames() as $fieldName)
        {
            $returnArray[$fieldName] = \DblEj\Data\Field::DATA_TYPE_UNKNOWN;
        }
        return $returnArray;
    }

    /**
     * A class's static field names should be returned by this method.
     * Some field publishers publish different fields based on their instance;
     * you would not do that in this method.
     *
     * @return string[] As array of field names.
     */
    public static function Get_FieldNames()
    {
        return array_keys($this->_itemArray);
    }

    /**
     * Whether or not this field publisher's fields can be retreived from it statically,
     * or if the published fields are specific to a particular instance.
     */
    public static function Get_HasStaticFieldNames()
    {
        return true;
    }
}