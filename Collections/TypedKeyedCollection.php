<?php

namespace DblEj\Collections;

class TypedKeyedCollection
extends KeyedCollection
implements IKeyedCollection,
           ITypedCollection
{
    private $_typeName;

    /**
     * Instantiate a TypedKeyedCollection, optionally initializing it with an array.
     *
     * @param array $collectionArray
     * An array of items to initialize the collection with.
     * The items must all be of the type specified in $typeName.
     *
     * @param string $typeName
     * The fully qualified name of the data type that each item in this collection must be.
     *
     * @throws \DblEj\System\InvalidDataTypeException
     * If any item in the <i>collectionArray</i> is not of the <i>typeName</i>,
     * this Exception will be thrown.
     */
    public function __construct(array $collectionArray = null, $typeName = "\DblEj\System\Object\BaseObject")
    {
        $this->_typeName = $typeName;
        if ($collectionArray && !self::DoesCollectionHaveSingleType($collectionArray))
        {
            throw new \DblEj\System\InvalidDataTypeException("TypedCollection with one or more invalid items");
        }
        parent::__construct($collectionArray);
    }

    /**
     * Add an item to the collection.
     *
     * @param mixed $item The item to add
     * @param int|string $key The key to index the item by
     *
     * @return \DblEj\Collections\TypedKeyedCollection
     */
    public function AddItem($item, $key = null)
    {
        $this->_throwExceptionOnInvalidItemType($item);
        parent::AddItem($item, $key);
        return $this;
    }

    public function MergeWith(IKeyedCollection $collection)
    {
        if ($this->DoesCollectionMatchType($collection))
        {
            parent::MergeWith($collection);
        }
        else
        {
            throw new \DblEj\System\InvalidDataTypeException("TypedCollection with one or more invalid items");
        }
        return $this;
    }

    /**
     * Check if the passed collection's items are all of the same data type as this collection's item type.
     *
     * @param \DblEj\Collections\DblEj\Collections\ICollection $items A collection of items to test for type compliance with this collection.
     * @return boolean
     * Returns true if the passed collection is typed the same as this collection.
     * That is, if all of it's items are of the same type as this collection's item type.
     * A collection can pass this test even if it is not a TypedCollection.
     */
    public function DoesCollectionMatchType(DblEj\Collections\ICollection $items)
    {
        $returnValue = true;
        foreach ($items->GetItems() as $item)
        {
            if (!is_a($item, $this->_typeName))
            {
                $returnValue = false;
                break;
            }
        }
        return $returnValue;
    }

    /**
     * Check if the passed array's elements are all of the same data type as this collection's item type.
     *
     * @param array $items The array to check.
     * @return boolean
     * Returns true if the passed array's elements are all
     * of the same type as this collection's item type.
     */
    public function DoesArrayMatchType(array $items)
    {
        $returnValue = true;
        foreach ($items as $item)
        {
            if (!is_a($item, $this->_typeName))
            {
                $returnValue = false;
                break;
            }
        }
        return $returnValue;
    }

    /**
     * Check if the passed collection has items that are all of the same type.
     *
     * @return boolean <i>True</i> if all items are of the same type, otherwise <i>false</i>.
     */
    public static function DoesCollectionHaveSingleType($items)
    {
        $returnValue = true;
        $firstType   = null;
        foreach ($items as $item)
        {
            if ($firstType === null)
            {
                $firstType = get_class($item);
            }
            if (!is_a($item, $firstType))
            {
                $returnValue = false;
                break;
            }
        }
        return $returnValue;
    }

    private function _throwExceptionOnInvalidItemType($item)
    {
        if (!is_a($item, $this->_typeName))
        {
            throw new \DblEj\System\InvalidDataTypeException(get_class($item), $this->_typeName);
        }
    }
}