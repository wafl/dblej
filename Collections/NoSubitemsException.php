<?php

namespace DblEj\Collections;

/**
 * Thrown when trying to add a subitem to an item that cannot have sub items.
 */
class NoSubitemsException
extends \DblEj\System\Exception
{

    /**
     * The key of the item that cannot have subitems.
     *
     * @param string $key
     * @param int $severity
     */
    public function __construct($key, $severity = E_WARNING)
    {
        parent::__construct("The specified item ($key) cannot have subitems added to it", $severity, null);
    }
}