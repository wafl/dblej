<?php

namespace DblEj\Collections;

/**
 * A collection that can sort it's items.
 */
interface ISortableCollection
extends ICollection
{

    /**
     * Sort this collection's items.
     *
     * @param mixed $sortOptions
     */
    public function Sort($sortOptions);
}