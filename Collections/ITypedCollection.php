<?php

namespace DblEj\Collections;

/**
 * A strongly-typed collection.
 */
interface ITypedCollection
extends ICollection
{

    /**
     * Check if a non-typed collection contains all items that have
     * the same data type as this collection's item data type.
     *
     * @param \DblEj\Collections\DblEj\Collections\ICollection The collection to check.
     */
    public function DoesCollectionMatchType(DblEj\Collections\ICollection $items);

    /**
     * Check if an array has all elements that have
     * the same data type as this collection's item data type.
     *
     * @param array $items The array to check.
     */
    public function DoesArrayMatchType(array $items);

    /**
     * Check if the passed collection has items that are all of the same type.
     *
     * @return boolean <i>True</i> if all items are of the same type, otherwise <i>false</i>.
     */
    public static function DoesCollectionHaveSingleType($items);
}