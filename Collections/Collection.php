<?php
namespace DblEj\Collections;

/**
 * Stores, and provides easy access to, a collection of anything.
 *
 * @tags Collections
 */
class Collection
implements ICollection
{
    /**
     * @var \ArrayIterator
     */
    protected $_itemArray;

    /**
     * Collection constructor.
     * Pass in an array to initialize the collection with the array.
     * Each array element will become a collection item.
     *
     * @param array $collectionArray
     */
    public function __construct(array $collectionArray = null)
    {
        $this->_itemArray = new \ArrayIterator();
        if ($collectionArray)
        {
            $itemIdx = 0;
            foreach ($collectionArray as $item)
            {
                $this->_itemArray[$itemIdx] = $item;
                $itemIdx++;
            }
        }
    }

    /**
     * Add an item to the collection.
     *
     * @assert ("StringItem") == $this->object
     * @param mixed $item
     */
    public function AddItem($item)
    {
        $this->_itemArray[] = $item;
        return $this;
    }

    /**
     * Get the collection item at the specified index.
     *
     * @param integer $itemIndex
     * @return mixed The collection item found at the specified index
     */
    public function GetItem($itemIndex)
    {
        return $this->_itemArray[$itemIndex];
    }

    /**
     * Count the number of items in the collection.
     *
     * @return integer The number of items in the collection.
     */
    public function GetItemCount()
    {
        return count($this->_itemArray);
    }

    /**
     * Get all items from the collection as an array.
     *
     * @return array The internal array of items in the collection.
     */
    public function GetItems()
    {
        return $this->_itemArray;
    }

    /**
     * Remove the specified collection item.
     *
     * @param integer $item
     * @return Collection Returns <em>this</em> to allow for chained method calls.
     */
    public function RemoveItem($item)
    {
        $itemIndex = array_search($this->_itemArray, $item);
        array_splice($this->_itemArray, $itemIndex, 1);
        return $this;
    }

    /**
     * Merge this collection's items with another collection's items and return the resulting collection.
     *
     * @param \DblEj\Collections\ICollection $collection The collection who's items will be merged with this collection's items.
     *
     * @return \DblEj\Collections\Collection A new collection that is the result of the merge.
     */
    public function GetMergedWith(ICollection $collection)
    {
        $newCollection = new Collection();
        foreach ($this->GetItems() as $item)
        {
            $newCollection->AddItem($item);
        }
        foreach ($collection as $item)
        {
            $newCollection->AddItem($item);
        }
        return $newCollection;
    }

    /**
     * Merge this collection with another collection.
     *
     * @param \DblEj\Collections\ICollection $collection The collection to merge with this one.
     * @return \DblEj\Collections\Collection Returns this collection, now merged with the passed collection.
     */
    public function MergeWith(ICollection $collection)
    {
        foreach ($collection as $item)
        {
            $this->AddItem($item);
        }
        return $this;
    }

    /**
     * Check if the specified <em>item</em> is a part of this collection.
     *
     * @param mixed $item The item to check.
     *
     * @return integer The index of the item if it is a part of this collection,
     * or false if the item is not a part of this collection.
     */
    public function Contains($item)
    {
        return array_search($item, $this->_itemArray) !== false;
    }

    /**
     * Check if the passed variable is a collection.
     *
     * @param mixed $variable
     * @return boolean
     */
    public static function IsCollection($variable)
    {
        return is_subclass_of($variable, "\DblEj\Collections\ICollection");
    }

    /**
     * Php iterator implementation
     * @internal
     * @access protected
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_itemArray);
    }

    public function __toArray()
    {
        return $this->_itemArray;
    }

    public function __toString()
    {
        return "Collection (" . $this->GetItemCount() . " Items)";
    }
}