/*
 *@namespace DblEj.Parsing
 */
Namespace("DblEj.Parsing");

/*
 *@class Xml Utility class for parsing XML
 *@static
 */
DblEj.Parsing.Xml = {};

/**
 * @function CreateDocFromXmlString Pass in an xml string and get a parsed xml document object in return
 * @static
 * @param xmlString string
 * @return mixed If the client is IE then I will return an ActiveX XMLDOM object, otherwise this I attempt to return a Document object
 */
DblEj.Parsing.Xml.CreateDocFromXmlString = function (xmlString)
{
    var xmlDoc;
    if (window.DOMParser)
    {
        var parser = new DOMParser();
        xmlDoc = parser.parseFromString(xmlString, "text/xml");
    } else {
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(xmlString);
    }
    return xmlDoc;
};
/**
 * @function GetAttributeByName Gets an attribute from an XML Node
 * @static
 * @param xmlNode Node
 * @param attributeName string
 * @return Attr
 */
DblEj.Parsing.Xml.GetAttributeByName = function (xmlNode, attributeName)
{
    var atts = xmlNode.attributes;
    var returnAtt = null;
    for (var attIdx = 0; attIdx < atts.length; attIdx++)
    {
        if (atts[attIdx].name == attributeName)
        {
            returnAtt = atts[attIdx];
        }
    }
    return returnAtt;
};

/**
 * @function GetAttributeValueByName Gets an attribute value from an XML Node
 * @static
 * @param xmlNode Node
 * @param attributeName string
 * @return string
 */
DblEj.Parsing.Xml.GetAttributeValueByName = function (xmlNode, attributeName)
{
    var atts = xmlNode.attributes;
    if (atts)
    {
        var returnAtt = null;
        for (var attIdx = 0; attIdx < atts.length; attIdx++)
        {
            if (atts[attIdx].name == attributeName)
            {
                returnAtt = atts[attIdx];
            }
        }
        if (returnAtt)
        {
            return returnAtt.value;
        } else {
            return null;
        }
    } else {
        return null;
    }
};

/**
 * @function GetNodeTextValue Gets the inner-text value from an XML Node
 * @static
 * @param xmlNode Node
 * @return string The text value of the XML Node, or null if there is no value
 */
DblEj.Parsing.Xml.GetNodeTextValue = function (xmlNode)
{
    if (xmlNode.text)
    {
        return xmlNode.text;
    }
    else if (xmlNode.textContent)
    {
        return xmlNode.textContent;
    }
    else if (xmlNode.innerText)
    {
        return xmlNode.innerText;
    } else {
        return null;
    }
};