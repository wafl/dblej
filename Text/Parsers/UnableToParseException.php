<?php

namespace DblEj\Text\Parsers;

/**
 * Thrown when the string can't be parsed
 */
class UnableToParseException
extends \Exception
{

    function __construct($parseTypeString, $reason = "unknown", $severity = E_ERROR, $inner = null)
    {
        $message = "Unable to parse $parseTypeString (reason: $reason)";
        parent::__construct($message, $severity, $inner);
    }
}