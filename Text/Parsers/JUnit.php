<?php

namespace DblEj\Text\Parsers;

/**
 * JUnit log parser.
 */
class JUnit
{

    /**
     * Parse the specified JUnit xml.
     *
     * @param String $inputXml
     * @return array
     * @throws UnableToParseException
     */
    public static function Parse($inputXml)
    {
        $parser       = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        $parsedVals   = array();
        $parseSuccess = xml_parse_into_struct($parser, $inputXml, $parsedVals);

        $suites = array();

        if ($parseSuccess)
        {
            $currentSuite = null;
            $currentCase  = null;

            foreach ($parsedVals as $parsedVal)
            {
                switch ($parsedVal["tag"])
                {
                    case "testsuite":
                        $vals = $parsedVal;
                        if ($vals["type"] == "open")
                        {
                            $currentSuite = $vals["attributes"];
                            if (isset($currentSuite["file"]))
                            {
                                $suites[$currentSuite["name"]] = $currentSuite;
                            }
                        }
                        else
                        {
                            $currentSuite = null;
                        }
                        break;
                    case "testcase":
                        if (!isset($suites[$currentSuite["name"]]["allCases"]))
                        {
                            $suites[$currentSuite["name"]]["allCases"] = array();
                        }
                        if ($parsedVal["type"] == "open")
                        {
                            $caseName                                                          = $parsedVal["attributes"]["name"];
                            $suites[$currentSuite["name"]]["allCases"][$caseName]              = $parsedVal;
                            $currentCase                                                       = $vals;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["isFailure"] = 0;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["isError"]   = 0;
                        }
                        elseif ($parsedVal["type"] == "complete")
                        {
                            $caseName                                                           = $parsedVal["attributes"]["name"];
                            $suites[$currentSuite["name"]]["allCases"][$caseName]               = $parsedVal;
                            $currentCase                                                        = null;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["isFailure"]  = 0;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["isError"]    = 0;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["lineNumber"] = 0;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["fileName"]   = 0;
                        }
                        else
                        {
                            $currentCase = null;
                        }
                        break;
                    case "failure":
                        if ($parsedVal["type"] == "complete")
                        {
                            $message                                                           = trim($parsedVal["value"]);
                            $messageLines                                                      = explode("\n", $message);
                            $messageLines                                                      = array_filter($messageLines);
                            $messageLines                                                      = array_splice($messageLines, 1, count($messageLines) - 2);
                            $message                                                           = $messageLines[0];
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["isFailure"] = 1;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["message"]   = $message;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["callStack"] = $messageLines;
                        }
                        break;
                    case "error":
                        if ($parsedVal["type"] == "complete")
                        {
                            $message      = trim($parsedVal["value"]);
                            $messageLines = explode("\n", $message);
                            $messageLines = array_filter($messageLines);
                            if (count($messageLines) > 3)
                            {
                                $messageLines = array_splice($messageLines, 1, count($messageLines) - 2);
                                $message      = $messageLines[0];
                            }
                            elseif (count($messageLines) > 1)
                            {
                                $message = $messageLines[1];
                            }
                            else
                            {
                                $message = implode(",", $messageLines);
                            }
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["isError"]   = 1;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["message"]   = $message;
                            $suites[$currentSuite["name"]]["allCases"][$caseName]["callStack"] = $messageLines;
                        }
                        break;
                }
            }
            xml_parser_free($parser);
            return ($suites);
        }
        else
        {
            if ($parser)
            {
                $lastXmlError = xml_get_error_code($parser);
                xml_parser_free($parser);
            }
            else
            {
                $lastXmlError = "No parser";
            }
            throw new UnableToParseException("JUnit XML", $lastXmlError);
        }
    }
}