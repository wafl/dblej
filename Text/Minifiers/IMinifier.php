<?php

namespace DblEj\Text\Minifiers;

/**
 * Provides methods for minification.
 */
interface IMinifier
{

    public static function Minify($source);

    public static function GetAvailableOptions();

    public static function SetOption($optionName, $optionValue);
}