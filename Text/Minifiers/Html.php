<?php

namespace DblEj\Text\Minifiers;

/**
 * HTML minifier.
 */
class Html
implements IMinifier
{

    public static function Minify($html)
    {
        $resultHtml = "";

        //add NOMINIFYs to <code> and <pre> blocks
        $html            = str_replace("<code", "<!--NOMINIFY--><code", $html);
        $html            = str_replace("</code>", "</code><!--ENDNOMINIFY-->", $html);
        $html            = str_replace("<pre", "<!--NOMINIFY--><pre", $html);
        $html            = str_replace("</pre>", "</pre><!--ENDNOMINIFY-->", $html);

        $htmlChunks = self::_getHtmlChunkedByNoMinifys($html);
        foreach ($htmlChunks as $htmlChunk)
        {
            if ($htmlChunk["CanMinify"])
            {

                $minifiedPiece = preg_replace('/\h\h\h+/', "", $htmlChunk["Html"]);
                $minifiedPiece = preg_replace('/\n+/', " ", $minifiedPiece);
                $resultHtml .= $minifiedPiece;
            } else {
                $resultHtml .= $htmlChunk["Html"];
            }
        }
        return $resultHtml;
    }

    private static function _getHtmlChunkedByNoMinifys($html)
    {
        $resultChunks = [];
        $nominifyStartPos = 0;
        $nominifyEndPos = 0;
        do
        {
            $nominifyStartPos = stripos($html, "<!--NOMINIFY-->", $nominifyEndPos);
            if ($nominifyEndPos > 0 && ($nominifyEndPos != strlen($html)))
            {
                $nominifyEndPos += 18; //go past the ENDNOMINIFY tag
            }

            if ($nominifyStartPos !== false)
            {
                //add the regular minifiable html that occurred after the last nominify ended (or zero on start)
                if ($nominifyEndPos != $nominifyStartPos)
                {
                    $resultChunks[] = ["Html"=>substr($html, $nominifyEndPos, $nominifyStartPos - $nominifyEndPos), "CanMinify"=>true];
                }

                $nominifyEndPos = stripos($html, "<!--ENDNOMINIFY-->", $nominifyStartPos);
                if ($nominifyEndPos === false)
                {
                    //stray NOMINIFY tag has no end tag. Lets just consider the end of the file the end location
                    $nominifyEndPos = strlen($html);
                }
                if ($nominifyStartPos !== false)
                {
                    $nominifyStartPos += 15;  //go past the NOMINIFY tag
                }
                $resultChunks[] = ["Html"=>substr($html, $nominifyStartPos, $nominifyEndPos - $nominifyStartPos), "CanMinify"=>false];

            } else {
                if ($nominifyEndPos != strlen($html))
                {
                    $resultChunks[] = ["Html"=>substr($html, $nominifyEndPos), "CanMinify"=>true];
                }
                break;
            }

        } while ($nominifyStartPos !== false);

        return $resultChunks;
    }
    public static function SetOption($optionName, $optionValue)
    {
        throw new \Exception("Invalid option sent to Html minifier");
    }

    public static function GetAvailableOptions()
    {
        return array();
    }
}