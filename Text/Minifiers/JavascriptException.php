<?php
namespace DblEj\Text\Minifiers;

/**
 * Thrown when there is an unexpected condition or event while minifying javascript.
 */
class JavascriptException
extends \Exception
{

}