<?php

namespace DblEj\Text\Minifiers;

/**
 * Utility class for serving minified files and content.
 */
class FileServer
{

    public static function ServeFiles(array $filelist, \DblEj\Presentation\RenderOptions $renderOptions, $mimType, $filename = null, $lastModifiedTimestamp = null, $allowCache = true, $cacheTimeoutSecs = 600, $minifierClassName = "\DblEj\Text\Minifiers\Javascript", $smartyProcess = false, \Smarty $smarty = null, \DblEj\Data\IDataStore $memoryStore = null)
    {
        $latestFileTouchDate = 0;
        $outputData          = "";
        if (count($filelist) > 0)
        {
            foreach ($filelist as $file)
            {
                if ($smartyProcess)
                {
                    $filecontents = $smarty->fetch($file, $renderOptions->Get_Key1());
                }
                else
                {
                    $filecontents = file_get_contents($file);
                    if (filemtime($file) > $latestFileTouchDate)
                    {
                        $latestFileTouchDate = filemtime($file);
                    }
                }
                $outputData .= $filecontents . \PHP_EOL;
            }
            if (!$lastModifiedTimestamp)
            {
                $lastModifiedTimestamp = $latestFileTouchDate;
            }
            if (!$filename)
            {
                $filename = basename($filelist[0]);
            }
            \DblEj\Communication\Http\Util::ServeContent($outputData, $mimType, $filename, false, $lastModifiedTimestamp, false, $allowCache, 300000);
        }
    }

    public static function GetMinifiedFileContent(array $filelist, \DblEj\Presentation\RenderOptions $renderOptions, $mimType, $filename = null, $lastModifiedTimestamp = null, $allowCache = true, $cacheTimeoutSecs = 600, $minifierClassName = "\DblEj\Text\Minifiers\Javascript", $preProcessByViewRenderer = false, $viewRendererOrSmartyObject = null)
    {
        $returnData = null;
        $contentArray = array();
        if (!$filename && count($filelist) > 0)
        {
            $filename = basename($filelist[0]);
        }
        if ($filename)
        {
            $latestFileTouchDate = 0;
            foreach ($filelist as $fileInfo)
            {
                if (is_array($fileInfo)) //added support for $file to be an array (instead of just the filename string).  This allows us to pass options along with the filename.
                {
                    $fileName = $fileInfo["File"];
                    $minify = isset($fileInfo["Minify"])?$fileInfo["Minify"]:$renderOptions->Get_MinifyWhenPossible();
                } else {
                    $fileName = $fileInfo;
                    $minify = $renderOptions->Get_MinifyWhenPossible();
                }
                $contentArray[] = ["Contents"=>file_get_contents($fileName), "Minify"=>$minify];
                if (filemtime($fileName) > $latestFileTouchDate)
                {
                    $latestFileTouchDate = filemtime($fileName);
                }
            }
            if (!$lastModifiedTimestamp)
            {
                $lastModifiedTimestamp = $latestFileTouchDate;
            }
        }
        $returnData .= self::GetMinifiedContent($contentArray, $renderOptions, $minifierClassName, $preProcessByViewRenderer, $viewRendererOrSmartyObject);
        return $returnData;
    }

    public static function ServeMinifiedFiles(array $filelist, DblEj\Presentation\RenderOptions $renderOptions, $mimType, $filename = null, $lastModifiedTimestamp = null, $allowCache = true, $cacheTimeoutSecs = 600, $minifierClassName = "\\DblEj\\Minification\\Javascript", $preProcessByViewRenderer = false, $viewRendererOrSmartyObject = null)
    {
        $filecontentsArray = self::GetMinifiedFileContent($filelist, $renderOptions, $mimType, $filename, $lastModifiedTimestamp, $allowCache, $cacheTimeoutSecs, $minifierClassName, $preProcessByViewRenderer, $viewRendererOrSmartyObject);
        self::ServeMinifiedContent($filecontentsArray, $renderOptions, $mimType, $filename, $lastModifiedTimestamp, $allowCache, $cacheTimeoutSecs, $minifierClassName, $preProcessByViewRenderer, $viewRendererOrSmartyObject);
    }

    public static function GetMinifiedContent(array $filecontentsArray, \DblEj\Presentation\RenderOptions $renderOptions, $minifierClassName = "\\DblEj\\Minification\\Javascript", $preprocessByViewRenderer = false, $viewRendererOrSmartyObject = null)
    {
        $outputData = "";
        if ($preprocessByViewRenderer)
        {
            if ($viewRendererOrSmartyObject == null)
            {
                throw new \Exception("cannot preprocess file without a view renderer (ex: smarty)");
            }
        }
        if (count($filecontentsArray) > 0)
        {
            foreach ($filecontentsArray as $filecontentsInfo)
            {
                if (is_array($filecontentsInfo))
                {
                    $filecontents = $filecontentsInfo["Contents"];
                    $minifyOverride = isset($filecontentsInfo["Minify"])?($filecontentsInfo["Minify"]?true:false):null;
                } else {
                    $filecontents = $filecontentsInfo;
                    $minifyOverride = null;
                }
                if ($preprocessByViewRenderer)
                {
                    if (is_a($viewRendererOrSmartyObject, "Smarty"))
                    {
                        /*
                         * @depracated used to be hard-coded to smarty.  now should use generic viewrenderer
                         */
                        $filecontents = $viewRendererOrSmartyObject->fetch("string:" . $filecontents);
                    }
                    else
                    {
                        $filecontents = $viewRendererOrSmartyObject->RenderString($filecontents, $renderOptions);
                    }
                }
                if (($minifyOverride == true || ($minifyOverride === null && $renderOptions->Get_MinifyWhenPossible())) && $minifierClassName)
                {
                    $outputData .= $minifierClassName::Minify($filecontents) . \PHP_EOL;
                }
                else
                {
                    $outputData .= $filecontents;
                }
            }
        }
        return $outputData;
    }

    public static function ServeMinifiedContent(array $filecontentsArray, \DblEj\Presentation\RenderOptions $renderOptions, $mimType, $filename, $lastModifiedTimestamp = 0, $allowCache = true, $cacheTimeoutSecs = 600, $minifierClassName = "\DblEj\Text\Minifiers\Javascript", $preprocessByViewRenderer = false, $viewRendererOrSmartyObject = null)
    {
        $content = self::GetMinifiedContent($filecontentsArray, $renderOptions, $mimType, $filename, $lastModifiedTimestamp, $allowCache, $cacheTimeoutSecs, $minifierClassName, $preprocessByViewRenderer, $viewRendererOrSmartyObject);
        \DblEj\Communication\Http\Util::ServeContent($content, $mimType, $filename, false, $lastModifiedTimestamp, false, $allowCache, 300000);
    }
}
?>