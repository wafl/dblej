<?php

namespace DblEj\Configuration;

/**
 * An option that would typically be used as part of a configuration.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Collections\Option
 */
class Option
extends \DblEj\Collections\Option
{
}