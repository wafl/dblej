/**
 * @class Global
 */

/**
 * @function Iteratize
 * Make an Array or other collection of objects iteratable by DblEj.
 * Once an object is iteratized, you can iterate (and act on) each of it's elements using element.OnEach()
 *
 * @param {Array|NodeList|HTMLElementList|FileList|Object} collection The object to make iteratable.
 *
 * @return {Array|NodeList|HTMLElementList|FileList|Object}
 */
function Iteratize(collection, ignoreTemplateElements)
{
    if (!IsDefined(ignoreTemplateElements))
    {
        ignoreTemplateElements = true;
    }
    if (!IsDefined(collection.OnEach))
    {

        collection.OnEach = function (doThisOnEach)
        {
            var returnElem;
            if (IsArray(collection))
            {
                var collectionCopy = []; //make a copy in case this is a nodelist and the callback manipulates the list, would leave us with broken (incompleted) loop
                for (var boxIdx = 0; boxIdx < collection.length; boxIdx++)
                {
                    if (!ignoreTemplateElements || !IsDefined(collection[boxIdx].ContainsAncestorType) || !collection[boxIdx].ContainsAncestorType("template"))
                    {
                        collectionCopy[boxIdx] = collection[boxIdx];
                    }
                }

                for (var boxIdx = 0; boxIdx < collectionCopy.length; boxIdx++)
                {
                    returnElem = collectionCopy[boxIdx];
                    if (returnElem != collectionCopy.OnEach)
                    {
                        doThisOnEach(returnElem);
                    }
                }
            } else {
                for (var boxIdx in collection)
                {
                    if (!IsFunction(collection[boxIdx]))
                    {
                        returnElem = collection[boxIdx];
                        if (returnElem != collection.OnEach)
                        {
                            doThisOnEach(returnElem);
                        }
                    }
                }
            }
            return collection;
        };

        collection.OnEachKeyed = function (doThisOnEach)
        {
            var returnElem;
            if (IsArray(collection))
            {
                var collectionCopy = []; //make a copy in case this is a nodelist and the callback manipulates the list, would leave us with broken (incompleted) loop
                for (var boxIdx = 0; boxIdx < collection.length; boxIdx++)
                {
                    if (!ignoreTemplateElements || !IsDefined(collection[boxIdx].ContainsAncestorType) || !collection[boxIdx].ContainsAncestorType("template"))
                    {
                        collectionCopy[boxIdx] = collection[boxIdx];
                    }
                }

                for (var boxIdx = 0; boxIdx < collectionCopy.length; boxIdx++)
                {
                    returnElem = collectionCopy[boxIdx];
                    if (returnElem != collectionCopy.OnEach)
                    {
                        doThisOnEach(boxIdx, returnElem);
                    }
                }
            } else {
                for (var boxIdx in collection)
                {
                    if (!IsFunction(collection[boxIdx]))
                    {
                        returnElem = collection[boxIdx];
                        if (returnElem != collection.OnEach)
                        {
                            doThisOnEach(boxIdx, returnElem);
                        }
                    }
                }
            }
            return collection;
        };

        /**
         * @function SetData
         * Store the data value in all of the matching elements.
         * 
         * @param {String} dataName
         * @param {String} dataValue
         * 
         * @returns {Array|FileList|NodeList|Iteratize.collection|Object|HTMLElementList}
         */
        collection.SetData = function(dataName, dataValue)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.SetData(dataName, dataValue);
                });
            return collection;
        };

        collection.SetInnerHtml = function(html)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.SetInnerHtml(html);
                });
            return collection;
        };

        collection.Append = function(html)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Append(html);
                });
            return collection;
        };

        collection.EnableInput = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.EnableInput();
                });
            return collection;
        };

        collection.DisableInput = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.DisableInput();
                });
            return collection;
        };

        collection.Show = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Show();
                });
            return collection;
        };

        collection.FadeIn = function(speed, callbackFunction, endZIndex, endOpacity)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.FadeIn(speed, callbackFunction, endZIndex, endOpacity);
                });
            return collection;
        };

        collection.FadeOut = function(speed, callbackFunction, endZIndex, endOpacity)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.FadeOut(speed, callbackFunction, endZIndex, endOpacity);
                });
            return collection;
        };

        collection.SetOpacity = function(opacity)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.SetOpacity(opacity);
                });
            return collection;
        };

        collection.Hide = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Hide();
                });
            return collection;
        };
        collection.Cloak = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Cloak();
                });
            return collection;
        };
        collection.UnCloak = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.UnCloak();
                });
            return collection;
        };
        collection.CloseVertical = function (speed, callback)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.CloseVertical(speed, callback);
                });
            return collection;
        };
        collection.OpenVertical = function (speed, callback, recalculateHeight)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.OpenVertical(speed, callback, recalculateHeight);
                });
            return collection;
        };

        collection.Disable = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Disable();
                });
            return collection;
        };
        collection.ToggleDisplay = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.ToggleDisplay();
                });
            return collection;
        };
        collection.ToggleHidden = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.ToggleHidden();
                });
            return collection;
        };
        collection.ToggleEnabled = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.ToggleEnabled();
                });
            return collection;
        };
        collection.Enable = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Enable();
                });
            return collection;
        };
        collection.SelectAllText = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.SelectAllText();
                });
            return collection;
        };
        collection.Pulse = function (speed, color, callbackFunction)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Pulse(speed, color, callbackFunction);
                });
            return collection;
        };
        collection.Remove = function ()
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Remove();
                });
            return collection;
        };

        collection.SetText = function(newText)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.SetText(newText);
                });
            return collection;
        };
        collection.Set_Value = function (newValue)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.Set_Value(newValue);
                });
            return collection;
        };
        collection.SetValue = collection.Set_Value;
        collection.SetAttribute = function (attributeName, value)
        {
            if (attributeName != "")
            {
                collection.OnEach(
                    function (elem)
                    {
                        elem.SetAttribute(attributeName, value);
                    });
            }
            return collection;
        };
        collection.RemoveAttribute = function (attributeName)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.RemoveAttribute(attributeName);
                });
            return collection;
        };

        collection.AddClass = function (className)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddClass(className);
                });
            return collection;
        };
        collection.RemoveClass = function (className)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.RemoveClass(className);
                });
            return collection;
        };
        collection.AddClickHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddClickHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddMouseDownHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddMouseDownHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddMouseUpHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddMouseUpHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddDragStartHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddDragStartHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddDragOverHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddDragOverHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddDropHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddDropHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddFocusHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddFocusHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddBlurHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddBlurHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddSubmitHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddSubmitHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };

        collection.AddChangeHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddChangeHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        
        collection.AddKeyUpHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddKeyUpHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };

        collection.AddKeyDownHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddKeyDownHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        
        collection.AddKeyPressHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddKeyPressHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddMouseInHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddMouseInHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.AddMouseOutHandler = function (handlerFunction, targetSelector, duringCapture)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.AddMouseOutHandler(handlerFunction, targetSelector, duringCapture);
                });
            return collection;
        };
        collection.SetCss = function (property, value, animationSpeed)
        {
            collection.OnEach(
                function (elem)
                {
                    elem.SetCss(property, value, animationSpeed);
                });
            return collection;
        };
        collection.Set_Value = function (value, triggerChange)
        {
            collection.OnEach(
                function (elem)
                {
                    if (IsDefined(elem.Set_Value))
                    {
                        elem.Set_Value(value, triggerChange);
                    }
                });
            return collection;
        };
    }
    return collection;
}