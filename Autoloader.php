<?php

namespace DblEj;

class Autloader
{
    public static function __dblEjAutoLoad($classname)
    {
        $triedPaths   = array();
        $triedPaths[] = $classname;
        if (!class_exists($classname, false))
        {
            $classfile = str_replace("\\", "/", $classname);
            if (substr($classfile, 0, 1) == "/")
            {
                $classfile = substr($classfile, 1);
            }
            if (substr($classfile, 0, 5) == "DblEj")
            {
                $classfile = substr($classfile, 5);
                $classpath = __DIR__ . "$classfile.php";
                if (@file_exists($classpath))
                {
                    require_once($classpath);
                    return true;
                }
            }
        }
        return false;
    }
}
spl_autoload_register(array("DblEj\\Autloader", "__dblEjAutoLoad"));