window.document.AppHasStarted = false;
window.document.WindowIsLoaded = false;

DblEj.EventHandling.Events.AddHandler(window, "load", function(e){ window.document.WindowIsLoaded = true; });

DblEj.StartFunctions = [];
DblEj.StartApp = function()
{
    for (var i = 0; i < DblEj.StartFunctions.length; i++)
    {
        var handlerFunction = DblEj.StartFunctions[i];
        handlerFunction();
    }
    window.document.AppHasStarted = true;
    DblEj.StartFunctions = null;
};

/**
 * @function Benchmark
 * Benchmark another function
 *
 * @param {object} functionContext
 * The value of "this" within the function being benchmarked.
 * If the function is a method on an HTML element, then this argument should be the HTML element object
 *
 * @param {function} functionToBenchMark
 * Pointer to the function to benchmark
 *
 * @param {array} inputs
 * Args to pass to function being benchmarked.
 * @default []
 *
 * @param {Integer} iterations
 * The number of iterations
 * @default 100
 *
 * @param [Boolean] includeSummary
 * If true, a summary will be included in the output
 * @default true
 *
 * @return {Number}
 * How many milliseconds it took to finish the function iterations times
 */
DblEj.Benchmark = function(functionContext, functionToBenchMark, inputs, iterations, includeSummary)
{
    if (!IsDefined(iterations) || !iterations)
    {
        iterations = 100;
    }
    if (!IsDefined(inputs) || !inputs)
    {
        inputs = [];
    }
    if (!IsDefined(includeSummary) || !includeSummary)
    {
        includeSummary = true;
    }
    var startTime = DblEj.Util.Time.GetTimestampMs();

    for (var iteration = 0; iteration < iterations; iteration++)
    {
        functionToBenchMark.apply(functionContext, inputs);
    }
    var ttl = DblEj.Util.Time.GetTimestampMs() - startTime;
    return includeSummary?(iterations+" iterations in "+ttl+" ms"):ttl;
};

/**
 * @function Start
 * Calls the specified function when the client is finished loading.
 * Wrap your startup functions in this function to ensure that you are not
 * using client objects before they're ready.
 *
 * If you call this function after the application has already started (or the window is already loaded, when waitForCompleteDom = true), then the
 * specified <i>handlerFunction</i> is called immediately.
 *
 * @param {Function} handlerFunction The callback function to be called when the application is ready and, optionally, the DOM is fully loaded.
 * @param {Boolean} waitForWindowLoad If False, then the handlerFunction will be called immediately once the Javascript dependencies are ready.  If True, then it will not be called until the entire DOM is loaded.
 * @returns {Void}
 */
Start = function (handlerFunction, waitForWindowLoad)
{
    if (!IsDefined(waitForWindowLoad) || !waitForWindowLoad)
    {
        waitForWindowLoad = false;
    }
    if (IsDefined(handlerFunction)) //if they call without passing a handler then just ignore it (otherwise throws fatal)
    {
        if (waitForWindowLoad)
        {
            if (window.document.WindowIsLoaded)
            {
                handlerFunction(); ///if the window is already loaded then there is nothing to wait for, run their callback
            } else {
                DblEj.EventHandling.Events.AddHandler(window, "load", handlerFunction);
            }
        } else {
            if (window.document.AppHasStarted === true)
            {
                handlerFunction(); ///if the app has already started then there is nothing to wait for, run their callback
            } else {
                DblEj.StartFunctions[DblEj.StartFunctions.length] = handlerFunction;
            }
        }
    }
};

/**
 * DblEj is a library for php and javascript
 * @module DblEj
 */
Start(function () {
    if (typeof HTMLTemplateElement == 'undefined' || typeof HTMLTemplateElement == undefined)
    {
        var docFrag;
        Iteratize(document.getElementsByTagName("template"))
            .OnEach(function (elem)
            {
                docFrag = document.createDocumentFragment();
                docFrag.appendChild(elem.firstChild);
                elem.content = docFrag;
            });
    }
});