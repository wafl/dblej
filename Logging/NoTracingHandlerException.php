<?php

namespace DblEj\Logging;

/**
 * Thrown when there is no handler for the trace.
 */
class NoTracingHandlerException
extends \Exception
{

    public function __construct($traceMsg, $traceSeverity, $severity = E_ERROR, $inner = null)
    {
        $severitString = \DblEj\Logging\Tracing::GetLogLevelText($traceSeverity);
        parent::__construct("Could not trace a message because there is no trace handler.  Either call Tracing::SetTraceHandler(), pass a trace handler as an argument into the helper you just called, or call Trace() on the handler directly.  Message ($severitString): $traceMsg", $severity, $inner);
    }
}