<?php
namespace DblEj\Logging;

/**
 * Utility class for server-side tracing.
 * @static
 */
class Tracing
{
    /**
     * Info Log level
     * @var int
     * @deprecated since revision 1295 in favor of the constant of the same name
     */
    public static $LOGLEVEL_INFO    = 1;

    /**
     * Warning Log level
     * @var int
     * @deprecated since revision 1295 in favor of the constant of the same name
     */
    public static $LOGLEVEL_WARNING = 2;

    /**
     * Error Log level
     * @var int
     * @deprecated since revision 1295 in favor of the constant of the same name
     */
    public static $LOGLEVEL_ERROR   = 3;

    /**
     * Fatal Log level
     * @var int
     * @deprecated since revision 1295 in favor of the constant of the same name
     */
    public static $LOGLEVEL_FATAL   = 4;

    const LOGLEVEL_INFO    = 1;
    const LOGLEVEL_WARNING = 2;
    const LOGLEVEL_ERROR   = 3;
    const LOGLEVEL_FATAL   = 4;

    private static $_traceHandlers = [];

    /**
     * Set the current trace handler.
     *
     * @deprecated since revision 1295 in favor of AddTraceHandler(ITraceHandler, string)
     *
     * @param \DblEj\Logging\ITraceHandler $traceHandler
     */
    public static function SetTraceHandler(ITraceHandler $traceHandler)
    {
        self::$_traceHandlers = [$traceHandler];
    }

    /**
     * Add a trace handler to the list of active trace handlers.
     *
     * @param \DblEj\Logging\ITraceHandler $traceHandler
     */
    public static function AddTraceHandler(ITraceHandler $traceHandler, $logLevel = null, $key = null)
    {
        if ($key)
        {
            self::$_traceHandlers[$key] = $traceHandler;
        } else {
            self::$_traceHandlers[] = $traceHandler;
        }
        if ($traceHandler)
        {
            $traceHandler->SetLogLevel($logLevel);
        }
        return true;
    }

    public static function RemoveTraceHandler($key)
    {
        if (isset(self::$_traceHandlers[$key]))
        {
            unset(self::$_traceHandlers[$key]);
        }

        return true;
    }
    public static function GetLogLevelText($logLevel)
    {
        $levelText = "Unknown Log Level";
        switch ($logLevel)
        {
            case Tracing::$LOGLEVEL_INFO:
                $levelText = "INFO";
                break;
            case Tracing::$LOGLEVEL_WARNING:
                $levelText = "WARNING";
                break;
            case Tracing::$LOGLEVEL_ERROR:
                $levelText = "WARNING";
                break;
            case Tracing::$LOGLEVEL_FATAL:
                $levelText = "FATAL";
                break;
        }
        return $levelText;
    }

    /**
     * Send a message to the attached trace handlers.
     *
     * @param string $msg
     * @param int $logLevel Only handlers set to listen at or above this level will handle the trace
     * @param \DblEj\Logging\ITraceHandler $traceHandler If specified, then this handler is used instead of the internal handler list
     * @throws NoTracingHandlerException
     */
    public static function Trace($msg, $fileName, $lineNumber, $logLevel, ITraceHandler $traceHandler = null)
    {
        if ($traceHandler)
        {
            $traceHandler->HandleTrace($msg, $fileName, $lineNumber, $logLevel);
        }
        elseif (self::$_traceHandlers)
        {
            foreach (self::$_traceHandlers as $traceHandler)
            {
                $traceHandler->HandleTrace($msg, $fileName, $lineNumber, $logLevel);
            }
        }
        else
        {
            throw new NoTracingHandlerException($msg, $logLevel);
        }
    }

    public static function TraceInfo($msg, $fileName, $lineNumber, ITraceHandler $traceHandler = null)
    {
        self::Trace($msg, $fileName, $lineNumber, self::$LOGLEVEL_INFO, $traceHandler);
    }

    public static function TraceWarning($msg, $fileName, $lineNumber, ITraceHandler $traceHandler = null)
    {
        self::Trace($msg, $fileName, $lineNumber, self::$LOGLEVEL_WARNING, $traceHandler);
    }

    public static function TraceError($msg, $fileName, $lineNumber, ITraceHandler $traceHandler = null)
    {
        self::Trace($msg, $fileName, $lineNumber, self::$LOGLEVEL_ERROR, $traceHandler);
    }

    public static function TraceFatal($msg, $fileName, $lineNumber, ITraceHandler $traceHandler = null)
    {
        self::Trace($msg, $fileName, $lineNumber, self::$LOGLEVEL_FATAL, $traceHandler);
    }

    public static function TraceException(\Exception $ex, ITraceHandler $traceHandler = null)
    {
        switch ($ex->getCode())
        {
            case E_NOTICE:
            case E_USER_NOTICE:
            case E_STRICT:
                self::TraceInfo($ex->getMessage() . ", " . $ex->getTraceAsString(), $ex->getFile(), $ex->getLine(), $traceHandler);
                break;
            case E_ERROR:
            case E_USER_ERROR:
            case E_COMPILE_ERROR:
            case E_PARSE:
            case E_RECOVERABLE_ERROR:
                self::TraceError($ex->getMessage() . ", " . $ex->getTraceAsString(), $ex->getFile(), $ex->getLine(), $traceHandler);
                break;
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
                self::TraceFatal($ex->getMessage() . ", " . $ex->getTraceAsString(), $ex->getFile(), $ex->getLine(), $traceHandler);
                break;
            default:
                self::TraceWarning($ex->getMessage() . ", " . $ex->getTraceAsString(), $ex->getFile(), $ex->getLine(), $traceHandler);
                break;
        }
    }
}