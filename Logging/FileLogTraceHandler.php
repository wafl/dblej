<?php

namespace DblEj\Logging;

class FileLogTraceHandler
extends TraceHandlerBase
{
    const OPEN_MODE_ONCE         = 1;
    const OPEN_MODE_EVERY_FLUSH  = 2;
    //@todo add more flush modes
    const FLUSH_MODE_EVERY_TRACE = 1;

    private $_fileHandle;
    private $_logBuffer;

    function __construct($fileName, $logLineFormat = "D,L,T,M", $dateFormat = "Y m d G i", $domainNameOrAppIdentifier = null, $fileOpenMode = self::OPEN_MODE_ONCE, $flushMode = self::FLUSH_MODE_EVERY_TRACE)
    {
        $options = new \DblEj\Configuration\OptionList();
        $options->AddOption("FileName", null);
        $options->AddOption("FileOpenMode", self::OPEN_MODE_ONCE);
        $options->AddOption("FlushMode", self::FLUSH_MODE_EVERY_TRACE);

        $options->SetOption("FileName", $fileName);
        $options->SetOption("FileOpenMode", $fileOpenMode);
        $options->SetOption("FlushMode", $flushMode);

        parent::__construct($logLineFormat, $dateFormat, $domainNameOrAppIdentifier, $options);

        if ($this->Get_FileOpenMode() == self::OPEN_MODE_ONCE)
        {
            $this->_fileHandle = fopen($fileName, "a");
        }
        $this->_logBuffer = "";
    }

    function __destruct()
    {
        if ($this->_fileHandle && is_resource($this->_fileHandle))
        {
            fclose($this->_fileHandle);
        }
        $this->_fileHandle = null;
    }

    public function Get_FlushName()
    {
        return $this->GetOptionValue("FileName");
    }

    public function Get_FileOpenMode()
    {
        return $this->GetOptionValue("FileOpenMode");
    }

    public function Get_FlushMode()
    {
        return $this->GetOptionValue("FlushMode");
    }

    public function OnTrace($msg, $logLevel, $logLevelText, $timeStamp, $originalMessage)
    {
        $this->_logBuffer .= $msg;
        switch ($this->Get_FlushMode())
        {
            case FLUSH_MODE_EVERY_TRACE:
                $this->_flush();
                break;
        }
    }

    private function _flush()
    {
        if ($this->Get_FileOpenMode() == self::OPEN_MODE_EVERY_FLUSH)
        {
            if (!$this->_fileHandle || !is_resource($this->_fileHandle))
            {
                $this->_fileHandle = fopen($this->Get_FlushName(), "a");
            }
        }
        $buffer           = $this->_logBuffer;
        $this->_logBuffer = ""; //todo: Make this transactional, perhaps using DblEj\Coding\Lockable to be sure we dont erase newly logged stuff
        fwrite($this->_fileHandle, $buffer);
        if ($this->Get_FileOpenMode() == self::OPEN_MODE_EVERY_FLUSH)
        {
            fclose($this->_fileHandle);
        }
    }

    public function GetAllowedOptions()
    {
        return array(
            "FileName",
            "FileOpenMode",
            "FlushMode");
    }
}