<?php

namespace DblEj\Logging;

/**
 * Provides methods for handling tracing calls by the application.
 */
interface ITraceHandler
{

    public function HandleTrace($msg, $fileName, $lineNumber, $logLevel);

    public function SetOption($optionName, $optionValue);

    public function GetOption($optionName);

    public function GetOptions();

    public function GetAllowedOptions();

    public function SetLogLevel($logLevel);
}