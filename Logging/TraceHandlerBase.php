<?php

namespace DblEj\Logging;

use DblEj\Collections\InvalidKeyException,
    DblEj\Configuration\InvalidOptionException,
    DblEj\Configuration\OptionList,
    Exception;

/**
 * Base class for server-side trace handlers.
 */
abstract class TraceHandlerBase
implements ITraceHandler
{
    protected $_logLineFormat = "D,L,T,M";
    protected $_dateFormat    = "Y m d G i";
    protected $_options;
    protected $_domainNameOrAppId;
    private $_currentLogLevel = Tracing::LOGLEVEL_WARNING;

    public function __construct($domainNameOrAppIdentifier = null, $logLineFormat = "[%D] [%L] [%T] [%F at %N] %M", $dateFormat = "Y-m-d G:i", OptionList $options = null, $logLevel = Tracing::LOGLEVEL_WARNING)
    {
        $this->_logLineFormat     = $logLineFormat;
        $this->_dateFormat        = $dateFormat;
        $this->_domainNameOrAppId = $domainNameOrAppIdentifier;
        $this->_currentLogLevel   = $logLevel;
        if ($options)
        {
            $this->_options = $options;
        }
        else
        {
            $this->_options = new OptionList();

            $allOptions = $this->GetAllowedOptions();
            foreach ($allOptions as $optionName => $defaultValue)
            {
                $this->_options->AddOption($optionName, $defaultValue);
            }
        }
    }

    abstract public function OnTrace($msg, $logLevel, $logLevelText, $timeStamp, $originalMessage);

    public function OnSetOption($optionName, $optionValue)
    {
        $this->_options->SetOption($optionName, $optionValue);
    }

    final public function SetOption($optionName, $optionValue)
    {
        if (isset($this->GetAllowedOptions()[$optionName]))
        {
            $this->OnSetOption($optionName, $optionValue); //did it this way so user can override event if they way
        }
        else
        {
            throw new InvalidKeyException($optionName);
        }
    }

    final public function GetOption($optionName)
    {
        if ($this->_options->ContainsKey($optionName))
        {
            return $this->_options->GetOption($optionName);
        }
        else
        {
            return null;
        }
    }

    final public function GetOptions()
    {
        return $this->_options;
    }

    final public function GetOptionValue($optionName)
    {
        $option = $this->_options->GetOption($optionName);
        if (!$option)
        {
            throw new InvalidOptionException($optionName);
        }
        return $option->Get_CurrentValue();
    }

    /**
     * Set the lowest level that this trace handler should handle.
     * @param int $logLevel A constant from one of the following: Tracing::LOGLEVEL_INFO, Tracing::LOGLEVEL_WARNING, Tracing::LOGLEVEL_ERROR, Tracing::LOGLEVEL_FATAL
     * @throws Exception If an invalid $logLevel is specified.
     */
    public function SetLogLevel($logLevel)
    {
        if ($logLevel < Tracing::LOGLEVEL_INFO || $logLevel > Tracing::LOGLEVEL_FATAL)
        {
            throw new Exception("Invalid log level specified");
        }
        $this->_currentLogLevel = $logLevel;
    }

    /**
     *
     * @param string $msg
     * @param LOG_LEVEL $logLevel
     * @todo Allow escaping so if they want a literal D, L, T, or M they can get one
     */
    final public function HandleTrace($msg, $fileName, $lineNumber, $logLevel)
    {
        if ($this->_currentLogLevel <= $logLevel)
        {
            $timeStamp        = date($this->_dateFormat);
            $formattedLogLine = $this->_logLineFormat;
            $formattedLogLine = str_replace(array(
                "%D",
                "%L",
                "%T",
                "%F",
                "%N",
                "%M"), array(
                $this->_domainNameOrAppId,
                Tracing::GetLogLevelText($logLevel),
                $timeStamp,
                $fileName,
                $lineNumber,
                $msg), $formattedLogLine);
            $this->OnTrace($formattedLogLine, $logLevel, Tracing::GetLogLevelText($logLevel), $timeStamp, $msg);
        }
    }
}