<?php

namespace DblEj\Logging;

/**
 * A trace handler that writes all traces with php's error_log.
 */
class PhpLogTraceHandler
extends TraceHandlerBase
{
    const LOGTYPE_SYSTEM = 0;
    const LOGTYPE_EMAIL  = 1;
    const LOGTYPE_FILE   = 3;
    const LOGTYPE_SAPI   = 4;

    public function GetAllowedOptions()
    {
        return array(
            "PhpLogType"             => self::LOGTYPE_SYSTEM,
            "DestinationEmailOrFile" => null,
            "EmailHeaders"           => null);
    }

    public function OnTrace($msg, $logLevel, $logLevelText, $timeStamp, $originalMessage)
    {
        error_log($msg, $this->GetOptionValue("PhpLogType"), $this->GetOptionValue("DestinationEmailOrFile"), $this->GetOptionValue("EmailHeaders"));
    }
}