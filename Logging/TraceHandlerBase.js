/*
 * @namespace DblEj.Logging
 */
Namespace("DblEj.Logging");

/**
 * @class TraceHandlerBase Base class for client-side trace handlers.
 * @extends Class
 * @return {DblEj.Logging.TraceHandlerBase}
 */
DblEj.Logging.TraceHandlerBase = Class.extend({
    /**
     * @constructor
     * @param {int} logLevel One of the constants from DblEj.Logging.Tracing.LOGLEVEL_*
     */
    init: function (logLevel)
    {
        if (IsDefined(logLevel))
        {
            this._logLevel = logLevel;
        } else {
            this._logLevel = DblEj.Logging.Tracing.LOGLEVEL_WARNING;
        }
    },
    /**
     * Set the lowest log level to trace
     * @param {int} logLevel One of the constants from DblEj.Logging.Tracing.LOGLEVEL_*
     * @returns {DblEj.Logging.TraceHandlerBase}
     */
    SetLogLevel: function(logLevel)
    {
        this._logLevel = logLevel;
        return this;
    },
    /**
     * Handle a trace
     * @param {string} $msg
     * @param {int} logLevel One of the constants from DblEj.Logging.Tracing.LOGLEVEL_*
     * @returns {DblEj.Logging.TraceHandlerBase}
     */
    HandleTrace: function (msg, logLevel)
    {
        if (this._logLevel <= logLevel)
        {
            this.OnTrace(msg, logLevel);
        }
        return this;
    },
    OnTrace: function(msg, logLevel) { throw "Trace handlers must implement the OnTrace method"; }
});