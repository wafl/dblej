/*
 * @namespace DblEj.Logging
 */
Namespace("DblEj.Logging");

/*
 * @class Tracing Utility class provides the Trace() method for client-side tracing.
 * @static
 */
DblEj.Logging.Tracing =
{
    LOGLEVEL_INFO: 1,
    LOGLEVEL_WARNING: 2,
    LOGLEVEL_ERROR: 3,
    LOGLEVEL_FATAL: 4,
    _traceHandlers: []
};

DblEj.Logging.Tracing.AddTraceHandler = function(traceHandler, logLevel)
{
    if (IsDefined(logLevel) && logLevel != null)
    {
        traceHandler.SetLogLevel(logLevel);
    }
    this._traceHandlers[this._traceHandlers.length] = traceHandler;

    return true;
};

/**
 * @function Trace
 * Send the speicifed message to subscribed trace handlers.
 *
 * @param {String} message
 * @param {Integer} $logLevel
 * @returns {undefined}
 */
DblEj.Logging.Tracing.Trace = function (message, logLevel)
{
    if (!IsDefined(logLevel) || logLevel == null)
    {
        logLevel = DblEj.Logging.Tracing.LOGLEVEL_WARNING;
    }
    for (var traceHandlerIdx=0; traceHandlerIdx < this._traceHandlers.length; traceHandlerIdx++)
    {
        this._traceHandlers[traceHandlerIdx].HandleTrace(message, logLevel);
    }

    return true;
};