<?php

namespace DblEj\Logging;

/**
 * All traces are sent to php's output (usually a web browser or a terminal).
 */
class EchoConsoleTraceHandler
extends TraceHandlerBase
{

    public function OnTrace($msg, $logLevel, $logLevelText, $timeStamp, $originalMessage)
    {
        print $msg . PHP_EOL;
    }

    public function GetAllowedOptions()
    {
        return array();
    }
}