<?php

namespace DblEj\Logging;

/**
 * A trace handler that ignores all traces.
 */
class NullTraceHandler
implements ITraceHandler
{

    public function GetAllowedOptions()
    {
        return array();
    }

    public function HandleTrace($msg, $fileName, $lineNumber, $logLevel)
    {
        
    }

    public function SetOption($optionName, $optionValue)
    {

    }

    public function GetOption($optionName)
    {
        
    }

    public function GetOptions()
    {

    }

    public function SetLogLevel($logLevel)
    {

    }
}