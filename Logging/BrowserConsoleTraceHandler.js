/*
 * @namespace DblEj.Logging
 */
Namespace("DblEj.Logging");

/**
 * @class ControllerBase Base class for client-side controllers.
 * @extends Class
 * @return {Boolean}
 */
DblEj.Logging.BrowserConsoleTraceHandler = DblEj.Logging.TraceHandlerBase.extend ({
    OnTrace: function (message, logLevel)
    {
        if (IsDefined(console) && IsDefined(console.log))
        {
            console.log(message);
        }
        return true;
    }
});