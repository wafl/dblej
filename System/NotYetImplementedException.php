<?php

namespace DblEj\System;

/**
 * Thrown when a method that is not implemented yet is called.
 */
class NotYetImplementedException
extends \Exception
{

    public function __construct($appliesTo)
    {
        if (!is_string($appliesTo))
        {
            throw new InvalidArgumentException("appliesTo", "Must be a string");
        }
        parent::__construct("Not Yet Implemented: $appliesTo", E_ERROR, null);
    }
}