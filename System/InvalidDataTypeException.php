<?php

namespace DblEj\System;

/**
 * Thrown when an object or other data is treated in a way that is not consistent with it's data type.
 */
class InvalidDataTypeException
extends \Exception
{

    public function __construct($dataTypeName, $expectedType, $severity = E_WARNING)
    {
        $message = "Invalid data type: $dataTypeName.  Expecting $expectedType";
        parent::__construct($message, $severity, null);
    }
}