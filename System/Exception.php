<?php
namespace DblEj\System;

/**
 * The base class for all exceptions
 */
abstract class Exception
extends \Exception
{
    /**
     *
     * @var \DblEj\System\Exception
     */
    private $_innerException;

    protected $_publicDetails = null;

    /**
     * Construct the exception
     * @param string $message
     * @param int $severity a PHP error level value
     * @param \Exception $innerException
     * @param string $publicDetails
     */
    public function __construct($message, $severity = E_WARNING, \Exception $innerException = null, $publicDetails = null)
    {
        parent::__construct($message, $severity, $innerException);
        $this->_innerException = $innerException;
        $this->_publicDetails = $publicDetails;
    }

    public function Get_InnerException()
    {
        return $this->_innerException;
    }

    public function Get_ErrorMessage()
    {
        return $this->message;
    }

    public function Get_PublicDetails()
    {
        return $this->_publicDetails;
    }
}