<?php
namespace DblEj\System;

/**
 * Thrown when a file that doesn't exist or is invalid is referenced.
 */
class InvalidFileException
extends \DblEj\System\Exception
{

    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("$message", $severity, $inner);
    }
}