<?php
namespace DblEj\System;

/**
 * Thrown when an extension is needed for a requested operation, but the extension is not installed.
 */
class MissingPhpExtensionException
extends \DblEj\System\Exception
{

    public function __construct($extensionName, $message = "", $severity = E_WARNING, \Exception $innerException = null)
    {
        $message = "A required PHP Extension, $extensionName, is not installed." . $message;
        parent::__construct($message, $severity, $innerException);
    }
}