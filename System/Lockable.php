<?php

namespace DblEj\System;

/**
 * Base class for any class that needs to provide object-level locking.
 */
abstract class Lockable
extends \DblEj\EventHandling\EventRaiser
implements ILockable
{
    const LOCK_RETRIEVED = 9001;
    const LOCK_RELEASED  = 9002;

    private $_lockNames = [];

    /**
     * Get a lock of the specified name.
     * Once this lock is gotten, no one else can get this lock until it is released.
     * @param string $lockName
     * @return string|boolean The name of the lock if the lock operation is successful, otherwise <i>false</i>.
     */
    public function GetLock($lockName)
    {
        $returnLock = false;
        if (array_search($lockName, $this->_lockNames) === false)
        {
            $this->_lockNames[] = $lockName;
            $returnLock         = $lockName;
            $this->raiseEvent(new \DblEj\EventHandling\EventInfo(self::LOCK_RETRIEVED));
        }
        return $returnLock;
    }

    /**
     * Release the specified lock on this object.
     * @param string $lockName The name of the lock.
     * @return boolean <i>True</i> on success, otherwise <i>false</i>.
     */
    public function ReleaseLock($lockName)
    {
        $lockIdx = array_search($lockName, $this->_lockNames);
        if ($lockIdx !== false)
        {
            unset($this->_lockNames[$lockIdx]);
            $this->raiseEvent(new \DblEj\EventHandling\EventInfo(self::LOCK_RELEASED));
            return true;
        }
        else
        {
            return false;
        }
    }

    public function GetRaisedEventTypes()
    {
        return array(
            "LOCK_RETRIEVED" => LOCK_RETRIEVED,
            "LOCK_RELEASED"  => LOCK_RELEASED);
    }
}