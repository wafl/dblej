<?php

namespace DblEj\System;

/**
 * Thrown when an invalid argument is passed to a function.
 */
class InvalidArgumentException
extends \Exception
{

    public function __construct($argument, $reason = null, $severity = E_ERROR)
    {
        $message = "Invalid argument: $argument.";
        if ($reason)
        {
            $message .= "  $reason";
        }
        parent::__construct($message, $severity, null);
    }
}