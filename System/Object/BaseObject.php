<?php

namespace DblEj\System\Object;

/**
 * The base class for all classes.
 */
abstract class BaseObject
{
    private $_timeInstanstiated;

    public function __construct()
    {
        $this->_timeInstanstiated = time();
    }
}