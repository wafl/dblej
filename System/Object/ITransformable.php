<?php

namespace DblEj\System\Object;

/**
 * Interface for any classes that perform any sort of trsnformation.
 */
interface ITransformable
{

    public function Transform($input);
}