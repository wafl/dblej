<?php

namespace DblEj\System;

/**
 * Thrown when a deprecated item is used.
 */
class DeprecatedException
extends \Exception
{

    public function __construct($deprecatedObjectName, $deprecatedVersion, $replacementObjectName)
    {
        $message = "$deprecatedObjectName is deprecated as of $deprecatedVersion.  You should use $replacementObjectName instead.";
        parent::__construct($message, E_DEPRECATED, null);
    }
}