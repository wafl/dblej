<?php
namespace DblEj\System;

/**
 * Thrown when an object cannot be instantiated.
 */
class InstantiationFailedException
extends \DblEj\System\Exception
{

}