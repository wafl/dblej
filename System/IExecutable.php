<?php

namespace DblEj\System;

interface IExecutable
{
    public function Execute($command = null, $appliesTo = null, $parameters = null);
}
