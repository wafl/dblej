<?php

namespace DblEj\System;

/**
 * Exposes methods that can be used to lock access to an item so that it can only be used by a single
 * consumer at a time.
 */
interface ILockable
{

    /**
     *Lock the specified name on the lockable object.
     *
     * @param string $lockName The name of the lock to get.
     * @param string $timeout The amount of time, in seconds, to wait for the lock before timing out.
     * This can help reduce/prevent waiting too long for a lock and potential dead-locks and
     * other race-conditions.
     *
     * @return The lock returned by the database engine.
     */
    public function GetLock($lockName, $timeout = null);

    /**
     * Release the lock of the specified name.
     *
     * @param string $lockName The name of the lock to release.
     */
    public function ReleaseLock($lockName);
}