<?php

namespace DblEj\System;

/**
 * Exposes the <i>Run</i> method for any object that can be ran.
 */
interface IRunnable
{
    public function Run(&$exitCode);
}