<?php
namespace DblEj\Internationalization;

/**
 * Provides a single method, GetTranslatedText, for translating text to a localized language.
 * Each implementation of ITranslator should be for a single locale and should be able
 * get the correct text for that locale.
 */
interface ITranslator
{

    /**
     * Lookup the text based on the specified <i>$textName</i> and
     * return the version for this translator's locale.
     *
     * @param string $textName The name of the text to translate.
     * @return string The translated text.
     */
    public function GetTranslatedText($textName);
}