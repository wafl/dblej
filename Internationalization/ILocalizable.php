<?php

namespace DblEj\Internationalization;

/**
 * Provides methods for localizing an object.
 */
interface ILocalizable
{

    /**
     * Set the current locale that the object should use when interpreting or generating any localizable data.
     *
     * @param string $code The locale code.
     */
    public function SetLocaleByCode($code);

    /**
     * Set the current locale that the object should use when interpreting or generating any localizable data.
     *
     * @param Locale The locale.
     */
    public function SetLocale(Locale $locale);
}