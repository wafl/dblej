<?php
namespace DblEj\Internationalization;

/**
 * Represents a locale.
 */
class Locale
implements ITranslator
{
    private $_code;
    private $_translator;

    /**
     * Create an instance of this locale.
     * @param string $code The locale's code.
     * @param \DblEj\Internationalization\ITranslator $translator The translator for this locale.
     */
    public function __construct($code, ITranslator $translator)
    {
        $this->_translator = $translator;
        $this->_code       = $code;
    }

    /**
     * Get's a value for the specified lookup string that is appropriate for this locale.
     *
     * @param string $lookupString Usually a token name representing a certain piece of text.
     *
     * @return string The translated text.
     */
    public function GetTranslatedText($lookupString)
    {
        return $this->_translator->GetTranslatedText($lookupString);
    }
}