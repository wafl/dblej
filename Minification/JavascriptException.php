<?php

namespace DblEj\Minification;

/**
 * Thrown when there is an unexpected condition or event while minifying javascript.
 * @deprecated since revision 1630 in favor of DblEj\Text\Minifiers
 */
class JavascriptException
extends \Exception
{

}