<?php
namespace DblEj\Minification;

/**
 * HTML minifier.
 * @deprecated since revision 1630 in favor of DblEj\Text\Minifiers
 */
class Html
implements IMinifier
{
    public static function Minify($html)
    {
        $html            = str_replace("<code", "<!--NOMINIFY--><code", $html);
        $html            = str_replace("</code>", "</code><!--ENDNOMINIFY-->", $html);
        $html            = str_replace("<pre", "<!--NOMINIFY--><pre", $html);
        $html            = str_replace("</pre>", "</pre><!--ENDNOMINIFY-->", $html);
        $htmlPieces      = explode("<!--NOMINIFY-->", $html);
        $html            = "";
        $nominifyStarted = false;
        foreach ($htmlPieces as $htmlPiece)
        {
            if ($nominifyStarted)
            {
                $nominifyPieces = explode("<!--ENDNOMINIFY-->", $htmlPiece);
                $html .= $nominifyPieces[0];
                if (count($nominifyPieces) > 1)
                {
                    $minifiedPiece = preg_replace('/\h\h\h+/', "", $nominifyPieces[1]);
                    $minifiedPiece = preg_replace('/\n+/', " ", $minifiedPiece);
                    $html .= $minifiedPiece;
                }
                $nominifyStarted = false;
            }
            else
            {
                $nominifyPieces = explode("<!--ENDNOMINIFY-->", $htmlPiece);
                if (count($nominifyPieces) > 1)
                {
                    $html .= $nominifyPieces[0];
                    $minifiedPiece = preg_replace('/\h\h\h+/', "", $nominifyPieces[1]);
                    $minifiedPiece = preg_replace('/\n+/', " ", $minifiedPiece);
                    $html .= $minifiedPiece;
                }
                else
                {
                    $minifiedPiece = preg_replace('/\h\h\h+/', "", $nominifyPieces[0]);
                    $minifiedPiece = preg_replace('/\n+/', " ", $minifiedPiece);
                    $html .= $minifiedPiece;
                }
                $nominifyStarted = true;
            }
        }
        return $html;
    }

    public static function SetOption($optionName, $optionValue)
    {
        throw new \Exception("Invalid option sent to Html minifier");
    }

    public static function GetAvailableOptions()
    {
        return array();
    }
}