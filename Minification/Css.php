<?php

namespace DblEj\Minification;

/**
 * A simple CSS minifier.
 * @deprecated since revision 1630 in favor of DblEj\Text\Minifiers
 *
 * <code>
 * $minifiedCss =\DblEj\Minification\Css::minify($css);
 * </code>
 *
 * Options:
 *
 * remove-last-semicolon: Removes the last semicolon in
 * the style definition of an element (activated by default).
 *
 * preserve-urls: Preserves every url defined in an url()-
 * expression. This option is only required if you have
 * defined really uncommon urls with multiple spaces or
 * combination of colon, semi-colon, braces with leading or
 * following spaces.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @package 	\DblEj\Minification
 * @author 		Joe Scylla <joe.scylla@gmail.com>
 * @copyright 	2008 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 	2014 The Wafl Group <wafl@wafl.org>
 * @license 	http://opensource.org/licenses/mit-license.php MIT License
 * @version 	1.0.2 (2014-05-09)
 *
 * THIS HAS BEEN MODIFIED FROM ITS ORIGINAL VERSION and repackaged for the DblEj library and licenced under GPL2
 *
 */
class Css
implements IMinifier
{
    private static $_removeLastSemiColon = true;
    private static $_preserveUrls        = false;

    /**
     * Minifies Css stylesheet
     *
     * <code>
     * $css_minified = Css::Minify($cssFileContent);
     * </code>
     *
     * @param	string			$css		Stylesheet definitions as string
     *
     * @return	string			Minified Css stylesheet
     */
    public static function Minify($css)
    {
        if (self::$_preserveUrls)
        {
            // Encode url() to base64
            $css = preg_replace_callback("/url\s*\((.*)\)/siU", "cssmin_encode_url", $css);
        }
        // Remove comments
        $css = preg_replace("/\/\*[\d\D]*?\*\/|\t+/", " ", $css);
        // Replace CR, LF and TAB to spaces
        $css = str_replace(array(
            "\n",
            "\r",
            "\t"), " ", $css);
        // Replace multiple to single space
        $css = preg_replace("/\s\s+/", " ", $css);
        // Remove unneeded spaces
        $css = preg_replace("/\s*({|}|=|~|\+|>|\||;|:|,)\s*/", "$1", $css);
        if (self::$_removeLastSemiColon)
        {
            // Removes the last semicolon of every style definition
            $css = str_replace(";}", "}", $css);
        }
        $css = trim($css);
        if (self::$_preserveUrls)
        {
            // Decode url()
            $css = preg_replace_callback("/url\s*\((.*)\)/siU", "cssmin_encode_url", $css);
        }
        return $css;
    }

    /**
     * Encodes a url() expression.
     *
     * @param	array	$match
     * @return	string
     */
    function cssmin_encode_url($match)
    {
        return "url(" . base64_encode(trim($match[1])) . ")";
    }

    public static function GetAvailableOptions()
    {
        return array(
            "remove-last-semicolon",
            "preserve-urls");
    }

    public static function SetOption($optionName, $optionValue)
    {
        switch ($optionName)
        {
            case "remove-last-semicolon":
                self::$_removeLastSemiColon = $optionValue;
                break;
            case "preserve-urls":
                self::$_preserveUrls        = $optionValue;
                break;
        }
    }
}