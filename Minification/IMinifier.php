<?php

namespace DblEj\Minification;

/**
 * Provides methods for minification.
 * @deprecated since revision 1630 in favor of DblEj\Text\Minifiers
 */
interface IMinifier
{

    public static function Minify($source);

    public static function GetAvailableOptions();

    public static function SetOption($optionName, $optionValue);
}