<?php

namespace DblEj\Presentation;

use DblEj\Extension\IExtension;

/**
 * Provides methods for rendering Presentation Templates.
 *
 * @deprecated since revision 1629 in favor of DblEj\Presentation\Integration\ITemplateRenderer
 * @see \DblEj\Presentation\Integration\ITemplateRenderer
 */
interface ITemplateRenderer
extends IExtension
{

    /**
     *
     */
    public function PostInit();

    /**
     * Render the template.
     *
     * @param \DblEj\Presentation\ITemplate $template
     * @param \DblEj\Presentation\RenderOptions $renderOptions
     */
    public function Render(ITemplate $template, RenderOptions $renderOptions);

    /**
     * Check if the specified template exists.
     * @param string $template
     */
    public function DoesTemplateExist($template);

    /**
     *
     * @param string $cacheId
     */
    public function DoesCacheExist($templateFileName, $cacheId);

    public function DeleteCache($includeCompiled = false);

    /**
     *
     * @param string $string
     * @param \DblEj\Presentation\RenderOptions $renderOptions
     */
    public function RenderString($string, RenderOptions $renderOptions);

    /**
     *
     * @param string $folderPath
     * @param int $renderPriority
     */
    public function AddTemplateFolder($folderPath, $renderPriority = null);

    /**
     *
     */
    public function GetTemplateFolders();

    public function Get_IsReady();

    /**
     *
     * @param string $dataName
     * @param mixed $value
     */
    public function SetGlobalData($dataName, $value);

    /**
     *
     * @param string $dataName
     * @param mixed $value
     */
    public function SetGlobalDataReference($dataName, &$value);

    /**
     *
     * @param string $alias
     * @param string $staticClass
     */
    public function SetStaticData($alias, $staticClass);

    /**
     *
     * @param string $dataName
     */
    public function GetGlobalData($dataName);
}