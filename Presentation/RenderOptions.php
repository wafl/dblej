<?php

namespace DblEj\Presentation;

/**
 * Presentation Template rendering options.
 */
class RenderOptions
{
    private $_minifyWhenPossible       = true;
    private $_useServerSideCache        = true;
    private $_serverSideCacheTimeout = 3600;
    private $_key1;
    private $_key2;
    private $_tokenReplacements;
    private static $_defaultInstance;

    public static function GetDefaultInstance()
    {
        if (!self::$_defaultInstance)
        {
            self::$_defaultInstance = new RenderOptions(true, true);
        }
        return self::$_defaultInstance;
    }

    public function __construct($minifyWhenPossible = true, $useServerSideCache = true, $key1 = null, $key2 = null, $serverSideCacheTimeout = 3600)
    {
        $this->_minifyWhenPossible      = $minifyWhenPossible;
        $this->_useServerSideCache      = $useServerSideCache;
        $this->_key1                    = $key1;
        $this->_key2                    = $key2;
        $this->_tokenReplacements       = new \DblEj\Data\ArrayModel();
        $this->_serverSideCacheTimeout  = $serverSideCacheTimeout;
    }

    public function AddToken(array $tokenArray)
    {
        foreach ($tokenArray as $tokenVar => $tokenVal)
        {
            $this->_tokenReplacements[$tokenVar] = $tokenVal;
        }
    }

    public function AddObjectToken(\ArrayAccess $tokenArray)
    {
        foreach ($tokenArray as $tokenVar => $tokenVal)
        {
            $this->_tokenReplacements[$tokenVar] = $tokenVal;
        }
    }

    public function Get_MinifyWhenPossible()
    {
        return $this->_minifyWhenPossible;
    }
    public function Set_MinifyWhenPossible($minify)
    {
        $this->_minifyWhenPossible = $minify?true:false;
        return $this;
    }

    public function Get_UseServerSideCache()
    {
        return $this->_useServerSideCache;
    }

    public function Get_ServerSideCacheTimeout()
    {
        return $this->_serverSideCacheTimeout;
    }

    public function Get_Key1()
    {
        return $this->_key1;
    }

    public function Get_Key2()
    {
        return $this->_key2;
    }

    public function GetTokens()
    {
        return $this->_tokenReplacements;
    }
}