<?php

namespace DblEj\Presentation;

/**
 * Provides the Render method for objects that can be rendered by a template renderer.
 */
interface IRenderable
{

    /**
     * Render the renderable object.
     * @param \DblEj\Presentation\ITemplateRenderer $renderer
     * @param \DblEj\Presentation\RenderOptions $options
     */
    public function Render(ITemplateRenderer $renderer, \DblEj\Presentation\RenderOptions $options);
}