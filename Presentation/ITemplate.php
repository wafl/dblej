<?php

namespace DblEj\Presentation;

/**
 * Presentation Template
 */
interface ITemplate
{

    /**
     * The title.
     *
     * @return String
     */
    public function Get_Title();

    /**
     * The filename.
     *
     * @return String
     */
    public function Get_Filename();

    /**
     * @param String $title
     */
    public function Set_Title($title);

    /**
     *
     * @param String $filename
     */
    public function Set_Filename($filename);
}