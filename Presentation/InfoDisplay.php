<?php

namespace DblEj\Presentation;

/**
 * Encapsulates information to be displayed for human consumption.
 */
class InfoDisplay
{
    private $_title;
    private $_description;
    private $_finePrint;

    /**
     *
     * @param string $title
     * @param string $description
     * @param string $finePrint
     */
    public function __construct($title, $description = null, $finePrint = null)
    {
        $this->_title       = $title;
        $this->_description = $description;
        $this->_finePrint   = $finePrint;
    }

    /**
     * The message title.
     * @return string
     */
    public function Get_Title()
    {
        return $this->_title;
    }

    /**
     * The message to display.
     * @return string
     */
    public function Get_Description()
    {
        return $this->_description;
    }

    /**
     * Any qualifications or quantifications to the description.
     * @return string
     */
    public function Get_FinePrint()
    {
        return $this->_finePrint;
    }
}