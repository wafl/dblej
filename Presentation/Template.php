<?php

namespace DblEj\Presentation;

/**
 * A Presentation Template.
 */
class Template
implements ITemplate
{
    private $_title;
    private $_filename;

    /**
     *
     * @param string $title
     * @param string $filename
     */
    public function __construct($title, $filename)
    {
        $this->_title    = $title;
        $this->_filename = $filename;
    }

    public function Get_Filename()
    {
        return $this->_filename;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    /**
     *
     * @param string $filename
     */
    public function Set_Filename($filename)
    {
        $this->_filename = $filename;
    }

    /**
     *
     * @param string $title
     */
    public function Set_Title($title)
    {
        $this->_title = $title;
    }

    public function RenderHtml(Integration\ITemplateRenderer $renderer, $replaceVariables = [])
    {
        $renderOptions = new \DblEj\Presentation\RenderOptions(true, false);
        if ($replaceVariables != null && count($replaceVariables) > 0)
        {
            $renderOptions->AddToken($replaceVariables);
        }
        return $renderer->Render($this, $renderOptions);
    }
}