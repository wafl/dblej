<?php

namespace DblEj\Presentation\Integration;

/**
 * Provides methods for implementing a font provider driver, where a font provider is some service that outputs CSS fonts and related stylesheets.
 */
interface IFontProvider
{

    /**
     * The name of the provider, ie "Google"
     *
     * @return string
     */
    public function Get_Title();

    /**
     * The url to the stylesheet, a string with the following tokens:
     * %f=font-family, %a=provider-args
     * note: token replacement values will be url encoded
     * example (assuming family="Open Sans" and arguments=":400,700,400italic"):
     * http://fonts.googleapis.com/css?family=%f:%a
     *
     * @return string
     */
    public function Get_StylesheetBaseUrl();
}