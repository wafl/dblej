<?php

namespace DblEj\Presentation\Integration;

use DblEj\Extension\IExtension;

/**
 * Provides methods for rendering Presentation Templates.
 */
interface ITemplateRendererExtension
extends ITemplateRenderer, IExtension
{
}