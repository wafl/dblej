<?php

namespace DblEj\Presentation\Integration;

interface IFontProviderExtension
extends IFontProvider, \DblEj\Extension\IExtension
{
}