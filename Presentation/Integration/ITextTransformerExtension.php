<?php

namespace DblEj\Presentation\Integration;

use DblEj\Extension\IExtension;

interface ITextTransformerExtension
extends ITextTransformer, IExtension
{
}
