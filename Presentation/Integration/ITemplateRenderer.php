<?php

namespace DblEj\Presentation\Integration;

/**
 * Provides methods for rendering Presentation Templates.
 */
interface ITemplateRenderer
{
    /**
     *
     */
    public function PostInit();

    /**
     * Render the template.
     *
     * @param \DblEj\Presentation\ITemplate $template
     * @param \DblEj\Presentation\RenderOptions $renderOptions
     */
    public function Render(\DblEj\Presentation\ITemplate $template, \DblEj\Presentation\RenderOptions $renderOptions);

    /**
     * Check if the specified template exists.
     * @param string $template
     */
    public function DoesTemplateExist($template);

    /**
     *
     * @param string $cacheId
     */
    public function DoesCacheExist($templateFileName, $cacheId);

    public function DeleteCache($includeCompiled = false);

    /**
     *
     * @param string $string
     * @param \DblEj\Presentation\RenderOptions $renderOptions
     */
    public function RenderString($string, \DblEj\Presentation\RenderOptions $renderOptions);

    /**
     *
     * @param string $folderPath
     * @param int $renderPriority
     */
    public function AddTemplateFolder($folderPath, $renderPriority = null);

    /**
     *
     */
    public function GetTemplateFolders();

    public function Get_IsReady();

    /**
     *
     * @param string $dataName
     * @param mixed $value
     */
    public function SetGlobalData($dataName, $value);

    /**
     *
     * @param string $dataName
     * @param mixed $value
     */
    public function SetGlobalDataReference($dataName, &$value);

    /**
     *
     * @param string $alias
     * @param string $staticClass
     */
    public function SetStaticData($alias, $staticClass);

    /**
     *
     * @param string $dataName
     */
    public function GetGlobalData($dataName);
}