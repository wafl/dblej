<?php

namespace DblEj\Text\Parsers;

/**
 * Thrown when the string can't be parsed
 *
 * @deprecated since revision 1630 in favor of DblEj\Text\Parsing
 */
class UnableToParseException
extends \Exception
{

    function __construct($parseTypeString, $reason = "unknown", $severity = E_ERROR, $inner = null)
    {
        $message = "Unable to parse $parseTypeString (reason: $reason)";
        parent::__construct($message, $severity, $inner);
    }
}