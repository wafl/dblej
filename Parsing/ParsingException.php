<?php

namespace DblEj\Text\Parsers;

/**
 * Thrown when there is an error while parsing.
 *
 * @deprecated since revision 1630 in favor of DblEj\Text\Parsing
 */
class ParsingException
extends \DblEj\System\Exception
{
    private $_lineNum;
    private $_charPos;

    public function __construct($lineNumber = null, $charPos = null, $message = "", $severity = E_ERROR, $inner = null)
    {

        $this->_lineNum = $lineNumber;
        $this->_charPos = $charPos;
        parent::__construct("There was an error parsing the string.  Line $lineNumber Pos $charPos\n$message", $severity, $inner);
    }
}