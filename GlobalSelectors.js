/**
 * @class Global
 */

/**
 * @function $ Get a specific element by its id
 *
 * @param {String} elementName the id of the element
 * @return {Element} the element if its found, otherwise null */
function $(elementName)
{
    var element = document.getElementById(elementName);
    return element;
}

/**
 * @function $$ Get all elements of the specified type
 *
 * @param {String} elementType the type of html element (the html tag name example: div, p, li)
 * @param {HTMLElement} [parentElement=document] all matches must be a descendat of this element
 *
 * @return {Element[]} an array of the matching elements
 */
function $$(elementType, parentElement)
{
    if (!IsDefined(parentElement))
    {
        parentElement = document;
    }
    var returnElements = parentElement.getElementsByTagName(elementType.toUpperCase());
    if (returnElements.length === 0)
    {
        returnElements = parentElement.getElementsByTagName(elementType.toLowerCase());
    }
    returnElements = Iteratize(returnElements);
    return returnElements;
}

/**
 * @function $class Get the first element (inside of parentElement, or the document if no parentElement specified) that has the specified class (and optionally tag type).
 *
 * @param {String} className The css class to search for
 * @param {String} [elementType=*] the type of html element (the html tag name example: div, p, li)
 * @param {HTMLElement} [parentElement=document] The parent element to search beneath
 *
 * @return {Element} the matching element, or null if there are no matches
 */
function $class(className, elementType, parentElement)
{
    var matches = $$class(className, elementType, parentElement);
    if (matches.length > 0)
    {
        return matches[0];
    } else {
        return null;
    }
}
/**
 * @function $$class
 * Get all elements (inside of parentElement, or the document if no parentElement specified) that have the specified class (and optionally tag type).
 *
 * @param {String} className The css class to search for
 * @param {String} [elementType=*] the type of html element (the html tag name example: div, p, li)
 * @param {HTMLElement} [parentElement=document] The parent element to search beneath
 *
 * @return {Element[]} an array of the matching elements
 */
function $$class(className, elementType, parentElement)
{
    var elements;
    var returnElements;
    if (IsNullOrUndefined(elementType))
    {
        elementType = "*";
    }
    elements = $$q("."+className, parentElement);

    if (elementType == "*")
    {
        returnElements = elements;
    } else {
        var element;
        returnElements = new Array;
        elementType = elementType.toUpperCase();
        for (var elemIdx = 0; elemIdx < elements.length; elemIdx++)
        {
            element = elements[elemIdx];
            if (element.tagName.toUpperCase() == elementType)
            {
                returnElements[returnElements.length] = element;
            }
        }
    }

    returnElements = Iteratize(returnElements);
    return returnElements;
}

/**
 * @function $$query Get elements that match the selector query
 *
 * @param {String} selectors
 * @param {HTMLElement} [parentElement=document] The parent element to search beneath
 *
 * @return {Element[]} an Iteratized Array of matching elements
 */
function $$query(selectors, parentElement)
{
    if (IsNullOrUndefined(parentElement) || (parentElement == window))
    {
        parentElement = document;
    }
    try
    {
        var elements = parentElement.querySelectorAll(selectors);
    }
    catch (err)
    {
        if (err.code == 12)
        {
            throw new Error("Invalid selector string passed: " + selectors);
        } else {
            throw err;
        }
    }
    return Iteratize(elements);
}

/**
 * @function $$q Get elements that match the selector query
 * Alias of $$query.
 *
 * @param {String} selectors
 * @param {HTMLElement} [parentElement=document] The parent element to search beneath
 *
 * @return {Element[]} an Iteratized Array of matching elements
 */
function $$q(selectors, parentElement)
{
    return $$query(selectors, parentElement);
}

/**
 * @function $query Get the first element that match the selector query
 *
 * @param {String} selectors The selectors to use for the query
 * @param {HTMLElement} [parentElement=document] The parent element to search beneath

 * @return {Element} Returns the matching element
 */
function $query(selectors, parentElement)
{
    if (!IsDefined(parentElement) || parentElement == window)
    {
        parentElement = document;
    }
    var matches = parentElement.querySelectorAll(selectors);
    if (matches.length > 0)
    {
        return matches[0];
    } else {
        return null;
    }
}

/**
 * @function $query Get the first element that match the selector query
 * Alias of $query.
 *
 * @param {String} selectors The selectors to use for the query
 * @param {HTMLElement} [parentElement=document] The parent element to search beneath

 * @return {Element} Returns the matching element
 */
function $q(selectors, parentElement)
{
    return $query(selectors, parentElement);
}

/**
 * @function $create create an element from html without placing it in the dom
 *
 * @param {String} elementString the html contents of the element
 *
 * @return {Element} the new element
 */
function $create(elementString)
{
    var div = document.createElement('div');
    div.innerHTML = elementString;

    var returnEl;
    if (div.childNodes.length > 1)
    {
        returnEl = div.childNodes;
    }
    else
    {
        returnEl = div.firstChild;
    }
    return returnEl;
}