<?php

namespace DblEj\Mvc;

/**
 * Thrown when there isn't a valid renderer for the MVC Views.
 */
class InvalidViewEngineException
extends \Exception
{

    public function __construct($viewEngineName = null, $severity = E_ERROR)
    {
        if ($viewEngineName)
        {
            parent::__construct("Invalid view engine specified (engine: $viewEngineName)", $severity, null);
        }
        else
        {
            parent::__construct("A valid View Engine extension (ex: smarty, twig) could not be found", $severity, null);
        }
    }
}