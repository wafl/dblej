<?php

namespace DblEj\Mvc;

/**
 * Provides methods for interfacing with a class that represents an MVC View.
 */
interface IView
{

    /**
     * @return \DblEj\Data\IModel The data model
     */
    public function Get_Model();

    public function GetModelData($dataName);

    public function SetModelData($dataName, $dataValue);

    public function Set_Model(\DblEj\Data\IModel $model);
}