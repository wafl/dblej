<?php

namespace DblEj\Mvc;

use DblEj\Presentation\ITemplateRenderer,
    DblEj\Mvc\SitePage;

/**
 * Response to an HTTP request in an MVC Web Application.
 */
class SitePageResponse
extends ViewResponse
implements \DblEj\Communication\Http\ISitePageResponse
{
    private $_sitePage;

    /**
     * Create an instance of the SitePageResponse class
     * @param SitePage $sitePage The site page to return to the server
     * @param ITemplateRenderer $renderer The view engine that will be used to render the site page
     * @param string $responseCode The HTTP response code to be returned to the server
     * @param mixed $headers an array of HTTP headers to be returned to the server
     * @param mixed $renderVersionKey1
     * @param mixed $renderVersionKey2
     * @param array $controls
     * @param RenderViewOptions $renderViewOptions
     */
    public function __construct(SitePage &$sitePage, $responseCode = self::HTTP_OK_200, \DblEj\Communication\Http\Headers $headers = null, $options = null)
    {
        $this->_sitePage = $sitePage;
        parent::__construct($this->_sitePage, $responseCode, $headers, $options);
    }

    /**
     * @return \DblEj\Mvc\SitePage
     */
    public function Get_SitePage()
    {
        return $this->_sitePage;
    }
}