<?php

namespace DblEj\Mvc;

use DblEj\Mvc\IView;

/**
 * An HTTP response that encapsulates an MVC view.
 */
class ViewResponse
extends \DblEj\Communication\Http\Response
{

    public function __construct(IView $view, $responseCode = self::HTTP_OK_200, \DblEj\Communication\Http\Headers $headers = null, $options = null)
    {
        parent::__construct($view, $responseCode, $headers, \DblEj\Communication\Http\Response::CONTENT_TYPE_RENDERABLE_OUTPUT, $options);
    }
}