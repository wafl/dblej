<?php

namespace DblEj\Mvc;

use DblEj\Data\IModel;

/**
 * A page (View) in an MVC Web Application.
 */
class SitePage
extends \DblEj\SiteStructure\SitePage
implements IView
{
    private $_viewName;

    /**
     *
     * @param string $uniqueId
     * @param string $title
     * @param string $description
     * @param string $controllerPath
     * @param string $presentationPath
     * @param integer $displayOrder
     * @param string $parentAreaId
     * @param boolean $isAreaDefault
     * @param string $fullTitle
     * @param string $keywords
     * @param boolean $preprocessCss
     * @param \DblEj\Extension\ControlBase[]|string[] $pageControls An array of control objects or if the engine isn't ready to instantiate controls then an array of control names
     */
    public function __construct($uniqueId, $title, $description, $controllerPath, $presentationPath, $displayOrder = 0, $parentAreaId = null, $isAreaDefault = false, $fullTitle = null, $keywords = null, $preprocessCss = false, $pageControls = [], $registeredBy = null)
    {
        parent::__construct($uniqueId, $title, $description, $controllerPath, $presentationPath, $displayOrder, $parentAreaId, $isAreaDefault, $fullTitle, $keywords, $preprocessCss, $pageControls, $registeredBy);
        $this->_tokenData = new \DblEj\Data\ArrayModel();
    }


    public function DoesClientControllerExist(\DblEj\Application\IMvcWebApplication $app)
    {
        return $this->DoesClientLogicExist($app);
    }

    /**
     *
     * @return string
     */
    public function GetServerController()
    {
        return $this->GetServerLogicFile();
    }

    /**
     *
     * @return string
     */
    public function GetClientController()
    {
        return $this->GetClientLogicFile();
    }

    /**
     *
     * @return string
     */
    public function Get_ControllerPath()
    {
        return $this->_logicPath;
    }

    /**
     *
     * @param string $filename
     */
    public function Set_ControllerPath($filename)
    {
        $this->_logicPath = $filename;
    }

    /**
     *
     * @return string
     */
    public function Get_ViewName()
    {
        return $this->_viewName;
    }

    /**
     * Get the data model.
     *
     * @return IModel
     */
    public function Get_Model()
    {
        return $this->_tokenData;
    }

    /**
     *
     * @param string $viewName
     */
    public function Set_ViewName($viewName)
    {
        $this->_viewName = $viewName;
    }

    /**
     *
     * @param \DblEj\Data\IModel $model
     */
    public function Set_Model(IModel $model)
    {
        $this->_tokenData = $model;
    }

    /**
     *
     * @param type $dataName
     * @return mixed
     */
    public function GetModelData($dataName)
    {
        return $this->Get_Model()->GetFieldValue($dataName);
    }

    /**
     *
     * @param string $dataName
     * @param mixed $dataValue
     */
    public function SetModelData($dataName, $dataValue)
    {
        return $this->Get_Model()->SetFieldValue($dataName, $dataValue);
    }

    /**
     *
     * @param \DblEj\Presentation\ITemplateRenderer $renderer
     * @param \DblEj\Presentation\RenderOptions $renderOptions
     * @return string The HTML of the rendered page.
     */
    public function Render(\DblEj\Presentation\Integration\ITemplateRenderer $renderer, \DblEj\Presentation\RenderOptions $renderOptions)
    {
        $renderOptions->AddToken(array("MODEL" => $this->Get_Model()));
        return parent::Render($renderer, $renderOptions);
    }
}