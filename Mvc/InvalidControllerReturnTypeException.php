<?php

namespace DblEj\Mvc;

/**
 * Thrown when a server side controller returns an unexpected response.
 */
class InvalidControllerReturnTypeException
extends \DblEj\Communication\Http\InvalidResponseException
{

    public function __construct($requestUri, $typeReceived)
    {
        parent::__construct($requestUri, "Invalid data returned from the controller.  I was expecting a \DblEj\Communication\Http\Response.  Instead, you sent a $typeReceived", E_ERROR, null);
    }
}