<?php

namespace DblEj\Mvc;

/**
 * An MVC View.
 */
class View
implements IView
{
    private $_viewName;
    private $_model;
    private $_templateFilename;

    public function __construct($viewName, \DblEj\Data\IModel $model, $templateFilename)
    {
        $this->_viewName         = $viewName;
        $this->_model            = $model;
        $this->_templateFilename = $templateFilename;
    }

    public function Get_ViewName()
    {
        return $this->_viewName;
    }

    public function Get_Model()
    {
        return $this->_model;
    }

    public function Get_TemplateFilename()
    {
        return $this->_templateFilename;
    }

    public function Set_ViewName($viewName)
    {
        $this->_viewName = $viewName;
    }

    public function Set_Model(\DblEj\Data\IModel $model)
    {
        $this->_model = $model;
    }

    public function Set_TemplateFilename($templateFilename)
    {
        $this->_templateFilename = $templateFilename;
    }

    public function GetModelData($dataName)
    {
        
    }

    public function SetModelData($dataName, $dataValue)
    {

    }
}