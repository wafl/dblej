<?php

namespace DblEj\Mvc;

/**
 * A collection of <i>IView</i>s.
 */
class ViewCollection
extends \DblEj\Collections\TypedKeyedCollection
{

    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray, "\DblEj\Mvc\IView");
    }

    /**
     * Get the IView that has the specified id.
     * @param string $viewId
     * @return \DblEj\Mvc\IView
     */
    public function GetView($viewId)
    {
        return $this->GetItem($viewId);
    }

    /**
     * Add a new view
     * @param IView $view
     */
    public function AddView(\DblEj\Mvc\IView $view)
    {
        return $this->AddItem($view, $view->Get_ViewName());
    }
}