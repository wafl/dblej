/**
 *
 * @namespace DblEj.Mvc
 */
Namespace("DblEj.Mvc");

/**
 * @class ControllerBase Base class for client-side controllers.
 * @extends Class
 * @return {DblEj.Mvc.ControllerBase}
 */
DblEj.Mvc.ControllerBase = Class.extend({
    init: function ()
    {
        /**
         * Whether or not the dependencies have finished loading
         * @memberOf DblEj.Mvc.ControllerBase
         * @type {Boolean}
         */
        if (this.GetModelDependencies().length == 0)
        {
            this.dependenciesLoaded = true;
        } else {
            this.dependenciesLoaded = false;
        }
    },
    Get_DefaultActionName: function ()
    {
        return "DefaultAction";
    },
    DefaultAction: function ()
    {
        throw "You must override the DefaultAction method";
    },
    /**
     * Get an array of the names of the model classes that should be available to the client.
     *
     * @returns {Array|null}
     */
    GetModelDependencies: function ()
    {
        return [];
    },
    /**
     * Code to run when the dependencies have finished loading
     * @returns {Void}
     */
    OnDependenciesLoaded: function ()
    {
        this.dependenciesLoaded = true;
    }
});