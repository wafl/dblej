<?php

namespace DblEj\Mvc;

use DblEj\Application\IMvcWebApplication,
    DblEj\Communication\Http\Headers,
    DblEj\Communication\Http\Request,
    DblEj\Communication\Http\Response,
    DblEj\Mvc\SitePageResponse,
    DblEj\Communication\HttpRequest,
    DblEj\Communication\JsonUtil,
    DblEj\Data\IModel,
    DblEj\Presentation\RenderOptions,
    Exception;

/**
 * Base class for server side controllers.
 */
abstract class ControllerBase
extends \DblEj\Communication\Http\RequestHandlerBase
{

    public function __construct()
    {

    }

    public function __call($name, $arguments)
    {
        if (!method_exists($this, $name))
        {
            if ($name == $this->Get_DefaultActionName())
            {
                throw new \Exception("You have set $name as the default action, but no such action method exists", E_ERROR);
            }
            else
            {
                $exception = new \Exception("Invalid method/action \"$name\" called on Controller " . get_called_class(), E_ERROR);
                throw $exception;
            }
        }
    }

    public function PrepareHttpResponse(Request $request, $requestStringOrObject, \DblEj\Application\IWebApplication $app)
    {
        $prepMethodName = "Prepare".$requestStringOrObject;
        if (is_string($requestStringOrObject) && method_exists($this, $prepMethodName))
        {
            return call_user_func(array($this, $prepMethodName), $request, $app);
        } else {
            return parent::PrepareHttpResponse($request, $requestStringOrObject, $app);
        }
    }

    public function HandleHttpRequest(\DblEj\Communication\Http\Request $request, $requestString, \DblEj\Application\IApplication $app)
    {
        $className            = get_class($this);
        $clientSideClassName  = str_replace("\\", ".", $className);
        $clientSideClassParts = explode(".", $clientSideClassName);
        $allInputs            = $request->GetAllInputs();
        $allInputsJson        = JsonUtil::EncodeJson($allInputs);
        $response             = call_user_func(array($this, $requestString), $request, $app);
        if (is_subclass_of($response, "\\DblEj\\Communication\\Http\\ISitePageResponse"))
        {
            $response->Set_LastModifiedTime($response->Get_SitePage()->GetLastModifiedTime($app));

            $testControllerJs = "";
            $testPartString   = "";
            foreach ($clientSideClassParts as $clientSideClassPart)
            {
                if ($testPartString)
                {
                    $testPartString .= ".";
                    $testControllerJs .= " && ";
                }
                $testPartString .= $clientSideClassPart;
                $testControllerJs .= "typeof $testPartString != 'undefined'";
            }
            $clientSideControllerAction = "
				<script>
					Start
					(
						function()
						{
							if ($testControllerJs)
							{
								document.clientController = new $clientSideClassName();
								var dependencies = document.clientController.GetModelDependencies();
								var dependenciesLoading = dependencies.length;
								if (!IsNullOrUndefined(dependencies))
								{
									dependencies = Iteratize(dependencies);
									dependencies.OnEach(
										function(dependency)
										{
											var dataModelPath = DblEj.Communication.JsonUtil._decodeLibBasePath + dependency + '.js?rev=".$app->Get_Settings()->Get_Application()->Get_Version()."';
											var functionalModelPath = DblEj.Communication.JsonUtil._decodeLibPath + dependency + '.js?rev=".$app->Get_Settings()->Get_Application()->Get_Version()."';
                                            var dataModelPath;
											DblEj.SiteStructure.SitePage.IncludeJs(dataModelPath,
											function()
											{
												DblEj.SiteStructure.SitePage.IncludeJs(functionalModelPath,
												function()
												{
													dependenciesLoading--;
													if (dependenciesLoading == 0)
													{
														document.clientController.OnDependenciesLoaded();
													}
												});
											});
										}
									);
								}
								if (typeof document.clientController.$requestString != 'undefined')
								{
									document.clientController.$requestString($allInputsJson);
								}
							}
						}
					);
				</script>";
            $response->Get_SitePage()->AddRawHeadHtml($clientSideControllerAction);
        }
        return $response;
    }


    /**
     * Create a default response that returns the specified SitePage.
     *
     * @param \DblEj\Mvc\SitePage $currentSitePage
     * @param RenderOptions $options
     * @param Headers $headers
     * @return \DblEj\Mvc\SitePageResponse
     */
    protected function createResponseFromSitePage(\DblEj\SiteStructure\SitePage $currentSitePage, RenderOptions $options, Headers $headers = null)
    {
        $httpResponse = new SitePageResponse($currentSitePage, Response::HTTP_OK_200, $headers, $options);
        return $httpResponse;
    }

    protected function actionRedirect(Request $request, $action)
    {
        $url = $request->Get_RequestUrl();
        $url = preg_replace("/&+Action=[a-zA-Z0-9-\\s%\\;]+/", "", $url);
        $url = preg_replace("/\\?+Action=[a-zA-Z0-9-\\s%\\;]+&+/", "?", $url);
        $url = preg_replace("/\\?+Action=[a-zA-Z0-9-\\s%\\;]+/", "", $url);
        if ($action != $this->Get_DefaultActionName())
        {
            if (stristr($url, "?") === false)
            {
                $url = $url . "?";
            }
            if (substr($url, strlen($url)-1) != "?")
            {
                $url = $url . "&";
            }
            $url = $url . "Action=$action";
        }
        return $this->createRedirectUrlResponse($url, false, true);
    }

    protected function Get_DefaultActionName()
    {
        return "DefaultAction";
    }

    abstract public function DefaultAction(Request $request, IMvcWebApplication $app);

    final protected function GetActionVarType()
    {
        return HttpRequest::INPUT_GET;
    }

    /**
     * The name of the request variable that contains the name of the <i>action</i>.
     * @return string
     */
    final protected function GetActionVarName()
    {
        return "Action";
    }
}