<?php

namespace DblEj\Mvc;

/**
 * Adds methods to the standard SiteMap that provide the ability
 * to populate a sitemap from the SiteStructure.
 */
class SiteMap
extends \DblEj\SiteStructure\SiteMap
{

    public function ParseSiteStructureConfig(array $siteStructureConfig, \DblEj\SiteStructure\SiteArea $parentArea = null, \DblEj\Application\IApplication $application = null)
    {
        return $this->ParseMvcSiteStructureConfig($siteStructureConfig, $parentArea, $application);
    }

    public function ParseMvcSiteStructureConfig(array $siteStructureConfig, \DblEj\SiteStructure\SiteArea $parentArea = null, \DblEj\Application\IMvcWebApplication $application = null)
    {
        $siteAreaDisplayOrder = 0;
        foreach ($siteStructureConfig as $itemName => &$itemChildren)
        {
            if (isset($itemChildren["Controller"]))
            {
                //this is a page
                $displayOrder  = isset($itemChildren["DisplayOrder"]) ? $itemChildren["DisplayOrder"] : 999;
                $isDefault     = isset($itemChildren["IsDefault"]) ? $itemChildren["IsDefault"] : false;
                $description   = isset($itemChildren["Description"]) ? $itemChildren["Description"] : "";
                $caption       = isset($itemChildren["Caption"]) ? $itemChildren["Caption"] : "";
                $keywords      = isset($itemChildren["Keywords"]) ? $itemChildren["Keywords"] : "";
                $preProcessCss = isset($itemChildren["PreProcessCss"]) ? $itemChildren["PreProcessCss"] : false;
                $pageControlNames  = isset($itemChildren["Controls"]) ? $itemChildren["Controls"] : [];
                if (!is_array($pageControlNames))
                {
                    if ($pageControlNames)
                    {
                        $pageControlNames = [$pageControlNames];
                    } else {
                        $pageControlNames = [];
                    }
                }

                if ($parentArea)
                {
                    $parentPathString = $parentArea->GetPathString(".");
                    $this->AddSitePage(new \DblEj\Mvc\SitePage($parentPathString . "." . $itemName, $itemName, $description, $itemChildren["Controller"], $itemChildren["Presentation"], $displayOrder, $parentArea->Get_AreaId(), $isDefault, $caption, $keywords, $preProcessCss, $pageControlNames, "MVC Site Structure Config parent"));
                }
                else
                {
                    $this->AddSitePage(new \DblEj\Mvc\SitePage($itemName, $itemName, $description, $itemChildren["Controller"], $itemChildren["Presentation"], $displayOrder, null, $isDefault, $caption, $keywords, $preProcessCss, $pageControlNames, "MVC Site Structure Config"));
                }
            }
            elseif (strtolower($itemName) != "menusettings")
            {
                //this is an area
                $siteAreaDisplayOrder++;

                $areaCaption      = $itemName;
                $areaMenuCssClass = "";

                //check for area properties
                if (!is_array($itemChildren))
                {
                    throw new \Exception("Error parsing Site Structure at item: $itemName");
                }
                foreach ($itemChildren as $pageChildName => $pageChild)
                {
                    switch (strtolower($pageChildName))
                    {
                        case "menusettings":
                            foreach ($pageChild as $pageChildSetting => $pageChildSettingValue)
                            {
                                switch (strtolower($pageChildSetting))
                                {
                                    case "caption":
                                        $areaCaption      = $pageChildSettingValue;
                                        break;
                                    case "cssclass":
                                        $areaMenuCssClass = $pageChildSettingValue;
                                        break;
                                }
                            }
                            break;
                    }
                }

                $siteArea = new \DblEj\SiteStructure\SiteArea($itemName, $areaCaption, $siteAreaDisplayOrder, 0, null, $parentArea, $areaMenuCssClass);
                $this->AddSiteArea($siteArea);
                $this->ParseMvcSiteStructureConfig($itemChildren, $siteArea, $application);
            }
        }
        return $this;
    }
}