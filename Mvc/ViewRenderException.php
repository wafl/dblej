<?php

namespace DblEj\Mvc;

/**
 * Thrown when there is an error while rendering the view.
 */
class ViewRenderException
extends \Exception
{

    public function __construct(IView $view, $severity = E_ERROR, $inner = null, $message = "")
    {
        $message = $message ? "There was an error while rendering the view.  $message" . $view->Get_ViewName() : "There was an error while rendering the view.  See inner-exception for more details." . $view->Get_ViewName();
        parent::__construct($message, $severity, $inner);
    }
}