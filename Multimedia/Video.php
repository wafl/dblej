<?php

namespace DblEj\Multimedia;

/**
 * Represents an video file.
 *
 * Provides methods for saving different file formats of various bitrate, framerate, and size.
 *
 * Requires the GD2 library.
 */
class Video
implements IVideoFile
{
    private $_originalFilename;
    private static $_converterPath = "ffmpeg";

    public function __construct($fileName)
    {
        $this->_originalFilename = $fileName;
    }

    /**
     * Specify the path to the converter application (ffmpeg).
     * If ffmpeg is on the <i>PATH</i>, then you do not need to set this.
     *
     * @param string $path The absolute path to the video converter application.
     *
     * @return \DblEj\Multimedia\Video
     * Returns <i>this</i>, allowing for chained calls to this object.
     */
    public static function Set_ConverterPath($path)
    {
        self::$_converterPath = $path;
    }

    /**
     * Save the video to disk.
     *
     * @param string $fileName
     * The destination video file name.  If it does not exist it will be created.
     * If it exists, it will be overwritten.
     *
     * @param string $title
     * A human-readable friendly name for this video.
     * The <i>title</i> will be tagged to the video as metadata when possible.
     *
     * @param int $width
     * The destination video width, in pixels.
     *
     * @param int $height
     * The destination video height, in pixels.
     *
     * @param int $vbrQuality
     * When specified, the resulting video will have a variable bit rate.
     * The resulting bitrate will be whatever is needed to maintain the specified quality.
     * Between 1 and 30 where 1 is the worst quality and 30 is the best quality.
     *
     * @param int $bitrateKhz
     * When specified, the resulting video will have a fixed bitrate of this amount.
     *
     * @param int $fps
     * The resulting video frames per second.
     *
     * @param string $aspectRatio
     * The resulting video aspect ratio, specified as w:h.
     *
     * @param int $timeLimit in seconds
     * The resulting video maximum time length.
     * If the source video is longer than this amount, the destination video will be truncated to meet this length.
     *
     * @param int $sizeLimit in bytes
     * The resulting video maximum file size.
     * The video will be shortened to meet this size limit.
     *
     * @return \DblEj\Multimedia\Video
     * Returns <i>this</i>, allowing for chained calls to this object.
     */
    public function Save($fileName = null, $title = null, $width = null, $height = null, $vbrQuality = null, $bitrateKhz = null, $fps = null, $aspectRatio = null, $timeLimit = null, $sizeLimit = null, $maxCpuThreads = null, $niceness = null)
    {
        if (!$fileName)
        {
            $fileName = sys_get_temp_dir() . uniqid() . ".mp4";
        }
        $convertPath = self::$_converterPath;
        $cmd = "$convertPath -i \"$this->_originalFilename\"";

        if ($maxCpuThreads)
        {
            $convertPath .= " -threads $maxCpuThreads";
        }

        if ($timeLimit)
        {
            $cmd .= " -t $timeLimit";
        }
        if ($sizeLimit)
        {
            $cmd .= " -fs $sizeLimit";
        }
        if ($aspectRatio)
        {
            $cmd .= " -aspect $aspectRatio";
        }
        if ($fps)
        {
            $cmd .= " -r $fps";
        }
        if ($bitrateKhz)
        {
            $cmd .= " -b:v {$bitrateKhz}k";
        }
        if ($vbrQuality)
        {
            $vbrQuality = $vbrQuality * 10 / 30;
            $vbrQuality = 28 - $vbrQuality;
            $cmd .= " -crf $vbrQuality";
        }
        if ($width && $height)
        {
            $cmd .= " -s {$width}x{$height}";
        }
        if ($title)
        {
            $cmd .= " -metadata title=\"".  str_replace("\"", "\\\"", $title)."\"";
        }
        $cmd .= " \"$fileName\"";
        $outputLines = array();
        exec($cmd, $outputLines);
        return $this;
    }

    /**
     * Remove any audo channels from the video file
     * @param string $destFile The full path to the file where the modified file will be saved.
     * @return \DblEj\Multimedia\Video Returns this for easy chained calls
     */
    public function StripAudio($destFile)
    {
        $convertPath = self::$_converterPath;
        $cmd = "$convertPath -i \"$this->_originalFilename\" -vcodec copy -an \"$destFile\"";
        $outputLines = array();
        exec($cmd, $outputLines);
        return $this;
    }

    /**
     * Save a copy of the video without any processing.
     *
     * @param string $destinationFile
     * The destination video file name.
     * If it does not exist it will be created.  If it exists, it will be overwritten.
     *
     * @return \DblEj\Multimedia\Video
     * Returns <i>this</i>, allowing for chained calls to this object.
     */
    public function SaveCopy($destinationFile)
    {
        copy($this->_originalFilename, $destinationFile);
        return $this;
    }

    /**
     * Save an image of the specified video frame.
     *
     * @param string $fileName
     * The destination image file name.
     * If it does not exist it will be created.  If it exists, it will be overwritten.
     *
     * @param int $width
     * The width of the image, in pixels.
     *
     * @param int $height
     * The hieght of the image, in pixels.
     *
     * @param int $moviePosition
     * The frame number to save.
     *
     * @return \DblEj\Multimedia\Video
     * Returns <i>this</i>, allowing for chained calls to this object.
     */
    public function SaveFrameGrab($fileName, $width, $height, $moviePosition = 1, $overlayPngFile = null, $maxCpuThreads = null, $niceness = null)
    {
        $convertPath = self::$_converterPath;
        if ($maxCpuThreads)
        {
            $convertPath .= " -threads $maxCpuThreads";
        }

        $cmd = "$convertPath -ss $moviePosition -i \"$this->_originalFilename\" -vframes 1 -s {$width}x{$height} -f image2 \"$fileName\"";

        $outputLines = array();
        exec($cmd, $outputLines);

        if (file_exists($fileName) && $overlayPngFile)
        {
            //overlay play button
            $playButton = imagecreatefrompng($overlayPngFile);
            $frameGrab  = null;
            $overlaySize = getimagesize($overlayPngFile);
            $middleX     = $width / 2 - $overlaySize[0] / 2;
            $middleY     = $height / 2 - $overlaySize[1] / 2;

            if (\DblEj\Util\Strings::EndsWith($fileName, ".jpg"))
            {
                $frameGrab = imagecreatefromjpeg($fileName);
                if ($playButton && $frameGrab)
                {
                    imagecopy($frameGrab, $playButton, $middleX, $middleY, 0, 0, $overlaySize[0], $overlaySize[1]);
                }
                imagejpeg($frameGrab, $fileName);
            }
            elseif (\DblEj\Util\Strings::EndsWith($fileName, ".png"))
            {
                $frameGrab = imagecreatefrompng($fileName);
                if ($playButton && $frameGrab)
                {
                    imagecopy($frameGrab, $playButton, $middleX, $middleY, 0, 0, $overlaySize[0], $overlaySize[1]);
                }
                imagepng($frameGrab, $fileName);
            }
            elseif (\DblEj\Util\Strings::EndsWith($fileName, ".gif"))
            {
                $frameGrab = imagecreatefromgif($fileName);
                if ($playButton && $frameGrab)
                {
                    imagecopy($frameGrab, $playButton, $middleX, $middleY, 0, 0, $overlaySize[0], $overlaySize[1]);
                }
                imagegif($frameGrab, $fileName);
            }
        }
        return $this;
    }
}