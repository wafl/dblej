<?php

namespace DblEj\Multimedia;

/**
 * Provides methods for interfacing with an image file on disk or in memory.
 */
interface IImageFile
{

    public function __construct($fileName = null, $widthPx = null, $heightPx = null);

    public function Save($fileName = null, $format = self::FORMAT_JPG, $quality = 75);

    public function ResampleAndSave($destinationFile, $destW, $destH, $format, $quality, $formatDescription = null, $destWidthIncWhitespace = null, $destHeightIncWhitespace = null);
}