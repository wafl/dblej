<?php

namespace DblEj\Multimedia\Integration;

interface IVideoProviderExtension
extends IVideoProvider, \DblEj\Extension\IExtension
{
}