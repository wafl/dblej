<?php

namespace DblEj\Multimedia\Integration;

/**
 * Provides methods for interfacing with an online video provider.
 */
interface IVideoProvider
{

    /**
     * Get the provider's unique id for the video at the specified url.
     * If the provider does not id their videos in any significant waym the url itself can be used as the id.
     *
     * @param string $url The URL of the video.
     * @param boolean $resolveRedirects Whether or not to honor redirects at the specified URL.
     */
    public function GetVideoIdFromUrl($url, $resolveRedirects = false);

    /**
     * Extract the provider's unique id for the video from the specified embed code snippet.
     * @param string $embedCode
     */
    public function GetVideoIdFromEmbedCode($embedCode);

    /**
     * Get information about the video with the specified id.
     * @param string $videoId
     */
    public function GetInfoFromId($videoId);

    /**
     * Get a thumbnail image URL for the video with the specified id.
     * @param string $videoId
     */
    public function GetThumbnailFromVideoId($videoId);

    /**
     * Get the url of the video with the specified id.
     * @param string $videoId
     */
    public function GetUrlFromVideoId($videoId);

    /**
     * Whether this provider requires the client to resolve 301's.
     */
    public function RequiresResolve301();

    /**
     * Get the canonical destination URL for the specified URL.
     * @param string $url
     */
    public function ResolveUrl($url);

    /**
     * Get code that can be used to embed the video with the specified id.
     * @param string $videoId
     */
    public function GetEmbedCodeFromVideoId($videoId);
}