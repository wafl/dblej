<?php

namespace DblEj\Multimedia;

/**
 * Thrown when invalid paramters are passed to a Multimedia class constructor.
 */
class InvalidMediaConstructionException
extends MultimediaException
{
    public function __construct($message, $severity = E_WARNING, \Exception $innerException = null)
    {
        parent::__construct("Invalid constructor arguments. $message", $severity, $innerException);
    }
}