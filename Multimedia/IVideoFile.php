<?php

namespace DblEj\Multimedia;

/**
 * Provides methods for interfacing with an video file on disk.
 */
interface IVideoFile
{

    function __construct($fileName);

    public function Save($fileName = null, $title = null, $width = null, $height = null, $vbrQuality = null, $bitrateKhz = null, $fps = null, $aspectRatio = null, $timeLimit = null, $sizeLimit = null);

    public function SaveCopy($destinationFile);

    public function SaveFrameGrab($fileName, $width, $height, $moviePosition = 1);

    public static function Set_ConverterPath($path);
}