<?php
namespace DblEj\Multimedia;

/**
 * Represents an image file.
 *
 * Provides methods for saving different file formats (with settable compression levels)
 * and image resampling.
 *
 * Requires the GD2 library.
 */
class Image
implements IImageFile
{
    const FORMAT_JPG = 1;
    const FORMAT_PNG = 2;
    const FORMAT_GIF = 3;

    private $_imageResource;
    private $_originalFilename;
    private $_filename;
    private $_originalFormat;

    /**
     * Create an instance of an image.
     * You can specify an existing file or you can start blank.
     *
     * @param string $fileName
     * The name of the existing image file.
     * If <i>fileName</i> is <i>null</i> then you must provide the <i>widthPx</i> and <i>heightPx</i>.
     *
     * @param int $widthPx
     * The width, in pixels, of the new image.
     * This argument is only used when <i>$fileName</i> is <i>null</i>.
     *
     * @param int $heightPx
     * The height, in pixels, of the new image.
     * This argument is only used when <i>$fileName</i> is <i>null</i>.
     *
     * @throws InvalidMediaConstructionException
     * When $fileName is <i>null</i> and <i>widthPx</i> and/or <i>heightPx</i> is <i>null</i> or less than 1.
     */
    public function __construct($fileName = null, $widthPx = null, $heightPx = null)
    {
        if ($fileName)
        {
            $imageSize            = getimagesize($fileName);
            $this->_imageResource = imagecreatefromstring(file_get_contents($fileName));
            if (!$this->_imageResource)
            {
                throw new \Exception("Invalid image file");
            }
            if ($widthPx || $heightPx)
            {
                if (!$widthPx)
                {
                    $widthPx = $imageSize[0];
                }
                if (!$heightPx)
                {
                    $heightPx = $imageSize[1];
                }
                if ($imageSize[0] > $widthPx || $imageSize[1] > $heightPx)
                {
                    $newImage             = imagecreatetruecolor($widthPx, $heightPx);
                    imagecopy($newImage, $this->_imageResource, 0, 0, 0, 0, $widthPx, $heightPx);
                    $this->_imageResource = $newImage;
                }
            }
        }
        elseif ($widthPx && $heightPx)
        {
            $this->_imageResource = imagecreatetruecolor($widthPx, $heightPx);
        }
        else
        {
            throw new InvalidMediaConstructionException("To instantiate an image, you must pass either: fileName or both widthPx and heightPx.");
        }
        $this->_originalFilename = $fileName;
        $this->_filename = $fileName;

        $lastThree = substr($this->_originalFilename, strlen($this->_originalFilename) - 3);
        switch ($lastThree)
        {
            case "jpg":
                $this->_originalFormat = self::FORMAT_JPG;
                break;
            case "gif":
                $this->_originalFormat = self::FORMAT_GIF;
                break;
            case "png":
                $this->_originalFormat = self::FORMAT_PNG;
                break;
        }
    }

    /**
     * Save the image to disk.
     *
     * @param string $fileName
     * The name of the file to save.
     * If the file does not exist it will be created.
     * If the file does exist, it will be ovewritten.
     * The user must have write permissions to the folder/file.
     *
     * @param int $format One of: FORMAT_JPG, FORMAT_PNG, FORMAT_GIF.
     *
     * @param int $quality number between 1-100.
     * For PNG files, this specifies the compression amount (and affects the file's size).
     * For JPG files, this specifies the image quality (and affects the file's size).
     * For GIF files, this argument is ignored.
     *
     * A higher value results in a larger file and, in some cases, slower compression.
     */
    public function Save($fileName = null, $format = null, $quality = 75)
    {

        if (!$format)
        {
            $format = $this->_originalFormat?$this->_originalFormat:self::FORMAT_JPG;
        }

        if (!$fileName)
        {
            $fileName = $this->_originalFilename;
        }
        if (!$fileName)
        {
            $ext = null;
            switch ($format)
            {
                case self::FORMAT_JPG:
                    $ext = "jpg";
                    break;
                case self::FORMAT_PNG:
                    $ext = "png";
                    break;
                case self::FORMAT_GIF:
                    $ext = "gif";
                    break;
            }

            $fileName = sys_get_temp_dir() . uniqid() . ".$ext";
        }
        switch ($format)
        {
            case self::FORMAT_JPG:
                imagejpeg($this->_imageResource, $fileName, $quality);
                break;
            case self::FORMAT_PNG:
                if ($quality < 10)
                {
                    $quality = 10;
                }
                if ($quality > 100)
                {
                    $quality = 100;
                }
                imagepng($this->_imageResource, $fileName, 10 - ($quality / 10));
                break;
            case self::FORMAT_GIF:
                imagegif($this->_imageResource, $fileName);
                break;
            default:
                throw new \Exception("Invalid image type");
        }
        $this->_filename = $fileName;
        return $this;
    }

    public function OverlayImage($imageResource, $x, $y)
    {
        imagecopy($this->_imageResource, $imageResource, $x, $y, 0, 0, imagesx($imageResource), imagesy($imageResource));
    }

    public function ReduceMetaSize()
    {
        $file = $this->_filename;
        $returnStats = ["OriginalSize"=>filesize($file), "NewSize"=>null];
        if (stristr($file, ".png") !== false && stristr($file, ".pngcrush") === false)
        {
            clearstatcache();
            $fileSize = filesize($file);
            if (file_exists("$file.pngcrush"))
            {
                unlink("$file.pngcrush");
            }
            shell_exec("pngcrush -brute -reduce \"$file\" \"$file.pngcrush\"");
            clearstatcache();
            if (file_exists("$file.pngcrush"))
            {
                if (filesize("$file.pngcrush") < $fileSize)
                {
                    unlink($file);
                    rename("$file.pngcrush", $file);
                } else {
                    unlink("$file.pngcrush");
                }
            }
            clearstatcache();
            shell_exec("optipng -zs 0-3 -nc -zc 9 -f 0-5 \"$file\"");
            clearstatcache();
            shell_exec("optipng -i 0 -zs 0-3 -nc -zc 9 -f 0-5 \"$file\"");
            clearstatcache();
            $returnStats["NewSize"] = filesize($file);
        }
        elseif (stristr($file, ".jpg") !== false)
        {
            $fileSize = filesize($file);
            shell_exec("jpegoptim --strip-all \"$file\"");
            clearstatcache();
            $newFileSize = filesize($file);
            $returnStats["NewSize"] = $newFileSize;
        }
        return $returnStats;
    }

    public static function GetDefaultFontFile()
    {
        return realpath(__DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Fonts/OpenSans-Regular.ttf");
    }
    /**
     *
     * @param float $size Font size in points
     * @param integer $x
     * @param integer $y
     * @param string $text
     * @param string $color
     * @param string $fontfile
     * @param integer $angle
     */
    public function AddText($size, $leftX, $bottomY, $text, $color = "000000", $fontfile = null, $angle = 0)
    {
        if (!$fontfile)
        {
            $fontfile = self::GetDefaultFontFile();
        }
        $rgb = \DblEj\Util\Colors::GetRgbFromHexString($color);
        $color = imagecolorallocate($this->_imageResource, $rgb[0], $rgb[1], $rgb[2]);
        imagettftext($this->_imageResource, $size, $angle, $leftX, $bottomY, $color, $fontfile, $text);
    }
    public function AddRectangle($x, $y, $width, $height, $strokeThickness = 1, $strokeColor = "000000", $bgColor = null)
    {
        $strokeRGB = \DblEj\Util\Colors::GetRgbFromHexString($strokeColor);
        if ($bgColor)
        {
            $bgRGB = \DblEj\Util\Colors::GetRgbFromHexString($bgColor);
        }

        $strokeColorResource = imagecolorallocate($this->_imageResource, $strokeRGB[0], $strokeRGB[1], $strokeRGB[2]);
        if ($bgColor)
        {
            $bgColorResource = imagecolorallocate($this->_imageResource, $bgRGB[0], $bgRGB[1], $bgRGB[2]);
        } else {
            $bgColorResource = null;
        }
        for ($strokeIdx = 0; $strokeIdx < $strokeThickness; $strokeIdx++)
        {
            imagerectangle($this->_imageResource, $x, $y, $x + $width, $y + $height, $strokeColorResource);
            $x = $x + 1;
            $y = $y + 1;
            $width = $width - 2;
            $height = $height - 2;
        }
        if ($bgColorResource)
        {
            imagefilltoborder($this->_imageResource, $x + 1, $y + 1, $strokeColorResource, $bgColorResource);
        }
        return $this;
    }

    /**
     * Fill the image with the specified color
     *
     * @param String $color 6 character hex color string, or a 3 element array like [r,g,b]
     * @param integer $x
     * @param integer $y
     */
    public function Fill($color, $x = 0, $y = 0)
    {
        if (!is_array($color) && is_string($color))
        {
            $rgb = \DblEj\Util\Colors::GetRgbFromHexString($color);
        } elseif (is_array($color)) {
            $rgb = $color;
        }
        $bgColorResource = imagecolorallocate($this->_imageResource, $rgb[0], $rgb[1], $rgb[2]);
        imagefill($this->_imageResource, $x, $y, $bgColorResource);
    }
    /**
     * Resizes an image using smoothing and interpolation to reduces pixelation and other artifacts common with image resizing.
     *
     * @param string $destinationFile
     * @param int $destW The width that the image will be scaled to.
     * @param int $destH The height that the image will be scaled to.
     * @param int $format One of: FORMAT_JPG, FORMAT_PNG, FORMAT_GIF.
     *
     * @param int $quality number between 1-100.
     * For PNG files, this specifies the compression amount (and affects the file's size).
     * For JPG files, this specifies the image quality (and affects the file's size).
     * For GIF files, this argument is ignored.
     *
     * @param string $formatDescription
     * A human-readable description of the new file's size and quality, used for display purposes.
     * Currently this is only used in the exception message when something goes wrong with resampling.
     *
     * @param type $destWidthIncWhitespace
     * Specifies the total width of the new image, including any whitespace that you want to add
     * to the left/right side of the image.
     * This is useful for resizing an image to specific dimensions without needing to stretch the actual image contents.
     * This allows you to maintain proportions for the image content, while still creating fixed dimension images
     * that may not be of the same proportions as the original image.
     *
     * @param type $destHeightIncWhitespace
     * Specifies the total height of the new image, including any whitespace that you want to add
     * to the top/bottom of the image.
     * This is useful for resizing an image to specific dimensions without needing to stretch the actual image contents.
     * This allows you to maintain proportions for the image content, while still creating fixed dimension images
     * that may not be of the same proportions as the original image.
     *
     * @return \DblEj\Multimedia\Image
     * The new image object.
     *
     * @throws \DblEj\Multimedia\MultimediaException
     */
    public function ResampleAndSave($destinationFile, $destW, $destH, $format = self::FORMAT_JPG, $quality = 75, $formatDescription = null, $destWidthIncWhitespace = null, $destHeightIncWhitespace = null)
    {
        $srcW = imagesx($this->_imageResource);
        $srcH = imagesy($this->_imageResource);
        if ($destW == null)
        {
            $destW = $srcW;
        }
        if ($destH == null)
        {
            $destH = $srcH;
        }

        $offsetX = 0;
        $offsetY = 0;
        if ($destWidthIncWhitespace)
        {
            $offsetX = $destWidthIncWhitespace - $destW;
            if ($offsetX)
            {
                $offsetX = $offsetX / 2;
            }
        }
        else
        {
            $destWidthIncWhitespace = $destW;
        }
        if ($destHeightIncWhitespace)
        {
            $offsetY = $destHeightIncWhitespace - $destH;
            if ($offsetY)
            {
                $offsetY = $offsetY / 2;
            }
        }
        else
        {
            $destHeightIncWhitespace = $destH;
        }

        if ($destW != $srcW || $destH != $srcH || $destWidthIncWhitespace != $destW || $destHeightIncWhitespace != $destH || $format == self::FORMAT_JPG)
        {
            $scaledImage = imagecreatetruecolor($destWidthIncWhitespace, $destHeightIncWhitespace);
            $whiteColor = imagecolorallocate($scaledImage, 255, 255, 255);
            imagefill($scaledImage, 0, 0, $whiteColor);
            imagefilter($scaledImage, IMG_FILTER_NEGATE);
            imagefilter($this->_imageResource, IMG_FILTER_NEGATE);

            imagecopyresampled($scaledImage, $this->_imageResource, $offsetX, $offsetY, 0, 0, $destW, $destH, $srcW, $srcH);

            imagefilter($scaledImage, IMG_FILTER_NEGATE);
            imagefilter($this->_imageResource, IMG_FILTER_NEGATE);

            if (file_exists($destinationFile))
            {
                unlink($destinationFile);
            }
            switch ($format)
            {
                case self::FORMAT_JPG:
                    if (!imagejpeg($scaledImage, $destinationFile, $quality))
                    {
                        throw new MultimediaException("Could not save media jpg ($formatDescription)");
                    }
                    break;
                case self::FORMAT_PNG:
                    if ($quality < 10)
                    {
                        $quality = 10;
                    }
                    if ($quality > 100)
                    {
                        $quality = 100;
                    }
                    if (!imagepng($scaledImage, $destinationFile, 10 - ($quality / 10)))
                    {
                        throw new MultimediaException("Could not save media png ($formatDescription)");
                    }
                    break;
                case self::FORMAT_GIF:
                    if (!imagegif($scaledImage, $destinationFile))
                    {
                        throw new MultimediaException("Could not save media gif ($formatDescription)");
                    }
                    break;
            }
        }
        return $this;
    }

    public function FadeInFromTop($gradientImage, $fadeOutTopStart, $destHeightIncWhitespace, $destWidthIncWhitespace)
    {
        imagealphablending($gradientImage, true);
        $transparentColor = imagecolorallocatealpha($gradientImage, 0, 0, 0, 127);
        imagefill($gradientImage, 0, 0, $transparentColor);
        for ($y = 0; $y < $destHeightIncWhitespace; $y++)
        {
            for ($x = 0; $x < $destWidthIncWhitespace; $x++)
            {
                $currentPixelColorIndex = imagecolorat($this->_imageResource, $x, $y);
                $currentPixelColors     = imagecolorsforindex($this->_imageResource, $currentPixelColorIndex);
                $fadeProgressFactor     = min($y / $fadeOutTopStart, 1);
                $alpha                  = $y >= $fadeOutTopStart ? 0 : 127 - ($fadeProgressFactor * 127);
                $new_color              = imagecolorallocatealpha($gradientImage, $currentPixelColors["red"], $currentPixelColors["green"], $currentPixelColors["blue"], $alpha);
                imagesetpixel($gradientImage, $x, $y, $new_color);
            }
        }
        imagesavealpha($gradientImage, true);
        return $gradientImage;
    }
    public function Get_Width()
    {
        return imagesx($this->_imageResource);
    }
    public function Get_Height()
    {
        return imagesy($this->_imageResource);
    }

    /**
     * Crop the image
     *
     * @param int $width
     * Width after crop in pixels
     *
     * @param type $height
     * Height after crop in pixels
     *
     * @param type $offsetX
     * By default, the image will be cropped with an equal reduction on all sides.
     * However, if you specify a value for offsetX, then the image will be cut by that value on the left side,
     * and the right side would be cut by as much as needed to reach the desired width.
     *
     * @param type $offsetY
     * By default, the image will be cropped with an equal reduction on all sides.
     * However, if you specify a value for offsetY, then the image will be cut by that value on the top side,
     * and the bottom side would be cut by as much as needed to reach the desired height.
     */
    public function Crop($width, $height, $offsetX = null, $offsetY = null)
    {
        $imageHeight = imagesy($this->_imageResource);
        $imageWidth = imagesx($this->_imageResource);
        if ($width > $imageWidth)
        {
            //throw new \Exception("Cannot crop an image to a width larger than it's current width (current: $imageWidth, requested: $width)");
        }
        if ($height > $imageHeight)
        {
            //throw new \Exception("Cannot crop an image to a height larger than it's current height (current: $imageHeight, requested: $height)");
        }
        if ($offsetX === null)
        {
            $offsetX = ($imageWidth - $width) / 2;
        }
        if ($offsetY === null)
        {
            $offsetY = ($imageHeight - $height) / 2;
        }
        $newimg = imagecreatetruecolor($width, $height);
        $background = imagecolorallocate($newimg, 255, 255, 255);
        imagefill($newimg, 0, 0, $background);
        imagecopy($newimg, $this->_imageResource, $offsetX<0?abs($offsetX):0, $offsetY<0?abs($offsetY):0, $offsetX>0?$offsetX:0, $offsetY>0?$offsetY:0, min($imageWidth, $width), min($imageHeight, $height));
        $this->_imageResource = $newimg;
    }

    public function Pad($width, $height, $color = 0xFFFFFF)
    {
        $colorHex = dechex($color);
        $colorHex = str_pad($colorHex, 6, 0);
        $r = substr($colorHex, 0, 2);
        $g = substr($colorHex, 2, 2);
        $b = substr($colorHex, 4, 2);
        $r = hexdec($r);
        $g = hexdec($g);
        $b = hexdec($b);
        $imageHeight = imagesy($this->_imageResource);
        $imageWidth = imagesx($this->_imageResource);

        $width = max($width, $imageWidth);
        $height = max($height, $imageHeight);

        if ($width > $imageWidth || $height > $imageHeight)
        {
            $offsetX = ($width - $imageWidth) / 2;
            $offsetY = ($height - $imageHeight) / 2;
            $newimg = imagecreatetruecolor($width, $height);
            $background = imagecolorallocate($newimg, $r, $g, $b);
            imagefill($newimg, 0, 0, $background);
            imagecopy($newimg, $this->_imageResource, $offsetX, $offsetY, 0, 0, $imageWidth, $imageHeight);
            imagedestroy($this->_imageResource);
            $this->_imageResource = $newimg;
        }
        return $this;
    }

    /**
     * Remove whitespace surrounding an image
     *
     * @param boolean $maintainProportions
     * OPTIONAL
     * Whether or not to maintain the images pre-trim width:height ratio
     * Default = true
     *
     * @param int $padding
     * OPTIONAL
     * The amount of whitespace to leave around the image
     * Default = 0
     *
     * @param int $maxArtifactSize
     * OPTIONAL
     * Some images have barely-visible pixles/pixel-clusters around the edges that are usually artifacts from compression.  If $maxArtifactSize is greater than 0 ,
     * then Trim() will attempt to treat these pixels (or clusters up to $maxArtifactSize x $maxArtifactSize pixels) the same as white space.  Because of the $maxArtifactSize pixel tolerence, images that are less than $maxArtifactSize * 2 + padding pixels in their width or height should leave this option off.
     * Default = 0
     *
     * @param boolean $equalXTrim OPTIONAL trim the same amount from the left and right sides (whichever is less) Default = false
     * @param boolean $equalYTrim OPTIONAL trim the same amount from the top and bottom sides (whichever is less) Default = false
     *
     */
    public function Trim($maintainProportions = true, $padding = 0, $maxArtifactSize = 0, $equalXTrim = false, $equalYTrim = false)
    {
        //find the size of the borders
        $whiteTop = 0;
        $whiteBottom = 0;
        $whiteLeft = 0;
        $whiteRight = 0;

        //top
        $whiteTop = $this->findEdge($maxArtifactSize, 1, "y");

        //bottom
        $whiteBottom = $this->findEdge($maxArtifactSize, -1, "y");

        //left
        $whiteLeft = $this->findEdge($maxArtifactSize, 1, "x");

        //right
        $whiteRight = $this->findEdge($maxArtifactSize, -1, "x");

        if ($equalXTrim)
        {
            $whiteLeft = min($whiteLeft, $whiteRight);
            $whiteRight = $whiteLeft;
        }
        if ($equalYTrim)
        {
            $whiteTop = min($whiteTop, $whiteBottom);
            $whiteBottom = $whiteTop;
        }

        $oldWidth = imagesx($this->_imageResource);
        $oldHeight = imagesy($this->_imageResource);

        $whiteLeft = max(0, $whiteLeft - $padding);
        $whiteRight = max(0, $whiteRight - $padding);
        $whiteTop = max(0, $whiteTop - $padding);
        $whiteBottom = max(0, $whiteBottom - $padding);
        $xWhite = $whiteLeft+$whiteRight;
        $yWhite = $whiteTop+$whiteBottom;
        $newWidth = $oldWidth-$xWhite;
        $newHeight = $oldHeight-$yWhite;

        if ($maintainProportions)
        {
            if ($xWhite < $yWhite)
            {
                $unConstrainedNewHeight = $newHeight;
                $newHeight = $oldHeight * $newWidth / $oldWidth;
                $heightDiff = $newHeight - $unConstrainedNewHeight;
                $whiteTop = $whiteTop - $heightDiff / 2;
                $whiteBottom = $whiteBottom - $heightDiff / 2;
            } else {
                $unConstrainedNewWidth = $newWidth;
                $newWidth = $oldWidth * $newHeight / $oldHeight;
                $widthDiff = $newWidth - $unConstrainedNewWidth;
                $whiteLeft = $whiteLeft - $widthDiff / 2;
                $whiteRight = $whiteRight - $widthDiff / 2;
            }
        }
        if ($newWidth > $maxArtifactSize *2 && $newHeight > $maxArtifactSize *2 && ($newWidth != $oldWidth || $newHeight != $oldHeight))
        {
            //copy the contents, excluding the border
            $newimg = imagecreatetruecolor($newWidth, $newHeight);
            $background = imagecolorallocate($newimg, 255, 255, 255);
            imagefill($newimg, 0, 0, $background);
            imagecopy($newimg, $this->_imageResource, 0, 0, $whiteLeft, $whiteTop, $newWidth, $newHeight);
            $this->_imageResource = $newimg;
            return true;
        } else {
            return false;
        }
    }

    private function findEdge ($maxArtifactSize, $direction = 1, $dimension = "x")
    {
        $edgeFound = false;

        $totalRows = imagesy($this->_imageResource);
        $totalCols = imagesx($this->_imageResource);
        $startX = $direction == 1 ? 0 : $totalCols - 1;
        $startY = $direction == 1 ? 0 : $totalRows - 1;
        $endX = $direction == -1 ? 0 : $totalCols - 1;
        $endY = $direction == -1 ? 0 : $totalRows - 1;

        $edgeFound = false;
        $xConcurrentNonWhites = 0;
        $yConcurrentNonWhites = 0;
        if ($dimension == "x")
        {
            for ($x = $startX; $x != $endX; $x = $x + $direction)
            {
                $somethingFoundOnY = false;
                for ($y = $startY; $y != $endY; $y = $y + $direction)
                {
                    if (imagecolorat($this->_imageResource, $x, $y) != 0xFFFFFF)
                    {
                        if (!$somethingFoundOnY)
                        {
                            $xConcurrentNonWhites++;
                        }
                        $somethingFoundOnY = true;
                        if ($xConcurrentNonWhites >= $maxArtifactSize)
                        {
                            if (!$edgeFound || ($x < $edgeFound && $direction == 1) || ($x > $edgeFound && $direction == -1))
                            {
                                $edgeFound = $x;
                            }
                        }
                    }
                }
                if ($edgeFound)
                {
                    break;
                }
                if (!$somethingFoundOnY)
                {
                    $xConcurrentNonWhites = 0;
                }
            }
        }
        elseif ($dimension == "y")
        {
            for ($y = $startY; $y != $endY; $y = $y + $direction)
            {
                $somethingFoundOnX = false;
                for ($x = $startX; $x != $endX; $x = $x + $direction)
                {
                    if (imagecolorat($this->_imageResource, $x, $y) != 0xFFFFFF)
                    {
                        if (!$somethingFoundOnX)
                        {
                            $yConcurrentNonWhites++;
                        }
                        $somethingFoundOnX = true;
                        if ($yConcurrentNonWhites >= $maxArtifactSize)
                        {
                            if (!$edgeFound || ($y < $edgeFound && $direction == 1) || ($y > $edgeFound && $direction == -1))
                            {
                                $edgeFound = $y;
                            }
                        }
                    }
                }
                if ($edgeFound)
                {
                    break;
                }
                if (!$somethingFoundOnX)
                {
                    $yConcurrentNonWhites = 0;
                }
            }
        }

        $edgeCoord = $edgeFound - ($maxArtifactSize * $direction);
        if ($direction == -1)
        {
            if ($dimension == "x")
            {
                $edgeCoord = $totalCols - $edgeCoord;
            } else {
                $edgeCoord = $totalRows - $edgeCoord;
            }
        }

        return $edgeCoord;
    }
}