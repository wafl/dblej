<?php

namespace DblEj\Multimedia;

/**
 * Information about a video, typically from a video provider.
 */
class VideoInfo
{
    private $_thumbnailImage;
    private $_title;
    private $_description;

    public function Get_ThumbnailImage()
    {
        return $this->_thumbnailImage;
    }

    public function Set_ThumbnailImage($newValue)
    {
        $this->_thumbnailImage = $newValue;
    }

    public function Get_Title()
    {
        return $this->_title;
    }

    public function Set_Title($newValue)
    {
        $this->_title = $newValue;
    }

    public function Get_Description()
    {
        return $this->_description;
    }

    public function Set_Description($newValue)
    {
        $this->_description = $newValue;
    }
}