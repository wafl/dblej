<?php

namespace DblEj\Multimedia;

/**
 * A multimedia exception.
 */
class MultimediaException
extends \DblEj\System\Exception
{
}