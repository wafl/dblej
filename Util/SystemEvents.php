<?php

namespace DblEj\Util;

final class SystemEvents
{
    const BEFORE_INITIALIZE                     = "BEFORE_INITIALIZE";
    const BEFORE_USER_INSTANTIATION             = "BEFORE_USER_INSTANTIATION";
    const BEFORE_TEMPLATE_ENGINE_INITIALIZE     = "BEFORE_TEMPLATE_ENGINE_INITIALIZE";
    const AFTER_INITIALIZE                      = "AFTER_INITIALIZE";
    const AFTER_USER_INSTANTIATION              = "AFTER_USER_INSTANTIATION";
    const AFTER_EXTENSION_INSTALL               = "AFTER_EXTENSION_INSTALL";
    const AFTER_EXTENSIONS_INITIALIZED          = "AFTER_EXTENSIONS_INITIALIZED";
    const AFTER_EXTENSIONS_SITEPAGES_REGISTERED = "AFTER_EXTENSIONS_SITEPAGES_REGISTERED";

    const BEFORE_DATA_CONNECT                 = "BEFORE_DATA_CONNECT";
    const AFTER_DATA_CONNECT                  = "AFTER_DATA_CONNECT";
    const AFTER_TEMPLATE_ENGINE_INITIALIZE    = "AFTER_TEMPLATE_ENGINE_INITIALIZE";
    const BEFORE_EXECUTE_TEST_CASE            = "BEFORE_EXECUTE_TEST_CASE";
    const AFTER_EXECUTE_TEST_CASE             = "AFTER_EXECUTE_TEST_CASE";
    const BEFORE_EXECUTE_UNITTEST_CASE        = "BEFORE_EXECUTE_UNITTEST_CASE";
    const AFTER_EXECUTE_UNITTEST_CASE         = "AFTER_EXECUTE_UNITTEST_CASE";
    const BEFORE_EXECUTE_INTEGRATIONTEST_CASE = "BEFORE_EXECUTE_INTEGRATIONTEST_CASE";
    const AFTER_EXECUTE_INTEGRATIONTEST_CASE  = "AFTER_EXECUTE_INTEGRATIONTEST_CASE";
    const OBJECT_GLOBAL_EVENT                 = "OBJECT_CUSTOM_EVENT";

    const BEFORE_SESSION_START                = "BEFORE_SESSION_START";
    const AFTER_SESSION_START                 = "AFTER_SESSION_START";
    const BEFORE_SESSION_CLOSE                = "BEFORE_SESSION_CLOSE";
    const AFTER_SESSION_CLOSE                 = "AFTER_SESSION_CLOSE";

    private static $_handlers;
    private static $_handledEventLog = [];
    private static function _initOnce()
    {
        if (!self::$_handlers)
        {
            self::$_handlers = new \DblEj\EventHandling\DynamicEventHandlerCollection();
        }
    }

    /**
     *
     * @param {String} $eventType
     * @param \callable $callback
     * @return \DblEj\EventHandling\DynamicEventHandler
     */
    public static function AddSystemHandler($eventType, callable $callback)
    {
        self::_initOnce();
        $handler = new \DblEj\EventHandling\DynamicEventHandler($eventType, $callback);
        self::$_handlers->AddHandlerItem($handler);
        return $handler;
    }

    /**
     *
     * @param \DblEj\Util\DynamicEventHandler $handler
     */
    public static function RemoveSystemHandler(DynamicEventHandler $handler)
    {
        self::_initOnce();
        self::$_handlers->RemoveHandlerItem($handler);
    }

    /**
     *
     * @param \callable $callback
     */
    public static function RemoveSystemHandlersByCallback(callable $callback)
    {
        self::_initOnce();
        self::$_handlers->RemoveByCallback($callback);
    }

    /**
     *
     * @param \DblEj\EventHandling\EventInfo $eventInfo
     */
    public static function RaiseSystemEvent(\DblEj\EventHandling\EventInfo $eventInfo)
    {
        self::_initOnce();
        self::$_handlers->HandleEvent($eventInfo);

        self::$_handledEventLog[] = [time(), $eventInfo->Get_EventType()];
    }

    public static function HasSystemEventTypeFired($eventTypeId)
    {
        foreach (self::$_handledEventLog as $handledEvent)
        {
            if ($handledEvent[1] == $eventTypeId)
            {
                return true;
            }
        }
        return false;
    }
}