<?php

namespace DblEj\Util;

/**
 * String related utility functions.
 */
class Strings
{
    const RANDOM_GENERATOR_MT  = 0;
    const RANDOM_GENERATOR_SSL = 1;

    /**
     *
     * @param integer $length
     * @param string[] $excludeChars
     * @param boolean $allUppercase
     * @param integer $algorithm
     * @return string
     */
    public static function GenerateRandomString($length, $excludeChars = array(
        "0",
        "O",
        "1",
        "L",
        "I"), $allUppercase = true, $algorithm = self::RANDOM_GENERATOR_SSL)
    {
        $uuid = "";

        //if the specified all uppercase they likely only passed in exlude chars in uppercase.
        //lower-case would slip through and then get strtoupper'd.
        //So here we add all lower case variants of the exclude chars
        if ($allUppercase)
        {
            foreach ($excludeChars as $excludeChar)
            {
                if (!is_numeric($excludeChar) && ctype_upper($excludeChar))
                {
                    $excludeChars[] = strtolower($excludeChar);
                }
            }
        }
        $lastChars = "";
        while (strlen($uuid) < $length)
        {
            $chars = "";
            while ($chars == "" || (self::_charsContainChars($chars, $excludeChars) !== false))
            {
                switch ($algorithm)
                {
                    case self::RANDOM_GENERATOR_MT:
                        $chars = mt_rand(0, 36);
                        $chars = base_convert($chars, 16, 36);
                        break;
                    case self::RANDOM_GENERATOR_SSL:
                        $chars = openssl_random_pseudo_bytes(1);
                        $chars = bin2hex($chars);
                        $chars = base_convert($chars, 16, 36);
                        break;
                }
                if ($allUppercase)
                {
                    $chars = strtoupper($chars);
                }
            }
            if ($chars != $lastChars)
            {
                $uuid .= $chars;
                $lastChars = $chars;
            }
        }
        if (strlen($uuid) > $length)
        {
            $uuid = substr($uuid, 0, $length);
        }
        return $uuid;
    }

    private static function _charsContainChars($chars1, $chars2)
    {
        $contains = false;
        foreach (str_split($chars1) as $char)
        {
            if (array_search($char, $chars2) !== false)
            {
                $contains = true;
                break;
            }
        }
        return $contains;
    }

    /**
     *
     * @param string $mixedString
     * @return string
     */
    public static function RemoveNonAlphaNumeric($mixedString)
    {
        $returnString = preg_replace("/[^A-Za-z0-9]/", "", $mixedString);
        return $returnString;
    }

    /**
     *
     * @param string $string
     * @return string
     */
    public static function RemoveUrls($string)
    {
        $pattern     = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
        $replacement = "";
        return preg_replace($pattern, $replacement, $string);
    }

    /**
     * Split a string into an array of characters with support for multi-byte encodings
     *
     * @since 0.1.629
     * @param string $inputString
     * @return array
     */
    public static function SplitChars($inputString)
    {
        return preg_split('//u', $inputString, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     *
     * @param string $subject
     * @param string $startsWithString
     * @param boolean $skipWhiteSpace
     * @return boolean
     */
    public static function StartsWith($subject, $startsWithString, $skipWhiteSpace = false)
    {
        if ($skipWhiteSpace)
        {
            $subject          = trim($subject);
            $startsWithString = trim($startsWithString);
        }
        $length = strlen($startsWithString);
        return (substr($subject, 0, $length) === $startsWithString);
    }

    /**
     *
     * @param string $subject
     * @param string $endsWithString
     * @return boolean
     */
    public static function EndsWith($subject, $endsWithString)
    {
        $length = strlen($endsWithString);
        if ($length == 0)
        {
            return true;
        }

        return (substr($subject, -$length) === $endsWithString);
    }

    /**
     *
     * @param string $subject
     * @param string $replace
     * @param string $replaceWith
     * @return string
     */
    public static function ReplaceBeginning($subject, $replace, $replaceWith)
    {
        if (substr($subject, 0, strlen($replace)) == $replace)
        {
            $subject = $replaceWith . substr($subject, strlen($replace));
        }
        return $subject;
    }

    /**
     * Collapse whitespace characters into a single space (or whatever is specified in <i>$collapseTo</i>).
     * @param string $string The string to collapse the whitespace in.
     * @param string $collapseTo The character(s) to collapse the whitespace into.
     * @return string The new string is returned.
     */
    public static function CollapseWhitespace($string, $collapseTo = " ")
    {
        //collapse whitespace to one space
        $splitString = preg_split("/[\s]+/", $string);
        return implode($collapseTo, $splitString);
    }

    public static function CollapseChars($string, $charToCollapse, $collapseTo = " ")
    {
        $splitString = preg_split("/[$charToCollapse]+/", $string);
        return implode($collapseTo, $splitString);
    }

    /**
     * Whether or not the string is comprised of all latin characters
     * credit goes to: https://stackoverflow.com/a/16853473/546833
     *
     * @param string $string The string
     * @return boolean
     */
    public static function IsLatin($string)
    {
        return mb_check_encoding($string, 'ASCII');
    }

    /**
     * Whether or not the string contains any multi byte characters
     * credit goes to: https://stackoverflow.com/a/16853473/546833
     *
     * @param string $string The string
     * @return boolean
     */
    public static function ContainsMultibyte($string)
    {
        return !mb_check_encoding($string, 'ASCII') && mb_check_encoding($string, 'UTF-8');
    }

    public static function IsUtf8($string)
    {
        return mb_check_encoding($string, 'UTF-8');
    }

    public static function IsIso8859($string)
    {
        return mb_check_encoding($string, 'IS0-8859');
    }
}