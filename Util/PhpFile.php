<?php
namespace DblEj\Util;

class PhpFile
extends File
{
    private static function doesStringStartPhpClass($fileLine, $includeInterfaces = true, $includeTraits = true)
    {
        $startsWithClass = false;
        if
        (
            Strings::StartsWith($fileLine, "class") ||
            Strings::StartsWith($fileLine, "final class") ||
            Strings::StartsWith($fileLine, "abstract class")
        )
        {
            $startsWithClass = true;
        }
        if (!$startsWithClass && $includeInterfaces)
        {
            if (
            Strings::StartsWith($fileLine, "interface") ||
            Strings::StartsWith($fileLine, "final interface")
            )
            {
                $startsWithClass = true;
            }
        }
        if (!$startsWithClass && $includeTraits)
        {
            if (Strings::StartsWith($fileLine, "trait "))
            {
                $startsWithClass = true;
            }
        }
        return $startsWithClass;
    }

    /**
     * Checks the first $linesToCheck of a file to see if it defines a class by searching the file, line by line,
     * for a line that starts with "class"
     *
     * @param string $filePath The file to check
     * @param int $linesToCheck The number of lines in the file to try before giving up
     * @param boolean $ignoreComments Whather comment lines should could against the $linesToCheck.
     * If ignoreComments is true, they will not count
     *
     * @return boolean
     */
    public static function DoesFileHavePhpClass($filePath, $linesToCheck = 10, $ignoreComments = true, $includeInterfaces = true, $includeTraits = true)
    {
        $startsWithClass = false;
        try
        {
            $fileHandle = @fopen($filePath, "r");
            if ($fileHandle === false)
            {
                $error = error_get_last();
                if ($error)
                {
                    throw new FileException($error["message"], $error["type"]);
                } else {
                    throw new FileException("Cannot open file \"$filePath\"");
                }
            }
            $linesRead = 0;
            while (!feof($fileHandle) && ($linesRead < $linesToCheck) && !$startsWithClass)
            {
                $fileLine = trim(fgets($fileHandle));
                if (!$ignoreComments ||
                ((!Strings::StartsWith($fileLine, "//", true) &&
                !Strings::StartsWith($fileLine, "/*", true))))
                {
                    $startsWithClass = self::doesStringStartPhpClass($fileLine, $includeInterfaces, $includeTraits);
                    $linesRead++;
                }
            }
        }
        catch (\Exception $ex)
        {
            throw new FileException("There was an error while trying to read the file to check if it has a php class.", E_WARNING, $ex);
        }
        //		finally //not supported on older php's so commenting out for now
        //		{
        //			if ($fileHandle !== false)
        //			{
        //				fclose($fileHandle);
        //			}
        //		}
        if ($fileHandle !== false)
        {
            fclose($fileHandle);
        }
        return $startsWithClass;
    }

    /**
     * Checks if a file contains the php opening tag ( "lessthan ?php" ) and thus if it is a PHP file.
     *
     * This method will open the file and read the first $linesToCheck lines
     * searching for the opening tag.
     *
     * @param string $filePath
     * @param int $linesToCheck optional, default = 3.
     * How many lines to read off the beginning of the file before giveing up.
     * @return boolean
     * @throws FileException
     */
    public static function DoesFileHaveOpeningPhpTag($filePath, $linesToCheck = 3)
    {
        $isPhpFile = false;
        try
        {
            $fileHandle = @fopen($filePath, "r");
            if ($fileHandle === false)
            {
                $error = error_get_last();
                if ($error)
                {
                    throw new FileException($error["message"], $error["type"]);
                } else {
                    throw new FileException("Cannot open file \"$filePath\"");
                }
            }
            $linesRead = 0;
            while (!feof($fileHandle) && ($linesRead < $linesToCheck) && !$isPhpFile)
            {
                $fileLine = fgets($fileHandle);
                $isPhpFile = Strings::StartsWith(trim($fileLine), "<?php");
                $linesRead++;
            }
        }
        catch (\Exception $ex)
        {
            throw new FileException("There was an error while trying to read the file to check if it has an opening php tag.", E_WARNING, $ex);
        }
        //		finally //commented since not widely supported yet in most php installs
        //		{
        //			if ($fileHandle !== false)
        //			{
        //				fclose($fileHandle);
        //			}
        //		}
        if ($fileHandle !== false)
        {
            fclose($fileHandle);
        }
        return $isPhpFile;
    }
}