/*
 *@namespace DblEj.Util
 */
Namespace("DblEj.Util");

/*
 * @class Encoding
 * @static
 */

DblEj.Util.Encoding = {};

/**
 * @function EncodeUtf8
 * @static
 *
 * @param {String} stringToEncode
 * @return {String}
 */
DblEj.Util.Encoding.EncodeUtf8 = function (stringToEncode)
{
    return unescape(encodeURIComponent(stringToEncode));
};

/**
 * @function DecodeUtf8
 * @static
 * @param {String} stringToDecode
 * @return {String}
 */
DblEj.Util.Encoding.DecodeUtf8 = function (stringToDecode)
{
    return decodeURIComponent(escape(stringToDecode));
};


/**
 * @function EncodeMD5
 *
 * @param {String} stringToEncode
 * @returns {String}
 */
DblEj.Util.Encoding.EncodeMD5 = function (stringToEncode)
{
    x = str2blks_MD5(stringToEncode);
    a = 1732584193;
    b = -271733879;
    c = -1732584194;
    d = 271733878;

    for (i = 0; i < x.length; i += 16)
    {
        olda = a;
        oldb = b;
        oldc = c;
        oldd = d;

        a = Md5Step2(a, b, c, d, x[i + 0], 7, -680876936);
        d = Md5Step2(d, a, b, c, x[i + 1], 12, -389564586);
        c = Md5Step2(c, d, a, b, x[i + 2], 17, 606105819);
        b = Md5Step2(b, c, d, a, x[i + 3], 22, -1044525330);
        a = Md5Step2(a, b, c, d, x[i + 4], 7, -176418897);
        d = Md5Step2(d, a, b, c, x[i + 5], 12, 1200080426);
        c = Md5Step2(c, d, a, b, x[i + 6], 17, -1473231341);
        b = Md5Step2(b, c, d, a, x[i + 7], 22, -45705983);
        a = Md5Step2(a, b, c, d, x[i + 8], 7, 1770035416);
        d = Md5Step2(d, a, b, c, x[i + 9], 12, -1958414417);
        c = Md5Step2(c, d, a, b, x[i + 10], 17, -42063);
        b = Md5Step2(b, c, d, a, x[i + 11], 22, -1990404162);
        a = Md5Step2(a, b, c, d, x[i + 12], 7, 1804603682);
        d = Md5Step2(d, a, b, c, x[i + 13], 12, -40341101);
        c = Md5Step2(c, d, a, b, x[i + 14], 17, -1502002290);
        b = Md5Step2(b, c, d, a, x[i + 15], 22, 1236535329);

        a = Md5Step3(a, b, c, d, x[i + 1], 5, -165796510);
        d = Md5Step3(d, a, b, c, x[i + 6], 9, -1069501632);
        c = Md5Step3(c, d, a, b, x[i + 11], 14, 643717713);
        b = Md5Step3(b, c, d, a, x[i + 0], 20, -373897302);
        a = Md5Step3(a, b, c, d, x[i + 5], 5, -701558691);
        d = Md5Step3(d, a, b, c, x[i + 10], 9, 38016083);
        c = Md5Step3(c, d, a, b, x[i + 15], 14, -660478335);
        b = Md5Step3(b, c, d, a, x[i + 4], 20, -405537848);
        a = Md5Step3(a, b, c, d, x[i + 9], 5, 568446438);
        d = Md5Step3(d, a, b, c, x[i + 14], 9, -1019803690);
        c = Md5Step3(c, d, a, b, x[i + 3], 14, -187363961);
        b = Md5Step3(b, c, d, a, x[i + 8], 20, 1163531501);
        a = Md5Step3(a, b, c, d, x[i + 13], 5, -1444681467);
        d = Md5Step3(d, a, b, c, x[i + 2], 9, -51403784);
        c = Md5Step3(c, d, a, b, x[i + 7], 14, 1735328473);
        b = Md5Step3(b, c, d, a, x[i + 12], 20, -1926607734);

        a = Md5Step4(a, b, c, d, x[i + 5], 4, -378558);
        d = Md5Step4(d, a, b, c, x[i + 8], 11, -2022574463);
        c = Md5Step4(c, d, a, b, x[i + 11], 16, 1839030562);
        b = Md5Step4(b, c, d, a, x[i + 14], 23, -35309556);
        a = Md5Step4(a, b, c, d, x[i + 1], 4, -1530992060);
        d = Md5Step4(d, a, b, c, x[i + 4], 11, 1272893353);
        c = Md5Step4(c, d, a, b, x[i + 7], 16, -155497632);
        b = Md5Step4(b, c, d, a, x[i + 10], 23, -1094730640);
        a = Md5Step4(a, b, c, d, x[i + 13], 4, 681279174);
        d = Md5Step4(d, a, b, c, x[i + 0], 11, -358537222);
        c = Md5Step4(c, d, a, b, x[i + 3], 16, -722521979);
        b = Md5Step4(b, c, d, a, x[i + 6], 23, 76029189);
        a = Md5Step4(a, b, c, d, x[i + 9], 4, -640364487);
        d = Md5Step4(d, a, b, c, x[i + 12], 11, -421815835);
        c = Md5Step4(c, d, a, b, x[i + 15], 16, 530742520);
        b = Md5Step4(b, c, d, a, x[i + 2], 23, -995338651);

        a = Md5Step5(a, b, c, d, x[i + 0], 6, -198630844);
        d = Md5Step5(d, a, b, c, x[i + 7], 10, 1126891415);
        c = Md5Step5(c, d, a, b, x[i + 14], 15, -1416354905);
        b = Md5Step5(b, c, d, a, x[i + 5], 21, -57434055);
        a = Md5Step5(a, b, c, d, x[i + 12], 6, 1700485571);
        d = Md5Step5(d, a, b, c, x[i + 3], 10, -1894986606);
        c = Md5Step5(c, d, a, b, x[i + 10], 15, -1051523);
        b = Md5Step5(b, c, d, a, x[i + 1], 21, -2054922799);
        a = Md5Step5(a, b, c, d, x[i + 8], 6, 1873313359);
        d = Md5Step5(d, a, b, c, x[i + 15], 10, -30611744);
        c = Md5Step5(c, d, a, b, x[i + 6], 15, -1560198380);
        b = Md5Step5(b, c, d, a, x[i + 13], 21, 1309151649);
        a = Md5Step5(a, b, c, d, x[i + 4], 6, -145523070);
        d = Md5Step5(d, a, b, c, x[i + 11], 10, -1120210379);
        c = Md5Step5(c, d, a, b, x[i + 2], 15, 718787259);
        b = Md5Step5(b, c, d, a, x[i + 9], 21, -343485551);

        a = Add16(a, olda);
        b = Add16(b, oldb);
        c = Add16(c, oldc);
        d = Add16(d, oldd);
    }
    return Int32ToHex(a) + Int32ToHex(b) + Int32ToHex(c) + Int32ToHex(d);
};


/*
 * Convert a 32-bit number to a hex string with ls-byte first.
 *
 * @param {Number} num
 * @return {String}
 */
function Int32ToHex(num)
{
    var hex_chr = "0123456789abcdef";
    str = "";
    for (j = 0; j <= 3; j++)
        str += hex_chr.charAt((num >> (j * 8 + 4)) & 0x0F) +
            hex_chr.charAt((num >> (j * 8)) & 0x0F);
    return str;
}

/*
 * Convert a string to a sequence of 16-word blocks, stored as an array.
 * Append padding bits and the length, as described in the MD5 standard.
 */
function str2blks_MD5(str)
{
    nblk = ((str.length + 8) >> 6) + 1;
    blks = new Array(nblk * 16);
    for (i = 0; i < nblk * 16; i++)
        blks[i] =
            0;
    for (i = 0; i < str.length; i++)
        blks[i >> 2] |= str.charCodeAt(i) << ((i % 4) * 8);
    blks[i >> 2] |= 0x80 << ((i % 4) * 8);
    blks[nblk * 16 - 2] = str.length * 8;
    return blks;
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function Add16(x, y)
{
    var lsw = (x & 0xFFFF) + (y & 0xFFFF);
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left
 */
function RotateLeft(num, cnt)
{
    return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * These functions implement the basic operation for each round of the
 * algorithm.
 */
function Md5PartProcess(q, a, b, x, s, t)
{
    return Add16(RotateLeft(Add16(Add16(a, q), Add16(x, t)), s), b);
}
function Md5Step2(a, b, c, d, x, s, t)
{
    return Md5PartProcess((b & c) | ((~b) & d), a, b, x, s, t);
}
function Md5Step3(a, b, c, d, x, s, t)
{
    return Md5PartProcess((b & d) | (c & (~d)), a, b, x, s, t);
}
function Md5Step4(a, b, c, d, x, s, t)
{
    return Md5PartProcess(b ^ c ^ d, a, b, x, s, t);
}
function Md5Step5(a, b, c, d, x, s, t)
{
    return Md5PartProcess(c ^ (b | (~d)), a, b, x, s, t);
}