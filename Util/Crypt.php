<?php

namespace DblEj\Util;

/**
 * Utility class for encrypting/decrypting data.
 */
class Crypt
{

    /**
     *
     * @param string $stringToEncrypt
     * @param string $key
     * @return string
     */
    public static function AesEncrypt($stringToEncrypt, $key)
    {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $stringToEncrypt, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    /**
     *
     * @param string $encryptedString
     * @param string $key
     * @return string
     */
    public static function AesDecrypt($encryptedString, $key)
    {
        //base64 doesnt allow spaces and for some reason
        //sometimes (intermittent) the string is saved with spaces instead of plus signs.
        //this converts those back to plus signs
        //otherwise card numbers are arbled, and declined by processor.
        if (strstr($encryptedString, " ") !== false)
        {
            $encryptedString = str_replace(" ", "+", $encryptedString);
        }
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($encryptedString), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    /**
     * @return string
     */
    public static function GenerateUuidV4()
    {
        $data = openssl_random_pseudo_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0010
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     *
     * @param string $namespace
     * @param string $name
     * @return string
     * @throws \DblEj\Cryptography\Security\CryptographyException
     */
    public static function GenerateUuidV5($namespace, $name)
    {
        if (preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $namespace) !== 1)
        {
            throw new \DblEj\Security\Cryptography\CryptographyException("Invalid namespace");
        }
        $nhex = str_replace(array(
            '-',
            '{',
            '}'), '', $namespace);
        $nstr = '';
        for ($i = 0; $i < strlen($nhex); $i+=2)
        {
            $nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
        }
        $hash = sha1($nstr . $name);
        return sprintf('%08s-%04s-%04x-%04x-%12s', substr($hash, 0, 8), substr($hash, 8, 4), (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000, (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000, substr($hash, 20, 12));
    }
}