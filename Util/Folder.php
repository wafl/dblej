<?php
namespace DblEj\Util;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Folder utility functions.
 */
class Folder
{
    /**
     * Get the files in a folder.  Optionally recursive.
     * @param string $dir
     * @param boolean $recursive
     * @param string $pattern
     * @param boolean $caseSensitive
     * @param boolean $ignorePaths
     * @return string[]
     */
    public static function GetFiles($dir, $recursive = false, $pattern = "*", $caseSensitive = true, $ignorePaths = array())
    {
        $returnFiles  = array();
        $childObjects = self::GetAllChildObjects($dir, $recursive, $pattern, $caseSensitive, $ignorePaths);
        foreach ($childObjects as $child)
        {
            if (is_file($child))
            {
                $returnFiles[] = $child;
            }
        }
        return $returnFiles;
    }

    /**
     * Get files and directories that are inside the specified folder
     * @param string $dir The directory to search in
     * @param bool $recursive If true, then all descendants will be returned
     * @param string $pattern A Unix-style file pattern string
     * @return array An array of strings representing the full absolute paths of the child objects
     */
    public static function GetAllChildObjects($dir, $recursive = false, $pattern = "*", $caseSensitive = true, array $ignorePaths = array())
    {
        $path = array();
        if (!Strings::EndsWith($dir, "\\") && !Strings::EndsWith($dir, "/"))
        {
            $dir.=DIRECTORY_SEPARATOR;
        }

        if ($pattern && (!Strings::StartsWith($pattern, "*")))
        {
            $pattern = str_replace("\\", "\\\\", $dir).$pattern;
        }

        $stack[] = $dir;
        while ($stack)
        {
            $thisdir = array_pop($stack);
            if ($dircont = scandir($thisdir))
            {
                $i = 0;
                while (isset($dircont[$i]))
                {
                    if ($dircont[$i] !== '.' && $dircont[$i] !== '..')
                    {
                        $current_file = "{$thisdir}{$dircont[$i]}";
                        if (is_dir($current_file))
                        {
                            $current_file.=DIRECTORY_SEPARATOR;
                        }
                        if (!in_array($current_file, $ignorePaths) && !in_array(basename($current_file), $ignorePaths))
                        {
                            if (is_dir($current_file) || is_file($current_file))
                            {
                                if (fnmatch($pattern, $current_file, (!$caseSensitive ? FNM_CASEFOLD : 0)))
                                {
                                    $path[] = "{$thisdir}{$dircont[$i]}";
                                }
                            }
                            if (is_dir($current_file))
                            {
                                if (fnmatch($pattern, $current_file))
                                {
                                    if ($recursive)
                                    {
                                        $stack[] = $current_file;
                                    }
                                }
                            }
                        }
                    }
                    $i++;
                }
            }
        }
        return $path;
    }

    /**
     * Perform operation on each child
     * @param string $dir The directory to search in
     * @param bool $recursive If true, then all descendants will be returned
     * @param string $pattern A Unix-style file pattern string
     * @return array An array of strings representing the full absolute paths of the child objects
     */
    public static function WalkAllChildObjects($dir, $operation, $recursive = false, $pattern = "*", $caseSensitive = true, array $ignorePaths = array(), $bottomUp = true, $includeFolders = true, $includeFiles = true)
    {
        $path = array();
        $dirStack = array();
        $childStack = array();
        if (!Strings::EndsWith($dir, "\\") && !Strings::EndsWith($dir, "/"))
        {
            $dir.=DIRECTORY_SEPARATOR;
        }
        $dirStack[] = $dir;
        while ($dirStack)
        {
            $thisdir = array_pop($dirStack);
            if ($dircont = scandir($thisdir))
            {
                $i = 0;
                while (isset($dircont[$i]))
                {
                    if ($dircont[$i] !== '.' && $dircont[$i] !== '..')
                    {
                        $current_file = "{$thisdir}{$dircont[$i]}";
                        if (is_dir($current_file))
                        {
                            $current_file.=DIRECTORY_SEPARATOR;
                        }
                        if (!in_array($current_file, $ignorePaths) && !in_array(basename($current_file), $ignorePaths))
                        {
                            if (is_dir($current_file) || is_file($current_file))
                            {
                                if (fnmatch($pattern, $current_file, !$caseSensitive ? FNM_CASEFOLD : 0))
                                {
                                    $path[] = "{$thisdir}{$dircont[$i]}";

                                    if (($includeFolders && is_dir($current_file)) || ($includeFiles && is_file($current_file)))
                                    {
                                        $childStack[] = "{$thisdir}{$dircont[$i]}";
                                    }
                                }
                            }
                            if (is_dir($current_file))
                            {
                                if (fnmatch($pattern, $current_file))
                                {
                                    if ($recursive)
                                    {
                                        $dirStack[] = $current_file;
                                    }
                                }
                            }
                        }
                    }
                    $i++;
                }
            }
        }

        if ($bottomUp)
        {
            $childStack = array_reverse($childStack);
        }
        foreach ($childStack as $item)
        {
            $operation($item);
        }
        return $path;
    }

    /**
     *
     * @param string $dir
     * @param string $extension
     * @return string
     */
    public static function GetNewestFileInFolder($dir, $extension = null)
    {
        $mostRecentFilePath  = "";
        $mostRecentFileMTime = 0;
        $iterator            = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($iterator as $fileinfo)
        {
            if ($fileinfo->isFile())
            {
                $filePath = $fileinfo->getPathname();
                $fileExt  = File::GetExtension($filePath);
                if (!$extension || ($fileExt == $extension))
                {
                    if ($fileinfo->getMTime() > $mostRecentFileMTime)
                    {
                        $mostRecentFileMTime = $fileinfo->getMTime();
                        $mostRecentFilePath  = $fileinfo->getPathname();
                    }
                }
            }
        }
        return $mostRecentFilePath;
    }

    public static function DeleteEverythingInFolder($folderPath)
    {
        self::WalkAllChildObjects($folderPath, function($path){ unlink($path);}, true);
        return true;
    }

    public static function DeleteFilesInFolder($folderPath, $recursive = false)
    {
        self::WalkAllChildObjects($folderPath, function($path){ unlink($path);}, $recursive, "*", true, [], true, false, true);
        return true;
    }
}