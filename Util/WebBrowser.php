<?php

namespace DblEj\Util;

class WebBrowser
{

    public static function GetCapabilities($agent = null)
    {
        $browscapini_cache = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "browsecap.ini.cache";
        if (!file_exists($browscapini_cache) || (time() - filemtime($browscapini_cache)) > 2592000)
        {
            //update the cache every 30 days
            $browscapini = __DIR__ . DIRECTORY_SEPARATOR . "php_browscap.ini";
            $brows = parse_ini_file($browscapini, true, INI_SCANNER_RAW);
            file_put_contents($browscapini_cache, serialize($brows));
        }
        $brows = unserialize(file_get_contents($browscapini_cache));

        //credit goes to http://php.net/manual/en/function.get-browser.php#70641
        $agent = $agent ? $agent : $_SERVER['HTTP_USER_AGENT'];
        $yu    = array();
        $q_s   = array(
            "#\.#",
            "#\*#",
            "#\?#");
        $q_r   = array(
            "\.",
            ".*",
            ".?");
        $hu = [];
        foreach ($brows as $k => $t)
        {
            if (fnmatch($k, $agent))
            {
                $yu['browser_name_pattern'] = $k;
                $pat                        = preg_replace($q_s, $q_r, $k);
                $yu['browser_name_regex']   = strtolower("^$pat$");
                foreach ($brows as $g => $r)
                {
                    if ($t['Parent'] == $g)
                    {
                        foreach ($brows as $a => $b)
                        {
                            if (isset($r['Parent']) && ($r['Parent'] == $a))
                            {
                                $yu = array_merge($yu, $b, $r, $t);
                                foreach ($yu as $d => $z)
                                {
                                    $l      = strtolower($d);
                                    $hu[$l] = $z;
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
        if (!isset($hu["device_type"]))
        {
            $hu["device_type"] = "unknown";
        }
        if (!isset($hu["browser"]))
        {
            $hu["browser"] = "unknown";
        }
        if (!isset($hu["version"]))
        {
            $hu["version"] = "unknown";
        }
        if (!isset($hu["platform"]))
        {
            $hu["platform"] = "unknown";
        }
        return $hu;
    }
}