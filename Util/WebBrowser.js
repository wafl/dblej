/*
 *@namespace DblEj.Util
 */
Namespace("DblEj.Util");

/*
 *@class WebBrowser
 *@static
 */
DblEj.Util.WebBrowser = {};

DblEj.Util.WebBrowser.Browsers =
    {
        IELT5: 1,
        IE5: 2,
        IE6: 3,
        IE7: 4,
        IE8: 5,
        IE9: 6,
        IE10: 7,
        IEGT10: 8,
        FFLT1: 10,
        FF1: 11,
        FF2: 12,
        FF3: 13,
        FFGT3: 14,
        MOZILLA: 20,
        NETSCAPE: 30,
        OTHER: 40
    };

/**
 * @function IsIeVariant
 * @static
 */
DblEj.Util.WebBrowser.IsIeVariant = function ()
{
    var returnVal = false;
    switch (DblEj.Util.WebBrowser.Browser)
    {
        case DblEj.Util.WebBrowser.Browsers.IELT5:
        case DblEj.Util.WebBrowser.Browsers.IE5:
        case DblEj.Util.WebBrowser.Browsers.IE6:
        case DblEj.Util.WebBrowser.Browsers.IE7:
        case DblEj.Util.WebBrowser.Browsers.IE8:
        case DblEj.Util.WebBrowser.Browsers.IE9:
        case DblEj.Util.WebBrowser.Browsers.IE10:
        case DblEj.Util.WebBrowser.Browsers.IEGT10:
            returnVal = true;
            break;
    }
    return returnVal;
};

DblEj.Util.WebBrowser.IsNonCompliantIe = function ()
{
    var returnVal = false;
    switch (DblEj.Util.WebBrowser.Browser)
    {
        case DblEj.Util.WebBrowser.Browsers.IELT5:
        case DblEj.Util.WebBrowser.Browsers.IE5:
        case DblEj.Util.WebBrowser.Browsers.IE6:
        case DblEj.Util.WebBrowser.Browsers.IE7:
        case DblEj.Util.WebBrowser.Browsers.IE8:
            returnVal = true;
            break;
    }
    return returnVal;
};

/**
 * @function CreateCookie
 * @static
 *
 * @param {String} cookieName
 * @param {String} value
 * @param {Nnumber} days
 */
DblEj.Util.WebBrowser.CreateCookie = function (cookieName, value, days, domain, path)
{
    var cookieString = cookieName + "=" + value;

    if (!IsDefined(path) || path === null)
    {
        path = "/";
    }
    if (IsDefined(days) && days)
    {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        cookieString += "; expires=" + date.toGMTString();
    }

    cookieString += "; path=" + path;

    if (IsDefined(domain) && domain)
    {
        cookieString += "; domain=" + domain;
    }

    document.cookie = cookieString;
};

/**
 * @function ReadCookie
 * @static
 *
 * @param {String} cookieName
 */
DblEj.Util.WebBrowser.ReadCookie = function (cookieName)
{
    var nameEQ = cookieName + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++)
    {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c =
                c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
};

/**
 * @function EraseCookie
 * @static
 *
 * @param {String} cookieName
 */
DblEj.Util.WebBrowser.EraseCookie = function (cookieName)
{
    DblEj.Util.WebBrowser.CreateCookie(cookieName, "", -1);
};

/**
 * @function GetSupportedInputTypes Get the input types that are supported by the current browser
 * @returns {DblEj.Util.WebBrowser.GetSupportedInputTypes.supported}
 */
DblEj.Util.WebBrowser.GetSupportedInputTypes = function()
{
    if (!IsDefined(DblEj.Util.WebBrowser._supportedInputTypes) || DblEj.Util.WebBrowser._supportedInputTypes == null)
    {
        DblEj.Util.WebBrowser._supportedInputTypes = {Date: false, Number: false, Time: false, Month: false, Week: false, Textbox: false, Hidden: false, Radio: false, Checkbox: false, File: false};
        var tester = document.createElement('input');

        for(var i in DblEj.Util.WebBrowser._supportedInputTypes){
            var lowerCaseType = i.toLowerCase();
            try
            {
                tester.type = lowerCaseType;
                tester.value = ':(';

                if(tester.type === lowerCaseType && tester.value === ''){
                    DblEj.Util.WebBrowser._supportedInputTypes[i] = true;
                }
            }
            catch (error)
            {

            }
        }
    }
    return DblEj.Util.WebBrowser._supportedInputTypes;
};

var userAgentString = navigator.userAgent.toLowerCase();
if (userAgentString.indexOf('msie 2') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IELT5;
} else if (userAgentString.indexOf('msie 3') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IELT5;
} else if (userAgentString.indexOf('msie 4') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IELT5;
} else if (userAgentString.indexOf('msie 5') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IE5;
} else if (userAgentString.indexOf('msie 6') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IE6
} else if (userAgentString.indexOf('msie 7') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IE7;
} else if (userAgentString.indexOf('msie 8') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IE8;
} else if (userAgentString.indexOf('msie 9') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IE9;
} else if (userAgentString.indexOf('msie 10') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IE10;
} else if (userAgentString.indexOf('msie') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.IEGT10;
} else if (userAgentString.indexOf('firefox/3') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.FF3;
} else if (userAgentString.indexOf('firefox/2') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.FF2;
} else if (userAgentString.indexOf('firefox/1') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.FF1;
} else if ((userAgentString.indexOf('firefox') > -1) && (userAgentString.indexOf('firefox/') == -1)) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.FFLT1;
} else if (userAgentString.indexOf('firefox/') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.FFGT3;
} else if (userAgentString.indexOf('mozilla') > -1) {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.MOZILLA;
} else {
    DblEj.Util.WebBrowser.Browser = DblEj.Util.WebBrowser.Browsers.OTHER;
}