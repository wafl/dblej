<?php
namespace DblEj\Util;

use finfo;

/**
 * File utility functions.
 */
class File
{

    /**
     * Get a file's extension
     * @param string $filepath
     * @return string
     *
     * @assert ("/Some/Test/File/Path.ext") == "ext"
     * @assert ("/Some/../T !@.est/File/Path.ext") == "ext"
     */
    public static function GetExtension($filepath)
    {
        $lastDotPos = strrpos($filepath, ".");
        if ($lastDotPos)
        {
            $fileLengthWithoutExt = strlen($filepath) - $lastDotPos;
            $ext                  = substr($filepath, $lastDotPos + 1, $fileLengthWithoutExt);
        }
        else
        {
            $ext = "";
        }
        return $ext;
    }

    /**
     * Get a file's MIME type
     * @param string $filepath The file to get the mime type of.
     * @param string $magicPath DEPRECATED Ignored since @1111.
     * @return string
     */
    public static function GetMimeType($filepath, $magicPath = "/usr/share/misc/magic")
    {
        try
        {
            $finfo = new finfo(FILEINFO_MIME);
            if ($finfo)
            {
                $mimeType = $finfo->file($filepath);
            }
        }
        catch (\Exception $err)
        {
            $mimeType = null;
        }
        if (!$mimeType || substr($mimeType, 0, 10) == "text/plain")
        {
            $ext = self::GetExtension($filepath);
            switch (strtolower($ext))
            {
                case "png":
                    $mimeType = "image/png";
                    break;
                case "jpg":
                    $mimeType = "image/jpeg";
                    break;
                case "gif":
                    $mimeType = "image/gif";
                    break;
                default:
                    if (substr($mimeType, 0, 10) != "text/plain")
                    {
                        $mimeType = "application/unknown";
                    }
            }
        }
        return $mimeType;
    }

    /**
     * Delete the specified file if it exists.
     * @param string $filePath
     * @return boolean Returns TRUE on success or FALSE if the file doesn't exist or if deletion fails.
     *
     * @assert ("Some/Non/Existent/FilePath.noexist") == false
     */
    public static function DeleteFile($filePath)
    {
        if (file_exists($filePath))
        {
            return unlink($filePath);
        }
        else
        {
            return false;
        }
    }
}