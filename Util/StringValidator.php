<?php

namespace DblEj\Util;

class StringValidator
{

    /**
     * Check if an email address is structured correctly
     * @param string $email the email address to be validated
     * @return boolean true if the email address is valid
     */
    public static function CheckEmailAddress($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     *
     * @param string $string
     * @return boolean
     */
    public static function IsStringAlphaNumeric($string)
    {
        return ctype_alnum($string);
    }
}