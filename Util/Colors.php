<?php

namespace DblEj\Util;

/**
 * Utility class for working with colors.
 */
class Colors
{

    /**
     * Returns a decimal value between 0 and 765 representing the difference between the two colors.
     * 0 means the colors are identical.  765 means the colors are exact opposites.
     * @param int $r1
     * @param int $g1
     * @param int $b1
     * @param int $r2
     * @param int $g2
     * @param int $b2
     * @return integer
     */
    public static function GetContrast($r1, $g1, $b1, $r2, $g2, $b2)
    {
        return max($r1, $r2) - min($r1, $r2) +
        max($g1, $g2) - min($g1, $g2) +
        max($b1, $b2) - min($b1, $b2);
    }

    public static function GetRgbFromHexString($hexString)
    {
        if (substr($hexString, 0, 1) == "#")
        {
            $hexString = substr($hexString, 1);
        }
        $r = substr($hexString, 0, 2);
        $g = substr($hexString, 2, 2);
        $b = substr($hexString, 4, 2);
        $r = hexdec($r);
        $g = hexdec($g);
        $b = hexdec($b);

        return [$r, $g, $b];
    }

    public static function GetRandomColor($minR = 0, $minG = 0, $minB = 0, $maxR = 255, $maxG = 255, $maxB = 255)
    {
        $r = mt_rand($minR, $maxR);
        $g = mt_rand($minG, $maxG);
        $b = mt_rand($minB, $maxB);
        return [$r, $g, $b];
    }
}