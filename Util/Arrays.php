<?php

namespace DblEj\Util;

class Arrays
{
    public static function MakeIndexesSequential(array $array)
    {
        $nonAssocArray = array_values($array);
        for ($arrayIdx = 0; $arrayIdx < count($nonAssocArray); $arrayIdx++)
        {
            if (is_array($nonAssocArray[$arrayIdx]))
            {
                $nonAssocArray[$arrayIdx] = self::MakeIndexesSequential($nonAssocArray[$arrayIdx]);
            }
        }

        return $nonAssocArray;
    }

    public static function GetElementValue($array, $elemKey, $defaultValue = null)
    {
        return isset($array[$elemKey])?$array[$elemKey]:$defaultValue;
    }
}