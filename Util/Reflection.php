<?php

namespace DblEj\Util;

/**
 * Reflection related utility methods.
 */
class Reflection
{

    /**
     *
     * @param string $fullyQualifiedClassname
     * @return string
     */
    public static function GetBaseClassname($fullyQualifiedClassname)
    {
        $reflection = new \ReflectionClass($fullyQualifiedClassname);
        return $reflection->getShortName();
    }
}