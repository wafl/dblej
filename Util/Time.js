/*
 *@namespace DblEj.Util
 */
Namespace("DblEj.Util");

/*
 *@class Time
 *@static
 */
DblEj.Util.Time = {};

DblEj.Util.Time.MonthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
DblEj.Util.Time.MonthNameAbbreviations = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
/**
 * @function GetReadableTime
 * @param {Number} timeStamp Optional, default=false
 * @param {Boolean} includeTime Optional, default=false
 * @param {Boolean} usaStyle Should the USA date format stype be used
 * @param {Boolean} useMonthName Optional, default=false
 * @param {Boolean} abbreviateMonthName Optional, default=false
 *
 * @return {String}
 * @static
 */
DblEj.Util.Time.GetReadableTime = function (timeStamp, includeTime, usaStyle, useMonthName, abbreviateMonthName)
{
    var d;
    var curr_date;
    var curr_month;
    var curr_year;
    var curHour;
    var curAmPm;
    var returnString = "";
    if (!IsDefined(includeTime) || includeTime == null)
    {
        includeTime = true;
    }
    if (!IsDefined(useMonthName) || useMonthName == null)
    {
        useMonthName = false;
    }
    if (!IsDefined(abbreviateMonthName) || abbreviateMonthName == null)
    {
        abbreviateMonthName = false;
    }
    if (IsDefined(timeStamp) && timeStamp != null)
    {
        d = new Date(timeStamp * 1000);
    } else {
        d = new Date();
    }
    if (!IsDefined(usaStyle))
    {
        usaStyle = false;
    }

    curr_date = d.getDate();
    curr_month = d.getMonth();
    if (useMonthName)
    {
        if (abbreviateMonthName)
        {
            curr_month = DblEj.Util.Time.MonthNameAbbreviations[curr_month];
        } else {
            curr_month = DblEj.Util.Time.MonthNames[curr_month];
        }
    }
    curr_year = d.getFullYear();
    curHour = d.getHours();

    if (useMonthName)
    {
        returnString = curr_month.toString() + " " + curr_date + ", " + curr_year;
    } else {
        curr_month++;
        if (usaStyle)
        {
            returnString = curr_month + "/" + curr_date + "/" + curr_year;
        } else {
            returnString = (curr_year + "-" + curr_month + "-" + curr_date);
        }
    }
    if (includeTime)
    {
        if (curHour > 12)
        {
            curHour = curHour - 12;
            curAmPm = "pm";
        } else {
            curAmPm = "am";
        }
        returnString = returnString + " " + curHour + ":" + d.getMinutes() + ":" + d.getSeconds() + " " + curAmPm;
    }
    return returnString;
};

/**
 * @function GetTimestamp
 * Get the number of seconds since Unix epoch
 * 
 * @returns {Number}
 */
DblEj.Util.Time.GetTimestamp = function ()
{
    return Math.round(DblEj.Util.Time.GetTimestampMs() / 1000);
};


/**
 * @function GetTimestampMs
 * Get the number of milliseconds since Unix epoch
 *
 * @returns {Number}
 */
DblEj.Util.Time.GetTimestampMs = function ()
{
    var now = new Date();
    return Math.round(now.getTime());
};

/**
 *
 * @returns {Number}
 */
DblEj.Util.Time.GetTimezoneOffset = function ()
{
    return (new Date()).getTimezoneOffset() * -1;
};
DblEj.Util.Time.FormatTimestamp = function(format, timeStamp)
{
    if (IsNullOrUndefined(timeStamp))
    {
        timeStamp = DblEj.Util.Time.GetTimestamp() * 1000;
    }
    var date = new Date(timeStamp);
    function pad(value) {
        return (value.toString().length < 2) ? '0' + value : value;
    }
    return format.replace(/%([a-zA-Z])/g, function (_, fmtCode) {
        switch (fmtCode) {
        case 'Y':
            return date.getUTCFullYear();
        case 'y':
            return date.getUTCFullYear().toString().substr(2, 2);
        case 'm':
            return pad(date.getUTCMonth() + 1);
        case 'd':
            return pad(date.getUTCDate());
        case 'H':
            return pad(date.getUTCHours());
        case 'M':
            return pad(date.getUTCMinutes());
        case 'S':
            return pad(date.getUTCSeconds());
        default:
            throw new Error('Unsupported format code: ' + fmtCode);
        }
    });
};
DblEj.Util.Time.ParseTimestamp = function(timeStamp)
{
    var d;
    var curr_date;
    var curr_month;
    var curr_year;
    var curHour;
    var curAmPm;
    if (IsDefined(timeStamp) && timeStamp != null)
    {
        d = new Date(timeStamp * 1000);
    } else {
        d = new Date();
    }

    curr_date = d.getDate();
    curr_month = d.getMonth()+1;
    curr_year = d.getFullYear();
    curHour = d.getHours();
    if (curHour > 12)
    {
        curHour = curHour - 12;
        curAmPm = "pm";
    } else {
        curAmPm = "am";
    }
    return { "Date": curr_date, "Month": curr_month, "Year": curr_year, "Hour": curHour };
};

DblEj.Util.Time.GetTimeLapsedString = function(timeStamp, roundToLargestUnit)
{
    return DblEj.Util.Time.GetDurationString(Math.floor(((new Date()).getTime() / 1000) - timeStamp), roundToLargestUnit);
};

/**
 * @function GetDurationString
 * Get a human-readable text string to represent the specified duration.
 * Credit goes to http://stackoverflow.com/a/3177838
 * @param {type} timeStamp
 * @returns {String}
 */
DblEj.Util.Time.GetDurationString = function(seconds, roundToLargestUnit)
{
    if (!IsDefined(roundToLargestUnit))
    {
        roundToLargestUnit = false;
    }
    var years = Math.floor(seconds / 31536000);
    seconds -= years * 31536000;
    var days     = Math.floor(seconds / 86400);
    seconds -= days * 86400;
    var hours    = Math.floor(seconds / 3600);
    seconds -= hours * 3600;
    var minutes  = Math.floor(seconds / 60);
    seconds  = Math.ceil(seconds - minutes * 60);
    var returnString = "";


    if (years > 0)
    {
        returnString += years + " year" + (years>1?"s":"");
    }

    if ((!roundToLargestUnit || (years < 1)) && days > 0)
    {
        returnString += " " + days + " day" + (days>1?"s":"");
    }

    if ((!roundToLargestUnit || (days < 1)) && hours > 0)
    {
        returnString += " " + hours + " hour" + (hours>1?"s":"");
    }
    if ((!roundToLargestUnit || (days < 1 && hours < 1)) && minutes > 0)
    {
        returnString += " " + minutes + " minute" + (minutes>1?"s":"");
    }

    if ((!roundToLargestUnit || (days < 1 && hours < 1 && minutes < 1)) && seconds > 0)
    {
        returnString += " " + seconds + " second" + (seconds>1?"s":"");
    }
    if (returnString == "")
    {
        returnString = "0 seconds";
    }
    else if (returnString.substr(0, 1) == ",")
    {
        returnString = returnString.substr(1, returnString.length-1);
    }
    return returnString;
};