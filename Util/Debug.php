<?php

namespace DblEj\Util;

/**
 * Debugging utility functions.
 */
class Debug
{

    /**
     *
     * @param boolean $ignoreArgs
     * @param integer $limit
     * @return string
     */
    public static function GetStackTrace($ignoreArgs = true, $limit = 50, $ignoreLastTraceSteps = 0)
    {
        $ignoreLastTraceSteps++;
        $backtrace = debug_backtrace($ignoreArgs ? DEBUG_BACKTRACE_IGNORE_ARGS : null, $limit);
        if ($ignoreLastTraceSteps && (count($backtrace) > $ignoreLastTraceSteps))
        {
            $backtrace = array_slice($backtrace, $ignoreLastTraceSteps, null, false);
        }
        return $backtrace;
    }

    public static function DieR($expression, $maxDepth = 10, $returnHtml = true, $hidePrivateMembers = true, $includeBacktrace = true, $backtraceLimit = 50, $ignoreLastTraceSteps = 1)
    {
        $ignoreLastTraceSteps++;
        die(self::GetVarString($expression, $maxDepth, $returnHtml, $hidePrivateMembers, $includeBacktrace, $backtraceLimit, $ignoreLastTraceSteps));
    }

    public static function PrintR($expression, $maxDepth = 10, $returnHtml = true, $hidePrivateMembers = true, $includeBacktrace = true, $backtraceLimit = 50, $ignoreLastTraceSteps = 1)
    {
        $ignoreLastTraceSteps++;
        print(self::GetVarString($expression, $maxDepth, $returnHtml, $hidePrivateMembers, $includeBacktrace, $backtraceLimit, $ignoreLastTraceSteps));
    }

    public static function GetVarString($expression, $maxDepth = 10, $returnHtml = true, $hidePrivateMembers = true, $includeBacktrace = true, $backtraceLimit = 50, $ignoreLastTraceSteps = 0)
    {
        $ignoreLastTraceSteps++;
        $returnString = self::recursiveGetVarString($expression, $maxDepth, 4, $hidePrivateMembers);
        if ($includeBacktrace)
        {
            $backtrace = self::GetStackTrace(true, $backtraceLimit, $ignoreLastTraceSteps);
            $returnString .= "\nBacktrace: ".print_r($backtrace, true);
        }
        if ($returnHtml)
        {
            $returnString = "<pre style='text-align: left; max-width: 1000px; margin: 1em auto; border: solid 1px #dfdfdf; border-radius: .25em; padding: 1em;'>$returnString</pre>";
        }
        return $returnString;
    }

    public static function GetResourceAsString($resource)
    {
        $resourceType = get_resource_type($resource);
        return "$resourceType resource";
    }

    public function GetArrayAsString(array $array, $maxDepth = 10, $tabWidth = 4, $hidePrivateMembers = true)
    {
        return self::recursiveGetArrayAsString($array, $maxDepth, $tabWidth, $hidePrivateMembers);
    }

    public function GetObjectAsString($object, $maxDepth = 10, $tabWidth = 4, $hidePrivateMembers = true)
    {
        return self::recursiveGetObjectAsString($object, $maxDepth, $tabWidth, $hidePrivateMembers);
    }

    private static function recursiveGetVarString($expression, $maxDepth = 10, $tabWidth = 4, $hidePrivateMembers = true, $currentDepth = 0, $currentCursorIndent = 0, &$evaluatedExpressions = null)
    {
        if ($evaluatedExpressions == null)
        {
            $evaluatedExpressions = array();
        }

        if (!is_object($expression) || (array_search($expression, $evaluatedExpressions, true) === false))
        {
            $evaluatedExpressions[] = $expression;
            $returnString           = "";

            $dataType = strtolower(gettype($expression));
            if ($dataType == "object")
            {
                $classInfo = new \ReflectionClass($expression);
                $dataType = strtolower($classInfo->getName());
            }
            switch ($dataType)
            {
                case "string":
                    $returnString .= var_export($expression, true);
                    break;
                case "boolean":
                    $returnString .= $expression ? "true" : "false";
                    break;
                case "integer":
                case "double":
                case "float":
                    $returnString .= $expression;
                    break;
                case "array":
                case "arrayiterator":
                    if ($currentDepth < $maxDepth)
                    {
                        $returnString .= self::recursiveGetArrayAsString($expression, $maxDepth, $tabWidth, $hidePrivateMembers, $currentDepth, $currentCursorIndent, $evaluatedExpressions);
                    }
                    else
                    {
                        $returnString .= "[truncated array]";
                    }
                    break;
                case "resource":
                    $returnString .= self::GetResourceAsString($expression);
                    break;
                case "null":
                    $returnString .= "null";
                    break;
                case "unknown type":
                    $returnString .= "unknown/invalid data type";
                    break;
                case "object":
                default:
                    if ($currentDepth < $maxDepth)
                    {
                        if (is_a($expression, "\\DblEj\\Data\\Model"))
                        {
                            $returnString = self::recursiveGetArrayAsString($expression->Get_FieldValues(), $maxDepth, $tabWidth, $hidePrivateMembers, $currentDepth, $currentCursorIndent, $evaluatedExpressions);
                        } else {
                            $returnString .= self::recursiveGetObjectAsString($expression, $maxDepth, $tabWidth, $hidePrivateMembers, $currentDepth, $currentCursorIndent, $evaluatedExpressions);
                        }
                    }
                    else
                    {
                        $returnString .= "{truncated object}";
                    }
                    break;
            }
        }
        else
        {
            $returnString = "*recursion*";
        }

        return $returnString;
    }

    private static function recursiveGetArrayAsString($array, $maxDepth = 10, $tabWidth = 4, $hidePrivateMembers = true, $currentDepth = 0, $currentCursorIndent = 0, &$evaluatedExpressions = null)
    {
        $currentDepth++;
        $indentString = str_pad("", $currentCursorIndent);
        $returnString = "Array\n$indentString(\n";

        $indentString = str_pad("", $tabWidth + $currentCursorIndent);
        $startObjectCursorIndent = $currentCursorIndent;

        if ($currentDepth <= $maxDepth)
        {
            foreach ($array as $key => $val)
            {
                $returnString .= $indentString . "[$key] => ";
                $currentCursorIndent = strlen($indentString);
                $returnString .= self::recursiveGetVarString($val, $maxDepth, $tabWidth, $hidePrivateMembers, $currentDepth, $currentCursorIndent, $evaluatedExpressions) . "\n";
            }
        }
        else
        {
            $returnString .= "{$indentString}...";
        }
        $indentString = str_pad("", $startObjectCursorIndent);
        $returnString .= "{$indentString})\n";

        return $returnString;
    }

    public static function recursiveGetObjectAsString($object, $maxDepth = 10, $tabWidth = 4, $hidePrivateMembers = true, $currentDepth = 0, $currentCursorIndent = 0, &$evaluatedExpressions = null)
    {
        if (gettype($object) !== "object")
        {
            throw new \InvalidArgumentException("The passed value is not a valid object");
        }

        $currentDepth++;
        $indentString = str_pad("", $currentCursorIndent);

        $returnString     = "{\n";
        $reflectionObject = new \ReflectionObject($object);

        $indentString            = str_pad("", $tabWidth + $currentCursorIndent);
        $startObjectCursorIndent = $currentCursorIndent;
        if ($currentDepth <= $maxDepth)
        {
            foreach ($reflectionObject->getProperties() as $property)
            {
                if (!$hidePrivateMembers || !$property->isPrivate())
                {
                    if ($property->isPrivate() || $property->isProtected())
                    {
                        $property->setAccessible(true);
                    }
                    $propertyName  = $property->getName();
                    $propertyValue = $property->getValue($object);
                    if ($property->isPrivate() || $property->isProtected())
                    {
                        $property->setAccessible(false);
                    }
                    $returnString .= $indentString . "$propertyName: ";
                    $currentCursorIndent = strlen($indentString . "$propertyName: ");
                    $returnString .= self::recursiveGetVarString($propertyValue, $maxDepth, $tabWidth, $hidePrivateMembers, $currentDepth, $currentCursorIndent, $evaluatedExpressions) . "\n";
                }
            }
        }
        else
        {
            $returnString .= "...";
        }

        $indentString = str_pad("", $startObjectCursorIndent);
        $returnString .= "{$indentString}}\n";

        return $returnString;
    }
}