/*
 *@namespace DblEj.Util
 */
/* global DblEj */

Namespace("DblEj.Util");

/*
 *@class Objects
 *@static
 */
DblEj.Util.Objects = {};

/**
 * @function CloneObjectWithPropertiesSortedByName
 * @static
 *
 * @param {Object} object
 * @param {Boolean} reverse
 */
DblEj.Util.Objects.CloneObjectWithPropertiesSortedByName = function (object, reverse) {
    // Setup Arrays
    var sortedKeys = new Array();
    var sortedObj = {};

    // Separate keys and sort them
    for (var i in object) {
        sortedKeys.push(i);
    }
    sortedKeys.sort();
    if (reverse)
    {
        sortedKeys.reverse();
    }
    // Reconstruct sorted obj based on keys
    for (var i in sortedKeys) {
        sortedObj[sortedKeys[i]] = object[sortedKeys[i]];
    }
    return sortedObj;
};

/**
 * @function CloneObjectWithPropertiesSortedByCustom
 * @static
 *
 * @param {Object} object
 * @param {Boolean} reverse
 * @param {Function} compareMethod
 */
DblEj.Util.Objects.CloneObjectWithPropertiesSortedByCustom = function (object, reverse, compareMethod) {
    // Setup Arrays
    var sortedKeys = new Array();
    var sortedObj = {};

    // Separate keys and sort them
    for (var i in object) {
        sortedKeys.push([i, object[i]]);
    }
    sortedKeys.sort(compareMethod);
    if (reverse)
    {
        sortedKeys.reverse();
    }
    // Reconstruct sorted obj based on keys
    for (var i in sortedKeys) {
        sortedObj[sortedKeys[i][0]] = sortedKeys[i][1];
    }
    return sortedObj;
};