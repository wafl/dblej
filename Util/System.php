<?php
namespace DblEj\Util;

class System
{
    public static function GetLibraryVersion($versionPart = null)
    {
        static $version = null;
        if (!$version)
        {
            $versionFile   = __DIR__ . DIRECTORY_SEPARATOR . "..".DIRECTORY_SEPARATOR.".dblej";
            if (file_exists($versionFile))
            {
                $dblejMetaFile = file($versionFile);
                $version      = explode(".", trim($dblejMetaFile[3]));
            }
        }
        return $versionPart !== null?$version[$versionPart]:$version;
    }
}