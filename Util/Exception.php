<?php

namespace DblEj\Util;

class Exception
{

    /**
     *
     * @param integer $severity
     * @return string
     */
    public static function GetSeverityConstantName($severity)
    {
        switch ($severity)
        {
            case E_WARNING:
                return "E_WARNING";
                break;
            case E_PARSE:
                return "E_PARSE";
                break;
            case E_ERROR:
                return "E_ERROR";
                break;
            case E_NOTICE:
                return "E_NOTICE";
                break;
            case E_CORE_ERROR:
                return "E_CORE_ERROR";
                break;
            case E_CORE_WARNING:
                return "E_CORE_WARNING";
                break;
            case E_COMPILE_ERROR:
                return "E_COMPILE_ERROR";
                break;
            case E_COMPILE_WARNING:
                return "E_COMPILE_WARNING";
                break;
            case E_USER_ERROR:
                return "E_USER_ERROR";
                break;
            case E_USER_WARNING:
                return "E_USER_WARNING";
                break;
            case E_USER_NOTICE:
                return "E_USER_NOTICE";
                break;
            case E_STRICT:
                return "E_STRICT";
                break;
            case E_RECOVERABLE_ERROR:
                return "E_RECOVERABLE_ERROR";
                break;
            case E_DEPRECATED:
                return "E_DEPRECATED";
                break;
            case E_USER_DEPRECATED:
                return "E_USER_DEPRECATED";
                break;
        }
    }
}