/*
 *@namespace DblEj.Util
 */
Namespace("DblEj.Util");

/*
 *@class StringValidator
 *@static
 */
DblEj.Util.StringValidator = {};

/**
 * @function CheckEmailAddress
 * @static
 *
 * @param {String} emailAddress
 */
DblEj.Util.StringValidator.CheckEmailAddress = function (emailAddress)
{
    var apos = emailAddress.indexOf("@");
    var dotpos = emailAddress.lastIndexOf(".");
    var isValid = false;
    if (apos < 1 || dotpos - apos < 2)
    {
        isValid = false;
    }
    else
    {
        isValid = true;
    }
    return isValid;
};
/**
 * @function CheckUsername
 * @static
 *
 * @param {String} username
 */
DblEj.Util.StringValidator.CheckUsername = function (username)
{
    var isValid = false;
    var regex = new RegExp("[^a-zA-Z0-9\-]");
    var match = regex.exec(username);
    isValid = (match == null);
    return isValid;
};

/**
 *
 * @param {String} cardNumber
 * @returns {String}
 */
DblEj.Util.StringValidator.CheckCreditCardNumber = function (cardNumber)
{
    //*CARD TYPES            *PREFIX                            *WIDTH
    //American Express       34, 37                             15
    //Diners Club            300 to 305, 36                     14
    //Carte Blanche          38                                 14
    //Discover               6011, 622126-622925, 644-649, 65   16
    //EnRoute                2014, 2149                         15
    //JCB                    3                                  16
    //JCB                    2131, 1800                         15
    //Mastercard             50 to 55                           16
    //Visa                   4                                  13, 16

    var cardType = null;
    cardNumber = cardNumber.toString();
    cardNumber = cardNumber.replace(/[^A-Za-z0-9]/g, "");
    if (cardNumber.length > 12 && IsNumeric(cardNumber) && cardNumber.length < 17)
    {
        var firstTwo = parseInt(cardNumber.substring(0, 2));
        if (firstTwo == 34 || firstTwo == 37)
        {
            cardType = "American Express";
        }
        else if (firstTwo == 36)
        {
            cardType = "Diners Club";
        }
        else if (firstTwo == 38)
        {
            cardType = "Carte Blanche";
        }
        else if (firstTwo == 65)
        {
            cardType = "Discover";
        }
        else if (firstTwo >= 50 && firstTwo <= 55)
        {
            cardType = "Mastercard";
        }
        else
        {
            var firstFive = parseInt(cardNumber.substring(0, 5));
            if (firstFive >= 622126 && firstFive <= 622925)
            {
                cardType = "Discover";
            } else {
                var firstFour = parseInt(cardNumber.substring(0, 4));
                if (firstFour == 2014 || firstFour == 2149)
                {
                    cardType = "EnRoute";
                }
                else if (firstFour == 2131 || firstFour == 1800)
                {
                    cardType = "JCB";
                }
                else if (firstFour == 6011)
                {
                    cardType = "Discover";
                } else {
                    var firstThree = parseInt(cardNumber.substring(0, 3));
                    if (firstThree >= 300 && firstThree <= 305)
                    {
                        cardType = "American Diners Club";
                    } else if (firstThree >= 644 && firstThree <= 649)
                    {
                        cardType = "Discover";
                    } else {
                        var firstOne = parseInt(cardNumber.substring(0, 1));
                        if (firstOne == 3)
                        {
                            cardType = "JCB";
                        }
                        else if (firstOne == 4)
                        {
                            cardType = "Visa";
                        }
                    }
                }
            }
        }
    }
    return cardType;
};

/**
 *
 * @param {mixed} mixed_var
 * @returns {Boolean}
 */
DblEj.Util.StringValidator.IsNumeric = function (mixed_var)
{
    return (typeof (mixed_var) === 'number' || typeof (mixed_var) === 'string') && mixed_var !== '' && !isNaN(mixed_var);
};