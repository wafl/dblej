<?php
namespace DblEj\Util;

class Time
{

    public static function GetDaysInMonth($month, $year, $calendar = 0)
    {
        switch ($calendar)
        {
            case CAL_GREGORIAN:
                return date('t', mktime(0, 0, 0, $month, 1, $year));
            default:
                throw new \Exception("Only the GREGORIAN calendar is supported");
        }
    }

    /**
     * Get a human-readable text string to represent the specified duration.
     *
     * @param int $durationInSeconds The duration, specified in total number of seconds.
     * @param string $daysLabel The label to use for days.
     * @param string $hoursLabel The label to use for hours.
     * @param string $minutesLabel The label to use for minutes.
     * @param string $secondsLabel The label to use for seconds.
     * @param string $separatorBetweenUnits The character to use between each unit of time.
     * @param string $separatorBetweenValueAndLabel The character to use between each numeric unit and it's suffix label.
     * @param boolean $padEachSection
     * @param boolean $roundToLargestUnit If TRUE, then only represent up to the last time unit that has a value greater than zero.  For example, 120 seconds would be converted to "2 minutes" instead of "0 Hours 2 Minutes 1 Second"
     *
     * @return string a string such as "5 days, 3 hours, and 2 minutes"
     *
     * @assert (120, "d", "h", "m", "s", ",", "", false, true) == "2m"
     * @assert (122, "d", "h", "m", "s", ",", "", false, false) == "0d,0h,2m,2s"
     * @assert (122, "d", "h", "m", "s", ",", "", true, false) == "00d,00h,02m,02s"
     */
    public static function GetDurationString($durationInSeconds, $daysLabel = "days", $hoursLabel = "hours", $minutesLabel = "minutes", $secondsLabel = "seconds", $separatorBetweenUnits = " ", $separatorBetweenValueAndLabel = " ", $padEachSection = true, $roundToLargestUnit = false)
    {
        $duration = '';
        $days     = floor($durationInSeconds / 86400);
        $durationInSeconds -= $days * 86400;
        $hours    = floor($durationInSeconds / 3600);
        $durationInSeconds -= $hours * 3600;
        $minutes  = floor($durationInSeconds / 60);
        $seconds  = ceil($durationInSeconds - $minutes * 60);

        if (!$roundToLargestUnit || $days > 0)
        {
            if ($padEachSection || ($days > 0))
            {
                if ($padEachSection)
                {
                    $duration .= str_pad($days, 2, 0, STR_PAD_LEFT) . $separatorBetweenValueAndLabel . "$daysLabel";
                }
                else
                {
                    $duration .= $days . $separatorBetweenValueAndLabel . "$daysLabel";
                }
            }
        }
        if (!$roundToLargestUnit || ($days < 1 && $hours > 0))
        {
            if ($padEachSection || ($hours > 0))
            {
                if ($padEachSection)
                {
                    $duration .= $separatorBetweenUnits . str_pad($hours, 2, 0, STR_PAD_LEFT) . $separatorBetweenValueAndLabel . "$hoursLabel";
                }
                else
                {
                    $duration .= $separatorBetweenUnits . $hours . $separatorBetweenValueAndLabel . "$hoursLabel";
                }
            }
        }
        if (!$roundToLargestUnit || ($days < 1 && $hours < 1 && $minutes > 0))
        {
            if ($padEachSection || ($minutes > 0))
            {
                if ($padEachSection)
                {
                    $duration .= $separatorBetweenUnits . str_pad($minutes, 2, 0, STR_PAD_LEFT) . $separatorBetweenValueAndLabel . "$minutesLabel";
                }
                else
                {
                    $duration .= $separatorBetweenUnits . $minutes . $separatorBetweenValueAndLabel . "$minutesLabel";
                }
            }
        }
        if (!$roundToLargestUnit || ($days < 1 && $hours < 1 && $minutes < 1 && $seconds > 0))
        {
            if ($padEachSection || ($seconds > 0))
            {
                if ($padEachSection)
                {
                    $duration .= $separatorBetweenUnits . str_pad($seconds, 2, 0, STR_PAD_LEFT) . $separatorBetweenValueAndLabel . "$secondsLabel";
                }
                else
                {
                    $duration .= $separatorBetweenUnits . $seconds . $separatorBetweenValueAndLabel . "$secondsLabel";
                }
            }
        }
        if ($duration == "")
        {
            return "0 $secondsLabel";
        }
        elseif (substr($duration, 0, 1) == ",")
        {
            $duration = substr($duration, 1);
        }
        return trim($duration);
    }
}