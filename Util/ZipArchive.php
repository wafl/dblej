<?php
/*
 * Based on:
 *
 * FlxZipArchive, Extends ZipArchiv.
 * by Nicolas Heimann
 * Add Dirs with Files and Subdirs.
 * Add Files & Dirs to archive.
 *
 */
namespace DblEj\Util;

class ZipArchive
extends \ZipArchive
{

    /**
     * Add a Dir with Files and Subdirs to the archive
     * modified from its original for DblEj
     *
     * @param string $location Real Location
     * @param string $name Name in Archive
     * @param mixed $excludes the name of a file or an array of folder names to exclude from archive
     * @author Nicolas Heimann
     * */
    public function addDir($location, $name, $excludes)
    {
        $this->addEmptyDir($name);

        $this->_addDirDo($location, $name, $excludes);
    }

    private function _addDirDo($location, $name, $excludes)
    {
        $name .= '/';
        $location .= '/';

        if (!is_array($excludes))
        {
            $excludes = array($excludes);
        }
        // Read all Files in Dir
        $dir  = opendir($location);
        while ($file = readdir($dir))
        {
            if ($file == '.' || $file == '..' || array_search($file, $excludes) !== false)
            {
                continue;
            }
            // Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
            if ((filetype($location . $file) == 'dir'))
            {
                $this->addDir($location . $file, $name . $file, $excludes);
            }
            else
            {
                $this->addFile($location . $file, $name . $file);
            }
        }
    }
}