<?php

namespace DblEj\Util;

/**
 * Math utility functions
 */
class Math
{

    /**
     * Get the fibonacci value for the specified sequence position.
     * @param integer $seriesNumber The sequence position to get the value for.
     * The sequence starts at 0, as do the sequence position indexes.
     * For example, a value of 3 will get the fourth number in a Fibonacci sequence (2).
     *
     * The sequence is:
     * 0,1,1,2,3,5,8,13,21,34,55,89,144, ...
     *
     * Or, if notated with the indexes (the $seriesNumber is in brackets):
     * [0]=0, [1]=1, [2]=1, [3]=2, [4]=3, [5]=5, [6]=8, [7]=13, [8]=21, [9]=34, [10]=55, [11]=89, [12]=144, ...
     *
     * @return integer Returns the fibonacci value.
     * @throws \Exception Throws an exception if an invalid sequence position is specified.
     *
     * @assert (3) == 2
     * @assert (8) == 21
     */
    public static function GetFibonacci($seriesNumber)
    {
        $returnValue = 0;
        if ($seriesNumber < 0)
        {
            throw new \Exception("Series number cannot be less than 0");
        }
        if ($seriesNumber == 0)
        {
            $returnValue = 0;
        }
        elseif ($seriesNumber <= 2)
        {
            $returnValue = 1;
        }
        else
        {
            $int1 = 1;
            $int2 = 1;

            $fib = 0;

            for ($i = 1; $i <= $seriesNumber - 2; $i++)
            {
                $fib  = $int1 + $int2;
                $int2 = $int1;
                $int1 = $fib;
            }
            $returnValue = $fib;
        }
        return $returnValue;
    }

    /**
     * Round the specified $number to the nearest value that is a factor of $quantize.
     *
     * @param float $number The number to be rounded.
     * @param integer $quantize The value to snap $number to the nearest factor of.  For example, to round $number to the nearest 1/4th, $quantize would need to be seat at .25.
     * @param integer $mode Whether to round up or down.  -1 Rounds down.  1 Rounds up.  0, or any other $mode will round to the nearest value.
     * @return integer The quantized result.
     *
     * @assert (111, 25) == 125
     * @assert (111, 25, -1) == 100
     * @assert (111, 25, 0) == 100
     * @assert (.7, .25, 1) == .75
     */
    public static function RoundQuantized($number, $quantize, $mode = 1)
    {
        if ($mode == 1)
        {
            return ceil($number / $quantize) * $quantize;
        }
        elseif ($mode == -1)
        {
            return floor($number / $quantize) * $quantize;
        }
        else
        {
            return round($number / $quantize) * $quantize;
        }
    }

    public static function SubtractFixedDecimals($number1, $number2, $precision = 2)
    {
        $precisionMultiplier = pow(10, $precision);
        $number1 = intval(round($number1 * $precisionMultiplier));
        $number2 = intval(round($number2 * $precisionMultiplier));

        if ($number2 > $number1)
        {
            $answer = ($number2 - $number1) * -1;
        } else {
            $answer = $number1 - $number2;
        }
        $answer = $answer / 100;
        return $answer;
    }

    /**
     * Get the modulo between two floating point numbers
     * @param float $x
     * @param float $y
     * @return float The remainder if dividing $y into $x
     */
    public static function RoundMod($x, $y) {
        $i = round($x / $y);
        return round($x - $i * $y, 12);
    }
}