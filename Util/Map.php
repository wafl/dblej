<?php

namespace DblEj\Util;

class Map
{
    /**
     * Calculate the distance between to points on earth.
     * @param \DblEj\UI\Point $coord1 The first point, x = Latitude degrees, y = Longitude degrees
     * @param \DblEj\UI\Point $coord2 The second point, x = Latitude degrees, y = Longitude degrees
     *
     * @return float The distance between the points, in kilometers
     */
    public static function GetDistanceBetweenCoordinates(\DblEj\UI\Point $coord1, \DblEj\UI\Point $coord2)
    {
        $R = 6371; // km
        $lat1Rad = deg2rad($coord1->Get_X());
        $lat2Rad = deg2rad($coord2->Get_X());
        $latDiffRad = deg2rad($coord2->Get_X()-$coord1->Get_X());
        $lonDiffRad = deg2rad($coord2->Get_Y()-$coord1->Get_Y());

        $a = sin($latDiffRad/2) * sin($latDiffRad/2) + cos($lat1Rad) * cos($lat2Rad) * sin($lonDiffRad/2) * sin($lonDiffRad/2);
        $angularDistance = 2 * atan2(sqrt($a), sqrt(1-$a));

        return $R * $angularDistance;
    }
}