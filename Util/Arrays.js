/*
 *@namespace DblEj.Util
 */
/* global DblEj */

Namespace("DblEj.Util");

/*
 * @class Arrays
 * @static
 *
 * @deprecated since revision 1629 in favor of native array extension
 */

DblEj.Util.Arrays = {};

/**
 * @function GetElementIndex
 * @static
 *
 * @param {Array} array
 * @param {String|Number} elemValue
 */
DblEj.Util.Arrays.GetElementIndex = function (array, elemValue) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == elemValue)
            return i;
    }
    return -1;
};

/**
 * @function HasElementWithValue
 * @static
 *
 * @param {Array} array
 * @param {String|Number} elemValue
 */
DblEj.Util.Arrays.HasElementWithValue = function (array, elemValue) {
    return DblEj.Util.Arrays.GetElementIndex(array, elemValue) >= 0;
};