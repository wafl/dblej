<?php

namespace DblEj\Util;

/**
 * A file related exception.
 */
class FileException
extends \DblEj\System\Exception
{

    public function __construct($message, $severity = E_ERROR, $inner = null)
    {
        parent::__construct("$message", $severity, $inner);
    }
}