/**
 * @class Global
 */

/**
 * @function _ Asyncronous ajax request
 *
 * @param {String} call
 * @param {object} [requestObject]
 * @param {String} [sendToken]
 * @param {function} [callbackMethod]
 * @param {String} [method=post]
 * @param {Boolean} [useDefaultJsonParser=true]
 *
 * @return {Boolean} indicates if the send was successful
 */
function _(call, requestObject, sendToken, callbackMethod, method, useDefaultJsonParser)
{
    return DblEj.Communication.Ajax.Utils.SendRequestAsync(call, requestObject, sendToken, callbackMethod, method, useDefaultJsonParser);
}

/**
 * @function __ Synchronous ajax request
 *
 * @param {String} call
 * @param {object} [requestObject]
 * @param {String} [method=post]
 * @param {Boolean} [useDefaultJsonParser=true]
 *
 * @return {object} ajax result
 */
function __(call, requestObject, method, useDefaultJsonParser)
{
    return DblEj.Communication.Ajax.Utils.SendRequest(call, requestObject, method, useDefaultJsonParser);
}

/**
 * @function ___ Asynchronous xhr file upload
 *
 * Send a file to the server.
 *
 * @param {String} call
 * @param {String} file
 * @param {object} [requestObject]
 * @param {function} [progressCallback]
 * @param {function} [completedCallback]
 * @returns {void}
 */
function ___(call, file, requestObject, progressCallback, completedCallback)
{
    return DblEj.Communication.Ajax.Utils.SendFile(call, file, requestObject, progressCallback, completedCallback);
}

/**
 * @function ____ Asynchronous form submit
 *
 * Send a form to the server.
 *
 * @param {String} call
 * @param {HTMLFormElement} form
 * @param {Function} callback
 * @param {Object} [requestObject]
 * @param {String} [sendToken]
 * @param {Boolean} [useDefaultJsonParser]
 * @returns {void}
 */
function ____(call, form, callback, requestObject, sendToken, useDefaultJsonParser)
{
    DblEj.Communication.Ajax.Utils.SendForm(call, form, requestObject, callback, sendToken, useDefaultJsonParser);
}

function _____(call, requestObject)
{
    return DblEj.Communication.WebSocket.AppApi.SendString(call, requestObject);
}