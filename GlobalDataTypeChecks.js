/**
 * @function IsFunction
 * Check if a variable is a function.
 * From http://stackoverflow.com/questions/5999998/how-can-i-check-if-a-javascript-variable-is-function-type
 *
 * @param {mixed} functionToCheck
 *
 * @returns {Boolean}
 */
function IsFunction(functionToCheck)
{
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

/**
 * @function IsObject
 * Check if a variable is an object
 *
 * @param {mixed} testVariable
 *
 * @returns {Boolean}
 */
function IsObject(testVariable)
{
    return typeof testVariable == "object";
}

/**
 * @function IsString
 * Check if a variable is a string
 *
 * @param {mixed} testVariable
 *
 * @returns {Boolean}
 */
function IsString(testVariable)
{
    return typeof testVariable == "string";
}


/**
 * @function IsDefined Check if a the value of an atttribute passed to a function has been defined
 *
 * @param {mixed} testVariable the variable to see if it is defined
 *
 * @returns {Boolean} boolean indicating if the passed variable has been defined
 */
function IsDefined(testVariable)
{
    return typeof testVariable !== undefined && typeof testVariable !== "undefined";
}

/**
 * @function IsNullOrUndefined Check if the passed argument is null or undefined.
 *
 * @param {mixed} testVariable
 *
 * @returns {Boolean}
 */
function IsNullOrUndefined(testVariable)
{
    return !IsDefined(testVariable) || (testVariable === null);
}

/**
 * @function IsArray
 * Check if the passed argument is a Javascript "Array"
 *
 * @param {mixed} element
 *
 * @returns {Boolean}
 */
function IsArray(element)
{
    return (Object.prototype.toString.call(element) === '[object Array]')
        || Object.prototype.toString.call(element) === '[object NodeList]'
        || Object.prototype.toString.call(element) === '[object HTMLCollection]';
}

/**
 * @function IsIteratableByNumericIndex
 * Check if element is iteratable and if the element keys are numeric (as in an Array, FileList, HTMLElementList, etc...).
 *
 * @param {HTMLElement} element
 *
 * @returns {Boolean}
 */
function IsIteratableByNumericIndex(element)
{
    return (IsArray(element))
        || Object.prototype.toString.call(element) === '[object FileList]';
}

/**
 * @function IsDomObject
 * Check if a variable is an object that can be added to the DOM
 *
 * @param {mixed} testVariable
 *
 * @returns {Boolean}
 */
function IsDomObject(testVariable)
{
    return IsNode(testVariable) || IsElement(testVariable) || testVariable == window.document || testVariable == window;
}

/**
 * @function IsNode
 * Check if variable is an HTML Node object
 *
 * @param {mixed} testVariable
 *
 * @returns {Boolean}
 */
function IsNode(testVariable) {
    return (
        typeof Node === "object" ? testVariable instanceof Node :
        testVariable && typeof testVariable === "object" && typeof testVariable.nodeType === "number" &&
        typeof testVariable.nodeName === "string"
        );
}

/**
 * @function IsElement
 * Check if variable is an HTML Element
 *
 * @param {mixed} testVariable
 *
 * @returns {Boolean}
 */
function IsElement(testVariable) {
    return (
        typeof HTMLElement === "object" ? testVariable instanceof HTMLElement : //DOM2
        testVariable && typeof testVariable === "object" && testVariable !== null && o.nodeType === 1 &&
        typeof testVariable.nodeName === "string"
        );
}

/**
 * @function IsNumeric
 * Check if the passed string can be parsed as a numeric value
 *
 * @param {String} testString The string to test
 *
 * @return {Boolean}
 */
function IsNumeric(testString) {
    return !isNaN(parseFloat(testString)) && isFinite(testString);
}