<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface IUserGroup
extends \DblEj\Resources\IActor
{

    public function AddUserToGroup(\DblEj\Users\IUserGroupUser $user);

    public function RemoveUserFromGroup(\DblEj\Users\IUserGroupUser $user);

    public function Get_Title();

    public function Set_Title($newTitle);

    public function Get_UserGroupId();

    public function Set_UserGroupId($newGroupId);

    public function GetUsers();
}