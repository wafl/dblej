<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface IUserGroupSaveableUser
extends IUserGroupUser, \DblEj\Data\IPersistantModel
{
}