<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface ISaveableUser
extends IUser, \DblEj\Data\IPersistantModel
{
}