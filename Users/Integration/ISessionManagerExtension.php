<?php

namespace DblEj\Users\Integration;

/**
 * @deprecated since revision 1630
 */
interface ISessionManagerExtension
extends ISessionManager, \DblEj\Extension\IExtension
{
}
