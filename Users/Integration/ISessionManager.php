<?php
namespace DblEj\Users\Integration;

/**
 * @deprecated since revision 1630
 */
interface ISessionManager
{
    public function OpenSession();
}