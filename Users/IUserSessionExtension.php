<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1629 in favor of Users\Integration\ISessionManagerExtension
 */
interface IUserSessionExtension
extends \DblEj\Extension\IExtension
{
    function OpenSession();
}
