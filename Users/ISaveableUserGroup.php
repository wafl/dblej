<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface ISaveableUserGroup
extends IUserGroup, \DblEj\Data\IPersistantModel
{
}