<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface ISaveableUserSession
extends IUserSession, \DblEj\Data\IPersistableModel
{
}