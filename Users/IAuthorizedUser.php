<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface IAuthorizedUser
extends IUser
{
    public function Set_AuthorizationKey($newAuthorizationKey);

    public function Logout($killSession = true);

    public function GetIsAuthorized($authorizationKey);

    public function GetIsLoggedin();

    public static function Login($username, $authorizationKey);

    public static function LoginBySession();

    public static function RegisterNewUser($username, $authorizationKey);
}