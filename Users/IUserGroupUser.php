<?php

namespace DblEj\Users;

/**
 * @deprecated since revision 1630
 */
interface IUserGroupUser
extends IUser
{
    public function Get_UserGroup();

    public function Set_UserGroup(\DblEj\Users\IUserGroup $newUserGroup);

    public function Get_UserGroupId();
}