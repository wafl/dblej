var DblEj = {};

/*
 @copyright (c) 2009 Maxime Bouroumeau-Fuseau
 @license MIT-style license.
 @version 1.1
 @class Namespace
 */
var Namespace = (function () {
    /**
     * @function _toArray Returns an object in an array unless the object is an array
     *
     * @param	mixed	obj
     * @return	Array
     */
    var _toArray = function (obj) {
        // checks if it's an array
        if (typeof (obj) == 'object' && obj.sort) {
            return obj;
        }
        return new Array(obj);
    };

    /**
     * @class _namespace
     * @constructor _namespace Creates an Object following the specified namespace identifier.
     * @public
     * @param 	String	identifier	The namespace string
     * @param	Object	klasses		(OPTIONAL) An object which properties will be added to the namespace
     * @return	Object				The most inner object
     */
    var _namespace = function (identifier) {
        var klasses = arguments[1] || false;
        var ns = window;

        if (identifier != '') {
            var parts = identifier.split(Namespace.separator);
            for (var i = 0; i < parts.length; i++) {
                if ((typeof ns[parts[i]] === undefined) || (typeof ns[parts[i]] == "undefined")) {
                    ns[parts[i]] = {};
                }
                ns = ns[parts[i]];
            }
        }

        if (klasses) {
            for (var klass in klasses) {
                ns[klass] = klasses[klass];
            }
        }

        return ns;
    };

    /**
     * @function exist Checks if the specified identifier is defined
     *
     * @public
     * @param 	String	identifier	The namespace string
     * @return	Boolean
     */
    _namespace.exist = function (identifier) {
        if (identifier == '')
            return true;

        var parts = identifier.split(Namespace.separator);
        var ns = window;
        for (var i = 0; i < parts.length; i++) {
            if (!ns[parts[i]]) {
                return false;
            }
            ns = ns[parts[i]];
        }

        return true;
    };

    /**
     * @function mapIdentifierToUri Maps an identifier to a uri. Is public so it can be overriden by custom scripts.
     *
     * @public
     * @param	String	identifier 	The namespace identifier
     * @return	String				The uri
     */
    _namespace.mapIdentifierToUri = function (identifier) {
        var regexp = new RegExp('\\' + Namespace.separator, 'g');
        return Namespace.baseUri + identifier.replace(regexp, '/') + '.js';
    };


    /**
     * @function use Imports properties from the specified namespace to the global space (ie. under window)
     *
     * The identifier string can contain the * wildcard character as its last segment (eg: com.test.*)
     * which will import all properties from the namespace.
     *
     * If not, the targeted namespace will be imported (ie. if com.test is imported, the test object
     * will now be global)
     *
     * @public
     * @param	String		identifier 	The namespace string
     */
    _namespace.use = function (identifier) {
        var identifiers = _toArray(identifier);

        for (var i = 0; i < identifiers.length; i++) {
            identifier = identifiers[i];

            var parts = identifier.split(Namespace.separator);
            var target = parts.pop();
            var ns = _namespace(parts.join(Namespace.separator));

            if (target == '*') {
                for (var objectName in ns) {
                    window[objectName] = ns[objectName];
                }
            } else {
                // imports only one object
                if (ns[target]) {
                    // the object exists, import it
                    window[target] = ns[target];
                }
            }

        }
    };

    /**
     * @function from Binds the and use() method to a specified identifier
     *
     * @public
     * @param	String	identifier 	The namespace identifier
     * @return	Object
     */
    _namespace.from = function (identifier) {
        return {
            /**
             * Includes the identifier specified in from() and unpack
             * the specified indentifier in _identifier
             *
             * @see Namespace.use()
             */
            use: function (_identifier) {
                if (_identifier.charAt(0) == '.') {
                    _identifier = identifier + _identifier;
                }
            }
        };
    };

    /**
     * @function registerNativeExtensions
     * Adds methods to javascript native's object
     * Inspired by http://thinkweb2.com/projects/prototype/namespacing-made-easy/
     *
     * @public
     */
    _namespace.registerNativeExtensions = function () {
        /**
         * @see Namespace()
         */
        String.prototype.namespace = function () {
            var klasses = arguments[0] || {};
            return _namespace(this.valueOf(), klasses);
        };
        /**
         * @see Namespace.use()
         */
        String.prototype.use = function () {
            return _namespace.use(this.valueOf());
        };
        /**
         * @see Namespace.from()
         */
        String.prototype.from = function () {
            return _namespace.from(this.valueOf());
        };
        /**
         * @see Namespace.use()
         */
        Array.prototype.use = function () {
            return _namespace.use(this);
        };
    };

    return _namespace;
})();

/**
 * @var String Namespace segment separator
 */
Namespace.separator = '.';

