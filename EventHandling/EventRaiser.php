<?php

namespace DblEj\EventHandling;

/**
 * Base class that raises events.
 * Use the <i>BeginBatch</i> and <i>FinishBatch</i> methods to raise events transactionally.
 */
abstract class EventRaiser
implements IEventRaiser
{
    private $_handlers;
    private $_inBatch;
    private $_queuedEvents = array();

    public function __construct()
    {
        $this->_handlers = new DynamicEventHandlerCollection();
    }

    /**
     * Begin a transactional batch.
     */
    public function BeginBatch()
    {
        $this->_inBatch = true;
    }

    /**
     * Finish a transactional batch.
     */
    public function FinishBatch()
    {
        $this->_inBatch = false;
        foreach ($this->_queuedEvents as $queuedEvent)
        {
            $this->raiseEvent($queuedEvent);
        }
    }

    /**
     * Add an event handler.
     *
     * @param \DblEj\EventHandling\DynamicEventHandler $handler
     * @return \DblEj\EventHandling\DynamicEventHandlerCollection <i>$this->_handlers</i> is returned to facilitate chained method calling.
     */
    final public function AddHandler(DynamicEventHandler $handler)
    {
        return $this->_handlers->AddHandlerItem($handler);
    }

    /**
     * Remove an event handler.
     *
     * @param \DblEj\EventHandling\DynamicEventHandler $handler
     * @return \DblEj\EventHandling\DynamicEventHandlerCollection <i>$this->_handlers</i> is returned to facilitate chained method calling.
     */
    final public function RemoveHandler(DynamicEventHandler $handler)
    {
        $this->_handlers->RemoveHandlerItem($handler);
        return $this;
    }

    final protected function raiseEvent(EventInfo $eventInfo, $raiseGlobally = false)
    {
        if (!$this->_inBatch)
        {
            if (!$this->_handlers)
            {
                $backtrace      = debug_backtrace();
                $lastCall       = $backtrace[0];
                $calledFromFile = $lastCall["file"];
                $calledFromLine = $lastCall["line"];

                throw new \Exception("Subclasses of EventRaiser (in this case: " . get_called_class() . ") must call the parent constructor before attempting to raise any events.  Event called at line $calledFromLine in $calledFromFile.");
            }
            $raisedEventTypes = $this->GetRaisedEventTypes();
            if (!is_a($raisedEventTypes, "\\DblEj\\EventHandling\\EventTypeCollection"))
            {
                throw new \Exception("The event raiser returned an incorrectly typed list of raised event types.  Expecting an EventTypeCollection.");
            }
            if ($raisedEventTypes->ContainsEventType($eventInfo->Get_EventType()))
            {
                $this->_handlers->HandleEvent($eventInfo);
                if ($raiseGlobally)
                {
                    \DblEj\Util\SystemEvents::RaiseSystemEvent(New EventInfo(Events::OBJECT_GLOBAL_EVENT, $this, $eventInfo->Get_PromptedBy(), $eventInfo->Get_Message()));
                }
            }
            else
            {
                throw new \Exception("The event raiser (" . get_class($this) . ") tried to raise an event (" . $eventInfo->Get_EventType() . ") that isn't in it's raised event types list");
            }
        }
        else
        {
            $this->_queuedEvents[] = $eventInfo;
        }
    }

    /**
     * Check if the passed event type is an event type raised by this EventRaiser.
     *
     * @param string|int $eventType
     * @return boolean
     */
    final public function GetIsRaisedEventType($eventType)
    {
        return $this->GetRaisedEventTypes()->ContainsEventType($eventType);
    }

    /**
     * Get a human-readable message for the given event type.
     *
     * @param string|int $eventType
     * @return string
     * @throws \DblEj\Coding\InvalidArgumentException
     */
    public function GetEventTypeString($eventType)
    {
        if (!$this->GetIsRaisedEventType($eventType))
        {
            throw new \DblEj\Coding\InvalidArgumentException($eventType, "This object does not raise events of the specified type");
        }
        $eventTypes = $this->GetRaisedEventTypes();
        return $eventTypes->GetKeyForItem($eventType);
    }
}