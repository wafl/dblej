/**
 * @namespace DblEj.EventHandling
 */
Namespace("DblEj.EventHandling");


/**
 * Information about a client-side event.
 */
DblEj.EventHandling.EventInfo = Class.extend({
    init: function (eventType, appliesToObject, promptedByObject, message)
    {
        this._eventType = eventType;
        this._message   = message;
        this._appliesTo = appliesToObject;
        this._promtedBy = promptedByObject;
    },
    Get_EventType: function()
    {
        return this._eventType;
    },
    Get_AppliesTo: function()
    {
        return this._appliesTo;
    },
    Get_PromptedBy: function()
    {
        return this._promtedBy;
    },
    Get_Message: function()
    {
        return this._message;
    }
});