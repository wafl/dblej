<?php

namespace DblEj\EventHandling;

/**
 * An interface that provides methods for adding and removing event handlers to an
 * object that raises events.  It also provides methods that allow an object to
 * notify consumers of the types of events it raises.
 */
interface IEventRaiser
{

    /**
     * Add an event handler.
     *
     * @param \DblEj\EventHandling\DynamicEventHandler $handler
     * @return \DblEj\EventHandling\DynamicEventHandlerCollection <i>$this->_handlers</i> is returned to facilitate chained method calling.
     */
    public function AddHandler(DynamicEventHandler $handler);

    /**
     * Remove an event handler.
     *
     * @param \DblEj\EventHandling\DynamicEventHandler $handler
     * @return \DblEj\EventHandling\DynamicEventHandlerCollection <i>$this->_handlers</i> is returned to facilitate chained method calling.
     */
    public function RemoveHandler(DynamicEventHandler $handler);

    /**
     * Return a collection of event types raised by instances of this class.
     */
    public function GetRaisedEventTypes();

    /**
     * Check if the passed event type is an event type raised by this EventRaiser.
     *
     * @param string|int $eventType
     * @return boolean
     */
    public function GetIsRaisedEventType($eventType);

    /**
     * Get a human-readable message for the given event type.
     *
     * @param string|int $eventType
     * @return string
     * @throws \DblEj\Coding\InvalidArgumentException
     */
    public function GetEventTypeString($eventType);
}