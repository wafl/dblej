<?php

namespace DblEj\EventHandling;

/**
 * A collection of event types.
 */
class EventTypeCollection
extends \DblEj\Collections\KeyedCollection
{

    public function AddEventType($eventType, $eventTypeName)
    {
        $return = $this->AddItem($eventType, $eventTypeName);
        return $return;
    }

    public function RemoveEventTypeByName($eventTypeName)
    {
        $return = $this->RemoveItemByKey($eventTypeName);
        return $return;
    }

    public function RemoveEventType($eventType)
    {
        $this->onEventTypeRemoved($eventType, $eventType->Get_CollectionKey());
        $return = $this->RemoveItem($eventType);
        return $return;
    }

    public function GetEventType($eventTypeName)
    {
        return $this->GetItem($eventTypeName);
    }

    public function ContainsEventType($eventType)
    {
        return $this->Contains($eventType);
    }

    public function GetMergedWith(\DblEj\Collections\IKeyedCollection $collection)
    {
        if ($collection === null)
        {
            throw new \DblEj\System\InvalidArgumentException("collection", "collection cannot be null");
        }
        $newCollection = new EventTypeCollection();
        foreach ($this->GetItems() as $key => $item)
        {
            $newCollection->AddItem($item, $key);
        }
        foreach ($collection as $key => $item)
        {
            $newCollection->AddItem($item, $key);
        }
        return $newCollection;
    }

    public function MergeWith(\DblEj\Collections\IKeyedCollection $collection)
    {
        if ($collection === null)
        {
            throw new \DblEj\System\InvalidArgumentException("collection", "collection cannot be null");
        }
        foreach ($collection as $key => $item)
        {
            $this->AddItem($item, $key);
        }
        return $this;
    }
}