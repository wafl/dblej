/**
 * @namespace DblEj.EventHandling
 */
Namespace("DblEj.EventHandling");

/**
 * @class EventHandler
 * An client-side event handler.
 * 
 * @extends Class
 */
DblEj.EventHandling.EventHandler = Class.extend({
    init: function (obj, eventName, callback, duringCapture, targetSelectorOrElement)
    {
        this._eventName = eventName;
        this._callback = callback;
        this._duringCapture = duringCapture;
        this._uid = String.CreateUUID();
        this._targetSelector = targetSelectorOrElement;
        this._targetObject = obj;
        this._nativeListener = null;
    },
    Get_Uid: function ()
    {
        return this._uid;
    },
    Get_EventName: function ()
    {
        return this._eventName;
    },
    Get_Object: function ()
    {
        return this._targetObject;
    },
    Get_Callback: function ()
    {
        return this._callback;
    },
    Get_DuringCapture: function ()
    {
        return this._duringCapture;
    },
    Get_TargetSelector: function()
    {
        return this._targetSelector;
    },
    Set_NativeListener: function(nativeListener)
    {
        this._nativeListener = nativeListener;
        return this;
    },
    Get_NativeListener: function()
    {
        return this._nativeListener;
    },
    GetTargetsFromSelector: function (parent)
    {
        var elems = new Array();
        if (IsDefined(this._targetSelector))
        {
            if (IsArray(this._targetSelector))
            {
                this._targetSelector.OnEach(
                    function (elem)
                    {
                        elems[elems.length] = elem;
                    });
            }
            else if (IsDomObject(this._targetSelector))
            {
                elems[elems.length] = this._targetSelector;
            }
            else if (this._targetSelector)
            {
                $$q(this._targetSelector, parent)
                    .OnEach(
                        function (elem)
                        {
                            elems[elems.length] = elem;
                        });
            } else {
                elems[elems.length] = parent;
            }
            return Iteratize(elems);
        } else {
            return [];
        }
    }
});