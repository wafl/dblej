<?php

namespace DblEj\EventHandling;

/**
 * Information about a server-side event.
 */
class EventInfo
{
    private $_eventType;
    private $_message;
    private $_appliesTo;
    private $_promtedBy;

    public function __construct($eventType = null, $appliesToObject = null, $promptedByObject = null, $message = null)
    {
        $this->_eventType = $eventType;
        $this->_message   = $message;
        $this->_appliesTo = $appliesToObject;
        $this->_promtedBy = $promptedByObject;
    }

    public function Get_EventType()
    {
        return $this->_eventType;
    }

    public function Get_AppliesTo()
    {
        return $this->_appliesTo;
    }

    public function Get_PromptedBy()
    {
        return $this->_promtedBy;
    }

    public function Get_Message()
    {
        return $this->_message;
    }
}