<?php

namespace DblEj\EventHandling;

/**
 * A collection of event handlers.
 * The collection itself can also handle events,
 * by passing events to all of the event handlers within it,
 * creating a chain of responsibility where each event handler
 * has a chance to handle every event.
 */
class DynamicEventHandlerCollection
extends \DblEj\Collections\Collection
{

    public function __construct(array $collectionArray = null)
    {
        parent::__construct($collectionArray);
    }

    final public function AddHandlerItem(\DblEj\EventHandling\DynamicEventHandler $handler)
    {
        $this->AddItem($handler);
        return $this;
    }

    final public function RemoveHandlerItem(\DblEj\EventHandling\DynamicEventHandler $handler)
    {
        $this->RemoveItem($handler);
        return $this;
    }

    final public function RemoveByCallback(caller $callback)
    {
        throw new \DblEj\Coding\NotYetImplementedException("DynamicEventHandlerCollection->RemoveByCallback");
    }

    final public function GetEventHandlerItems()
    {
        return $this->GetItems();
    }

    final public function HandleEvent(EventInfo $eventInfo)
    {
        foreach ($this->GetEventHandlerItems() as $handler)
        {
            $handler->HandleEvent($eventInfo);
        }
        return $this;
    }
}