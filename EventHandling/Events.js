/*
 * @namespace DblEj.EventHandling
 */
Namespace("DblEj.EventHandling");

/*
 * @class Events Utility class for raising and handling events.
 * @static
 */
DblEj.EventHandling.Events = {};
DblEj.EventHandling.Events._allHandlers = new Array();
DblEj.EventHandling.Events.GetAllHandlers = function ()
{
    return DblEj.EventHandling.Events._allHandlers;
};

/**
 * @function AddHandler Add an event handler for a specific html target and event
 *  
 * @static
 * @todo There is a bug if you add multiple handlers to the same object and each handler specifies a different targetOrSelectorEleemtn
 *		 The bug is that it will combine the target selectors for all handlers on that object.
 *		 
 * @param {Node} obj
 * @param {String} eventName
 * @param {Function} listenerFunction
 * @param {Boolean} duringCapture
 * @param {String} targetSelectorOrElement
 * @return void
 */
DblEj.EventHandling.Events.AddHandler = function (obj, eventName, callback, duringCapture, targetSelectorOrElement)
{
    if (!IsDefined(callback))
    {
        throw "AddHandler requires a valid listenerFunction";
    }
    callback = callback.Bind(obj);
    var handler = new DblEj.EventHandling.EventHandler(obj, eventName, callback, duringCapture?true:false, targetSelectorOrElement);
    DblEj.EventHandling.Events._allHandlers[DblEj.EventHandling.Events._allHandlers.length] = handler;
    DblEj.EventHandling.Events._attachEventListener(obj, eventName, handler, duringCapture?true:false);
    return handler;
};

/**
 * @function RaiseEvent Raise an event
 * @param {Object} obj
 * @param {String} eventName
 * @param {DblEj.EventHandling.EventInfo} eventInfo
 * @returns {void}
 */
DblEj.EventHandling.Events.RaiseEvent = function(obj, eventName, eventInfo)
{
    if (DblEj.Util.WebBrowser.IsNonCompliantIe())
    {
        if (!IsDefined(eventInfo))
        {
            eventInfo = document.createEventObject();
        }
        obj.fireEvent("on" + eventName, eventInfo);
    } else {
        if (!IsDefined(eventInfo))
        {
            eventInfo = document.createEvent("HTMLEvents");
        }
        eventInfo.initEvent(eventName, true, true);
        obj.dispatchEvent(eventInfo);
    }
};
DblEj.EventHandling.Events._nativeEvents = ["click", "change", "mousein", "mouseout", "keypress", "load", "remove", "focus", "blur",
                                            "mousedown", "mouseup", "mouseover", "mousemove", "keydown", "keyup", "keypress"];
                                        
DblEj.EventHandling.Events._attachEventListener = function (obj, eventName, handler, duringCapture)
{
    var eventTarget;

    if (DblEj.Util.WebBrowser.IsNonCompliantIe())
    {
        var nativeListener =
            function (event)
            {
                if (event.bubbles)
                {
                    eventTarget = event.srcElement;
                    if (!IsDefined(handler.Get_TargetSelector()) || handler.Get_TargetSelector() == null)
                    {
                        handler.Get_Callback()(event);
                    } else {
                        var targetElements = handler.GetTargetsFromSelector(obj);
                        eventTarget = event.target;
                        targetElements.OnEach(
                            function (elem)
                            {
                                if (elem === eventTarget)
                                {
                                    handler.Get_Callback()(event);
                                }
                            });
                    }
                } else {
                    handler.Get_Callback()(event);
                }
            };
        handler.Set_NativeListener(nativeListener);
        obj.attachEvent('on' + eventName, handler.Get_NativeListener());
    } else {
        var nativeListener =
            function (event)
            {
                if (event.bubbles)
                {
                    if (!IsDefined(handler.Get_TargetSelector()) || handler.Get_TargetSelector() == null)
                    {
                        handler.Get_Callback()(event);
                    } else {
                        var targetElements = handler.GetTargetsFromSelector(obj);
                        eventTarget = event.target;
                        targetElements.OnEach(
                            function (elem)
                            {
                                if (elem == eventTarget)
                                {
                                    handler.Get_Callback()(event);
                                }
                            });
                    }
                } else {
                    handler.Get_Callback()(event);
                }
            };
        handler.Set_NativeListener(nativeListener);
        obj.addEventListener(eventName, handler.Get_NativeListener(), duringCapture?true:false);
    }
};

/**
 * @function RemoveHandler Remove an event handler for a specific html target and event
 * @static
 * @param {String} uid The UID of the handler to remove.  The UID can be found on the handler object any time of it's instantiation.
 */
DblEj.EventHandling.Events.RemoveHandler = function (uid)
{
    var handler;
    var handlerToRemove;
    for (var handleridx = DblEj.EventHandling.Events._allHandlers.length - 1; handleridx >= 0; handleridx--)
    {
        handler = DblEj.EventHandling.Events._allHandlers[handleridx];
        if (handler.Get_Uid() == uid)
        {
            handlerToRemove = handler;
            break;
        }
    }
    if (IsDefined(handlerToRemove) && handlerToRemove !== null)
    {
        if (DblEj.Util.WebBrowser.IsNonCompliantIe())
        {
            handlerToRemove.Get_Object().detachEvent(handlerToRemove.Get_EventName(), handlerToRemove.Get_NativeListener());
        } else {
            handlerToRemove.Get_Object().removeEventListener(handlerToRemove.Get_EventName(), handlerToRemove.Get_NativeListener(), handlerToRemove.Get_DuringCapture()?true:false);
        }

        DblEj.EventHandling.Events._allHandlers.RemoveElement(handlerToRemove);
    }

};


DblEj.EventHandling.Events.RemoveAllHandlersFromObject = function (targetObject)
{
    var handler;
    var objHandlersRemaining = 0;
    var handlerToRemove;
    for (var handleridx = DblEj.EventHandling.Events._allHandlers.length - 1; handleridx >= 0; handleridx--)
    {
        handler = DblEj.EventHandling.Events._allHandlers[handleridx];
        if (handler.Get_Object() == targetObject)
        {
            handlerToRemove = handler;
            if (DblEj.Util.WebBrowser.IsNonCompliantIe())
            {
                handlerToRemove.Get_Object().detachEvent(handlerToRemove.Get_EventName(), handlerToRemove.Get_NativeListener());
            } else {
                handlerToRemove.Get_Object().removeEventListener(handlerToRemove.Get_EventName(), handlerToRemove.Get_NativeListener(), handlerToRemove.Get_DuringCapture()?true:false);
            }

            DblEj.EventHandling.Events._allHandlers.RemoveElement(handlerToRemove);
        }
    }

};


/**
 * @function PreventDefaultEvent Prevent the default handler from handing this event (and thus prevent the default action from occurring).
 * @static
 * @param {Object} e The javascript event to prevent the default handling of.
 * @return void
 */
DblEj.EventHandling.Events.PreventDefaultEvent = function (e)
{
    if (!IsDefined(e) || !e)
    {
        e = window.event;
    }
    if (IsDefined(e.preventDefault)) {
        e.preventDefault();
    }
    if ('returnValue' in e) {
        e.returnValue = false;
    }
};

/**
 * @function StopeEventPropagation Prevent anyone from handling this event further.
 * @static
 * @param {Object} e The javascript event to prevent any further handling of.
 * @return void
 */
DblEj.EventHandling.Events.StopeEventPropagation = function (e)
{
    if (IsDefined(e.stopPropagation)) {
        e.stopPropagation();
    }
    if ('cancelBubble' in e) {
        e.cancelBubble = true;
    }
};