<?php

namespace DblEj\EventHandling;

/**
 * Server-side event handler that can handle events raised by an <i>EventRaiser</i>.
 *
 */
final class DynamicEventHandler
{
    private $_eventType;
    private $_callback;

    public function __construct($eventType, callable $callback)
    {
        if (!$eventType || !$callback)
        {
            throw new \DblEj\System\InvalidArgumentException("EventType, Callback", "EventType and callback are both required");
        }
        $this->_eventType = $eventType;
        $this->_callback  = $callback;
    }

    /**
     *
     * @param \DblEj\EventHandling\EventInfo $eventInfo
     * @return mixed
     */
    public function HandleEvent(EventInfo $eventInfo)
    {
        if ($eventInfo->Get_EventType() == $this->_eventType)
        {
            return call_user_func($this->_callback, $eventInfo);
        }
    }
}