<?php

namespace DblEj\Resources;

/**
 * An interface that exposes methods useful for implementing objects that encapsulate multiple resources.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
interface IResourceContainer
extends \DblEj\EventHandling\IEventRaiser
{

    public function GetRestrictedResources();

    public function AddRestrictedResource(IRestrictedResource $resource);

    public function RemoveRestrictedResource(IRestrictedResource $resource);

    /**
     *
     * @param string $resourceId
     * @param string $resourceType
     *
     * @return \DblEj\Resources\IRestrictedResource Returns the resource, or null if it cannot be found
     */
    public function GetRestrictedResource($resourceId, $resourceType);
}