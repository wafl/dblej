<?php

namespace DblEj\Resources;

/**
 * A collection of ResourcePermissions
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
class ResourcePermissionCollection
extends \DblEj\Collections\TypedCollection
{

    public function __construct(array $resourcePermissionArray = null)
    {
        parent::__construct($resourcePermissionArray, "\DblEj\Resources\IResourcePermission");
    }

    /**
     *
     * @param \DblEj\Resources\IActor $actor
     * @param \DblEj\Resources\Resource $resource
     * @return \DblEj\Resources\ResourcePermission
     */
    public function GetPermission(\DblEj\Resources\IActor $actor, \DblEj\Resources\IResource $resource)
    {
        $returnPermission = null;
        /* @var $resourcePermission \DblEj\Resources\ResourcePermission */
        foreach ($this->GetItems() as $resourcePermission)
        {
            if
                (
                    (
                        (
                            $resourcePermission->Get_ResourceId() == $resource->Get_ResourceId()
                        )
                        || $resourcePermission->Get_ResourceId() == "*"
                    )
                    &&
                    (
                        (
                            $resourcePermission->Get_ResourceType() == $resource->Get_ResourceType()
                        )
                        || $resourcePermission->Get_ResourceType() == "*"
                    )
                    &&
                    (
                        (
                            $resourcePermission->Get_ActorId() == $actor->Get_ActorId()
                        )
                        || $resourcePermission->Get_ActorId() == "*"
                    )
                    &&
                    (
                        (
                            $resourcePermission->Get_ActorTypeId() == $actor->Get_ActorTypeId()
                        )
                        || $resourcePermission->Get_ActorTypeId() == "*"
                    )
                )
            {
                $returnPermission = $resourcePermission;
                break;
            }
        }
        return $returnPermission;
    }

    public function SetPermission(\DblEj\Resources\IActor $actor, \DblEj\Resources\IResource $resource, $permission = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $permission = new \DblEj\Resources\ResourcePermission($resource, $actor, $permission);
        $this->AddItem($permission);
        return $permission;
    }

    public function RemovePermission(\DblEj\Resources\IActor $actor, \DblEj\Resources\IResource $resource)
    {
        $permission = $this->GetPermission($actor, $resource);
        $this->RemoveItem($permission);
    }

    /**
     *
     * @param \DblEj\Resources\IActor $actor
     * @param \DblEj\Resources\IResource $resource
     * @param int $permissionType
     * @return \DblEj\Resources\IResourcePermission|null
     */
    public function IsActorPermitted(\DblEj\Resources\IActor $actor = null, \DblEj\Resources\IResource $resource = null, $permissionType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $permission = $this->GetPermission($actor, $resource);

        if (!$permission)
        {
            $resourcePermission = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE;
        }
        else
        {
            $resourcePermission = $permission->Get_Permission();
        }

        if ($permission && (intval($resourcePermission) & intval($permissionType)))
        {
            return $permission;
        } else {
            return null;
        }
    }
}