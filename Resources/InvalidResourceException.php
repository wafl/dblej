<?php

namespace DblEj\Resources;

/**
 * Thrown when a resource that doesn't exist or is otherwise invalid is referenced.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
class InvalidResourceException
extends \Exception
{

    public function __construct($resourceName, $reason = null, $severity = E_ERROR)
    {
        $message = "Invalid resource: $resourceName.";
        if ($reason)
        {
            $message .= "  $reason";
        }
        parent::__construct($message, $severity, null);
    }
}