<?php
namespace DblEj\Resources;

/**
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
abstract class Actor
implements IActor
{
    use ActorTrait;
}