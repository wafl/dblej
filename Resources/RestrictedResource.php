<?php
namespace DblEj\Resources;

/**
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
class RestrictedResource
extends \DblEj\Resources\Resource
implements \DblEj\Resources\IRestrictedResource
{
    public function GetPermissions(\DblEj\Resources\IActor $actor, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_MODIFY)
    {
        return [];
    }
}