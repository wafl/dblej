<?php

namespace DblEj\Resources;

/**
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
interface IResourcePermission
{
    /**
     * @return \DblEj\Resources\IResource
     */
    public function Get_Resource();

    public function Get_ResourceId();

    public function Get_ResourceType();

    public function Get_Permission();

    public function Get_Actor();

    public function Get_ActorId();

    public function Get_ActorTypeId();

    public function __toString();
}