<?php
namespace DblEj\Resources;

/**
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
trait ActorTrait
{
    public function HasAccessToResource(\DblEj\Application\IApplication $app, $resourceId, $resourceType, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ, $checkAllContexts = false)
    {
        $isAllowed = $app->IsAllowedId($resourceId, $resourceType, $this, $accessType);
        if (!$isAllowed)
        {
            foreach ($this->GetParentActors() as $parentActor)
            {
                if (is_object($parentActor))
                {
                    $isAllowed = $parentActor->HasAccessToResource($app, $resourceId, $resourceType, $accessType);
                }
                if ($isAllowed)
                {
                    break;
                }
            }
        }
        if (!$isAllowed && $checkAllContexts)
        {
            foreach ($this->GetContextualActor("All") as $contextualActor)
            {
                if ($contextualActor != $this)
                {
                    $isAllowed = $contextualActor->HasAccessToResource($app, $resourceId, $resourceType, $accessType, false);
                    if ($isAllowed)
                    {
                        break;
                    }
                }
            }
        }
        return $isAllowed;
    }

    public function HasAccessToModuleScreenSection(\DblEj\Application\IApplication $app, $screenSectionId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->HasAccessToResource($app, $screenSectionId, \DblEj\Resources\Resource::RESOURCE_TYPE_MODULE_SCREEN_SECTION, $accessType);
    }

    public function HasAccessToModuleScreen(\DblEj\Application\IApplication $app, $screenId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->HasAccessToResource($app, $screenId, \DblEj\Resources\Resource::RESOURCE_TYPE_MODULE_SCREEN, $accessType);
    }

    public function HasAccessToMethod(\DblEj\Application\IApplication $app, $methodUid, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->HasAccessToResource($app, $methodUid, \DblEj\Resources\Resource::RESOURCE_TYPE_METHOD, $accessType);
    }

    public function HasAccessToProcess(\DblEj\Application\IApplication $app, $processUid, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ, $checkAllActorContexts = false)
    {
        return $this->HasAccessToResource($app, $processUid, \DblEj\Resources\Resource::RESOURCE_TYPE_PROCESS, $accessType, $checkAllActorContexts);
    }

    public function HasAccessToModule(\DblEj\Application\IApplication $app, $moduleId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->HasAccessToResource($app, $moduleId, \DblEj\Resources\Resource::RESOURCE_TYPE_MODULE, $accessType);
    }

    public function GrantAccessToResource(\DblEj\Application\IApplication $app, $resourceId, $resourceType, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        $returnPermission = $app->IsAllowedId($resourceId, $resourceType, $this, $accessType);
        if (!$returnPermission)
        {
            $resource = $app->GetRestrictedResource($resourceId, $resourceType);
            if ($resource)
            {
                $returnPermission = $app->AddResourcePermission($this, $resource, $accessType);
            } else {
                throw new \Exception("Cannot grant access to invalid resource, $resourceType: $resourceId");
            }
        }
        return $returnPermission;
    }

    public function GrantAccessToModule(\DblEj\Application\IApplication $app, $moduleId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->GrantAccessToResource($app, $moduleId, Resource::RESOURCE_TYPE_MODULE, $accessType);
    }

    public function GrantAccessToModuleScreen(\DblEj\Application\IApplication $app, $moduleScreenId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->GrantAccessToResource($app, $moduleScreenId, Resource::RESOURCE_TYPE_MODULE_SCREEN, $accessType);
    }

    public function GrantAccessToModuleScreenSection(\DblEj\Application\IApplication $app, $moduleScreenSectionId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->GrantAccessToResource($app, $moduleScreenSectionId, Resource::RESOURCE_TYPE_MODULE_SCREEN_SECTION, $accessType);
    }

    public function GrantAccessToMethod(\DblEj\Application\IApplication $app, $methodUid, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->GrantAccessToResource($app, $methodUid, Resource::RESOURCE_TYPE_METHOD, $accessType);
    }

    public function GrantAccessToProcess(\DblEj\Application\IApplication $app, $processUid, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ)
    {
        return $this->GrantAccessToResource($app, $processUid, Resource::RESOURCE_TYPE_PROCESS, $accessType);
    }

    public function GetParentActors()
    {
        return [];
    }

    public function GetContextualActor($context)
    {
        return [$this];
    }
}