<?php

namespace DblEj\Resources;

/**
 * Provides a standard interface for Resources.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
interface IResource
extends \DblEj\Identity\IUnique
{
    public function Get_ResourceId();

    public function Get_ResourceType();

    public function Get_Title();
}