<?php

namespace DblEj\Resources;

/**
 * Encapsulates a collection of ResourcePermissions and exposes methods for checking the collection for certain permissions.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
class ResourcePermissionContainer
extends ResourceContainer
implements IResourcePermissionContainer
{
    private $_resourcePermissionsIndex;
    //Keep permissions in a static so they dont litter our instance debug dumps
    private static $_allResourcePermissions = [];
    private static function _addResourcePermissions(ResourcePermissionCollection $resourcePermissions)
    {
        $idx = count(self::$_allResourcePermissions);
        self::$_allResourcePermissions[$idx] = $resourcePermissions;
        return $idx;
    }

    function __construct(ResourceCollection $allResources, ResourcePermissionCollection $resourcePermissions)
    {
        parent::__construct($allResources);
        $this->_resourcePermissionsIndex = self::_addResourcePermissions($resourcePermissions);
    }

    public function GetResourcePermissions()
    {
        return self::$_allResourcePermissions[$this->_resourcePermissionsIndex];
    }

    public function AddResourcePermission(IActor $actor, IResource $resource, $permission = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        return $this->GetResourcePermissions()->SetPermission($actor, $resource, $permission);
    }

    public function AddResourcePermissionObject(IResourcePermission $resourcePermission)
    {
        $this->GetResourcePermissions()->AddItem($resourcePermission);
        return $this;
    }

    public function RemoveResourcePermission(IActor $actor, IResource $resource)
    {
        $this->GetResourcePermissions()->RemovePermission($actor, $resource);
        return $this;
    }

    public function IsAllowed(IResource $resource, IActor $actor = null, $permissionType = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        if ($actor)
        {
            return $this->GetResourcePermissions()->IsActorPermitted($actor, $resource, $permissionType);
        }
        else
        {
            return count($this->GetResourcePermissions()) == 0?false:reset($this->GetResourcePermissions());
        }
    }

    public function IsAllowedId($resourceId, $resourceType, IActor $actor, $permissionType = ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $resource = $this->GetRestrictedResource($resourceId, $resourceType);
        if ($resource)
        {
            return $this->GetResourcePermissions()->IsActorPermitted($actor, $resource, $permissionType);
        }
        else
        {
            return false;
        }
    }
}