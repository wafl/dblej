<?php

namespace DblEj\Resources;

use DblEj\EventHandling\EventInfo,
    DblEj\EventHandling\EventRaiser,
    DblEj\EventHandling\EventTypeCollection;

/**
 * Encapsulates a collection of Resources and fires events when Resources are added/removed to/from the collection.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
class ResourceContainer
extends EventRaiser
implements IResourceContainer
{
    const EVENT_RESOURCE_ADDED   = 1501;
    const EVENT_RESOURCE_REMOVED = 1502;

    private $_restrictedResourcesIndex;

    //Keep resources in a static so they dont litter our instance debug dumps
    private static $_allRestrictedResources = [];
    private static function _addRestrictedResources(ResourceCollection $restrictedResources)
    {
        $idx = count(self::$_allRestrictedResources);
        self::$_allRestrictedResources[$idx] = $restrictedResources;
        return $idx;
    }

    function __construct(ResourceCollection $restrictedResources)
    {
        parent::__construct();
        $this->_restrictedResourcesIndex = self::_addRestrictedResources($restrictedResources);
    }

    public function GetRestrictedResources()
    {
        return self::$_allRestrictedResources[$this->_restrictedResourcesIndex];
    }

    public function AddRestrictedResource(IRestrictedResource $resource)
    {
        $this->GetRestrictedResources()->AddResource($resource);
        $this->raiseEvent(new EventInfo(self::EVENT_RESOURCE_ADDED, $resource, $this, "The resource was added"));
        return $this;
    }

    public function RemoveRestrictedResource(IRestrictedResource $resource)
    {
        $this->GetRestrictedResources()->RemoveResource($resource);
        $this->raiseEvent(new EventInfo(self::EVENT_RESOURCE_REMOVED, $resource, $this, "The resource was removed"));
        return $this;
    }

    public function GetRestrictedResource($resourceId, $resourceType)
    {
        return $this->GetRestrictedResources()->GetResource($resourceId, $resourceType);
    }

    public function GetRaisedEventTypes()
    {
        return new EventTypeCollection([self::EVENT_RESOURCE_ADDED,
            self::EVENT_RESOURCE_REMOVED]);
    }
}