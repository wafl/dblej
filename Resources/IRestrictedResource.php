<?php

namespace DblEj\Resources;

/**
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
interface IRestrictedResource
extends IResource
{
    public function GetPermissions(IActor $actor, $accessType = ResourcePermission::RESOURCE_PERMISSION_READ);
}
