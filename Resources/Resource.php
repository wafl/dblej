<?php

namespace DblEj\Resources;

/**
 * A Resource contains information about anything that can be acted on by an Actor.
 * So, basically, anything can be treated as a Resource.
 *
 * Resources are used in DblEj mainly for access control.
 * Actors act on Resources.
 * An application can define ResourcePermisssions which will restrict
 * non-permitted Actors from ccting on a particular Resource.
 *
 * The Resource class contains a reference to the actual resource using the
 * ResourceId and ResourceType properties.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
class Resource
extends \DblEj\System\Object\BaseObject
implements IResource
{
    const RESOURCE_TYPE_GENERAL       = 0;
    const RESOURCE_TYPE_PERSON        = 11;
    const RESOURCE_TYPE_PLACE         = 12;
    const RESOURCE_TYPE_THING         = 13;
    const RESOURCE_TYPE_PEOPLE        = 14;
    const RESOURCE_TYPE_OBJECT        = 21;
    const RESOURCE_TYPE_CLASS         = 22;
    const RESOURCE_TYPE_SCRIPT        = 23;
    const RESOURCE_TYPE_APICALL       = 24;
    const RESOURCE_TYPE_METHOD        = 25;
    const RESOURCE_TYPE_PROCESS       = 26;
    const RESOURCE_TYPE_MODULE        = 31;
    const RESOURCE_TYPE_MODULE_SCREEN = 32;
    const RESOURCE_TYPE_MODULE_WIDGET = 33;
    const RESOURCE_TYPE_SITEPAGE      = 41;
    const RESOURCE_TYPE_SITEAREA      = 42;
    const RESOURCE_TYPE_MODULE_SCREEN_SECTION = 43;
    const RESOURCE_TYPE_BINARY_FILE   = 50;
    const RESOURCE_TYPE_IMAGE         = 51;
    const RESOURCE_TYPE_NULL          = 999;

    protected $_resourceId;
    protected $_resourceType;
    protected $_title;

    /**
     *
     * @param string $title
     * @param string $resourceId
     * @param integer $resourceType
     */
    public function __construct($title = null, $resourceId = null, $resourceType = self::RESOURCE_TYPE_GENERAL)
    {
        parent::__construct();
        if (!$resourceId)
        {
            $resourceId = \DblEj\Util\Crypt::GenerateUuidV4();
        }
        if (!$title)
        {
            $title = $resourceId;
        }
        $this->_resourceId   = $resourceId;
        $this->_resourceType = $resourceType;
        $this->_title        = $title;
    }

    /**
     *
     * @return string
     */
    public function Get_ResourceId()
    {
        return $this->_resourceId;
    }

    /**
     *
     * @return integer
     */
    public function Get_ResourceType()
    {
        return $this->_resourceType;
    }

    /**
     *
     * @return string
     */
    public function Get_Title()
    {
        return $this->_title;
    }

    /**
     *
     * @return string
     */
    public function Get_Id()
    {
        return $this->Get_ResourceId();
    }

    /**
     *
     * @return string
     */
    public function Get_Uid()
    {
        return "Resource_" . $this->Get_ResourceType() . $this->Get_ResourceId();
    }
}