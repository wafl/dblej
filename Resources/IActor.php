<?php

namespace DblEj\Resources;

/**
 * Provides methods to make a class capable of <i>acting</i> on a Resource.
 *
 * @deprecated since revision 1630 in favor of the DblEj\AccessControl namespace
 */
interface IActor
{
    /**
     * @property ActorId
     * @return mixed A unique (among actors of this type) id for this actor.
     */
    public function Get_ActorId();

    /**
     * @property ActorTypeId
     * @return int The resource type of this Actor (Note: actors are also resources).
     */
    public function Get_ActorTypeId();

    /**
     * The human-readable display name for this actor.
     */
    public function Get_DisplayName();

    /**
     * If this actor is part of a heirarchy that dictates how it can act, then the parent actors are specified here.
     * For example, a "user" actor might have parent "user group" that the use inherits acting behavior from
     */
    public function GetParentActors();

    /**
     * An actor might act one way within a certain context, but act differently in another context.
     * For example, a website user on a marketplace might be a buyer and a seller, and the user
     * object might act differently depending on the role.  The generic User object would not have access to
     * seller tools.  But the Seller object would.  If the User object was being checked for access, it would be denied.
     * In that instance, a User class might implement GetContextualActor and return itself when the context is "User"
     * but would return the Seller object when the context is "Seller".
     * Then the process could call GetContextualActor("Seller") and they would get the appropriate actor's behavior.
     *
     * @return IActor|IActor[] An actor or an array of actors that will act on behalf of this actor for the specified context
     */
    public function GetContextualActor($context);

    public function HasAccessToResource(\DblEj\Application\IApplication $app, $resourceId, $resourceType, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function HasAccessToModule(\DblEj\Application\IApplication $app, $moduleId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function HasAccessToModuleScreen(\DblEj\Application\IApplication $app, $moduleScreenId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function HasAccessToModuleScreenSection(\DblEj\Application\IApplication $app, $moduleScreenSectionId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function HasAccessToMethod(\DblEj\Application\IApplication $app, $methodUid, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function GrantAccessToResource(\DblEj\Application\IApplication $app, $resourceId, $resourceType, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function GrantAccessToModule(\DblEj\Application\IApplication $app, $moduleId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function GrantAccessToModuleScreen(\DblEj\Application\IApplication $app, $moduleScreenId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function GrantAccessToModuleScreenSection(\DblEj\Application\IApplication $app, $moduleScreenSectionId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);

    public function GrantAccessToMethod(\DblEj\Application\IApplication $app, $methodId, $accessType = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_READ);
}