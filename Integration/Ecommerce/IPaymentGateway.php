<?php

namespace DblEj\Integration\Ecommerce;

/**
 * A generic interface for payment processing gateways.
 *
 * @deprecated since revision 1629 in favor of DblEj\Commerce\Integration\IPaymentGateway
 * @see \DblEj\Commerce\Integration\IPaymentGateway
 */
interface IPaymentGateway
extends IPaymentSender, IPaymentReceiver
{

}