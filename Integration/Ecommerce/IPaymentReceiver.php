<?php

namespace DblEj\Integration\Ecommerce;

/**
 * @deprecated since revision 1629 in favor of DblEj\Commerce\Integration\IPaymentReceiver
 * @see \DblEj\Commerce\Integration\IPaymentReceiver
 */
interface IPaymentReceiver
extends \DblEj\Commerce\Integration\IPaymentReceiver
{
}
