<?php

namespace DblEj\Integration\Ecommerce;

/**
 * @deprecated since revision 1629 in favor of DblEj\Commerce\Integration\IPaymentSender
 * @see \DblEj\Commerce\Integration\IPaymentSender
 */
interface IPaymentSender
extends \DblEj\Commerce\Integration\IPaymentSender
{
}
