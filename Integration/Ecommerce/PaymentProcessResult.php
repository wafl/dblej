<?php

namespace DblEj\Integration\Ecommerce;

/**
 * @deprecated since revision 1629 in favor of DblEj\Commerce\PaymentProcessResult
 * @see \DblEj\Commerce\PaymentProcessResult
 */
class PaymentProcessResult
{
    private $_statusId;
    private $_responseCode;
    private $_refCode;
    private $_rawResponse;
    private $_fraudRiskLevel;
    private $_fraudRiskCode;
    private $_wasSuccessful;
    private $_statusExplanation;
    private $_customResponseData;
    private $_redirectClientUrl;

    const RISK_NONE   = 0;
    const RISK_LOW    = 100;
    const RISK_MEDIUM = 500;
    const RISK_HIGH   = 1000;

    const PAYMENTSTATUS_CLEARED = 101;
    const PAYMENTSTATUS_PENDING_ADDRESS_VERIFICATION = 201;
    const PAYMENTSTATUS_PENDING_NOTSETTLED = 202;
    const PAYMENTSTATUS_PENDING_BANKPROCESSING = 203; //such as an online check or a wire
    const PAYMENTSTATUS_PENDING_RISK_ASSESSMENT = 204;
    const PAYMENTSTATUS_PENDING_OTHER = 299;
    const PAYMENTSTATUS_FAILED_REJECTED = 801;
    const PAYMENTSTATUS_FAILED_CANCELLED = 802;
    const PAYMENTSTATUS_FAILED_SECURITY = 803;
    const PAYMENTSTATUS_FAILED_ADDRESS = 804;
    const PAYMENTSTATUS_FAILED_EXPIRATION = 805;
    const PAYMENTSTATUS_FAILED_INVALID = 806;
    const PAYMENTSTATUS_FAILED_OTHER = 899;
    const PAYMENTSTATUS_UNKNOWN = 999;

    /**
     * Create the result for the ProcessPayment method of an IPaymentGateway.
     *
     * @param string $statusid The status of this payment.  Must be one of PaymentProcessResult::PAYMENTSTATUS_* constants
     * @param string $rawResponse The raw response from the gateway.
     * @param string $refCode Gateway specific reference code.
     * @param string $responseCode Gateway specific response code.
     * @param string $fraudRiskLevel Gateway specific risk level.
     * @param string $fraudRiskCode Gateway specific code prividing details about the risk level.
     */
    public function __construct($statusid, $wasSuccessful = false, $rawResponse = "", $refCode = "", $responseCode = "", $fraudRiskLevel = null, $fraudRiskCode = null, $statusExplanation = null, $customResponseData = [], $redirectClientUrl = null)
    {
        $this->_statusId       = $statusid;
        $this->_responseCode   = $responseCode;
        $this->_refCode        = $refCode;
        $this->_rawResponse    = $rawResponse;
        $this->_wasSuccessful  = $wasSuccessful;
        $this->_fraudRiskLevel = (is_null($fraudRiskLevel) ? self::RISK_NONE : $fraudRiskLevel);
        $this->_fraudRiskCode  = (is_null($fraudRiskCode) ? '' : $fraudRiskCode);
        $this->_statusExplanation = $statusExplanation;
        $this->_customResponseData = $customResponseData;
        $this->_redirectClientUrl = $redirectClientUrl;
    }

    public function Get_RedirectClientUrl()
    {
        return $this->_redirectClientUrl;
    }

    public function Get_WasSuccessful()
    {
        return $this->_wasSuccessful;
    }

    /**
     * Gateway specific id representing the status of this payment.
     * @return string
     */
    public function Get_StatusId()
    {
        return $this->_statusId;
    }

    /**
     * Gateway provided explanation for the current status, if any.
     * @return string
     */
    public function Get_StatusExplanation()
    {
        return $this->_statusExplanation;
    }

    /**
     * Standardized payment status
     * @return string One of PaymentProcessResult::PAYMENTSTATUS_* constants
     */
    public function Get_PaymentStatus()
    {
        return $this->_statusId;
    }

    /**
     * Gateway specific response code.
     * @return string
     */
    public function Get_ResponseCode()
    {
        return $this->_responseCode;
    }

    /**
     * Gateway specific reference code.
     * @return string
     */
    public function Get_RefCode()
    {
        return $this->_refCode;
    }

    /**
     * Get the raw response from the gateway.
     * @return string
     */
    public function Get_RawResponse()
    {
        return $this->_rawResponse;
    }

    /**
     * Returns the fraud risk level, a value defined by this gateway.
     *
     * @return int|string
     */
    public function Get_FraudRiskLevel()
    {
        return $this->_fraudRiskLevel;
    }

    /**
     * Returns the fraud risk code, a value defined by this gateway.
     *
     * @return int|string
     */
    public function Get_FraudRiskCode()
    {
        return $this->_fraudRiskCode;
    }

    public function Get_CustomResponseData()
    {
        return $this->_customResponseData;
    }
}