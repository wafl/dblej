<?php

namespace DblEj\Integration\Ecommerce;

/**
 * @deprecated
 */
class PaymentResult
{
    private $_statusId;
    private $_responseCode;
    private $_refCode;
    private $_rawResponse;

    public function __construct($statusid, $rawResponse = "", $refCode = "", $responseCode = "")
    {
        $this->_statusId     = $statusid;
        $this->_responseCode = $responseCode;
        $this->_refCode      = $refCode;
        $this->_rawResponse  = $rawResponse;
    }

    public function Get_StatusId()
    {
        return $this->_statusId;
    }

    public function Get_ResponseCode()
    {
        return $this->_responseCode;
    }

    public function Get_RefCode()
    {
        return $this->_refCode;
    }

    public function Get_RawResponse()
    {
        return $this->_rawResponse;
    }
}