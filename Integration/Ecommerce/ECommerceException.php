<?php

namespace DblEj\Integration\Ecommerce;

/**
 * A generic exception for ecommerce related operations.
 *
 * @deprecated since revision 1629 in favor of DblEj\Commerce\ECommerceException
 * @see \DblEj\Commerce\ECommerceException
 */
class ECommerceException
extends \DblEj\System\Exception
{
}