<?php
namespace DblEj\Integration\Shippers;


/**
 * @deprecated since revision 1629 in favor of DblEj\Commerce\ShipmentStatus
 * @see \DblEj\Commerce\ShipmentStatus
 */
class ShipmentStatus
{
    private $_currentLocation;
    private $_expectedDeliveryDate;
    private $_deliveredDate;
    private $_sentDate;
    private $_currentStatus;
    private $_statusHistory;
    private $_notes;

    public function Get_CurrentLocation()
    {
        return $this->_currentLocation;
    }
    public function Get_ExpectedDeliveryDate()
    {
        return $this->_expectedDeliveryDate;
    }
    public function Get_DeliveredDate()
    {
        return $this->_deliveredDate;
    }
    public function Get_SentDate()
    {
        return $this->_sentDate;
    }
    public function Get_CurrentStatus()
    {
        return $this->_currentStatus;
    }
    public function Get_StatusHistory()
    {
        return $this->_statusHistory;
    }
    public function Get_Notes()
    {
        return $this->_notes;
    }

    public function Set_CurrentLocation($newValue)
    {
        $this->_currentLocation=$newValue;
    }
    public function Set_ExpectedDeliveryDate($newValue)
    {
        $this->_expectedDeliveryDate=$newValue;
    }
    public function Set_DeliveredDate($newValue)
    {
        $this->_deliveredDate=$newValue;
    }
    public function Set_SentDate($newValue)
    {
        $this->_sentDate=$newValue;
    }
    public function Set_CurrentStatus($newValue)
    {
        $this->_currentStatus=$newValue;
    }
    public function Set_StatusHistory($newValue)
    {
        $this->_statusHistory=$newValue;
    }
    public function Set_Notes($newValue)
    {
        $this->_notes=$newValue;
    }
}