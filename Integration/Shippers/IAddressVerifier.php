<?php
namespace DblEj\Integration\Shippers;

/**
 * @deprecated since revision 1629 in favor of DblEj\Commerce\Integration\IAddressVerifier
 * @see \DblEj\Commerce\Integration\IAddressVerifier
 */
interface IAddressVerifier
extends \DblEj\Commerce\Integration\IAddressVerifier
{
}