<?php
namespace DblEj\Integration\Shippers;

/**
 * @deprecated since revision 1629 in favor of DblEj\Commerce\Integration\IShipper
 * @see \DblEj\Commerce\Integration\IShipper
 */
interface IShipper
extends \DblEj\Commerce\Integration\IShipper
{
}