<?php

namespace DblEj\IO;

/**
 * @deprecated since revision 1630 in favor of DblEj\Communication
 */
interface IInput
extends \DblEj\EventHandling\IEventRaiser
{

    function __construct(IOutput $inputSignal);

    function onInput($input);
}