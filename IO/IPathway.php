<?php

namespace DblEj\IO;

/**
 * @deprecated since revision 1630 in favor of DblEj\Communication
 */
interface IPathway
extends IInput, IOutput
{

}