<?php

namespace DblEj\IO;

/**
 * @deprecated since revision 1630 in favor of DblEj\Communication
 */
interface IOutput
extends \DblEj\EventHandling\IEventRaiser
{

    function sendOutput($output);
}