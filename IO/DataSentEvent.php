<?php

namespace DblEj\IO;

/**
 * Raised by an output it sends data.
 *
 * @deprecated since revision 1630 in favor of DblEj\Communication
 */
class DataSentEvent
extends \DblEj\EventHandling\EventInfo
{
    const EVENT_DATASENT = 1410;

    private $_data;

    public function __construct($data, DataReceiver $recipient, \DblEj\IO\IOutput $sender)
    {
        parent::__construct(self::EVENT_DATASENT, $sender, $sender);
        $this->_data = $data;
    }
}