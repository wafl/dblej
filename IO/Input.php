<?php

namespace DblEj\IO;

/**
 * @deprecated since revision 1630 in favor of DblEj\Communication
 */
class Input
extends DataReceiver
implements IInput
{
    private $_inputSignal;

    public function __construct(IOutput $inputSignal)
    {
        parent::__construct();
        $this->_inputSignal = $inputSignal;
        $this->_inputSignal->AddHandler
        (
        new \DblEj\EventHandling\DynamicEventHandler(DataReceivedEvent::EVENT_DATARECEIVED, array(
            $this,
            "onInput")
        )
        );
    }

    protected function onInput($input)
    {
        $this->onDataReceived($input, $this->_inputSignal);
    }
}