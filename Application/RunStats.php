<?php

namespace DblEj\Application;

/**
 * Used to store run-time performance information about an application.
 */
class RunStats
{
    private $_app;
    private $_timeReceived;
    private $_timeResponded;
    private $_routeTaken;
    private $_exception;
    private $_response;

    /**
     * The constructor should be called as early as possible, since
     * it's timing is used as the start point for performance calculations.
     *
     * @param \DblEj\Application\IApplication $app The application to be measured.
     */
    public function __construct(IApplication $app)
    {
        $this->_timeReceived = microtime(true);
        $this->_app          = $app;
    }

    /**
     * The time that the current request was received as a unix timestamp in microseconds.
     * used as a start point for performance related calculations.
     *
     * @return integer
     */
    public function Get_TimeRequestReceived()
    {
        return $this->_timeReceived;
    }

    /**
     * Call this method as soon as the response is ready to be returned.
     * This will be used as an end-point for performance related calculations.
     *
     * @param \DblEj\Communication\IResponse $response The response.
     * @param \DblEj\Communication\IRoute $route The route that handled the request and provided the response.
     */
    public function SetResponse(\DblEj\Communication\IResponse $response = null, \DblEj\Communication\IRoute $route = null)
    {
        $this->_timeResponded = microtime(true);
        $this->_routeTaken    = $route;
        $this->_response      = $response;
    }

    /**
     * The time that the response was sent.
     * This will be used as an end-point for performance related calculations.
     *
     * @return integer
     */
    public function Get_TimeResponseSent()
    {
        return $this->_timeResponded;
    }

    /**
     * The route that was taken for this request.
     *
     * @return \DblEj\Communication\IRoute
     */
    public function Get_UsedRoute()
    {
        return $this->_routeTaken;
    }

    /**
     * The Exception (if any) that was returned by the Aplication or Router when handling this route.
     *
     * @return \Exception The exception, or null if there was no exception.
     */
    public function Get_Exception()
    {
        return $this->_exception;
    }

    /**
     * If the response was an exception, set it here.
     * This will be used as an end-point for performance related calculations.
     *
     * @param \Exception $exception The exception.
     * @param \DblEj\Communication\IResponse $response
     * @param \DblEj\Communication\IRoute $route
     */
    public function SetExceptionResponse(\Exception $exception, \DblEj\Communication\IResponse $response = null, \DblEj\Communication\IRoute $route = null)
    {
        $this->_exception = $exception;
        $this->SetResponse($response, $route);
    }
    //@todo implement some php class that defines __toArray

    /**
     * Get an array of the statistics.
     * @return array
     */
    public function GetStatsArray()
    {
        $returnArray = array(
            "StartTimestamp"  => $this->_timeReceived,
            "FinishTimestamp" => $this->_timeResponded,
            "Start"           => strftime("%Y-%m-%d  %I:%M %p", $this->_timeReceived),
            "Finish"          => strftime("%Y-%m-%d  %I:%M %p", $this->_timeResponded),
            //"Result"=>$this->_response,
            //"Exception"=>$this->_exception,
            //"RouteTaken"=>$this->_routeTaken,
            "TimeLapsed"      => $this->_timeResponded - $this->_timeReceived
        );
        return $returnArray;
    }
}