<?php

namespace DblEj\Application;

use DblEj\Communication\Http\Request as HttpRequest,
    DblEj\Communication\Request,
    DblEj\Resources\IActor,
    DblEj\Resources\ResourcePermission,
    DblEj\SiteStructure\SiteArea,
    DblEj\SiteStructure\SitePage;

/**
 * Interface for Web applications
 */
interface IWebApplication
extends IApplication
{

    public function SetGlobalTemplateData($dataname, $datavalue);

    /**
     * Get an array of skins that this application supports.
     *
     * @return \DblEj\UI\Skin[] An array of <em>Skin</em> objects.
     */
    public function Get_Skins();

    /**
     * Check if the specified Actor has the specified permission to the specified Site Area.
     *
     * @param \DblEj\Resources\IActor $actor The Actor that wants to access the Site Area
     * @param \DblEj\SiteStructure\SiteArea $siteArea The Site Area that the Actor is trying to access
     * @param type $permissions The permissions being requested by the Actor
     *
     * @return boolean TRUE if the Actor has the requested permission, otherwise FALSE.
     */
    public function IsActorAllowedAccessToSiteArea(IActor $actor, SiteArea $siteArea, $permissions = ResourcePermission::RESOURCE_PERMISSION_NONE);

    /**
     * Check if the specified Actor has the specified permission to the specified Site Page
     * @param \DblEj\SiteStructure\SitePage $sitePage The SitePage that the Actor is trying to access
     * @param \DblEj\Resources\IActor $actor The actor that wants access to the Site Page
     * @param type $permissions The permissions being requested by the Actor
     *
     * @return boolean TRUE if the Actor has the requested permission, otherwise FALSE.
     */
    public function IsActorAllowedAccessToSitePage(SitePage $sitePage, IActor $actor = null, $permissions = ResourcePermission::RESOURCE_PERMISSION_NONE);

    /**
     * Get this applications Site Map based on the Site Structure.
     *
     * @return \DblEj\SiteStructure\SiteMap The site map
     */
    public function Get_SiteMap();

    /**
     * Get a SitePage that should be used for the given request path
     *
     * @param string $requestPath
     * @param SiteArea $currentArea
     *
     * @return SitePage Get the registered SitePage, or create and register a new SitePage, that should be used for the given request path
     */
    public function GetSitePageByUri($requestPath, SiteArea $currentArea = null);

    /**
     * Get a SitePage that should be used for the given request
     *
     * @param Request $request
     * @param SiteArea $currentArea
     *
     * @return SitePage Get the registered SitePage, or create and register a new SitePage, that should be used for the given request
     */
    public function GetSitePageByRequest(HttpRequest $request, SiteArea $currentArea = null);

    /**
     * Get a SitePage that has been pre-registered with the application.
     * Use the passed in URI to find the Site Page within the Site Structure (based on the requested URL).
     *
     * @param string $requestPath The requested URI
     * @param SiteArea $currentArea Only look for Site Pages within this Site Area.
     */
    public function GetRegisteredSitePageByUri($requestPath, SiteArea $currentArea = null);

    /**
     * Get a SitePage that has been pre-registered with the application.
     * Use the passed in Http Request to find the Site Page within the Site Structure (based on the requested URL).
     *
     * @param Request $request
     * @param SiteArea $currentArea
     *
     * @return SitePage The registered SitePage, the unregistered sitepage, or null if it can not be found
     */
    public function GetRegisteredSitePageByRequest(HttpRequest $request, SiteArea $currentArea = null);

    /**
     * Get a SitePage that has been pre-registered with the application or an unregistered SitePage if it can be found.
     * @param string $uniqueId
     *
     * @return SitePage The registered SitePage, the unregistered sitepage, or null if it can not be found
     */
    public function GetSitePage($uniqueId);

    public function GetSiteArea($uniqueId);

    public function GetSitePagesInArea($areaUniqueId, $includeGrandChildren = false);

    /**
     * Get a SitePage that has been pre-registered with the application or an unregistered SitePage if it can be found.
     * @param string $pageFile
     * @param SiteArea $parentArea
     *
     * @return SitePage The registered SitePage or null if it can not be found
     */
    public function GetSitePageByFilename($pageFile, SiteArea $parentArea = null);

    /**
     * Pre-register a Site Page with the application, making the application aware of the pages existense within the Site Structure.
     *
     * @param string $title
     * The short-form name of the page
     *
     * @param string $logicFile
     * The server-side file that will handle requests for this Site Page
     *
     * @param string $templateName
     * The name of the Presentation Template that will be used when rendering the Site Page
     *
     * @param int $menuDisplayOrder
     * The ordinal for this Site Page when the Site Pages are shown as a menu choice
     *
     * @param SiteArea $parentArea
     * The Site Area that this Site Page lives beneath
     *
     * @param boolean $isAreaDefault
     * Whether or not the registered Site Page will be the default page for the specified <em>parentArea</em>.
     * If this is set to TRUE, then any requests for the Site Area directory (a URL ending with a forward-slash) will be routed to registered SitePage
     *
     * @param string $description
     * A description for this page
     *
     * @param string $fullTitle
     * The long form name of the page
     *
     * @param string $keywords
     * Keywords for this page
     *
     * @param boolean $preprocessCss
     */
    public function RegisterSitePage($title, $logicFile, $templateName, $menuDisplayOrder = 0, SiteArea $parentArea = null, $isAreaDefault = false, $description = "", $fullTitle = "", $keywords = "", $preprocessCss = false, $registeredBy = "unknown");

    /**
     * Returns true if a Site Page with the specified criteria has been pre-registered with the application.
     *
     * @param string $logicFile The name of the server-side request handler for this page.
     * @param SiteArea $parentArea The Site Area to look for the page in.
     */
    public function DoesSitePageExist($logicFile, SiteArea $parentArea = null);

    /**
     * Get the application's settings
     * @return \Wafl\Application\Settings\AllWeb
     */
    public function Get_Settings();
}