<?php
namespace DblEj\Application;

use DblEj\Mvc\IView;

/**
 * Interface for MVC Web Applications
 */
interface IMvcWebApplication
extends IWebApplication
{

    /**
     * Get a view by it's id.
     *
     * @param int|string $viewid A unique id for this view
     *
     * @return IView
     */
    public function GetView($viewid);

    /**
     * Get an array of all views.
     *
     * @return IView[]
     */
    public function GetAllViews();

    /**
     * Add a view to the internal list of views.
     *
     * @param IView $view
     */
    public function AddView(IView $view);
}