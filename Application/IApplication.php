<?php

namespace DblEj\Application;

/**
 * Base interface for all applications.
 */
interface IApplication
extends
    \DblEj\Internationalization\ITranslator,
    \DblEj\Internationalization\ILocalizable,
    \DblEj\System\IRunnable,
    \DblEj\EventHandling\IEventRaiser,
    \DblEj\Data\IStorageEngineContainer,
    \DblEj\Data\IDataStoreContainer,
    \DblEj\Resources\IResourcePermissionContainer
{

    /**
     * Application initialization.
     *
     * @param \DblEj\Authentication\IUser $user
     * @param boolean $uiPage
     * @param string $storageEngineType Fully qualified classname of a Storage Engine driver
     */
    public function Initialize(\DblEj\Authentication\IUser $user = null, $uiPage = true, $storageEngineType = "\\Wafl\\Extensions\\Storage\\Mysql");

    /**
     * Load the application's configurations
     */
    public function LoadConfig();

    /**
     * Run the application
     */
    public function Run(&$exitCode = null);

    /**
     * Get the application's settings
     * @return \Wafl\Application\Settings\All
     */
    public function Get_Settings();

    /**
     * Get a specific subset of the application's settings
     * @param string $sectionName
     */
    public function GetSettingsSection($sectionName);

    /**
     * Set a top level setting for this application.
     * @param string $settingName
     * @param mixed $settingValue
     */
    public function SetSetting($settingName, $settingValue);

    /**
     * Get the value for the specified setting.
     *
     * @param string $settingName
     */
    public function GetSettingValue($settingName);

    public function Get_LocalIncludeFolder();

    public function GetClientIncludes();

    /**
     * Get the folder that holds the logic for this application
     */
    public function Get_LocalLogicFolder();

    public static function Get_SettingsClassName();

    public function Set_ClientGmtOffset($gmtOffset);

    public function Get_ClientGmtOffset();

    public function Get_Session();

    public function GetSessionData($dataName);
    public function StoreSessionData($dataName, $dataValue);
    public function DeleteSessionData($dataName);
}