<?php

namespace DblEj\Extension;

/**
 * A screen in a modular web application.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Extension\Screen
 */
class Screen
implements IScreen
{
    private $_screenId;

    /**
     * The SitePage that this Screen represents
     * @var \DblEj\SiteStructure\SitePage
     */
    private $_sitePage;

    public function __construct(\DblEj\SiteStructure\SitePage $sitePage)
    {
        $this->_sitePage = $sitePage;
        $this->_screenId = $sitePage->Get_Id();
    }

    public function Get_ResourceId()
    {
        return $this->_screenId;
    }

    public function Get_ResourceType()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_MODULE_SCREEN;
    }

    public function Get_SitePage()
    {
        return $this->_sitePage;
    }

    public function Get_SiteArea()
    {
        return $this->_sitePage->Get_ParentArea();
    }

    public function Get_SitePageId()
    {
        return $this->_sitePage->Get_PageId();
    }

    public function Get_SiteAreaId()
    {
        $area = $this->_sitePage->Get_ParentArea();
        return $area->Get_AreaId();
    }

    public function Get_Title()
    {
        return $this->_sitePage->Get_FullTitle();
    }

    public function Get_ScreenId()
    {
        return $this->_screenId;
    }
}