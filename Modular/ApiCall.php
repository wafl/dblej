<?php

namespace DblEj\Extension;

/**
 * An API call in a modular web application.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Extension\ApiCall
 */
class ApiCall
implements IApiCall
{
    /**
     * The API Call that this ApiCall represents
     * @var string
     */
    private $_apiCallString;

    public function __construct($apiCallString)
    {
        $this->_apiCallString = $apiCallString;
    }

    public function Get_ResourceId()
    {
        return $this->_apiCallString;
    }

    public function Get_ResourceType()
    {
        return \DblEj\Resources\Resource::RESOURCE_TYPE_APICALL;
    }

    public function Get_ApiCallId()
    {
        return $this->_apiCallString;
    }

    public function Get_Title()
    {
        return $this->_apiCallString;
    }

    public function Get_Id()
    {
        return $this->_apiCallString;
    }

    public function Get_Uid()
    {
        return "API Call: ".$this->_apiCallString;
    }
}