<?php

namespace DblEj\Extension;

/**
 * Represents a widget in a modular web application.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Extension\IWidget
 */
interface IWidget
{

    public function Get_WidgetId();

    public function Get_Title();

    public function Get_View();
}