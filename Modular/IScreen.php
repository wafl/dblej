<?php

namespace DblEj\Extension;

/**
 * Represents a screen in a modular web application.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Extension\IScreen
 */
interface IScreen
{

    public function Get_ScreenId();

    public function Get_Title();

    public function Get_SitePage();
}