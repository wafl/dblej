<?php

namespace DblEj\Extension;

/**
 * Represents an API call in a modular web application.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Extension\IApiCall
 */
interface IApiCall
extends \DblEj\Resources\IResource
{

    public function Get_ApiCallId();

    public function Get_Title();
}