<?php

namespace DblEj\Extension;

/**
 * Adds methods to a ModuleCollection that will go
 * through the modules in the collection to find permissions
 * for specific Site Areas and Site Pages.
 *
 * @deprecated since revision 1630 in favor of \DblEj\Extension\ModuleChain
 */
class ModuleChain
extends ModuleCollection
{

    private $_defaultPermissionForUnregisteredSiteAreas = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE;
    private $_defaultPermissionForUnregisteredSitePages = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE;
    private $_defaultPermissionForUnregisteredApiCalls = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE;
    public function Set_DefaultPermissionForUnregisteredSiteAreas($defaultPermission)
    {
        $this->_defaultPermissionForUnregisteredSiteAreas = $defaultPermission;
        return $this;
    }
    public function Set_DefaultPermissionForUnregisteredSitePages($defaultPermission)
    {
        $this->_defaultPermissionForUnregisteredSitePages = $defaultPermission;
        return $this;
    }
    public function Set_DefaultPermissionForUnregisteredApiCalls($defaultPermission)
    {
        $this->_defaultPermissionForUnregisteredApiCalls = $defaultPermission;
        return $this;
    }

    public function IsActorPermittedToSiteArea(\DblEj\Application\IApplication $app, \DblEj\Resources\IActor $actor, \DblEj\SiteStructure\SiteArea $siteArea, \DblEj\Resources\ResourcePermissionContainer $permissionContainer, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $hasAccessToPageInArea = $this->_defaultPermissionForUnregisteredSiteAreas;
        if ($permissions)
        {
            //modules dont keep track of site areas.  So instead, we go through all of the pages in an area.
            //if the actor has access to any of the pages in the area, then they have access to the area (though only the specified pages)
            foreach ($siteArea->GetChildPages() as $sitePage)
            {
                $hasAccessToPageInArea = $this->IsActorPermittedToSitePage($app, $actor, $sitePage, $permissionContainer, $permissions);
                if ($hasAccessToPageInArea)
                {
                    break;
                }
            }
        } else {
            $hasAccessToPageInArea = new \DblEj\Resources\ResourcePermission($siteArea, $actor, $permissions);
        }
        return $hasAccessToPageInArea;
    }

    /**
     *
     * @param \DblEj\Resources\IActor|null $actor
     * @param \DblEj\SiteStructure\SitePage $sitePage
     * @param \DblEj\Resources\ResourcePermissionContainer $permissionContainer
     * @param int $permissions
     * @return boolean
     */
    public function IsActorPermittedToSitePage(\DblEj\Application\IApplication $app, \DblEj\Resources\IActor $actor, \DblEj\SiteStructure\SitePage $sitePage, \DblEj\Resources\ResourcePermissionContainer $permissionContainer, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $module      = null;
        $screen      = null;

        foreach ($this->GetModules() as $tryModule)
        {
            foreach ($tryModule->GetScreens() as $tryScreen)
            {
                $modulePage = $tryScreen->Get_SitePage();
                if ($modulePage->Get_PageId() == $sitePage->Get_PageId())
                {
                    $module = $tryModule;
                    $screen = $tryScreen;
                    break 2;
                }
            }
        }
        if ($permissions)
        {
            $modulePermission = null;
            $screenPermission = null;

            if ($module)
            {
                $modulePermission = $actor->HasAccessToModule($app, $module->Get_ModuleId(), $permissions);
                $screenPermission = $actor->HasAccessToModuleScreen($app, $screen->Get_ScreenId(), $permissions);
            }
            if ($this->_defaultPermissionForUnregisteredSitePages && !$screenPermission && !$modulePermission)
            {
                $screenPermission = $this->_defaultPermissionForUnregisteredSitePages;
            }
        } elseif ($screen) {
            $screenPermission = new \DblEj\Resources\ResourcePermission($screen, $actor, $permissions);
        } else {
            $screenPermission = $this->_defaultPermissionForUnregisteredSitePages;
        }
        return $screenPermission?$screenPermission:$modulePermission;
    }

    public function IsActorPermittedToApiCall(\DblEj\Application\IApplication $app, \DblEj\Resources\IActor $actor, $apiCall, \DblEj\Resources\ResourcePermissionContainer $permissionContainer, $permissions = \DblEj\Resources\ResourcePermission::RESOURCE_PERMISSION_NONE)
    {
        $apiPermission = null;
        $modulePermission = null;

        $module      = null;
        $foundApiCall      = null;

        foreach ($this->GetModules() as $tryModule)
        {
            foreach ($tryModule->GetApiCalls() as $tryApiCall)
            {
                if ($tryApiCall->Get_ApiCallId() == $apiCall)
                {
                    $module = $tryModule;
                    $foundApiCall = $tryApiCall;
                    break 2;
                }
            }
        }
        if ($permissions)
        {
            if ($module)
            {
                $modulePermission = $actor->HasAccessToModule($app, $module->Get_ModuleId(), $permissions);
                $apiPermission = $actor->HasAccessToResource($app, $foundApiCall->Get_ApiCallId(), \DblEj\Resources\Resource::RESOURCE_TYPE_APICALL, $permissions);
            }
            if ($this->_defaultPermissionForUnregisteredApiCalls && !$apiPermission && !$modulePermission)
            {
                $apiPermission = $this->_defaultPermissionForUnregisteredApiCalls;
            }
        } elseif ($foundApiCall) {
            $apiPermission = new \DblEj\Resources\ResourcePermission($foundApiCall, $actor, $permissions);
        } else {
            $apiPermission = $this->_defaultPermissionForUnregisteredApiCalls;
        }
        return $apiPermission?$apiPermission:$modulePermission;
    }
}