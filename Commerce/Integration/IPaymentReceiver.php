<?php
namespace DblEj\Commerce\Integration;

interface IPaymentReceiver
{
    /**
     * Process a credit card, check, or other electronic payment via a payment processor.
     *
     * @param string $accountKey        Many gateways require an additional key for application access.  That key goes here.
     * @param string $payerFirstName    The account holder's first name.
     * @param string $payerLastName     The account holder's last name.
     * @param string $payerAddress      The account holder's address.
     * @param string $payerCity         The account holder's city.
     * @param string $payerState        The account holder's state.
     * @param string $payerPostal       The account holder's postal or zip code.
     * @param string $payerCountry      The account holder's country or country code.
     * @param float  $amount            The amount of the payment.
     * @param string $paymentMethod     The method of payment (values are gateway dependent).
     * @param string $accountNumber     The card or account number.
     * @param int    $expirationMonth   The expiration month of the card or account number.
     * @param int    $expirationYear    The expiration year of the card or account number.
     * @param string $securityCode      Any of the following: CVC, CVV, or other security code.
     * @param string $currencyType      The type of currency.
     * @param string $notes             Any notes that are associated with this payment.
     * @param string $referenceCode     Any reference code, such as an invoice number, that is associated with this paymemt.
     * @param boolean $testMode         A boolean indicating that this payment is for testing purposes only.
     * @param string $recipientData Pass if recipient is different than the processor
     * @param array|null $paidForItems  An array of items that were purchased for this payment.
     * [
     *      [0] => [Sku, ItemName, Qty, PriceEach, OptionalTax, OptionalDescription],
     *      [1] => [Sku, ItemName, Qty, PriceEach, OptionalTax, OptionalDescription],
     *      ...
     * ]
     * @return \DblEj\Commerce\PaymentProcessResult The result of the payment.
     */
    public function ProcessPayment($payerFirstName, $payerLastName, $payerAddress, $payerCity, $payerState, $payerPostal, $payerCountry, $amount, $paymentMethod, $accountNumber, $expirationMonth = null, $expirationYear = null, $securityCode = null, $currencyType = null, $notes = null, $invoiceId = null, $referenceCode = null, $recipientData = null, $paidForItems = null, $shippingAmount = null, $testMode = false, &$rawSentToApi = null, $customData = []);

    /**
     * Authorize credit card, check, or other electronic payment without actually processing it.
     *
     * @param string $payerFirstName    The account holder's first name.
     * @param string $payerLastName     The account holder's last name.
     * @param string $payerAddress      The account holder's address.
     * @param string $payerCity         The account holder's city.
     * @param string $payerState        The account holder's state.
     * @param string $payerPostal       The account holder's postal or zip code.
     * @param string $payerCountry      The account holder's country or country code.
     * @param float $amount            The amount of the payment.
     * @param string $paymentMethod     The method of payment (values are gateway dependent).
     * @param string $accountNumber     The card or account number.
     * @param int    $expirationMonth   The expiration month of the card or account number.
     * @param int    $expirationYear    The expiration year of the card or account number.
     * @param string $securityCode      Any of the following: CVC, CVV, or other security code.
     * @param string $currencyType      The type of currency.
     * @param string $notes             Any notes that are associated with this payment.
     * @param string $referenceCode     Any reference code, such as an invoice number, that is associated with this paymemt.
     * @param boolean $testMode         A boolean indicating that this payment is for testing purposes only.
     *
     * @return string
     */
    public function AuthorizePayment($payerFirstName, $payerLastName, $payerAddress, $payerCity, $payerState, $payerPostal, $payerCountry, $amount, $paymentMethod, $accountNumber, $expirationMonth = null, $expirationYear = null, $securityCode = null, $currencyType = null, $notes = null, $referenceCode = null, $testMode = false, $recipientData = null);

    /**
     * Capture a previously authorized payment.
     *
     * @param float $amount
     * @param string $authorizationId
     * @param string $notes
     * @param string $referenceCode
     * @param boolean $testMode
     */
    public function CapturePayment($amount, $authorizationId, $notes = null, $referenceCode = null, $testMode = false);

    /**
     * Void a previously authorized payment.
     *
     * @param float $amount
     * @param string $authorizationId
     * @param string $notes
     * @param string $referenceCode
     * @param boolean $testMode
     */
    public function VoidPayment($amount, $authorizationId, $notes = null, $referenceCode = null, $testMode = false);

    /**
     * Refund part or all of an already processed payment.
     *
     * @param string $authorizationId
     * @param float $amount
     * @param boolean $partialRefund
     * @param string $notes
     * @param string $referenceCode
     * @param boolean $testMode
     */
    public function RefundPayment($authorizationId, $amount, $partialRefund = false, $notes = null, $referenceCode = null, $testMode = false);

    public function SavePaymentCard($userId, $firstName, $lastName, $cardType, $cardNumber, $expMonth, $expYear, $cvv2 = null, $streetAddress = null, $city = null, $state = null, $zip = null, $country = null, $testMode = false, $payerEmailAddress = null, $customData = [], $institutionNumber = null);

    public function ProcessSavedCardPayment($cardKey, $amount, $description = "", $payerId = null, $currencyType = "USD", $invoiceId = null, $paidForItems = null, $shippingAmount = null, $recipientData = null, $testMode = false, $buyerEmail = null, $buyerPhone = null, &$rawSentToApi = null, $customData = []);

    /**
     *
     * @param type $criteria
     */
    public function GetFraudRiskLevel($criteria = null);

    /**
     *
     * @param type $criteria
     */
    public function GetFraudRiskCode($criteria = null);

    public function Configure($settingName, $settingValue);

    public function SupportsSavedCards();

    public function Get_RefundTimeLimit();

    public function RequiresPayerAuthorization($cardIsSaved = false);
}
