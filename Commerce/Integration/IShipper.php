<?php
namespace DblEj\Commerce\Integration;

interface IShipper
{
    /**
     * @param string $trackingid
     * @return ShipmentStatus
     */
    public function GetShipmentStatus($carrierName, $trackingid);

    /**
     *
     * @param string $service
     * @param string $sourceAddress
     * @param string $sourceCity
     * @param string $sourceStateOrRegion
     * @param string $sourceCountry
     * @param string $sourcePostalCode
     * @param string $destAddress
     * @param string $destCity
     * @param string $destStateOrRegion
     * @param string $destCountry
     * @param string $destPostalCode
     * @param string $packageType
     * @param string $packageQualifier
     * @param int $weight Weight in ounces
     * @param int $packageWidth Width in inches
     * @param int $packageHeight Height in inches
     * @param int $packageLength Length in inches
     * @param int $packageGirth Girth in inches
     * @param decimal $valueOfContents
     * @param bool $tracking
     * @param decimal $insuranceAmount
     * @param bool $signatureRequired
     * @param decimal $codAmount
     * @param string $contentsType
     */
    public function GetShippingCost(
        $service, $sourceName, $sourceCompany, $sourceAddress = null, $sourceCity = null, $sourceStateOrRegion = null, $sourceCountry = null, $sourcePostalCode = null,
        $sourcePhone = null, $sourceEmail = null, $destName = null, $destAddress = null, $destCity = null, $destStateOrRegion = null, $destCountry = null, $destPostalCode = null, $destPhone = null, $destEmail = null,
        $packageType = null, $packageQualifier = null, $weight = null, $packageWidth = null, $packageHeight = null, $packageLength = null, $packageGirth = null,
        $valueOfContents = null, $tracking = false, $insuranceAmount = null, $codAmount = null, $contentsType = null, $serviceFlags = []
    );

    public function GetCarrierNames();

    public function GetServiceNames($carrierName = null);

    public function GetPackageTypes($serviceName = null);

    public function GetPackageQualifiers($serviceName = null, $packageType = null);

    public function GetServiceOptions($serviceName = null, $packageType = null, $packageQualifier = null);

    public function SetCredentials($id, $keyOrSignature, $authCodeOrPassword);

    public function CreateShipment($service, $sourceName, $sourceCompany, $sourceAddress = null, $sourceCity = null, $sourceStateOrRegion = null, $sourceCountry = null, $sourcePostalCode = null,
        $sourcePhone = null, $sourceEmail = null, $destName = null, $destAddress = null, $destCity = null, $destStateOrRegion = null, $destCountry = null, $destPostalCode = null, $destPhone = null, $destEmail = null,
        $packageType = null, $packageQualifier = null, $weight = null, $packageWidth = null, $packageHeight = null, $packageLength = null, $packageGirth = null,
        $valueOfContents = null, $tracking = false, $insuranceAmount = null, $codAmount = null, $contentsType = null, $serviceFlags = []);

    public function GetShipmentLabels($shipmentUid);
}