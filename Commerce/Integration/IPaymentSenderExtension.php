<?php
namespace DblEj\Commerce\Integration;

interface IPaymentSenderExtension
extends IPaymentSender, \DblEj\Extension\IExtension
{
}