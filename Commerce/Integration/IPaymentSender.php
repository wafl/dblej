<?php
namespace DblEj\Commerce\Integration;

interface IPaymentSender
{
    public function ProcessPayout($payeeFirstName, $payeeLastName, $payeeAddress, $payeeCity, $payeeState, $payeePostal, $payeeCountry, $amount, $paymentMethod, $accountNumber, $institutionNumber = null, $expirationMonth = null, $expirationYear = null, $securityCode = null, $currencyType = null, $notes = null, $invoiceId = null, $referenceCode = null, $senderData = null, $paidForItems = null, $shippingAmount = null, $testMode = false, &$rawSentToApi = null, $customData = array());

    public function SavePayoutAccount($userId, $firstName, $lastName, $accountType, $accountNumber, $businessName = null, $institutionNumber = null, $expMonth = null, $expYear = null, $cvv2 = null, $streetAddress = null, $city = null, $state = null, $zip = null, $country = null, $taxId = null, $dob = null, $payeeEmailAddress = null, $customData = array(), $testMode = false);

    public function ProcessSavedAccountPayout($accountToken, $amount, $description = "", $payerId = null, $currencyType = "USD", $invoiceId = null, $paidForItems = null, $shippingAmount = null, $recipientData = null, $testMode = false, $buyerEmail = null, $buyerPhone = null, &$rawSentToApi = null, $customData = []);

    public function Configure($settingName, $settingValue);

    public function SupportsSavedPayoutAccounts();

    public function RequiresPayeeAuthorization($cardIsSaved = false);
}
