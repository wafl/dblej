<?php
namespace DblEj\Commerce\Integration;

interface ISellerAggregatorExtension
extends ISellerAggregator, \DblEj\Extension\IExtension
{
}