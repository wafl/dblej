<?php
namespace DblEj\Commerce\Integration;

interface IAddressVerifier
{
    /**
     * Check an address to see if it is correct and if it isn't, suggest corrections.
     *
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $region
     * @param string $country
     * @param string $postalCode
     * @param string $company
     *
     * @return array|int -2 = invalid address, -1 = there is no match.  0 = the address is verified exactly as is, no suggesstions. 1 = there are multiple possible matches for address, no suggesstions. Array = Suggested match.
     */
    public function VerifyAddress($address1, $address2, $city, $region, $country, $postalCode = null, $company = null);
}