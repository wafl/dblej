<?php
namespace DblEj\Commerce\Integration;

/**
 * A generic interface for payment processing gateways.
 */
interface IPaymentGateway
extends IPaymentSender, IPaymentReceiver
{

}