<?php
namespace DblEj\Commerce;

class SaveCardResult
{
    private $_referenceId;
    private $_errorTitle;
    private $_errorDetails;
    private $_rawResponse;
    private $_customResponseData;

    public function __construct($referenceId, $rawResponse, $errorTitle = "", $errorDetails = "", $customResponseData = [])
    {
        $this->_referenceId = $referenceId;
        $this->_errorDetails = $errorDetails;
        $this->_errorTitle = $errorTitle;
        $this->_rawResponse  = $rawResponse;
        $this->_customResponseData = $customResponseData;
    }
    public function Get_ReferenceId()
    {
        return $this->_referenceId;
    }
    public function Get_ErrorDetails()
    {
        return $this->_errorDetails;
    }
    public function Get_ErrorTitle()
    {
        return $this->_errorTitle;
    }
    public function Get_RawResponse()
    {
        return $this->_rawResponse;
    }
    public function Get_CustomResponseData()
    {
        return $this->_customResponseData;
    }
}