<?php
namespace DblEj\Commerce;

/**
 * A generic exception for ecommerce related operations.
 */
class ECommerceException
extends \DblEj\System\Exception
{
}