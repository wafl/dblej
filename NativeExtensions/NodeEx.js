/**
 * @namespace Default
 * @class Node
 */

var nodePrototype;
if (IsDefined(Node))
{
    nodePrototype = Node.prototype;
} else {
    nodePrototype = Element.prototype;
}

/**
 * @function ContainsDescendant
 * Check if the specified element is a descendent of this element.
 *
 * @param {Element} elem
 *
 * @returns {Boolean}
 */
nodePrototype.ContainsDescendant = function (elem)
{
    return this.contains(elem);
};

/**
 * @function IsDescendantOf
 * Check if this element is a descendent of the specified element.
 * @param {Element} elem
 * @returns {Boolean}
 */
nodePrototype.IsDescendantOf = function (elem)
{
    return elem.ContainsDescendant(this);
};

/**
 * @function GetParent
 * Get this element's parent element.
 *
 * @returns {Element} This element's parent element.
 */
nodePrototype.GetParent = function ()
{
    if (IsDefined(this.parentNode))
    {
        return this.parentNode;
    } else {
        return null;
    }
};

nodePrototype.SurroundBy = function(surroundingElement) {
    this.InsertBeforeMe(surroundingElement);
    surroundingElement.appendChild(this);
    return this;
};

/**
 * @function GetChildren
 * Get all of the element's child nodes.
 *
 * @returns {NodeList}
 */
nodePrototype.GetChildren = function () {
    return this.childNodes;
};

/**
 * @function GetChildrenCount
 * Get the number of child nodes this element contains.
 *
 * @returns {Number}
 */
nodePrototype.GetChildrenCount = function () {
    return this.childNodes.length;
};

/**
 * @function GetChildElements
 * Get all of the element's child html elements.
 *
 * @param {Boolean} makeIteratable Whether or not to Iteratize the results
 *
 * @returns {NodeList}
 */
nodePrototype.GetChildElements = function (makeIteratable) {
    if (!IsDefined(makeIteratable))
    {
        makeIteratable = false;
    }
    if (makeIteratable)
    {
        return Iteratize(IsDefined(this["children"])?this.children:this.childNodes);
    } else {
        return IsDefined(this["children"])?this.children:this.childNodes;
    }
};

/**
 * @function GetDisplayedChildElements
 * @param {Boolean} useComputedStyle
 * @returns {Array}
 */
nodePrototype.GetDisplayedChildElements = function (useComputedStyle) {
    if (!IsDefined(useComputedStyle))
    {
        useComputedStyle = true;
    }
    var childElements = this.GetChildElements();
    var childElement;
    var returnElements = [];
    for (var elemIdx = 0; elemIdx < childElements.length; elemIdx++)
    {
        childElement = childElements[elemIdx];
        if (childElement.IsShowing(useComputedStyle, false))
        {
            returnElements[returnElements.length] = childElement;
        }
    }
    return returnElements;
};

/**
 * @function GetDisplayedChildElementCount
 * @param {Boolean} useComputedStyle
 * @returns {Number}
 */
nodePrototype.GetDisplayedChildElementCount = function (useComputedStyle) {
    return this.GetDisplayedChildElements(useComputedStyle).length;
};


/**
 * @function GetChildElementCount
 * Get the number of child html elements this element contains.
 *
 * @returns {Number}
 */
nodePrototype.GetChildElementCount = function () {
    return IsDefined(this["children"])?this.children.length:this.childNodes.length;
};

/**
 * @function GetDescendants
 * Get all of the element's descendants.
 *
 * @returns {NodeList}
 */
nodePrototype.GetDescendants = function () {
    return this.getElementsByTagName('*');
};

/**
 * @function GetChildrenByClassName
 * Get all of the element's descendants (not children) that match the passed criteria. (misnamed)
 *
 * @param {String} searchClass
 * @param {String} tag
 *
 * @returns {NodeList}
 */
nodePrototype.GetChildrenByClassName = function (searchClass, tag) {
    var classElements = new Array();
    if (tag == null)
        tag = '*';
    var els = this.getElementsByTagName(tag);
    var elsLen = els.length;
    var pattern = new RegExp("(^|\\\\s)" + searchClass + "(\\\\s|$)");
    for (var i = 0, j = 0; i < elsLen; i++) {
        if (pattern.test(els[i].className)) {
            classElements[j] = els[i];
            j++;
        }
    }
    return classElements;
};

nodePrototype.GetDescendant = function(selector)
{
    return $q(selector, this);
};

nodePrototype.RemoveDescendants = function (selector)
{
    $$q(selector, this).OnEach
    (
        function(elem)
        {
            elem.Remove();
        }
    );
    return this;
};

/**
 * @function InsertBeforeMe
 * Insert an element before this element.
 *
 * @param {Element} insertElement The element to insert.
 * @returns {Element} Returns this object for chained method calls.
 */
nodePrototype.InsertBeforeMe = function (insertElement)
{
    if (insertElement != null)
    {
        if (IsArray(insertElement))
        {
            var lastElem = this;
            var elem;
            for (var elemIdx = insertElement.length - 1; elemIdx >= 0; elemIdx--)
            {
                elem = insertElement[elemIdx];
                lastElem.InsertBeforeMe(elem);
                lastElem = elem;
            }
        } else {
            this.GetParent().insertBefore(insertElement, this);
        }
    }
    return this;
};