/**
 * @namespace Default
 * @class window Additional functionality for the native window class.
 */

/**
 * @function OpenLinkWithUserConfirmation
 * @param {String} caption
 * @param {String} link
 * @returns {undefined}
 */
window.OpenLinkWithUserConfirmation = function (caption, link)
{
    if (confirm(caption)) {
        window.location.href = link;
    }
};

/**
 * @function LoadPageWithDelay
 * @param {String} pageUrl
 * @param {Number} delayMs
 * @param {Boolean} showDivElem
 * @returns {undefined}
 */
window.LoadPageWithDelay = function (pageUrl, delayMs, showDivElem)
{
    if (showDivElem) {
        showLoadingDiv(showDivElem);
    }
    setTimeout(function(){ window.LoadNewPage(pageUrl); }, delayMs);
};

/**
 * @function LoadNewPage
 * @param {String} pageUrl
 * @returns {undefined}
 */
window.LoadNewPage = function (pageUrl)
{
    window.location.href = pageUrl;
};

/**
 * @function LoadNewPageInNewWindow
 * @param {String} pageUrl
 * @returns {undefined}
 */
window.LoadNewPageInNewWindow = function (pageUrl)
{
    window.open(pageUrl);
};

/**
 * @function ShowPopup
 * @param {String} url
 * @param {String} referid
 * @param {Number} width
 * @param {Number} height
 * @returns {undefined}
 */
window.ShowPopup = function (url, referid, width, height)
{
    window.open(url, referid, "toolbar=0,scrollbars=0,location=1,statusbar=0,menubar=0,resizable=0,width=" + width + ",height=" +
        height);
};

/**
 * @function GetOffsetPoint
 * @returns {DblEj.UI.Point}
 */
window.GetOffsetPoint = function ()
{
    var x, y;
    if (IsDefined(this.pageXOffset))
    {
        x = this.pageXOffset;
        y = this.pageYOffset;
    }
    else if (IsDefined(document.body.scrollLeft))
    {
        x = document.body.scrollLeft;
        y = document.body.scrollTop;
    }
    else if (IsDefined(document.documentElement) && IsDefined(document.documentElement.scrollLeft))
    {
        x = document.documentElement.scrollLeft;
        y = document.documentElement.scrollTop;
    }
    else
    {
        x = 0;
        y = 0;
    }

    return new DblEj.UI.Point(x, y);
};

/**
 * @function GetSize;
 * @returns {DblEj.UI.Point}
 */
window.GetSize = function ()
{
    var pt = new DblEj.UI.Point(document.body.scrollWidth, document.body.scrollHeight);
    return pt;
};

/**
 * @function AddMouseMoveHandler
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {window}
 */
window.AddMouseMoveHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "mousemove", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddScrollHandler
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {window}
 */
window.AddScrollHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "scroll", handlerFunction, null, targetSelector);
    return this;
};

window.AddWheelHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "wheel", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddFocusHandler
 * Add a handler for the focus event
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {window}
 */
window.AddFocusHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "focus", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddFocusHandler
 * Add a handler for the blur event
 *
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {window}
 */
window.AddBlurHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "blur", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddPageShowHandler
 * Add a handler for the pageshow event
 *
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {window}
 */
window.AddPageShowHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "pageshow", handlerFunction, null, targetSelector);
    return this;
};

window.AddBeforeUnloadHandler = function (handlerFunction)
{
    DblEj.EventHandling.Events.AddHandler(window, "beforeunload", handlerFunction, null);
    return this;
};

/**
 * @function AddResizeHandler
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {window}
 */
window.AddResizeHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "resize", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function SurroundSelection
 * Surround the current selection with the specified element.
 *
 * @param {Element} surroundingElement The parent element that the current selection will become a child of.
 * @returns {window}
 */
window.SurroundSelection = function(surroundingElement) {
    if (window.getSelection) {
        var sel = window.getSelection();
        if (sel.rangeCount) {
            var range = sel.getRangeAt(0).cloneRange();
            surroundingElement.appendChild(range.extractContents());
            range.insertNode(surroundingElement);
        }
    }
    return this;
};

/**
 * @function GetSelectionContainer
 * Get the element that contains the current selection.
 *
 * @returns {Node}
 */
window.GetSelectionContainer = function()
{
    var returnNode = window;
    if (window.getSelection) {
        var sel = window.getSelection();
        if (sel.rangeCount) {
            var range = sel.getRangeAt(0).cloneRange();
            returnNode = range.commonAncestorContainer;
        }
    }
    return returnNode;
};

/**
 * @function GetSelectionSize
 * Get the number of characters currently selected.
 *
 * @returns {Integer}
 */
window.GetSelectionSize = function()
{
    var returnSize = 0;
    if (window.getSelection) {
        var sel = window.getSelection();
        if (sel.rangeCount) {
            var range = sel.getRangeAt(0);
            returnSize = range.endOffset - range.startOffset;
        }
    }
    return returnSize;
};

/**
 * @function SurroundSelectionHtml
 * Wrap the current selection in the element specified by the surroundingElementHtml argument.
 *
 * @param {String} surroundingElementHtml HTML that defines an element to be used as a parent container for the current selection.  Must be a complete element with open and close tags.  The current selection will be appended to this elements children.
 * @returns {window}
 */
window.SurroundSelectionHtml = function(surroundingElementHtml) {
    var dummyDiv = document.createElement("div");
    var frag = document.createDocumentFragment();
    var moveElem;
    dummyDiv.innerHTML = surroundingElementHtml;
    while (moveElem = dummyDiv.firstChild)
    {
        frag.appendChild(moveElem);
    }
    return window.SurroundSelection(frag.firstChild);
};

/**
 * @function Close
 * 
 * Close the current window
 * @returns {window}
 */
window.Close = function()
{
    window.close();
    return this;
};

window.AddScrollHandler(function(event)
{
    window.RepositionDialogsWithinScreen();
});
window.AddResizeHandler(function(event)
{
    window.RepositionDialogsWithinScreen();
});

window.RepositionDialogsWithinScreen = function()
{
    $$q(".Dialog").OnEach(function(dlg)
    {
       var dlgPos = dlg.GetAbsolutePosition(true);
       var dlgSize = dlg.GetAbsoluteSize();
       var dlgY = dlgPos.Get_Y();
       var dlgX = dlgPos.Get_X();
       var offset = window.GetOffsetPoint();
       var pageSize = DblEj.UI.Utils.GetPageSize(true);

       var dlgYCeil = Math.min(0, pageSize.Get_Y() - dlgSize.Get_Y());
       var dlgXWallLeft = Math.min(0, pageSize.Get_X() - dlgSize.Get_X());
       var dlgYFloor = pageSize.Get_Y() - dlgYCeil;
       var dlgXWallRight = pageSize.Get_X() - dlgXWallLeft;

       if (dlgY < dlgYCeil)
       {
           dlg.SetCssY(dlgYCeil + offset.Get_Y());
       }
       else if (dlgY + dlgSize.Get_Y() > dlgYFloor)
       {
           dlg.SetCssY(dlgYFloor - dlgSize.Get_Y() + offset.Get_Y());
       }
       if (dlgX < dlgXWallLeft)
       {
           dlg.SetCssX(dlgXWallLeft + offset.Get_X());
       }
       else if (dlgX + dlgSize.Get_X() > dlgXWallRight)
       {
           dlg.SetCssX(dlgXWallRight - dlgSize.Get_X() + offset.Get_X());
       }
    });
};

/**
 * @function GotoFullScreen
 * Go to full screen mode (depending onbrowser support)
 * @returns {window}
 */
window.GotoFullScreen = function()
{
    // Supports most browsers and their versions.
    var requestMethod = document.body.requestFullScreen || document.body.webkitRequestFullScreen || document.body.mozRequestFullScreen || document.body.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(document.body);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
    return this;
};

/**
 * @function ReplaceUrl
 * 
 * Replace the current URL in the browser history with the newUrl passed in and set the browser's address bar.
 * @param {String} newUrl
 * @returns {window}
 */
window.ReplaceUrl = function(newUrl)
{
    if (IsDefined(window["history"]))
    {
        window.history.replaceState(null, null, newUrl);
    }
    return this;
};

/**
 * @function PushUrl
 * 
 * Add a new URL on to the end of the browser history and set the browser's address bar.
 * @param {String} newUrl
 * @returns {window}
 */
window.PushUrl = function(newUrl)
{
    if (IsDefined(window["history"]))
    {
        window.history.pushState(null, null, newUrl);
    }
    return this;
};