/**
 * @namespace Default
 * @class Document
 */

if (!IsDefined(document.getElementsByTagName))
{
    document.getElementsByTagName = function (tagName)
    {
        return document.body.all.tags(tagName);
    };
}

if (!IsDefined(document.getElementsByClassName))
{
    document.getElementsByClassName = function (className)
    {
        return document.querySelectorAll("." + className)
    };
}

document.GetData = function(scriptElementSelector, dataTypeString)
{
    if (!IsDefined(dataTypeString))
    {
        dataTypeString = "application/json";
    }
    scriptElementSelector = scriptElementSelector.Trim() + '[type="'+dataTypeString+'"]';
    var scriptElement = $q(scriptElementSelector);
    var returnData = scriptElement.GetText();

    if (dataTypeString == "application/json")
    {
        returnData = DblEj.Communication.JsonUtil.DecodeJson(returnData);
    }
    return returnData;
};