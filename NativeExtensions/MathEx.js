/**
 * @namespace Default
 * @class Math Additional functionality for the native Math utility class.
 */

/**
 * @function GetDistance
 * @param {Float} point1
 * @param {Float} point2
 * @returns {Number}
 */
Math.GetDistance = function (point1, point2)
{
    returnDist = Math.sqrt(Math.pow(point2.Get_X() - point1.Get_X(), 2) + Math.pow(point2.Get_Y() - point1.Get_Y(), 2));
    return returnDist;
};

Math.Round = function(value, precision)
{
    var power = Math.pow(10, precision || 0);
    var rounded = Math.round(value * power) / power;
    return parseFloat(rounded.toFixed(precision));
};

/**
 * @function GetFormattedDecimal
 * @param {Float} value
 * @param {int} precision
 * @returns {String}
 */
Math.GetFormattedDecimal = function (value, precision) {
    var power = Math.pow(10, precision || 0);
    var rounded = Math.round(value * power) / power;
    return String(rounded.toFixed(precision));
};

/**
 * @method GetFraction
 * From http://stackoverflow.com/a/24637901/546833
 * 
 * @param {Float} value
 * @returns {String}
 */
Math.GetFraction = function (value)
{
    if (value == undefined || value == null || isNaN(value))
        return "";

    function _fractionFormatterHighestCommonFactor(u, v) {
        var U = u, V = v;
        while (true) {
            if (!(U %= V))
                return V;
            if (!(V %= U))
                return U;
        }
    }

    var parts = value.toString()
        .split('.');
    if (parts.length == 1)
        return parts;
    else if (parts.length == 2) {
        var wholeNum = parts[0];
        var decimal = parts[1];
        var denom = Math.pow(10, decimal.length);
        var factor = _fractionFormatterHighestCommonFactor(decimal, denom);
        return (wholeNum == '0' ? '' : (wholeNum + " ")) + (decimal / factor) + '/' + (denom / factor);
    } else {
        return "";
    }
};