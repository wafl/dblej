/**
 * @namespace Default
 * @class Date Additional functionality for the native Date class.
 */

/**
 * @function ToFormattedString
 * Return the date as a formatted string.
 * 
 * @param {String} format
 * @returns {String}
 */
Date.prototype.ToFormattedString = function (format)
{
    if (!IsDefined(format))
    {
        format = "yyyy-MM-dd ht";
    }
    var hours = this.getHours();
    var ttime = "AM";
    var monthString = "";
    if (format.indexOf("t") > -1 && hours > 12)
    {
        hours = hours - 12;
        ttime = "PM";
    }
    switch (this.getMonth())
    {
        case 0:
            monthString = "Jan";
            break;
        case 1:
            monthString = "Feb";
            break;
        case 2:
            monthString = "Mar";
            break;
        case 3:
            monthString = "Apr";
            break;
        case 4:
            monthString = "May";
            break;
        case 5:
            monthString = "Jun";
            break;
        case 6:
            monthString = "Jul";
            break;
        case 7:
            monthString = "Aug";
            break;
        case 8:
            monthString = "Sep";
            break;
        case 9:
            monthString = "Oct";
            break;
        case 10:
            monthString = "Nov";
            break;
        case 11:
            monthString = "Dec";
            break;
    }
    var o = {
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(),    //day
        "h+": hours,   //hour
        "m+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds(), //millisecond,
        "t+": ttime,
        "n+": monthString //month short string
    };

    if (/(y+)/.test(format))
        format =
            format.replace(RegExp.$1,
                (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" +
            k +
            ")").test(format))
            format = format.replace(RegExp.$1,
                RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
    return format;
};