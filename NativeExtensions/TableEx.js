/**
 * @namespace Default
 * @class HTMLTableElement Additional functionality for the native HTMLTableElement class.
 */

/**
 * @function RemoveAllRows;
 * @param {Boolean} includeHeaderRow
 * @returns {HTMLTableElement} Returns <i>this</i> for chained method calls.
 */
HTMLTableElement.prototype.RemoveAllRows = function (includeHeaderRow)
{
    if (!IsDefined(includeHeaderRow))
    {
        includeHeaderRow = true;
    }
    while (this.rows.length > (includeHeaderRow ? 0 : 1))
    {
        this.deleteRow(this.rows.length - 1);
    }
    return this;
};

/**
 * @function GetAllRows
 * @returns {HTMLTableRowElement.prototype[]}
 */
HTMLTableElement.prototype.GetAllRows = function()
{
    return $$q("tr", this);
};

/**
 * @function GetHeaderRows
 * @returns {HTMLTableRowElement.prototype[]}
 */
HTMLTableElement.prototype.GetHeaderRows = function()
{
    return $$q("thead tr", this);
};

/**
 * @function GetBodyRows
 * @returns {HTMLTableRowElement.prototype[]}
 */
HTMLTableElement.prototype.GetBodyRows = function()
{
    return $$q("tbody tr", this);
};

/**
 * @function AddRow
 * @returns {HTMLTableRowElement}
 */
HTMLTableElement.prototype.AddRow = function ()
{
    return this.insertRow(this.rows.length);
};

/**
 * @function AddColumn
 *
 * @param {String} headerCaption
 * @param {String} className
 * @returns {HTMLTableElement}
 */
HTMLTableElement.prototype.AddColumn = function (headerCaption, className)
{
    if (!IsDefined(className))
    {
        className = null;
    }
    for (var rowIdx = 0; rowIdx < this.rows.length; rowIdx++)
    {
        var row = this.rows[rowIdx];
        var newCell = row.AddCell(null, className);
        if (!IsNullOrUndefined(this.tHead) && row.IsDescendantOf(this.tHead))
        {
            newCell.SetText(headerCaption);
        }
    }
    return this;
};

/**
 * @function RemoveColumn
 * @param {Int} colIndex
 * @returns {HTMLTableElement}
 */
HTMLTableElement.prototype.RemoveColumn = function(colIndex)
{
    for (var rowIdx = 0; rowIdx < this.rows.length;rowIdx++)
    {
        var row = this.rows[rowIdx];
        row.RemoveCell(colIndex);
    }
    return this;
};