/**
 * @namespace Default
 * @class HTMLUListElement Additional functionality for the native HTMLUListElement class.
 */

/**
 * @function AddLi
 * Add an item (&lt;li&gt;) to the list.
 *
 * @param {String} liText The text of the item to add.
 * @param {boolean} prepend Whether this item should be prepended to the list instead of being appended.
 * @returns {Element} The new list item.
 */
HTMLUListElement.prototype.AddLi = function (liText, prepend)
{
    if (!IsDefined(prepend) || (prepend == null))
    {
        prepend = false;
    }
    var newLi = document.createElement("li");
    newLi.SetText(liText);
    if (prepend)
    {
        this.insertBefore(newLi, this.firstChild);
    } else {
        this.appendChild(newLi);
    }
    return newLi;
};

/**
 * @namespace Default
 * @class HTMLOListElement Additional functionality for the native HTMLOListElement class.
 */
HTMLOListElement.prototype.AddLi = function (liText, prepend)
{
    if (!IsDefined(prepend) || (prepend == null))
    {
        prepend = false;
    }
    var newLi = document.createElement("li");
    newLi.SetText(liText);
    if (prepend)
    {
        this.insertBefore(newLi, this.firstChild);
    } else {
        this.appendChild(newLi);
    }
    return newLi;
};