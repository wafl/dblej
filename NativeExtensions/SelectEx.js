/**
 * @namespace Default
 * @class HTMLSelectElement Additional functionality for the native HTMLSelectElement class.
 */

/**
 * @function AddOption
 * @param {String} caption
 * @param {String} value
 * @param {Boolean} onlyifnew
 * @param {Boolean} selected
 * @returns {Option}
 */
HTMLSelectElement.prototype.AddOption = function (caption, value, onlyifnew, selected)
{
    var returnVar = null;
    var alreadyexists = this.HasOption(value);
    if (!onlyifnew || !alreadyexists)
    {
        returnVar = new Option(caption, value);
        if (IsDefined(this["add"]))
        {
            this.add(returnVar, null);
        } else {
            this.appendChild(returnVar);
        }
        if (selected)
        {
            this.selectedIndex = returnVar.index;
        }
    }
    return returnVar;
};

HTMLSelectElement.prototype.GetAllOptions = function()
{
    return this.options;
};

/**
 * @function RemoveSelectedOption
 * @returns {HTMLSelectElement} Returns <i>this</i> for chained method calls.
 */
HTMLSelectElement.prototype.RemoveSelectedOption = function ()
{
    this.remove(this.selectedIndex);
    return this;
};

HTMLSelectElement.prototype.SelectAllText = function()
{
    return this;
};

/**
 * @function RemoveAllOptions
 * @returns {HTMLSelectElement} Returns <i>this</i> for chained method calls.
 */
HTMLSelectElement.prototype.RemoveAllOptions = function ()
{
    this.length = 0;
    return this;
};

/**
 * @function RemoveOption
 * @param {type} optionValue
 * @returns {HTMLSelectElement} Returns <i>this</i> for chained method calls.
 */
HTMLSelectElement.prototype.RemoveOption = function (optionValue)
{
    var i;
    i = this.GetOptionIndex(optionValue);
    if (i >= 0)
    {
        this.remove(i);
    }
    return this;
};

/**
 * @function SelectOptionByText
 * @param {type} textValue
 * @returns {HTMLSelectElement} Returns <i>this</i> for chained method calls.
 */
HTMLSelectElement.prototype.SelectOptionByText = function (textValue, triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }

    var option;
    for (var optionIdx = 0; optionIdx < this.children.length; optionIdx++)
    {
        option = this.children[optionIdx];
        if (option.tagName.toUpperCase() === "OPTION")
        {
            if (option.text === textValue)
            {
                this.selectedIndex = optionIdx;
                option.selected = true;
                break;
            }
        }
    }
    if (triggerChange)
    {
        this.Trigger("change");
    }
    return this;
};

HTMLSelectElement.prototype.SelectOptionByValue = function (selectValue, triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }

    var option;
    for (var optionIdx = 0; optionIdx < this.children.length; optionIdx++)
    {
        option = this.children[optionIdx];
        if (option.tagName.toUpperCase() === "OPTION")
        {
            if (option.value === selectValue)
            {
                this.selectedIndex = optionIdx;
                option.selected = true;
                break;
            }
        }
    }
    if (triggerChange)
    {
        this.Trigger("change");
    }
    return this;
};

/**
 * @function GetSelectedOption
 * @returns {Option}
 */
HTMLSelectElement.prototype.GetSelectedOption = function ()
{
    return this[this.selectedIndex];
};

/**
 * @function GetSelectedOptionIndex
 * @returns {String}
 */
HTMLSelectElement.prototype.GetSelectedOptionIndex = function ()
{
    return this.selectedIndex;
};

/**
 * @function GetSelectedOptionText
 * @returns {String}
 */
HTMLSelectElement.prototype.GetSelectedOptionText = function ()
{
    return this.selectedIndex > -1?this[this.selectedIndex].text:"";
};

HTMLSelectElement.prototype.GetSelectedOptionValue = function ()
{
    return this.selectedIndex > -1?this[this.selectedIndex].value:"";
};

/**
 * @function GetSelectOptionIndex
 * Get the index of the option with the value specified in optionValue
 *
 * @param {String} optionValue
 * @returns {Number}
 */
HTMLSelectElement.prototype.GetSelectOptionIndex = function (optionValue)
{
    var selectedIdx = -1;
    for (var optIdx = 0; optIdx < this.length; optIdx++)
    {
        if (this[optIdx].value == optionValue)
        {
            selectedIdx = optIdx;
            break;
        }
    }
    return selectedIdx;
};

/**
 * @function GetSelectOptionText
 * Get the text for the option with the value specified in optionValue
 * 
 * @param {String} optionValue
 * @returns {String}
 */
HTMLSelectElement.prototype.GetSelectOptionText = function (optionValue)
{
    if (this.GetOptionIndex(optionValue) >= 0)
    {
        return this[this.GetOptionIndex(optionValue)].text;
    } else {
        return null;
    }
};

/**
 * @function HasOption
 * @param {String} optionValue
 * @returns {Boolean}
 */
HTMLSelectElement.prototype.HasOption = function (optionValue)
{
    return this.GetOptionIndex(optionValue) != -1;
};

/**
 * @function GetOption
 * Get the option with the value specified in optionValue
 *
 * @param {type} optionValue
 * @returns {unresolved}
 */
HTMLSelectElement.prototype.GetOption = function (optionValue)
{
    if (this.HasOption(optionValue))
    {
        return this[this.GetOptionIndex(optionValue)];
    } else {
        return null;
    }
};


/**
 * @function SortByOptionValues
 * @returns {HTMLSelectElement} Returns <i>this</i> for chained method calls.
 */
HTMLSelectElement.prototype.SortByOptionValues = function () {
    var tmpAry = new Array();
    for (var i = 0; i < this.options.length; i++) {
        tmpAry[i] = new Array();
        tmpAry[i][0] = this.options[i].value;
        tmpAry[i][1] = this.options[i].text;
        tmpAry[i][2] = {};
        for (var attIdx = 0; attIdx < this.options[i].attributes.length; attIdx++)
        {
            if (this.options[i].attributes[attIdx].name != "text" && this.options[i].attributes[attIdx].name != "value")
            {
                tmpAry[i][2][this.options[i].attributes[attIdx].name] = this.options[i].attributes[attIdx].value;
            }
        }
    }
    tmpAry.sort();
    while (this.options.length > 0) {
        this.options[0] = null;
    }
    for (var i = 0; i < tmpAry.length; i++) {
        var op = new Option(tmpAry[i][1], tmpAry[i][0]);


        for (var att in tmpAry[i][2])
        {
            if (att.length > 5 && att.substring(0, 5) == "data-")
            {
                op.SetData(att.substring(5, att.length), tmpAry[i][2][att]);
            } else {
                op.SetAttribute(att, tmpAry[i][2][att]);
            }
        }

        this.options[i] = op;
    }
    return this;
};

/**
 * @function SortByOptionText
 * @returns {HTMLSelectElement} Returns <i>this</i> for chained method calls.
 */
HTMLSelectElement.prototype.SortByOptionText = function (blankValuesFirst) {
    if (!IsDefined(blankValuesFirst))
    {
        blankValuesFirst = true;
    }
    var tmpAry = new Array();
    for (var i = 0; i < this.options.length; i++) {
        tmpAry[i] = new Array();

        tmpAry[i][0] = this.options[i].text;
        tmpAry[i][1] = this.options[i].value;
        tmpAry[i][2] = {};
        for (var attIdx = 0; attIdx < this.options[i].attributes.length; attIdx++)
        {
            if (this.options[i].attributes[attIdx].name != "text" && this.options[i].attributes[attIdx].name != "value")
            {
                tmpAry[i][2][this.options[i].attributes[attIdx].name] = this.options[i].attributes[attIdx].value;
            }
        }
    }
    
    tmpAry.sort();
    while (this.options.length > 0) {
        this.options[0] = null;
    }
    var blanksFound = 0;
    var nonBlanksFound = 0;
    if (blankValuesFirst)
    {
        for (var i = 0; i < tmpAry.length; i++) {
            if (tmpAry[i][1] == "")
            {
                var op = new Option(tmpAry[i][0], tmpAry[i][1]);

                for (var att in tmpAry[i][2])
                {
                    if (att.length > 5 && att.substring(0, 5) == "data-")
                    {
                        op.SetData(att.substring(5, att.length), tmpAry[i][2][att]);
                    } else {
                        op.SetAttribute(att, tmpAry[i][2][att]);
                    }
                }

                this.options[blanksFound] = op;
                blanksFound++;
            }
        }
    }
    for (var i = 0; i < tmpAry.length; i++) {
        if (!blankValuesFirst || tmpAry[i][1] != "")
        {
            var op = new Option(tmpAry[i][0], tmpAry[i][1]);
            for (var att in tmpAry[i][2])
            {
                if (att.length > 5 && att.substring(0, 5) == "data-")
                {
                    op.SetData(att.substring(5, att.length), tmpAry[i][2][att]);
                } else {
                    op.SetAttribute(att, tmpAry[i][2][att]);
                }
            }


            this.options[nonBlanksFound+blanksFound] = op;
            nonBlanksFound++;
        }
    }
    return this;
};

/**
 * @function Set_Value
 * @param {String|Number} newValue
 * @param {Boolean} triggerChange
 *
 * @returns {HTMLSelectElement} Returns this element for chained method calls.
 */
HTMLSelectElement.prototype.Set_Value = function (newValue, triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }

    if (this.value != newValue)
    {
        this.value = newValue;
        if (triggerChange)
        {
            this.Trigger("change");
        }
    }
    return this;
};

/**
 * @function Get_Value
 * Get the current value for this input element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLSelectElement.prototype.Get_Value = function ()
{
    return this.value;
};

/**
 * @function GetValue
 * Get the current value for this input element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLSelectElement.prototype.GetValue = function ()
{
    return this.Get_Value();
};

/**
 * @function GetValueOrNull
 * Get the current value for this input element, or null if the value is empty.
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLSelectElement.prototype.GetValueOrNull = function ()
{
    return this.value != "" ? this.value : null;
};

/**
 * @function SetValidationError
 *
 * @param {String} errorString
 * @returns {HTMLSelectElement} Returns this element for chained method calls.
 */
HTMLSelectElement.prototype.SetValidationError = function (errorString, showIndicator)
{
    this.setCustomValidity(errorString);
    var safari = (navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) > -1 && ((navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) != "");
    if (safari && showIndicator)
    {
        var messageElem = $q(".ValidationErrorMessage");
        if (messageElem == null)
        {
            messageElem = $create("<div class='ValidationErrorMessage'>"+errorString+"</div>");
            messageElem
                .SetCss("position", "absolute")
                .SetCss("padding", "1em")
                .SetCss("background-color", "#ffffff")
                .SetCss("opacity", ".9")
                .SetCss("border-style", "solid")
                .SetCss("border-width", "1px")
                .SetCss("border-color", "#afafaf")
                .SetCss("font-size", "70%")
                .SetCss("z-index", "99999");

            if (this.GetParent().GetCss("position", true) == "relative" || this.GetParent().GetCss("position", true) == "absolute")
            {
                messageElem
                    .SetCss("top", (this.GetRelativePosition().Get_Y() + this.GetAbsoluteSize().Get_Y() + 2)+"px")
                    .SetCss("left", this.GetRelativePosition().Get_X()+"px")
            } else {
                messageElem
                    .SetCss("top", (this.GetAbsolutePosition().Get_Y() + this.GetAbsoluteSize().Get_Y() + 2)+"px")
                    .SetCss("left", this.GetAbsolutePosition().Get_X()+"px")
            }

            if (!IsDefined(this._hasValidationFocusHandler) || !this._hasValidationFocusHandler)
            {
                this._hasValidationFocusHandler= true;
                this.AddFocusHandler(function(event)
                {
                    var messageElem = $q(".ValidationErrorMessage", this.GetParent());
                    if (messageElem != null)
                    {
                        messageElem.Hide();
                    }
                });
            }
        }
        if (errorString != "")
        {
            this.GetParent().AppendChild(messageElem);
            messageElem.Show();
        }
    }
};

/**
 * @function ClearValidationError
 *
 * @returns {HTMLSelectElement} Returns this element for chained method calls.
 */
HTMLSelectElement.prototype.ClearValidationError = function ()
{
    this.setCustomValidity("");
    var safari = (navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) > -1 && ((navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) != "");
    if (safari)
    {
        var messageElem = $q(".ValidationErrorMessage", this.GetParent());
        if (messageElem != null)
        {
            messageElem.Hide();
        }
    }
};

/**
 * @function IsValid
 *
 * @returns {Boolean}
 */
HTMLSelectElement.prototype.IsValid = function ()
{
    return this.validity.valid;
};

HTMLSelectElement.prototype.GetOptionIndex = HTMLSelectElement.prototype.GetSelectOptionIndex;
HTMLSelectElement.prototype.GetOptionText = HTMLSelectElement.prototype.GetSelectOptionText;

/**
 * @namespace Default
 * @class HTMLOptionElement Additional functionality for the native HTMLOptionElement class.
 */

/**
 * @function Set_Value
 * Set the current value for this option element
 * @param {String|Number|null} newValue
 * @param {Boolean} triggerChange
 * @returns {HTMLOptionElement} Returns this element for chained method calls.
 */
HTMLOptionElement.prototype.Set_Value = function (newValue, triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }
    if (this.type == "checkbox")
    {
        if (this.checked != newValue)
        {
            this.checked = newValue;
            if (this.checked)
            {
                this.SetAttribute("checked", true);
            } else {
                this.RemoveAttribute("checked");
            }
            if (triggerChange)
            {
                this.Trigger("change");
            }
        }
    } else {
        if (this.value != newValue)
        {
            this.value = newValue;
            this.SetAttribute("value", newValue);
            if (triggerChange)
            {
                this.Trigger("change");
            }
        }
    }
    return this;
};

/**
 * @function SetValue
 * Set the current value for this option element
 * @param {String|Number|null} newValue
 * @param {Boolean} triggerChange
 * @returns {HTMLOptionElement.prototype} Returns this element for chained method calls.
 */
HTMLOptionElement.prototype.SetValue = function (newValue, triggerChange)
{
    return this.Set_Value(newValue, triggerChange);
};


/**
 * @function Get_Value
 * Get the current value for this option element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLOptionElement.prototype.Get_Value = function ()
{
    if (this.type == "checkbox")
    {
        return this.checked;
    } else {
        return this.value;
    }
};

/**
 * @function GetValue
 * Get the current value for this option element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLOptionElement.prototype.GetValue = function ()
{
    return this.Get_Value();
};

/**
 * @function GetValueOrNull
 * Get the current value for this option element, or null if the value is empty.
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLOptionElement.prototype.GetValueOrNull = function ()
{
    return this.value != "" ? this.value : null;
};


if (typeof HTMLDataListElement != undefined && typeof HTMLDataListElement != 'undefined')
{
    /**
     * @namespace Default
     * @class HTMLDataListElement Additional functionality for the native HTMLDataListElement class.
    */
    HTMLDataListElement.prototype.AddOption = HTMLSelectElement.prototype.AddOption;
    HTMLDataListElement.prototype.GetAllOptions = HTMLSelectElement.prototype.GetAllOptions;
    HTMLDataListElement.prototype.HasOption = HTMLSelectElement.prototype.HasOption;
    HTMLDataListElement.prototype.RemoveAllOptions = HTMLSelectElement.prototype.RemoveAllOptions;
    HTMLDataListElement.prototype.RemoveOption = HTMLSelectElement.prototype.RemoveOption;
    HTMLDataListElement.prototype.GetOptionIndex = HTMLSelectElement.prototype.GetOptionIndex;
    HTMLDataListElement.prototype.GetOptionText = HTMLSelectElement.prototype.GetOptionText;
}