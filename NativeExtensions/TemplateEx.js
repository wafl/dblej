/**
 * @namespace Default
 * @class HTMLTemplateElement
 */


var templateClass;

if (typeof HTMLTemplateElement != undefined && typeof HTMLTemplateElement != 'undefined')
{
    templateClass = HTMLTemplateElement;
}
else if (typeof HTMLUnknownElement != undefined && typeof HTMLUnknownElement != 'undefined')
{
    templateClass = HTMLUnknownElement;
}
else
{
    templateClass = HTMLElement;
}


/**
 * @function Render
 * Render a client-side template.
 *
 * @param {Element} appendTo The element that will contain the newly created element.
 * @default document
 *
 * @param {Object} replacementVariables Token replacements when rendering the template.
 * @default []
 *
 * @param {Boolean} autoShow If the new element should be immediately shown.
 * @default true
 *
 * @param {Boolean} prepend If the rendered element(s) should be prepended instead of appended
 * @default false
 *
 * @returns {HTMLElement} The rendered element.
 */
templateClass.prototype.Render = function (appendTo, replacementVariables, autoShow, prepend)
{
    if (IsNullOrUndefined(prepend))
    {
        prepend = false;
    }
    if (IsNullOrUndefined(appendTo))
    {
        appendTo = document;
    }
    if (IsNullOrUndefined(autoShow))
    {
        autoShow = true;
    }
    if (IsNullOrUndefined(replacementVariables))
    {
        replacementVariables = [];
    }

    var clonedNode;
    var newElements = [];
    var newElement;

    var renderDepth = this.GetData("render-depth");
    if (!renderDepth)
    {
        renderDepth = 0;
    }
    if (typeof HTMLTemplateElement != undefined && typeof HTMLTemplateElement != 'undefined')
    {
        //template support
        var contentNode = this.content;
        if (renderDepth > 0)
        {
            if (contentNode.nodeType == 11)
            {
                contentNode = contentNode.GetChildElements()[0];
                while (contentNode.nodeType == 3 && IsDefined(contentNode["nodeValue"]) && (contentNode.nodeValue == " " || contentNode.nodeValue == "") && IsDefined(contentNode["nextSibling"]) && contentNode.nextSibling)
                {
                    contentNode = contentNode.nextSibling;
                }
            }
            while (renderDepth > 1)
            {
                if (contentNode.GetChildElementCount() > 0)
                {
                    contentNode = contentNode.GetChildElements()[0];
                    while (contentNode.nodeType == 3 && IsDefined(contentNode["nodeValue"]) && (contentNode.nodeValue == " " || contentNode.nodeValue == "") && IsDefined(contentNode["nextSibling"]) && contentNode.nextSibling)
                    {
                        contentNode = contentNode.nextSibling;
                    }
                    if (contentNode.nodeType == 1)
                    {
                        renderDepth--;
                    }
                } else {
                    renderDepth = 1;
                }
            }
            clonedNode = contentNode.cloneNode(true);
            while (clonedNode.childNodes.length > 0)
            {
                newElement = clonedNode.childNodes[0];
                if ((newElement.nodeType != 3) || (newElement.textContent.trim() != ""))
                {
                    appendTo.AppendChild(newElement);
                    newElements[newElements.length] = newElement;
                } else {
                    clonedNode.RemoveChild(newElement);
                }

            }
        } else {
            clonedNode = contentNode.cloneNode(true);
            for (var childIdx = clonedNode.childNodes.length-1; childIdx >= 0; childIdx--)
            {
                newElement = clonedNode.childNodes[childIdx];
                if ((newElement.nodeType != 3) || (newElement.textContent.trim() != ""))
                {
                    newElements[newElements.length] = newElement;
                } else {
                    clonedNode.removeChild(newElement);
                }
            }
            if (clonedNode.childNodes.length > 0)
            {
                appendTo.AppendChild(clonedNode);
            }
        }
    } else {
        //no template support
        var contentNode = this;
        if (renderDepth > 0)
        {
            while (renderDepth > 0)
            {
                if (contentNode.GetChildElementCount() > 0)
                {
                    contentNode = contentNode.GetChildElements()[0];
                    if (contentNode.nodeType == 1)
                    {
                        renderDepth--;
                    }
                } else {
                    renderDepth = 0;
                }
            }
        }

        for (var childIdx = 0; childIdx < contentNode.childNodes.length; childIdx++)
        {
            newElement = contentNode.childNodes[childIdx].cloneNode(true);
            if ((newElement.nodeType != 3) || (newElement.textContent.trim() != ""))
            {
                appendTo.appendChild(newElement);
                newElements[newElements.length] = newElement;
            }
        }
    }

    for (var newElemIdx = newElements.length - 1; newElemIdx >= 0; newElemIdx--)
    {
        for (var replacementVariableIdx in replacementVariables)
        {
            newElement = newElements[newElemIdx];
            if (newElement.nodeType == 1 || newElement.nodeType == 3)
            {
                newElement.StringReplaceTextNodes("[" + replacementVariableIdx + "]", replacementVariables[replacementVariableIdx]);
                newElement.StringReplaceTextNodes("_$" + replacementVariableIdx + "$_", replacementVariables[replacementVariableIdx]);
            }
            if (newElement.nodeType == 1)
            {
                newElement.StringReplaceAttributeValues("\\[" + replacementVariableIdx + "\\]", replacementVariables[replacementVariableIdx]);
                newElement.StringReplaceAttributeValues("_\\$" + replacementVariableIdx + "\\$_", replacementVariables[replacementVariableIdx]);
            }

            if (prepend)
            {
                newElement.GetParent().PrependChild(newElement);
            }
            
            if (autoShow && newElement.nodeType == 1) {
                newElement.Show();
            }
        }
    }
    return newElements.length > 1 ? newElements : newElements[0];
};
