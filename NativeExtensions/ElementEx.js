/**
 * @namespace Default
 * @class Element Additional functionality for the native Element class.
 */

/* global HTMLElement, Element */

Element._blockSelectors = [];
Element._blockTags = ["div", "body", "canvas", "iframe", "p", "iframe", "ul", "ol", "dl", "li", "header", "footer",
                      "h1", "h2", "h3", "h4", "h5", "h6", "hgroup",
                      "frame", "table", "form", "section", "aside", "main", "article", "menu"];
Element._infrequentStyles   = ["animation*", "transform*", "fill*", "flood*", "stroke*", "flex*", "writing-mode", "speak",
                               "font-stretch", "font-variant", "border-image*",
                               "background-blend-mode", "unicode*", "touch*", "vector*", "image-rendering", "isolation", "orphans", "-webkit*",
                               "dominant-baseline", "paint-order", "backface*", "text-rendering", "text-align-last",
                               "will-change", "pointer*", "object*", "page*", "motion*", "empty-cells", "cursor",
                               "border-image-width", "letter-spacing", "mix-blend-mode", "font-variant-ligatures", "shape-rendering",
                               "color-interpolation-filters", "color-interpolation", "marker*", "font-kerning", "caption-side", "tab-size",
                               "perspective*", "filter", "mask*", "shape*", "clip*", "stop*", "color*", "baseline*", "text-anchor",
                               "justify-content", "cx", "cy", "x", "y", "rx", "ry", "r", "overflow-wrap", "wiodows", "text-*"];
Element._nonSizingStyles    = ["display", "animation*", "transform*", "fill*", "stroke*", "writing-mode",
                               "image-rendering", "isolation", "orphans", "-webkit*",
                               "dominant-baseline", "paint-order", "backface*", "text-rendering", "text-align-last",
                               "will-change", "pointer*", "object*", "page*", "motion*", "empty-cells", "cursor", "mix-blend-mode",
                               "border-color", "color*", "background*", "box-shadow"];
Element._sizingStyles       = ["width", "height", "top", "bottom", "left", "right", "border*", "margin*", "padding*", "overflow",
                               "visibility", "font*", "text-overflow", "overflow", "letter-spacing", "border-box", "border-collapse", "display",
                               "line-height", "list-style", "float", "position", "clear", "clip"];

Element.prototype._validatedHandlers = new Array();
Element.prototype._hidePending = false;
Element.prototype._cachedCssRuleSelectors = null;
Element.prototype._cachedCssRuleStyles = null;
Element.prototype.OuterDialog = null;
Element.prototype.OriginalWidth = 0;
Element.prototype.OriginalHeight = 0;
Element.prototype.OriginalPaddingTop = 0;
Element.prototype.OriginalPaddingLeft = 0;
Element.prototype.OriginalPaddingRight = 0;
Element.prototype.OriginalPaddingBottom = 0;
Element.prototype.OriginalMarginTop = 0;
Element.prototype.OriginalMarginLeft = 0;
Element.prototype.OriginalMarginRight = 0;
Element.prototype.OriginalMarginBottom = 0;
Element.prototype.OriginalOverflow = null;
Element.prototype.IsClosed = null;
Element.prototype.IsOpening = false;

Element.AddBlockSelector = function(selector)
{
    if (Element._blockSelectors.indexOf(selector) == -1)
    {
        Element._blockSelectors[Element._blockSelectors.length] = selector;
    }
    return Element;
};

Element.AddBlockTag = function(blockTag)
{
    if (Element._blockTags.indexOf(blockTag) == -1)
    {
        Element._blockTags[Element._blockTags.length] = blockTag;
    }
    return Element;
};

/**
 * @function AddScrollHandler
 * Add a handler for the scroll event
 * @param {Function} handlerFunction
 * @param {String} targetSelector
 * @returns {Element}
 */
Element.prototype.AddScrollHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "scroll", handlerFunction, null, targetSelector);
    return this;
};

Element.prototype.AddWheelHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(window, "wheel", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function HasClass Check if this element has the passed class
 * Check if the element has a certain CSS Class
 * 
 * @param {String} className the name of the class to check for
 * 
 * @return {Boolean} A boolean indiciating if this element has the class
 */
Element.prototype.HasClass = function (className)
{
    if (this.className == className)
    {
        return true;
    } else {
        var classNames = this.className.split(" ");
        return (classNames.indexOf(className) > -1);
    }
};


if (!IsDefined(Element.prototype.getElementsByClassName))
{
    Element.prototype.getElementsByClassName = function (className)
    {
        return this.querySelectorAll("." + className);
    };
}

Element._highestElementZIndex = 0;

/**
 * @function BringToFront
 * Set this element's z-index higher than any other element, bringing it to the front.
 * 
 * @param {Element} elementsToStayBehindOf
 * If you want this element to remain behind a certain element (and all elements with an equal or greater z-index than it), specify an element here.
 * 
 * @returns {Number} The z-index that was assigned to this element.
 */
Element.prototype.BringToFront = function(elementsToStayBehindOf)
{
    if (!IsDefined(elementsToStayBehindOf))
    {
        if (this.HasClass("Dialog"))
        {
            elementsToStayBehindOf = [];
        } else {
            elementsToStayBehindOf = $$q(".Dialog");
        }
    }
    if (elementsToStayBehindOf === null)
    {
        elementsToStayBehindOf = [];
    }
    if (Element._highestElementZIndex == 0)
    {
        Element._highestElementZIndex = DblEj.UI.Utils.GetMaxZIndex();
    }
    var ceiling = Element._highestElementZIndex + 1;
    for (var frontElemIdx = 0; frontElemIdx < elementsToStayBehindOf.length; frontElemIdx++)
    {
        var fronElem = elementsToStayBehindOf[frontElemIdx];
        var fronZ = parseInt(fronElem.GetStyle("z-index"));
        if (!isNaN(fronZ))
        {
            ceiling = fronZ - 1;
        }
    }
    if (ceiling > Element._highestElementZIndex)
    {
        Element._highestElementZIndex = ceiling;
    }
    this.SetCss("z-index", Element._highestElementZIndex);
    return Element._highestElementZIndex;
};

/**
 * @function ReplaceHtml
 * Replace the element with the specified html.
 *
 * @param {String} html
 * @returns {Element} Returns the newly created element.
 */
Element.prototype.ReplaceHtml = function (html)
{
    var newElem = $create(html);
    if (IsDefined(newElem.length))
    {
        for (var elemIdx = 0; elemIdx < newElem.length; elemIdx++)
        {
            this.InsertAfterMe(newElem[elemIdx]);
        }
    } else {
        this.InsertAfterMe(newElem);
    }
    this.Remove();
    return newElem;
};


/**
 * @function SetInnerHtml
 * Replace the contents of the element with the specified html.
 * 
 * @param {String} html
 * @returns {Element} Returns the element for chained method calls.
 */
Element.prototype.SetInnerHtml = function (html)
{
    this.innerHTML = html;
    return this;
};

/**
 * @function GetInnerHtml
 * Get the contents of this element.
 * 
 * @returns {String}
 */
Element.prototype.GetInnerHtml = function ()
{
    return this.innerHTML;
};

/**
 * @function GetOuterHtml
 * Get the html for this element.
 * 
 * @returns {String}
 */
Element.prototype.GetOuterHtml = function ()
{
    var containerDiv = document.createElement("div");
    containerDiv.AppendChild(this.Clone());
    return containerDiv.GetInnerHtml();
};

/**
 * @function AnimateProperty
 * Animate a CSS property on this element.
 * 
 * @param {String} propertyName The name of the CSS property to animate.  This can be in css format (example-property-name) or in Javascript format (examplePropertyName).
 * @param {Float} targetNumericValue The desired ending value for the specified property, without any text suffixes (such as "px").
 * @param {String} valueNonNumericSuffix The suffix for the ending value (such as "px").
 * @param {Number} speed Units per minute The speed of the animation, in units per minute.  Higher values result in faster animations.
 * The unit is determined by the property being set and potentially the valueNonNumericSuffix.
 * Typically, if you specify a valueNonNumericSuffix, that will also be the unit used.
 * @param {Number} accelerate
 * @param {Number} decelerate
 * @param {Number} accelerateCurve
 * @param {Number} decelerateCurve
 * @param {Function} callback The function to call when the animation is completed.
 * @returns {Element} Returns the element for chained method calls.
 */
Element.prototype.AnimateProperty =
    function (propertyName, targetNumericValue, valueNonNumericSuffix, speed, accelerate, decelerate, accelerateCurve, decelerateCurve, callback, valueNonNumericPrefix, startValue)
    {
        if (!IsDefined(speed) || speed <= 0 || speed === null)
        {
            speed = 7200;
        }
        if (!IsDefined(accelerate) || accelerate < 0)
        {
            accelerate = 0;
        }
        if (!IsDefined(decelerate) || decelerate < 0)
        {
            decelerate = 0;
        }
        if (!IsDefined(accelerateCurve))
        {
            accelerateCurve = 1;
        }
        if (!IsDefined(decelerateCurve))
        {
            decelerateCurve = 1;
        }

        DblEj.UI.Effects.AddAnimation(this, propertyName, targetNumericValue, valueNonNumericSuffix, speed, accelerate, decelerate, accelerateCurve, decelerateCurve, callback, valueNonNumericPrefix, startValue);
        return this;
    };

/**
 * @function AnimateJsProperty
 * Animate a property on this element.
 * 
 * @param {String} propertyName The name of the property to animate.
 * @param {Float} targetNumericValue The desired ending value for the specified property, without any text suffixes.
 * @param {String} valueNonNumericSuffix The suffix for the ending value.
 * @param {Number} speed Pixels per minute The speed of the animation, in pixels per minute.  Higher values result in faster animations.
 * @param {Number} accelerate
 * @param {Number} decelerate
 * @param {Number} accelerateCurve
 * @param {Number} decelerateCurve
 * @param {Function} callback
 * 
 * @returns {Element} Returns the element for chained method calls.
 */
Element.prototype.AnimateJsProperty =
    function (propertyName, targetNumericValue, valueNonNumericSuffix, speed, accelerate, decelerate, accelerateCurve, decelerateCurve, callback)
    {
        if (!IsDefined(speed) || speed <= 0)
        {
            speed = 7200;
        }
        if (!IsDefined(accelerate) || accelerate < 0)
        {
            accelerate = 0;
        }
        if (!IsDefined(decelerate) || decelerate < 0)
        {
            decelerate = 0;
        }
        if (!IsDefined(accelerateCurve))
        {
            accelerateCurve = 1;
        }
        if (!IsDefined(decelerateCurve))
        {
            decelerateCurve = 1;
        }

        DblEj.UI.Effects.AddPropertyAnimation(this, propertyName, targetNumericValue, valueNonNumericSuffix, speed, accelerate, decelerate, accelerateCurve, decelerateCurve, callback);
        return this;
    };

/**
 * @function Rotate
 * 
 * @param {Number} targetRotationDegrees
 * @param {Number} currentRotationDegrees
 * @param {Function} callback
 * @param {Number} speed
 * @returns {Element} Returns this element for easy method chaining
 */
Element.prototype.Rotate = function(targetRotationDegrees, currentRotationDegrees, callback, speed)
{
    if (!IsDefined(currentRotationDegrees) || currentRotationDegrees == null)
    {
        currentRotationDegrees = 0;
    }
    if (!IsDefined(speed) || speed == null)
    {
        speed = 28800;
    }
    this.AnimateProperty("-ms-transform", targetRotationDegrees, "deg)", speed, null, null, null, null, callback, "rotate(", currentRotationDegrees);
    this.AnimateProperty("-webkit-transform", targetRotationDegrees, "deg)", speed, null, null, null, null, callback, "rotate(", currentRotationDegrees);
    this.AnimateProperty("transform", targetRotationDegrees, "deg)", speed, null, null, null, null, callback, "rotate(", currentRotationDegrees);
    return this;
};

/**
 * @function Move
 * @param {float} destinationX
 * @param {float} destinationY
 * @param {string} nonNumericSuffix
 * @param {float} speed
 * @param {function} callback
 * @param {float} accelerate
 * @param {float} decelerate
 * @param {boolean} useCssPosition
 * @returns {Element} Returns this element for easy chained calls
 */
Element.prototype.Move = function(destinationX, destinationY, nonNumericSuffix, speed, callback, accelerate, decelerate, useCssPosition)
{
    var startPosition = this.GetAbsolutePosition();

    if (!IsDefined(useCssPosition) || useCssPosition == null)
    {
        useCssPosition = "relative";
    }
    if (!IsDefined(speed) || speed == null)
    {
        speed = 28800;
    }
    if (!IsDefined(nonNumericSuffix) || nonNumericSuffix == null)
    {
        nonNumericSuffix = "px";
    }
    if (!IsDefined(destinationX) || destinationX == null)
    {
        destinationX = startPosition.Get_X();
    }
    if (!IsDefined(destinationY) || destinationY == null)
    {
        destinationY = startPosition.Get_Y();
    }

    var xDistance = Math.abs(destinationX - startPosition.Get_X());
    var yDistance = Math.abs(destinationY - startPosition.Get_Y());
    this.SetCssLocation(startPosition.Get_X(), startPosition.Get_Y());
    this.SetCss("position", useCssPosition);

    this.AnimateProperty("left", destinationX, nonNumericSuffix, speed, accelerate, decelerate, null, null, xDistance>=yDistance?callback:null, null, startPosition.Get_X());
    this.AnimateProperty("top", destinationY, nonNumericSuffix, speed, accelerate, decelerate, null, null, xDistance<yDistance?callback:null, null, startPosition.Get_Y());
    
    return this;
};

/**
 * @function ToggleOpenVertical
 * Toggle an element between open and closed with a vertical animation.
 * 
 * @param {Number} speed
 * @param {Function} callback
 * @param {Boolean} recalculateHeight
 * @param {Integer} endHeight
 * @param {Boolean} constrainHeightByContainer
 *
 * @returns {Element} Return this element for chained method calls.
 */
Element.prototype.ToggleOpenVertical = function (speed, callback, recalculateHeight, endHeight, constrainHeightByContainer)
{
    if (this.IsShowing())
    {
        this.CloseVertical(speed, callback);
    } else {
        this.OpenVertical(speed, callback, recalculateHeight, endHeight, constrainHeightByContainer);
    }
    return this;
};

/**
 * @function OpenVertical
 * Open an element vertically (by inceasing it's height)
 * 
 * @param {Number} speed The speed that the animation will occur, in units per minute.
 * @param {Function} callback
 * @param {Boolean} recalculateHeight
 * @param {Integer} endHeight
 * @param {Boolean} constrainHeightByContainer
 *
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.OpenVertical = function (speed, callback, recalculateHeight, endHeight, constrainHeightByContainer)
{
    if (this.OriginalOverflow === null)
    {
        this.OriginalPaddingTop = parseFloat(this.GetStyle("padding-top").replace(/px/g, ""));
        this.OriginalPaddingBottom = parseFloat(this.GetStyle("padding-bottom").replace(/px/g, ""));
        this.OriginalMarginTop = parseFloat(this.GetStyle("margin-top").replace(/px/g, ""));
        this.OriginalMarginBottom = parseFloat(this.GetStyle("margin-bottom").replace(/px/g, ""));
        this.OriginalOverflow = this.GetStyle("overflow");
    }

    if (!IsDefined(this._originalStyle))
    {
        this._originalStyle = this.GetCurrentStyle();
    }

    if (!IsDefined(recalculateHeight))
    {
        recalculateHeight = false;
    }

    if (!IsDefined(constrainHeightByContainer))
    {
        constrainHeightByContainer = false;
    }
    if (!this.IsOpening)
    {
        this.IsOpening = true;

        if (this.OriginalOverflow === null)
        {
            this.OriginalOverflow = this.GetStyle("overflow");
        }
        
        if (!IsDefined(speed) || speed === 0 || speed === null)
        {
            speed = 100000;
        }

        var startOpacity = this.GetOpacity();
        var startHeight = 0;
        if (this.IsShowing())
        {
            startHeight = this.GetAbsoluteSize(false, false, true, true).Get_Y();
        } else {
            startHeight = 0;
        }
        this.SetOpacity(0);
        if (!IsDefined(endHeight) || endHeight == null || endHeight == 0)
        {
            if (recalculateHeight || !this.OriginalHeight)
            {
                var fullSize = this.GetAbsoluteSize(false, recalculateHeight, true, constrainHeightByContainer);
                endHeight = fullSize.Get_Y() + this.OriginalPaddingBottom + this.OriginalPaddingTop;
                this.OriginalHeight = endHeight;
            }
            else
            {
                endHeight = this.OriginalHeight;
            }
        }
        this.SetCss("overflow", "hidden");
        this.SetCss("height", startHeight+"px");
        
        if (this.OriginalWidth)
        {
            this.SetCss("width", this.OriginalWidth + "px");
        }
        this.Show(false);
        this.SetOpacity(startOpacity);

        this.AnimateProperty("height", endHeight, "px", speed, null, null, null, null,
            function ()
            {
                this.SetCss("overflow", this.OriginalOverflow);
                this.IsOpening = false;
                if (IsDefined(callback) && callback != null)
                {
                    callback();
                }
            }.Bind(this), null, startHeight);
        if (this.OriginalPaddingTop > 0)
        {
            this.AnimateProperty("padding-top", this.OriginalPaddingTop, "px", speed);
        }
        if (this.OriginalPaddingBottom > 0)
        {
            this.AnimateProperty("padding-bottom", this.OriginalPaddingBottom, "px", speed);
        }
        if (this.OriginalMarginBottom > 0)
        {
            this.AnimateProperty("margin-bottom", this.OriginalMarginBottom, "px", speed);
        }
        if (this.OriginalMarginTop > 0)
        {
            this.AnimateProperty("margin-top", this.OriginalMarginTop, "px", speed);
        }
        this.IsClosed = false;
    }
    return this;
};


/**
 * @function CloseVertical
 * Close an element vertically (by decreasing it's height)
 * 
 * @param {Number} speed The speed that the animation will occur, in pixels per minute.
 * 
 * @param {Function} callback
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.CloseVertical = function (speed, callback)
{
    if (!IsDefined(this._originalStyle))
    {
        this._originalStyle = this.GetCurrentStyle();
    }

    if (!this.IsClosed && this.IsShowing() )
    {
        if (!IsDefined(speed) || speed === 0 || speed === null)
        {
            speed = 100000;
        }
        if (!this.IsOpening)
        {
            this.OriginalHeight = this.GetAbsoluteSize().Get_Y();
            this.OriginalPaddingTop = parseFloat(this.GetStyle("padding-top").replace(/px/g, ""));
            this.OriginalPaddingBottom = parseFloat(this.GetStyle("padding-bottom").replace(/px/g, ""));
            this.OriginalMarginTop = parseFloat(this.GetStyle("margin-top").replace(/px/g, ""));
            this.OriginalMarginBottom = parseFloat(this.GetStyle("margin-bottom").replace(/px/g, ""));
            this.OriginalOverflow = this.GetStyle("overflow");
        }
        this.SetCss("overflow", "hidden");
        this.AnimateProperty("height", 0, "px", speed, null, null, null, null, function () {
            this.AnimateProperty("padding-top", 0, "px", speed);
            this.AnimateProperty("padding-bottom", 0, "px", speed);
            this.AnimateProperty("margin-top", 0, "px", speed);
            this.AnimateProperty("margin-bottom", 0, "px", speed);
            this.Hide();
            if (IsDefined(callback) && callback != null) {
                callback();
            }
        }.Bind(this));
        this.IsOpening = false;
        this.IsClosed = true;
    }
    return this;
};

/**
 * @function ToggleVertical
 * An alias of ToggleOpenVertical.
 * Open/Close an element vertically.
 * 
 * @param {Number} speed The speed that the animation will occur, in pixels per minute.
 * @param {Function} callback
 * @param {Boolean} recalculateHeight
 * @param {Integer} endHeight
 * @param {Boolean} constrainHeightByContainer
 * 
 * @returns {Element} returns this element for chained method calls
 */
Element.prototype.ToggleVertical = function (speed, callback, recalculateHeight, endHeight, constrainHeightByContainer)
{
    this.ToggleOpenVertical(speed, callback, recalculateHeight, endHeight, constrainHeightByContainer);
    return this;
};

/**
 * @function OpenHorizontal
 * Open an element horizontally (by inceasing it's width)
 * 
 * @param {Number} speed The speed that the animation will occur, in units per minute.
 * The unit is determined by the property being set and potentially the valueNonNumericSuffix.
 * Typically, if you specify a valueNonNumericSuffix, that will also be the unit used.
 * 
 * @param {Number} endWidth The target width.
 * 
 * @param {Function} callback
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.OpenHorizontal = function (speed, endWidth, callback)
{
    var fullSize = this.GetAbsoluteSize();
    if (!IsDefined(endWidth) || endWidth == null)
    {
        if (this.OriginalWidth)
        {
            endWidth = this.OriginalWidth;
        } else {
            endWidth = fullSize.Get_X();
        }
    }
    if (!IsDefined(speed) || speed == 0 || speed == null)
    {
        speed = 100000;
    }
    this.SetCss("width", "0px");
    if (this.OriginalHeight)
    {
        this.SetCss("height", this.OriginalHeight + "px");
    }
    this.Show(false);
    this.AnimateProperty("width", endWidth, "px", speed, null, null, null, null, callback);
    if (this.OriginalPaddingLeft > 0)
    {
        this.AnimateProperty("padding-left", this.OriginalPaddingLeft, "px", speed);
    }
    if (this.OriginalPaddingRight > 0)
    {
        this.AnimateProperty("padding-right", this.OriginalPaddingRight, "px", speed);
    }
    return this;
};

/**
 * @function CloseHorizontal
 * Close an element horizontally (by decreasing it's width)
 * 
 * @param {Number} speed The speed that the animation will occur, in units per minute.
 * The unit is determined by the property being set and potentially the valueNonNumericSuffix.
 * Typically, if you specify a valueNonNumericSuffix, that will also be the unit used.
 * 
 * @param {Function} callback
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.CloseHorizontal = function (speed, callback)
{
    if (!IsDefined(speed) || speed == 0)
    {
        speed = 50000;
    }
    this.SetCss("min-width", 0);
    this.SetCss("overflow", "hidden");
    this.OriginalWidth = this.GetStyle("width")
        .replace(/px/g, "");
    this.OriginalPaddingLeft = this.GetStyle("padding-left")
        .replace(/px/g, "");
    this.OriginalPaddingRight = this.GetStyle("padding-right")
        .replace(/px/g, "");
    this.AnimateProperty("width", 0, "px", speed, null, null, null, null, function () {
        this.Hide();
        if (IsDefined(callback) && callback != null) {
            callback();
        }
    }.Bind(this));
    this.AnimateProperty("padding-left", 0, "px", speed);
    this.AnimateProperty("padding-right", 0, "px", speed);
    return this;
};

/**
 * @function SetCssX
 * Set the element's left value, in pixels
 * 
 * @param {Number} x
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetCssX = function (x)
{
    this.style.left = x + 'px';
    return this;
};

/**
 * @function SetCssY
 * Set the element's top value, in pixels
 * 
 * @param {Number} y
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetCssY = function (y)
{
    this.style.top = y + 'px';
    return this;
};

/**
 * @function SetCssLocation
 * Set the element's top/left location in pixels
 * 
 * @param {Number} x
 * @param {Number} y
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetCssLocation = function (x, y)
{
    this.style.left = x + 'px';
    this.style.top = y + 'px';
    return this;
};

/**
 * @function SetCssSize
 * Set the element's size in pixels
 * 
 * @param {Number} width
 * @param {Number} height
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetCssSize = function (width, height)
{
    this.style.width = width + 'px';
    this.style.height = height + 'px';
    return this;
};

/**
 * @function GetIsDisplayed
 * Determine whether or not this element's current display style is a visible one.
 * 
 * @returns {Boolean}
 */
Element.prototype.GetIsDisplayed = function ()
{
    return this.style.display != "none";
};

/**
 * @function IsShowing
 * Whether or not this element is currently showing.
 * 
 * @param {Boolean} useComputedStyle
 * @param {Boolean} checkAncestors
 *
 * @returns {Boolean}
 */
Element.prototype.IsShowing = function (useComputedStyle, checkAncestors, fastMode)
{
    if (this === document.body || this === document || this.GetCss("position", true) == "fixed" || this.GetCss("position", true) == "absolute")
    {
        checkAncestors = false;
        fastMode = false;
    }
    
    if (!IsDefined(fastMode))
    {
        fastMode = true;
    }

    if (fastMode)
    {
        return this.offsetParent !== null;
    }
    
    if (!IsDefined(useComputedStyle))
    {
        useComputedStyle = true;
    }

    if (!IsDefined(checkAncestors))
    {
        if (this.GetCss("position", true) == "fixed" || this.GetCss("position", true) == "absolute")
        {
            checkAncestors = false;
        } else {
            checkAncestors = this.offsetParent !== null;
        }
    }
    var isShowing = true;
    if (this.GetCss("display", useComputedStyle) === 'none' || this.GetCss("height", true) === '0px' ||
        (IsDefined(this.hidden) && this.hidden) || this.GetOpacity() == 0 || this.GetAttribute("hidden"))
    {
        isShowing = false;
    }
    else if (checkAncestors)
    {
        var parent = this.GetParent();
        if (parent === null || !IsDefined(parent.tagName) || parent.tagName === "html" || parent.tagName === "HTML")
        {
            isShowing = true;
        } else {
            isShowing = IsDefined(parent.IsShowing)?parent.IsShowing(useComputedStyle, true):true;
        }
    }

    return isShowing;
};

/**
 * @function SetOpacity
 * Set the element's opacity
 * 
 * @param {Number} opacity a percentage from 0 to 100
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetOpacity = function (opacity)
{
    this.style.opacity = .01 * opacity;
    this.style.MozOpacity = .01 * opacity;
    this.style.filter = "alpha(opacity=" + opacity + ")";
    return this;
};

/**
 * Get this element's CSS opacity, expressed as a percentage (1 - 100)
 * @returns {Number} this element's CSS opacity, expressed as a percentage (1 - 100)
 */
Element.prototype.GetOpacity = function ()
{
    if (IsDefined(this.style.opacity) && (this.style.opacity != null) && (this.style.opacity != ""))
    {
        return this.style.opacity*100;
    }
    else if (IsDefined(this.style.MozOpacity) && (this.style.MozOpacity != null) && (this.style.MozOpacity != "") )
    {
        return this.style.MozOpacity*100;
    }
    else if (this.GetCss("opacity") != null && this.GetCss("opacity") != "")
    {
        return this.GetCss("opacity")*100;
    }
    return 100;
};

/**
 * @function AddClass Add one or more css classes to this element
 * Add one or more css classes to this element.
 * 
 * @param {String|Array} classes pass a single string for one class, or an array of strings for multiple classes.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddClass = function (classes)
{

    if (!IsArray(classes))
    {
        classes = [classes];
    }

    for (var i = 0; i < classes.length; i++)
    {
        var className = classes[i];

        if (!this.HasClass(className))
        {
            if (this.className != "")
            {
                var classNames = this.className.split(" ");
                classNames.push(className);
                this.className = classNames.join(' ');
            } else {
                this.className = className;
            }
        }
    }
    return this;
};

/**
 * @function RemoveClass Remove one or more css classes from this element
 * Remove one or more css classes from this element.
 * 
 * @param {String|Array} classes pass a single string for one class, or an array of strings for multiple classes.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.RemoveClass = function (classes)
{
    if (!IsArray(classes))
    {
        classes = [classes];
    }

    for (var i = 0; i < classes.length; i++)
    {
        var className = classes[i];

        if (this.HasClass(className))
        {
            var classNames = this.className.split(' ');
            classNames.splice(classNames.indexOf(className), 1);
            this.className = classNames.join(' ');
        }
    }
    return this;
};


/**
 * @function Hide
 * Hide this element by setting it's display to "none"
 * 
 * @param {Number} delayMs How long to wait before hiding
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Hide = function (delayMs)
{
    if (!IsDefined(this._originalStyle))
    {
        this._originalStyle = this.GetCurrentStyle();
    }
    
    if (!IsDefined(delayMs))
    {
        delayMs = 0;
    }
    if (delayMs == 0)
    {
        this._hidePending = false;
        if (this.GetCurrentStyle().display != "none")
        {
            this.visibleDisplayStyle = this.GetCurrentStyle().display;
            this.style.display = "none";
        }
    } else {
        this._hidePending = true;
        setTimeout(function ()
        {
            if (this._hidePending)
            {
                this.Hide();
            }
        }.Bind(this), delayMs);
    }
    return this;
};

/**
 * @function Show
 * Show this element by setting it's display, visibility, and opacity
 * 
 * @param {Boolean} [restoreFromAnimations=true]
 * @default true
 *
 * @param {String} [displayClass=block|inline|inline-block|table-row]
 * @default Depends on element type.
 *
 * @param {Number} [opacity=1]
 * Between 0 and 1, where 0 is completely transparent and 1 is completely opaque.
 * Set to <em>false</em> if you want to leave the opacity as-is.
 *
 * @default 1
 * 
 * @returns {Element}
 * Returns this element for chained method calls
 */
Element.prototype.Show = function (restoreFromAnimations, displayClass, opacity)
{
    DblEj.UI.Effects.ClearAnimations(this);
    if (!IsDefined(this._originalStyle))
    {
        this._originalStyle = this.GetCurrentStyle();
    }

    if (!IsDefined(opacity) || opacity === null)
    {
        if (this.GetCss("opacity", true) > 0)
        {
            opacity = false;
    }
    }
    if (!IsDefined(displayClass))
    {
        displayClass = null;
    }
    if (displayClass == null)
    {
        if (this.GetParent() != null && this.GetParent().HasClass("Layout") &&
            this.GetParent().HasClass("Flow"))
        {
            displayClass = "inline-block";
        }
        else if (this.visibleDisplayStyle && (this.visibleDisplayStyle != "none"))
        {
            displayClass = this.visibleDisplayStyle;
        }
        else if (this.tagName.toLowerCase() === "tr")
        {
            displayClass = "table-row";
        }
        else if (this.tagName.toLowerCase() === "li")
        {
            displayClass = "list-item";
        }
        else if (this.tagName.toLowerCase() === "table")
        {
            displayClass = "table";
        }
        else
        {
            displayClass = IsDefined(this._originalStyle)?(this._originalStyle["display"]?this._originalStyle["display"]:null):null;
            if (displayClass == null || displayClass == "none")
            {
                displayClass = this.GetClassCss("display", "none");
            }
            if (displayClass == null)
            {
                if (Element._blockTags.indexOf(this.tagName.toLowerCase()) > -1)
                {
                    displayClass = "block";
                } else {
                    for (var blockSelectorIdx = 0; blockSelectorIdx < Element._blockSelectors.length; blockSelectorIdx++)
                    {
                        if (this.matches(Element._blockSelectors[blockSelectorIdx]))
                        {
                            displayClass = "block";
                            break;
                        }
                    }
                }
            }
            if (displayClass == null)
            {
                displayClass = "inline";
            }

        }
    }

    if (!this.HasBeenAddedToDom())
    {
        throw "Element must be added to the DOM before it can be shown";
    }
    if (!IsDefined(restoreFromAnimations) || restoreFromAnimations === null)
    {
        restoreFromAnimations = true;
    }
    if (restoreFromAnimations)
    {
        if (this.OriginalHeight)
        {
            this.SetCss("height", this.OriginalHeight + "px");
        }
        if (this.OriginalWidth)
        {
            this.SetCss("width", this.OriginalWidth + "px");
        }
    }
    this._hidePending = false;
    if (this.GetStyle("display").toLowerCase() === "none" || this.GetStyle("display") === "")
    {
        this.SetCss("display", displayClass);
    }

    if (this.GetStyle("visibility").toLowerCase() === "hidden")
    {
        this.SetCss("visibility", 'visible');
    }
    if (opacity !== false)
    {
        this.SetCss("opacity", opacity);
    }
    this.RemoveAttribute("hidden");
    this.Trigger("Show");
    return this;
};


/**
 * Make this element invisible, while still occupying it's space in the layout.
 * This is accomplished by setting the CSS visibility to "hidden"
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Cloak = function()
{
    this.SetCss("visibility", 'hidden');
    return this;
};

/**
 * Make this element visible, where it had been previously made invisible by using the Cloak method, or where it's CSS visibility had been set to "hidden".
 * This is accomplished by setting the CSS visibility to "visible"
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.UnCloak = function()
{
    this.SetCss("visibility", 'visible');
    return this;
};

/**
 * @function HasBeenAddedToDom
 * Check if this element is part of the DOM.
 */
Element.prototype.HasBeenAddedToDom = function ()
{
    var elem = this;
    do
    {
        elem = elem.GetParent();
        if (elem === document)
        {
            break;
        }
    } while (elem !== null);

    return elem === document;
};

/**
 * @function FadeOut
 * Fade out the element by animating it's opacity from it's current value down to zero.
 *
 * @example
 * let unit = 6 (represents 1/10th of a minute)
 * ttl = (1 / speed) * unit seconds
 *
 * speed of 1 means it will take six seconds fade (1/1 * 6).
 * speed of 6 means it will take one second to fade (1/6 * 6).
 * speed of 12 means it will take half a second to fade (1/12 * 6).
 *
 * @param {Number} speed The speed of the fade expressed in percent points per 1/10th of a minute (six seconds)
 *
 * @param {Function} callbackFunction
 * @param {Number} endZIndex
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.FadeOut = function (speed, callbackFunction, endZIndex, endOpacity)
{
    if (!IsDefined(endOpacity) || endOpacity==null)
    {
        endOpacity = 0;
    }
    this.Fade(this.GetOpacity()/100, endOpacity, speed, -1, function ()
    {
        this.Hide();
        if (IsDefined(callbackFunction) && (callbackFunction != null))
        {
            callbackFunction();
        }
    }.Bind(this), endZIndex);
    return this;
};

/**
 * @function FadeIn
 * Fade in the element by animating it's opacity from it's opacity from 0 to 1.
 * 
 * @example
 * let unit = 6 (represents 1/10th of a minute)
 * ttl = (1 / speed) * unit seconds
 *
 * speed of 1 means it will take six seconds fade (1/1 * 6).
 * speed of 6 means it will take one second to fade (1/6 * 6).
 * speed of 12 means it will take half a second to fade (1/12 * 6).
 * 
 * @param {Number} speed The speed of the fade expressed in percent points per 1/10th of a minute (six seconds) *
 * @param {Function} callbackFunction
 * @param {Number} endZIndex
 * @param {Number} endOpacity Between a percentage 0 and 100
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.FadeIn = function (speed, callbackFunction, endZIndex, endOpacity)
{
    if (!IsDefined(endOpacity) || endOpacity==null)
    {
        endOpacity = 100;
    }
    if (this.OriginalHeight)
    {
        this.SetCss("height", this.OriginalHeight + "px");
    }
    if (this.OriginalWidth)
    {
        this.SetCss("width", this.OriginalWidth + "px");
    }
    if (!this.IsShowing())
    {
        this.Show(true, null, false);
        this.SetOpacity(0);
    }
    this.Fade(this.GetOpacity()/100, endOpacity, speed, 1, callbackFunction, endZIndex);
    return this;
};

/**
 * @function Fade
 * Fade the element in or out by animating it's opacity.
 * 
 * @param {Number} currentOpacity The opacity to start with.  Range: 0 to 1.
 * @param {Number} endOpacity The opacity to end with.  Range: 0 to 1.
 * @param {Number} speed The speed of the fade expressed in percent points per 1/10th of a minute (six seconds)
 * For example,
 * speed of 10 means it will take 1 minute to fade.
 * speed of 100 means it will take six seconds fade.
 * speed of 600 means it will take one second to fade.
 * speed of 1200 means it will take half a second to fade.
 *
 * @param {Number} fadeDirection -1 or 1
 * @param {Function} callbackFunction
 * @param {Number} endZIndex
 * 
 * @returns {Element} Returns this element for chained method calls.
 */

Element.prototype.Fade = function (currentOpacity, endOpacity, speed, fadeDirection, callbackFunction, endZIndex)
{
    if (!IsDefined(endOpacity) || endOpacity == null)
    {
        endOpacity = 1;
    } else {
        endOpacity = endOpacity/100;
    }
    if (!IsDefined(currentOpacity) || currentOpacity == null || currentOpacity === "")
    {
        currentOpacity = this.GetCss("opacity");
    }
    if (!IsDefined(currentOpacity) || currentOpacity == null || currentOpacity === "")
    {
        if ((endOpacity == 0) || (fadeDirection == -1))
        {
            currentOpacity = 1;
        } else {
            currentOpacity = 0;
        }
    }
    if (typeof fadeDirection == undefined || typeof fadeDirection == "undefined" || fadeDirection == null)
    {
        if (currentOpacity > endOpacity)
        {
            fadeDirection = -1;
        } else {
            fadeDirection = 1;
        }
    }
    if ((!IsDefined(speed)) || (speed == null))
    {
        speed = 5;
    }

    this.SetCss("opacity", currentOpacity);
    this.AnimateProperty("opacity", endOpacity, "", speed * 10, 0, 0, 0, 0, callbackFunction);
    return this;
};

/**
 * @function GrowY
 * Animate the element's height, increasing it until it reaches the specified height.
 *
 * @param {Number} increment
 * @param {Number} finalHeight
 *
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.GrowY = function (increment, finalHeight)
{
    var height = this.offsetHeight;
    this.style.height = (height + increment) + "px";
    if ((height < finalHeight) && (height < 3000))
    {
        setTimeout(
            function ()
            {
                this.GrowY(increment, finalHeight);
            }.Bind(this), 30);
    }
    return this;
};

/**
 * @function ShrinkY
 * Animate the element's height, reducing it until it reaches the specified height.
 *
 * @param {Number} increment
 * @param {Number} finalHeight
 *
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ShrinkY = function (increment, finalHeight)
{
    var height = this.offsetHeight;
    this.style.height = (height - increment) + "px";
    if ((height > finalHeight) && (height > 0))
    {
        setTimeout(
            function ()
            {
                this.ShrinkY(increment, finalHeight);
            }.Bind(this), 30);
    }
    return this;
};

/**
 *
 * @param {int} direction 1=left, 2=right, 3=up, 4=down
 * @param {Function} callback
 * @param {float} accelerate Percentage between 1 and 100 that specifies at what point to reach full speed
 * @param {float} decelerate
 * @returns {Element}
 */

Element.prototype.SlideOffScreen = function (direction, callback, speed, accelerate, decelerate)
{
    if (!IsDefined(direction) || direction == null)
    {
        direction = 1;
    }
    if (!IsDefined(speed) || speed == null)
    {
        speed = 480000;
    }
    
    var pos = this.GetRelativePosition();
    var size = this.GetAbsoluteSize();
    var windowSize = DblEj.UI.Utils.GetPageSize(false);
    //document.body.AppendChild(this);
    this
        .SetCss("position", "relative")
        .SetCssLocation(pos.Get_X(), pos.Get_Y());

    this.GetParent().SetCss("overflow", "hidden");
    if (direction == 1)
    {
        this.AnimateProperty("left", -size.Get_X(), "px", speed, accelerate, decelerate, null, null, callback, null, pos.Get_X());
    }
    else if (direction == 2)
    {
        this.AnimateProperty("left", windowSize.Get_X(), "px", speed, accelerate, decelerate, null, null, callback, null, pos.Get_X());
    }
    else if (direction == 3)
    {
        this.Move(pos.Get_X(), -size.Get_Y(), "px", speed, callback, accelerate, decelerate);
    }
    else if (direction == 4)
    {
        this.Move(pos.Get_X(), windowSize.Get_Y(), "px", speed, callback, accelerate, decelerate);
    }

    return this;
};

/**
 * Append this element to another element's children
 * @param {Element} parentElement
 * @returns {Element} returns this element to allow for chained method calling
 */
Element.prototype.AppendTo = function(parentElement)
{
    if (!IsDefined(parentElement) || parentElement === null)
    {
        parentElement = document.body;
    }
    parentElement.AppendChild(this);
    return this;
};

/**
 * @function CollidesWith
 * Test if this element is in collision with the specified element.
 * @param {Element} element
 * @returns {Boolean} <i>True</i> if the elements are colliding, otherwise <i>false</i>.
 */
Element.prototype.CollidesWith = function (element)
{
    var myPosition = this.GetAbsolutePosition();
    var hisPosition = element.GetAbsolutePosition();

    var myLeft = myPosition.Get_X();
    var myRight = myLeft + this.clientWidth;
    var myTop = myPosition.Get_Y();
    var myBottom = myTop + this.clientHeight;

    var hisLeft = hisPosition.Get_X();
    var hisRight = hisLeft + element.clientWidth;
    var hisTop = hisPosition.Get_Y();
    var hisBottom = myTop + element.clientHeight;

    return (myRight >= hisLeft) && (myLeft <= hisRight) &&
        (myBottom >= hisTop) && (myTop <= hisBottom);
};

Element.prototype.GetScrollOffset = function()
{
    if (IsDefined(this.pageXOffset))
    {
        x = this.pageXOffset;
        y = this.pageYOffset;
    }
    else if (IsDefined(this.scrollLeft))
    {
        x = this.scrollLeft;
        y = this.scrollTop;
    }
    else
    {
        x = 0;
        y = 0;
    }
};

/**
 * @function GetAbsoluteSize
 * Get the absolute size of this element, in pixels.
 * We use two different methods to achieve this:
 * The "old" way is we will actually clone the element and try to mimic its same suurounding to get the absolute height.
 * The "new" way (when slowMode = null) is to not clone at all but instead to use calculatedStyle.
 * 
 * @param {Boolean} isClone Set internally by DblEj to avoid infinite recursion when cloning non-DOM elements for size-testing purposes.
 * @param {Boolean} recalculate Whether or not you want the height to be recalculated based on its contents.
 * @param {Boolean} constrainByContainerWidth
 * @param {Boolean} constrainByContainerHeight
 * @param {Boolean} slowMode
 * If we should use slower methods to calculate, which can sometimes be more accurate.
 * false = use a shallow clone to get the element's independent size by using element properties such as scrollHeight, height, and innerHeight
 * true = use a deep clone to get the element's independent size by using element properties such as scrollHeight, height, and innerHeight
 * null = use css's computedStyle and do not clone (fastest. Default as of rev 1886)
 * Note that isClone, recalculate, constrainByContainerWidth and constrainByContainerHeight have no effect when slowMode = null
 * @returns {DblEj.UI.Point} The element's absolute size.
 */
Element.prototype.GetAbsoluteSize = function (isClone, recalculate, constrainByContainerWidth, constrainByContainerHeight, slowMode) {

    var absSizeX = 0;
    var absSizeY = 0;

    if (!IsDefined(slowMode))
    {
        //we want to use the new fast method (slowMode = null) when we can. But it wont work for hidden elements
        if (this.IsShowing())
        {
            slowMode = null;
        } else {
            slowMode = false;
        }
    }

    if (slowMode === null)
    {
        absSizeX = parseFloat(this.GetStyle("width"));
        absSizeY = parseFloat(this.GetStyle("height"));
    }
    else
    {
        if (!IsDefined(recalculate))
        {
            recalculate = false;
        }
        if (!IsDefined(constrainByContainerWidth))
        {
            constrainByContainerWidth = true;
        }
        if (!IsDefined(constrainByContainerHeight))
        {
            constrainByContainerHeight = false;
        }
        var maxYForXCalc = "none";
        var maxXForYCalc = "none";
        var startMaxY = "none";
        var startMaxX = "none";

        startMaxY = this.GetCss("max-height");
        startMaxX = this.GetCss("max-width");
        if (startMaxX != "" && startMaxX != "none")
        {
            maxXForYCalc = startMaxX;
        }
        else if (this.HasData("container-x-bound"))
        {
            maxXForYCalc = this.GetData("container-x-bound");
        }
        if (startMaxY != "" && startMaxY != "none")
        {
            maxYForXCalc = startMaxY;
        }
        else if (this.HasData("container-y-bound"))
        {
            maxYForXCalc = this.GetData("container-y-bound");
        }

        if (IsDefined(this.offsetWidth) && this.offsetWidth > 0 && this.offsetHeight > 0) {

            this.SetCss("max-width", maxXForYCalc);
            this.SetCss("max-height", maxYForXCalc);

            absSizeY += this.offsetHeight;
            if (IsDefined(this.scrollHeight) && (absSizeY < this.scrollHeight) && (this.GetStyle("overflow") == "hidden"))
            {
                absSizeY = this.scrollHeight;
            }

            absSizeX += this.offsetWidth;
            if (IsDefined(this.scrollWidth) && (absSizeX < this.scrollWidth) && (this.GetStyle("overflow") == "hidden"))
            {
                absSizeX = this.scrollWidth;
            }

            this.SetCss("max-width", startMaxX);
            this.SetCss("max-height", startMaxY);
        } else if (IsDefined(this.scrollWidth) && this.scrollWidth > 0 && this.scrollHeight > 0) {
            this.SetCss("max-width", maxXForYCalc);
            this.SetCss("max-height", maxYForXCalc);

            absSizeY = this.scrollHeight;
            absSizeX = this.scrollWidth;

            this.SetCss("max-width", startMaxX);
            this.SetCss("max-height", startMaxY);
        } else if (IsDefined(this.outerWidth) && this.outerWidth > 0 && this.outerHeight > 0) {
            this.SetCss("max-width", maxXForYCalc);
            this.SetCss("max-width", startMaxX);

            absSizeY = this.outerHeight;
            absSizeX = this.outerWidth;

            this.SetCss("max-height", maxYForXCalc);
            this.SetCss("max-height", startMaxY);
        } else if (IsDefined(this.width) && this.width > 0 && this.height > 0) {
            this.SetCss("max-width", maxXForYCalc);
            this.SetCss("max-width", startMaxX);

            absSizeY = this.height;
            absSizeX = this.width;

            this.SetCss("max-height", maxYForXCalc);
            this.SetCss("max-height", startMaxY);
        } else {
            //maybe it isnt visible
            if (!isClone) //prevent recursion
            {
                var clone = this.Clone(true, false, slowMode, true, true);
                clone.style.position = "absolute";
                clone.style.top = "-5000px";
                clone.style.display = "block";
                if (recalculate)
                {
                    clone.SetCss("height", "auto");
                    clone.SetCss("overflow", "visible");
                    clone.SetCss("bottom", "auto");
                    var container = null;

                    if (constrainByContainerWidth)
                    {
                        container = this.GetVisibleContainer();
                        if (container != null)
                        {
                            clone.SetData("container-x-bound", container.GetStyle("width"));
                        }
                    }
                    if (constrainByContainerHeight)
                    {
                        if (container == null)
                        {
                            container = this.GetVisibleContainer();
                        }
                        if (container != null)
                        {
                            clone.SetData("container-y-bound", container.GetStyle("height"));
                        }
                    }
                } else {
                    clone.SetCss("height", this.GetStyle("height"));
                }
                document.body.appendChild(clone);
                clone.hidden = false;
                var returnSize = clone.GetAbsoluteSize(true, recalculate, constrainByContainerWidth, constrainByContainerHeight, slowMode);
                document.body.removeChild(clone);
                return returnSize;
            }
        }
    }
    return new DblEj.UI.Point(absSizeX, absSizeY);
};

/**
 * @function GetVisibleContainer
 * Get this elements nearest ancestor that is visible
 * @returns {Element}
 */
Element.prototype.GetVisibleContainer = function()
{
    var parent = this.GetParent();
    var visibleParent = null;
    while (parent != null)
    {
        if ( IsDefined(parent.IsShowing) && parent.IsShowing())
        {
            visibleParent = parent;
            break;
        }
        parent = parent.GetParent();
    }

    return visibleParent;
};

/**
 * @function GetAbsolutePosition
 * Get this element's absolute position, in pixels.
 * 
 * @returns {DblEj.UI.Point}
 */
Element.prototype.GetAbsolutePosition = function (relativeToViewport) {
    var parentObj = this.offsetParent;
    var parentPosition;
    if (parentObj)
    {
        parentPosition = parentObj.GetAbsolutePosition();
    } else {
        parentPosition = new DblEj.UI.Point(0, 0);
    }

    if (!IsDefined(relativeToViewport) || relativeToViewport == null)
    {
         relativeToViewport = false;
    }

    var pos = null;
    if (IsDefined(this.offsetLeft))
    {
        pos = new DblEj.UI.Point(this.offsetLeft + parentPosition.Get_X(), this.offsetTop + parentPosition.Get_Y());
    } else {
        pos = new DblEj.UI.Point(this.scrollLeft + parentPosition.Get_X(), this.scrollTop + parentPosition.Get_Y());
    }

    if (relativeToViewport == true)
    {
        var offset = window.GetOffsetPoint();
        pos.Set_X(pos.Get_X() - offset.Get_X());
        pos.Set_Y(pos.Get_Y() - offset.Get_Y());
    }
    return pos;
};

/**
 * @function GetRelativePosition
 * Get this element's relative (to it's parent) position, in pixels.
 *
 * @returns {DblEj.UI.Point}
 */
Element.prototype.GetRelativePosition = function () {
    var curleft = this.offsetLeft;
    var curtop = this.offsetTop;
    return new DblEj.UI.Point(curleft, curtop);
};

/**
 * @function Remove
 * Remove this element from it's parent.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Remove = function ()
{
    var parent = this.GetParent();
    if (parent != null)
    {
        parent.RemoveChild(this);
    }
    return this;
};

/**
 * @function RemoveChild
 * Remove a child node from this element.
 * 
 * @param {Node} childNode
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.RemoveChild = function (childNode)
{
    if (childNode.GetParent() != null)
    {
        this.removeChild(childNode);
    }
    return this;
};

/**
 * @function RemoveAllChildren
 * Remove all children of this element.
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.RemoveAllChildren = function ()
{
    while (this.hasChildNodes())
    {
        this.removeChild(this.childNodes[0]);
    }
    return this;
};

/**
 * @function ScrollTo
 * Scroll this element's parent container so that this element is in the visible viewport.
 * @param {HTMLElement} relativeToParent
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ScrollTo = function (relativeToParent, centered, absoluteOffset)
{
    var pos;
    var targetY = 0;

    if (typeof relativeToParent == undefined || typeof relativeToParent == "undefined")
    {
        relativeToParent = false;
    }
    if (typeof centered == undefined || typeof centered == "undefined")
    {
        centered = true;
    }
    if (typeof absoluteOffset == undefined || typeof absoluteOffset == "undefined" || !absoluteOffset)
    {
        absoluteOffset = 0;
    }
    if (relativeToParent)
    {
        pos = this.GetRelativePosition();
        targetY = pos.Get_Y();

        if (relativeToParent === true)
        {
            this.parentNode.scrollTop = targetY;
        } else {
            relativeToParent.scrollTop = targetY;
        }
    } else {
        pos = this.GetAbsolutePosition();
        var parentHeight = DblEj.UI.Utils.GetPageSize().Get_Y();
        var elemHeight = this.GetAbsoluteSize().Get_Y();

        if (centered)
        {
            targetY = pos.Get_Y() - (parentHeight/2) - (elemHeight/2) - absoluteOffset;
        } else {
            targetY = pos.Get_Y() - elemHeight - absoluteOffset;
        }
        if (IsDefined(window.scrollTo))
        {
            window.scrollTo(window.offsetX, targetY);
        }
        else if (IsDefined(window.pageYOffset))
        {
            window.pageYOffset = targetY;
        }
        else if (IsDefined(document.documentElement) && IsDefined(document.documentElement.scrollTop))
        {
            document.documentElement.scrollTop = targetY;
        } 
        else if (IsDefined(document.scrollTop))
        {
            document.documentElement.scrollTop = targetY;
        } else {
            window.offsetY = targetY;
        }
    }
    return this;
};

/**
 * @function Disable
 * Disable the element.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Disable = function ()
{
    this.SetAttribute("disabled", true)
        .AddClass("Disabled")
        .RemoveClass("Enabled");
    if (!IsDefined(this["onclick"]) || this["onclick"] == null)
    {
        this.onclick = function(event) { return false; };
        this.SetData("click-cancelled", "1");
    }
    return this;
};

/**
 * @function Enable
 * Enable the element.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Enable = function ()
{
    this.RemoveAttribute("disabled")
        .AddClass("Enabled")
        .RemoveClass("Disabled");
    if (this.GetData("click-cancelled") == "1")
    {
        this.onclick = null;
    }
    this.SetData("click-cancelled", null);

    return this;
};

/**
 * @function DisableInput
 * Disallow user input into this element.
 *
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.DisableInput = function ()
{
    this.SetAttribute("readonly", true);
    this.AddClass("ReadOnly");
    return this;
};

/**
 * @function EnableInput
 * Allow user input into this element.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.EnableInput = function ()
{
    this.RemoveAttribute("readonly");
    this.RemoveClass("ReadOnly");
    return this;
};

/**
 * @function SetFocus
 * Set the user's cursor to this element.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetFocus = function ()
{
    this.focus();
    return this;
};

/**
 * @function DisableTextSelection
 * Disable the selection of text within this element.
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.DisableTextSelection = function ()
{
    this.onmousedown = function () {
        return false;
    };
    this.onselectionstart = function () {
        return false;
    };
    this.onselectstart = function () {
        return false;
    };
    if (!this.draggable && !this.HasAttribute("draggable"))
    {
        this.ondragstart = function () {
            return false;
        };
    }
    return this;
};

/**
 * @function ClearText
 * Remove any text nodes that are children of this element.
 * 
 * @returns {Element}
 */
Element.prototype.ClearText = function ()
{
    return this.SetText("");
};

/**
 * @function SetText
 * Remove any existing text nodes from this element and then add a text node with the specified value to this element.
 * @param {String} newText
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetText = function (newText)
{
    this.RemoveAllChildren();
    this.appendChild(document.createTextNode(newText));
    return this;
};

/**
 * @function ToggleText
 * Toggle the text contents of this element between two values.
 * @param {String} text1
 * @param {String} text2
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleText = function (text1, text2)
{
    if (this.GetText() == text1)
    {
        this.SetText(text2);
    } else {
        this.SetText(text1);
    }
    return this;
};

/**
 * @function ToggleHtml
 * Toggle the InnerHtml contents of this element between two values.
 * @param {String} html1
 * @param {String} html2
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleHtml = function (html1, html2)
{
    if (this.GetInnerHtml() == html1)
    {
        this.SetInnerHtml(html2);
    } else {
        this.SetInnerHtml(html1);
    }
    return this;
};

/**
 * @function FindClosest
 * Get the closest element that matches the querySelector
 * 
 * @param {String} querySelector
 * The CSS query selector to match on
 *
 * @param {Boolean} [includeSelf=false]
 * If true, then this element will be returned if it matches the querySelector.
 * If false, then the nearest matching element that is not this element will be returned.
 * @default false
 * 
 * @returns {Element} The closest element.
 */
Element.prototype.FindClosest = function (querySelector, includeSelf)
{
    if (!IsDefined(querySelector) || !querySelector)
    {
        throw "Invalid querySelector passed to element.FindClosest";
    }
    if (!IsDefined(includeSelf))
    {
        includeSelf = false;
    }
    if (includeSelf && this.matches(querySelector))
    {
        return this;
    } else {
        var returnElems;
        if (this === window)
        {
            returnElems = $$q(querySelector, document);
            if (returnElems.length > 0)
            {
                return returnElems[0];
            }
        } else {
            var parentNode = this;
            var leastDistance = 999999999;
            var leastDistanceElem = null;
            var siblings = this.GetSiblings(true);
            var sibling;
            var foundSelf = false;
            for (var siblingIdx = siblings.length - 1; siblingIdx >= 0; siblingIdx--)
            {
                sibling = siblings[siblingIdx];
                if (sibling == this)
                {
                    foundSelf = true;
                }
                else if (foundSelf && sibling.matches(querySelector))
                {
                    leastDistanceElem = sibling;
                    break;
                }
                else if (sibling.matches(querySelector))
                {
                    leastDistanceElem = sibling;
                }
            }
            if (leastDistanceElem == null)
            {
                do
                {
                    if ((parentNode !== null) && (parentNode !== document))
                    {
                        if (parentNode.matches(querySelector))
                        {
                            returnElems = [parentNode];
                        }
                        else
                        {
                            returnElems = $$q(querySelector, parentNode);
                        }
                        parentNode = parentNode.GetParent();
                    }

                } while ((returnElems.length === 0) && parentNode !== null && parentNode !== document);


                if (this.IsShowing())
                {
                    Iteratize(returnElems);
                    returnElems.OnEach(
                        function (elem)
                        {
                            if (elem.IsShowing() && (this.GetDistanceFrom(elem) < leastDistance))
                            {
                                leastDistance = this.GetDistanceFrom(elem);
                                leastDistanceElem = elem;
                            }
                        }.Bind(this)
                        );
                }
                if ((leastDistanceElem == null) && returnElems.length > 0)
                {
                    leastDistanceElem = returnElems[0];
                }
            }
            if (leastDistanceElem !== null)
            {
                return leastDistanceElem;
            } else {
                return null;
            }
        }
    }
};

/**
 * @function FindNext
 * Get the next element that matches the querySelector
 * 
 * @param {String} querySelector
 * @returns {Element} The closest element.
 */
Element.prototype.FindNext = function (querySelector)
{
    var returnNode = null;
    if (this === window && this === window.document && this === window.document.body)
    {
        throw "Cannot call FindNext on the root elements";
    } else {
        var parent = this.GetParent();
        var siblings = this.GetSiblings(true);
        var sibling;

        if (parent !== null)
        {
            $$q(querySelector, parent)
                .OnEach
                (
                    function (elem)
                    {
                        if ((returnNode === null))
                        {
                            if (elem.GetDomDepth() > this.GetDomDepth())
                            {
                                returnNode = elem;
                            }
                            else if (elem.GetDomDepth() == this.GetDomDepth())
                            {
                                var foundMyself = false;
                                for (var siblingIdx in siblings)
                                {
                                    sibling = siblings[siblingIdx];
                                    if (foundMyself)
                                    {
                                        if (IsDefined(sibling.matches) && sibling.matches(querySelector))
                                        {
                                            returnNode = sibling;
                                        }
                                        break;
                                    }
                                    if (sibling == this)
                                    {
                                        foundMyself = true;
                                    }
                                }
                            }
                        }
                    }.Bind(this)
                    );
        }
        return returnNode;
    }
};

Element.prototype.FindPrevious = function (querySelector)
{
    var returnNode = null;
    if (this === window && this === window.document && this === window.document.body)
    {
        throw "Cannot call FindPrevious on the root elements";
    } else {
        var parent = this.GetParent();
        var siblings = this.GetSiblings(true);
        var sibling;
        var prevSibling = null;
        if (parent !== null)
        {
            if ($$q(querySelector, parent).length > 0 && $$q(querySelector, parent)[$$q(querySelector, parent).length-1].GetDomDepth() < this.GetDomDepth())
            {
                returnNode = $$q(querySelector, parent)[$$q(querySelector, parent).length-1];
            } else {
                $$q(querySelector, parent)
                    .OnEach
                    (
                        function (elem)
                        {
                            if ((returnNode === null))
                            {
                                if (elem.GetDomDepth() == this.GetDomDepth())
                                {
                                    for (var siblingIdx in siblings)
                                    {
                                        sibling = siblings[siblingIdx];
                                        if (sibling == this && prevSibling !== null)
                                        {
                                            if (IsDefined(prevSibling.matches) && prevSibling.matches(querySelector))
                                            {
                                                returnNode = prevSibling;
                                            }
                                            break;
                                        }
                                        prevSibling = sibling;
                                    }
                                }
                            }
                        }.Bind(this)
                        );
            }
        }
        return returnNode;
    }
};

/**
 * @function GetDistanceFrom
 * Get the distance from the specified element.
 * 
 * @param {HTMLElement} elem
 * 
 * @returns {Number} The distance, in pixels.
 */
Element.prototype.GetDistanceFrom = function (elem)
{
    if (!this.IsShowing() || !elem.IsShowing())
    {
        throw "Cannot calculate the distance between invisible items";
    } else {
        return Math.GetDistance(this.GetAbsolutePosition(), elem.GetAbsolutePosition());
    }
};

/**
 * @function GetSiblings
 * Get this elements siblings.
 * 
 * @param {Boolean} [includeSelf=false] whether or not to include this element in the return list.
 * @returns {Array}
 */
Element.prototype.GetSiblings = function (includeSelf)
{
    if (!IsDefined(includeSelf))
    {
        includeSelf = false;
    }
    var children;
    if (this !== window && this !== window.document && this !== window.document.body)
    {
        var returnChildren;
        children = this.GetParent()
            .GetChildElements();
        if (!includeSelf)
        {
            var child;
            returnChildren = [];
            for (var childIdx = 0; childIdx < children.length; childIdx++)
            {
                child = children[childIdx];
                if (child !== this)
                {
                    returnChildren[returnChildren.length] = child;
                }
            }
        } else {
            returnChildren = children;
        }
    } else {
        throw "The window, window.document, and window.document.body never have siblings";
    }
    return returnChildren;
};

/**
 * @function GetAncestors
 * Get the ancestory chain for this element.
 * 
 * @returns {Array}
 */
Element.prototype.GetAncestors = function ()
{
    var parentElem = this;
    var ancestorArray = [];
    do
    {
        if (parentElem.nodeType === document.ELEMENT_NODE)
        {
            parentElem = parentElem.GetParent();
            if (parentElem !== null)
            {
                ancestorArray[ancestorArray.length] = parentElem;
            }
        } else {
            break;
        }
    } while (parentElem !== null)
    return ancestorArray;
};

/**
 * @function GetDomDepth
 * Get the depth of this element within it's parent document's tree.
 * 
 * @returns {Number}
 */
Element.prototype.GetDomDepth = function ()
{
    var ancestors = this.GetAncestors();
    return ancestors.length;
};

/**
 * @function ContainsAncestor
 * Check if the specified element is an ancestor of this element.
 * 
 * @param {HTMLElement} elem
 * @returns {Boolean}
 */
Element.prototype.ContainsAncestor = function (elem)
{
    var contains = false;
    var parentElem = this.GetParent();
    while (parentElem !== null)
    {
        if (parentElem == elem)
        {
            contains = true;
            break;
        }
        if (parentElem != document)
        {
            parentElem = parentElem.GetParent();
        } else {
            parentElem = null;
        }
    }

    return contains;
};

Element.prototype.ContainsAncestorType = function (elemType)
{
    var contains = false;
    var parentElem = this.GetParent();
    while (parentElem !== null)
    {
        if (parentElem.nodeName.toLowerCase() == elemType.toLowerCase())
        {
            contains = true;
            break;
        }
        if (parentElem != document)
        {
            parentElem = parentElem.GetParent();
        } else {
            parentElem = null;
        }
    }

    return contains;
};

/**
 * @function ContainsSibling
 * Check whether or not the passed element is a sibling of this element.
 * 
 * @param {HTMLElement} elem
 * 
 * @returns {Boolean}
 */
Element.prototype.ContainsSibling = function (elem)
{
    var contains = false;
    var siblingElems = elem.GetParent()
        .GetChildren();

    for (var siblingIdx in siblingElems)
    {
        if (siblingElems[siblingIdx] === this)
        {
            contains = true;
            break;
        }
    }

    return contains;
};

/**
 * @function ContainsCousin
 * Check whether or not the passed element is a nephew/niece of this element (this method is mis-named).
 * 
 * @param {HTMLElement} elem
 * 
 * @returns {Boolean}
 */
Element.prototype.ContainsCousin = function (elem)
{
    var contains = false;
    var siblingElems = elem.GetParent()
        .GetChildren();
    var testElem;
    var cousins;

    for (var siblingIdx in siblingElems)
    {
        testElem = siblingElems[siblingIdx];
        cousins = testElem.GetChildren();
        for (var cousinIdx in cousins)
        {
            if (cousins[cousinIdx] == this)
            {
                contains = true;
                break;
            }
        }
    }

    return contains;
};

/**
 * @function GetLastChild
 * Get the last child node of this element.
 * 
 * @returns {HTMLElement|Node|null}
 */
Element.prototype.GetLastChild = function ()
{
    var children = this.GetChildren();
    if (children.length > 0)
    {
        return children[children.length - 1];
    } else {
        return null;
    }
};

/**
 * @function GetLastChildElement
 * @returns {Array}
 */
Element.prototype.GetLastChildElement = function ()
{
    var children = this.GetChildElements();
    if (children.length > 0)
    {
        return children[children.length - 1];
    } else {
        return null;
    }
};

/**
 * @function SelectAllText
 * Select all text
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SelectAllText = function ()
{
    var doc = document
        , range, selection
    ;
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(this);
        range.select();
    } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(this);
        selection.removeAllRanges();
        selection.addRange(range);
    }
    return this;
};

/**
 * @function GetText
 * Get the text value for this node.
 * If this is not a text node, then get the text contents.
 * 
 * @returns {String}
 */
Element.prototype.GetText = function ()
{
    if (IsDefined(this.nodeType) && this.nodeType === 3) //this is a text node already
    {
        return this.nodeValue;
    }
    else if (IsDefined(this.textContent))
    {
        return this.textContent;
    }
    else if (IsDefined(this.innerText))
    {
        return this.innerText;
    }
    else
    {
        return this.innerHtml;
    }
};


/**
 * @function ToggleClass
 * Toggle a CSS class on and off
 * 
 * @param {String} className
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleClass = function (className)
{
    if (this.HasClass(className))
    {
        this.RemoveClass(className);
    }
    else
    {
        this.AddClass(className);
    }
    return this;
};

/**
 * @function ToggleCss
 * Toggle a css property between two values.
 * 
 * @param {String} cssProperty
 * @param {String|Number} value1
 * @param {String|Number} value2
 * @param {Number} [animationSpeed]
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleCss = function (cssProperty, value1, value2, animationSpeed)
{
    var curVal = this.GetCss(cssProperty);
    var testPattern = /([0-9]+)([A-Za-z]*)/g;
    if (curVal === value1)
    {
        var value2Parts = testPattern.exec(value2);
        if (IsDefined(animationSpeed) && animationSpeed !== null)
        {
            this.AnimateProperty(cssProperty, value2Parts[1], value2Parts.length > 2 ? value2Parts[2] : null, animationSpeed);
        } else {
            this.SetCss(cssProperty, value2);
        }

    } else {
        var value1Parts = testPattern.exec(value1);
        if (IsDefined(animationSpeed) && animationSpeed !== null)
        {
            this.AnimateProperty(cssProperty, value1Parts[1], value1Parts.length > 2 ? value1Parts[2] : null, animationSpeed);
        } else {
            this.SetCss(cssProperty, value1);
        }
    }
    return this;
};

/**
 * @function ToggleDisplay
 * Toggle this element's display style between whatever it currently is and "none"
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleDisplay = function ()
{
    if (!this.IsShowing())
    {
        this.Show();
        }
    else
    {
        this.Hide();
    }
    return this;
};

/**
 * @function ToggleHidden
 * Toggle this elements hidden attribute
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleHidden = function ()
{
    if (this.HasAttribute("hidden"))
    {
        this.RemoveAttribute("hidden");
    } else {
        this.SetAttribute("hidden", true);
    }
    return this;
}

/**
 * @function ToggleEnabled
 * Toggle this elements disabled attribute
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleEnabled = function ()
{
    if (this.HasAttribute("disabled"))
    {
        this.Enable();
    } else {
        this.Disable();
    }
    return this;
}

/**
 * @function SetCss
 * @param {String} property
 * @param {String|Number} value
 * @param {Number} animationSpeed
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetCss = function (property, value, animationSpeed)
{
    if (value === undefined)
        value = null;

    var testPattern = /([0-9]+)([A-Za-z]*)/g;
    property = normalizeCssProperty(property);

    if (IsDefined(animationSpeed) && animationSpeed !== null)
    {
        var valueParts = testPattern.exec(value);

        this.AnimateProperty(property, valueParts[1], valueParts.length > 2 ? valueParts[2] : null, animationSpeed);
    } else {
        this.style[property] = value;
    }
    return this;

};

Element.prototype.GetClassCss = function(property, ignoreValue, reloadCssRules) {
    if (this._cachedCssRuleSelectors == null || reloadCssRules)
    {
        var rule;
        var sheet;
        var sheetIdx;
        var ruleIdx;
        var sheets = document.styleSheets;
        var selectors;
        var selectorIdx;
        
        this._cachedCssRuleSelectors = [];
        this._cachedCssRuleStyles = [];
        for (sheetIdx = 0; sheetIdx < sheets.length; sheetIdx++) {
            sheet = sheets[sheetIdx];
            try
            {
                if(sheet.cssRules)
                {
                    for (ruleIdx = 0; ruleIdx < sheet.cssRules.length; ruleIdx++) {
                        rule = sheet.cssRules[ruleIdx];
                        if (rule.selectorText)
                        {
                            selectors = rule.selectorText.split(',');
                            for (selectorIdx = 0; selectorIdx < selectors.length; selectorIdx++)
                            {
                                this._cachedCssRuleSelectors[this._cachedCssRuleSelectors.length] = selectors[selectorIdx];
                                this._cachedCssRuleStyles[this._cachedCssRuleStyles.length] = rule.style;
                            }
                        }
                    }
                }
            }
            catch(ex)
            {
                if(ex.name !== 'SecurityError')
                {
                    throw e;
                }
            }
        }
    }
    
    var classes = this.className.split(" ");
    var selector;
    var ruleIdx;
    var ruleStyle;
    var ruleValue;
    
    for (var classIdx = 0; classIdx < classes.length; classIdx++)
    {
        selector = "."+classes[classIdx];
        ruleIdx = this._cachedCssRuleSelectors.indexOf(selector);
        if (ruleIdx !== -1) {
            ruleStyle = this._cachedCssRuleStyles[ruleIdx];
            ruleValue = ruleStyle[property];
            if (ruleValue != "" && ruleValue != ignoreValue)
            {
                return ruleValue;
            }
        }
    }
    return null;
};

/**
 * @function GetCss
 * Get the current value of the specified CSS property.
 * 
 * @param {String} property
 * @param {Boolean} [useComputed=false] Instead of using the value of the style attribute, compute the actual current value.
 * 
 * @returns {String|Number|null}
 */
Element.prototype.GetCss = function (property, useComputed)
{
    var style;
    property = normalizeCssProperty(property);

    if (!IsDefined(useComputed))
    {
        useComputed = false;
    }
    if (useComputed)
    {
        style = this.GetCurrentStyle();
    } else {
        style = this.style;
    }
    if (IsDefined(style[property]) && style[property] !== null)
    {
        var returnValue = style[property];
        if (IsNumeric(returnValue))
        {
            returnValue = parseFloat(returnValue);
        }
        return returnValue;
    } else {
        return null;
    }
};

/**
 * @function GetStyle
 * Get the current value of the specified style.
 * 
 * @param {String} cssProperty
 * 
 * @returns {String|Number|null}
 */
Element.prototype.GetStyle = function (cssProperty) {
    var strValue = "";
    if (document.defaultView && document.defaultView.getComputedStyle) {
        strValue = document.defaultView.getComputedStyle(this, "").getPropertyValue(cssProperty);
    }
    else if (this.currentStyle) {
        cssProperty = cssProperty.replace(/\-(\w)/g, function (strMatch, p1) {
            return p1.toUpperCase();
        });
        strValue = this.currentStyle[cssProperty];
    }
    return strValue;
};

/**
 * @function GetCurrentStyle
 * Get the current style for this element.
 * 
 * @returns {String|Number|null}
 */
Element.prototype.GetCurrentStyle = function () {
    var returnStyle;
    if (this.currentStyle)
    {
        returnStyle = this.currentStyle;
    }
    else if (document.defaultView && document.defaultView.getComputedStyle) {
        returnStyle = document.defaultView.getComputedStyle(this, "");
    }
    else if (window.getComputedStyle)
    {
        returnStyle = window.getComputedStyle(this, "");
    }
    return returnStyle;
};

/**
 * @function Pulse
 * Display a "pulse" visual effect on the element.
 * 
 * @param {Number} [speed=5]
 * @param {String} [color=this.style.backgroundColor]
 * @param {Function} [callbackFunction]
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Pulse = function (speed, color, callbackFunction)
{
    if (!IsDefined(speed) || speed === null)
    {
        speed = 20;
    }
    if (!IsDefined(color) || color === null)
    {
        color = this.style.backgroundColor;
    }
    var oldColor = this.style.backgroundColor;
    this.style.backgroundColor = color;
    this.SetOpacity(0);
            this.FadeIn(speed,
                function ()
                {
                    this.style.backgroundColor = oldColor;
                    if (IsDefined(callbackFunction) && callbackFunction !== null)
                    {
                        callbackFunction();
                    }
                }.Bind(this));
    return this;
};

/**
 * @function GetAllTextNodes
 * Get all text nodes descendant of this element.
 * 
 * @returns {Array}
 */
Element.prototype.GetAllTextNodes = function ()
{
    var n, a = [], walk = document.createTreeWalker(this, NodeFilter.SHOW_TEXT, null, false);
    while (n = walk.nextNode())
        a.push(n);
    return a;
};

/**
 * @function StringReplaceTextNodes
 * Replace text within this element
 * 
 * @param {String} search the text to replace
 * @param {String} replace the replacement text
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.StringReplaceTextNodes = function (search, replace)
{
    var n;
    var walk = document.createTreeWalker(this, NodeFilter.SHOW_TEXT, null, false);
    while (n = walk.nextNode())
    {
        if (IsObject(replace))
        {
            if (n.data == search)
            {
                n.InsertBeforeMe(replace);
                n.data = "";
            }
        } else {
            n.data = n.data.replace(search, replace);
        }
    }

    return this;
};


/**
 * @function StringReplaceAttributeValues
 * Replace attribute values within this element.
 * For templates of img or input tags, you can use the data-attribute-value and data-attribute-src instead of value/src
 * that they can be handled correctly and don't load images before src is ready.
 * 
 * @param {String} searchRegex regex of the part of each attribute value to replace
 * @param {String} replace the replacement text
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.StringReplaceAttributeValues = function (searchRegex, replace)
{
    var n;
    var idx, att;
    var newAttName;
    var attValue;
    var origAttValue;

    var needsSrcUpdate = false;
    var regexp = new RegExp(searchRegex, "gi");
    //first do my attributes
    for (idx = this.attributes.length - 1; idx >= 0; idx--)
    {
        att = this.attributes[idx];
        origAttValue = this.GetAttribute(att.name);
        attValue = origAttValue.replace(regexp, replace);
        if (att.name == "value")
        {
            if (att.name.match(regexp) != null)
            {
                newAttName = att.name.replace(regexp, replace);
                this.RemoveAttribute(att.name);
                this.SetAttribute(newAttName, attValue);
            } else if (origAttValue != attValue) {
                this.Set_Value(attValue);
            }
        } 
        else if (att.name == "id")
        {
            if (att.name.match(regexp) != null)
            {
                newAttName = att.name.replace(regexp, replace);
                this.RemoveAttribute(att.name);
                this.SetAttribute(newAttName, attValue);
            } else if (origAttValue != attValue) {
                this.id = attValue;
            }
        }
        else
        {
            if (att.name.match(regexp) != null)
            {
                newAttName = att.name.replace(regexp, replace);
                this.RemoveAttribute(att.name);
                this.SetAttribute(newAttName, attValue);
            } else if (att.name == "data-attribute-value") {
                this.RemoveAttribute(att.name);
                this.SetValue(attValue);
            } else if (origAttValue != attValue) {
                newAttName = att.name;
                this.SetAttribute(newAttName, attValue);
            }

            if (att.name == "data-attribute-src")
            {
                needsSrcUpdate = true;
            }
        }
    }

    if (needsSrcUpdate)
    {
        //convert any data-attribute-src to actual src now (deferred until now to prevent multiple image loads if image url has multiple variables
        for (idx = this.attributes.length - 1; idx >= 0 ; idx--)
        {
            att = this.attributes[idx];
            if (att.name == "data-attribute-src")
            {
                var attSrc = this.GetAttribute(att.name);
                this.RemoveAttribute(att.name);
                this.SetAttribute("src", attSrc);
            }
        }
    }

    //now do my decendant nodes
    var walk = document.createTreeWalker(this, NodeFilter.SHOW_ELEMENT, null, false);
    while (n = walk.nextNode())
    {
        needsSrcUpdate = false;
        for (idx = n.attributes.length - 1; idx >= 0; idx--)
        {
            att = n.attributes[idx];
            origAttValue = n.GetAttribute(att.name);
            attValue = origAttValue.replace(regexp, replace);
            if (att.name == "value")
            {
                if (att.name.match(regexp) != null)
                {
                    newAttName = att.name.replace(regexp, replace);
                    n.RemoveAttribute(att.name);
                    n.SetAttribute(newAttName, attValue);
                } else if (origAttValue != attValue) {
                    n.SetValue(attValue);
                }
            } 
            else if (att.name == "id")
            {
                if (att.name.match(regexp) != null)
                {
                    newAttName = att.name.replace(regexp, replace);
                    n.RemoveAttribute(att.name);
                    n.SetAttribute(newAttName, attValue);
                } else if (origAttValue != attValue) {
                    n.id = attValue;
                }
            }
            else
            {
                if (att.name.match(regexp) != null)
                {
                    newAttName = att.name.replace(regexp, replace);
                    n.RemoveAttribute(att.name);
                    n.SetAttribute(newAttName, attValue);
                } else if (att.name == "data-attribute-value") {
                    n.RemoveAttribute(att.name);
                    n.SetValue(attValue);
                } else if (origAttValue != attValue) {
                    newAttName = att.name;
                    n.SetAttribute(newAttName, attValue);
                }
            }
            if (att.name == "data-attribute-src")
            {
                needsSrcUpdate = true;
            }
        }

        if (needsSrcUpdate)
        {
            //convert any data-attribute-src to actual src now (deferred until now to prevent multiple image loads if image url has multiple variables
            for (idx = n.attributes.length - 1; idx >= 0 ; idx--)
            {
                att = n.attributes[idx];
                if (att.name == "data-attribute-src")
                {
                    var attSrc = n.GetAttribute(att.name);
                    n.RemoveAttribute(att.name);
                    n.SetAttribute("src", attSrc);
                }
            }
        }
    }
    return this;
};

/**
 * @function Clone Clone this element.
 * 
 * @param {Boolean} [deepClone=true]
 * @param {Boolean} allowSameId default = false
 * @param {Boolean} cloneAllStyles default = false
 * @param {Boolean} ignoreInfrequentStyles default = true
 * @param {Boolean} ignoreNonSizingStyles default = false
 *
 * @returns {Element} The cloned element.
 */
Element.prototype.Clone = function (deepClone, allowSameId, cloneAllStyles, ignoreInfrequentStyles, ignoreNonSizingStyles)
{
    if (!IsDefined(deepClone))
    {
        deepClone = true;
    }
    if (!IsDefined(allowSameId))
    {
        allowSameId = false;
    }
    if (!IsDefined(cloneAllStyles))
    {
        cloneAllStyles = false;
    }
    if (!IsDefined(ignoreInfrequentStyles))
    {
        ignoreInfrequentStyles = true;
    }
    if (!IsDefined(ignoreNonSizingStyles))
    {
        ignoreNonSizingStyles = false;
    }
    var clonedNode = this.cloneNode(deepClone);
    if (!allowSameId)
    {
        //if there is css based on the id selector that affects the look/layout of the element then the clone will not look identical
        clonedNode.id = clonedNode.id + "-clone-" + String.CreateUUID();
    }
    if (cloneAllStyles)
    {
        var ignoreStyles = null;
        if (ignoreInfrequentStyles && ignoreNonSizingStyles)
        {
            ignoreStyles = Element._infrequentStyles.concat(Element._nonSizingStyles);
        } else if (ignoreInfrequentStyles) {
            ignoreStyles = Element._infrequentStyles;
        } else if (ignoreNonSizingStyles) {
            ignoreStyles = Element._nonSizingStyles;
        }
        this.ApplyAllStylesToElement(clonedNode, ignoreStyles, deepClone);
    }
    return clonedNode;
};

/**
 * @function ApplyAllStylesToElement
 * Apply all CSS styles from this element to the passed element.
 * This includes all CSS styles, except for those specified in ignoreStyles, even if it has not been set explicitly.
 *
 * @param {Element} element The element to apply the styles to
 * @param {Array} ignoreStyles Styles to ignore.
 * If you do not specify anything for this argument, a default list of styles will be ignored (defined in Element._infrequentStyles).
 * If you don't want to ignore any styles, pass null.
 * @param {Boolean} recursive Set to true to apply child styles as well
 * 
 * @returns {Element} Returns this element for easy chained method calls
 */
Element.prototype.ApplyAllStylesToElement = function(element, ignoreStyles, recursive)
{
    if (!IsDefined(ignoreStyles))
    {
        ignoreStyles = Element._infrequentStyles;
    }
    else if (ignoreStyles == null)
    {
        ignoreStyles = [];
    }
    if (!IsDefined(recursive))
    {
        recursive = false;
    }
    var allStyles = IsDefined(this.currentStyle)?this.currentStyle:document.defaultView.getComputedStyle(this);
    var styleName;
    var ignoreStyle;
    var wildLoc;

    for (var ignoreIdx = 0; ignoreIdx < ignoreStyles.length; ignoreIdx++)
    {
        ignoreStyle = ignoreStyles[ignoreIdx];
        wildLoc = ignoreStyle.indexOf("*");
        var origTestStyleDeleteme = ignoreStyle;
        if (wildLoc == 0)
        {
            ignoreStyles[ignoreIdx] = null;
            ignoreStyle = ignoreStyle.substring(1);
            wildLoc = ignoreStyle.length;
            for (var styleIdx = 0; styleIdx < allStyles.length; styleIdx++)
            {
                if (allStyles[styleIdx].length >= wildLoc && (allStyles[styleIdx].substring(allStyles[styleIdx].length - wildLoc) == ignoreStyle))
                {
                    //ignore this
                    if (ignoreStyles[ignoreIdx] == null)
                    {
                        ignoreStyles[ignoreIdx] = allStyles[styleIdx];
                    } else {
                        ignoreStyles[ignoreStyles.length] = allStyles[styleIdx];
                    }
                }
            }
        }
        else if (wildLoc == ignoreStyle.length - 1)
        {
            ignoreStyles[ignoreIdx] = null;
            ignoreStyle = ignoreStyle.substring(0, wildLoc);
            for (var styleIdx = 0; styleIdx < allStyles.length; styleIdx++)
            {
                if (allStyles[styleIdx].length >= wildLoc && (allStyles[styleIdx].substring(0, wildLoc) == ignoreStyle))
                {
                    //ignore this
                    if (ignoreStyles[ignoreIdx] == null)
                    {
                        ignoreStyles[ignoreIdx] = allStyles[styleIdx];
                    } else {
                        ignoreStyles[ignoreStyles.length] = allStyles[styleIdx];
                    }
                }
            }
        }
        if (ignoreStyles[ignoreIdx] == null)
        {
            ignoreStyles[ignoreIdx] = origTestStyleDeleteme;
        }
    }
    for (var styleIdx = 0; styleIdx < allStyles.length; styleIdx++)
    {
        styleName = allStyles[styleIdx];
        if (ignoreStyles.indexOf(styleName) == -1)
        {
            element.SetCss(styleName, allStyles[styleName]);
        }
    }
    if (recursive)
    {
        var allElements = this.children;
        for (var elemIdx =0; elemIdx < allElements.length; elemIdx++)
        {
            var childElem = allElements[elemIdx];
            childElem.ApplyAllStylesToElement(element.children[elemIdx], ignoreStyles, recursive);
        }
    }
};

/**
 * @function GetAttribute
 * Get an attribute's current value
 * 
 * @param {String} attributeName
 * 
 * @returns {String|Number|null}
 */
Element.prototype.GetAttribute = function (attributeName)
{
    return this.getAttribute(attributeName);
};

/**
 * @function HasAttribute
 * Determine whether or not this element has the specified attribute.
 * 
 * @param {String} attribute
 * 
 * @returns {Boolean}
 */
Element.prototype.HasAttribute = function (attribute)
{
    return this.hasAttribute(attribute);
};

/**
 * @function ToggleAttribute
 * Toggle an attribute between two values.
 * 
 * @param {String} attributeName
 * @param {String|Number|null} value1
 * @param {String|Number|null} value2
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ToggleAttribute = function (attributeName, value1, value2)
{
    if (IsDefined(value1) && IdDefined(value2))
    {
        if (this.GetAttribute(attributeName) == value1)
        {
            this.SetAttribute(attributeName, value2);
        } else {
            this.SetAttribute(attributeName, value1);
        }
    }
    else if (this.HasAttribute(attributeName))
    {
        this.RemoveAttribute(attributeName);
    } else {
        this.SetAttribute(attributeName);
    }
    return this;
};

/**
 * @function SetAttribute
 * Set the value for an attribute
 * 
 * @param {String} attributeName
 * @param {String|Number|null} value
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.SetAttribute = function (attributeName, value)
{
    if (attributeName != "")
    {
        if (!IsDefined(this.attributes[attributeName]) && value !== false)
        {
            var attribute = document.createAttribute(attributeName);
            this.setAttributeNode(attribute);
        }
        if (IsDefined(this.attributes[attributeName]) && value === false)
        {
            this.RemoveAttribute(attributeName);
        }
        else if (IsDefined(value) && value !== true && value !== false)
        {
            this.attributes[attributeName].nodeValue = value;
        }
    }
    return this;
};

/**
 * @function RemoveAttribute
 * Remove an attribute from the element.
 * 
 * @param {String} attributeName
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.RemoveAttribute = function (attributeName)
{
    this.removeAttribute(attributeName);
    return this;
};

/**
 * @function AddLoadHandler
 * Add handler to be called when this element is loaded.
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {DblEj.EventHandling.EventHandler}
 */
Element.prototype.AddLoadHandler = function (handlerFunction, targetSelector)
{
    return DblEj.EventHandling.Events.AddHandler(this, "load", handlerFunction, null, targetSelector);
};

/**
 * @function RemoveHandler
 * Remove an event handler.
 * 
 * @param {String} uid the event handler's unique id
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.RemoveHandler = function (uid)
{
    DblEj.EventHandling.Events.RemoveHandler(uid);
    return this;
};

Element.prototype.RemoveAllHandlers = function()
{
    DblEj.EventHandling.Events.RemoveAllHandlersFromObject(this);
    return this;
};

/**
 * @function AddClickHandler
 * Add a onclick handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddClickHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "click", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddFocusHandler
 * Add a onfocus handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddFocusHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "focus", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddBlurHandler
 * Add a onblur handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddBlurHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "blur", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddChangeHandler
 * Add a onchange handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddChangeHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "change", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddMouseDownHandler
 * Add a onmousedown handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddMouseDownHandler = function (handlerFunction, targetSelector)
{
    if (IsDefined(this["ontouchstart"]) && !IsDefined(this["onmousedown"]))
    {
        DblEj.EventHandling.Events.AddHandler(this, "touchstart", handlerFunction, null, targetSelector);
    } else {
    DblEj.EventHandling.Events.AddHandler(this, "mousedown", handlerFunction, null, targetSelector);
    }
    return this;
};

/**
 * @function AddMouseUpHandler
 * Add a onmouseup handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddMouseUpHandler = function (handlerFunction, targetSelector)
{
    if (IsDefined(this["ontouchstart"]) && !IsDefined(this["onmouseup"]))
    {
        DblEj.EventHandling.Events.AddHandler(this, "touchend", handlerFunction, null, targetSelector);
    } else {
    DblEj.EventHandling.Events.AddHandler(this, "mouseup", handlerFunction, null, targetSelector);
    }
    return this;
};

/**
 * @function AddMouseInHandler
 * Add a onmouseover handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * @param {Boolean} duringCapture
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddMouseInHandler = function (handlerFunction, targetSelector, duringCapture)
{
    DblEj.EventHandling.Events.AddHandler(this, "mouseover", handlerFunction, duringCapture, targetSelector);
    return this;
};

/**
 * @function AddMouseOutHandler
 * Add a mouseout handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * @param {Boolean} duringCapture
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddMouseOutHandler = function (handlerFunction, targetSelector, duringCapture)
{
    DblEj.EventHandling.Events.AddHandler(this, "mouseout", handlerFunction, duringCapture, targetSelector);
    return this;
};

/**
 * @function AddMouseMoveHandler
 * Add a onmousemove handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddMouseMoveHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "mousemove", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddHandler
 * Add an event handler
 * 
 * @param {String} eventName
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * @param {Boolean} duringCapture
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddHandler = function (eventName, handlerFunction, targetSelector, duringCapture)
{
    DblEj.EventHandling.Events.AddHandler(this, eventName, handlerFunction, duringCapture, targetSelector);
    return this;
};

/**
 * @function AddKeyDownHandler
 * Add a onkeydown handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddKeyDownHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "keydown", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddKeyUpHandler
 * Add a onkeyup handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddKeyUpHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "keyup", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function AddKeyPressHandler
 * Add a onkeypress handler
 * 
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AddKeyPressHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "keypress", handlerFunction, null, targetSelector);
    return this;
};


Element.prototype.AddDragStartHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "dragstart", handlerFunction, null, targetSelector);
    return this;
};
Element.prototype.AddDragOverHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "dragover", handlerFunction, null, targetSelector);
    return this;
};
Element.prototype.AddDropHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "drop", handlerFunction, null, targetSelector);
    return this;
};


/**
 * @function ShowDialog
 * Show this element in a popup dialog
 * 
 * @param {String} [title]
 * @param {Function} [onConfirmCallback]
 * @param {Number} [width]
 * @param {Number} [height]
 * @param {Number} [x]
 * @param {Number} [y]
 * @param {Function} [onCancelCallback]
 * @param {String} [confirmButtonCaption]
 * @param {String} [cancelButtonCaption]
 * @param {Boolean} [autohide]
 * 
 * @returns {DblEj.UI.Dialog}
 */
Element.prototype.ShowDialog = function (title, onConfirmCallback, width, height, x, y,
    onCancelCallback, confirmButtonCaption, cancelButtonCaption, autohide, zIndexOrMaxZIndexSelector)
{
    var dialog;
    if (this.OuterDialog == null)
    {
        dialog =
            DblEj.UI.Dialog.ShowDialog(title, this, onConfirmCallback, width, height, x, y, onCancelCallback, confirmButtonCaption, cancelButtonCaption, autohide, zIndexOrMaxZIndexSelector);
        this.OuterDialog = dialog;
        this.hidden = false;
    } else {
        dialog = this.OuterDialog;
        this.Pulse();
        dialog.Show();
    }
    return dialog;
};

/**
 * @function CloseDialog
 * If this element was showing in a dialog using ShowDialog, then this will close the dialog
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.CloseDialog = function (cancelled)
{
    if (IsDefined(this.OuterDialog) && this.OuterDialog !== null && IsDefined(this.OuterDialog.Close))
    {
        this.OuterDialog.Close(cancelled);
        this.OuterDialog = null;
    }
    return this;
};

/**
 * @function ShowLightbox
 * Show this element as a modal lightbox
 * 
 * @param {Number} [fadeSpeed=0]
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ShowLightbox = function (fadeSpeed, hideCallback)
{
    DblEj.UI.Lightbox.ShowLightbox(this, fadeSpeed, hideCallback);
    return this;
};

/**
 * @function Trigger
 * Simulate an event on this element
 * 
 * @param {String} eventName
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Trigger = function (eventName)
{
    DblEj.EventHandling.Events.RaiseEvent(this, eventName);
    return this;
};
/**
 * @function RaiseEvent
 * Alias of Trigger().  Simulate an event on this element.
 *
 * @param {String} eventName
 *
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.RaiseEvent = function (eventName)
{
    return this.Trigger(eventName);
};
/**
 * @function Append
 * Append the html to the end of this element's contents
 * 
 * @param {String} html
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Append = function (html)
{
    if (IsDefined(this.insertAdjacentHTML))
    {
        this.insertAdjacentHTML("beforeend", html);
    } else {
        var dummyDiv = document.createElement("div");
        var frag = document.createDocumentFragment();
        var moveElem;
        dummyDiv.innerHTML = html;
        while (moveElem = dummyDiv.firstChild)
        {
            frag.appendChild(moveElem);
        }
        this.appendChild(frag);
    }
    return this;
};

/**
 * @function Prepend
 * Prepend the html to the beginning of this element's contents
 * 
 * @param {String} html
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.Prepend = function (html)
{
    if (IsDefined(this.insertAdjacentHTML))
    {
        this.insertAdjacentHTML("afterbegin", html);
    } else {
        var dummyDiv = document.createElement("div");
        var frag = document.createDocumentFragment();
        var moveElem;
        dummyDiv.innerHTML = html;
        while (moveElem = dummyDiv.firstChild)
        {
            frag.appendChild(moveElem);
        }
        this.insertBefore(frag, this.childNodes[0]);
    }
    return this;
};

/**
 * @function PrependChild
 * Prepend the childElements to the beginning of this element's contents.
 * 
 * @param {Array|NodeList} childElements
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.PrependChild = function (childElements)
{
    var children = this.GetChildren();
    if (children.length > 0)
    {
        var firstChild = children[0];
        if (IsArray(childElements))
        {
            Iteratize(childElements);
            childElements.OnEach(
                function (childElement)
                {
                    this.insertBefore(childElement, firstChild);
                }.Bind(this));
        } else {
            this.insertBefore(childElements, firstChild);
        }
    } else {
        this.AppendChild(childElements);
    }

    return this;
};

/**
 * @function AppendChild
 * Append the childElements to the end of this element's contents.
 * 
 * @param {Array|NodeList|HTMLElement} childElements
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.AppendChild = function (childElements)
{
    if (IsArray(childElements))
    {
        Iteratize(childElements);
        childElements.OnEach(
            function (childElement)
            {
                this.appendChild(childElement);
            }.Bind(this));
    } else {
        this.appendChild(childElements);
    }
    return this;
};

/**
 * @function ReplaceAllChildren
 * Replace the contents of this element with the specified elements (or nothing).
 * 
 * @param {Array|NodeList|null} childElements
 * 
 * @returns {Element} Returns this element for chained method calls.
 */
Element.prototype.ReplaceAllChildren = function (childElements)
{
    this.RemoveAllChildren();
    if (!IsNullOrUndefined(childElements))
    {
        this.AppendChild(childElements);
    }
    return this;
};

/**
 * @function GetData
 * Get the specified data attached to this element.
 * @param {String} dataname
 * @returns {String|Number}
 */
Element.prototype.GetData = function (dataname)
{
    if (IsDefined(this.dataset))
    {
        if (IsDefined(this.dataset[normalizeCssProperty(dataname)]))
        {
            var dataVal = this.dataset[normalizeCssProperty(dataname)];
            if (dataVal === 'null')
            {
                dataVal = null;
            }
            return dataVal;
        } else {
            return null;
        }
    } else {
        if (IsDefined(this.getAttribute("data-"+dataname)))
        {
            var dataVal = this.getAttribute("data-"+dataname);
            if (dataVal == 'null')
            {
                dataVal = null;
            }
            return dataVal;
        } else {
            return null;
        }
    }
};

/**
 * @function SetData
 * Attach the specified data to this element.
 *
 * @param {String} dataname
 * @param {String|Number} datavalue
 * @returns {Element} Returns this object for chained method calls.
 */
Element.prototype.SetData = function (dataname, datavalue)
{
    this.dataset[normalizeCssProperty(dataname)] = datavalue;
    return this;
};

/**
 * Check if this element has the specified data attribute
 * @param {String} dataname
 * @returns {Boolean}
 */
Element.prototype.HasData = function (dataname)
{
    return this.GetData(dataname) !== null;
};

/**
 * @function InsertAfterMe
 * Insert an element after this element.
 *
 * @param {Element} insertElement The element to insert.
 * @returns {Element} Returns this object for chained method calls.
 */
Element.prototype.InsertAfterMe = function (insertElement)
{
    this.GetParent()
        .insertBefore(insertElement, this.nextElementSibling);
    return this;
};

/**
 * Show this element in full screen mode.
 *
 * @returns {Element.prototype} This element for easy chained method calls
 */
Element.prototype.GotoFullScreen = function()
{
    // Supports most browsers and their versions.
    var requestMethod = this.requestFullScreen || this.webkitRequestFullScreen || this.mozRequestFullScreen || this.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(this);
    } else {
        throw "This browser or client does not support full screen elements";
    }
    return this;
};

/**
 * @class HtmlElement
 */
if (typeof HTMLElement.prototype.matches == 'undefined' || typeof HTMLElement.prototype.matches == undefined)
{
    if (typeof HTMLElement.prototype.msMatchesSelector != 'undefined' && typeof HTMLElement.prototype.msMatchesSelector !=
        undefined)
    {
        HTMLElement.prototype.matches = HTMLElement.prototype.msMatchesSelector;
    }
    else if (typeof HTMLElement.prototype.mozMatchesSelector != 'undefined' && typeof HTMLElement.prototype.mozMatchesSelector !=
        undefined)
    {
        HTMLElement.prototype.matches = HTMLElement.prototype.mozMatchesSelector;
    }
    else if (typeof HTMLElement.prototype.webkitMatchesSelector != 'undefined' &&
        typeof HTMLElement.prototype.webkitMatchesSelector != undefined)
    {
        HTMLElement.prototype.matches = HTMLElement.prototype.webkitMatchesSelector;
    }
    else if (typeof HTMLElement.prototype.oMatchesSelector != 'undefined' && typeof HTMLElement.prototype.oMatchesSelector !=
        undefined)
    {
        HTMLElement.prototype.matches = HTMLElement.prototype.oMatchesSelector;
    }
}
