/**
 * @namespace Default
 * @class HTMLTextAreaElement
 */

/**
 * @function Set_Value
 * @param {String|Number} newValue
 *
 * @returns {HTMLInputElement} Returns this element for chained method calls.
 */
HTMLTextAreaElement.prototype.Set_Value = function (newValue)
{
    if (newValue === null)
    {
        newValue = '';
    }
    this.value = newValue;
    return this;
};

/**
 * @function SetValue
 * @param {String|Number} newValue
 *
 * @returns {HTMLInputElement} Returns this element for chained method calls.
 */
HTMLTextAreaElement.prototype.SetValue = function (newValue)
{
    if (newValue === null)
    {
        newValue = '';
    }
    return this.Set_Value(newValue);
};

/**
 * @function Get_Value
 * Get the current value for this input element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLTextAreaElement.prototype.Get_Value = function ()
{
    return this.value;
};


/**
 * @function GetValue
 * Get the current value for this input element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLTextAreaElement.prototype.GetValue = function ()
{
    return this.Get_Value();
};

/**
 * @function GetValueOrNull
 * Get the current value for this input element, or null if the value is empty.
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLTextAreaElement.prototype.GetValueOrNull = function ()
{
    return this.value != "" ? this.value : null;
};

/**
 * @function SetValidationError
 *
 * @param {String} errorString
 * @returns {HTMLTextAreaElement} Returns this element for chained method calls.
 */
HTMLTextAreaElement.prototype.SetValidationError = function (errorString, showIndicator)
{
    this.setCustomValidity(errorString);
    var safari = (navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) > -1 && ((navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) != "");
    if (safari && showIndicator)
    {
        var messageElem = $q(".ValidationErrorMessage");
        if (messageElem == null)
        {
            messageElem = $create("<div class='ValidationErrorMessage'>"+errorString+"</div>");
            messageElem
                .SetCss("position", "absolute")
                .SetCss("padding", "1em")
                .SetCss("background-color", "#ffffff")
                .SetCss("opacity", ".9")
                .SetCss("border-style", "solid")
                .SetCss("border-width", "1px")
                .SetCss("border-color", "#afafaf")
                .SetCss("font-size", "70%")
                .SetCss("z-index", "99999");

            if (this.GetParent().GetCss("position", true) == "relative" || this.GetParent().GetCss("position", true) == "absolute")
            {
                messageElem
                    .SetCss("top", (this.GetRelativePosition().Get_Y() + this.GetAbsoluteSize().Get_Y() + 2)+"px")
                    .SetCss("left", this.GetRelativePosition().Get_X()+"px")
            } else {
                messageElem
                    .SetCss("top", (this.GetAbsolutePosition().Get_Y() + this.GetAbsoluteSize().Get_Y() + 2)+"px")
                    .SetCss("left", this.GetAbsolutePosition().Get_X()+"px")
            }

            if (!IsDefined(this._hasValidationFocusHandler) || !this._hasValidationFocusHandler)
            {
                this._hasValidationFocusHandler= true;
                this.AddFocusHandler(function(event)
                {
                    var messageElem = $q(".ValidationErrorMessage", this.GetParent());
                    if (messageElem != null)
                    {
                        messageElem.Hide();
                    }
                });
            }
        }
        if (errorString != "")
        {
            this.GetParent().AppendChild(messageElem);
            messageElem.Show();
        }
    }
    return this;
};

/**
 * @function ClearValidationError
 *
 * @returns {HTMLTextAreaElement} Returns this element for chained method calls.
 */
HTMLTextAreaElement.prototype.ClearValidationError = function ()
{
    this.setCustomValidity("");
    var safari = (navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) > -1 && ((navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) != "");
    if (safari)
    {
        var messageElem = $q(".ValidationErrorMessage", this.GetParent());
        if (messageElem != null)
        {
            messageElem.Hide();
        }
    }
    return this;
};

/**
 * @function IsValid
 *
 * @returns {Boolean}
 */
HTMLTextAreaElement.prototype.IsValid = function ()
{
    return this.validity.valid;
};

HTMLTextAreaElement.prototype.SelectAll = function ()
{
    this.select();
    return this;
};
HTMLTextAreaElement.prototype.SelectAllText = function()
{
    return this.SelectAll();
};

/**
 * Copies the current value of this textarea to the user's clipboard.
 * Only supported on late-2015 version browsers and newer.
 * Older browsers show them a popup that allows them to copy.
 * @returns {boolean} true on success or false otherwise
 */
HTMLTextAreaElement.prototype.CopyValueToClipboard = function()
{
    var success = false;
    if (IsDefined(document.execCommand))
    {
        this.SelectAll();
        try
        {
            success = document.execCommand('copy');
        }
        catch (err)
        {

        }
    }
    if (!success)
    {
        window.prompt("Copy to clipboard: Ctrl+C (Cmd+C on Mac), Enter", this.Get_Value());
        success = true;
    }
    if (IsDefined(document.selection) && IsDefined(document.selection.empty))
    {
        document.selection.empty();
    } else {
        window.getSelection().removeAllRanges();
    }
    return success;
};

/**
 * If set to true, then the text area will automatically resize itself (height only) to fit its contents
 * @param {boolean} autoHeight
 * @param {string} minHeight CSS min height, including the unit suffix.  Default: "50px"
 * @param {string} maxHeight CSS max height, including the unit suffix.  Default: "300px"
 * @returns {undefined}
 */
HTMLTextAreaElement.prototype.Set_AutoHeight = function(autoHeight, minHeight, maxHeight)
{
    if (!IsDefined(minHeight))
    {
        minHeight = "50px";
    }
    if (!IsDefined(maxHeight))
    {
        maxHeight = "300px";
    }
    this.SetData("autoheight", autoHeight?"1":"0");
    if (autoHeight)
    {
        this.SetCss("height", this.scrollHeight + "px");
        this.SetCss("resize", "none");
        this.SetCss("overflow", "hidden");
        this.SetCss("min-height", minHeight);
        this.SetCss("max-height", maxHeight);
        this.FitToContents();
    } else {
        this.SetCss("height", "auto");
        this.SetCss("resize", "both");
        this.SetCss("overflow", "visible");
        this.SetCss("min-height", "none");
        this.SetCss("max-height", "none");
    }


    if (this.GetData("autoheightHandlersAdded") != "1")
    {
        this.AddKeyUpHandler(
            function()
            {
                if (this.GetData("autoheight") == "1")
                {
                    this.FitToContents();
                }
            }.Bind(this));

        this.AddChangeHandler(
            function()
            {
                if (this.GetData("autoheight") == "1")
                {
                    this.FitToContents();
                }
            }.Bind(this));
        this.SetData("autoheightHandlersAdded", "1");
    }

};

HTMLTextAreaElement.prototype.FitToContents = function()
{
    if (this.GetData("autoheight") == "1")
    {
        this.SetCss("height", this.scrollHeight + "px");
    }
};