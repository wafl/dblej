/**
 * @namespace Default
 * @class HTMLInputElement
 */

/**
 * @function AddValidatedHandler
 * Add a onvalidate handler to form field
 *
 * @param {Function} handlerFunction
 *
 * @returns {HTMLInputElement.prototype} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.AddValidatedHandler = function (handlerFunction)
{
    this._validatedHandlers[this._validatedHandlers.length] = handlerFunction;
    return this;
};

/**
 * @function Uncheck Uncheck this input if it is a radio or checkbox
 * @param {Boolean} triggerChange If the input is currently checked, should the change event be triggered when we uncheck it?
 * @returns {HTMLInputElement.prototype} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.Uncheck = function (triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }
    if (this.type == "checkbox" || this.type == "radio")
    {
        if (this.checked)
        {
            this.checked = false;
            this.RemoveAttribute("checked");
            if (triggerChange)
            {
                this.Trigger("change");
            }
        }
    } else {
        throw "Uncheck() is only valid if the input type is radio or checkbox";
    }
    return this;
};


/**
 * @function Check Check this input if it is a radio or checkbox
 * @param {Boolean} triggerChange If the input is currently checked, should the change event be triggered when we uncheck it?
 * @returns {HTMLInputElement.prototype} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.Check = function (triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }
    if (this.type == "checkbox" || this.type == "radio")
    {
        if (!this.checked)
        {
            this.checked = true;
            this.SetAttribute("checked", true);
            if (triggerChange)
            {
                this.Trigger("change");
            }
        }
    } else {
        throw "Check() is only valid if the input type is radio or checkbox";
    }
    return this;
};


/**
 * @function Set_Value
 * Set the current value for this input element
 * @param {String|Number|null} newValue
 * @param {Boolean} triggerChange
 * @returns {HTMLInputElement} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.Set_Value = function (newValue, triggerChange)
{
    if (IsNullOrUndefined(triggerChange))
    {
        triggerChange = false;
    }
    if (this.type == "checkbox")
    {
        if (newValue === null)
        {
            newValue = false;
        }
        if (this.checked != newValue)
        {
            this.checked = newValue;
            if (this.checked)
            {
                this.SetAttribute("checked", true);
            } else {
                this.RemoveAttribute("checked");
            }
            if (triggerChange)
            {
                this.Trigger("change");
            }
        }
    } else {
        if (newValue === null)
        {
            newValue = '';
        }
        if (this.value != newValue)
        {
            this.value = newValue;
            this.SetAttribute("value", newValue);
            if (triggerChange)
            {
                this.Trigger("change");
            }
        }
    }
    return this;
};
HTMLInputElement.prototype.ToggleValue = function (value1, value2, triggerChange)
{
    if (this.value == value1)
    {
        this.Set_Value(value2, triggerChange);
    } else {
        this.Set_Value(value1, triggerChange);
    }
    return this;
}

/**
 * @function SetValue
 * Set the current value for this input element
 * @param {String|Number|null} newValue
 * @param {Boolean} triggerChange
 * @returns {HTMLInputElement.prototype} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.SetValue = function (newValue, triggerChange)
{
    return this.Set_Value(newValue, triggerChange);
};


/**
 * @function Get_Value
 * Get the current value for this input element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLInputElement.prototype.Get_Value = function ()
{
    if (this.type == "checkbox")
    {
        return this.checked;
    } else {
        return this.value;
    }
};

/**
 * @function GetValue
 * Get the current value for this input element
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLInputElement.prototype.GetValue = function ()
{
    return this.Get_Value();
};

/**
 * @function GetValueOrNull
 * Get the current value for this input element, or null if the value is empty.
 *
 * @returns {String|Number|null} Returns this element's current value.
 */
HTMLInputElement.prototype.GetValueOrNull = function ()
{
    return this.value != "" ? this.value : null;
};

/**
 * @function SetValidationError
 * Invalidate the input and tell the user why
 *
 * @param {String} errorString
 * @param {Boolean} showIndicator Only applies for safari, other browsers manage themselevs
 * @returns {HTMLInputElement} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.SetValidationError = function (errorString, showIndicator)
{
    this.setCustomValidity(errorString);
    var safari = (navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) > -1 && ((navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) != "");
    if (safari && showIndicator)
    {
        var messageElem = $q(".ValidationErrorMessage");
        if (messageElem == null)
        {
            messageElem = $create("<div class='ValidationErrorMessage'>"+errorString+"</div>");
            messageElem
                .SetCss("position", "absolute")
                .SetCss("padding", "1em")
                .SetCss("background-color", "#ffffff")
                .SetCss("opacity", ".9")
                .SetCss("border-style", "solid")
                .SetCss("border-width", "1px")
                .SetCss("border-color", "#afafaf")
                .SetCss("font-size", "70%")
                .SetCss("z-index", "99999");

            if (this.GetParent().GetCss("position", true) == "relative" || this.GetParent().GetCss("position", true) == "absolute")
            {
                messageElem
                    .SetCss("top", (this.GetRelativePosition().Get_Y() + this.GetAbsoluteSize().Get_Y() + 2)+"px")
                    .SetCss("left", this.GetRelativePosition().Get_X()+"px")
            } else {
                messageElem
                    .SetCss("top", (this.GetAbsolutePosition().Get_Y() + this.GetAbsoluteSize().Get_Y() + 2)+"px")
                    .SetCss("left", this.GetAbsolutePosition().Get_X()+"px")
            }

            if (!IsDefined(this._hasValidationFocusHandler) || !this._hasValidationFocusHandler)
            {
                this._hasValidationFocusHandler= true;
                this.AddFocusHandler(function(event)
                {
                    var messageElem = $q(".ValidationErrorMessage", this.GetParent());
                    if (messageElem != null)
                    {
                        messageElem.Hide();
                    }
                });
            }
        }
        if (errorString != "")
        {
            this.GetParent().AppendChild(messageElem);
            messageElem.Show();
        }
    }
    return this;
};

/**
 * @function ClearValidationError
 *
 * @returns {HTMLInputElement} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.ClearValidationError = function ()
{
    this.setCustomValidity("");
    var safari = (navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) > -1 && ((navigator.vendor && (navigator.vendor != "") && navigator.vendor.indexOf('Apple')) != "");
    if (safari)
    {
        var messageElem = $q(".ValidationErrorMessage", this.GetParent());
        if (messageElem != null)
        {
            messageElem.Hide();
        }
    }

    return this;
};

/**
 * @function IsValid
 *
 * @returns {Boolean} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.IsValid = function ()
{
    return this.validity.valid;
};

/**
 * @function CopyValueToClipboard
 * Copies the current value of this input to the user's clipboard.
 * Only supported on late-2015 version browsers and newer.
 * Older browsers show them a popup that allows them to copy.
 * @returns {boolean} true on success or false otherwise
 */
HTMLInputElement.prototype.CopyValueToClipboard = function()
{
    var success = false;
    if (IsDefined(document.execCommand))
    {
        this.SelectAll();
        try
        {
            success = document.execCommand('copy');
        }
        catch (err)
        {

        }
    }
    if (!success)
    {
        window.prompt("Copy to clipboard: Ctrl+C (Cmd+C on Mac), Enter", this.Get_Value());
        success = true;
    }
    if (IsDefined(document.selection) && IsDefined(document.selection.empty))
    {
        document.selection.empty();
    } else {
        window.getSelection().removeAllRanges();
    }
    return success;
};

HTMLInputElement.prototype.SelectAll = function ()
{
    if (this.type == "text" || this.type == "number" || this.type == "search" || this.type == "file" || this.type == "password" || this.type == "email")
    {
        this.select();
    } else {
        throw "SelectAll() only works on text, search, number, file, email, and password inputs";
    }
    return this;
};

HTMLInputElement.prototype.SelectAllText = function()
{
    return this.SelectAll();
};

/**
 * @function SelectRange
 * @param {int} startPos
 * @param {int} endPos
 * @returns {HTMLInputElement} Returns this element for chained method calls.
 */
HTMLInputElement.prototype.SelectRange = function (startPos, endPos)
{
    this.SetFocus();
    if (IsDefined(this.selectionStart)) {
        this.selectionStart = startPos;
        this.selectionEnd = endPos;
    } else if (document.selection && document.selection.createRange) {
        // IE branch
        this.select();
        var range = document.selection.createRange();
        range.collapse(true);
        range.moveEnd("character", endPos);
        range.moveStart("character", startPos);
        range.select();
    }
    return this;
};