/**
 * @namespace Default
 * @class HTMLFormElement
 */
/**
 * @function AddSubmitHandler
 *
 * @param {Function} handlerFunction
 * @returns {Element} Returns this object for chained method calls.
 */
HTMLFormElement.prototype.AddSubmitHandler = function (handlerFunction)
{
    DblEj.EventHandling.Events.AddHandler(this, "submit", handlerFunction, null);
    return this;
};

/**
 * @function AddChangeHandler
 * Add a onchange handler
 *
 * @param {Function} handlerFunction
 * @param {String} targetSelector Only fire the event if it occurs on these element descendants
 *
 * @returns {Element} Returns this element for chained method calls.
 */
HTMLFormElement.prototype.AddChangeHandler = function (handlerFunction, targetSelector)
{
    DblEj.EventHandling.Events.AddHandler(this, "change", handlerFunction, null, targetSelector);
    return this;
};

/**
 * @function CheckValidity
 * Check if the form is valid.  Most clients will display some sort of indicator and explanation for invalid fields.
 *
 * @returns Boolean
 */
HTMLFormElement.prototype.CheckValidity = function ()
{
    return this.checkValidity();
};