/**
 * @namespace Default
 * @class HTMLTableRowElement Additional functionality for the native HTMLTableRowElement class.
 */


/**
 * @function Remove
 * @returns {HTMLTableRowElement.prototype}
 */
HTMLTableRowElement.prototype.Remove = function () {
    if (this.parentNode != null)
    {
        while (this.hasChildNodes()) {
            this.removeChild(this.lastChild);
        }
        this.parentNode.removeChild(this);
    }
    return this;
};

/**
 * @function AddCell
 * Add a new cell to the end of this row
 * 
 * @param {String} innerHtml OPTIONAL The HTML to put into the new cell
 * @param {String} className OPTIONAL The name of the class to be applied to the new cell
 * @returns {HTMLTableCellElement.prototype}
 */
HTMLTableRowElement.prototype.AddCell = function (innerHtml, className)
{
    if (!IsDefined(className))
    {
        className = null;
    }
    if (IsDefined(innerHtml) || innerHtml == null)
    {
        innerHtml = "";
    }

    var newCell = this.insertCell(this.cells.length);
    newCell.SetInnerHtml(innerHtml);
    return newCell;
};

/**
 * @function GetCell
 * Get the cell at the specified index (0 based)
 * 
 * @param {Int} cellIndex
 * @returns {HTMLTableCellElement.prototype}
 */
HTMLTableRowElement.prototype.GetCell = function (cellIndex)
{
    if (!IsDefined(cellIndex) || cellIndex == null)
    {
        cellIndex = 0;
    }
    return this.cells[cellIndex];
};

/**
 * @function GetCellCount
 * @returns {Int}
 */
HTMLTableRowElement.prototype.GetCellCount = function ()
{
    return this.cells.length;
};


/**
 * @function RemoveCell
 * Remove the cell at the specified index (0 based)
 *
 * @param {Int} cellIndex
 * @returns {HTMLTableRowElement.prototype}
 */
HTMLTableRowElement.prototype.RemoveCell = function (cellIndex)
{
    if (!IsDefined(cellIndex) || cellIndex == null)
    {
        cellIndex = this.cells.length - 1;
    }
    this.deleteCell(cellIndex);
    return this;
};