/**
 * @namespace Default
 * @class Function Additional functionality for the native Function class.
 */

/**
 * @function Bind
 * Bind a function's context to a particular object.
 * @param {Object} obj
 * @returns {Function}
 */
Function.prototype.Bind = function (obj) {
    var method = this,
        temp = function () {
            return method.apply(obj, arguments);
        };
    return temp;
};