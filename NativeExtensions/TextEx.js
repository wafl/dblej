/**
 * @namespace Default
 * @class Text
 */

/**
 * Replace string within string
 * 
 * @param {String} search
 * @param {String} replace
 * @returns {Text.prototype}
 */
Text.prototype.StringReplaceTextNodes = function (search, replace)
{
    if (IsNode(replace))
    {
        if (this.data == search)
        {
            this.InsertBeforeMe(replace);
            this.data = "";
        }
    } else {
        this.data = this.data.replace(search, replace);
    }

    return this;
};

Text.prototype.Remove = function()
{
    return this.remove();
};