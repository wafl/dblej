/**
 * @namespace Default
 * @class String Additional functionality for the native String class.
 */

/**
 * @function UrlEncode
 * URL encode this string
 */
String.prototype.UrlEncode = function ()
{
    // The Javascript escape and unescape functions do not correspond
    // with what browsers actually do...
    var SAFECHARS = "0123456789" + // Numeric
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + // Alphabetic
        "abcdefghijklmnopqrstuvwxyz" +
        "-_.!~*'()";					// RFC2396 Mark characters
    var HEX = "0123456789ABCDEF";

    var encoded = "";
    for (var i = 0; i < this.length; i++) {
        var ch = this.charAt(i);
        if (ch == " ") {
            encoded += "+";				// x-www-urlencoded, rather than %20
        } else if (SAFECHARS.indexOf(ch) != -1) {
            encoded += ch;
        } else {
            var charCode = ch.charCodeAt(0);
            if (charCode > 255) {
                encoded += "+";
            } else {
                encoded += "%";
                encoded += HEX.charAt((charCode >> 4) & 0xF);
                encoded += HEX.charAt(charCode & 0xF);
            }
        }
    } // for

    return encoded;
};

/**
 * @function UrlDecode
 * URL decode this string
 */
String.prototype.UrlDecode = function ()
{
    // Replace + with ' '
    // Replace %xx with equivalent character
    // Put [ERROR] in output if %xx is invalid.
    var HEXCHARS = "0123456789ABCDEFabcdef";
    var plaintext = "";
    var i = 0;
    while (i < this.length) {
        var ch = this.charAt(i);
        if (ch == "+") {
            plaintext += " ";
            i++;
        } else if (ch == "%") {
            if (i < (this.length - 2)
                && HEXCHARS.indexOf(this.charAt(i + 1)) != -1
                && HEXCHARS.indexOf(this.charAt(i + 2)) != -1) {
                plaintext += unescape(this.substr(i, 3));
                i += 3;
            } else {
                plaintext += "%[ERROR]";
                i++;
            }
        } else {
            plaintext += ch;
            i++;
        }
    } // while
    return plaintext;
};

/**
 * @function HtmlEncode
 * Get an html-encoded copy of the string.
 * credit to: http://stackoverflow.com/a/15348311/546833
 * @param {String} inputString
 * @returns {String} Returns the encoded string
 */
String.prototype.HtmlEncode = function( ) {
    return document.createElement( 'a' ).appendChild(document.createTextNode( this ) ).parentNode.innerHTML;
};

/**
 * @function HtmlDecode
 * Get a decoded copy of the html.
 * credit to: http://stackoverflow.com/a/15348311/546833
 * @param {String} html The HTML to decode
 * @returns {String} Returns the encoded string
 */
String.prototype.HtmlDecode = function( ) {
    var a = document.createElement( 'a' );
    a.innerHTML = this;
    return a.textContent;
};

/**
 * @function Capitalize
 * Capitalize the first character of this string.
 */
String.prototype.Capitalize = function ()
{
    return this.charAt(0)
        .toUpperCase() + this.slice(1);
};

/**
 * @function Trim
 * Trim any whitespace of the beginning and end of this string
 */
String.prototype.Trim = function () {
    return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
};

/**
 * @function Collapse
 * Collapse the whitespace in this string
 */
String.prototype.Collapse = function () {
    return this.replace(/\s/g, '');
};

/**
 * @function CreateUUID
 * Create an RFC4122 UUID
 * 
 * @static
 */
String.CreateUUID = function () {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789ABCDEF";
    for (var i = 0; i < 32; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[12] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[16] = hexDigits.substr((s[16] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01

    var uuid = s.join("");
    return uuid;
};
/**
 * @function UcWords
 * An alias of UppercaseWords
 * @returns {String}
 */
String.prototype.UcWords = function ()
{
    return this.UppercaseWords();
};
/**
 * @function UppercaseWords
 * Capitalize the first letter of each word.
 * 
 * @returns {String}
 */
String.prototype.UppercaseWords = function ()
{
    var str = '';
    var parts = this.split(' ');

    Iteratize(parts);
    parts.OnEach(function (part) {
        if (part.length > 0)
        {
            str += part.charAt(0)
                .toUpperCase() + part.slice(1, part.length)
                .toLowerCase() + ' ';
        }
    });

    return str.Trim();
};

/**
 * @function CalculateWidth
 * Calculate the width, in pixels, of this string if rendered in the given font.
 * credit goes to http://stackoverflow.com/users/623933/bob-monteverde
 * @param {String} font
 * @returns {Number}
 */
String.prototype.CalculateWidth = function (font) {
    if (!IsDefined(font) || font == "" || font == null)
    {
        font = "12px arial";
    }
    var testElement = document.createElement("div");
    testElement.appendChild(document.createTextNode(this));
    testElement.style.position = "absolute";
    testElement.style.float = "left";
    testElement.style.whiteSpace = "nowrap";
    testElement.style.visibility = "hidden";
    testElement.style.font = font;
    document.body.appendChild(testElement);
    var width = testElement.GetAbsoluteSize()
        .Get_X();
    document.body.removeChild(testElement);
    return width;
};

/**
 * @function ToFloat
 * Returns a float representation of this string.
 * This function uses parseFloat internally, after converting and interpreting well-known number conventions.
 * 
 * @returns {Number}
 */
String.prototype.ToFloat = function()
{
    var str = this.trim();

    if (str.length > 2 && str.substr(0, 1) == "(" && str.substr(str.length-1, 1) == ")")
    {
        str = "-" + str.substr(1, str.length-2);
    }
    if (str == "")
    {
        str = "0";
    }
    return parseFloat(str);
};

/**
 * @function ToInt
 * Returns an integer representation of this string 
 * This function uses parseInt internally, after converting and interpreting well-known number conventions.
 * 
 * @returns {Number}
 */
String.prototype.ToInt = function()
{
    var str = this.trim();
    if (str.length > 2 && str.substr(0, 1) == "(" && str.substr(str.length-1, 1) == ")")
    {
        str = "-" + str.substr(1, str.length-2);
    }
    if (str == "")
    {
        str = "0";
    }
    return parseInt(str);
};