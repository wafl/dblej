/**
 * @namespace Default
 * @class Array
 */

/**
 * @function RemoveElementsByIndex
 * Remove a range of elements from the array.
 * Originally By John Resig (MIT Licensed)
 * Modified for DblEj
 *
 * @param {Number} fromIndex
 * @param {Number} [toIndex]
 *
 * @returns {Array}
 */
Array.prototype.RemoveElementsByIndex = function (fromIndex, toIndex) {
    fromIndex = parseInt(fromIndex);
    if (IsDefined(toIndex) && toIndex != null)
    {
        toIndex = parseInt(toIndex);
    }
    var rest = this.slice((toIndex || fromIndex) + 1 || this.length);
    this.length = fromIndex < 0 ? this.length + fromIndex : fromIndex;
    return this.push.apply(this, rest);
};

/**
 * @function RemoveElement
 * Remove an element from the array
 *
 * @param {HTMLElement} element
 *
 * @returns {Array}
 */
Array.prototype.RemoveElement = function (element) {
    var returnVal;
    for (var elemIdx in this)
    {
        if (this[elemIdx] === element)
        {
            returnVal = this.RemoveElementsByIndex(elemIdx);
            break;
        }
    }
    return returnVal;
};

if (!Array.prototype.indexOf)
{
    Array.prototype.indexOf = function (elt /*, from*/)
    {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++)
        {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}